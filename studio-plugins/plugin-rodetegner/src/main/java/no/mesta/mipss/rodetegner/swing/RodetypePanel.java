package no.mesta.mipss.rodetegner.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodetegner.RodeController;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.rodetype.NyRodetypeDialog;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.progressbar.ProgressController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;

/**
 * Panel for å velge, legge til og slette rodetyper
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class RodetypePanel extends AbstractRodePanel{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private RodetegnerModule plugin;
    private ProgressController progressController;
    
    private boolean brukerKanLageRodetyper;
    
    public RodetypePanel(RodetegnerModule plugin) {
        setBorder(BorderFactory.createTitledBorder("Rodetype"));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.plugin = plugin;
        progressController = new ProgressController(null);
        
        JButton propertiesButton = new JButton(new PropertiesAction("Egenskaper", IconResources.EDIT_ICON));
        JButton deleteButton = new JButton(new DeleteRodetypeAction(resources.getGuiString("rodetypepanel.slett"), IconResources.BUTTON_CANCEL_ICON));
        JButton newButton = new JButton(new NewRodetypeAction(resources.getGuiString("rodetypepanel.ny"), IconResources.NEW_ICON));
        
        propertiesButton.setToolTipText(resources.getGuiString("rodetypepanel.properties.tooltip"));
        deleteButton.setToolTipText(resources.getGuiString("rodetypepanel.delete.tooltip"));
        newButton.setToolTipText(resources.getGuiString("rodetypepanel.new.tooltip"));
        
        brukerKanLageRodetyper = UserUtils.isAdmin(plugin.getLoader().getLoggedOnUser(true));
        	
        if (brukerKanLageRodetyper){
        	Binding newActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rodetype != null}"), newButton, BeanProperty.create("enabled"));
            Binding deleteActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rodetype != null && rodetype.driftkontrakt!=null}"), deleteButton, BeanProperty.create("enabled"));
            plugin.getFieldToolkit().addBinding(newActivate);
            plugin.getFieldToolkit().addBinding(deleteActivate);
        }else{
        	deleteButton.setEnabled(false);
        	newButton.setEnabled(false);
        }
        
        JComboBox rodetypeCombo = new JComboBox();
		plugin.getFieldToolkit().bindDropDown(rodetypeCombo, RodeController.class, plugin.getRodeController(), "rodetype", null, Rodetype.class, plugin.getRodeController().getRodetyper(null), resources.getGuiString("rodetypepanel.velgkontrakt"));
        plugin.setRodetypeCombo(rodetypeCombo);
        
        //lag bindinger til knappene slik at de aktiveres når det er valgt en rodetype i controlleren.
        
        Binding driftkontraktNotNull = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), BeanProperty.create("driftkontraktNotNull"), rodetypeCombo, BeanProperty.create("enabled"));        
        Binding propertiesActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rodetype != null}"), propertiesButton, BeanProperty.create("enabled"));
        plugin.getFieldToolkit().addBinding(driftkontraktNotNull);
        plugin.getFieldToolkit().addBinding(propertiesActivate);
        
        //juster størrelsen på komponentene
        setComboSize(rodetypeCombo);
        setRoderButtonSize(propertiesButton);
        setButtonSize(deleteButton);
        setButtonSize(newButton);
        
        //knappepanel for ny og slett knappene
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(propertiesButton);
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(deleteButton);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(newButton);
        setButtonPanelSize(buttonPanel);
        
        //legg til komponentene til hovedpanelet
        add(rodetypeCombo);
        add(Box.createVerticalStrut(5));
        add(buttonPanel);
    }
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	plugin = null;
    }
    public int getVerticalStrut() {
        return 0;
    }

	/**
	 * @return the controller
	 */
	public ProgressController getProgressController() {
		return progressController;
	}

    /**
     * Actionklasse for å slette en rodetype
     */
    class DeleteRodetypeAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public DeleteRodetypeAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	
        	String r = plugin.getRodeController().getRodetype().getNavn();
        	MipssDialogue dialog = new MipssDialogue(
        			(JFrame)SwingUtilities.windowForComponent(RodetypePanel.this), 
        			IconResources.QUESTION_ICON, 
        			resources.getGuiString("rodetypepanel.delete.confirm.title"), 
        			resources.getGuiString("rodetypepanel.delete.confirm.message")+": \""+r+"\" ?", 
        			MipssDialogue.Type.QUESTION,
					new MipssDialogue.Button[] { MipssDialogue.Button.YES, MipssDialogue.Button.NO });
        	dialog.askUser();
        	
        	if (dialog.getPressedButton() == MipssDialogue.Button.YES) {
        		plugin.getRodeController().slettRodetype(plugin.getRodeController().getRodetype());
        	}
        }
    }
    /**
     * Actionklasse for å opprette en ny rodetype
     */
    class NewRodetypeAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public NewRodetypeAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
            NyRodetypeDialog dialog = new NyRodetypeDialog(plugin);
            dialog.pack();
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        }
    }
    
    class PropertiesAction extends AbstractAction{
    	public PropertiesAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	NyRodetypeDialog dialog = new NyRodetypeDialog(plugin, plugin.getRodeController().getRodetype());
        	dialog.setEditable(brukerKanLageRodetyper);
            dialog.pack();
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        }
    }
}
