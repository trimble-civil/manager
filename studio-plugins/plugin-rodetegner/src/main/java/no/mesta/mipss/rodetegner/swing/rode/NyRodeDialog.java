package no.mesta.mipss.rodetegner.swing.rode;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinetttype;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.AbstractRodePanel;
import no.mesta.mipss.rodetegner.swing.DefaultWrapPanel;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dialog for å opprette nye roder.
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
@SuppressWarnings("serial")
public class NyRodeDialog extends JDialog{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    private RodetegnerModule plugin;
    private NyRodePanel rodePanel;
    private Rode nyRode = new Rode();
    private Rode bakRode;
    private boolean editMode;
    public NyRodeDialog(RodetegnerModule plugin) {
        super(plugin.getLoader().getApplicationFrame(), true);
        setTitle("Opprett ny rode");
        this.plugin = plugin;
        editMode = false;
        nyRode.setVeinett(plugin.getRodeController().getNyttVeinett(nyRode));
        nyRode.setRodetype(plugin.getRodeController().getRodetype());
        initGui();
    }
	
    public NyRodeDialog(RodetegnerModule plugin, Rode rode){
    	super(plugin.getLoader().getApplicationFrame(), true);
        setTitle("Egenskaper for rode: "+rode.getNavn());
        this.plugin = plugin;
        this.bakRode = rode;
        nyRode.setRodetype(rode.getRodetype());
        nyRode.setBeskrivelse(rode.getBeskrivelse());
        nyRode.setGyldigFraDato(rode.getGyldigFraDato());
        nyRode.setGyldigTilDato(rode.getGyldigTilDato());
        nyRode.setNavn(rode.getNavn());
        nyRode.setUtenforKontrakt(rode.getUtenforKontrakt());
        Veinett v = new Veinett();
        v.setGuiFarge(rode.getVeinett().getGuiFarge());
        nyRode.setVeinett(v);
        this.editMode = true;
        initGui();
    }
    private void initGui(){
//        setResizable(false);
        setLayout(new BorderLayout());
        
        List<AbstractRodePanel> panels = new ArrayList<AbstractRodePanel>();
        if (editMode){
        	rodePanel = new NyRodePanel(plugin, nyRode, true);
        }else
        	rodePanel = new NyRodePanel(plugin, nyRode);
        panels.add(rodePanel);
        panels.add(getButtonPanel());
        
        DefaultWrapPanel wrap = new DefaultWrapPanel(panels);
        add(wrap);
//        add(getButtonPanel(), BorderLayout.SOUTH);
    }
    
    private AbstractRodePanel getButtonPanel(){
        AbstractRodePanel panel = new AbstractRodePanel(){
            public int getVerticalStrut(){
                return 5;
            }
        };
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        
        JButton cancelButton = new JButton(new CancelButtonAction("Avbryt", IconResources.CANCEL_ICON));
        JButton okButton = new JButton(new OkButtonAction("Lagre", IconResources.OK2_ICON));
        
        panel.add(Box.createHorizontalGlue());
        panel.add(cancelButton);
        panel.add(Box.createHorizontalStrut(5));
        panel.add(okButton);
        return panel;
    }
    
    /**
     * Actionklasse for å opprette en ny rode
     */
    class OkButtonAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public OkButtonAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	if (editMode){
        		boolean gyldighetEndret = !(new EqualsBuilder().append(bakRode.getGyldigFraDato(), nyRode.getGyldigFraDato()).append(bakRode.getGyldigTilDato(), nyRode.getGyldigTilDato()).isEquals());
        		//hvis gyldig til dato før og etter er null og fradato er lik er roden ikke endret. 
        		//equalsbuilderen over vil håndtere andre eventualiteter.
        		if (nyRode.getGyldigTilDato()==null&&bakRode.getGyldigTilDato()==null&&bakRode.getGyldigFraDato().equals(nyRode.getGyldigFraDato())){
        			gyldighetEndret=false;
        		}
        		
        		bakRode.setBeskrivelse(nyRode.getBeskrivelse());
        		bakRode.setGyldigFraDato(nyRode.getGyldigFraDato());
        		bakRode.setGyldigTilDato(nyRode.getGyldigTilDato());
                bakRode.setUtenforKontrakt(nyRode.getUtenforKontrakt());
        		bakRode.setNavn(nyRode.getNavn());
        		if (bakRode.getVeinett()!=null){
        			bakRode.getVeinett().setGuiFarge(nyRode.getVeinett().getGuiFarge());
        		}
        		if (gyldighetEndret){
        			Map<Rode, List<Veinettveireferanse>> overlapp = plugin.getRodeController().sjekkOverlapp(null, bakRode);
        			if (overlapp!=null){
	        			plugin.getRodeController().displayOverlappendeRoder(overlapp, bakRode.getNavn());
	        			return;
        			}
        		}
        		
        		plugin.getRodeController().mergeRode(bakRode);
        		plugin.getRodeController().updateRodeColor(bakRode);
        	}
        	else{
        		if (rodePanel.isCopySelected()){
        			Rode src = rodePanel.getSelectedRode();
        			nyRode.getVeinett().setNavn(nyRode.getNavn());
        			Rode dest = nyRode;
        			if (!plugin.getRodeController().copyRode(src, dest)){
        				return;
        			}
        		}else{
        			nyRode.getVeinett().setNavn(nyRode.getNavn());
        			plugin.getRodeController().persistRode(nyRode);
        		}
        	}
        	plugin.sendRefreshMessageToDriftkontraktmodule();
			
            NyRodeDialog.this.dispose();
        }
    }
    /**
     * Actionklasse for å avbryte opprettingen av ny rode
     */
    class CancelButtonAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public CancelButtonAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
            NyRodeDialog.this.dispose();
        }
    }
}
