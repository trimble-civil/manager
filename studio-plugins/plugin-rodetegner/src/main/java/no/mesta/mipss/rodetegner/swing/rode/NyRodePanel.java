package no.mesta.mipss.rodetegner.swing.rode;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRode;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodetegner.RodeController;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.AbstractRodePanel;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.ColorComboRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.LevProdtypeRodeTableObject;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;

/**
 * Panel for å lage nye roder.
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
@SuppressWarnings("serial")
public class NyRodePanel extends AbstractRodePanel{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
	
    private RodetegnerModule plugin ;
    private JCheckBox copyCheck = new JCheckBox("Kopier en annen rode");
    private Rode rode;
    private boolean editMode;
    
    private JComboBox rodeCombo;
    private JComboBox rodetypeCombo;
    
    private MipssBeanTableModel<LevProdtypeRodeTableObject> leverandorModel;
	private JMipssBeanTable<LevProdtypeRodeTableObject> leverandorTable;

    
    public NyRodePanel(RodetegnerModule plugin, Rode rode ) {
        this(plugin, rode, false);
    }
    
    
    public NyRodePanel(RodetegnerModule plugin, Rode rode, boolean editMode ) {
        this.plugin = plugin;
        this.rode = rode;
        this.editMode = editMode; 
        if (!editMode)
        	plugin.getRodeController().setRode(rode);
        if (editMode)
        	initLeverandorTable();
        initGui();
    }
    
    private void initLeverandorTable(){
		leverandorModel = new MipssBeanTableModel<LevProdtypeRodeTableObject>(LevProdtypeRodeTableObject.class, "leverandorNr", "leverandorNavn", "prodtyperForGui", "fraDato", "tilDato");
		MipssRenderableEntityTableColumnModel columnModelLeverandor = new MipssRenderableEntityTableColumnModel(LevProdtypeRodeTableObject.class, leverandorModel.getColumns());
		leverandorModel.setBeanInterface(RodeController.class);
		leverandorModel.setBeanInstance(plugin.getRodeController());
		leverandorModel.setBeanMethod("getLeverandorForRodeList");
		leverandorModel.setBeanMethodArguments(null);
		leverandorModel.setBeanMethodArgValues(null);
		leverandorModel.loadData();
		
		leverandorTable = new JMipssBeanTable<LevProdtypeRodeTableObject>(leverandorModel, columnModelLeverandor);
		leverandorTable.setDoWidthHacks(true);
		leverandorTable.setFillsViewportWidth(true);
		
    }
    /**
     * Initialiserer gui komponentene
     */
    private void initGui(){

        setLayout(new GridBagLayout());
        
        JLabel nameLabel = new JLabel("Rodenavn");
        JTextField nameField = new JTextField();
        nameField.setDocument(new DocumentFilter(100, null, false));
        
        JTextArea descriptionArea = new JTextArea();
        descriptionArea.setDocument(new DocumentFilter(255, null, false));
        JScrollPane descriptionScroll = new JScrollPane(descriptionArea);
        descriptionScroll.setPreferredSize(new Dimension(350,80));
        JMipssDatePicker gyldigFra = new JMipssDatePicker(Clock.now());
        gyldigFra.setTimeFormat(JMipssDatePicker.TIME_FORMAT.START_OF_DAY);
        JMipssDatePicker gyldigTil = new JMipssDatePicker(Clock.now());
        gyldigTil.setTimeFormat(JMipssDatePicker.TIME_FORMAT.END_OF_DAY);
        gyldigFra.pairWith(gyldigTil);

        JLabel kontraktSSVLabel = new JLabel("Roden er i sin helhet utenfor kontraktsgrensen");

        JCheckBox chkKontraktSVV = new JCheckBox();
        if(rode != null && rode.getUtenforKontrakt() != null) {
            chkKontraktSVV.setSelected(rode.getUtenforKontrakt());
        }
        chkKontraktSVV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBox box = (JCheckBox) e.getSource();
                if(box.isSelected()) {
                    rode.setUtenforKontrakt(true);
                }
                else {
                    rode.setUtenforKontrakt(false);
                }
            }
        });

        Rodetype rodeType = rode.getRodetype();
        if(rodeType.getId() == 15) {
            kontraktSSVLabel.setVisible(true);
            chkKontraktSVV.setVisible(true);
        }
        else {
            kontraktSSVLabel.setVisible(false);
            chkKontraktSVV.setVisible(false);
        }

        BindingGroup group = new BindingGroup();
        group.addBinding(BindingHelper.createbinding(rode, "navn", nameField, "text", UpdateStrategy.READ_WRITE));
        group.addBinding(BindingHelper.createbinding(rode, "beskrivelse", descriptionArea, "text", UpdateStrategy.READ_WRITE));
        group.addBinding(BindingHelper.createbinding(rode, "gyldigFraDato", gyldigFra, "date", UpdateStrategy.READ_WRITE));
        group.addBinding(BindingHelper.createbinding(rode, "gyldigTilDato", gyldigTil, "date", UpdateStrategy.READ_WRITE));
        group.addBinding(BindingHelper.createbinding(rode, "utenforKontraktFlagg",chkKontraktSVV,"selected",UpdateStrategy.READ_WRITE));
        group.bind();
        
        if (!editMode){
	        gyldigFra.setDate(plugin.getRodeController().getDriftkontrakt().getGyldigFraDato());
	        gyldigTil.setDate(plugin.getRodeController().getDriftkontrakt().getGyldigTilDato());
        }
        
        JLabel gyldigFraLabel = new JLabel("Gyldig fra");
        JLabel gyldigTilLabel = new JLabel("Gyldig til og med");
        JLabel beskrivelseLabel = new JLabel("Beskrivelse av roden");

        JButton leggTilLeverandor = new JButton(getLeggTilLeverandorAction());
        
        JLabel rodetypeLabel = new JLabel("Rodetype");
        JLabel rodeLabel = new JLabel("Rode");
        
        JLabel fargeLabel = new JLabel("Rodefarge");
        JComboBox fargeCombo = new JComboBox();
        fargeCombo.setRenderer(new ColorComboRenderer());


        fargeCombo.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getItem() instanceof Color){
					rode.getVeinett().setGuiFarge(ColorUtil.toHexString((Color)e.getItem()));
				}
			}
        });
        List<Color> rodefarger = getRodefarger();
        
        Color rodefarge = ColorUtil.fromHexString(rode.getVeinett().getGuiFarge());
        if (rodefarge==null)
        	rodefarge=Color.white;
        if (!rodefarger.contains(rodefarge)){
        	rodefarger.add(rodefarge);
        }
        for (Color c:rodefarger){
        	fargeCombo.addItem(c);
        }
        if (editMode){
        	fargeCombo.setSelectedItem(rodefarge);
        }else{
        	int s = rodefarger.size();
        	int index = (int)(Math.random()*s);
        	fargeCombo.setSelectedIndex(index);
        }

        add(nameLabel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
        add(nameField, new GridBagConstraints(1,0, 3,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
        
        add(gyldigFraLabel, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
        add(gyldigFra, 		new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
        
        add(gyldigTilLabel, new GridBagConstraints(2,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
        add(gyldigTil, 		new GridBagConstraints(3,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
        
        add(fargeLabel, new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
        add(fargeCombo,new GridBagConstraints(1,2, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));

        add(kontraktSSVLabel,new GridBagConstraints(0,3, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,2,5), 0,0));
        add(chkKontraktSVV,new GridBagConstraints(3,3, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,2,5), 0,0));

        add(beskrivelseLabel, new GridBagConstraints(0,4, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,2,5), 0,0));
        add(descriptionScroll, new GridBagConstraints(0,5, 4,2, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,5,5,5), 0,0));
        



        if (!editMode){
        	createCopyPanel();
            add(leggTilLeverandor, new GridBagConstraints(0,7, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
        	add(copyCheck, new GridBagConstraints(0,8, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
        	add(rodetypeLabel, new GridBagConstraints(0,9, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
        	add(rodetypeCombo, new GridBagConstraints(1,9, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
        	add(rodeLabel, new GridBagConstraints(0,10, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
        	add(rodeCombo, new GridBagConstraints(1,10, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
        
        }else{
        	JPanel panel = new JPanel(new BorderLayout());
        	panel.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("leverandor.title")));
        	JScrollPane scroll = new JScrollPane(leverandorTable);
        	leverandorTable.setPreferredScrollableViewportSize(new Dimension(0,0));
        	scroll.setPreferredSize(new Dimension(406, 80));
        	panel.add(scroll);
        	add(panel, new GridBagConstraints(0,6, 4,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
        	nameField.setText(rode.getNavn());
        	descriptionArea.setText(rode.getBeskrivelse());
        	gyldigFra.setDate(rode.getGyldigFraDato());
        	gyldigTil.setDate(rode.getGyldigTilDato());
        }
    }
    
    public Action getLeggTilLeverandorAction() {
    	return new AbstractAction(resources.getGuiString("button.leggTil"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {	
				LeggTilLevProdtypePanel levProdtypePanel = new LeggTilLevProdtypePanel(plugin.getRodeController());
				LeggTilRelasjonDialog<List<LevProdtypeRode>> relDialog = new LeggTilRelasjonDialog<List<LevProdtypeRode>>(plugin.getLoader().getApplicationFrame(), levProdtypePanel);
				List<LevProdtypeRode> levProsessRodeList = relDialog.showDialog();
				if(levProsessRodeList != null) {
					if(rode.getLevProdtypeRodeList() == null) {
						rode.setLevProdtypeRodeList(levProsessRodeList);
					} else {
						rode.getLevProdtypeRodeList().addAll(levProsessRodeList);
					}
				}
				
			}
    	};
	}
    private List<Color> getRodefarger(){
    	List<Color> rodeFarger = new ArrayList<Color>();
    	String fargeString = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.rodegenerator", "rodefarger").getVerdi();
    	
    	String[] farger = StringUtils.split(fargeString, "|");
    	for (String s:farger){
    		rodeFarger.add(getColorFromString(s));
    	}
    	return rodeFarger;
    }
    private Color getColorFromString(String s){
    	String[] rgb = StringUtils.split(s, ",");
    	int r = Integer.valueOf(rgb[0]);
    	int g = Integer.valueOf(rgb[1]);
    	int b = Integer.valueOf(rgb[2]);
    	return new Color(r,g,b);
    }
    /**
     * Lager et panel som inneholder en checkbox som aktiverer valgene for å kopiere en rode
     * @return JPanel checkbox panelet
     */
    private JPanel createCopyCheckPanel(){
        JPanel copyPanel = new JPanel();
        copyPanel.setLayout(new BoxLayout(copyPanel, BoxLayout.LINE_AXIS));
        copyPanel.add(copyCheck);
        copyPanel.add(Box.createHorizontalGlue());
        return copyPanel;
    }
    
    /**
     * Lager et panel som inneholder komponenter for å kopiere en eksisterende rode
     * @return panelet med komponentene
     */
    private void createCopyPanel(){
//        JPanel copyPanel = new JPanel();
//        copyPanel.setLayout(new BoxLayout(copyPanel, BoxLayout.LINE_AXIS));
//        copyPanel.setBorder(BorderFactory.createTitledBorder(""));
//        JLabel rodetypeLabel = new JLabel("Rodetype");
//        JLabel rodeLabel = new JLabel("Rode");
        
        rodetypeCombo =  new JComboBox();
        rodeCombo = new JComboBox();
        
        MipssListCellRenderer<Rode> rodeRenderer = new MipssListCellRenderer<Rode>();
        rodeCombo.setRenderer(rodeRenderer);
        
        
        MipssListCellRenderer<Rodetype> renderer = new MipssListCellRenderer<Rodetype>();
        List<Rodetype> rodetyper = plugin.getRodegeneratorBean().getRodetypeForKontrakt(plugin.getRodeController().getDriftkontrakt().getId());
        MipssComboBoxModel model = new MipssComboBoxModel(rodetyper, true);
        rodetypeCombo.setModel(model);
        rodetypeCombo.setRenderer(renderer);

        rodetypeCombo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Rodetype t = (Rodetype)rodetypeCombo.getSelectedItem();
				List<Rode> roder = plugin.getRodegeneratorBean().getRodeForKontrakt(plugin.getRodeController().getDriftkontrakt().getId(), t.getId());
				MipssComboBoxModel model = new MipssComboBoxModel(roder, true);
		        rodeCombo.setModel(model);
			}
        });
        
        
        
//        copyPanel.add(rodetypeLabel);
//        copyPanel.add(Box.createHorizontalStrut(5));
//        copyPanel.add(rodetypeCombo);
//        copyPanel.add(Box.createHorizontalStrut(10));
//        copyPanel.add(rodeLabel);
//        copyPanel.add(Box.createHorizontalStrut(5));
//        copyPanel.add(rodeCombo);
        
        FieldToolkit toolkit = FieldToolkit.createToolkit();
        toolkit.bindCheckBox(copyCheck, JComboBox.class, rodetypeCombo, "enabled", null);
        toolkit.bindCheckBox(copyCheck, JComboBox.class, rodeCombo, "enabled", null);
        toolkit.bind();
        copyCheck.setSelected(false);
//        return copyPanel;
    }

    public Rode getSelectedRode(){
    	return (Rode)rodeCombo.getSelectedItem();
    }
    public boolean isCopySelected(){
    	return copyCheck.isSelected();
    }
    
    
    
    /**
     * Returnerer roden som blir laget i dette panelet
     * 
     * @retrun Rode den nye roden som er laget. 
     */
    public Rode getRode(){
        return rode;
    }


	@Override
	public int getVerticalStrut() {
		// TODO Auto-generated method stub
		return 0;
	}
}
