package no.mesta.mipss.rodetegner.tools;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.MouseInputListener;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.PinLayer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;
import no.mesta.mipss.mapplugin.widgets.Widget;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.webservice.NvdbWebservice;
import no.mesta.mipss.webservice.ReflinkStrekning;

import org.jdesktop.swingx.mapviewer.GeoPosition;

public class VeiTool implements MouseInputListener{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
	private RodetegnerModule plugin;
	private VeinettService bean;
	private NvdbWebservice nvdb;
	private MaintenanceContract cbean;
	
	private TextboxData data;
	private TextboxWidget text;
	private PinLayer point;
	//private File vidkonImages;
	public VeiTool(RodetegnerModule plugin){
		bean = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
		nvdb = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
		cbean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		this.plugin = plugin;
		initTextbox();
		initPoint();
	}

	private void initPoint(){
		plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.SELECTED.getType(), "veipin");
		point = new PinLayer("veipin", false, PinLayer.ORIENTATION.CENTER, null, false);
		plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.SELECTED.getType(), point, true);
	}
	
	private void initTextbox() {
		plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.TEXTBOX.getType(), "veiinfoText");
		plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.TEXTBOX.getType(), "veiinfo");
		data = new TextboxData();
		data.setTitle(resources.getGuiString("vei.klikk"));
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		dataList.add(data);
		text = new TextboxWidget("veiinfoText", plugin.getMainMap(), 10, Widget.GLUE.TOP_LEFT, dataList);
		text.setPreferredLayerType(MipssMapLayerType.TEXTBOX.getType());
		text.setWaitInfo(resources.getGuiString("vei.sok"));
		plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.TEXTBOX.getType(), text, true);
	}
	
	public void setVeiref(Veinettveireferanse veiref, Driftkontrakt kontrakt){
		data.getData().clear();
		data.setTitle(veiref==null?resources.getGuiString("vei.fantikke"):resources.getGuiString("vei.fant"));
		if (veiref!=null){
			data.addData(resources.getGuiString("vei.txt"), getTextFromVeiref(veiref));
		}
		if (kontrakt!=null){
			data.addData(resources.getGuiString("vei.driftkontrakt"), kontrakt.getTextForGUI());
		}
	}
	
	public void setSearching(boolean searching){
		data.setLoadingDone(!searching);
		plugin.getMainMap().repaint();//OFF EDT....
	}
	private String getTextFromVeiref(Veinettveireferanse v){
		return v.getFylkesnummer()+"/"+v.getKommunenummer()+" "+v.getVeikategori()+""+v.getVeistatus()+v.getVeinummer()+" HP"+v.getHp()+"/"+v.getFraKm();//-"+v.getTilKm()+")";

	}
	public void setVeireferanse(Veinettveireferanse v){
		Point p = new Point();
		Driftkontrakt d = setVeireferansePoint(p, v);
		if (p.getX()!=0&&p.getY()!=0){
			setVeiref(v, d);	
			MipssGeoPosition gp = new MipssGeoPosition(p.getY(), p.getX());
			plugin.getMainMap().ensureVisibilityForPosition(gp, 0);
		}
	}
	@Override
	public void mouseClicked(final MouseEvent e) {
		final GeoPosition pos = plugin.getMainMap().convertPointToGeoPosition(e.getPoint());
		point.setPos(pos);
		plugin.getMainMap().repaint();
		e.consume();
		synchronized(VeiTool.class){
	        new Thread(){
	        	public void run(){
	        		setSearching(true);
	        		Point p = new Point();
	        		p.setLocation(pos.getLongitude(), pos.getLatitude());
	        		Veinettveireferanse veiref = bean.getVeireferanseForPosisjon(p);
	        		
	        		Driftkontrakt d = setVeireferansePoint(p, veiref);
	        		setVeiref(veiref, d);
	        		setSearching(false);
	        	}
	        }.start();
        }
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	@Override
	public void mouseExited(MouseEvent e) {
	}
	@Override
	public void mousePressed(MouseEvent e) {
	}
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	@Override
	public void mouseDragged(MouseEvent e) {
	}
	@Override
	public void mouseMoved(MouseEvent e) {
	}
	
	private Driftkontrakt setVeireferansePoint(Point p, Veinettveireferanse veiref) {
		Driftkontrakt d=null;
		if (veiref!=null){
			List<Veinettreflinkseksjon> l = new ArrayList<Veinettreflinkseksjon>();
			if (veiref.getVeinettreflinkseksjon()==null){
				List<Veinettreflinkseksjon> ref = nvdb.getReflinkSectionsWithinRoadref(veiref);
				if (ref.size()>0){
					veiref.setVeinettreflinkseksjon(ref.get(0));
				}else{
					return null;
				}
			}
			l.add(veiref.getVeinettreflinkseksjon());
			List<ReflinkStrekning> g = nvdb.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(l);
			if (g.size()>0){
				Point[] v = g.get(0).getVectors();
				if (v!=null&&v.length>0){
					GeoPosition gp = new GeoPosition(v[0].y, v[0].x);
					point.setPos(gp);
	        		p.setLocation(gp.getLongitude(), gp.getLatitude());
				}
			}
			if (l.size()>0&&p!=null){
				d = cbean.getDriftkontraktOnVeiref(l.get(0), p);
			}
			/*
			VidkonFile vf = null;
			for (File f:vidkonImages.listFiles()){
				vf = searchFiles(veiref, f);
				if (vf !=null) {		
					break;
				}
			}
			if (vf!=null)
				DesktopHelper.openFile(vf.getF());
			else
				System.err.println("FOF");
			*/
		}
		return d;
	}
	/*
	private class VidkonFile{
		private File f;
		
		private int fylke;
		private String veistatus;
		private int veinr;
		private int hp;
		private double kmmax;
		private double kmmin;
		private boolean med;
		
		public VidkonFile(){
			
		}

		public File getF() {
			return f;
		}

		public int getFylke() {
			return fylke;
		}

		public String getVeistatus() {
			return veistatus;
		}

		public int getVeinr() {
			return veinr;
		}

		public int getHp() {
			return hp;
		}

		public double getKmmax() {
			return kmmax;
		}

		public double getKmmin() {
			return kmmin;
		}

		public boolean isMed() {
			return med;
		}

		public void setF(File f) {
			this.f = f;
		}

		public void setFylke(int fylke) {
			this.fylke = fylke;
		}

		public void setVeistatus(String veistatus) {
			this.veistatus = veistatus;
		}

		public void setVeinr(int veinr) {
			this.veinr = veinr;
		}

		public void setHp(int hp) {
			this.hp = hp;
		}

		public void setKmmax(double kmmax) {
			this.kmmax = kmmax;
		}

		public void setKmmin(double kmmin) {
			this.kmmin = kmmin;
		}

		public void setMed(boolean med) {
			this.med = med;
		}
		
	}
	*/
}
