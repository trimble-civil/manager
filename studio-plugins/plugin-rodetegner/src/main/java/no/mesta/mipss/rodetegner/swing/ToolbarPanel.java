package no.mesta.mipss.rodetegner.swing;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodeservice.RoderapportType;
import no.mesta.mipss.rodetegner.RodeController;
import no.mesta.mipss.rodetegner.RodeController.DrawMode;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.rodestrekning.RodestrekningerDialog;
import no.mesta.mipss.rodetegner.tools.VeiTool;
import no.mesta.mipss.ui.MipssSplitButton;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.roadpicker.JRoadSelectDialog;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;

/**
 * 
 * Panel for toolbar til rodegenerator
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class ToolbarPanel extends JPanel {
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
    
    private JToggleButton fraHpStartButton;
    private JToggleButton tilHpSluttButton;
    private JToggleButton helHpButton;
    private JToggleButton viaPunktButton;
    
    private JButton tabellVisningButton;
    private JButton saveButton;
    private JButton disposeButton;
    private JButton copyToClipboard;
    
    private MipssSplitButton veiButton;
    
    private MipssSplitButton rapportButton;

    
    private RodetegnerModule plugin;
	private JToggleButton panButton;
	private MipssSplitButton zoomButton;
	
	private VeiInfo veiInfo = new VeiInfo();
    
    public ToolbarPanel(RodetegnerModule plugin) {
        this.plugin = plugin;
        initComponents();
    }
    
    public void activatePanButton(){
    	panButton.setSelected(true);
    	plugin.getRodeController().setDrawMode(DrawMode.NONE);
    }
    private void initComponents() {
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        
        setBorder(BorderFactory.createTitledBorder(""));    
        
        zoomButton = new MipssSplitButton();
        zoomButton.setToolTipText(resources.getGuiString("toolbar.zoomButton.tooltip"));
        zoomButton.getArrowButton().setToolTipText(resources.getGuiString("toolbar.zoomButton.dd.tooltip"));
        zoomButton.addMenuItem(resources.getGuiString("toolbar.zoomButton.synlige.text"), new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	plugin.getMainMap().calculateZoomForAllLayers(true);
            }
        });
        
        zoomButton.addMenuItem(resources.getGuiString("toolbar.zoomButton.kontraktveinett.text"), new ActionListener(){
            public void actionPerformed(ActionEvent e){
                plugin.getRodeController().visKontraktOmrade();
            }
        });
//        zoomButton.addMenuItem(ResourceUtil.getResource("mipssmap.swing.ToolbarPanel.zoomButton.QuickZoom.text"), new ActionListener(){
//            public void actionPerformed(ActionEvent e){
//                quickZoomButtonActionPerformed(e);
//            }
//        });
        zoomButton.setIcon(IconResources.TOOL_ZOOM_ICON);
        zoomButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                plugin.getRodeController().setDrawMode(DrawMode.ZOOM);
            }                   
        });
        
        panButton = new JToggleButton(new PanAction("", IconResources.HAND_OPEN_ICON));
        panButton.setToolTipText(resources.getGuiString("toolbar.panButton.tooltip"));
        fraHpStartButton = new JToggleButton(new FraHPStartAction("", IconResources.FRA_HP_START_ICON));
        tilHpSluttButton = new JToggleButton(new TilHPSluttAction("", IconResources.TIL_HP_SLUTT_ICON));
        helHpButton = new JToggleButton(new HelHPAction("", IconResources.HELE_HP_ICON));
        viaPunktButton = new JToggleButton(new ViapunktAction("", IconResources.PEN_ICON));
        panButton.setSelected(true);
        
        fraHpStartButton.setToolTipText(resources.getGuiString("toolbar.fraHpStartButton.tooltip"));
        tilHpSluttButton.setToolTipText(resources.getGuiString("toolbar.tilHpSluttButton.tooltip"));
        helHpButton.setToolTipText(resources.getGuiString("toolbar.helHpButton.tooltip"));
        viaPunktButton.setToolTipText(resources.getGuiString("toolbar.viaPunktButton.tooltip"));
        
        tabellVisningButton = new JButton(new ShowTableAction(null, IconResources.TABLE2_ICON));
        tabellVisningButton.setToolTipText(resources.getGuiString("toolbar.tabellVisningButton.tooltip"));
        
        saveButton = new JButton(new SaveAction(null, IconResources.SAVE_ICON_BIG));
        saveButton.setToolTipText(resources.getGuiString("toolbar.saveButton.tooltip"));
        
        disposeButton = new JButton(new DisposeAction(null, IconResources.DISPOSE_ICON));
        disposeButton.setToolTipText(resources.getGuiString("toolbar.disposeButton.tooltip"));
        
        copyToClipboard = new JButton(new CopyToClipboardAction(null, IconResources.COPY_ICON_BIG));
        copyToClipboard.setToolTipText(resources.getGuiString("toolbar.copyToClipboard.tooltip"));
        
        veiButton = new MipssSplitButton();
        veiButton.addMenuItem(resources.getGuiString("ToolbarPanel.veiButton.punktTilVeiref"), resources.getGuiString("ToolbarPanel.veiButton.punktTilVeiref.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		veiButtonActionPerformed(e);
        	}
        }, IconResources.VEI_ICON);
        
        veiButton.addMenuItem(resources.getGuiString("ToolbarPanel.veiButton.veirefTilPunkt"), resources.getGuiString("ToolbarPanel.veiButton.veirefTilPunkt.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		veirefButtonActionPerformed(e);
        	}
        }, IconResources.MAP_PIN_LEFT_SM);
        veiButton.setIcon(IconResources.VEI_BIG_ICON);
        
        rapportButton = createRapportButton();
        
        setButtonSize(panButton);
        setButtonSize(zoomButton);
        setButtonSize(fraHpStartButton);
        setButtonSize(tilHpSluttButton);
        setButtonSize(helHpButton);
        setButtonSize(viaPunktButton);
        
        ButtonGroup toggleButtonGroup = new ButtonGroup();
        toggleButtonGroup.add(panButton);
        toggleButtonGroup.add(fraHpStartButton);
        toggleButtonGroup.add(tilHpSluttButton);
        toggleButtonGroup.add(helHpButton);
        toggleButtonGroup.add(viaPunktButton);
        toggleButtonGroup.add(zoomButton);
        toggleButtonGroup.add(veiButton);
        
        setButtonSize(tabellVisningButton);
        setButtonSize(saveButton);
        setButtonSize(disposeButton);
        setButtonSize(copyToClipboard);
        setButtonSize(veiButton);
        setButtonSize(rapportButton);
        
        Dimension smallSpace = new Dimension (5, 0);
        Dimension mediumSpace = new Dimension (15, 0);
        
        add(panButton);
        add(Box.createRigidArea(smallSpace));
        add(zoomButton);
        add(zoomButton.getArrowButton());
        add(Box.createRigidArea(smallSpace));
        add(copyToClipboard);
        add(Box.createRigidArea(mediumSpace));
        add(fraHpStartButton);
        add(Box.createRigidArea(smallSpace));
        add(tilHpSluttButton);
        add(Box.createRigidArea(smallSpace));
        add(helHpButton);
        add(Box.createRigidArea(smallSpace));
        add(viaPunktButton);
        add(Box.createRigidArea(mediumSpace));
        add(tabellVisningButton);
        add(Box.createRigidArea(mediumSpace));
        add(veiButton);
        add(veiButton.getArrowButton());
        add(Box.createRigidArea(smallSpace));
        add(saveButton);
        add(Box.createRigidArea(smallSpace));
        add(disposeButton);
        add(Box.createHorizontalGlue());
        add(rapportButton);
        add(rapportButton.getArrowButton());
        Binding frahpstartActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rode != null}"), fraHpStartButton, BeanProperty.create("enabled"));
        Binding tilhpsluttActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rode != null}"), tilHpSluttButton, BeanProperty.create("enabled"));
        Binding helhpActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rode != null}"), helHpButton, BeanProperty.create("enabled"));
        Binding viapunktActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rode != null}"), viaPunktButton, BeanProperty.create("enabled"));
        Binding tabellvisActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rode != null}"), tabellVisningButton, BeanProperty.create("enabled"));
        Binding saveActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${nyeRodestrekninger != null}"), saveButton, BeanProperty.create("enabled"));
        Binding disposeActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${nyeRodestrekninger != null}"), disposeButton, BeanProperty.create("enabled"));
        Binding rapportActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${driftkotrakt != null}"), rapportButton, BeanProperty.create("enabled"));
        
        plugin.getFieldToolkit().addBinding(frahpstartActivate);
        plugin.getFieldToolkit().addBinding(tilhpsluttActivate);
        plugin.getFieldToolkit().addBinding(helhpActivate);
        plugin.getFieldToolkit().addBinding(viapunktActivate);
        plugin.getFieldToolkit().addBinding(tabellvisActivate);
        plugin.getFieldToolkit().addBinding(saveActivate);
        plugin.getFieldToolkit().addBinding(disposeActivate);
        plugin.getFieldToolkit().addBinding(rapportActivate);
    }
    private MipssSplitButton createRapportButton() {
    	final MipssSplitButton rapportButton = new MipssSplitButton();
    	rapportButton.addActionListener(new ActionListener(){
    		public void actionPerformed(ActionEvent e){
    			rapportButton.getArrowButton().doClick();
    			rapportButton.setSelected(false);
    		}
    	});
    	rapportButton.addMenuItem(resources.getGuiString("rapportButton.overlappStroBroyt"), resources.getGuiString("rapportButton.overlappStroBroyt.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		overlappStroBroyt();
        	}
        }, IconResources.EXCEL_ICON16);
        
    	rapportButton.addMenuItem(resources.getGuiString("rapportButton.hullStroBroyt"), resources.getGuiString("rapportButton.hullStroBroyt.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		hullStroBroyt();
        	}
        }, IconResources.EXCEL_ICON16);
    	rapportButton.addMenuItem(resources.getGuiString("rapportButton.utenforKontrakt"), resources.getGuiString("rapportButton.utenforKontrakt.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		utenforKontrakt();
        	}
        }, IconResources.EXCEL_ICON16);
        rapportButton.setIcon(IconResources.EXCEL_ICON16);
        return rapportButton;
	}
    
    private void overlappStroBroyt(){
    	plugin.getRodeController().visRodeRapport(RoderapportType.OVERLAPPENDE_RODER);
    }
    private void hullStroBroyt(){
    	plugin.getRodeController().visRodeRapport(RoderapportType.HULL_RODER);
    }
    
    private void utenforKontrakt(){
    	plugin.getRodeController().visRodeRapport(RoderapportType.RODER_UTENFOR_KONTRAKT);
    }

	private void veiButtonActionPerformed(ActionEvent e){
    	plugin.getRodeController().setDrawMode(RodeController.DrawMode.VEI);
    }
    private void veirefButtonActionPerformed(ActionEvent e){
    	JRoadSelectDialog dialog = new JRoadSelectDialog(plugin.getLoader().getApplicationFrame(), true,false);
    	dialog.setDriftkontrakt(plugin.getRodeController().getDriftkontrakt());
    	
    	dialog.setVeiSok(veiInfo);
		dialog.setVisible(true);
		
		if (dialog.isSet()){
			veiInfo = dialog.getVeiSok();
			if (veiInfo!=null){
				VeiTool tool = new VeiTool(plugin);
				Veinettveireferanse vr = new Veinettveireferanse();
				vr.setFylkesnummer(Long.valueOf(veiInfo.getFylkesnummer()));
				vr.setKommunenummer(Long.valueOf(veiInfo.getKommunenummer()));
				vr.setVeikategori(veiInfo.getVeikategori());
				vr.setVeinummer(Long.valueOf(veiInfo.getVeinummer()));
				vr.setVeistatus(veiInfo.getVeistatus());
				vr.setFraKm(Double.valueOf((double)veiInfo.getMeter()/1000d));
				vr.setTilKm(Double.valueOf((double)veiInfo.getMeter()/1000d));
				vr.setHp(Long.valueOf(veiInfo.getHp()));
				tool.setVeireferanse(vr);
			}
		}
    }
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	plugin = null;
    }
    private void setButtonSize(JComponent b){
        Dimension s = new Dimension(35,35);
        b.setMaximumSize(s);
        b.setPreferredSize(s);
        b.setMinimumSize(s);
    }
    /**
     * Actionklasse for å klikke ok på rodestrekningvinduet
     */
    class ShowTableAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public ShowTableAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
            RodestrekningerDialog dialog = new RodestrekningerDialog(plugin);
            dialog.pack();
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        }
    }
    
    /**
     * Actionklasse for å klikke ok på rodestrekningvinduet
     */
    class SaveAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public SaveAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	Rode rode = plugin.getRodeController().getRode();
        	MipssDialogue d = new MipssDialogue((JFrame)SwingUtilities.windowForComponent(ToolbarPanel.this),
        			IconResources.QUESTION_ICON, 
        			resources.getGuiString("toolbar.addstrekninger.title"), 
        			resources.getGuiString("toolbar.addstrekninger.message")+rode.getNavn(), 
        			MipssDialogue.Type.QUESTION,
					new MipssDialogue.Button[] { MipssDialogue.Button.YES, MipssDialogue.Button.NO });
        	
        	if (d.askUser() == MipssDialogue.Button.YES) {
        		plugin.getRodeController().persistStrekninger();
			}
        }
    }
    
    class DisposeAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public DisposeAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	plugin.getRodeController().disposeStrekninger();
        }
    }
    
    class CopyToClipboardAction extends AbstractAction{
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public CopyToClipboardAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	plugin.getMainMap().copyToClipboard();
        }
    }
    
    class PanAction extends AbstractAction{
    	public PanAction(String text, Icon image){
    		super(text, image);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			plugin.getRodeController().setDrawMode(RodeController.DrawMode.NONE);
			
		}
    }
    
    class ViapunktAction extends AbstractAction{
    	public ViapunktAction(String text, Icon image){
    		super(text, image);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			plugin.getRodeController().setDrawMode(RodeController.DrawMode.VIAPUNKT);
			
		}
    }
    class HelHPAction extends AbstractAction{
    	public HelHPAction(String text, Icon image){
    		super(text, image);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			plugin.getRodeController().setDrawMode(RodeController.DrawMode.HEL_HP);
			
		}
    }
    class TilHPSluttAction extends AbstractAction{
    	public TilHPSluttAction(String text, Icon image){
    		super(text, image);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			plugin.getRodeController().setDrawMode(RodeController.DrawMode.TIL_HP_SLUTT);
			
		}
    }
    class FraHPStartAction extends AbstractAction{
    	public FraHPStartAction(String text, Icon image){
    		super(text, image);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			plugin.getRodeController().setDrawMode(RodeController.DrawMode.FRA_HP_START);
			
		}
    }
}
