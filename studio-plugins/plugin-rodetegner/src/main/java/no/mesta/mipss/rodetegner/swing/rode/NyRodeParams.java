package no.mesta.mipss.rodetegner.swing.rode;

import java.util.Date;

public class NyRodeParams {
	private String navn;
	private Date gyldigFra;
	private Date gyldigTil;
	private String beskrivelse;
	
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		this.navn = navn;
	}
	public Date getGyldigFra() {
		return gyldigFra;
	}
	public void setGyldigFra(Date gyldigFra) {
		this.gyldigFra = gyldigFra;
	}
	public Date getGyldigTil() {
		return gyldigTil;
	}
	public void setGyldigTil(Date gyldigTil) {
		this.gyldigTil = gyldigTil;
	}
	public String getBeskrivelse() {
		return beskrivelse;
	}
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
	
}
