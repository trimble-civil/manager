package no.mesta.mipss.rodetegner.task;


import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HentRodeveinettTask extends RodegeneratorTask{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private Rode rode ;
	private RodetegnerModule plugin;
	
	public HentRodeveinettTask(RodetegnerModule plugin, Rode rode){
		super(ExecutorTaskType.PARALELL);
		this.plugin = plugin;
		this.rode = rode;
	}

	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
			return null;
		}
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		VeinettService veinettService = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
		Veinett veinettForRode = veinettService.getVeinettForRode(rode);
		if (veinettForRode==null)
			return null;
		try{
			Layer l = new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).getLayerFromRode(rode, veinettForRode);
			return l;
		} catch (IllegalArgumentException e){
			log.warn("doInBackground", e);
		}
		return null;
	}

}
