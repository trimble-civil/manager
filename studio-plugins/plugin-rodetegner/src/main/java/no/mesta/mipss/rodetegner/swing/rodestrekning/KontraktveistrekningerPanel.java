package no.mesta.mipss.rodetegner.swing.rodestrekning;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.AbstractRodePanel;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.ui.table.VeireferanseTableObject;

import org.jdesktop.swingx.JXTable;
@Deprecated//Erstattet av KontraktveinettDialog

public class KontraktveistrekningerPanel extends AbstractRodePanel{

	private RodetegnerModule plugin;
	private JCheckBox alleCheck;
	private JCheckTablePanel checkTable;
	private MipssRenderableEntityTableModel<VeireferanseTableObject> kontraktveiTableModel;
	private JXTable kontraktveiTable;
	private boolean cancelled = false;
	
	public KontraktveistrekningerPanel(RodetegnerModule plugin){
		this.plugin = plugin;
		initGui();
	}
	
	private void initGui() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		alleCheck = new JCheckBox("Velg alle");
		
		checkTable = createVeirefTable();
		Dimension size = new Dimension(465, 300);
		checkTable.setPreferredSize(size);
		
		add(getMainPanel());
		add(Box.createRigidArea(new Dimension(0, 10)));
		add(getButtonPanel());
		add(Box.createRigidArea(new Dimension(0, 10)));
		
		hentKontraktvei();
	}
	@Override
	public int getVerticalStrut() {
		return 0;
	}
	
	private JPanel getMainPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.add(checkTable);
		return panel;
	}
	private JPanel getButtonPanel(){
		JButton okButton = new JButton(new OkAction("Ok", IconResources.OK2_ICON));
		JButton cancelButton = new JButton(new CancelAction("Avbryt", IconResources.CANCEL_ICON));
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		panel.add(Box.createRigidArea(new Dimension(5,0)));
		panel.add(alleCheck);
		panel.add(Box.createHorizontalGlue());
		panel.add(cancelButton);
		panel.add(Box.createRigidArea(new Dimension(5,0)));
		panel.add(okButton);
		panel.add(Box.createRigidArea(new Dimension(5,0)));
		return panel;
	}
	
	private JCheckTablePanel createVeirefTable(){
    	String[] columns = new String[]{"valgt", "fylkenummer", "kommunenummer", "veikategori", "veistatus", "veinummer", "hp", "fraKm", "tilKm"};
		kontraktveiTableModel = new MipssRenderableEntityTableModel<VeireferanseTableObject>(VeireferanseTableObject.class, new int[] { 0 }, columns);
		MipssRenderableEntityTableColumnModel veirefColumnModel = new MipssRenderableEntityTableColumnModel(VeireferanseTableObject.class, columns);
		kontraktveiTable = new JXTable(kontraktveiTableModel,veirefColumnModel);
		
		kontraktveiTable.getColumn(0).setPreferredWidth(10);
		kontraktveiTable.getColumn(1).setPreferredWidth(20);
		kontraktveiTable.getColumn(2).setPreferredWidth(35);
		kontraktveiTable.getColumn(3).setPreferredWidth(30);
        kontraktveiTable.getColumn(4).setPreferredWidth(25);
        kontraktveiTable.getColumn(5).setPreferredWidth(20);
        kontraktveiTable.getColumn(6).setPreferredWidth(10);
        kontraktveiTable.getColumn(7).setPreferredWidth(30);
        kontraktveiTable.getColumn(8).setPreferredWidth(30);
        
		return new JCheckTablePanel<VeireferanseTableObject>(kontraktveiTable, kontraktveiTableModel, alleCheck, "Henter kontraktveinett", false);
    }
	
	private void hentKontraktvei(){
		new Thread(){
			public void run(){
				synchronized(KontraktveistrekningerPanel.this){
			    	checkTable.setBusy(true);
			    	kontraktveiTableModel.getEntities().clear();
			    	kontraktveiTable.resetSortOrder();
			    	kontraktveiTable.getSelectionModel().clearSelection();
			    	Long kontraktId = plugin.getRodeController().getDriftkontrakt().getId();
			    	
			    	Rodegenerator bean= BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class);
			    	List<Veinettveireferanse> veinettForKontrakt=bean.getVeinettForDriftkontrakt(kontraktId);
			    	List<VeireferanseTableObject> veinett = new ArrayList<VeireferanseTableObject>();
			    	for (Veinettveireferanse v :veinettForKontrakt){
			    		veinett.add(new VeireferanseTableObject(v));
			    	}
			    	kontraktveiTableModel.getEntities().addAll(veinett);
			    	checkTable.setBusy(false);
			    	alleCheck.setEnabled(kontraktveiTableModel.getEntities().size()>0);
				}
			}
		}.start();
	}
	
	public List<Veinettveireferanse> getValgte(){
		if (cancelled)
			return null;
		else{
			List<Veinettveireferanse> veiList = new ArrayList<Veinettveireferanse>();
			for (VeireferanseTableObject o:kontraktveiTableModel.getEntities()){
				if (o.getValgt()){
					veiList.add(o.getVeireferanse());
				}
			}
			return veiList;
		}
	}
	
	class OkAction extends AbstractAction{

		public OkAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			SwingUtilities.windowForComponent(KontraktveistrekningerPanel.this).dispose();
		}
	}

	class CancelAction extends AbstractAction{

		public CancelAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			cancelled = true;
			SwingUtilities.windowForComponent(KontraktveistrekningerPanel.this).dispose();
		}
		
	}

	

}
