package no.mesta.mipss.rodetegner.tools;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.event.MouseInputAdapter;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.task.ViapunktTask;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;
import no.mesta.mipss.webservice.NvdbTranslator;
import no.mesta.mipss.webservice.ReflinkStrekning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * @author Harald A Kulø
  */
public class ViapunktTool extends MouseInputAdapter{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private LineListLayer layer;
    private boolean active = false;
    private Image cursorImage = IconResources.PEN_CURSOR_ICON.getImage();
    private Point hotspot = new Point(0, 30);
    private Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, hotspot, "pen");
	private RodetegnerModule plugin;
    private VeinettService veiService;
//    private NvdbWebservice nvdb;
    
    private Color color;
    private int width;
    public ViapunktTool(RodetegnerModule plugin) {
        this.plugin = plugin;
    	
        veiService = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
//        nvdb = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
		layer = plugin.getRodeController().getTempLayer();
        plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.RODE.getType(), layer, false);
        plugin.getMainMap().setCursor(cursor);
        
    }
    
    
    public void mouseClicked(MouseEvent e) {
        if (e.getButton()==MouseEvent.BUTTON3){
        	active = false;
        	List<MipssGeoPosition[]> points = layer.getPoints();
        	List<Point> pointList = new ArrayList<Point>();
        	
        	for (MipssGeoPosition[] gp:points){
        		Point p = new Point();
        		p.setLocation(gp[0].getLongitude(), gp[0].getLatitude());
        		pointList.add(p);
        	}
        	
        	final ViapunktTask task = new ViapunktTask(plugin, layer, pointList);
        	ProgressWorkerPanel workerPanel = new ProgressWorkerPanel("Leter etter strekning....");
            workerPanel.setWorker(task);
            task.setProgressController(plugin.getProgressController());
            task.setProgressWorkerPanel(workerPanel);
            plugin.getProgressController().addWorkerPanel(workerPanel);
            new Thread(){
            	public void run(){
            		try {
    					layer = (LineListLayer)task.get();
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    				} catch (ExecutionException e) {
    					e.printStackTrace();
    				}
            	}
            }.start();
        	
            
        }else{
            active = true;
            List<MipssGeoPosition[]> points = layer.getPoints();
            
            MipssGeoPosition[] geoLine = new MipssGeoPosition[2];
            //GeoPosition pointInGeo = map.getTileFactory().pixelToGeo(e.getPoint(), map.getZoom());
            GeoPosition tempPoint = plugin.getMainMap().convertPointToGeoPosition(e.getPoint());
            Point p = new Point();
            p.setLocation(tempPoint.getLongitude(), tempPoint.getLatitude());
            Point point = veiService.snapPosisjonTilVei(p);   
            if (point==null)//klarte ikke å snappe punkt til vei
            	return;
            MipssGeoPosition pointInGeo = new MipssGeoPosition(point.getY(), point.getX());
            
//            MipssGeoPosition pointInGeo = new MipssGeoPosition(tempPoint.getLatitude(), tempPoint.getLongitude());
            
            Bounds newBounds = new Bounds();
            newBounds.setLlx(pointInGeo.getLongitude());
            newBounds.setLly(pointInGeo.getLatitude());
            newBounds.setUrx(pointInGeo.getLongitude());
            newBounds.setUry(pointInGeo.getLatitude());
            
            //no points present, create a geoLine
            if (points==null){
                geoLine[0] = pointInGeo;
            }else{
                //points present
                if (points.size()>0){
                    
                	MipssGeoPosition[] gp = points.get(points.size()-1);
                    if (gp[1]!=null){
                        geoLine[0]=gp[1];
                        geoLine[1]=pointInGeo;
                    }
                    
                    //second point of last line is not present, set second point to the clicked point
                    if (gp[1]==null){
                        gp[1]=pointInGeo;
                    }else{//add the geoLine with second point as null
                        points.add(geoLine);
                        layer.getBounds().addBounds(newBounds);
                    }
                }
            }
            
            //add the first point to layer
            if (points == null){
                points = new ArrayList<MipssGeoPosition[]>();
                points.add(geoLine);
                layer.setBounds(newBounds);
                layer.setPoints(points);
            }
            plugin.getMainMap().getLayerHandler().changeLayer(layer);
            plugin.getMainMap().repaint();
        }
    }

    public void mouseMoved(MouseEvent e) {
        if (active){
            List<MipssGeoPosition[]> points = layer.getPoints();
            GeoPosition tempPoint = plugin.getMainMap().convertPointToGeoPosition(e.getPoint());
            MipssGeoPosition pointInGeo = new MipssGeoPosition(tempPoint.getLatitude(), tempPoint.getLongitude());
            if (points!=null){
                if (points.size()>0){
                    MipssGeoPosition[] lastPoint = points.get(points.size()-1);
                    lastPoint[1] = pointInGeo;
                    plugin.getMainMap().repaint();
                    
                }
            }
        }
    }
}
