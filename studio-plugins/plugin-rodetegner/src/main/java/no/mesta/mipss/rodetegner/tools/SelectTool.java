package no.mesta.mipss.rodetegner.tools;

import java.awt.event.MouseEvent;

import javax.swing.event.MouseInputListener;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.MovableLayer;
import no.mesta.mipss.mapplugin.layer.SelectableLayer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;
import no.mesta.mipss.rodetegner.swing.RodePanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class SelectTool implements MouseInputListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MapPanel mapPanel;
    private RodePanel rodePanel;
    public SelectTool(MapPanel mapPanel, RodePanel rodePanel) {
        this.mapPanel = mapPanel;
        this.rodePanel = rodePanel;
    }

    public void mouseClicked(MouseEvent e) {
    	e.consume();
        
    	if (e.getButton()==MouseEvent.BUTTON1){
    		MovableLayer layer = mapPanel.getLayerHandler().getMovableLayerAt(e.getPoint());
	        if (layer instanceof TextboxWidget){
	        	TextboxWidget tb = (TextboxWidget)layer;
	        	if (tb.isLeftButton(e.getPoint())){
	        		tb.leftButtonClick();
	        		mapPanel.repaint();
	        	}
	        	if (tb.isRightButton(e.getPoint())){
	        		tb.rightButtonClick();
	        		mapPanel.repaint();
	        	}
	        }
	        else{
		    	mapPanel.getLayerHandler().unselectAll();
		        
		        GeoPosition tempPos = mapPanel.convertPointToGeoPosition(e.getPoint());
		        MipssGeoPosition pos = new MipssGeoPosition(tempPos.getLatitude(), tempPos.getLongitude());
		        double res = mapPanel.getResolutions().getResolutionForZoom(mapPanel.getZoom());
		        String searchRadius = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "selectionSearchRadius").getVerdi();
		        int radius = 5;
		        if (searchRadius!=null)
		        	radius = Integer.parseInt(searchRadius);
		        SelectableLayer l = mapPanel.getLayerHandler().selectLayer(pos, res*radius);
		        if (l!=null){
		        	rodePanel.selectRow(l.getId());
		        }
	        }
	        
    	}
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
    }
}
