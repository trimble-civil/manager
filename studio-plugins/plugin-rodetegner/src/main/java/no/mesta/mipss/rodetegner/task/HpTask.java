package no.mesta.mipss.rodetegner.task;

import java.awt.Point;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.tools.HpTool.HPMode;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;
import no.mesta.mipss.webservice.NvdbWebservice;
import no.mesta.mipss.webservice.ReflinkStrekning;

public class HpTask extends RodegeneratorTask{
	private NvdbWebservice ws;
	private RodetegnerModule plugin;
	private HPMode hpMode;
	private Point point;
	private VeinettService veiService;

	public HpTask(RodetegnerModule plugin, HPMode hpMode, Point point){
		super(ExecutorTaskType.PARALELL);
		
		this.plugin = plugin;
		this.hpMode = hpMode;
		this.point = point;
		ws = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
        veiService = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);

	}
	//        Point point = veiService.snapPosisjonTilVei(p1);

	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
			return null;
		}
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
		Veinettreflinkseksjon rset = veiService.getVeinettreflinkseksjonForPosisjon(point);
//		List<Veinettreflinkseksjon> rs = ws.getReflinkSectionsFromGeometry(points);
//		Veinettreflinkseksjon rset=null;
//		if (rs.size()>0){
//			rset = rs.get(0);
//		}

		if (rset!=null){
			int reflinksectiondirection = 0;
			
			switch (hpMode) {
				case HEL_HP:
					reflinksectiondirection = 2;
					break;
				case TIL_HP_SLUTT:
					reflinksectiondirection  = 1;
					break;
				case FRA_HP_START:
					reflinksectiondirection = 0;
					break;
			}
			List<Veinettreflinkseksjon> hp = ws.getReflinkSectionsWithinHp(rset.getReflinkIdent().intValue(), rset.getFra(), reflinksectiondirection);
			List<ReflinkStrekning> geo = ws.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(hp);
			LineListLayer geometry = new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).convertPointVerticesToLineListLayer(geo);
			geometry.setId("tempRode");
			geometry.setPreferredLayerType(MipssMapLayerType.RODE.getType());
			LineListLayer l = (LineListLayer)plugin.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.RODE.getType(), "tempRode");
        	if (l!=null){
        		l.getPoints().addAll(geometry.getPoints());
        		plugin.getMainMap().getLayerHandler().changeLayer(l);
        	}else{
        		plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.RODE.getType(), geometry, true);
        	}
        	plugin.getRodeController().setNyeRodestrekninger(hp);
		}
		return null;
	}
}
