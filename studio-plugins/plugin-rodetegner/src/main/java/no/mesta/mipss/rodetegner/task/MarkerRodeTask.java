package no.mesta.mipss.rodetegner.task;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.JLabel;

import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

public class MarkerRodeTask extends RodegeneratorTask{

	private List<Veinettveireferanse> veiList;
	private RodetegnerModule plugin;
	private final JLabel infoLabel;
	private LineListLayer layer;

	public MarkerRodeTask(RodetegnerModule plugin, List<Veinettveireferanse> veiList, JLabel infoLabel) {
		super(ExecutorTaskType.PARALELL);
		this.plugin = plugin;
		this.veiList = veiList;
		this.infoLabel = infoLabel;
	}

	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
			return null;
		}
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
		String name = "markering";
    	layer = new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).getLayerFromVeinettveireferanseList(veiList);
    	if (layer !=null){
    		layer.setId(name);
    		layer.setColor(layer.getSelectionLayer().getColor());
    		return layer;
    	}
    	return null;
	}
	@Override
	protected void done(){
		super.done();
		if (layer==null){
			if (infoLabel!=null){
				infoLabel.setForeground(Color.red);
				infoLabel.setFont(infoLabel.getFont().deriveFont(Font.BOLD));
				infoLabel.setText("Valgt strekning innholder en ugyldig veireferanse");
			}
		}else{
			if (infoLabel!=null){
				infoLabel.setFont(infoLabel.getFont().deriveFont(Font.PLAIN));
				infoLabel.setForeground(Color.black);
				infoLabel.setText("Strekninger ok..");
			}
		}
	}

}
