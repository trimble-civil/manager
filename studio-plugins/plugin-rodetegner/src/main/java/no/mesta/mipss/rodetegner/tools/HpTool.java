package no.mesta.mipss.rodetegner.tools;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.concurrent.ExecutionException;

import javax.swing.event.MouseInputAdapter;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.task.HpTask;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.jdesktop.swingx.mapviewer.GeoPosition;

public class HpTool extends MouseInputAdapter{

	public enum HPMode{
		TIL_HP_SLUTT,
		FRA_HP_START,
		HEL_HP
	}
	
	private RodetegnerModule plugin;
	
	private HPMode hpMode;

	
	private VeinettService veiService;
	public HpTool(RodetegnerModule plugin, HPMode hpMode){
		this.plugin = plugin;
		this.hpMode = hpMode;
		plugin.getMainMap().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		veiService = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		GeoPosition tempPoint = plugin.getMainMap().convertPointToGeoPosition(e.getPoint());
    	/*Point p1 = new Point();
    	p1.setLocation(tempPoint.getLongitude(), tempPoint.getLatitude());
    	*/
    	
        Point p = new Point();
        p.setLocation(tempPoint.getLongitude(), tempPoint.getLatitude());
        Point point = veiService.snapPosisjonTilVei(p);          
        
        
//    	Point p2 = new Point();
//    	p2.setLocation(tempPoint.getLongitude()+10, tempPoint.getLatitude()+10);
//    	List<Point> points = new ArrayList<Point>();
//    	points.add(p1);
//    	points.add(p2);
    	
    	final HpTask task = new HpTask(plugin, hpMode, point);
    	ProgressWorkerPanel workerPanel = new ProgressWorkerPanel("leter etter HP...");
        workerPanel.setWorker(task);
        task.setProgressController(plugin.getProgressController());
        task.setProgressWorkerPanel(workerPanel);
        plugin.getProgressController().addWorkerPanel(workerPanel);
        new Thread(){
        	public void run(){
        		try {
					task.get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
        	}
        }.start();
	}
}
