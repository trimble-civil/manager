package no.mesta.mipss.rodetegner.swing;

import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.ui.JMipssContractPicker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel for å velge kontrakt
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class VelgKontraktPanel extends AbstractRodePanel{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    private JMipssContractPicker kontraktCombo;

	private RodetegnerModule plugin;
    public VelgKontraktPanel(RodetegnerModule plugin) {
    	this.plugin = plugin;
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setBorder(BorderFactory.createTitledBorder("Velg kontrakt"));
        
        kontraktCombo = new JMipssContractPicker(plugin, false);
        kontraktCombo.getDropdown().setMaximumRowCount(30);
                
		
        //kontraktCombo = new JComboBox();
        //MipssListCellRenderer renderer = new MipssListCellRenderer<Driftkontrakt>();
        //kontraktCombo.setRenderer((ListCellRenderer) renderer);
        //kontraktCombo = plugin.getFieldToolkit().createDropDown(RodeController.class, plugin.getRodeController(), "driftkontrakt", null, Driftkontrakt.class, plugin.getMipssMapBean().getDriftkontraktList());
        
        JPanel kontraktPanel = new JPanel();
        kontraktPanel.setLayout(new BoxLayout(kontraktPanel, BoxLayout.LINE_AXIS));
        kontraktPanel.add(kontraktCombo);
        add(kontraktPanel);
        add(Box.createVerticalStrut(5));
        if (plugin.getLoader().getSelectedDriftkontrakt()!=null){
        	int index = kontraktCombo.getModel().getKontrakter().indexOf(plugin.getLoader().getSelectedDriftkontrakt());
        	kontraktCombo.getDropdown().setSelectedIndex(index);
        }
        kontraktCombo.addPropertyChangeListener("valgtKontrakt", plugin.getRodeController());

    }
    
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	for(PropertyChangeListener l : kontraktCombo.getPropertyChangeListeners()){
    		kontraktCombo.removePropertyChangeListener(l);
    	}
    	kontraktCombo = null;
    	this.plugin = null;
    }
    public int getVerticalStrut() {
        return 0;
    }
}
