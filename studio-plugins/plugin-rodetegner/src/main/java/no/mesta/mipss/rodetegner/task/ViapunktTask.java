package no.mesta.mipss.rodetegner.task;

import java.awt.Point;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;
import no.mesta.mipss.webservice.NvdbWebservice;
import no.mesta.mipss.webservice.ReflinkStrekning;

public class ViapunktTask extends RodegeneratorTask{

	private RodetegnerModule plugin;
	private NvdbWebservice ws;
	private LineListLayer layer;
	private List<Point> pointList;
	public ViapunktTask(RodetegnerModule plugin, LineListLayer layer, List<Point> pointList) {
		super(ExecutorTaskType.PARALELL);
		this.plugin = plugin;
		this.layer = layer;
		this.pointList = pointList;
		ws = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
	}
	
	private LineListLayer getGeometry(List<Veinettreflinkseksjon> rs){
    	List<ReflinkStrekning> geo = ws.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(rs);
    	return new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).convertPointVerticesToLineListLayer(geo);
    }
	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
			return null;
		}
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
		List<Veinettreflinkseksjon> rs = ws.getReflinkSectionsFromGeometry(pointList);
    	LineListLayer geometry = getGeometry(rs);
    	geometry.setPreferredLayerType(layer.getPreferredLayerType());
    	geometry.setId("tempRode");
    	
    	LineListLayer l = (LineListLayer)plugin.getMainMap().getLayerHandler().getLayer(layer.getPreferredLayerType(), "tempRode");
    	plugin.getMainMap().getLayerHandler().removeLayer(layer.getPreferredLayerType(), layer.getId());
    	
    	if (l!=null){
    		l.getPoints().addAll(geometry.getPoints());
    		plugin.getMainMap().getLayerHandler().changeLayer(l);
    	}else{
    		plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.RODE.getType(), geometry, true);
    	}
    	layer = plugin.getRodeController().getTempLayer();
    	plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.RODE.getType(), layer, false);
    	plugin.getRodeController().setNyeRodestrekninger(rs);
		return layer;
	}

}
