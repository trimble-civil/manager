package no.mesta.mipss.rodetegner.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.TreePath;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodetegner.RodeController;

import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

public class OverlappendeRodePanel extends JPanel{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
	private final Map<Rode, List<Veinettveireferanse>> overlapp;
	private JXHeader header = new JXHeader();
	private JXTreeTable tableTree;
	private JScrollPane tableTreeScroll;
	private final RodeController rodeController;
	private DefaultTreeTableModel model;
	private final String rode;

	public OverlappendeRodePanel(RodeController rodeController, Map<Rode, List<Veinettveireferanse>> overlapp, String rode){
		this.rodeController = rodeController;
		this.overlapp = overlapp;
		this.rode = rode;
		initComponents();
		initGui();
	}

	private void initComponents() {
		header.setTitle(resources.getGuiString("overlappende.roder.header", new Object[]{rode}));
		header.setDescription(resources.getGuiString("overlappende.roder.message"));
		header.setIcon(IconResources.WARNING_ICON);
		DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode();
		for (Rode rode:overlapp.keySet()){
			OverlappTreeTableNode node = new OverlappTreeTableNode(rode);
			for (Veinettveireferanse ref:overlapp.get(rode)){
				VeireferanseTreeTableNode child = new VeireferanseTreeTableNode(ref);
				child.setAllowsChildren(false);
				node.add(child);
			}
			root.add(node);
		}
		List<String> columns = Arrays.asList("Rode/veiref", "Rodetype", "Gyldig fradato", "Gyldig tildato");
		model = new DefaultTreeTableModel(root, columns);
		tableTree = new JXTreeTable(model);
		tableTree.getColumn(0).setPreferredWidth(150);
		
		
		tableTree.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent e) {
				
				TreePath[] selectionPath = tableTree.getTreeSelectionModel().getSelectionPaths();
				List<Veinettveireferanse> veiList = new ArrayList<Veinettveireferanse>();
				for (TreePath tp:selectionPath){
					Object source = tp.getLastPathComponent();
					if (source instanceof VeireferanseTreeTableNode){
						Veinettveireferanse veiref = (Veinettveireferanse)((VeireferanseTreeTableNode)source).getUserObject();
						veiList.add(veiref);
					}
				}
				if (!veiList.isEmpty()){
					rodeController.markerStrekninger(veiList, null, true);
				}
			}
			
		});
		tableTreeScroll = new JScrollPane(tableTree);
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(header, 		 new GridBagConstraints(0,0,1,1,1.0,0.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
		add(tableTreeScroll, new GridBagConstraints(0,1,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
	}
	
	class OverlappTreeTableNode extends DefaultMutableTreeTableNode{
		private static final int RODE_NAVN = 0;
		private static final int RODETYPE = 1;
		private static final int GYLDIG_FRA_DATO = 2;
		private static final int GYLDIG_TIL_DATO = 3;
		OverlappTreeTableNode(Rode userObject){
			super(userObject);
		}
		@Override
		public int getColumnCount() {
			return 4;
		}
		@Override
		public Object getValueAt(int column) {
			Object toBeDisplayed = null;
			
			if (getUserObject() instanceof Rode) {
				Rode rode = (Rode) getUserObject();
				switch (column) {
				case RODE_NAVN:
					toBeDisplayed = rode.getNavn();
					break;
				case RODETYPE:
					toBeDisplayed = rode.getRodetype().getNavn();
					break;
				case GYLDIG_FRA_DATO:
					toBeDisplayed = MipssDateFormatter.formatDate(rode.getGyldigFraDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);
					break;
				case GYLDIG_TIL_DATO:
					toBeDisplayed = MipssDateFormatter.formatDate(rode.getGyldigTilDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);
					break;
				}
			}
			return toBeDisplayed;
		}
	}
	class VeireferanseTreeTableNode extends DefaultMutableTreeTableNode{
		private static final int VEIREF = 0;
		VeireferanseTreeTableNode(Veinettveireferanse userObject){
			super(userObject);
		}
		@Override
		public int getColumnCount() {
			return 4;
		}
		@Override
		public Object getValueAt(int column) {
			Object toBeDisplayed = null;
			
			if (getUserObject() instanceof Veinettveireferanse) {
				Veinettveireferanse veiref = (Veinettveireferanse) getUserObject();
				switch (column) {
				case VEIREF:
					toBeDisplayed = veirefToString(veiref);
					break;
				
				}
			}
			return toBeDisplayed;
		}
		private String veirefToString(Veinettveireferanse veiref){
			return veiref.getVeikategori()+veiref.getVeistatus()+veiref.getVeinummer()+" HP"+veiref.getHp()+" "+veiref.getFraKm()+"-"+veiref.getTilKm();
		}
	}
}
