package no.mesta.mipss.rodetegner.swing;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.util.LayerEvent;

 /**
  * Panel som inneholder kartet til rodegeneratoren.
  * 
  * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
  */
public class RodetegnerMapPanel extends MapPanel {
    public RodetegnerMapPanel() {
    }

    public void layerChanged(LayerEvent e) {
    
    }
}
