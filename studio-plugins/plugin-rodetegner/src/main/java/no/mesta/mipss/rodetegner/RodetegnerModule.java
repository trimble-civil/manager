package no.mesta.mipss.rodetegner;

import java.beans.PropertyChangeListener;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.messages.LevHarUlagredeEndringerMessage;
import no.mesta.mipss.messages.RefreshLeverandorMessage;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.produksjonsrapport.Prodrapport;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.rodetegner.swing.MainMapPanel;
import no.mesta.mipss.rodetegner.swing.MipssRodegeneratorPanel;
import no.mesta.mipss.rodetegner.swing.RodePanel;
import no.mesta.mipss.rodetegner.swing.ToolbarPanel;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.JXMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.progressbar.ProgressController;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.ui.table.RodeTableObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Modul for å tegne og generere roder
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class RodetegnerModule extends MipssPlugin{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MipssRodegeneratorPanel mainGuiPanel;
    private Rodegenerator rodegeneratorBean;
    private MipssMap mipssMapBean;
        
    private JXMipssBeanTable rodeTable;
    private MipssRenderableEntityTableModel<RodeTableObject> rodeTableModel;
    
    private JComboBox rodetypeCombo;
    
    private MapPanel mainMap;

	private RodeController rodeController;
    
    private FieldToolkit fieldToolkit;
    
    private RodePanel rodePanel;
    private boolean running;
    private MainMapPanel mapPanel;
	private Prodrapport prodrappBean;
    /**
     * Default konstruktør
     */
    public RodetegnerModule() {        
        
    }
    
    /**
     * Lukker denne pluginen
     */
    public void shutdown() {
        mainMap.dispose();
        mainGuiPanel.dispose();
        mapPanel.dispose();
        rodeController.dispose();
        mapPanel = null;
        rodeController  = null;
        mainMap  = null;
        mainGuiPanel = null;
        running = false;
        for (PropertyChangeListener l:props.getPropertyChangeListeners()){
			props.removePropertyChangeListener(l);
		}
        getFieldToolkit().unbindAndRemove();
    }
    
    /**
     * Initialiserer gui komponentene til denne pluginen
     */
    public void initPluginGUI() {
    	running = true;
    	mapPanel = new MainMapPanel(this);
    	
    	setMainMap(mapPanel.getMapViewPanel());
    	String[] columns = new String[]{"valgt", "guiFarge", "navn", "rodeLengde"};
		rodeTableModel = new MipssRenderableEntityTableModel<RodeTableObject>(RodeTableObject.class, new int[] { 0, 1 }, columns);
		
        mainGuiPanel = new MipssRodegeneratorPanel(this);
        getFieldToolkit().bind();
        
        if (loader.getSelectedDriftkontrakt()!=null){
        	rodeController.setDriftkontrakt(loader.getSelectedDriftkontrakt());
        	rodeController.visKontraktOmrade();
        }
        
    }    
   
    
    /**
     * Oppretter og returnerer en RodegeneratorBean
     * @return
     */
    public Rodegenerator getRodegeneratorBean(){
        if (rodegeneratorBean == null){
            rodegeneratorBean = BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class);
        }
        return rodegeneratorBean;
    }
    
    /**
     * Oppretter og returnerer en MipssMapBean
     * @return
     */
    public MipssMap getMipssMapBean(){
        if (mipssMapBean == null){
            mipssMapBean = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
        }
        return mipssMapBean;
    }
    
    /**
     * Oppretter og returnerer en ProdrapportBean
     * @return
     */
    public Prodrapport getProdrapportBean(){
    	if (prodrappBean == null){
    		prodrappBean = BeanUtil.lookup(Prodrapport.BEAN_NAME, Prodrapport.class);
    	}
    	return prodrappBean;
    		
    	
    }
    /**
     * Returnerer panelet som inneholder denne pluginen
     * @return
     */
    public JPanel getModuleGUI() {
        return mainGuiPanel;
    }
    
    /**
     * Returnerer tabellen som inneholder roder
     * @return
     */
    public JXMipssBeanTable getRodeTable() {
        return rodeTable;
    }
    /**
     * Returnerer tabellmodellen til tabellen som inneholder roder
     * @return
     */
    public MipssRenderableEntityTableModel<RodeTableObject> getRodeTableModel() {
        return rodeTableModel;
    }
    
    public void showError(String text){
        JOptionPane.showMessageDialog(null, text, "Feil", JOptionPane.ERROR);
    }
    /**
     * Returnerer RodeControlleren som inneholder valg gjort i GUI ang kontrakt, rodetype etc. 
     * @return
     */
    public RodeController getRodeController() {
        if (rodeController==null)
            rodeController = new RodeController(this);
        return rodeController;
    }
    
    public FieldToolkit getFieldToolkit() {
        if (fieldToolkit==null){
            fieldToolkit = FieldToolkit.createToolkit();
            
        }
        return fieldToolkit;
    }

    public void setRodetypeCombo(JComboBox rodetypeCombo) {
        this.rodetypeCombo = rodetypeCombo;
    }

    public JComboBox getRodetypeCombo() {
        return rodetypeCombo;
    }
    
    public MapPanel getMainMap() {
		return mainMap;
	}
    public void setMainMap(MapPanel mainMap) {
		this.mainMap =mainMap;
	}
    
    /**
	 * @return the progressController
	 */
	public ProgressController getProgressController() {
		return mainGuiPanel.getStatusPanel().getProgressController();
	}

	public RodePanel getRodePanel() {
		return rodePanel;
	}
	
	public ToolbarPanel getToolbarPanel(){
		return mainGuiPanel.getToolbarPanel();
	}
	public void setRodePanel(RodePanel rodePanel) {
		this.rodePanel = rodePanel;
	}

	public boolean isRunning() {
		return running;
	}

	public MainMapPanel getMapPanel() {
		return mapPanel; 
	}
	
	@SuppressWarnings("unchecked")
	public void sendRefreshMessageToDriftkontraktmodule(){
		Class<? extends MipssPlugin> pluginClass;
		try {
			pluginClass = (Class<? extends MipssPlugin>) Class.forName("no.mesta.mipss.driftkontrakt.DriftkontraktAdminModule");
			boolean isRunning = getLoader().isPluginRunning(pluginClass);
			if (isRunning){
				RefreshLeverandorMessage msg = new RefreshLeverandorMessage();
				msg.setTargetPlugin(getLoader().getPlugin(pluginClass));
				getLoader().sendMessage(msg);
			}
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public boolean driftkontraktmoduleHarUlagredeEndringer(){
		boolean isRunning = false;
    	//hvis driftkontraktmodulen kjører må vi sjekke om denne har endringer som ikke er lagret. 
		//dersom vi har endringer som ikke er lagret kan vi ikke legge til nye roder ettersom 
		//dette kan føre til at tilknytningene blir fjernet når brukeren lagrer i driftkontraktmodulen.
		//det er derfor viktig å sørge for at dataene til driftkontraktmodulen blir oppdatert når
		//tilstanden endres i fra denne modulen
		Class<? extends MipssPlugin> pluginClass = null;
		try {
			pluginClass = (Class<? extends MipssPlugin>) Class.forName("no.mesta.mipss.driftkontrakt.DriftkontraktAdminModule");
			isRunning = getLoader().isPluginRunning(pluginClass);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
			isRunning = false;
		}
		
		Boolean ulagredeEndringer =false;
		if (isRunning){
			LevHarUlagredeEndringerMessage msg = new LevHarUlagredeEndringerMessage();
			msg.setTargetPlugin(getLoader().getPlugin(pluginClass));
			getLoader().sendMessage(msg);
			ulagredeEndringer = msg.getUlagredeEndringer();
		}
		return ulagredeEndringer;
		
	}
}
