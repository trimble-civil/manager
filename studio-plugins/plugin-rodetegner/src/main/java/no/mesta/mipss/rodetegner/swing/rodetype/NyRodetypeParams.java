package no.mesta.mipss.rodetegner.swing.rodetype;

public class NyRodetypeParams {
	private String typeNavn;
	private boolean overlappendeRoder;
	private boolean kunEnRode;
	private boolean raporteringsdata;
	private String beskrivelse;
	
	public String getTypeNavn() {
		return typeNavn;
	}
	public void setTypeNavn(String typeNavn) {
		this.typeNavn = typeNavn;
	}
	public boolean isOverlappendeRoder() {
		return overlappendeRoder;
	}
	public void setOverlappendeRoder(boolean overlappendeRoder) {
		this.overlappendeRoder = overlappendeRoder;
	}
	public boolean isKunEnRode() {
		return kunEnRode;
	}
	public void setKunEnRode(boolean kunEnRode) {
		this.kunEnRode = kunEnRode;
	}
	public boolean isRaporteringsdata() {
		return raporteringsdata;
	}
	public void setRaporteringsdata(boolean raporteringsdata) {
		this.raporteringsdata = raporteringsdata;
	}
	public String getBeskrivelse() {
		return beskrivelse;
	}
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
}
