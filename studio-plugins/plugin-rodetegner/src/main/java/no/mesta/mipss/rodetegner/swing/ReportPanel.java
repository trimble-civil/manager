package no.mesta.mipss.rodetegner.swing;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.ui.UIUtils;
import no.mesta.mipss.ui.WaitPanel;

import org.apache.commons.io.IOUtils;

/**
 * Panel som viser et sammendrag av statusen til rodene
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class ReportPanel extends AbstractRodePanel{

	private RodetegnerModule plugin;
	private JRadioButton alleEksAvsl;
	private JRadioButton alleInkAvsl;
	private JRadioButton alleRodetyper;
	private JRadioButton valgteRodetype;
    
    public ReportPanel(RodetegnerModule plugin) {
        this.plugin = plugin;
		setBorder(BorderFactory.createTitledBorder("Rodeoversiktrapport"));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        JButton rapportButton = new JButton(new CreateRapportAction("Hent rodeoversikt", IconResources.EXCEL_ICON16));
        rapportButton.setMargin(new Insets(5, 5, 5, 5));
        
        alleEksAvsl = new JRadioButton("Aktive roder inkl. roder med start frem i tid");
        alleInkAvsl = new JRadioButton("Alle roder inkl. avsluttede");
        alleRodetyper = new JRadioButton("Alle rodetyper");
        valgteRodetype = new JRadioButton("Valgte");
        ButtonGroup bg = new ButtonGroup();
        bg.add(alleRodetyper);
        bg.add(valgteRodetype);
        ButtonGroup bg1 = new ButtonGroup();
        bg1.add(alleEksAvsl);
        bg1.add(alleInkAvsl);
        alleRodetyper.setSelected(true);
        alleEksAvsl.setSelected(true);
        JPanel alleEksPanel = getOptionPanel(alleEksAvsl);
        JPanel alleInkPanel = getOptionPanel(alleInkAvsl);
        JPanel alleRodetyperPanel = getRadioPanel(alleRodetyper, valgteRodetype, rapportButton);
        
        BindingHelper.createbinding(plugin.getRodeController(), "driftkontraktNotNull", rapportButton, "enabled").bind();
        BindingHelper.createbinding(plugin.getRodeController(), "${rodetype!=null}", valgteRodetype, "enabled").bind();
        BindingHelper.createbinding(plugin.getRodeController(), "driftkontraktNotNull", alleEksAvsl, "enabled").bind();
        BindingHelper.createbinding(plugin.getRodeController(), "driftkontraktNotNull", alleInkAvsl, "enabled").bind();
        BindingHelper.createbinding(plugin.getRodeController(), "driftkontraktNotNull", alleRodetyper, "enabled").bind();

        
        add(alleEksPanel);
        add(alleInkPanel);
        add(alleRodetyperPanel);
    }
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	plugin = null;
    }
    
    private JPanel getRadioPanel(JRadioButton b1, JRadioButton b2, JButton button){
    	JPanel panel = UIUtils.getHorizPanel();
    	panel.add(b1);
    	panel.add(b2);
    	panel.add(Box.createHorizontalGlue());
    	if (button!=null){
            setViewButtonSize(button);
            panel.add(button);
    	}
    	return panel;
    }
    private JPanel getOptionPanel(JRadioButton box){
    	JPanel panel = UIUtils.getHorizPanel();
    	box.setHorizontalAlignment(JCheckBox.LEFT);
    	setTextLabelSize(box);
    	panel.add(box);
    	panel.add(Box.createHorizontalGlue());
    	
    	return panel;
    }

    

    private void setTextLabelSize(JComponent text){
        Dimension size = new Dimension(250, 20);
        setComponentSize(size, text);
    }
    private void setViewButtonSize(JButton button){
        Dimension size = new Dimension(130, 25);
        setComponentSize(size, button);
    }

    public int getVerticalStrut() {
        return 0;
    }

    /**
     * Actionklasse for å hente en roderapport
     */
    class CreateRapportAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public CreateRapportAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
            final Driftkontrakt d = plugin.getRodeController().getDriftkontrakt();
            if (d!=null){
            	final JDialog dialog = displayWait();
        		dialog.setVisible(true);
        		
            	SwingWorker<?, ?> w = new SwingWorker<Void, Void>(){
        			public Void doInBackground(){
        				Long rodetypeId = null;
                    	Rodetype rodetype = plugin.getRodeController().getRodetype();
                    	if (rodetype!=null){
                    		rodetypeId = rodetype.getId();
                    	}
        				Long id = d.getId();
                    	ProdrapportParams params = new ProdrapportParams();
                    	params.setBrukerSign(plugin.getLoader().getLoggedOnUserSign());
                    	params.setDriftkontraktId(id);
                    	params.setAlleInkAvsl(alleInkAvsl.isSelected());
                    	params.setRodetypeId(valgteRodetype.isSelected()&&rodetype!=null?rodetypeId:null);
                    	
		            	byte[] rapport=null;
						try {
							rapport = plugin.getProdrapportBean().getRodeoversiktRapport(params);
						} catch (Exception e) {
							dialog.dispose();
							plugin.getLoader().handleException(e, plugin, "Feil under henting av Rodeoversikten");
							e.printStackTrace();
							return null;
						}
		            	File f = new File(getTempDir()+getRapportFileName(params, 0, d));
						int c = 0;
						while (f.exists()){
							f = new File(getTempDir()+getRapportFileName(params, c++, d));
						}
		            	if (rapport!=null){
							try {
								FileOutputStream fo = new FileOutputStream(f);
								ByteArrayInputStream st = new ByteArrayInputStream(rapport);
								IOUtils.copy(st,fo);
								IOUtils.closeQuietly(fo);
								IOUtils.closeQuietly(st);
								DesktopHelper.openFile(f);
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							dialog.dispose();
						}else{
							JOptionPane.showMessageDialog(SwingUtilities.windowForComponent(plugin.getModuleGUI()), "Fant ingen data for perioden", "Ingen data funnet", JOptionPane.INFORMATION_MESSAGE);
							dialog.dispose();
						}
		            	return null;
        			}
            	};
            	w.execute();
            }
        }
    }
    private static String getTempDir() {
		String tempdir = System.getProperty("java.io.tmpdir");
    	return tempdir;
    }
    
	private String getRapportFileName(ProdrapportParams p, int c, Driftkontrakt d){
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		String fileName = f.format(Clock.now());
		String knr = d.getKontraktnummer();
		knr = knr.replaceAll("/", "");
		
		if (c==0){
			fileName+="-Rodeoversikt-"+knr+".xls";
		}else{
			fileName+="-Rodeoversikt-"+knr+"("+c+").xls";
		}
		return fileName;
	}
	private JDialog displayWait(){
		JDialog dialog = new JDialog(SwingUtilities.getWindowAncestor(plugin.getModuleGUI()));
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		WaitPanel panel = new WaitPanel("Vennligst vent mens rapporten genereres");
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		return dialog;
	}

}

//private JLabel gapLabel;
//private JLabel overlapLabel;
//private JLabel outsideContractLabel;
//
//private JLabel gapPercentLabel;
//private JLabel overlapPercentLabel;
//private JLabel outsidePercentContractLabel;

//gapPercentLabel = new JLabel("100%");
//overlapPercentLabel = new JLabel("0%");
//outsidePercentContractLabel = new JLabel("0%");    
//
//gapLabel = new JLabel("Hull");
//overlapLabel = new JLabel("Overlapping");
//outsideContractLabel = new JLabel("Utenfor kontraktveinett");
//
//JButton showGapButton = new JButton(new ShowGapAction("Vis", IconResources.NORGE_ICON));
//JButton showOverlapButton = new JButton(new ShowOverlapAction("Vis", IconResources.NORGE_ICON));
//JButton showOutsideContractButton = new JButton(new ShowOutsideContractAction("Vis", IconResources.NORGE_ICON));
//
//JPanel gapPanel = getSummaryPanel(gapPercentLabel, gapLabel, showGapButton, true);
//JPanel overlapPanel = getSummaryPanel(overlapPercentLabel, overlapLabel, showOverlapButton, false);
//JPanel outsideContractPanel = getSummaryPanel(outsidePercentContractLabel, outsideContractLabel, showOutsideContractButton, true);

//private JPanel getSummaryPanel(JLabel percent, JLabel text, JButton button, boolean darker){
//percent.setHorizontalAlignment(JLabel.RIGHT);
//JPanel panel = new JPanel();
//panel.setOpaque(true);
//Color color = panel.getBackground();
//
//float factor = 0.97f;
//if (darker){
//  factor = 0.92f;
//}
//Color c = new Color(Math.max((int)(color.getRed()  *factor), 0), 
//           Math.max((int)(color.getGreen()*factor), 0),
//           Math.max((int)(color.getBlue() *factor), 0));
//panel.setBackground(c);
//
//panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
//
//setPercentSize(percent);
//setTextLabelSize(text);
//setViewButtonSize(button);
//
////panel.add(Box.createHorizontalStrut(20));
//panel.add(percent);
//panel.add(Box.createRigidArea(new Dimension (10, 0)));
//panel.add(text);
//panel.add(Box.createHorizontalGlue());
//panel.add(button);
////panel.add(Box.createRigidArea(new Dimension (20, 0)));
//return panel;
//}
///**
//* Actionklasse for å slette en rodetype
//*/
//class ShowGapAction extends AbstractAction{
// 
// /**
//  * Konstruktør
//  * @param text tekst til knappen
//  * @param image ikonet på knappen
//  */
// public ShowGapAction(String text, Icon image){
//     super(text, image);
// }
// /**
//  * Action
//  * @param e
//  */
// public void actionPerformed(ActionEvent e) {
//     
// }
//}
//
///**
//* Actionklasse for å slette en rodetype
//*/
//class ShowOverlapAction extends AbstractAction{
// 
// /**
//  * Konstruktør
//  * @param text tekst til knappen
//  * @param image ikonet på knappen
//  */
// public ShowOverlapAction(String text, Icon image){
//     super(text, image);
// }
// /**
//  * Action
//  * @param e
//  */
// public void actionPerformed(ActionEvent e) {
//     
// }
//}
///**
//* Actionklasse for å slette en rodetype
//*/
//class ShowOutsideContractAction extends AbstractAction{
// 
// /**
//  * Konstruktør
//  * @param text tekst til knappen
//  * @param image ikonet på knappen
//  */
// public ShowOutsideContractAction(String text, Icon image){
//     super(text, image);
// }
// /**
//  * Action
//  * @param e
//  */
// public void actionPerformed(ActionEvent e) {
//     
// }
//}