package no.mesta.mipss.rodetegner.swing;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import no.mesta.mipss.rodetegner.RodetegnerModule;

/**
 * Hovedpanelet til høyre i skjermbildet, som inneholder alle komponentene
 * for editering, legge til og slette roder og rodetyper
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class MainLeftPanel extends JPanel{
    
	private RodePanel rodePanel;
	private VelgKontraktPanel kontraktPanel;
	private RodetypePanel rodetypePanel;
	private ReportPanel summaryPanel;
	private VisKontraktveinettPanel visKontraktveinettPanel;
    public MainLeftPanel(RodetegnerModule plugin) {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        kontraktPanel = new VelgKontraktPanel(plugin);
        rodetypePanel = new RodetypePanel(plugin);
        rodePanel = new RodePanel(plugin);
        summaryPanel = new ReportPanel(plugin);
        add(kontraktPanel);
        add(rodetypePanel);
        add(rodePanel);
        add(summaryPanel);
        visKontraktveinettPanel = new VisKontraktveinettPanel(plugin);
        add(visKontraktveinettPanel);
        add(Box.createVerticalStrut(10));
        add(Box.createVerticalGlue());
    }
    
    
    public RodePanel getRodePanel(){
    	return rodePanel;
    }
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	rodePanel.dispose();
    	kontraktPanel.dispose();
    	rodetypePanel.dispose();
    	summaryPanel.dispose();
    	visKontraktveinettPanel.dispose();
    	
    	rodePanel = null;
    	kontraktPanel = null;
    	rodetypePanel=null;
    	summaryPanel = null;
    	visKontraktveinettPanel = null;
    	
    }
}
