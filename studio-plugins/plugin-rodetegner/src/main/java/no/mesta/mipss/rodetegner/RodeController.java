package no.mesta.mipss.rodetegner;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.layer.KontraktveiCollectionLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.tools.MoveLayerTool;
import no.mesta.mipss.mapplugin.tools.PanMapTool;
import no.mesta.mipss.mapplugin.tools.SelectionTool;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MapUtilities;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.MouseListenerManager;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRode;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinetttype;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.rodeservice.RoderapportType;
import no.mesta.mipss.rodetegner.swing.OverlappendeRodePanel;
import no.mesta.mipss.rodetegner.task.HentKontraktveinettTask;
import no.mesta.mipss.rodetegner.task.HentRodeveinettTask;
import no.mesta.mipss.rodetegner.task.MarkerRodeTask;
import no.mesta.mipss.rodetegner.task.RapportTask;
import no.mesta.mipss.rodetegner.tools.HpTool;
import no.mesta.mipss.rodetegner.tools.SelectTool;
import no.mesta.mipss.rodetegner.tools.VeiTool;
import no.mesta.mipss.rodetegner.tools.ViapunktTool;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.ui.VeitypePanel;
import no.mesta.mipss.ui.WaitPanel;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;
import no.mesta.mipss.ui.table.LevProdtypeRodeTableObject;
import no.mesta.mipss.ui.table.LeverandorSelectableObject;
import no.mesta.mipss.ui.table.ProdtypeSelectableObject;
import no.mesta.mipss.ui.table.RodeTableObject;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.webservice.NvdbWebservice;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;

/**
 * Kontrollerklasse for å styre valgene som blir gjort i gui
 * Gui komponentene for Rodegeneratoren bindes til verdier i denne klasse
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class RodeController implements PropertyChangeListener{
	
	public enum DrawMode{
		FRA_HP_START,
		HEL_HP,
		NONE,
		TIL_HP_SLUTT,
		VIAPUNKT, 
		ZOOM,
		VEI
	}
	
	private static boolean NVDB_AVAILABLE=true;
	
	private DrawMode drawMode;
	
    private Driftkontrakt driftkontrakt;
    private boolean driftkontraktNotNull;
    
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private List<Veinettreflinkseksjon> nyeRodestrekninger;
    private RodetegnerModule plugin;
    private PropertyChangeSupport props = new PropertyChangeSupport(this);
    private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
    private Rode rode;
    private Rodegenerator rodeBean;
	private MaintenanceContract contractBean;
	private NvdbWebservice nvdb;
	
	
	private boolean visKunAktiveRoder=true;
	
    private List<Rode> roder;
	private Rodetype rodetype;
    
	private JComboBoxBinding<Rodetype, List<Rodetype>, JComboBox> rodetypeBinding;

	private VeinettService veinettservice;
    
    /**
     * Konstruktør
     * @param plugin
     */
    public RodeController(RodetegnerModule plugin) {
        this.plugin = plugin;
        rodeBean = BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class);
        contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
        nvdb = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
        veinettservice = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
        
        isNvdbAvailable();
    }
    
    private boolean isNvdbAvailable(){
    	if (!nvdb.isNvdbLoaded()){
        	NVDB_AVAILABLE=false;
        	JOptionPane.showMessageDialog(plugin.getModuleGUI(), resources.getGuiString("nvdb.notavailable", EpostUtils.getSupportEpostAdresse()), resources.getGuiString("nvdb.notavailable.title"), JOptionPane.WARNING_MESSAGE);
        }
    	return NVDB_AVAILABLE;
    }
    public void addLayer(Layer layer, LayerType type){
    	plugin.getMainMap().getLayerHandler().addLayer(type, layer, true);
    }
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + l + ")");
        props.addPropertyChangeListener(l);
    }
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + propName + ", " + l + ")");
        props.addPropertyChangeListener(propName, l);
    }
    
    /**
     * Legger til en rode til listen med roder
     * @param rode
     */
    public void addRode(Rode rode){
        if (roder==null){
            roder = new ArrayList<Rode>();
        }
        List<Rode> old = this.roder;
        roder.add(rode);
        props.firePropertyChange("roder", old, roder);
    }
    
    /**
     * Kopierer en rode, sjekker overlapp på rodetypen.
     * 
     * @param src
     * @param dest
     * @return true hvis roden ble kopiert
     */
    public boolean copyRode(Rode src, Rode dest){
    	if (!isNvdbAvailable())
    		return false;
    	plugin.enforceWriteAccess();
    	dest.setKontraktId(driftkontrakt.getId());
    	//dest.setRodetype(src.getRodetype());
    	String sign = plugin.getLoader().getLoggedOnUserSign();
    	dest.getOwnedMipssEntity().setOpprettetAv(sign);
    	Map<Rode, List<Veinettveireferanse>> overlapp = rodeBean.sjekkForOverlapp(src, dest);
    	if (overlapp!=null&&!overlapp.isEmpty()){
    		displayOverlappendeRoder(overlapp, dest.getNavn());
    		return false;
    	}
    	Rode rode = rodeBean.copyRode(src, dest);
    	replaceRodeInModel(rode, null);
    	addRode(rode);
    	return true;
    }
    private JDialog displayWait(String msg){
		JDialog dialog = new JDialog(SwingUtilities.getWindowAncestor(plugin.getModuleGUI()));
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		dialog.setModal(true);
		WaitPanel panel = new WaitPanel(msg);
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		return dialog;
	}
    
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	this.plugin = null;
    	rodetypeBinding = null;
    }

    public void disposeStrekninger(){
    	plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.RODE.getType(), "tempRode");
    	setNyeRodestrekninger(null);
    }
    
    /**
     * Returnerer den valgte driftkontrakten
     * @return
     */
    public Driftkontrakt getDriftkontrakt() {
        log.trace("Henter valgt kontrakt");
        return driftkontrakt;
    }
   
    public List<Veinettreflinkseksjon> getNyeRodestrekninger(){
    	return nyeRodestrekninger;
    }
    
    public Rode getRode(){
    	return rode;
    }
    
    /**
     * Returnerer listen med roder
     * @return
     */
    public List<Rode> getRoder() {
        return roder;
    }

    /**
     * Returnerer den valgte rodetypen
     * @return
     */
    public Rodetype getRodetype() {
        log.debug("Henter rodetype:"+rodetype);
        return rodetype;
    }
    /**
     * Henter rodetyper fra sessionbean
     * @return
     */
    public List<Rodetype> getRodetyper(){
        log.trace("Henter rodetyper");
        if (driftkontrakt!=null){
        	List<Rodetype> rodetyper = plugin.getRodegeneratorBean().getRodetypeForKontrakt(driftkontrakt.getId());
        	
            return rodetyper;
        }
        return null;
    }
    //TODO sjekk overlapp
    public void endreRodetype(final List<Rode> rodeList, final Rodetype nyRodetype){
    	final JDialog d = displayWait("Vennligst vent mens rodetype endres...");
    	SwingWorker<?, ?> w = new SwingWorker<Object, Object>(){	
			public Object doInBackground(){
				try{
					List<Rode> roderSomVilOverlappe = new ArrayList<Rode>();
					Map<Rode, List<Veinettveireferanse>> sjekkOverlapp = sjekkOverlapp(rodeList, nyRodetype, roderSomVilOverlappe);
					if (sjekkOverlapp!=null){
						d.dispose();
						displayOverlappendeRoder(sjekkOverlapp, getJoinedRodeNavn(roderSomVilOverlappe));
						return null;
					}
					
			    	for (Rode r:rodeList){
			    		r.setRodetype(nyRodetype);
			    		plugin.getRodegeneratorBean().mergeRode(r);
			    	}
				} catch (Exception e){
					d.dispose();
					ErrorHandler.showError(e, resources.getGuiString("error.feilVedEndringAvRodetype", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		};
		log.debug("execute");
		w.execute();
		d.setVisible(true);
    	
    }
    
    private String getJoinedRodeNavn(List<Rode> rode){
    	List<String> rodenavn = new ArrayList<String>();
    	for (Rode r:rode){
    		rodenavn.add(r.getNavn());
    	}
    	return StringUtils.join(rodenavn, ',');
    }
    
    /**
     * Henter rodetyper fra sessionbean
     * @return
     */
    public List<Rodetype> getRodetyper(Long driftkontraktId){
        log.trace("Henter rodetyper");
        List<Rodetype> rodetypeForKontrakt =null;
        if (driftkontraktId!=null){
        	rodetypeForKontrakt = rodeBean.getRodetypeForKontrakt(driftkontraktId);
        	if (rodetypeForKontrakt.isEmpty()){
        		Rodetype empty = new Rodetype();
        		empty.setNavn("Kontrakten har ingen rodetyper...");
        		rodetypeForKontrakt.add(empty);
        	/*}else{ Farvel, vi sees senere.. ;-)
        		Date gyldigFraDato = driftkontrakt.getGyldigFraDato();
            	if (gyldigFraDato.after(Clock.now())){
            		List<Rodetype> utenprod = new ArrayList<Rodetype>();
            		for (Rodetype rt:rodetypeForKontrakt){
            			if (!rt.getGenererProduksjonFlagg()){
            				utenprod.add(rt);
            			}
            		}
            		rodetypeForKontrakt = utenprod;
            	}*/
        	}
        }
        if (rodetypeForKontrakt!=null){
        	Collections.sort(rodetypeForKontrakt, new Comparator<Rodetype>() {
				@Override
				public int compare(Rodetype r1, Rodetype r2) {
					return r1.getId().compareTo(r2.getId());
				}
			});
        }
        return rodetypeForKontrakt;
    }
    
    /**
     * Lager et temporært layer som benyttes av tegneverktøyene. 
     * @return
     */
    public LineListLayer getTempLayer(){
    	KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
		String col = params.hentEnForApp("studio.mipss.rodegenerator", "tegnefarge").getVerdi();
		String alpha = params.hentEnForApp("studio.mipss.rodegenerator", "transparency").getVerdi();
		String bredde = params.hentEnForApp("studio.mipss.rodegenerator", "tegnebredde").getVerdi();
		int a = Integer.valueOf(alpha);
		Color c = ColorUtil.fromHexString(col);
		c = new Color(c.getRed(), c.getGreen(), c.getBlue(), a);
		int width = Integer.valueOf(bredde);
		
    	LineListLayer layer = new LineListLayer("temp", null, false, null);
		layer.setPreferredLayerType(MipssMapLayerType.RODE.getType());
        layer.setAntialias(true);
        layer.setColor(c);
        layer.setStroke(width);
        layer.setConnectorMarkers(true);
        return layer;
    }
    
    /**
     * Returnerer true hvis Driftkontrakt ikke er null
     * @return
     */
    public boolean isDriftkontraktNotNull() {
        return driftkontraktNotNull;
    }

    public void markerStrekninger(List<Veinettveireferanse> veiList, JLabel infoLabel, final boolean recenterAndZoom) {
    	final MarkerRodeTask task = new MarkerRodeTask(plugin, veiList, infoLabel);
		ProgressWorkerPanel workerPanel = new ProgressWorkerPanel("hent veinett...");
        workerPanel.setWorker(task);
        task.setProgressController(plugin.getProgressController());
        task.setProgressWorkerPanel(workerPanel);
        plugin.getProgressController().addWorkerPanel(workerPanel);
        new Thread("Hent veinett Thread()"){
    		public void run(){
    			try{
    				Layer l = task.get();
    				if (l!=null){
	    				String id = l.getId();
	    				if (recenterAndZoom){
	    		    		l.setTriggerRecenterAndZoom(true);
	    		    	}
	    				plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.SELECTED.getType(), id);
	    		    	plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.SELECTED.getType(), l, true);
	    		    	
    				}
    			} catch (InterruptedException e1) {
					e1.printStackTrace();
				} catch (ExecutionException e1) {
					e1.printStackTrace();
				}
    		}
        }.start();
    }
    
    public void mergeRode(Rode rode){
    	if (!isNvdbAvailable())
    		return;
    	plugin.enforceWriteAccess();
    	String sign = plugin.getLoader().getLoggedOnUserSign();
		rode.getOwnedMipssEntity().setEndretAv(sign);
    	rode = rodeBean.mergeRode(rode);
    	rode.setRodeLengde(rodeBean.getLengdeForVeinett(rode.getVeinett().getId()));
    	replaceRodeInModel(rode, null);
    }
    
    
    public String mergeRodetype(Rodetype rodetype){
    	if (!isNvdbAvailable())
    		return "Feil";
    	plugin.enforceWriteAccess();
    	String msg=null;
    	for (Rodetype r:getRodetyper()){
    		if (r.getNavn().equals(rodetype.getNavn())){
    			msg = "Det finnes en rode med samme navn:"+rodetype.getNavn();
    		}
    	}
    	if (msg!=null)
    		return msg;
    	
    	String sign = plugin.getLoader().getLoggedOnUserSign();
    	rodetype.getOwnedMipssEntity().setEndretAv(sign);
    	rodetype = rodeBean.mergeRodetype(rodetype);
//    	setRodetype(null);
    	setRodetype(rodetype);
    	((Rodetype)plugin.getRodetypeCombo().getSelectedItem()).setNavn(rodetype.getNavn());
    	return msg;
    }
    
    public void mergeStekninger(final List<Veinettveireferanse> list){
    	if (!isNvdbAvailable())
    		return;
    	
    	final JDialog d = displayWait("Vennligst vent mens roden lagres...");
    	SwingWorker<?, ?> w = new SwingWorker<Object, Object>(){	
			public Object doInBackground(){
				try{
					List<Veinettreflinkseksjon> veinettreflinkseksjonFromVeireferanser = veinettservice.getVeinettreflinkseksjonFromVeireferanser(list);
					Map<Rode, List<Veinettveireferanse>> sjekkOverlapp = sjekkOverlapp(veinettreflinkseksjonFromVeireferanser, rode);
					if (sjekkOverlapp!=null){
						d.dispose();
						displayOverlappendeRoder(sjekkOverlapp, rode.getNavn());
						return null;
					}
					String sign = plugin.getLoader().getLoggedOnUserSign();
					rode.getOwnedMipssEntity().setEndretAv(sign);
					Rode rode = rodeBean.persistVeirefStrekninger(list, getRode());
			    	rode.setRodeLengde(rodeBean.getLengdeForVeinett(rode.getVeinett().getId()));
			    	visRode(rode, true, true);
			    	replaceRodeInModel(rode, true);
			    	d.dispose();
				} catch (Exception e){
					d.dispose();
					ErrorHandler.showError(e, resources.getGuiString("error.feilVedLagringAvRode", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		};
		log.debug("execute");
		w.execute();
		d.setVisible(true);
    	
		/*plugin.enforceWriteAccess();
    	Rode rode = rodeBean.persistVeirefStrekninger(list, getRode());
    	replaceRodeInModel(rode, null);
    	if (plugin.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.RODE.getType(), ""+rode.getId())!=null){
    		visRode(rode, true, true);
    	}*/
    }
    
    private List<Veinettreflinkseksjon> mergeStrekninger(List<Veinettreflinkseksjon> list){
    	log.debug("Sort start");
    	sort2(list);
    	log.debug("sort end..");
    	List<Veinettreflinkseksjon> merged = new ArrayList<Veinettreflinkseksjon>();
    	Veinettreflinkseksjon forrige = null;
    	for (Veinettreflinkseksjon v:list){
    		if (merged.isEmpty()){
    			merged.add(v);
    			forrige = v;
    		}else{
				//en annen reflink
				if (v.getReflinkIdent().longValue()!=forrige.getReflinkIdent().longValue()){
					merged.add(v);
					forrige = v;
				}else{
					double fra = v.getFra();
					double til = v.getTil();
					//overlapp
					if (fra<=forrige.getTil()&&til>forrige.getTil()){
						forrige.setTil(til);
					}else if (fra>forrige.getTil()){
						merged.add(v);
						forrige = v;
					}
				}
    		}
    	}
    	return merged;
    }
    public Map<Rode,List<Veinettveireferanse>> sjekkOverlapp(List<Veinettreflinkseksjon> nyeStrekninger, Rode rode){
    	Veinett veinett = veinettservice.getVeinettForRode(rode);
    	rode.setVeinett(veinett);
    	 Map<Rode, List<Veinettveireferanse>> sjekkForOverlapp = rodeBean.sjekkForOverlapp(nyeStrekninger, rode, rode.getRodetype().getId());
    	 if (sjekkForOverlapp.isEmpty())
    		 return null;
    	 return sjekkForOverlapp;
    }
    
    /**
     * Sjekker om en liste med roder vil generere overlapp på angitt rodetype. 
     * 
     * OBS rodene som vil bli overlappet har fått ny id og nytt navn og skal ikke på noen som helst måte brukes videre(SPESIELT IKKE LAGRES!)
     * @param roder
     * @param rodetype
     * @return
     */
    public Map<Rode,List<Veinettveireferanse>> sjekkOverlapp(List<Rode> roder, Rodetype rodetype, List<Rode> outRoderSomVilOverlappe){
    	Map<Rode, List<Veinettveireferanse>> overlapp = new HashMap<Rode, List<Veinettveireferanse>>();
    	for (Rode r:roder){
    		Veinett veinett = veinettservice.getVeinettForRode(r);
    		r.setVeinett(veinett);
        	Map<Rode, List<Veinettveireferanse>> sjekkForOverlapp = rodeBean.sjekkForOverlapp(null, r, rodetype.getId());
        	for (Rode overlappetRode:sjekkForOverlapp.keySet()){
        		overlappetRode.setId(new Random().nextLong());
        		overlappetRode.setNavn(overlappetRode.getNavn()+"->"+r.getNavn());
        	}
        	overlapp.putAll(sjekkForOverlapp);
        	if (!sjekkForOverlapp.isEmpty())
        		outRoderSomVilOverlappe.add(r);
    	}
    	
    	 if (overlapp.isEmpty())
    		 return null;
    	 return overlapp;
    }
    
    /**
     * Lagrer en rode og oppdaterer guid
     * @param rode
     */
    public void persistRode(Rode rode){	
    	plugin.enforceWriteAccess();
    	
    	rode.setKontraktId(driftkontrakt.getId());
    	rode.setRodetype(getRodetype());    	
    	String sign = plugin.getLoader().getLoggedOnUserSign();
		rode.getOwnedMipssEntity().setOpprettetAv(sign);
		rode.getOwnedMipssEntity().setOpprettetDato(Clock.now());
    	rode = rodeBean.persistRode(rode);
    	replaceRodeInModel(rode, null);
    	addRode(rode);
    }
    
    /**
     * Lagrer en ny rodetype
     * 
     * @param rodetype
     * @return feilmelding dersom det finnes en rodetype med samme navn
     */
    public String persistRodetype(Rodetype rodetype){
    	plugin.enforceWriteAccess();
    	String msg=null;
    	for (Rodetype r:getRodetyper()){
    		if (r.getNavn().equals(rodetype.getNavn())){
    			msg = "Det finnes en rode med samme navn:"+rodetype.getNavn();
    		}
    	}
    	if (msg!=null)
    		return msg;
    	rodetype.setValgbarFlagg(true);
    	rodetype.setGenererDauFlagg(false);
    	String sign = plugin.getLoader().getLoggedOnUserSign();
		rodetype.getOwnedMipssEntity().setOpprettetAv(sign);
		
    	rodetype = rodeBean.persistRodetypeForKontrakt(rodetype, driftkontrakt);
    	resetDriftkontrakt(getDriftkontrakt());
    	plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.RODE.getType());
    	setRodetype(rodetype);
    	return msg;
    }
    
    /**
     * Lagrer strekninger på en rode. 
     */
    public void persistStrekninger(){
    	if (!isNvdbAvailable())
    		return;
    	plugin.enforceWriteAccess();
    	final JDialog d = displayWait("Vennligst vent mens roden lagres...");
    	SwingWorker<?, ?> w = new SwingWorker<Object, Object>(){	
			public Object doInBackground(){
				try{
					Map<Rode, List<Veinettveireferanse>> sjekkOverlapp = sjekkOverlapp(nyeRodestrekninger, rode);
					if (sjekkOverlapp!=null){
						d.dispose();
						displayOverlappendeRoder(sjekkOverlapp, rode.getNavn());
						return null;
					}
					
					
					String sign = plugin.getLoader().getLoggedOnUserSign();
					rode.getOwnedMipssEntity().setEndretAv(sign);
			    	Rode rode = rodeBean.persistStrekninger(nyeRodestrekninger, getRode());
			    	rode.setRodeLengde(rodeBean.getLengdeForVeinett(rode.getVeinett().getId()));
			    	visRode(rode, true, true);
			    	plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.RODE.getType(), "tempRode");
			    	setNyeRodestrekninger(null);
			    	replaceRodeInModel(rode, true);
			    	d.dispose();
				} catch (Exception e){
					d.dispose();
					ErrorHandler.showError(e, resources.getGuiString("error.feilVedLagringAvRode", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		};
		log.debug("execute");
		w.execute();
		d.setVisible(true);
		log.debug("done executing..");
    }
    public void displayOverlappendeRoder(Map<Rode, List<Veinettveireferanse>> overlapp, String rode){
    	JDialog d = new JDialog(plugin.getLoader().getApplicationFrame(), resources.getGuiString("overlappende.roder.title"));
    	d.add(new OverlappendeRodePanel(this, overlapp, rode));
    	d.setSize(600, 400);
    	d.setLocationRelativeTo(null);
    	d.setVisible(true);
    }
    @Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();
		if (StringUtils.equals("valgtKontrakt", property)) {
			Driftkontrakt now = (Driftkontrakt) evt.getNewValue();
			setDriftkontrakt(now);
		}
	}
    
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + l + ")");
        props.removePropertyChangeListener(l);
    }
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + propName + ", " + l + ")");
        props.removePropertyChangeListener(propName, l);
    }
    
    public void removeRode(Rode rode){
    	List<Rode> old = this.roder;
    	roder.remove(rode);
    	props.firePropertyChange("roder", old, roder);
    }
    
    private void replaceRodeInModel(Rode rode, Boolean vis){
    	for (int i=0;i<plugin.getRodeTableModel().getEntities().size();i++){
    		RodeTableObject o = (RodeTableObject)plugin.getRodeTableModel().get(i);
    		if (o.getRode().getId().longValue()==rode.getId().longValue()){
    			o.setRode(rode);
    			if (vis!=null)
    				o.setValgt(vis);
    			plugin.getRodeTableModel().fireTableModelEvent(0, 0, TableModelEvent.UPDATE);
    			return;
    		}
    	}
    	RodeTableObject o = new RodeTableObject(rode);
    	plugin.getRodeTableModel().getEntities().add(o);
    	plugin.getRodeTableModel().fireTableModelEvent(0, 0, TableModelEvent.INSERT);
    }
    
    public void resetDriftkontrakt(Driftkontrakt driftkontrakt){
    	Driftkontrakt old = this.driftkontrakt;
    	plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.RODE.getType());
    	this.driftkontrakt = driftkontrakt;
    	props.firePropertyChange("driftkontrakt", old, driftkontrakt);
    	if (rodetypeBinding !=null)
        	rodetypeBinding.unbind();
        rodetypeBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ_WRITE, getRodetyper(driftkontrakt==null?null:driftkontrakt.getId()), plugin.getRodetypeCombo() );
        rodetypeBinding.bind();
    }
    
    private void resetManager(MouseListenerManager mm){
    	mm.removeListener(PanMapTool.class);
    	mm.removeListener(SelectTool.class);
    	mm.removeListener(MoveLayerTool.class);
    	mm.removeListener(ViapunktTool.class);
    	mm.removeListener(HpTool.class);
    	mm.removeListener(SelectionTool.class);
    }
    
    public void setDrawMode(DrawMode drawMode){
    	
    	MouseListenerManager mm = plugin.getMainMap().getMouseListenerManager();
    	resetManager(mm);
    	switch(drawMode){
    		case NONE : 
    			if (!DrawMode.NONE.equals(this.drawMode)){
	    			mm.addListener(0, new PanMapTool(plugin.getMainMap()));
	    			mm.addListener(0, new SelectTool(plugin.getMainMap(), plugin.getRodePanel()));
	    			mm.addListener(0, new MoveLayerTool(plugin.getMainMap()));
	    			plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.RODE.getType(), "temp");
    			}
    			break;
    		case ZOOM:
    			if (!DrawMode.ZOOM.equals(this.drawMode)){
    				mm.addListener(0, new SelectionTool(plugin.getMainMap()));
    			}
    			break;
    		case VIAPUNKT : 
    			if (!DrawMode.VIAPUNKT.equals(this.drawMode)){
    				ViapunktTool lineTool =  new ViapunktTool(plugin);
    				mm.addListener(0, lineTool);
    			}
    			break;
    		case FRA_HP_START : 
    			if (this.drawMode!=DrawMode.FRA_HP_START){
    				HpTool hpTool = new HpTool(plugin, HpTool.HPMode.FRA_HP_START);
    				mm.addListener(0, hpTool);
    			}
    			break;
    		case HEL_HP: 
    			if (this.drawMode!=DrawMode.HEL_HP){
    				HpTool hpTool = new HpTool(plugin, HpTool.HPMode.HEL_HP);
    				mm.addListener(0, hpTool);
    			}
    			break;
    		case TIL_HP_SLUTT: 
    			if (this.drawMode!=DrawMode.TIL_HP_SLUTT){
    				HpTool hpTool = new HpTool(plugin, HpTool.HPMode.TIL_HP_SLUTT);
    				mm.addListener(0, hpTool);
    			}
    			break;
    		case VEI : 
    			if (this.drawMode!=DrawMode.VEI){
    				Cursor veiCursor = Toolkit.getDefaultToolkit().createCustomCursor(IconResources.MAP_PIN_CURSOR.getImage(), new Point(6, 31), "PinCursor");
    		    	plugin.getMainMap().setCursor(veiCursor);
    				mm.addListener(0, new VeiTool(plugin));
    			}
    			break;
    		default : break;
    	}
    	this.drawMode = drawMode;
    }
    /**
     * Setter den valgte driftkontrakten
     * @param driftkontrakt
     */
    public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
        Driftkontrakt old = this.driftkontrakt;

        this.driftkontrakt = driftkontrakt;
        props.firePropertyChange("driftkontrakt", old, driftkontrakt);
        setDriftkontraktNotNull(driftkontrakt!=null);
        
        if (rodetypeBinding !=null)
        	rodetypeBinding.unbind();
        rodetypeBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ_WRITE, getRodetyper(driftkontrakt==null?null:driftkontrakt.getId()), plugin.getRodetypeCombo() );
        rodetypeBinding.bind();
        plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.RODE.getType());
        plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.KONTRAKTVEI.getType());
        visKontraktOmrade();
        
        log.trace("Setter valgt kontrakt");
    }
    
    public void setVisKunAktiveRoder(Boolean aktive){
    	Boolean old = this.visKunAktiveRoder;
    	this.visKunAktiveRoder = aktive;
    	props.firePropertyChange("visKunAktiveRoder", old, aktive);
    }
    
    
    public Boolean getVisKunAktiveRoder(){
    	return visKunAktiveRoder;
    }
    
    /**
     * Setter driftkontraktNotNull
     * 
     * @param driftkontraktNotNull
     */
    public void setDriftkontraktNotNull(boolean driftkontraktNotNull) {
        boolean old = this.driftkontraktNotNull;
        this.driftkontraktNotNull = driftkontraktNotNull;
        props.firePropertyChange("driftkontraktNotNull", old, driftkontraktNotNull);
        log.debug("setting driftkontraktnotnull:"+driftkontraktNotNull);
    }
    
    /**
     * Legger til nye rodestrekninger, passer på at det ikke eksisterer overlapp i veinettreflinkseksjonene
     * @param strekningList
     */
    public void setNyeRodestrekninger(List<Veinettreflinkseksjon> strekningList){
    	List<Veinettreflinkseksjon> old = this.nyeRodestrekninger;
    	if (nyeRodestrekninger==null)
    		nyeRodestrekninger = new ArrayList<Veinettreflinkseksjon>();
    	if (strekningList==null){
    		nyeRodestrekninger = null;
    	}
    	if (nyeRodestrekninger!=null){
    		nyeRodestrekninger.addAll(strekningList);
    		nyeRodestrekninger = mergeStrekninger(nyeRodestrekninger);
    	}
    	props.firePropertyChange("nyeRodestrekninger", old, nyeRodestrekninger);
    }
    
    public void setRode(Rode rode){
    	Rode old = this.rode;
    	this.rode = rode;
    	props.firePropertyChange("rode", old, rode);
    }
    
    public void setRodeFarge(Rode rode, Color rodeFarge){
    	plugin.enforceWriteAccess();
    	rode.getVeinett().setGuiFarge(ColorUtil.toHexString(rodeFarge));
    	mergeRode(rode);
    }
    
    /**
     * Set listen med roder
     * @param roder
     */
    public void setRoder(List<Rode> roder) {
        log.debug("Setter nye roder i rodecontroller: old="+this.roder+" new="+roder);
        List<Rode> old = this.roder;
        this.roder = roder;
        props.firePropertyChange("roder", old, roder);
    }
    
    /**
     * Set den valgte rodetypen
     * @param rodetype
     */
    public void setRodetype(Rodetype rodetype) {
        log.debug("Setter rodetype:"+rodetype);
        Rodetype old = this.rodetype;
        this.rodetype = rodetype;
        props.firePropertyChange("rodetype", old, rodetype);
        
        plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.RODE.getType());
        setRode(null);
        setNyeRodestrekninger(null);
        plugin.getToolbarPanel().activatePanButton();
        this.drawMode=null;
        setDrawMode(DrawMode.NONE);
    }
    
    public void slettRode(Rode rode){
    	plugin.enforceWriteAccess();
    	rodeBean.removeRode(rode);
    	removeRode(rode);
    	setRode(null);
    	plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.RODE.getType(), ""+rode.getId());
    }
    
    public void slettRodetype(Rodetype rodetype){
    	plugin.enforceWriteAccess();
    	rodeBean.removeRodetype(rodetype);
    	resetDriftkontrakt(getDriftkontrakt());
    }
    
    private void sort2(List<Veinettreflinkseksjon> list){
    	Collections.sort(list, new Comparator<Veinettreflinkseksjon>(){
			@Override
			public int compare(Veinettreflinkseksjon o1, Veinettreflinkseksjon o2) {
				long vn1 = o1.getReflinkIdent();
				long vn2 = o2.getReflinkIdent();
				
				double fra1 = o1.getFra();
				double fra2 = o2.getFra();
				
				double til1 = o1.getTil();
				double til2 = o2.getTil();
				
				int result = (vn1==vn2)? 0 : ((vn1<vn2)?-1:1);
				if (result==0){
					result = (fra1==fra2)?0: ((fra1<fra2)?-1:1);
					if (result==0){
						result = (til1==til2)?0:((til1<til2)?-1:1);
					}
				}
				return result;
			}
    	});
    }

    public void visKontraktOmrade(){
		if (driftkontrakt!=null&&driftkontrakt.getAreal()!=null){
			final Bounds b = MapUtilities.convertArealToBounds(driftkontrakt.getAreal());
        	if (plugin.getStatus().equals(MipssPlugin.Status.RUNNING))
        		plugin.getMainMap().ensureVisibilityForBounds(b);
        	else{
        		new Thread(){
        			public void run(){
		        		while (!plugin.getStatus().equals(MipssPlugin.Status.RUNNING)){
		        			Thread.yield();
		        		}
		        		try {
			        		SwingUtilities.invokeAndWait(new Runnable(){
	    						public void run(){
	    							plugin.getMainMap().ensureVisibilityForBounds(b);
	    						}
	    					});
		        		} catch (InvocationTargetException e){
		        			e.printStackTrace();
		        		} catch (InterruptedException e) {
							e.printStackTrace();
						}
		        	}
        		}.start();
        			
        	}
        }
    }

    /**
     * Viser frem kontraktveinett i kart, dersom veinettet ikke er lastet fra før vil det bli hentet fra databasen
     * @param kontrakt
     * @param visible om veinettet skal være synlig eller ikke
     * @param p 
     */
    public void visKontraktveinett(boolean visible, boolean veityper, final VeitypePanel p){
    	if (driftkontrakt!=null){
    		if ((plugin.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.KONTRAKTVEI.getType(), driftkontrakt.getId()+"")==null)){
    			final HentKontraktveinettTask task = new HentKontraktveinettTask(plugin, driftkontrakt, veityper);
    			ProgressWorkerPanel workerPanel = new ProgressWorkerPanel("hent kontraktveinett:"+driftkontrakt.getTextForGUI());
                workerPanel.setWorker(task);
                task.setProgressController(plugin.getProgressController());
                task.setProgressWorkerPanel(workerPanel);
                plugin.getProgressController().addWorkerPanel(workerPanel);
                new Thread("Hent veinett Thread()"){
    	    		public void run(){
		    			try{
		    				Layer l = task.get();
		    				addLayer(l, MipssMapLayerType.KONTRAKTVEI.getType());
		    				plugin.getMainMap().calculateZoomForLayer(l);
		    				if (p!=null){
	                			KontraktveiCollectionLayer kl = (KontraktveiCollectionLayer)l;
	                			kl.showEv(p.isEv());
	                			kl.showRv(p.isRv());
	                			kl.showFv(p.isFv());
	                			kl.showKv(p.isKv());
		    				}
		    			} catch (InterruptedException e1) {
							e1.printStackTrace();
						} catch (ExecutionException e1) {
							e1.printStackTrace();
						}
    	    		}
                }.start();
    		}	
	    	if (visible){
	    		plugin.getMainMap().getLayerHandler().showLayer(MipssMapLayerType.KONTRAKTVEI.getType(), driftkontrakt.getId()+"");
	    	}else{
	    		plugin.getMainMap().getLayerHandler().hideLayer(MipssMapLayerType.KONTRAKTVEI.getType(), driftkontrakt.getId()+"");
	    	}
    	}
    }
    
    public void visRodeRapport(RoderapportType type){
    	final RapportTask task = new RapportTask(plugin, driftkontrakt, type);
    	ProgressWorkerPanel wpanel = new ProgressWorkerPanel("Henter rapport");
    	wpanel.setWorker(task);
    	task.setProgressController(plugin.getProgressController());
    	task.setProgressWorkerPanel(wpanel);
    	plugin.getProgressController().addWorkerPanel(wpanel);
    	new Thread("Henter roderapport"){
    		public void run(){
    			try {
					task.get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
    		}
    	}.start();
    }
    public void updateRodeColor(final Rode rode){
    	Layer layer = plugin.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.RODE.getType(), rode.getId()+"");
    	if (layer!=null){
    		layer.setColor(ColorUtil.fromHexString(rode.getVeinett().getGuiFarge()));
    		plugin.getMainMap().getLayerHandler().changeLayer(layer);
    	}
    }
    /**
     * Viser en rode i kartet, dersom roden ikke er lastet fra før vil den bli hentet fra databasen.
     *  
     * @param rode
     * @param visible om roden skal være synlig eller ikke. 
     */
    public void visRode(final Rode rode, final boolean visible, boolean refresh){
    	if (refresh){
    		plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.RODE.getType(), rode.getId()+"");
    	}
        if ((plugin.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.RODE.getType(), rode.getId()+"")==null)&&visible){
        	final HentRodeveinettTask task = new HentRodeveinettTask(plugin, rode);
        	ProgressWorkerPanel workerPanel = new ProgressWorkerPanel("hent rode:"+rode.getNavn());
            workerPanel.setWorker(task);
            task.setProgressController(plugin.getProgressController());
            task.setProgressWorkerPanel(workerPanel);
            plugin.getProgressController().addWorkerPanel(workerPanel);
            
	    	new Thread("Hent rode Thread()"){
	    		public void run(){
					try {
						Layer l = task.get();
						if (l!=null){
							addLayer(l, MipssMapLayerType.RODE.getType());
							if (visible){
								plugin.getMainMap().getLayerHandler().showLayer(MipssMapLayerType.RODE.getType(), rode.getId()+"");
							}else{
								plugin.getMainMap().getLayerHandler().hideLayer(MipssMapLayerType.RODE.getType(), rode.getId()+"");
							}
						}
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					} catch (ExecutionException e1) {
						e1.printStackTrace();
					}
	    		}
	    	}.start();
	    }else{
			if (visible){
				plugin.getMainMap().getLayerHandler().showLayer(MipssMapLayerType.RODE.getType(), rode.getId()+"");
			}else{
				plugin.getMainMap().getLayerHandler().hideLayer(MipssMapLayerType.RODE.getType(), rode.getId()+"");
				
			}
	    }
    }
    
    /**
     * Henter alle prodtypene og wrapper de i en liste av ProdtypeSelectableObject
     * @return
     */
    public List<ProdtypeSelectableObject> getProdtypeList(){	
    	List<Prodtype> prodtypeList = contractBean.getProdtypeList();
    	
    	List<ProdtypeSelectableObject> prodtypeSelectableObjectList = new ArrayList<ProdtypeSelectableObject>();
    	for(Prodtype prodtype : prodtypeList) {
    		prodtypeSelectableObjectList.add(new ProdtypeSelectableObject(prodtype));
    	}
    	return prodtypeSelectableObjectList;
    }
    /**
     * Henter alle leverandørene som er tilknyttet roden og wrapper disse en en liste
     * av LeverandorSelectableObjects
     * @param rode
     * @return
     */
    public List<LeverandorSelectableObject> getLeverandorForKontraktList(){
    	List<Leverandor> leverandorList = contractBean.getLeverandorListKontrakt(driftkontrakt.getId());
    	List<LeverandorSelectableObject> levList = new ArrayList<LeverandorSelectableObject>();
    	for (Leverandor l:leverandorList){
    		levList.add(new LeverandorSelectableObject(l));
    	}
    	return levList;
    }
    
    public List<LevProdtypeRodeTableObject> getLeverandorForRodeList(){
    	List<LevProdtypeRode> leverandorList = contractBean.getLeverandorList(rode.getId());
    	List<LevProdtypeRodeTableObject> levProdtypeRodeObjectList = new ArrayList<LevProdtypeRodeTableObject>();
    	HashMap<Key, LevProdtypeRodeTableObject> hashMap = new HashMap<Key, LevProdtypeRodeTableObject>();
		for(LevProdtypeRode levProdtypeRode : leverandorList) {
			Key key = new Key(levProdtypeRode.getLeverandorNr(), levProdtypeRode.getRodeId(), levProdtypeRode.getFraDato(), levProdtypeRode.getTilDato());
			if(hashMap.containsKey(key)) {
				hashMap.get(key).getProdtypeList().add(levProdtypeRode.getProdtype());
			} else {
				LevProdtypeRodeTableObject levProdtypeRodeTableObject = new LevProdtypeRodeTableObject();
				levProdtypeRodeTableObject.setLeverandor(levProdtypeRode.getLeverandor());
				levProdtypeRodeTableObject.setRode(levProdtypeRode.getRode());
				levProdtypeRodeTableObject.setFraDato(levProdtypeRode.getFraDato());
				levProdtypeRodeTableObject.setTilDato(levProdtypeRode.getTilDato());
				levProdtypeRodeTableObject.getProdtypeList().add(levProdtypeRode.getProdtype());
				hashMap.put(key, levProdtypeRodeTableObject);
			}
		}
		
		levProdtypeRodeObjectList.addAll(hashMap.values());
		return levProdtypeRodeObjectList;
    }
    public Veinett getNyttVeinett(Rode rode){
		String navn = rode.getNavn();
    	Veinetttype type = new Veinetttype();
    	type.setNavn("Rode");
    	
    	Veinett v = new Veinett();
    	v.getOwnedMipssEntity().setOpprettetAv(plugin.getLoader().getLoggedOnUserSign());
    	v.getOwnedMipssEntity().setOpprettetDato(Clock.now());
    	v.setNavn(navn);
    	v.setGuiFarge("0xffffff");
    	v.setGuiTykkelse(2);
    	v.setVeinetttype(type);
    	return v;
    }
    class Key {
		String leverandorNr;
		Long rodeId;
		Timestamp fraDato;
		Timestamp tilDato;
		
		public Key(String leverandorNr, Long rodeId, Timestamp fraDato, Timestamp tilDato) {
			this.leverandorNr = leverandorNr;
			this.rodeId = rodeId;
			this.fraDato = fraDato;
			this.tilDato = tilDato;
		}
		
		@Override
	    public int hashCode() {
	        return new HashCodeBuilder().append(leverandorNr).append(rodeId).append(fraDato).append(tilDato).toHashCode();
	    }
		
		public boolean equals(Object o) {
	        if(o == null) {return false;}
	        if(o == this) {return true;}
	        if(!(o instanceof Key)) {return false;}
	        Key other = (Key) o;
	        return new EqualsBuilder().append(leverandorNr, other.leverandorNr).append(rodeId, other.rodeId).append(fraDato, other.fraDato).append(tilDato, other.tilDato).isEquals();
	    }
	}
}
