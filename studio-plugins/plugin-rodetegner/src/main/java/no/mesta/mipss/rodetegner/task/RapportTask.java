package no.mesta.mipss.rodetegner.task;

import java.util.List;

import javax.swing.JOptionPane;

import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.ExcelHelper;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.rodeservice.RoderapportType;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

public class RapportTask extends RodegeneratorTask{
	private Driftkontrakt kontrakt;
	private RodetegnerModule plugin;
	
	
		
	private RoderapportType type;
	
	
	public RapportTask(RodetegnerModule plugin, Driftkontrakt kontrakt, RoderapportType type){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.kontrakt = kontrakt;
		this.type = type;
		
	}	
	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
			return null;
		}
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
		Rodegenerator rodeBean = BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class);
		
		openReport(rodeBean.getRapportdata(type, kontrakt.getId()));
		return null;
	}
	private String getReportName(){
		switch(type){
			case HULL_RODER:return "HullRoder";
			case OVERLAPPENDE_RODER: return "OverlappendeRoder";
			case RODER_UTENFOR_KONTRAKT: return "RorderUtenforKontrakt";
		}
		return null;
	}
	private String getMessage(){
		switch(type){
			case HULL_RODER:return "Fant ingen hull";
			case OVERLAPPENDE_RODER: return "Fant ingen overlappende roder";
			case RODER_UTENFOR_KONTRAKT: return "Fant ingen roder utenfor kontrakt";
		}
		return "";
	}
	private void openReport(List<List<String>> data){
		
		if (data.size()==1||data.size()==0){
			JOptionPane.showMessageDialog(plugin.getModuleGUI(), getMessage(), "Fant ingen feil", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		List<String> header = data.get(0);
		ExcelHelper excel = new ExcelHelper();
		
		WritableWorkbook workbook = excel.createNewWorkbook(getReportName());
		WritableSheet rodeSheet = excel.createNewSheet("rodeinfo", workbook, 0);
		String[] headerColumns = header.toArray(new String[]{});
		excel.writeHeaders(rodeSheet, 0, 0, true, headerColumns);
		
		int row = 2;
		for (int i=1;i<data.size();i++){
			List<String> d = data.get(i);
			int col = 0;
			for (String s:d){
				excel.writeData(rodeSheet, row, col++, s);
			}
			row++;
		}
		excel.closeWorkbook(workbook);
		DesktopHelper.openFile(excel.getExcelFile());
	}	
}
