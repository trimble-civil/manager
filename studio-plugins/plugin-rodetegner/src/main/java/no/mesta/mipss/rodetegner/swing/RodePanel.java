package no.mesta.mipss.rodetegner.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.rode.NyRodeDialog;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.table.ColorEditor;
import no.mesta.mipss.ui.table.ColorRenderer;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.ui.table.RodeTableObject;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.error.ErrorInfo;

/**
 * Panel for å vise, legge til, slette roder
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
@SuppressWarnings("serial")
public class RodePanel extends AbstractRodePanel{
    private RodetegnerModule plugin;
    private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
    
	private MipssRenderableEntityTableModel<RodeTableObject> rodeTableModel;
	private JXTable rodeTable;
    private JCheckBox showAllCheck;
    private JCheckBox kunAktiveCheck;
    
    private JCheckTablePanel checkTable;
	private JMenu endreRodetypeMenu;
    public RodePanel(final RodetegnerModule plugin) {
        this.plugin = plugin;
        setBorder(BorderFactory.createTitledBorder("Roder"));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        JButton propertiesButton = new JButton(new PropertiesAction("Egenskaper", IconResources.EDIT_ICON));
        JButton deleteButton = new JButton(new DeleteRodeAction("Slett", IconResources.BUTTON_CANCEL_ICON));
        JButton newButton = new JButton(new NewRodeAction("Ny", IconResources.NEW_ICON));
        
        propertiesButton.setToolTipText(resources.getGuiString("rodepanel.properties.tooltip"));
        deleteButton.setToolTipText(resources.getGuiString("rodepanel.delete.tooltip"));
        newButton.setToolTipText(resources.getGuiString("rodepanel.new.tooltip"));
        
        showAllCheck = new JCheckBox("Vis alle i kart");
        showAllCheck.setToolTipText(resources.getGuiString("rodepanel.vis.tooltip"));
        showAllCheck.setEnabled(false);
        
        kunAktiveCheck = new JCheckBox("Vis kun aktive roder");
        kunAktiveCheck.setToolTipText(resources.getGuiString("rodepanel.aktive.tooltip"));
        kunAktiveCheck.setSelected(true);
        kunAktiveCheck.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				setRoder(plugin.getRodeController().getRodetype());
				plugin.getRodeController().setVisKunAktiveRoder(kunAktiveCheck.isSelected());
				for (RodeTableObject r:rodeTableModel.getEntities()){
					plugin.getRodeController().visRode(r.getRode(), false, false);
				}
			}
		});
        
        setRoderButtonSize(propertiesButton);
        setButtonSize(deleteButton);
        setButtonSize(newButton);
        
        checkTable = createRodeTable();
       
        JPanel rodePanel = new JPanel(new BorderLayout());
        rodePanel.add(checkTable);
        rodePanel.setPreferredSize(new Dimension(0, 200));
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(propertiesButton);
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(deleteButton);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(newButton);
        setButtonPanelSize(buttonPanel);
        JPanel checkPanel = new JPanel();
        checkPanel.setLayout(new BoxLayout(checkPanel, BoxLayout.LINE_AXIS));
        checkPanel.add(showAllCheck);
        checkPanel.add(Box.createHorizontalGlue());
        checkPanel.add(kunAktiveCheck);
        
        add(rodePanel);
        add(Box.createVerticalStrut(5));
        add(checkPanel);
        add(buttonPanel);
        
        Binding getRoder = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), BeanProperty.create("rodetype"), this, BeanProperty.create("roder"));
        Binding deleteActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rode != null}"), deleteButton, BeanProperty.create("enabled"));
        Binding newActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rodetype != null}"), newButton, BeanProperty.create("enabled"));        
        Binding propertiesActivate = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${rode != null}"), propertiesButton, BeanProperty.create("enabled"));
        
        plugin.getFieldToolkit().addBinding(getRoder);
        plugin.getFieldToolkit().addBinding(deleteActivate);
        plugin.getFieldToolkit().addBinding(newActivate);
        plugin.getFieldToolkit().addBinding(propertiesActivate);
        
        if (UserUtils.isAdmin(plugin.getLoader().getLoggedOnUser(true))){
        	addPopupListener();
        }
    }
    
    private void addPopupListener(){
    	JPopupMenu menu = new JPopupMenu();
    	
    	endreRodetypeMenu = new JMenu("Endre rodetype for synlige");
    	menu.add(endreRodetypeMenu);
    	
    	MouseListener listener = new PopupListener(menu);
    	rodeTable.addMouseListener(listener);
    }
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	plugin = null;
    }
    private JCheckTablePanel createRodeTable(){
    	String[] columns = new String[]{"valgt", "guiFarge", "navn", "rodeLengde"};
		
		rodeTableModel = plugin.getRodeTableModel();
		rodeTableModel.addTableModelListener(new TableModelListener(){
			
			@Override
			public void tableChanged(TableModelEvent e) {
				if (e.getType()==TableModelEvent.UPDATE){
					int row = e.getFirstRow();
					if (e.getColumn()==0){
						if (row<rodeTableModel.getSize()){
							RodeTableObject o = rodeTableModel.get(row);
							if (o.getRode().getVeinett()!=null){
								plugin.getRodeController().visRode(o.getRode(), o.getValgt(), false);
							}else{
							}
						}
					}
					if (e.getColumn()==1){
						if (row<rodeTableModel.getSize()){
							RodeTableObject o = rodeTableModel.get(row);
							if (o.getRode().getVeinett()!=null){
								try{
									plugin.getRodeController().setRodeFarge(o.getRode(), o.getGuiFarge());
									plugin.getRodeController().updateRodeColor(o.getRode());
								} catch (Exception ex){}
							}else{
							}
						}
					}
				}
			}
		});
		showAllCheck.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean show = showAllCheck.isSelected();
				for (RodeTableObject r:rodeTableModel.getEntities()){
					plugin.getRodeController().visRode(r.getRode(), show, false);
				}
			}
			
		});
		MipssRenderableEntityTableColumnModel rodeColumnModel = new MipssRenderableEntityTableColumnModel(RodeTableObject.class, columns);
		rodeTable = new JXTable(rodeTableModel,rodeColumnModel);
		rodeTable.setDefaultRenderer(Color.class, new ColorRenderer(true));
        rodeTable.setDefaultEditor(Color.class, new ColorEditor());
		rodeTable.getColumnModel().getColumn(0).setMaxWidth(25);
		rodeTable.getColumnModel().getColumn(1).setMaxWidth(35);
		rodeTable.getColumnModel().getColumn(3).setMaxWidth(85);
		rodeTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		rodeTable.getSelectionModel().setAnchorSelectionIndex(0);
		rodeTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent e) {
				
					int index = rodeTable.getSelectedRow();
					if (index<0){
						plugin.getRodeController().setRode(null);
					}else{
	                    int modelRow =rodeTable.convertRowIndexToModel(index);
	                    if (modelRow<rodeTableModel.getSize()){
							RodeTableObject o = rodeTableModel.get(modelRow);
							plugin.getRodeController().setRode(o.getRode());
	                    }
					}
				
			}
		});
		
		rodeTable.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if (e.getClickCount()==2){
					new PropertiesAction(null,null).actionPerformed(null);
				}
			}
		});
		return new JCheckTablePanel<RodeTableObject>(rodeTable, rodeTableModel, showAllCheck, "Henter roder", false);
    }
    
    public void selectRow(String rodeId){
    	try{
	    	Long id = Long.valueOf(rodeId);
	    	List<RodeTableObject> roder = rodeTableModel.getEntities();
	    	for (int i=0;i<roder.size();i++){
	    		RodeTableObject o = roder.get(i);
	    		if (o.getRode().getId().equals(id)){
	    			int index = rodeTable.convertRowIndexToView(i);
	    			rodeTable.getSelectionModel().setLeadSelectionIndex(index);
	    			break;
	    		}	
	    	}
    	} catch (Exception e){
    		
    	}
    }
    
    public void setRoder(final Rodetype rodetype){
    	if (rodetype!=null){
    		new Thread(){
    			public void run(){
    				synchronized(RodePanel.this){
				    	checkTable.setBusy(true);
				    	rodeTableModel.getEntities().clear();
				    	rodeTable.resetSortOrder();
				    	rodeTable.getSelectionModel().clearSelection();
				    	Long kontraktId = plugin.getRodeController().getDriftkontrakt().getId();
				    	Long rodetypeId = rodetype.getId();
				    	
				    	Rodegenerator bean= BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class);
				    	List<Rode> rodeForKontrakt = bean.getRodeForKontrakt(kontraktId, rodetypeId, Boolean.TRUE, Boolean.valueOf(kunAktiveCheck.isSelected()));
				    	List<RodeTableObject> roder = new ArrayList<RodeTableObject>();
				    	for (Rode r :rodeForKontrakt){
				    		RodeTableObject rt = new RodeTableObject(r);
				    		if (showAllCheck.isSelected()){
				    			plugin.getRodeController().visRode(rt.getRode(), true, false);
				    			rt.setValgt(Boolean.TRUE);
				    		}
				    		roder.add(rt);
				    	}
				    	plugin.getRodeController().setRoder(rodeForKontrakt);
				    	rodeTableModel.getEntities().addAll(roder);
				    	checkTable.setBusy(false);
				    	showAllCheck.setEnabled(rodeTableModel.getEntities().size()>0);
    				}
    			}
    		}.start();
    	}
    	else{
    		rodeTableModel.getEntities().clear();
    		showAllCheck.setEnabled(false);
    	}
    }
    
    public int getVerticalStrut() {
        return 0;
    }


    /**
     * Actionklasse for å slette en rode
     */
    class DeleteRodeAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public DeleteRodeAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	if (plugin.driftkontraktmoduleHarUlagredeEndringer()){
				JOptionPane.showMessageDialog(plugin.getLoader().getApplicationFrame(), resources.getGuiString("driftkontraktmoduleRunning.message2"), resources.getGuiString("driftkontraktmoduleRunning.title"), JOptionPane.WARNING_MESSAGE);
				return;
			}
        	
            int index = rodeTable.getSelectedRow();
            index = rodeTable.convertRowIndexToModel(index);
        	RodeTableObject o = rodeTableModel.get(index);
        	String r = o.getRode().getNavn();
        	
        	MipssDialogue dialog = new MipssDialogue(
        			plugin.getLoader().getApplicationFrame(), 
        			IconResources.QUESTION_ICON, 
        			resources.getGuiString("rodepanel.delete.confirm.title"), 
        			resources.getGuiString("rodepanel.delete.confirm.message")+": \""+r+"\" ?", 
        			MipssDialogue.Type.QUESTION,
					new MipssDialogue.Button[] { MipssDialogue.Button.YES, MipssDialogue.Button.NO });
        	dialog.askUser();
        	
        	if (dialog.getPressedButton() == MipssDialogue.Button.YES) {
        		try{
		        	plugin.getRodeController().slettRode(o.getRode());
		        	rodeTableModel.removeItem(o);
		        	plugin.sendRefreshMessageToDriftkontraktmodule();
        		} catch (Exception ex){
        			ErrorInfo info = new ErrorInfo("Feil ved sletting av rode", "Det oppstod en feil ved sletting av rode, roden er ikke slettet", null, "Advarsel", ex, Level.SEVERE, null);
        	        JXErrorPane.showDialog(plugin.getModuleGUI(), info);
        		}
			}
        }
    }
    /**
     * Actionklasse for å opprette en ny rode
     */
    class NewRodeAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public NewRodeAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
		public void actionPerformed(ActionEvent e) {
			if (plugin.driftkontraktmoduleHarUlagredeEndringer()){
				JOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(plugin.getModuleGUI()), resources.getGuiString("driftkontraktmoduleRunning.message"), resources.getGuiString("driftkontraktmoduleRunning.title"), JOptionPane.WARNING_MESSAGE);
				return;
			}
	
	    	if (plugin.getRodeController().getRodetype().getBareEnRodeFlagg()&&plugin.getRodeController().getRoder().size()>0){
	    		JOptionPane.showMessageDialog(plugin.getLoader().getApplicationFrame(), resources.getGuiString("rodepanel.tooManyRoder.message"), resources.getGuiString("rodepanel.tooManyRoder.title"), JOptionPane.WARNING_MESSAGE);
	    	}else{
	            NyRodeDialog dialog = new NyRodeDialog(plugin);
	            dialog.pack();
	            dialog.setLocationRelativeTo(null);
	            dialog.setVisible(true);
	    	}

			
        }
    }
    class PropertiesAction extends AbstractAction{
    	public PropertiesAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	if(plugin.getRodeController().getRode()==null){
        		JOptionPane.showMessageDialog(plugin.getLoader().getApplicationFrame(), resources.getGuiString("rodepanel.ikkevalgt.message"), resources.getGuiString("rodepanel.ikkevalgt.title"), JOptionPane.WARNING_MESSAGE);
        	}else{
	        	NyRodeDialog dialog = new NyRodeDialog(plugin, plugin.getRodeController().getRode());
	            dialog.pack();
	            dialog.setLocationRelativeTo(null);
	            dialog.setVisible(true);
        	}
        }
    }
    
    class PopupListener extends MouseAdapter {
        JPopupMenu popup;
 
        PopupListener(JPopupMenu popupMenu) {
            popup = popupMenu;
        }
 
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }
 
        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }
        
        private void maybeShowPopup(MouseEvent e) {
        	if (e.isPopupTrigger()&&e.getButton()==MouseEvent.BUTTON3) {
        		endreRodetypeMenu.removeAll();
                List<Rodetype> rodetyper = plugin.getRodeController().getRodetyper();
            	for (final Rodetype rt:rodetyper){
            		JMenuItem i = new JMenuItem(rt.getNavn());
            		
            		i.addActionListener(new ActionListener(){
            			private Rodetype nyRodetype = rt;
            			@Override
            			public void actionPerformed(ActionEvent e) {
            				List<RodeTableObject> alleValgte = checkTable.getAlleValgte();            
            				List<Rode> rodeList = new ArrayList<Rode>();
            				for (RodeTableObject rto:alleValgte){
            					rodeList.add(rto.getRode());
            				}
            				int v = JOptionPane.showConfirmDialog(plugin.getModuleGUI(), resources.getGuiString("message.endreRodetype.message", new Object[]{alleValgte.size(), nyRodetype.getNavn()}), "", JOptionPane.YES_NO_OPTION);
            				if (v==JOptionPane.YES_OPTION){
            					plugin.getRodeController().endreRodetype(rodeList, rt);
            					setRoder(plugin.getRodeController().getRodetype());
            				}
            			}
                	});
            		endreRodetypeMenu.add(i);
            	}
            	popup.show(e.getComponent(), e.getX(), e.getY());
            }
            
        }
    }
}
