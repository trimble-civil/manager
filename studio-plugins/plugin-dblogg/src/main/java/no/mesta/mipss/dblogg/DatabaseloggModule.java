package no.mesta.mipss.dblogg;

import javax.swing.JComponent;

import no.mesta.mipss.plugin.MipssPlugin;

@SuppressWarnings("serial")
public class DatabaseloggModule extends MipssPlugin{
	private DatabaseloggController controller;
	private DatabaseloggPanel databaseloggPanel;
	
	@Override
	public JComponent getModuleGUI() {
		return databaseloggPanel;
	}

	@Override
	protected void initPluginGUI() {
		controller = new DatabaseloggController(this);
		databaseloggPanel = new DatabaseloggPanel(getController());
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

	public DatabaseloggController getController() {
		return controller;
	}

}
