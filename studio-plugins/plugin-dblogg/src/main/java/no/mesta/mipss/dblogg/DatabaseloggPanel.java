package no.mesta.mipss.dblogg;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.jobbinfo.Feilmelding;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class DatabaseloggPanel extends JPanel{
	private DatabaseloggController controller;
	private static PropertyResourceBundleUtil guiProps;

	private JMipssBeanTable<Feilmelding> tblSakerList;
	private JMipssBeanScrollPane scrSakerList;
	
	private MipssBeanTableModel<Feilmelding> mdlSaker;
	private MipssRenderableEntityTableColumnModel tblmdlSakerColumnModel;
	
	private JPanel pnlTopPanel = new JPanel();
	private JPanel pnlSidebuttons = new JPanel();

	
	private JButton btnBehandletstatus = new JButton();
	private JButton btnBehandleMelding = new JButton();
	
	private Boolean behandletStatus;
	
	//Viser data om klikket feilmelding
	private JPanel pnlFeilmeldingVisning = new JPanel();
	private JTextField txtId = new JTextField();
	private JLabel lblId = new JLabel();
	private JTextField txtTypeId = new JTextField();
	private JLabel lblTypeId = new JLabel();
	private JTextField txtFeilmeldingstype = new JTextField();
	private JLabel lblFeilmeldingstype = new JLabel();
	private JTextField txtMelding = new JTextField();
	private JLabel lblMelding = new JLabel();
	private JTextField txtBehandletAv = new JTextField();
	private JLabel lblBehandletAv = new JLabel();
	private JTextField txtKommentar = new JTextField();
	private JLabel lblKommentar = new JLabel();
	private JTextField txtOpprettetDato = new JTextField();
	private JLabel lblOpprettetDato = new JLabel();


	private BindingGroup bg = new BindingGroup();
	

	public DatabaseloggPanel(DatabaseloggController controller) {
		this.controller = controller;
		behandletStatus = new Boolean(Boolean.FALSE);
		initComponents();
		initButtons();
		initGui();
		bind();
	}
	
	/**
	 * Visningsfeltene read-only, dimensjoner.
	 */
	private void initReadOnlyTextfields() {
		lblId.setText(getGuiProps().getGuiString("label.lblId"));
		lblTypeId.setText(getGuiProps().getGuiString("label.lblTypeId"));
		lblFeilmeldingstype.setText(getGuiProps().getGuiString("label.lblFeilmeldingstype"));
		lblMelding.setText(getGuiProps().getGuiString("label.lblMelding"));
		lblBehandletAv.setText(getGuiProps().getGuiString("label.lblBehandletAv"));
		lblKommentar.setText(getGuiProps().getGuiString("label.lblKommentar"));
		lblOpprettetDato.setText(getGuiProps().getGuiString("label.lblOpprettetDato"));
		
		txtId.setText("");
		txtTypeId.setText("");
		txtFeilmeldingstype.setText("");
		txtMelding.setText("");
		txtBehandletAv.setText("");
		txtKommentar.setText("");
		txtOpprettetDato.setText("");

		
		txtId.setEditable(false);
		txtTypeId.setEditable(false);
		txtFeilmeldingstype.setEditable(false);
		txtMelding.setEditable(false);
		txtBehandletAv.setEditable(false);
		txtKommentar.setEditable(true);
		txtOpprettetDato.setEditable(false);
		
		txtId.setMinimumSize(new Dimension(30, 21));
		txtTypeId.setMinimumSize(new Dimension(30, 21));
		txtFeilmeldingstype.setMinimumSize(new Dimension(30, 21));
		txtMelding.setMinimumSize(new Dimension(180, 21));
		txtBehandletAv.setMinimumSize(new Dimension(40, 21));
		txtKommentar.setMinimumSize(new Dimension(180, 21));
		txtOpprettetDato.setMinimumSize(new Dimension(30, 21));
	}


	/**
	 * initcomponent der man initialiserer gui komponentene som skal benyttes modell/tabell
	 */
	private void initComponents() {
		mdlSaker = new MipssBeanTableModel<Feilmelding>(Feilmelding.class, "opprettetDato", "${feilmeldingstype.navn}","kommentar", "melding");
		mdlSaker.setBeanInstance(controller);

		//Definerer metode for å hente ut meldinger via controlleren
		mdlSaker.setBeanMethod("hentMeldinger");
		mdlSaker.setBeanInterface(DatabaseloggController.class);
		mdlSaker.setBeanMethodArguments(new Class[]{Boolean.class});
		mdlSaker.setBeanMethodArgValues(new Object[]{behandletStatus});

		tblmdlSakerColumnModel = new MipssRenderableEntityTableColumnModel(Feilmelding.class, mdlSaker.getColumns());
		
		tblSakerList = new JMipssBeanTable<Feilmelding>(mdlSaker,tblmdlSakerColumnModel);

		
		//Intern layout til feilmeldingstabellen.
		tblSakerList.setDoWidthHacks(true);
		tblSakerList.setFillsViewportWidth(true);
		tblSakerList.addColumnStateSupport("ubehandledeSakerTable", controller.getPlugin());
		tblSakerList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblSakerList.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			if(e.getClickCount() >= 2) {
				//visSelectedOrdre();
				}
			}
		});
		
		//Individuelle kollonnebredder
		tblmdlSakerColumnModel.getColumn(0).setPreferredWidth(25);
		tblmdlSakerColumnModel.getColumn(1).setPreferredWidth(80);
		tblmdlSakerColumnModel.getColumn(2).setPreferredWidth(240);
		tblmdlSakerColumnModel.getColumn(3).setPreferredWidth(230);
		
		//Cellrenderer
		tblmdlSakerColumnModel.getColumn(0).setCellRenderer(new DateTimeRenderer(MipssDateFormatter.LONG_DATE_TIME_FORMAT));

		scrSakerList = new JMipssBeanScrollPane(tblSakerList, mdlSaker);
		mdlSaker.loadData();
	}
	
	private void initButtons() {
		btnBehandletstatus.setAction(getVisBehandletUbehandletAction());
		btnBehandleMelding.setAction(getbehandleMeldingAction());
		btnBehandleMelding.setEnabled(false);
	}



	private void initGui(){
		initReadOnlyTextfields();
		setLayout(new GridBagLayout());
		pnlTopPanel.setLayout(new GridBagLayout());
		pnlSidebuttons.setLayout(new GridBagLayout());
		pnlFeilmeldingVisning.setLayout(new GridBagLayout());
		pnlFeilmeldingVisning.setBorder(BorderResources.createComponentBorder());

		pnlFeilmeldingVisning.add(lblOpprettetDato,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0) );
		pnlFeilmeldingVisning.add(txtOpprettetDato,   new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 670), 0, 0) );

		pnlFeilmeldingVisning.add(lblBehandletAv, 	  new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0) );
		pnlFeilmeldingVisning.add(txtBehandletAv, 	  new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 670), 0, 0) );
		
		pnlFeilmeldingVisning.add(lblId, 			  new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0) );
		pnlFeilmeldingVisning.add(txtId, 			  new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 670), 0, 0) );

		pnlFeilmeldingVisning.add(lblTypeId, 		  new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0) );
		pnlFeilmeldingVisning.add(txtTypeId, 		  new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 670), 0, 0) );

		pnlFeilmeldingVisning.add(lblFeilmeldingstype,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0) );
		pnlFeilmeldingVisning.add(txtFeilmeldingstype,new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0) );

		pnlFeilmeldingVisning.add(lblMelding, 		  new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0) );
		pnlFeilmeldingVisning.add(txtMelding, 		  new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0) );
		
		pnlFeilmeldingVisning.add(lblKommentar, 	  new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0) );
		pnlFeilmeldingVisning.add(txtKommentar, 	  new GridBagConstraints(1, 6, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0) );
		
		
		
		pnlTopPanel.add(btnBehandletstatus, new GridBagConstraints(0, 0, 1, 1, 1, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));	
		pnlSidebuttons.add(btnBehandleMelding, new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 11, 0), 0, 0));
		
		add(pnlTopPanel, 			new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, 	new Insets(5, 0, 0, 0), 0, 0));
		add(scrSakerList,			new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, 	new Insets(5, 0, 0, 0), 0, 0));
		add(pnlSidebuttons, 		new GridBagConstraints(1, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
		add(pnlFeilmeldingVisning,	new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, 	new Insets(5, 0, 0, 0), 0, 0));


	}
	
	private Action getbehandleMeldingAction() {
		return new AbstractAction(getGuiProps().getGuiString("button.settBehandlet"), IconResources.EDIT_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblSakerList.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblSakerList.getRowCount()) {
					Feilmelding feilmelding = mdlSaker.get(tblSakerList.convertRowIndexToModel(selectedRow));
					if(feilmelding != null) {
						controller.setMeldingBehandlet(feilmelding, !behandletStatus, txtKommentar.getText());
					}
				} 
				mdlSaker.loadData();
				initReadOnlyTextfields();
			}	
		};
	}

	
	public Action getVisBehandletUbehandletAction(){
    	return new AbstractAction(getGuiProps().getGuiString("button.behandletStatus"), IconResources.RED_LIGHT) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(behandletStatus){
					behandletStatus = new Boolean(Boolean.FALSE);
					btnBehandletstatus.setText(getGuiProps().getGuiString("button.behandletStatus"));
					btnBehandleMelding.setText(getGuiProps().getGuiString("button.settBehandlet"));
					btnBehandletstatus.setIcon(IconResources.RED_LIGHT);
				}else{
					behandletStatus = new Boolean(Boolean.TRUE);
					btnBehandletstatus.setText(getGuiProps().getGuiString("button.ubehandletStatus"));
					btnBehandleMelding.setText(getGuiProps().getGuiString("button.settuBehandlet"));
					btnBehandletstatus.setIcon(IconResources.GREEN_LIGHT);
				}
				
				mdlSaker.setBeanMethodArgValues(new Object[]{behandletStatus});
				mdlSaker.loadData();
			}
    	};
    }
	

	private void bind(){
		Binding<JMipssBeanTable<Feilmelding>, Object, JButton, Object> tblSelectedBinding= BindingHelper.createbinding(tblSakerList, "${noOfSelectedRows>0}", btnBehandleMelding, "enabled");
		Binding<JMipssBeanTable<Feilmelding>, Object, DatabaseloggPanel, Object> txtVisningsData = BindingHelper.createbinding(tblSakerList, "selectedEntities", this, "visningsData");
		bg.addBinding(tblSelectedBinding);
		bg.addBinding(txtVisningsData);
		bg.bind();
	}
	
	public void setVisningsData(List<Feilmelding> valgtListe){
		if(valgtListe==null || valgtListe.size()==0){
			txtId.setText("");
			txtTypeId.setText("");
			txtFeilmeldingstype.setText("");
			txtMelding.setText("");
			txtBehandletAv.setText("");
			txtKommentar.setText("");
			txtOpprettetDato.setText("");
			return;
		}
		Feilmelding f = valgtListe.get(0);
		txtId.setText(f.getId().toString());
		txtTypeId.setText(f.getTypeId().toString());
		txtFeilmeldingstype.setText(f.getFeilmeldingstype().toString());
		txtMelding.setText(f.getMelding());
		txtBehandletAv.setText(f.getBehandletAv());
		txtKommentar.setText(f.getKommentar());
		txtOpprettetDato.setText(MipssDateFormatter.formatDate(f.getOpprettetDato(),MipssDateFormatter.DATE_FORMAT));
	}


   public static PropertyResourceBundleUtil getGuiProps() {
        if(guiProps == null) {
            guiProps = PropertyResourceBundleUtil.getPropertyBundle("dbloggText");
        }    
        return guiProps;
    }

}
