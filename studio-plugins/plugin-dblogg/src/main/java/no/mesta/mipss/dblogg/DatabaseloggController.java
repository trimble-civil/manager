package no.mesta.mipss.dblogg;

import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.jobbinfo.JobbinfoService;
import no.mesta.mipss.persistence.jobbinfo.Feilmelding;

@SuppressWarnings("serial")
public class DatabaseloggController {
	
	private JobbinfoService jobbInfo;
	private DatabaseloggModule plugin;

	public DatabaseloggController(DatabaseloggModule plugin){
		this.plugin = plugin;
		jobbInfo = BeanUtil.lookup(JobbinfoService.BEAN_NAME, JobbinfoService.class);
	}


	public List<Feilmelding> hentMeldinger(Boolean behandlet){
		if(behandlet){
			return jobbInfo.getBehandledeMeldinger();
		}else{
			return jobbInfo.getUbehandledeMeldinger();	
		}
	}
	
	public void setMeldingBehandlet(Feilmelding feilmelding, boolean b, String kommentar) {
		feilmelding.setBehandletFlagg(new Boolean(b));
		if(b){
			feilmelding.setBehandletAv(plugin.getLoader().getLoggedOnUserSign());
			feilmelding.setKommentar(kommentar);
			feilmelding.setBehandletDato(new Date());
		}else{
			feilmelding.setBehandletAv("");
		}
		jobbInfo.updateFeilmelding(feilmelding);
	}
	
	
	public List<Feilmelding> hentUbehandledeMeldinger(){
		return jobbInfo.getUbehandledeMeldinger();
	}
	
	public List<Feilmelding> hentBehandledeMeldinger(){
		return jobbInfo.getBehandledeMeldinger();
	}

	public DatabaseloggModule getPlugin() {
		return plugin;
	}
}
