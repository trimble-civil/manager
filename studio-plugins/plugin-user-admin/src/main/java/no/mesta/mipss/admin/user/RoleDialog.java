package no.mesta.mipss.admin.user;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;

import no.mesta.mipss.admin.user.ui.ModuleAccessTableModel;
import no.mesta.mipss.core.FieldListener;
import no.mesta.mipss.core.IBeanFieldChange;
import no.mesta.mipss.core.MipssBeanChangeRegister;
import no.mesta.mipss.core.MipssBeanListFieldListener;
import no.mesta.mipss.core.MipssBeanTableListener;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.persistence.tilgangskontroll.RolleBruker;
import no.mesta.mipss.persistence.tilgangskontroll.RolleTilgang;
import no.mesta.mipss.persistence.tilgangskontroll.Tilgangsnivaa;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.selectlist.MipssSelectionList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXTable;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class RoleDialog extends JFrame implements IBeanFieldChange {
	private ImageIcon abortIcon = IconResources.STOP_PROCESS_ICON;
	private final List<Tilgangsnivaa> availableLevels;
	private final List<Modul> availableModules;
	private final List<Bruker> availableUsers;
	private JButton cancelButton;
	private ImageIcon cancelIcon = IconResources.CANCEL_ICON;
	private final UserAdminController<Rolle> controller;
	private JTextArea descriptionField;
	private boolean isNew;
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private JButton okButton;
	private ImageIcon okIcon = IconResources.SAVE_ICON;
	private MipssBeanChangeRegister registeredBeans;
	private Rolle role;
	private JTextField roleField;
	private ModuleAccessTableModel<RolleTilgang> roleModuleAccessModel;
	private List<Bruker> originalUsers = new ArrayList<Bruker>();
	private Boolean userAbort = true;
	private MipssSelectionList<Bruker> usersList;
	private final MipssBeanTableModel<Rolle> roleModel;

	public RoleDialog(UserAdminController<Rolle> controller, Rolle role, MipssBeanTableModel<Rolle> roleModel) {
		this.roleModel = roleModel;
		this.availableModules = controller.getTilgjengeligeModuler();
		this.availableLevels = controller.getTilgjengeligeNivaaer();
		this.availableUsers = controller.getTilgjengeligeBrukere();
		this.controller = controller;

		if (role == null) {
			this.role = new Rolle();
			this.role.setOpprettetDato(Clock.now());
			this.role.setOpprettetAv(controller.getAdmin().getSignatur());

			isNew = true;
		} else {
			this.role = role;
			isNew = false;
			for (RolleBruker rb : role.getRolleBrukerList()) {
				originalUsers.add(rb.getBruker());
			}
		}

		registeredBeans = new MipssBeanChangeRegister(new String[] { "Navn", "Beskrivelse", "Brukere", "Tilganger" });

		try {
			initGUI();
		} catch (Exception e) {
			log.debug("RoleDialog(), e: " + e);
		}
	}

	private Rolle buildRolle() {
		if (isNew) {
			role.setOpprettetAv(controller.getAdmin().getSignatur());
			role.setOpprettetDato(Clock.now());
		}

		oppdaterTilganger();
		oppdaterRolleBrukere();

		return role;
	}

	private void changeCancelButton(boolean close) {
		if (close) {
			cancelButton.setText("Lukk");
			cancelButton.setIcon(cancelIcon);
		} else {
			cancelButton.setText("Avbryt");
			cancelButton.setIcon(abortIcon);
		}
	}

	private void enableSaveButton(boolean enabled) {
		okButton.setEnabled(enabled);
	}

	/**
	 * Returnerer rollen ved lagring når det lages en ny. Ellers null.
	 * Returnerer også rollen når det er gjort endringer ved visning. Ellers
	 * null.
	 * 
	 * @return
	 */
	public Rolle getRole() {
		log.debug("rolle: " + role + ", model: " + roleModuleAccessModel);

		Rolle rolle = null;
		if (isNew && !userAbort) {
			rolle = buildRolle();
		} else if (!userAbort) {
			if (registeredBeans.isChanged()) {
				rolle = buildRolle();
			}
		}

		return rolle;
	}

	private void initGUI() throws Exception {
		log.debug("initGUI() - start");

		String title = (isNew ? "Lag ny rolle" : "Vis/Endre " + role.getNavn());
		setTitle(title);
		setIconImage(IconResources.ROLES_ICON.getImage());

		Dimension minPanelSize = new Dimension(100, 80);
		Dimension prefPanelSize = new Dimension(400, 300);
		Dimension maxPanelSize = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);

		// Lag inputfelter for navn og beskrivelse
		roleField = new JTextField(role.getNavn());
		roleField.getDocument().addDocumentListener(
				new FieldListener<Rolle>(role, roleField.getText(), roleField, "Navn", this));
		ComponentSizeResources.setComponentSizes(roleField, new Dimension(30, 23), new Dimension(120, 23),
				new Dimension(Short.MAX_VALUE, 23));
		descriptionField = new JTextArea(role.getBeskrivelse());
		descriptionField.getDocument().addDocumentListener(
				new FieldListener<Rolle>(role, descriptionField.getText(), descriptionField, "Beskrivelse", this));
		JScrollPane descriptionPane = new JScrollPane(descriptionField);
		ComponentSizeResources.setComponentSizes(descriptionPane, minPanelSize, prefPanelSize, maxPanelSize);
		JXLabel roleLabel = new JXLabel("Rollenavn");
		roleLabel.setHorizontalAlignment(JXLabel.RIGHT);
		ComponentSizeResources.setComponentSize(roleLabel, new Dimension(50, 23));

		// Lag toppanel for rolle navn
		Box topPanel = BoxUtil.createHorizontalBox(0, roleLabel, roleField);

		// Sett opp picklist for brukere
		usersList = new MipssSelectionList<Bruker>(availableUsers, originalUsers, "Velg brukere", Bruker.class);
		usersList.addListSelectionListener((ListSelectionListener) new MipssBeanListFieldListener<Bruker>(
				usersList.getSelectedValues(), "Brukere", this));
		JScrollPane userScrollPane = new JScrollPane(usersList);
		ComponentSizeResources.setComponentSizes(userScrollPane, minPanelSize, prefPanelSize, maxPanelSize);

		// Setter opp modultilgang for bruker
		roleModuleAccessModel = new ModuleAccessTableModel<RolleTilgang>(Rolle.class, role, availableModules,
				availableLevels);
		JXTable mladenTable = new JXTable(roleModuleAccessModel);
		mladenTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane mladenScroller = new JScrollPane(mladenTable);
		mladenScroller.setBorder(new TitledBorder("Rolle tilganger"));
		roleModuleAccessModel.addTableModelListener((TableModelListener) new MipssBeanTableListener(
				roleModuleAccessModel, "Tilganger", this));
		ComponentSizeResources.setComponentSizes(mladenScroller, minPanelSize, prefPanelSize, maxPanelSize);

		// Lag knapper
		okButton = new JButton("Lagre", okIcon);
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				userAbort = false;
				role = getRole();
				if (isNew) {
					role = controller.lagreNyRole(role);
					roleModel.addItem(role);
				} else {
					role = controller.oppdaterRolle(role);
				}
				dispose();
			}
		});
		if (!isNew) {
			okButton.setEnabled(false);
			cancelButton = new JButton("Lukk", cancelIcon);
			roleField.setEditable(false);
		} else {
			cancelButton = new JButton("Avbryt", abortIcon);
		}
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		// Layout for knapper
		Box buttonPanel = BoxUtil.createHorizontalBox(0, Box.createHorizontalGlue(), okButton, BoxUtil
				.createHorizontalStrut(5), cancelButton);

		// faneark
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.add("Beskrivelse", descriptionPane);
		tabbedPane.add("Moduler", mladenScroller);
		tabbedPane.add("Brukere", userScrollPane);

		// Sett opp Layout for dialogen
		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(5));
		add(topPanel);
		add(BoxUtil.createVerticalStrut(5));
		add(tabbedPane);
		add(BoxUtil.createVerticalStrut(5));
		add(buttonPanel);

		pack();
		setLocationRelativeTo(controller.getParentPlugin().getModuleGUI());
		setVisible(true);
		log.debug("initGUI() - ferdig");
	}

	private void oppdaterRolleBrukere() {
		List<Bruker> roleUsers = usersList.getSelectedValues();
		List<RolleBruker> gamle = new ArrayList<RolleBruker>(role.getRolleBrukerList());
		for (RolleBruker rb : gamle) {
			Bruker b = rb.getBruker();
			if (!roleUsers.contains(b)) {
				role.removeRolleBruker(rb);
			}
		}
		for (Bruker b : roleUsers) {
			RolleBruker rb = new RolleBruker();
			rb.setBruker(b);
			rb.setRolle(role);
			rb.setOpprettetDato(Clock.now());
			rb.setOpprettetAv(controller.getAdmin().getSignatur());

			if (!role.getRolleBrukerList().contains(rb)) {
				role.addRolleBruker(rb);
			}
		}
	}

	private void oppdaterTilganger() {
		List<RolleTilgang> valgteTilganger = roleModuleAccessModel.getTilganger();
		List<RolleTilgang> gamle = role.getRolleTilgangList();
		List<RolleTilgang> remove = new ArrayList<RolleTilgang>();
		List<RolleTilgang> add = new ArrayList<RolleTilgang>();
		
		for (RolleTilgang rt : gamle) {
			if (!valgteTilganger.contains(rt)) {
				remove.add(rt);
			}
		}
		for (RolleTilgang r:remove){
			role.removeRolleTilgang(r);
		}
		for (RolleTilgang rt : valgteTilganger) {
			rt.setOpprettetAv(controller.getAdmin().getSignatur());
			rt.setOpprettetDato(Clock.now());
			if (!role.getRolleTilgangList().contains(rt)) {
				add.add(rt);
			}
		}
		for (RolleTilgang r:add){
			role.addRolleTilgang(r);
		}
	}

	public void setFieldChanged(Object instance, String beanField, boolean changed, boolean forceRemoval) {
		registeredBeans.setFieldChanged(role, beanField, changed, forceRemoval);

		Boolean changedUser = registeredBeans.isChanged();

		if (!isNew) {
			if (changedUser) {
				enableSaveButton(true);
				changeCancelButton(false);
			} else {
				enableSaveButton(false);
				changeCancelButton(true);
			}
		}
	}
}
