package no.mesta.mipss.admin.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.kontrakt.Arbeidsfunksjon;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.BrukerTilgang;
import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.persistence.tilgangskontroll.Tilgangsnivaa;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for UserAdmin plugin
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class UserAdminController<T extends IRenderableMipssEntity> {
	private static final String ERROR_NO_ACCESS = "NO ACCESS";
	private static final String ERROR_READ_ONLY = "READ ONLY ACCESS";
	private final static Logger log = LoggerFactory.getLogger(UserAdminController.class);
	private final AccessControl accessControlBean;
	private final Bruker admin;
	private final boolean banished;
	private List<Arbeidsfunksjon> funksjoner;
	private final Object FUNKSJONER_LOCK = new Object();
	private List<Modul> moduler;
	private final Object MODULER_LOCK = new Object();
	private List<Tilgangsnivaa> nivaaer;
	private final Object NIVAAER_LOCK = new Object();
	private final UserAdminModule parentPlugin;
	private final boolean readAccess;
	private List<Selskap> selskaper;
	private final Object SELSKAPER_LOCK = new Object();
	private final boolean writeAccess;

	public UserAdminController(UserAdminModule parentPlugin, AccessControl accessControlBean) {
		this.accessControlBean = accessControlBean;
		this.admin = parentPlugin.getLoader().getLoggedOnUser(false);
		this.parentPlugin = parentPlugin;

		Modul userAdminModule = parentPlugin.getModul();
		readAccess = admin.hasModuleReadAccess(userAdminModule);
		writeAccess = admin.hasModuleWriteAccess(userAdminModule);
		banished = admin.isBanishedFromModule(userAdminModule);

		log.debug("Admin: " + admin.getSignatur() + " read: " + readAccess + " write: " + writeAccess + " banished: "
				+ banished);
	}

	private void enforceReadAccess() {
		log.debug("enforceReadAccess() start");
		if (banished || (!readAccess && !writeAccess)) {
			throw new IllegalStateException(ERROR_NO_ACCESS);
		}
	}

	private void enforceWriteAccess() {
		log.debug("enforceWriteAccess() banished: " + banished + " writeAccess: " + writeAccess + " start");
		if (banished || !writeAccess) {
			JOptionPane.showMessageDialog(parentPlugin.getModuleGUI(), "Du har ikke skriverettigheter til denne modulen", "Manglende tilgang", JOptionPane.WARNING_MESSAGE);
			throw new IllegalStateException(ERROR_READ_ONLY);
		}
	}

	public Bruker getAdmin() {
		return admin;
	}

	public List<Arbeidsfunksjon> getArbeidsfunkjsoner() {
		enforceReadAccess();

		synchronized (FUNKSJONER_LOCK) {
			if (funksjoner == null) {
				funksjoner = getParentPlugin().getEntityService().getArbeidsfunksjonList();
				if (funksjoner == null) {
					funksjoner = new ArrayList<Arbeidsfunksjon>();
				}
				Collections.sort(funksjoner);
			}
		}

		return funksjoner;
	}

	public Bruker getBruker(String signatur, boolean refresh) {
		enforceReadAccess();
		return accessControlBean.hentBruker(signatur, refresh);
	}

	public UserAdminModule getParentPlugin() {
		return parentPlugin;
	}

	public Rolle getRolle(Long rolleId, boolean refresh) {
		enforceReadAccess();
		return accessControlBean.hentRolle(rolleId, refresh);
	}

	public List<Bruker> getTilgjengeligeBrukere() {
		enforceReadAccess();
		List<Bruker> brukere = accessControlBean.getBrukere();
		Collections.sort(brukere);
		return brukere;
	}

	public List<Modul> getTilgjengeligeModuler() {
		log.debug("getTilgjengeligeModuler() - start");
		enforceReadAccess();

		synchronized (MODULER_LOCK) {
			if (moduler == null) {
				moduler = accessControlBean.getModuler();
				if (moduler == null) {
					moduler = new ArrayList<Modul>();
				}
				Collections.sort(moduler);
			}
		}

		log.debug("getTilgjengeligeModuler() - ferdig, resultat: "
				+ (moduler == null ? "null" : moduler.size() + " moduler"));
		return moduler;
	}

	public List<Tilgangsnivaa> getTilgjengeligeNivaaer() {
		log.debug("getTilgjengeligeNivaaer() - start");
		enforceReadAccess();

		synchronized (NIVAAER_LOCK) {
			if (nivaaer == null) {
				nivaaer = accessControlBean.getTilgangsNivaaer();
				if (nivaaer == null) {
					nivaaer = new ArrayList<Tilgangsnivaa>();
				}
				Collections.sort(nivaaer);
			}
		}

		log.debug("getTilgjengeligeNivaaer() - ferdig, resultat: "
				+ (nivaaer == null ? "null" : nivaaer.size() + " nivaaer"));
		return nivaaer;
	}

	public List<Rolle> getTilgjengeligeRoller() {
		log.debug("getTilgjengeligeRoller() - start");
		enforceReadAccess();

		List<Rolle> roller = accessControlBean.getRoller();
		Collections.sort(roller);
		
		log.debug("getTilgjengeligeRoller() - ferdig, resultat: "
				+ (roller == null ? "null" : roller.size() + " roller"));
		return roller;
	}

	public List<Selskap> getTilgjengeligeSelskap() {
		log.debug("getTilgjengeligeSelskap() - start");
		enforceReadAccess();

		synchronized (SELSKAPER_LOCK) {
			if (selskaper == null) {
				selskaper = accessControlBean.getSelskaper();
				if (selskaper == null) {
					selskaper = new ArrayList<Selskap>();
				}
				Collections.sort(selskaper);
			}
		}

		log.debug("getTilgjengeligeSelskap() - ferdig, resultat: "
				+ (selskaper == null ? "null" : selskaper.size() + " selskaper"));
		return selskaper;
	}

	public boolean hasReadAccess() {
		return readAccess;
	}

	public boolean hasWriteAccess() {
		return writeAccess;
	}

	public boolean isBanished() {
		return banished;
	}

	public Bruker lagreNyBruker(Bruker bruker) {
		
		enforceWriteAccess();

		Bruker nyBruker = accessControlBean.lagreNyBruker(bruker, admin.getSignatur());
		log.debug("lagreNyBruker {}", nyBruker);
		return nyBruker;
	}
	
	public void validerData(){
		
	}

	public Rolle lagreNyRole(Rolle role) {
		enforceWriteAccess();

		Rolle nyRolle = accessControlBean.lagreNyRole(role, admin.getSignatur());
		log.debug("lagreNyRolle {}", nyRolle);
		return nyRolle;
	}

	public Bruker oppdaterBruker(Bruker bruker) {
		enforceWriteAccess();
		Bruker fraServer = accessControlBean.oppdaterBruker(bruker, admin.getSignatur());
		bruker.merge(fraServer);
		return bruker;
	}

	public Rolle oppdaterRolle(Rolle rolle) {
		enforceWriteAccess();

		Rolle fraServer = accessControlBean.oppdaterRolle(rolle, admin.getSignatur());
		rolle.merge(fraServer);
		return rolle;
	}

	public String slettBruker(Bruker bruker) {
		enforceWriteAccess();

		String msg = accessControlBean.slettBruker(bruker.getSignatur());
		log.debug("deleted");
		return msg;
	}

	public void slettRolle(Rolle rolle) {
		enforceWriteAccess();

		accessControlBean.slettRolle(rolle.getId());
	}

	public List<String> validerNyBruker(Bruker bruker, boolean useAD) {
		List<String> messages = new ArrayList<String>();

		if (bruker != null && StringUtils.isBlank(bruker.getSignatur())) {
			messages.add("Signatur må være fylt ut (6-8 tegn)");
		} else if (bruker != null && bruker.getSignatur().length() < 6 || bruker.getSignatur().length() > 8) {
			messages.add("Signatur må være på mellom 6 og 8 tegn");
		}

		if (bruker != null && StringUtils.isBlank(bruker.getNavn())) {
			messages.add("Navn må være fylt ut");
		}
		
		if (!useAD){
			if (bruker != null && StringUtils.isBlank(bruker.getPassordHashed())) {
				messages.add("Passord må være fylt ut");
			}
		}
		
		if (bruker.getEpost() != null && StringUtils.isBlank(bruker.getEpost())) {
			messages.add("Epost må være fylt ut");
		} else if (bruker.getEpost() != null && !bruker.getEpost().contains("@")) {
			messages.add("Du har skrevet inn en ugyldig e-postadresse");
		}
		
		return messages;
	}
}
