package no.mesta.mipss.admin.user;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;

import no.mesta.mipss.admin.user.ui.ModuleAccessTableModel;
import no.mesta.mipss.core.FieldListener;
import no.mesta.mipss.core.HashingUtils;
import no.mesta.mipss.core.IBeanFieldChange;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.MipssBeanChangeRegister;
import no.mesta.mipss.core.MipssBeanListFieldListener;
import no.mesta.mipss.core.MipssBeanTableListener;
import no.mesta.mipss.core.PasswordGeneratorUtils;
import no.mesta.mipss.ldap.LdapException;
import no.mesta.mipss.ldap.LdapUser;
import no.mesta.mipss.ldap.LdapUserFactory;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.kontrakt.Arbeidsfunksjon;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.BrukerTilgang;
import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.persistence.tilgangskontroll.RolleBruker;
import no.mesta.mipss.persistence.tilgangskontroll.RolleTilgang;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.persistence.tilgangskontroll.SelskapBruker;
import no.mesta.mipss.persistence.tilgangskontroll.Tilgangsnivaa;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.TextFieldReplicator;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.selectlist.MipssSelectionList;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssEntityRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;

/**
 * Dialog vindu for å arbeide med en bruker. Kan benyttes til å vise/endre
 * detaljer på en bestemt bruker.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class UserDialog extends JFrame implements IBeanFieldChange {
	private ImageIcon abortIcon = IconResources.STOP_PROCESS_ICON;
	private Bruker bruker;
	private JButton cancelButton;
	private ImageIcon cancelIcon = IconResources.CANCEL_ICON;
	private final UserAdminController<Bruker> controller;
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private boolean newUser;
	private JButton okButton;
	private ImageIcon okIcon = IconResources.SAVE_ICON;
	private MipssBeanChangeRegister registeredFields;
	private ModuleAccessTableModel<RolleTilgang> roleModuleAccessModel;
	private final List<Modul> tilgjengeligeModuler;
	private final List<Tilgangsnivaa> tilgjengeligeNivaaer;
	private final List<Rolle> tilgjengeligeRoller;
	private final List<Selskap> tilgjengeligeSelskap;
	private Boolean userAbort = true;
	private ModuleAccessTableModel<BrukerTilgang> userModuleAccessModel;
	private List<Rolle> opprinneligeRoller = new ArrayList<Rolle>();
	private List<Selskap> opprinneligeSelskap = new ArrayList<Selskap>();
	private MipssSelectionList<Rolle> rolleList;
	private MipssSelectionList<Selskap> selskapList;
	private final MipssBeanTableModel<Bruker> userModel;
	
	private static final String PASSORD_PLACEHOLDER = "********";
	private JTextField epostFelt;
	private JTextField passordFelt;
	private JTextField brukerSignaturFelt;
	private JTextField brukerNavnFelt;
	private Boolean useAD;
	private JCheckBox byttPassordChk;
	private JButton genererPassordButton;

	/**
	 * Konstruktøren som normalt skal benyttes når UserDialog skal benyttes
	 * 
	 * @param bruker
	 *            Hvis det ikke er snakk om en ny bruker. Hvis null, så vil en
	 *            bruker lages.
	 * @param tilgjengeligeRoller
	 *            Rollene det er mulig å velge i
	 * @param tilgjengeligeSelskap
	 *            Selskapene det er mulig å velge i
	 */
	public UserDialog(UserAdminController<Bruker> controller, Bruker bruker,
			MipssBeanTableModel<Bruker> userModel) {
		this.tilgjengeligeModuler = controller.getTilgjengeligeModuler();
		this.tilgjengeligeNivaaer = controller.getTilgjengeligeNivaaer();
		this.tilgjengeligeRoller = controller.getTilgjengeligeRoller();
		this.tilgjengeligeSelskap = controller.getTilgjengeligeSelskap();
		this.userModel = userModel;
		this.controller = controller;
		useAD = Boolean.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "useADLogIn").getVerdi());
		if (null == bruker) {
			newUser = true;
			this.bruker = new Bruker();
		} else {
			this.bruker = bruker;
			List<RolleBruker> rolleTilganger = bruker.getRolleBrukerList();
			for (RolleBruker rb : rolleTilganger) {
				opprinneligeRoller.add(rb.getRolle());
			}

			List<SelskapBruker> brukerSelskaper = bruker.getSelskapBrukerList();
			for (SelskapBruker sb : brukerSelskaper) {
				opprinneligeSelskap.add(sb.getSelskap());
			}
		}

		registerFields();

		try {
			initGUI();
		} catch (Exception e) {
			log.debug("initGUI() failed: ", e);
		}

	}

	/**
	 * Bygger bruker objektet
	 * 
	 * @return
	 */
	private Bruker buildUser() {
		if (newUser) {
			bruker.setOpprettetAv(controller.getAdmin().getSignatur());
			bruker.setOpprettetDato(Clock.now());
			bruker.setPilottilgangFlagg(1L);
		}
		bruker.setSignatur(brukerSignaturFelt.getText());
		bruker.setNavn(brukerNavnFelt.getText());
		bruker.setEpost(epostFelt.getText());
		bruker.setByttPassord(byttPassordChk.isSelected());
		
		if (!useAD){
			if (newUser){
				bruker.setPassordHashed(new HashingUtils().toHash(passordFelt.getText()));
			}else{
				if (!passordFelt.getText().equals(PASSORD_PLACEHOLDER)){
					bruker.setPassordHashed(new HashingUtils().toHash(passordFelt.getText()));
				}
			}
		}
		oppdaterBrukerSelskap();
		oppdaterBrukerRoller();
		oppdaterBrukerTilganger();

		return bruker;
	}

	private void changeCancelButton(boolean close) {
		if (close) {
			cancelButton.setText("Lukk");
			cancelButton.setIcon(cancelIcon);
		} else {
			cancelButton.setText("Avbryt");
			cancelButton.setIcon(abortIcon);
		}
	}

	private void enableSaveButton(boolean enabled) {
		okButton.setEnabled(enabled);
	}


	/**
	 * Initierer GUI til dette vinduet
	 * 
	 * @throws Exception
	 */
	private void initGUI() throws Exception {
		log.debug("initGUI() - start");

		String title = (newUser ? "Lag ny bruker" : "Vis/Endre " + bruker.getNavn());
		setTitle(title);
		setIconImage(IconResources.YELLOW_HELMET_ICON.getImage());

		brukerNavnFelt = new JTextField(bruker.getNavn());
		ComponentSizeResources.setComponentSizes(brukerNavnFelt, new Dimension(60, 23), new Dimension(200, 23),
				new Dimension(Short.MAX_VALUE, 23));
		// Setter opp panelet med bruker feltene
		// SwingWorkeren er for at GUI ikke skal vente på LDAP oppslaget.
		// Dette betyr at det kan ta litt tid for UNDO og AutoComplete
		// "virker" for brukernavn feltet
		JXLabel brukerNavnLabel = new JXLabel("Navn");
		brukerNavnLabel.setHorizontalAlignment(JXLabel.RIGHT);
		ComponentSizeResources.setComponentSize(brukerNavnLabel, new Dimension(50, 23));
		brukerSignaturFelt = new JTextField(bruker.getSignatur());
		
		ComponentSizeResources.setComponentSizes(brukerSignaturFelt, new Dimension(60, 23), new Dimension(200, 23),
				new Dimension(Short.MAX_VALUE, 23));
		SwingWorker<Void, Void> ldapWorker = new SwingWorker<Void, Void>() {
			private Map<String, LdapUser> users = null;

			/**
			 * @see javax.swing.SwingWorker#doInBackground()
			 */
			@Override
			protected Void doInBackground() {
				try {
					SwingUtilities.invokeAndWait(new Runnable(){
						public void run(){
							brukerNavnFelt.setEnabled(false);
							brukerSignaturFelt.setEnabled(false);
							byttPassordChk.setEnabled(false);
							genererPassordButton.setEnabled(false);
						}
					});
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				}
				
				try {
					if (newUser) {
						brukerNavnFelt.setText("Vennligst vent...");
						brukerSignaturFelt.setText("Vennligst vent...");
						log.debug("LDAP lookup starting");
						users = LdapUserFactory.getInstance().getUsers();
					}
				} catch (LdapException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(UserDialog.this, e.getMessage(), "Kunne ikke koble til LDAP", JOptionPane.ERROR_MESSAGE);
					log.debug("Ldap lookup failed", e);
				}
				return null;
			}

			/**
			 * @see javax.swing.SwingWorker#done()
			 */
			@Override
			protected void done() {
				if (newUser) {
					log.debug("LDAP lookup done");
					brukerNavnFelt.setText(null);
					brukerSignaturFelt.setText(null);
					users.put("Nøkkel 1", new LdapUser("Nøkkel1", "Verdi 1"));
					users.put("Nøkkel 2", new LdapUser("Nøkkel2", "Verdi 1"));

					List<LdapUser> userList = new ArrayList<LdapUser>(users.values());

					AutoCompleteDecorator.decorate(brukerSignaturFelt, userList, true, new LdapToStringConverter(true));
					// AutoCompleteDecorator kan ikke kjøre etter andre
					// listenere. Så derfor må denne skje i SwingWorker også
					brukerSignaturFelt.getDocument().addDocumentListener(
							new FieldListener<Bruker>(bruker, brukerSignaturFelt.getText(), brukerSignaturFelt,
									"Signatur", UserDialog.this));

					AutoCompleteDecorator.decorate(brukerNavnFelt, userList, true, new LdapToStringConverter(false));
					brukerNavnFelt.getDocument().addDocumentListener(
							new FieldListener<Bruker>(bruker, brukerNavnFelt.getText(), brukerNavnFelt, "Navn",
									UserDialog.this));

					brukerSignaturFelt.setEnabled(true);
					brukerNavnFelt.setEnabled(true);

					TextFieldReplicator.decorateTextField(UserDialog.this, brukerSignaturFelt, brukerNavnFelt,
							LdapUser.class, users, "SAMAccountName", "CommonName");
				}
			}
		};
		
		if (useAD){
			ldapWorker.execute();
		}

		JXLabel brukerSignaturLabel = new JXLabel("Signatur");
		brukerSignaturLabel.setHorizontalAlignment(JXLabel.RIGHT);
		
		JXLabel epostLabel = new JXLabel("E-post");
		epostLabel.setHorizontalAlignment(JXLabel.RIGHT);
		
		JXLabel passordLabel = new JXLabel("Passord");
		passordLabel.setHorizontalAlignment(JXLabel.RIGHT);
		
		epostFelt = new JTextField();
		epostFelt.setEnabled(true);
		ComponentSizeResources.setComponentSizes(epostFelt, new Dimension(60, 23), new Dimension(200, 23), new Dimension(Short.MAX_VALUE, 23));
		
		passordFelt = new JTextField();
		passordFelt.setEnabled(true);
		ComponentSizeResources.setComponentSizes(passordFelt, new Dimension(60, 23), new Dimension(200, 23), new Dimension(Short.MAX_VALUE, 23));		
		
		genererPassordButton = new JButton("Generer passord");
		genererPassordButton.setEnabled(true);
		genererPassordButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String pass = new PasswordGeneratorUtils().generatePassword();				
				passordFelt.setText(pass);
			}
		});
		
		byttPassordChk = new JCheckBox();
		byttPassordChk.setSelected(true);
		JXLabel byttPassordLabel = new JXLabel("Må skifte passord ved første pålogging");
		
		ComponentSizeResources.setComponentSize(brukerSignaturLabel, 	new Dimension(50, 23));
		ComponentSizeResources.setComponentSize(epostLabel, 			new Dimension(50, 23));
		ComponentSizeResources.setComponentSize(passordLabel, 			new Dimension(50, 23));
				
		Box brukerInfo = 	BoxUtil.createHorizontalBox(0, 
							BoxUtil.createVerticalBox(0, brukerSignaturLabel, BoxUtil.createVerticalStrut(5), brukerNavnLabel), 
							BoxUtil.createHorizontalStrut(5), 
							BoxUtil.createVerticalBox(0, brukerSignaturFelt, BoxUtil.createVerticalStrut(5), brukerNavnFelt)
						);
		
		Box top = BoxUtil.createVerticalBox(5, brukerInfo, 
				BoxUtil.createVerticalStrut(5),
				BoxUtil.createHorizontalBox(0, epostLabel, BoxUtil.createHorizontalStrut(5), epostFelt),				 
				BoxUtil.createHorizontalBox(0, passordLabel, BoxUtil.createHorizontalStrut(5), passordFelt, BoxUtil.createHorizontalStrut(10), genererPassordButton, BoxUtil.createHorizontalStrut(200)),
				BoxUtil.createHorizontalBox(0, byttPassordChk, byttPassordLabel, BoxUtil.createHorizontalStrut(360))
		);
		if (!newUser){
			if (bruker != null) {
				if (bruker.getEpost() != null) {
					epostFelt.setText(bruker.getEpost());
				}
				
				if (bruker.getByttPassord() != null) {
					byttPassordChk.setSelected(bruker.getByttPassord());
				} else {
					byttPassordChk.setSelected(false);
				}
				
				passordFelt.setText(PASSORD_PLACEHOLDER);	
				passordFelt.setEnabled(!useAD);
				genererPassordButton.setEnabled(!useAD);
				
				epostFelt.getDocument().addDocumentListener(
						new FieldListener<Bruker>(bruker, epostFelt.getText(), epostFelt,
								"Epost", UserDialog.this));
				passordFelt.getDocument().addDocumentListener(
						new FieldListener<Bruker>(bruker, passordFelt.getText(), passordFelt,
								"PassordHashed", UserDialog.this));
				byttPassordChk.addChangeListener(new ChangeListener() {
					
					@Override
					public void stateChanged(ChangeEvent e) {
						JCheckBox box = (JCheckBox) e.getSource();
						bruker.setByttPassord(box.isSelected());
						setFieldChanged(bruker, "byttPassord", BooleanUtils.isTrue(box.isSelected()), false);						
					}
				});
						
			}			
		}
		// Setter opp "picklist" for selskap
		selskapList = new MipssSelectionList<Selskap>(tilgjengeligeSelskap, opprinneligeSelskap,
				"Velg selskaper", Selskap.class);
		selskapList.addListSelectionListener((ListSelectionListener) new MipssBeanListFieldListener<Selskap>(
				selskapList.getSelectedValues(), "Selskaper", this));

		// Setter opp "picklist" for roller
		rolleList = new MipssSelectionList<Rolle>(tilgjengeligeRoller, opprinneligeRoller,
				"Velg roller", Rolle.class);
		rolleList.addListSelectionListener((ListSelectionListener) new MipssBeanListFieldListener<Rolle>(rolleList.getSelectedValues(),
				"Roller", this));
		rolleList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				roleModuleAccessModel.updateRoles(rolleList.getSelectedValues());
			}
		});
		// Setter opp modultilgang for bruker
		userModuleAccessModel = new ModuleAccessTableModel<BrukerTilgang>(Bruker.class, bruker, tilgjengeligeModuler,
				tilgjengeligeNivaaer);
		JXTable mladenTable = new JXTable(userModuleAccessModel);
		mladenTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane mladenScroller = new JScrollPane(mladenTable);
		mladenScroller.setBorder(new TitledBorder("Tilganger"));
		userModuleAccessModel.addTableModelListener((TableModelListener) new MipssBeanTableListener(
				userModuleAccessModel, "Tilganger", this));

		// Setter opp modultilgang for rollene
		roleModuleAccessModel = new ModuleAccessTableModel<RolleTilgang>(Rolle.class, opprinneligeRoller,
				tilgjengeligeModuler, tilgjengeligeNivaaer);
		JXTable mladenRoleTable = new JXTable(roleModuleAccessModel);
		mladenRoleTable.setEditable(false);
		mladenRoleTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		mladenRoleTable.setEnabled(false);
		JScrollPane mladenRoleScroller = new JScrollPane(mladenRoleTable);
		mladenRoleScroller.setBorder(new TitledBorder("Tilganger for valgte roller [KUN VISNING]"));
		// Setter opp knappepanelet
		okButton = new JButton("Lagre", okIcon);
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bruker = buildUser();
				List<String> feilMeldinger = controller.validerNyBruker(bruker, useAD);
				if (passordFelt.getText().length() < 4) {
					feilMeldinger.add("Passordet må minimum ha 4 tegn");
				}
				//TODO vis feilmeldingene til brukeren
				if (feilMeldinger.isEmpty()){
					userAbort = false;
					if (newUser) {
						bruker = controller.lagreNyBruker(bruker);
						userModel.addItem(bruker);
					} else {
						bruker = controller.oppdaterBruker(bruker);
					}
					dispose();
				} else{
					StringBuilder sb = new StringBuilder();
					sb.append("<html>Følgende feil ble funnet:<br><br>");
					for (String melding : feilMeldinger) {
						sb.append(" - " +melding + "<br>");
					}
					sb.append("</html>");

					JOptionPane.showMessageDialog(UserDialog.this, sb.toString(), "Feil ved validering", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		if (!newUser) {
			okButton.setEnabled(false);
			cancelButton = new JButton("Lukk", cancelIcon);
			brukerNavnFelt.setEditable(false);
			brukerSignaturFelt.setEditable(false);
		} else {
			cancelButton = new JButton("Avbryt", abortIcon);
		}
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		Box buttonPanel = BoxUtil.createHorizontalBox(0, Box.createHorizontalGlue(), okButton, BoxUtil
				.createHorizontalStrut(5), cancelButton);

		// Lager faneark
		JTabbedPane tabbedPane = new JTabbedPane();

		// panel for Selskap
		Box selskapPanel = BoxUtil.createHorizontalBox(0, selskapList);
		tabbedPane.add("Velg selskaper", selskapPanel);

		// panel for kontrakt
		final JMipssContractPicker contractPicker = new JMipssContractPicker(true);
		final MipssBeanTableModel<KontraktStab> kontraktStabModell = new MipssBeanTableModel<KontraktStab>(
				"stabfunksjoner", KontraktStab.class, "bruker", "driftkontrakt", "arbeidsfunksjon", "opprettetAv",
				"opprettetDato");
		MipssRenderableEntityTableColumnModel stabTCM = new MipssRenderableEntityTableColumnModel(KontraktStab.class,
				kontraktStabModell.getColumns());
		kontraktStabModell.makeDataObservable(bruker, "stabfunksjoner");
		final JMipssBeanTable<KontraktStab> table = new JMipssBeanTable<KontraktStab>(kontraktStabModell, stabTCM);
		kontraktStabModell.addTableModelListener((TableModelListener) new MipssBeanTableListener(kontraktStabModell,
				"Stabfunksjoner", this));
		table.setFillsViewportHeight(true);
		table.setFillsViewportWidth(true);
		table.setDefaultRenderer(Bruker.class, new MipssEntityRenderer());
		table.setDefaultRenderer(Driftkontrakt.class, new MipssEntityRenderer());
		table.setDefaultRenderer(Arbeidsfunksjon.class, new MipssEntityRenderer());
		table.setDefaultRenderer(Date.class, new DateTimeRenderer());
		JScrollPane kontraktScroll = new JScrollPane(table);
		JButton addContract = new JButton("Legg til");
		addContract.addActionListener(new ActionListener() {

			/** {@inheritDoc } */
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt kontrakt = contractPicker.getValgtKontrakt();
				if (kontrakt != null) {
					AddContract contractDialog = new AddContract(kontrakt);
					KontraktStab stab = contractDialog.getStab();
					if (stab != null) {
						if (!kontraktStabModell.contains(stab)) {
							bruker.addStabfunksjon(stab);
							controller.getParentPlugin().getLoader().flashMessage(
									"Lagt til \"" + stab.getTextForGUI() + "\"", 0);
						} else {
							controller.getParentPlugin().getLoader().flashMessage("Inneholder allerede funksjon", 0);
						}
					}
				} else {
					controller.getParentPlugin().getLoader().flashMessage("Velg en kontrakt først", 0);
				}
			}

		});
		JButton fjernContract = new JButton("Fjern");
		fjernContract.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				List<KontraktStab> stabfunksjoner = table.getSelectedEntities();
				for (KontraktStab stab : stabfunksjoner) {
					bruker.removeStabFunksjon(stab);
				}
			}
		});
		Box kontraktPanel = BoxUtil.createVerticalBox(0, BoxUtil.createHorizontalBox(0, contractPicker, Box
				.createHorizontalGlue()), kontraktScroll, BoxUtil.createHorizontalBox(0, Box.createHorizontalGlue(),
				fjernContract, BoxUtil.createHorizontalStrut(5), addContract));
		tabbedPane.add("Velg kontrakter", kontraktPanel);

		// panel for roller
		Box rollePanel = BoxUtil.createVerticalBox(0, rolleList, mladenRoleScroller);
		tabbedPane.add("Velg roller", rollePanel);

		// panel for tilganger
		Box tilgangPanel = BoxUtil.createHorizontalBox(0, mladenScroller);
		tabbedPane.add("Individuelle overstyringer", tilgangPanel);

		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(5));
		add(BoxUtil.createHorizontalBox(5, top));
		add(BoxUtil.createVerticalStrut(5));
		add(BoxUtil.createHorizontalBox(5, tabbedPane));
		add(BoxUtil.createVerticalStrut(5));
		add(BoxUtil.createHorizontalBox(5, buttonPanel));
		add(BoxUtil.createVerticalStrut(5));

		ComponentSizeResources.setComponentSizes(this, new Dimension(500, 400), new Dimension(700, 600), new Dimension(
				Short.MAX_VALUE, Short.MAX_VALUE));
		pack();
		setLocationRelativeTo(controller.getParentPlugin().getModuleGUI());
		setVisible(true);
		log.debug("initGUI() - ferdig");
	}

	private void oppdaterBrukerRoller() {
		List<Rolle> valgteRoller = rolleList.getSelectedValues();
		log.debug("buildUser(): valgteRoller:" + rolleList.getSelectedValues());
		List<RolleBruker> gamle = new ArrayList<RolleBruker>(bruker.getRolleBrukerList());
		for (RolleBruker rb : gamle) {
			Rolle r = rb.getRolle();
			if (!valgteRoller.contains(r)) {
				bruker.removeRolleBruker(rb);
			}
		}
		for (Rolle r : valgteRoller) {
			RolleBruker rb = new RolleBruker();
			rb.setBruker(bruker);
			rb.setRolle(r);
			rb.setOpprettetAv(controller.getAdmin().getSignatur());
			rb.setOpprettetDato(Clock.now());

			if (!bruker.getRolleBrukerList().contains(rb)) {
				bruker.addRolleBruker(rb);
			}
		}
	}

	private void oppdaterBrukerSelskap() {
		List<Selskap> valgteSelskap = selskapList.getSelectedValues();
		log.debug("buildUser(): valgteSelskap:" + valgteSelskap);
		List<SelskapBruker> gamle = new ArrayList<SelskapBruker>(bruker.getSelskapBrukerList());
		for (SelskapBruker sb : gamle) {
			Selskap s = sb.getSelskap();
			if (!valgteSelskap.contains(s)) {
				bruker.removeSelskapBruker(sb);
			}
		}

		for (Selskap s : valgteSelskap) {
			SelskapBruker sb = new SelskapBruker();
			sb.setBruker(bruker);
			sb.setSelskap(s);
			sb.setOpprettetAv(controller.getAdmin().getSignatur());
			sb.setOpprettetDato(Clock.now());

			if (!bruker.getSelskapBrukerList().contains(sb)) {
				bruker.addSelskapBruker(sb);
			}
		}
	}

	private void oppdaterBrukerTilganger() {
		List<BrukerTilgang> valgteTilganger = userModuleAccessModel.getTilganger();
		log.debug("buildUser(): valgteTilganger:" + valgteTilganger);
		List<BrukerTilgang> gamle = bruker.getBrukerTilgangList();
		List<BrukerTilgang> toRemove = new ArrayList<BrukerTilgang>();
		
		for (BrukerTilgang bt : gamle) {
			if (!valgteTilganger.contains(bt)) {
				toRemove.add(bt);
			}
		}
		for (BrukerTilgang b:toRemove){
			bruker.removeBrukerTilgang(b);
		}
		
		for (BrukerTilgang bt : valgteTilganger) {
			bt.setOpprettetAv(controller.getAdmin().getSignatur());
			bt.setOpprettetDato(Clock.now());
			if (!bruker.getBrukerTilgangList().contains(bt)) {
				bruker.addBrukerTilgang(bt);
			}else{
				changeIfStatusDifferent(bt, bruker);
			}
		}
	}
	
	private void changeIfStatusDifferent(BrukerTilgang bt, Bruker b){
		int index = b.getBrukerTilgangList().indexOf(bt);
		BrukerTilgang brukerTilgang = b.getBrukerTilgangList().get(index);
		if (mergeBrukerTilgang(bt, brukerTilgang)){
			b.removeBrukerTilgang(bt);
			b.addBrukerTilgang(brukerTilgang);
		}
	}
	
	private boolean mergeBrukerTilgang(BrukerTilgang src, BrukerTilgang dest){
		if (!src.getTilgangsnivaa().equals(dest.getTilgangsnivaa())){
			dest.setTilgangsnivaa(src.getTilgangsnivaa());
			return true;
		}
		return false;
	}
	/**
	 * Registerer feltene slik at endringer oppdages
	 * 
	 */
	private void registerFields() {
		registeredFields = new MipssBeanChangeRegister(new String[] { "Bruker", "Signatur", "Epost", "PassordHashed", "Selskaper", "Roller",
				"Tilganger", "Stabfunksjoner" });
		System.out.println(registeredFields.toString());
	}

	public void setFieldChanged(Object instance, String field, boolean changed, boolean forceRemoval) {
		registeredFields.setFieldChanged(bruker, field, changed, forceRemoval);

		Boolean changedUser = registeredFields.isChanged();

		if (!newUser) {
			if (changedUser) {
				enableSaveButton(true);
				changeCancelButton(false);
			} else {
				enableSaveButton(false);
				changeCancelButton(true);
			}
		}
	}

	private class AddContract extends JDialog {
		private boolean cancelled;
		private final KontraktStab stab;

		public AddContract(Driftkontrakt kontrakt) {
			super(UserDialog.this, "Legg til kontrakt", true);
			stab = new KontraktStab();
			stab.setBruker(bruker);
			stab.setDriftkontrakt(kontrakt);
			stab.setOpprettetAv(controller.getAdmin().getSignatur());
			stab.setOpprettetDato(new Timestamp(Clock.now().getTime()));

			MipssComboBoxModel<Arbeidsfunksjon> model = new MipssComboBoxModel<Arbeidsfunksjon>(controller
					.getArbeidsfunkjsoner(), false);
			final JComboBox combo = new JComboBox(model);
			combo.setRenderer(new MipssListCellRenderer<Arbeidsfunksjon>());
			JButton ok = new JButton("Legg til");
			ok.addActionListener(new ActionListener() {
				/** {@inheritDoc} */
				@Override
				public void actionPerformed(ActionEvent e) {
					cancelled = false;
					stab.setArbeidsfunksjon((Arbeidsfunksjon) combo.getSelectedItem());
					dispose();
				}
			});
			JButton cancel = new JButton("Avbryt");
			cancel.addActionListener(new ActionListener() {
				/** {@inheritDoc} */
				@Override
				public void actionPerformed(ActionEvent e) {
					cancelled = true;
					dispose();
				}
			});

			setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
			add(BoxUtil.createVerticalStrut(5));
			add(BoxUtil.createHorizontalBox(5, new JXLabel("Velg arbeidsfunksjon"), combo));
			add(BoxUtil.createVerticalStrut(5));
			add(BoxUtil.createHorizontalBox(5, Box.createHorizontalGlue(), cancel, ok));
			add(BoxUtil.createVerticalStrut(5));

			pack();
			setLocationRelativeTo(UserDialog.this);
			setResizable(false);
			log.debug("Opened add contract dialog");
			setVisible(true);
		}

		public KontraktStab getStab() {
			if (!cancelled) {
				return stab;
			} else {
				return null;
			}
		}
	}

	/**
	 * Brukes til å hente ut brukernavnet for autocomplete
	 * 
	 * @see org.jdesktop.swingx.autocomplete.AutoCompleteDecorator
	 */
	private class LdapToStringConverter extends ObjectToStringConverter {
		private boolean sAMAccount = true;

		/**
		 * Bestemmer hva som skal returneres av prefferedString
		 * 
		 * @param sAMAccount
		 */
		public LdapToStringConverter(boolean sAMAccount) {
			this.sAMAccount = sAMAccount;
		}

		/**
		 * @see org.jdesktop.swingx.autocomplete.ObjectToStringConverter#getPreferredStringForItem(Object)
		 * @param object
		 * @return
		 */
		@Override
		public String getPreferredStringForItem(Object object) {
			if (object == null) {
				return null;
			} else if (!(object instanceof LdapUser)) {
				return null;
			}

			LdapUser user = (LdapUser) object;

			if (sAMAccount) {
				return user.getSAMAccountName();
			} else {
				return user.getCommonName();
			}
		}
	}

}
