package no.mesta.mipss.admin.user;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Fane for å administrere brukere i MIPSS
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class UserAdminTabPanel extends AbstractGUITab<Bruker> {
	private static final Logger log = LoggerFactory.getLogger(UserAdminTabPanel.class);
	private final UserAdminController<Bruker> controller;
	private final MipssBeanTableModel<Bruker> model;

	/**
	 * @see no.mesta.mipss.admin.user.AbstractGUITab#AbstractGUITab(
	 *      MipssRenderableEntityTableModel<T> model)
	 * @param roleModel
	 */
	public UserAdminTabPanel(UserAdminModule parentPlugin, MipssBeanTableModel<Bruker> model,
			UserAdminController<Bruker> controller) {
		super(parentPlugin, model, controller);
		this.controller = controller;
		this.model = model;
	}

	/** {@inheritDoc} */
	@Override
	protected List<JButton> getActionButtons() {
		List<JButton> buttons = new ArrayList<JButton>();

		// 1. Lag knappene
		JButton addUserButton = createButton("Legg til bruker", IconResources.ADD_ITEM_ICON);
		buttons.add(addUserButton);
		ActionListener addwUser = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserDialog userDialog = new UserDialog(controller, null, model);
				
			}
		};
		addUserButton.addActionListener(addwUser);

		JButton removeUserButton = createButton("Slett bruker", IconResources.REMOVE_ITEM_ICON);
		ActionListener removeUserAction = new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				synchronized (model) {
					for (Bruker bruker : getTable().getSelectedEntities()) {
						RemoveItemDialog dialog = new RemoveItemDialog(getParentPlugin().getLoader().getApplicationFrame(), bruker);

						if (dialog.willRemoveItem()) {
							String msg = controller.slettBruker(bruker);
							if (msg!=null){
								JOptionPane.showMessageDialog(getParentPlugin().getModuleGUI(), "Kan ikke slette bruker.\nBrukeren inneholder kanskje data..\n\n"+msg, "Kan ikke slette bruker", JOptionPane.INFORMATION_MESSAGE);
								return;
							}
							model.removeItem(bruker);
							// tableView.getSelectionModel().setSelectionInterval(0,0);
							// // workaround for issue #855 i swingx
							log.debug("actionPerformed() fjern bruker knapp utført");
						}
					}
				}
			}
		};
		removeUserButton.addActionListener(removeUserAction);
		buttons.add(removeUserButton);

		JButton detailsUserButton = createButton("Endre/Vis bruker", IconResources.YELLOW_HELMET_ICON);
		ActionListener addEditViewUser = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Bruker> selection = getTable().getSelectedEntities();
				openEntities(selection);
			}
		};
		detailsUserButton.addActionListener(addEditViewUser);
		buttons.add(detailsUserButton);

		return buttons;
	}

	/** {@inheritDoc} */
	@Override
	protected String getItemListLabel() {
		return "Alle brukere";
	}

	/** {@inheritDoc} */
	@Override
	protected String getSearchLabel() {
		return "Finn bruker";
	}

	/** {@inheritDoc} */
	@Override
	public void openEntities(List<Bruker> selection) {
		for (Bruker bruker : selection) {
			Bruker brukerFraServer = controller.getBruker(bruker.getSignatur(), true);
			bruker.merge(brukerFraServer);
			log.debug("Opening: " + bruker);

			@SuppressWarnings("unused")
			UserDialog userDialog = new UserDialog(controller, bruker, model);
		}
	}
}
