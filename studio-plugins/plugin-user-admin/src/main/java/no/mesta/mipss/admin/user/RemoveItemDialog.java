package no.mesta.mipss.admin.user;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.resources.images.IconResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dialog for å bekrefte sletting av et element
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class RemoveItemDialog extends JDialog{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private IRenderableMipssEntity item = null;
    private ImageIcon warningIcon = null;
    private ImageIcon seriousOkIcon = null;
    private ImageIcon cancelIcon = null;
    private boolean removeItem = false;
        
    public RemoveItemDialog(JFrame parent, IRenderableMipssEntity item) {
        super(parent);
        log.debug("RemoveItemDialog(" + parent + "," + item + ") start");
        setModal(true);
        this.item = item;
        
        try {
            initGUI();
        } catch (Exception e) {
            log.debug("Exception: ", e);
        }
        log.debug("RemoveItemDialog(" + parent + "," + item + ") finished");
    }
    
    private void initGUI() throws Exception {
        warningIcon = IconResources.WARNING_ICON;
        seriousOkIcon = IconResources.SERIOUS_OK_ICON;
        cancelIcon = IconResources.STOP_PROCESS_ICON;
         
         setTitle("Slett \"" + item.getTextForGUI() + "\"?");       
         
        // Lager et panel med advarselen i
        JLabel warningTxtLabel = new JLabel("Advarsel, vil du virkelig slette \"" + item.getTextForGUI() + "\"?", warningIcon, SwingConstants.CENTER);
        JPanel textPanel = new JPanel(new BorderLayout());
        textPanel.add(warningTxtLabel, BorderLayout.CENTER);
        
        // Lager et panel med knapper i
        JPanel buttonPanel = new JPanel();
        JButton okButton = new JButton("Slett", seriousOkIcon);
        okButton.setIconTextGap(20);
        ActionListener okAction = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    removeItem = true;
                    dispose();
                }
            };
        okButton.addActionListener(okAction);
        JButton cancelButton = new JButton("Avbryt", cancelIcon);
        cancelButton.setIconTextGap(20);
        ActionListener cancelAction = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    removeItem = false;
                    dispose();
                }
            };
        cancelButton.addActionListener(cancelAction);
        GroupLayout buttonLayout = new GroupLayout(buttonPanel);
        buttonLayout.setAutoCreateGaps(true);
        buttonLayout.setAutoCreateContainerGaps(true);
        GroupLayout.SequentialGroup bhGroup = buttonLayout.createSequentialGroup();
        bhGroup.addGroup(buttonLayout.createParallelGroup().
            addComponent(cancelButton));
        bhGroup.addGroup(buttonLayout.createParallelGroup().
            addComponent(okButton));
        buttonLayout.setHorizontalGroup(bhGroup);
        
        GroupLayout.SequentialGroup bvGroup = buttonLayout.createSequentialGroup();
        bvGroup.addGroup(buttonLayout.createParallelGroup().
            addComponent(cancelButton).
            addComponent(okButton));
        buttonLayout.setVerticalGroup(bvGroup);
        
        // Dialogvinduet har GroupLayout
        GroupLayout layout = new GroupLayout(this.getContentPane());
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        hGroup.addGroup(layout.createParallelGroup().
            addComponent(textPanel).
            addComponent(buttonPanel));
        layout.setHorizontalGroup(hGroup);
        
        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        vGroup.addGroup(layout.createParallelGroup().
            addComponent(textPanel));
        vGroup.addGroup(layout.createParallelGroup().
            addComponent(buttonPanel));
        layout.setVerticalGroup(vGroup);
        
        setLayout(layout);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    public boolean willRemoveItem() {
        return removeItem;
    }
}
