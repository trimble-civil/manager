package no.mesta.mipss.admin.user.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.BrukerTilgang;
import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.persistence.tilgangskontroll.RolleTilgang;
import no.mesta.mipss.persistence.tilgangskontroll.Tilgangsnivaa;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Modell for tilgang til moduler
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class ModuleAccessTableModel<T> implements TableModel {
	private static final Logger log = LoggerFactory.getLogger(ModuleAccessTableModel.class);
	private static final int NOT_SET = -1;
	private static final int ROLEMODE = 1;
	private static final int ROLESMODE = 2;
	private static final int USERMODE = 0;
	private List<Tilgangsnivaa> accessLevels;
	private Bruker bruker;
	private Class<?> clazz;
	private Object entity;
	private int mode = NOT_SET;
	private List<Modul> modules;
	private List<Rolle> roles;
	private Rolle rolle;
	private List<TableModelListener> tableModelListeners = new ArrayList<TableModelListener>();
	private Map<AccessKey, AccessValue> values = new Hashtable<AccessKey, AccessValue>();

	/**
	 * Standard konstruktør
	 * 
	 * @param clazz
	 *            Klassen til enten BrukerTilgang eller RolleTilgang
	 * @param entity
	 *            et objekt, enten av typen BrukerTilgang eller RolleTilgang
	 * @param modules
	 *            liste av modulene i systemet
	 * @param accessLevels
	 *            listen av tilgangsnivåene i systemet
	 */
	@SuppressWarnings("unchecked")
	public ModuleAccessTableModel(Class<?> clazz, Object entity, List<Modul> modules, List<Tilgangsnivaa> accessLevels) {
		this.clazz = clazz;
		this.entity = entity;
		this.modules = modules;
		this.accessLevels = accessLevels;

		if (StringUtils.equals(clazz.getName(), Bruker.class.getName())) {
			bruker = (Bruker) entity;
			mode = USERMODE;
		} else if (StringUtils.equals(clazz.getName(), Rolle.class.getName()) && entity.getClass().equals(Rolle.class)) {
			rolle = (Rolle) entity;
			mode = ROLEMODE;
		} else {
			roles = (List<Rolle>) entity;
			mode = ROLESMODE;
		}

		mapLevels();
	}

	/**
	 * Legger til en lytter til modellen
	 * 
	 * @param l
	 */
	public void addTableModelListener(TableModelListener l) {
		tableModelListeners.add(l);
	}

	private AccessValue findAccessValue(Modul m, Tilgangsnivaa tn, List<T> access) {
		AccessValue value = new AccessValue();
		log.trace("findAccessValue(" + m + "," + tn + "," + access + ") start");
		for (Object o : access) {
			if (USERMODE == mode) {
				BrukerTilgang bt = (BrukerTilgang) o;
				if (m.equals(bt.getModul()) && tn.equals(bt.getTilgangsnivaa())) {
					value.isSet = true;
					value.brukerTilgang = bt;
					break;
				}
			} else if (ROLEMODE == mode || ROLESMODE == mode) {
				RolleTilgang rt = (RolleTilgang) o;
				if (m.equals(rt.getModul()) && tn.equals(rt.getTilgangsnivaa())) {
					value.isSet = true;
					value.rolleTilgang = rt;
					break;
				}
			}
		}

		log.trace("findAccessValue(" + m + "," + tn + "," + access + ") ferdig");
		return value;
	}

	/**
	 * Tilgangsnivåeene som modellen ble opprettet med
	 * 
	 * @return
	 */
	public List<Tilgangsnivaa> getAccessLevels() {
		return accessLevels;
	}

	/**
	 * Klassen som modellen ble opprettet med
	 * 
	 * @return
	 */
	public Class<?> getClazz() {
		return clazz;
	}

	/**
	 * Returnerer typen for kollonnene
	 * 
	 * @param columnIndex
	 * @return
	 */
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 0) {
			return String.class;
		} else {
			return Boolean.class;
		}
	}

	/**
	 * Tabellen har like mange kollonner som det er tilgangsnivåer + 1
	 * (modulnavnet)
	 * 
	 * @return
	 */
	public int getColumnCount() {
		return accessLevels.size() + 1;
	}

	/**
	 * Returnerer heading verdier for tabellen
	 * 
	 * @param columnIndex
	 * @return
	 */
	public String getColumnName(int columnIndex) {
		if (columnIndex == 0) {
			return "Modul";
		} else {
			return accessLevels.get(columnIndex - 1).getNivaa();
		}
	}

	/**
	 * Entiteten som modellen ble opprettet med
	 * 
	 * @return
	 */
	public Object getEntity() {
		return entity;
	}

	/**
	 * Modulene som modellen ble opprettet med
	 * 
	 * @return
	 */
	public List<Modul> getModules() {
		return modules;
	}

	@SuppressWarnings("unchecked")
	private List<T> getRoleAccess(List<Rolle> roles) {
		List<T> access = new ArrayList<T>();
		modules = new ArrayList<Modul>(); // Vis kun moduler rollen har
		log.trace("getRoleAccess(roles), roles: " + roles);
		for (Rolle r : roles) {
			List<RolleTilgang> tilganger = r.getRolleTilgangList();
			log.trace("getRoleAccess(roles), rolle: " + r.getNavn() + ", tilganger: " + tilganger);
			for (RolleTilgang rt : tilganger) {
				Modul m = rt.getModul();

				if (m != null && !modules.contains(m)) {
					modules.add(m);
					log.trace("getRoleAccess(), modul: " + m.getNavn() + " lagt til");
				}
				access.add((T) rt);
				log.trace("getRoleAccess(), nivå: " + rt.getTilgangsnivaa().getNivaa() + " lagt til");
			}
		}

		return access;
	}

	/**
	 * Tabellen har like mange rader som det er moduler
	 * 
	 * @return
	 */
	public int getRowCount() {
		return modules.size();
	}

	/**
	 * Benyttes for å hente ut en liste av enten BrukerTilgang eller
	 * RolleTilgang etter at brukeren har satt sine valg på modultilganger.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<T> getTilganger() {
		List<T> access = new ArrayList<T>();

		for (Modul m : modules) {
			for (Tilgangsnivaa tn : accessLevels) {
				AccessKey key = new AccessKey(m, tn);
				AccessValue value = values.get(key);

				if (value.isSet()) {
					if (USERMODE == mode) {
						access.add((T) value.getBrukerTilgang());
					} else {
						access.add((T) value.getRolleTilgang());
					}
				}
			}
		}

		return access;
	}

	/**
	 * For kollonne 0 gir den navnet til modulen, ellers gir den om brukeren har
	 * rettighet til den.
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		log.trace("getValueAt(" + rowIndex + "," + columnIndex + "), verdier: " + values.size());

		if (columnIndex == 0) {
			return modules.get(rowIndex).getNavn();
		} else {
			Modul m = modules.get(rowIndex);
			Tilgangsnivaa tn = accessLevels.get(columnIndex - 1);
			AccessKey key = new AccessKey(m, tn);
			AccessValue value = values.get(key);
			log.trace("getValueAt(" + rowIndex + "," + columnIndex + "), slår opp verdi["
					+ (value == null ? "null" : value.isSet()) + "] med nøkkel[" + key.getModul().getNavn()
					+ "," + key.getNivaa().getNivaa() + "]");
			return value.isSet;
		}
	}

	/**
	 * Tilgangsnivåene er editerbare, men det er ikke modulnavnene
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == 0) {
			return false;
		} else {
			if (USERMODE == mode || ROLEMODE == mode) {
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private void mapLevels() {

		List<T> access = null;
		if (USERMODE == mode) {
			log.debug("mapLevels() USERMODE");
			access = (List<T>) bruker.getBrukerTilgangList();
		} else if (ROLEMODE == mode) {
			log.debug("mapLevels() ROLEMODE");
			access = (List<T>) rolle.getRolleTilgangList();
		} else {
			log.debug("mapLevels() ROLESMODE");
			access = getRoleAccess(roles);
		}

		if (null == access) {
			access = new ArrayList<T>();
		}

		for (Modul m : modules) {
			for (Tilgangsnivaa tn : accessLevels) {
				AccessKey key = new AccessKey(m, tn);

				AccessValue value = findAccessValue(m, tn, access);
				log.trace("mapLevels(), mode=" + mode + ", lagt verdi[" + value.isSet + "] med nøkkel["
						+ key.getModul().getNavn() + "," + key.getNivaa().getNivaa() + "]");
				values.put(key, value);
			}
		}
	}

	/**
	 * Fjerner en lytter til modellen
	 * 
	 * @param l
	 */
	public void removeTableModelListener(TableModelListener l) {
		tableModelListeners.remove(l);
	}

	/**
	 * Setter verdien til rettigheten (kollonne) gitt en modul (rad)
	 * 
	 * @param aValue
	 * @param rowIndex
	 * @param columnIndex
	 */
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// Når en verdi settes må:
		// 1. cellen toggles
		// 2. events fyres
		if (columnIndex == 0) {
			return;
		}

		Boolean isSet = (Boolean) aValue;
		Modul m = modules.get(rowIndex);
		for (int i = 0; i < accessLevels.size(); i++) {
			Tilgangsnivaa tn = accessLevels.get(i);
			AccessKey key = new AccessKey(m, tn);
			AccessValue value = values.get(key);
			if (i == columnIndex - 1) {
				value.setIsSet(key, isSet);
			} else {
				value.setIsSet(key, false);
			}
		}

		TableModelEvent tme = new TableModelEvent(this, rowIndex, rowIndex, TableModelEvent.ALL_COLUMNS,
				TableModelEvent.UPDATE);
		for (TableModelListener tml : tableModelListeners) {
			tml.tableChanged(tme);
		}
	}

	/**
	 * Kan kalles når tabellgrunnlaget endrer seg, feks en bruker får en ny
	 * rolle
	 * 
	 * @param roles
	 */
	public void updateRoles(List<Rolle> roles) {
		synchronized (roles) {
			this.roles = roles;
			mapLevels();

			TableModelEvent tme = new TableModelEvent(this);
			for (TableModelListener tml : tableModelListeners) {
				tml.tableChanged(tme);
			}
		}
	}

	/**
	 * Nøkkel for oversikten over satte tilganger
	 * 
	 */
	class AccessKey {
		private Modul modul = null;
		private Tilgangsnivaa nivaa = null;

		/**
		 * Konstruktør
		 * 
		 * @param m
		 * @param nivaa
		 */
		public AccessKey(Modul m, Tilgangsnivaa nivaa) {
			this.modul = m;
			this.nivaa = nivaa;
		}

		/**
		 * @see java.lang.Object#equals
		 * @param o
		 * @return
		 */
		@Override
		public boolean equals(Object object) {
			if (object == null) {
				return false;
			}
			if (object == this) {
				return true;
			}
			if (!(object.getClass().equals(AccessKey.class))) {
				return false;
			} // mvn compiler liker ikke instanceof i generics klasser

			AccessKey other = (AccessKey) object;
			boolean isEquals = new EqualsBuilder().append(modul, other.getModul()).append(nivaa, other.getNivaa())
					.isEquals();
			return isEquals;
		}

		/**
		 * Modulen som benyttes
		 * 
		 * @return
		 */
		public Modul getModul() {
			return modul;
		}

		/**
		 * Nivået
		 * 
		 * @return
		 */
		public Tilgangsnivaa getNivaa() {
			return nivaa;
		}

		/**
		 * @see java.lang.Object#hashCode
		 * @return
		 */
		@Override
		public int hashCode() {
			int code = new HashCodeBuilder(15, 9).append(modul).append(nivaa).toHashCode();
			log.trace("hashCode(" + modul.getNavn() + "," + nivaa.getNivaa() + "): " + code);
			return code;
		}

		/**
		 * Modulen som benyttes
		 * 
		 * @param m
		 */
		public void setModul(Modul m) {
			modul = m;
		}

		/**
		 * Nivået
		 * 
		 * @param tn
		 */
		public void setNivaa(Tilgangsnivaa tn) {
			nivaa = tn;
		}
	}

	/**
	 * Verdien til cellen i tabellen
	 */
	class AccessValue {
		private BrukerTilgang brukerTilgang;
		private boolean isSet;
		private RolleTilgang rolleTilgang;

		@Override
		public boolean equals(Object o) {
			if (o == null)
				return false;
			if (o == this) {
				return true;
			} else {
				return false;
			}
		}

		public BrukerTilgang getBrukerTilgang() {
			return brukerTilgang;
		}

		public RolleTilgang getRolleTilgang() {
			return rolleTilgang;
		}

		@Override
		public int hashCode() {
			if (USERMODE == mode) {
				return new HashCodeBuilder(17, 19).append(brukerTilgang).hashCode();
			} else {
				return new HashCodeBuilder(17, 19).append(rolleTilgang).toHashCode();
			}
		}

		public boolean isSet() {

			return isSet;
		}

		public void setBrukerTilgang(BrukerTilgang bt) {
			brukerTilgang = bt;
		}

		public void setIsSet(AccessKey key, boolean flag) {
			if (USERMODE == mode) {
				if (!flag) {
					// Fjern tilgangen
					brukerTilgang = null;
				} else {
					// Lag en ny tilgang
					brukerTilgang = new BrukerTilgang();
					brukerTilgang.setBruker(bruker);
					brukerTilgang.setSignatur(bruker.getSignatur());
					brukerTilgang.setModul(key.getModul());
					brukerTilgang.setModulId(key.getModul().getId());
					brukerTilgang.setTilgangsnivaa(key.getNivaa());
					brukerTilgang.setOpprettetAv("todo");
					brukerTilgang.setOpprettetDato(Clock.now());
				}
			} else if (ROLEMODE == mode) {
				if (!flag) {
					// Fjern tilgangen
					rolleTilgang = null;
				} else {
					// Lag en ny tilgang
					rolleTilgang = new RolleTilgang();
					rolleTilgang.setRolle(rolle);
					rolleTilgang.setModul(key.getModul());
					rolleTilgang.setTilgangsnivaa(key.getNivaa());
					rolleTilgang.setOpprettetAv("todo");
					rolleTilgang.setOpprettetDato(Clock.now());
				}
			}

			isSet = flag;
		}

		public void setRolleTilgang(RolleTilgang rt) {
			rolleTilgang = rt;
		}
	}
}
