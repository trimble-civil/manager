package no.mesta.mipss.admin.user;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Fane for å administrere roller
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class RoleAdminTabPanel extends AbstractGUITab<Rolle> {
	private static final Logger log = LoggerFactory.getLogger(RoleAdminTabPanel.class);
	private final UserAdminController<Rolle> controller;
	private final MipssBeanTableModel<Rolle> roleModel;

	/**
	 * @see no.mesta.mipss.admin.user.AbstractGUITab#AbstractGUITab(
	 *      MipssRenderableEntityTableModel<T> model)
	 * @param roleModel
	 */
	public RoleAdminTabPanel(UserAdminModule parentPlugin, MipssBeanTableModel<Rolle> roleModel,
			UserAdminController<Rolle> controller) {
		super(parentPlugin, roleModel, controller);
		this.controller = controller;
		this.roleModel = roleModel;
	}

	/**
	 * @see no.mesta.mipss.admin.user.AbstractGUITab#getActionButtons()
	 * @return
	 */
	protected List<JButton> getActionButtons() {
		List<JButton> buttons = new ArrayList<JButton>();

		JButton addRoleButton = createButton("Legg til rolle", IconResources.ADD_ITEM_ICON);
		ActionListener addRoleAction = new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				RoleDialog roleDialog = new RoleDialog(controller, null, roleModel);
				Rolle rolle = roleDialog.getRole();
				if (rolle != null) {
					roleModel.addItem(rolle);
				}
			}
		};
		addRoleButton.addActionListener(addRoleAction);
		buttons.add(addRoleButton);

		JButton removeRoleButton = createButton("Slett rolle", IconResources.REMOVE_ITEM_ICON);
		removeRoleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (roleModel) {
					for (Rolle rolle : getTable().getSelectedEntities()) {
						RemoveItemDialog dialog = new RemoveItemDialog(getParentPlugin().getLoader()
								.getApplicationFrame(), rolle);
						if (dialog.willRemoveItem()) {
							controller.slettRolle(rolle);
							roleModel.removeItem(rolle);
							log.debug("actionPerformed() remove role button done");
						}
					}
				}
			}
		});
		buttons.add(removeRoleButton);

		JButton detailsRoleButton = createButton("Endre/Vis rolle", IconResources.ROLES_ICON);
		detailsRoleButton.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				List<Rolle> selection = getTable().getSelectedEntities();
				openEntities(selection);
			}
		});
		buttons.add(detailsRoleButton);

		return buttons;
	}

	/** {@inheritDoc} */
	@Override
	protected String getItemListLabel() {
		return "Alle roller";
	}

	/** {@inheritDoc} */
	@Override
	protected String getSearchLabel() {
		return "Finn rolle";
	}

	/** {@inheritDoc} */
	@Override
	public void openEntities(List<Rolle> selection) {
		for (Rolle rolle : selection) {
			Rolle serverRolle = controller.getRolle(rolle.getId(), true);
			rolle.merge(serverRolle);
			@SuppressWarnings("unused")
			RoleDialog roleDialog = new RoleDialog(controller, rolle, roleModel);
		}
	}
}
