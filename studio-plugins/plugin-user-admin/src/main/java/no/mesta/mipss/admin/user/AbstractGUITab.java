package no.mesta.mipss.admin.user;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.EntityToStringConverter;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.ui.table.RowPatternFilter;

import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.decorator.Filter;
import org.jdesktop.swingx.decorator.FilterPipeline;
import org.jdesktop.swingx.decorator.PatternFilter;
import org.jdesktop.swingx.decorator.SortOrder;

/**
 * Superklasse for faner i Brukeradministrasjonplugin.
 * 
 * Gir generisk implementasjon av jbinit funksjonen som sørger for lik layout i
 * hver fane med funksjonalitet
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href ="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public abstract class AbstractGUITab<T extends IRenderableMipssEntity> extends JPanel {
	private final UserAdminController<T> controller;
	private final MipssBeanTableModel<T> model;
	private final UserAdminModule parentPlugin;
	protected JMipssBeanTable<T> tableView;

	/**
	 * Konstruktøren som skal benyttes
	 * 
	 * @param model
	 */
	public AbstractGUITab(UserAdminModule parentPlugin, MipssBeanTableModel<T> model, UserAdminController<T> controller) {
		this.model = model;
		this.parentPlugin = parentPlugin;
		this.controller = controller;

		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Lager en standard størrelse knapp for GUIet
	 * 
	 * @param label
	 * @param iconFile
	 * @return
	 */
	protected JButton createButton(String label, ImageIcon icon) {
		JButton button = new JButton(label, icon);
		button.setHorizontalAlignment(SwingConstants.LEFT);
		button.setIconTextGap(1);
		button.setMargin(new Insets(3, 3, 3, 3));
		ComponentSizeResources.setComponentSize(button, new Dimension(130, 25));
		return button;
	}

	/**
	 * Henter ut raden med knapper som skal vises i fanen.
	 * 
	 * @return
	 */
	protected abstract List<JButton> getActionButtons();
	
	/**
	 * Kontrolleren til GUI'et
	 * 
	 * @return
	 */
	public UserAdminController<T> getController() {
		return controller;
	}

	/**
	 * Henter teksten til merket for listen
	 * 
	 * @return
	 */
	protected abstract String getItemListLabel();

	/**
	 * Returnerer modellen som benyttes av fanen
	 * 
	 * @return
	 */
	public MipssRenderableEntityTableModel<T> getModel() {
		return model;
	}

	/**
	 * Tilgang til pluginmodulen som eier denne fanen
	 * 
	 * @return
	 */
	public UserAdminModule getParentPlugin() {
		return parentPlugin;
	}

	/**
	 * Henter teksten til merket for søkefeltet
	 * 
	 * @return
	 */
	protected abstract String getSearchLabel();

	/**
	 * Tabellen som vises i fanen
	 * 
	 * @return
	 */
	protected JMipssBeanTable<T> getTable() {
		return tableView;
	}

	/**
	 * Layout for fanen
	 * 
	 * @throws Exception
	 */
	protected void jbInit() throws Exception {
		// Lager komponentene til søkefeltet
		final JTextField searchField = new JTextField();
		JXLabel searchFieldLabel = new JXLabel(getSearchLabel());
		ComponentSizeResources.setComponentSize(searchFieldLabel, new Dimension(60, 20));
		searchFieldLabel.setHorizontalAlignment(JXLabel.RIGHT);
		searchField.setPreferredSize(new Dimension(200, 20));
		searchField.setMaximumSize(new Dimension(200, 20));
		JButton clearField = new JButton("Nullstill");
		clearField.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				searchField.setText("");
			}
		});

		// Slå på autocomplete for listen
		AutoCompleteDecorator.decorate(searchField, model.getEntities(), false, new EntityToStringConverter<T>());

		// Lag tabell komponentene
		String[] columns = model.getColumns();
		MipssRenderableEntityTableColumnModel tcm = new MipssRenderableEntityTableColumnModel(model.getClazz(), columns);
		tableView = new JMipssBeanTable<T>(model, tcm);
		tableView.setDefaultRenderer(Date.class, new DateTimeRenderer());
//		tableView.setColumnControlVisible(true);
		tableView.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableView.setSortOrder(0, SortOrder.ASCENDING);
		tableView.setFillsViewportHeight(true);
		tableView.setFillsViewportWidth(true);
		tableView.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_ALL_COLUMNS);
		tableView.addMouseListener(new MouseAdapter() {
			/** {@inheritDoc} */
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && tableView.rowAtPoint(e.getPoint()) != -1) {
					if (e.getClickCount() == 2) {
						List<T> selection = tableView.selectMouseSelection(e);
						openEntities(selection);
					}
				}
			}
		});
		model.loadData();

		JMipssBeanScrollPane scrollList = new JMipssBeanScrollPane(tableView, model);
		JXLabel listLabel = new JXLabel(getItemListLabel());
		searchField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String filter = searchField.getText();
				Pattern pattern = Pattern.compile(filter,Pattern.CASE_INSENSITIVE);
				RowPatternFilter pf = new RowPatternFilter(pattern,tableView.getColumnCount());
				FilterPipeline fp = new FilterPipeline(new Filter[] { pf });
				tableView.setFilters(fp);
			}
		});

		// 1. Lag knappene
		List<JButton> buttons = getActionButtons();

		// 2. Sett opp panel for knapper
		Box buttonPanel = Box.createVerticalBox();
		int bCount = 0;
		for (JButton b : buttons) {
			buttonPanel.add(b);
			if (bCount < buttons.size() - 1) {
				buttonPanel.add(BoxUtil.createVerticalStrut(5));
			}
		}
		buttonPanel.add(Box.createVerticalGlue());

		// Lager toppanelet for søk
		Box topPanel = Box.createHorizontalBox();
		topPanel.add(searchFieldLabel);
		topPanel.add(BoxUtil.createHorizontalStrut(5));
		topPanel.add(searchField);
		topPanel.add(BoxUtil.createHorizontalStrut(5));
		topPanel.add(clearField);
		topPanel.add(Box.createHorizontalGlue());

		// Lag nedre halvdel av vindu,
		// holder liste og knappepanelet
		Box bottomPanel = Box.createVerticalBox();
		bottomPanel.add(BoxUtil.createHorizontalBox(0, listLabel, Box.createHorizontalGlue()));
		bottomPanel.add(BoxUtil.createVerticalStrut(5));
		bottomPanel.add(BoxUtil.createHorizontalBox(0, scrollList, Box.createHorizontalStrut(5), buttonPanel));

		// Lager hoved layouten
		// Øverst: Panelet med søk
		// Neders: Panelet med liste og knapper
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(5));
		add(topPanel);
		add(BoxUtil.createVerticalStrut(5));
		add(bottomPanel);
		add(BoxUtil.createVerticalStrut(5));
	}

	/**
	 * Åpner et utvalg
	 * 
	 * @param selection
	 */
	public abstract void openEntities(List<T> selection);
}
