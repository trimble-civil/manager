package no.mesta.mipss.admin.user;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hoved panelet for brukeradministrasjon-plugin Setter opp alle fanene
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class UserAdminPanel extends JPanel {
	private static final Logger logger = LoggerFactory.getLogger(UserAdminPanel.class);
    private final JTabbedPane tabPanel = new JTabbedPane();
    private final MipssBeanTableModel<Bruker> userModel;
    private final MipssBeanTableModel<Rolle> roleModel;
    private final UserAdminModule parentPlugin;
    
    /**
     * Hovedkonstruktøren, tar inn alle modellene som skal benyttes av
     * listene i tilhørende faner.
     * 
     * @param userModel
     * @param roleModel
     * @param modulModel
     */
    public UserAdminPanel(
        UserAdminModule parentPlugin,
        MipssBeanTableModel<Bruker> userModel,
        MipssBeanTableModel<Rolle> roleModel)
    {
        this.userModel = userModel;
        this.roleModel = roleModel;
        this.parentPlugin = parentPlugin;
        
        try {
            jbInit();
        } catch (Throwable e) {
            if(parentPlugin != null) {
            	parentPlugin.getLoader().handleException(e, parentPlugin, "Beklager en feil har oppstått");
            } else {
            	logger.error("Throwable in UserAdminPanel", e);
            }
        }
    }

    /**
     * Initierer layouten og komponentene i visningen
     * 
     * @throws Exception
     */
    private void jbInit() throws Exception {
        this.setLayout(new BorderLayout());
        
        UserAdminController<Bruker> brukerController = new UserAdminController<Bruker>(parentPlugin, parentPlugin.getAccessControlBean());
        UserAdminTabPanel userTab = new UserAdminTabPanel(parentPlugin, userModel, brukerController);
        tabPanel.addTab("Brukere", IconResources.YELLOW_HELMET_ICON, userTab);
        
        UserAdminController<Rolle> rolleController = new UserAdminController<Rolle>(parentPlugin, parentPlugin.getAccessControlBean());
        RoleAdminTabPanel roleTab = new RoleAdminTabPanel(parentPlugin, roleModel, rolleController);
        tabPanel.addTab("Roller", IconResources.ROLES_ICON, roleTab);
        
        add(tabPanel, BorderLayout.CENTER);
    }
}
