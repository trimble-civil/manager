package no.mesta.mipss.admin.user;

import javax.swing.JPanel;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.entityservice.MipssEntityService;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Plugin for å håndtere brukere
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class UserAdminModule extends MipssPlugin {
	private final static Logger logger = LoggerFactory.getLogger(UserAdminModule.class);
	private AccessControl accessControlBean;
	private MaintenanceContract contractBean;
	private MipssEntityService entityService;
	private UserAdminPanel myUserAdminPanel;

	public UserAdminModule() {
		logger.debug("UserAdminModule()");
	}

	public AccessControl getAccessControlBean() {
		return accessControlBean;
	}

	public MaintenanceContract getContractBean() {
		return contractBean;
	}

	public MipssEntityService getEntityService() {
		return entityService;
	}

	/** {@inheritDoc} */
	@Override
	public JPanel getModuleGUI() {
		logger.debug("getModuleGUI() returns gui panel");
		return myUserAdminPanel;
	}

	/** {@inheritDoc} */
	@Override
	public void initPluginGUI() {
		logger.debug("initPluginGUI() start");
		accessControlBean = BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class);
		contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		entityService = BeanUtil.lookup(MipssEntityService.BEAN_NAME, MipssEntityService.class);

		// Sett opp modellen for brukere
		MipssBeanTableModel<Bruker> userModel = new MipssBeanTableModel<Bruker>(Bruker.class, "navn", "signatur",
				"opprettetAv", "opprettetDato", "endretAv", "endretDato");
		userModel.setBeanInstance(accessControlBean);
		userModel.setBeanInterface(AccessControl.class);
		userModel.setBeanMethod("getBrukere");

		// Sett opp modellen for roller
		MipssBeanTableModel<Rolle> roleModel = new MipssBeanTableModel<Rolle>(Rolle.class, "navn");
		roleModel.setBeanInstance(accessControlBean);
		roleModel.setBeanInterface(AccessControl.class);
		roleModel.setBeanMethod("getRoller");

		myUserAdminPanel = new UserAdminPanel(this, userModel, roleModel);

		logger.debug("initPluginGUI() finished");
	}

	/** {@inheritDoc} */
	@Override
	public void shutdown() {
		logger.debug("shudown");
		loader = null;
		accessControlBean = null;
		contractBean = null;
		entityService = null;
	}
}
