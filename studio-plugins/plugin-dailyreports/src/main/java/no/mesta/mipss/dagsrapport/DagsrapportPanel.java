package no.mesta.mipss.dagsrapport;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import org.jdesktop.swingx.JXDatePicker;

public class DagsrapportPanel extends JPanel {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JComboBox jComboBox1 = new JComboBox();
    private JLabel jLabel2 = new JLabel();
    private JTextField jTextField1 = new JTextField();
    private JLabel jLabel3 = new JLabel();
    private JTable jTable1 = new JTable();
    private JScrollPane jScrollPane1 = new JScrollPane();
    private JPanel jPanel1 = new JPanel();
    private JLabel jLabel4 = new JLabel();
    private JXDatePicker jXDatePicker1 = new JXDatePicker();
    private JButton jButton2 = new JButton();
    private JPanel jPanel2 = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel jPanel3 = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JButton jButton1 = new JButton();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();

    public DagsrapportPanel() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buildTable(){
        
        Object[] kolonneNavn = new Object[]{"Navn", "E-post", "Kopi", "Dagsrapport?"};
        Object[][] verdier = {{"Mesta", "mesta@mesta.no", "mesta@mesta.no", new Boolean(true)},
                            {"LeverandÝr1", "leverandÝr@mesta.no", "leverandÝr@mesta.no", new Boolean(false)},
                            {"Mesta", "mesta@mesta.no", "mesta@mesta.no", new Boolean(true)},
                            {"LeverandÝr1", "leverandÝr@mesta.no", "leverandÝr@mesta.no", new Boolean(false)},
                            {"Mesta", "mesta@mesta.no", "mesta@mesta.no", new Boolean(true)},
                            {"LeverandÝr1", "leverandÝr@mesta.no", "leverandÝr@mesta.no", new Boolean(false)},
                            {"Mesta", "mesta@mesta.no", "mesta@mesta.no", new Boolean(true)},
                            {"LeverandÝr1", "leverandÝr@mesta.no", "leverandÝr@mesta.no", new Boolean(false)},
                            {"Mesta", "mesta@mesta.no", "mesta@mesta.no", new Boolean(true)},
                            {"LeverandÝr1", "leverandÝr@mesta.no", "leverandÝr@mesta.no", new Boolean(false)}};
        jTable1 = new JTable(verdier, kolonneNavn);    
        
    }
    
    private void createKontrakterModelCombo(){
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement("Vennligst velg kontrakt...");
        model.addElement("0301 - Oslo ytre by");
        model.addElement("0603 - Kongsberg");
        model.addElement("2007 - Hammerfest");
        jComboBox1.setModel(model);
    }
    private void jbInit() throws Exception {
        createKontrakterModelCombo();
        this.setLayout(gridBagLayout1);
        this.setSize(new Dimension(461, 440));
        buildTable();
        jLabel2.setText("SÝk - leverandÝr :");
        jLabel3.setText("Mottakere");
        jPanel1.setBorder(BorderFactory.createTitledBorder("Send ekstra dagsrapport til merkede leverandÝrer"));
        jPanel1.setLayout(gridBagLayout4);
        jLabel4.setText("Velg dag : ");
        jButton2.setText("Send dagsrapport");
        jPanel2.setBorder(BorderFactory.createTitledBorder("Velg kontrakt"));
        jPanel2.setLayout(gridBagLayout2);
        jPanel3.setBorder(BorderFactory.createTitledBorder("LeverandÝrer"));
        jPanel3.setLayout(gridBagLayout3);
        jButton1.setText("Legg til ny leverandÝr");
        jPanel1.add(jLabel4, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 0, 16), 0, 0));
        jPanel1.add(jXDatePicker1, 
                    new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 0, 69), 0, 0));
        jPanel1.add(jButton2, 
                    new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 0, 54), 0, 0));
        this.add(jPanel1, 
                 new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, 
                                        GridBagConstraints.BOTH, 
                                        new Insets(10, 0, 0, 0), 0, 10));
        jPanel2.add(jComboBox1, 
                    new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 5, 10, 5), 0, 0));
        this.add(jPanel2, 
                 new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, 
                                        GridBagConstraints.BOTH, 
                                        new Insets(0, 0, 0, 0), 0, 0));
        jPanel3.add(jLabel2, 
                    new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 0, 0), 0, 0));
        jPanel3.add(jTextField1, 
                    new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 0, 5), 0, 0));
        jScrollPane1.getViewport().add(jTable1, null);
        jPanel3.add(jScrollPane1, 
                    new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                           new Insets(5, 5, 0, 5), 392, 94));
        jPanel3.add(jLabel3, 
                    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(10, 5, 0, 0), 0, 0));
        jPanel3.add(jButton1, 
                    new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(10, 5, 0, 0), 0, 0));
        this.add(jPanel3, 
                 new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                        new Insets(10, 0, 0, 0), 0, 15));
    }
    
    public static void main(String[] args){
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException e) {
            // TODO
        } catch (IllegalAccessException e) {
            // TODO
        } catch (UnsupportedLookAndFeelException e) {
            // TODO
        } catch (InstantiationException e) {
            // TODO
        }
        
        JFrame frame = new JFrame();
        frame.setSize(456, 526);
        frame.add(new DagsrapportPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
