package no.mesta.mipss.mipssmap.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JDialog;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.klimadata.yr.WeatherChart;
import no.mesta.mipss.klimadata.yr.WeatherData;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

import org.jdesktop.swingx.mapviewer.GeoPosition;

public class HentYrDataTask extends MipssMapTask{

	private MipssMapModule plugin;
	private WeatherChart weatherPanel;
	private GeoPosition pos;
	private JDialog dialog;
	public HentYrDataTask(MipssMapModule plugin, GeoPosition pos, JDialog dialog){
		super(ExecutorTaskType.PARALELL);
		this.plugin = plugin;
		this.pos = pos;
		this.dialog = dialog;
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		List<WeatherData> weatherList = plugin.getKlimadataBean().getWeatherDataFromYr(pos.getLatitude(), pos.getLongitude());
		
		Date now = Clock.now();//CalendarUtil.getHoursAgo(Clock.now(), 1);
		List<WeatherData> cropped = new ArrayList<WeatherData>();
		for (WeatherData d:weatherList){
			if (d.getDateFrom().after(now)){
				cropped.add(d);
			}
		}
		weatherPanel = new WeatherChart(dialog, cropped);
		return null;
	}
	
	public WeatherChart getWeatherChart(){
		return weatherPanel;
	}

}
