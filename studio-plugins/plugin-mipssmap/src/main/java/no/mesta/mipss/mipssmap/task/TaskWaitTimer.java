package no.mesta.mipss.mipssmap.task;


import javax.swing.JProgressBar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * En timer som setter en progressbar indeterminate når en angitt tid har gått
 * @author Harald A. Kulø
 */
public class TaskWaitTimer implements Runnable{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private int maxWait = 2500;
    private int wait = 500;
    private boolean running=true;
    private int count = 0;
    
    private JProgressBar bar;
    public TaskWaitTimer(JProgressBar bar){
        this.bar = bar;
    }
    
    public void run(){
        synchronized(this){             
            while (running){
                try{
                    Thread.sleep(500);
                }catch (InterruptedException e){
                }
                count+=wait;
                if (count==maxWait&&running){
                    bar.setIndeterminate(true);
                    running = false;
                    bar.setString("Beklager ventetiden..");
                }
            }
        }
    }
    public void stopRunning(){
        running = false;
        bar.setIndeterminate(false);
    }
}

