package no.mesta.mipss.mipssmap.plugins.layer.vaerstasjon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

import javax.swing.JCheckBox;
import javax.swing.JDialog;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.task.HentVaerstasjonerTask;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Plugin som legger til funksjonalitet for å se værstasjoner i kart
 * samt hente data fra værstasjonene.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class VaerstasjonPlugin implements MipssMapLayerPlugin{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssMapText");

	private Logger log = LoggerFactory.getLogger(this.getClass());
	private MipssMapWindowManager windowManager;
    private MipssMapModule plugin;
    private JCheckButton button = new JCheckButton(ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.VaerstasjonPlugin.button.text"));

	private boolean loaded;
    
	public VaerstasjonPlugin(MipssMapWindowManager windowManager, MipssMapModule plugin) {
		this.windowManager = windowManager;
		this.plugin = plugin;
		getCheckButton().getButton().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                button_actionPerformed(e);
            }
        }); 
		getCheckButton().getCheckBox().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                check_actionPerformed(e);
            }
        });
        button.getButton().setToolTipText(resources.getGuiString("grunndata.vaerstasjoner.tooltip"));
        button.getCheckBox().setToolTipText(resources.getGuiString("grunndata.vis.vaerstasjoner.tooltip"));
	}

	/**
	 * Actionmetode for når brukeren klikker på checkboxen til layeret
	 * skjuler eller setter vaerstajonene synlige i kartet.
	 * @param e
	 */
    protected void check_actionPerformed(ActionEvent e) {
    	if (!loaded){
    		loaded = true;
    		button.getCheckBox().setSelected(false);
    		button_actionPerformed(null);
    	}else{
	    	JCheckBox check = (JCheckBox)e.getSource();
	        boolean visible = check.isSelected();
	        if (visible)
	    		plugin.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.VAERSTASJON.getType());
	    	else
	    		plugin.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.VAERSTASJON.getType());
    	}
		
	}

	protected void button_actionPerformed(ActionEvent e) {
		JDialog dialog = windowManager.getDialog(VaerstasjonDialog.class);
		if (dialog==null){
			dialog = new VaerstasjonDialog(plugin.getLoader().getApplicationFrame(), this, plugin);
			dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.VaerstasjonDialog.title"));
	        dialog.setSize(300, 200);
	        dialog.setLocationRelativeTo(null);
//	        dialog.setResizable(false);
			windowManager.addDialog(dialog);
		}
		
		dialog.setVisible(true);
	}
	
	public void createVaerstasjoner(){
		
		final HentVaerstasjonerTask task = new HentVaerstasjonerTask(plugin);
		
        ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.VaerstasjonPlugin.HentVaerstasjonerTask.title"));
        workerPanel.setWorker(task);
        task.setProgressController(plugin.getProgressController());
        task.setProgressWorkerPanel(workerPanel);
        plugin.getProgressController().addWorkerPanel(workerPanel);
		
	    log.debug("Henter værstasjoner fra eklima");
//		final HentVaerstasjonerTask task = new HentVaerstasjonerTask(plugin);
		task.execute();

		Thread wait = new Thread() {
			public void run() {
				Layer layer = null;
				try {
					log.debug("venter på webservice");
					layer = task.get();
					log.debug("layeret er laget, legger til i layerhandler..");
					if (task.isNoDataFound()) {
//						plugin.getStatusPanel().getProgressBar().setString("No data found");
						log.debug("sorry, ingen data funnet");
					} else if (layer != null) {
						plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.VAERSTASJON.getType(), layer, true);
						getCheckButton().setChecked(true);
					}
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		log.debug("starter ventetråden for å legge til lag");
		wait.start();
	}

	public JCheckButton getCheckButton() {        
		button.setEnabled(false);
        return button;
    }

	@Override
	public void reset() {
		button.getCheckBox().setSelected(false);
		loaded = false;
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		
	}
}
