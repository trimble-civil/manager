package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.mipssmapserver.KjoretoyVO;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.ui.KjoretoySokPanel;
import no.mesta.mipss.ui.table.KjoretoyTableObject;

import org.jdesktop.beansbinding.Binding;

public class ProduksjonPanelNew extends JPanel{

	private MipssMapModule module;
	private ProduksjonstyperPanel produksjonstyper;
	private KjoretoySokPanel kjoretoy;
	private KvalitetsPanel kvalitet;
	private ProduksjonPlugin plugin;
	private JTabbedPane tab;
	private JButton okButton;
	private JButton cancelButton;
	private JButton applyButton;
	
	public ProduksjonPanelNew(MipssMapModule module, ProduksjonPlugin plugin){
		this.module = module;
		this.plugin = plugin;
		setLayout(new BorderLayout());
		initGui();
	}
	
	private void initGui(){
		tab = new JTabbedPane();
		produksjonstyper = new ProduksjonstyperPanel(plugin);
		boolean erAdmin = UserUtils.isAdmin(module.getLoader().getLoggedOnUser(false));
		
		kjoretoy = new KjoretoySokPanel(module.getCurrentKontrakt(), erAdmin);
		kjoretoy.getKunKontraktRadio().setSelected(true);
		Binding binding = BindingHelper.createbinding(module, "currentKontrakt", kjoretoy, "valgtKontrakt");
		binding.bind();
		
		kvalitet = new KvalitetsPanel(plugin);
		tab.addTab(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanelNew.tab.typer.title"), produksjonstyper);
		tab.addTab(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanelNew.tab.kjoretoy.title"), kjoretoy);
		tab.addTab(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanelNew.tab.kvalitet.title"), kvalitet);
		add(tab, BorderLayout.CENTER);
		add(createButtonPanel(), BorderLayout.SOUTH);
		
		//kjoretoy.selectAll(true);
		while (!kjoretoy.isLoaded()){
			Thread.yield();
		}
		for (KjoretoyTableObject kto :kjoretoy.getKjoretoyTableModel().getEntities()){
			if (kto.getPda()!=null){
				kto.setValgt(false);
			}else{
				kto.setValgt(true);
			}
		}
	}
	
	private JPanel createButtonPanel(){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		buttonPanel.add(Box.createHorizontalGlue());
		okButton = new JButton(new OkAction(ResourceUtil.getResource("okButton.text"), null));
		cancelButton = new JButton(new CancelAction(ResourceUtil.getResource("avbrytButton.text"), null));
		applyButton = new JButton(new ApplyAction(ResourceUtil.getResource("brukButton.text"), null));
		buttonPanel.add(applyButton);
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(cancelButton);
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(okButton);
		return buttonPanel;
	}
	
	public boolean hentProduksjon(){
        boolean punkter = produksjonstyper.isPunktSelected();
        boolean strekninger = produksjonstyper.isStrekningSelected();
        boolean labels = produksjonstyper.isLabelsSelected();
        
        List<Prodtype> prodtypeList = null;
        prodtypeList = produksjonstyper.getSelectedProdtyper();
        
        List<KjoretoyVO> kjoretoyList = kjoretoy.getSelectedKjoretoy();
        ProduksjonCriteria criteria = new ProduksjonCriteria();
        criteria.setEnhetList(kjoretoyList);
        criteria.setLabels(labels);
        criteria.setPunkter(punkter);
        criteria.setKunSiste(produksjonstyper.isKunSisteSelected());
        criteria.setStrekninger(strekninger);
        criteria.setProdtyper(prodtypeList);
        criteria.setAllProduksjon(produksjonstyper.isAllProdSelected());
        criteria.setRmax(kvalitet.getRtil());
        criteria.setRmin(kvalitet.getRfra());
        criteria.setYmax(kvalitet.getYtil());
        criteria.setYmin(kvalitet.getYfra());
        criteria.setGmax(kvalitet.getGtil());
        criteria.setGmin(kvalitet.getGfra());
        criteria.setPdop(kvalitet.isPDOP());
        if (kvalitet.isDfu()){
        	criteria.setDfuId(kvalitet.getDfuId());
        }else{
        	criteria.setDfuId(null);
        }
        criteria.setRedVisible(kvalitet.isRedVisible());
        criteria.setYellowVisible(kvalitet.isYellowVisible());
        criteria.setGreenVisible(kvalitet.isGreenVisible());
        criteria.setGrunndata(kvalitet.isGrunndata());
        return plugin.hentProduksjon(criteria);
	}
	
	public boolean hentSVVProduksjon() {
		boolean punkter = produksjonstyper.isPunktSelected();
        
        List<Prodtype> prodtypeList = null;
        prodtypeList = produksjonstyper.getSelectedProdtyper();
        
        MipssMap bean = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
		List<KjoretoyVO> kjoretoyList = bean.getKjoretoyPdaForKontrakt(module.getCurrentKontrakt().getId());
        ProduksjonCriteria criteria = new ProduksjonCriteria();
        criteria.setEnhetList(kjoretoyList);
        criteria.setPunkter(punkter);
        criteria.setProdtyper(prodtypeList);
        criteria.setAllProduksjon(produksjonstyper.isAllProdSelected());
        return plugin.hentProduksjon(criteria);	
	}
	
	public boolean isTidsvinduSelected(){
		return produksjonstyper.isTidsvinduSelected();
	}
	public List<KjoretoyVO> getSelectedKjoretoy(){
		return kjoretoy.getSelectedKjoretoy();
	}
	public void setSanntidMode(){
		if (tab.getSelectedIndex()==2){
			tab.setSelectedIndex(1);
		}
		kvalitet.setVanligRadio(Boolean.TRUE);
		tab.setEnabledAt(2, false);
		
		produksjonstyper.setSanntidMode();
		
		kjoretoy.setSanntidMode();
		
		okButton.setEnabled(Boolean.FALSE);
		cancelButton.setEnabled(Boolean.FALSE);
		applyButton.setEnabled(Boolean.FALSE);
	}
	
	public void setPeriodeMode(){
		kvalitet.setVanligRadio(Boolean.FALSE);
		tab.setEnabledAt(2, Boolean.TRUE);
		
		produksjonstyper.setPeriodeMode();
		kjoretoy.setPeriodeMode();
		
		okButton.setEnabled(Boolean.TRUE);
		cancelButton.setEnabled(Boolean.TRUE);
		applyButton.setEnabled(Boolean.TRUE);
	}
	private class OkAction extends AbstractAction{
		public OkAction(String text, ImageIcon icon){
			super(text, icon);
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Container parent = ProduksjonPanelNew.this;
	        while (!(parent instanceof JDialog)){
	            parent = parent.getParent();
	        }
	        
	        if (hentProduksjon())
	        	((JDialog)parent).setVisible(Boolean.FALSE);
		}
	}
	
	private class CancelAction extends AbstractAction{
		public CancelAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Container parent = ProduksjonPanelNew.this;
	        while (!(parent instanceof JDialog)){
	            parent = parent.getParent();
	        }
	        ((JDialog)parent).setVisible(false);
		}
	}
	private class ApplyAction extends AbstractAction{
		public ApplyAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			hentProduksjon();
			
		}
	}
}
