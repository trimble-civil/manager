package no.mesta.mipss.mipssmap.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.resources.images.IconResources;

/**
 * Klasse for å lage en knapp med en tilhørende checkbox
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */

public class JCheckButton extends JPanel{
    private JCheckBox checkBox;
    private JButton button;
//    private JLabel icon;
    private JButton iconBtn;
    private Dimension iconSize = new Dimension(22,22);
    /**
     * Konstruktør
     * @param buttonText teksten på knappen
     */
    public JCheckButton(String buttonText) {
    
        checkBox = new JCheckBox();
        button = new JButton(buttonText);
        iconBtn = new JButton(IconResources.REFRESH_VIEW_ICON);
        iconBtn.setVisible(false);
        iconBtn.setMaximumSize(iconSize);
        iconBtn.setMinimumSize(iconSize);
        iconBtn.setPreferredSize(iconSize);
//        icon = new JLabel(IconResources.REFRESH_VIEW_ICON);
//        icon.setVisible(false);
//        icon.setOpaque(true);
//        icon.addMouseListener(new MouseAdapter(){
//			@Override
//			public void mouseEntered(MouseEvent e) {
//				icon.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
//			}
//			@Override
//			public void mouseExited(MouseEvent e) {
//				icon.setBorder(null); 
//			}
//			@Override
//			public void mousePressed(MouseEvent e) {
//				icon.setBackground(icon.getBackground().darker());
//			}
//			@Override
//			public void mouseReleased(MouseEvent e) {
//				icon.setBackground(icon.getBackground().brighter());
//			}
//        });
        initGui();
    }
    /**
     * Konstruktør
     * @param buttonText teksten på knappen
     */
    public JCheckButton(String buttonText, Action action) {
    
        checkBox = new JCheckBox();
        button = new JButton(buttonText);
//        icon = new JLabel(IconResources.REFRESH_VIEW_ICON);
//        icon.setVisible(false);
//        icon.setOpaque(true);
        
        iconBtn = new JButton(IconResources.REFRESH_VIEW_ICON);
        iconBtn.setVisible(false);
        iconBtn.setMaximumSize(iconSize);
        iconBtn.setMinimumSize(iconSize);
        iconBtn.setPreferredSize(iconSize);
//        setListener(action);
        initGui();
    }
    
    public void setAction(Action action){
    	iconBtn.setAction(action);
    }
    public void setIconTooltip(String tooltip){
    	iconBtn.setToolTipText(tooltip);
    }
//    private void setListener(final Action action){
//    	for (MouseListener ml:icon.getMouseListeners()){
//    		icon.removeMouseListener(ml);
//    	}
//    	if (action!=null){
//	    	icon.addMouseListener(new MouseAdapter(){
//				@Override
//				public void mouseEntered(MouseEvent e) {
//					icon.setBorder(BorderFactory.createLineBorder(Color.gray));
//				}
//				@Override
//				public void mouseExited(MouseEvent e) {
//					icon.setBorder(null); 
//				}
//				@Override
//				public void mousePressed(MouseEvent e) {
//					icon.setBackground(icon.getBackground().darker());
//				}
//				@Override
//				public void mouseReleased(MouseEvent e) {
//					icon.setBackground(icon.getBackground().brighter());
//				}
//				@Override
//				public void mouseClicked(MouseEvent e){
//					ActionEvent ev = new ActionEvent(JCheckButton.this, e.getID(), null);
//					action.actionPerformed(ev);
//				}
//	        });
//    	}
//    }
    public void setIconVisible(boolean visible){
    	iconBtn.setVisible(visible);
    }
    
    public void setIcon(ImageIcon icon){
    	this.iconBtn.setIcon(icon);
    }
    

    
    private void initGui(){
        this.removeAll();
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        Dimension buttonSize = new Dimension(120, 25);
        button.setPreferredSize(buttonSize);
        button.setMinimumSize(buttonSize);
        button.setMaximumSize(buttonSize);
        
        add(Box.createRigidArea(new Dimension(10, 0)));
        add(checkBox);
        add(Box.createRigidArea(new Dimension(5, 0)));
        add(button);
        add(Box.createRigidArea(new Dimension(9, 0)));
        add(iconBtn);
        add(Box.createHorizontalGlue());
    }
    /**
     * Setter checkboxen
     * @param checkBox
     */
    public void setCheckBox(JCheckBox checkBox) {
        this.checkBox = checkBox;
    }
    
    /**
     * Returnerer checkboxen
     * @return
     */
    public JCheckBox getCheckBox() {
        return checkBox;
    }
    
    /**
     * Setter knappen
     * @param button
     */
    public void setButton(JButton button) {
        this.button = button;
    }
    
    /**
     * Returnerer knappen
     * @return
     */
    public JButton getButton() {
        return button;
    }
    
    public void setEnabled(boolean enabled){
        getButton().setEnabled(enabled);
        getCheckBox().setEnabled(enabled);
        iconBtn.setEnabled(enabled);
    }
    
    public void setChecked(boolean checked){
        getCheckBox().setSelected(checked);
    }
}
