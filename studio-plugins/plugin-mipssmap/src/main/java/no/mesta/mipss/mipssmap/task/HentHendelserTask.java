package no.mesta.mipss.mipssmap.task;

import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mipssfelt.RskjemaQueryParams;
import no.mesta.mipss.mipssfelt.RskjemaService;
import no.mesta.mipss.mipssfelt.RskjemaSokResult;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

public class HentHendelserTask extends MipssMapTask{
	
	private RskjemaQueryParams filter;
	private MipssMapModule plugin;
	private final Long[] aarsakId;
	
	public HentHendelserTask(MipssMapModule plugin, ExecutorTaskType taskType, RskjemaQueryParams filter, Long[] aarsakId) {
		super(taskType);
		this.plugin = plugin;
		this.filter = filter;
		this.aarsakId = aarsakId;
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
		RskjemaService bean = BeanUtil.lookup(RskjemaService.BEAN_NAME, RskjemaService.class);
		List<RskjemaSokResult> hendelser = bean.sokHendelse(filter.getDateFra(), filter.getDateTil(), filter.getValgtKontrakt(), aarsakId);

		
		LayerBuilder b = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
		return b.getPointLayerCollectionFromHendelser(hendelser);
	}
}
