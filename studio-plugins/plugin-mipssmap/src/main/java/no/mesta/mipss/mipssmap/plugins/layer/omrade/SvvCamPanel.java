package no.mesta.mipss.mipssmap.plugins.layer.omrade;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXHeader;

import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.resources.images.IconResources;

@SuppressWarnings("serial")
public class SvvCamPanel extends JPanel {

	private OmradePlugin plugin;
	private MipssMapModule module;
	public SvvCamPanel(OmradePlugin plugin, MipssMapModule module) {
		this.plugin = plugin;
		this.module = module;
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		JButton hentStasjonerButton = new JButton(ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.SvvCamPanel.hentStasjonerButton.text"));
		
		hentStasjonerButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				hentStasjonerButton_actionPerformed(e);
			}
		});
		add(getHeader());
		add(Box.createVerticalStrut(20));
		add(Box.createVerticalGlue());
		add(hentStasjonerButton);
		add(Box.createVerticalStrut(20));
		
	}
	private JXHeader getHeader(){
		String title = ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.SvvCamPanel.header.title");
		String description = ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.SvvCamPanel.header.description");
		JXHeader header = new JXHeader();
		header.setIcon(IconResources.WEBCAM_ICON_32);
		header.setTitle(title);
		header.setDescription(description);
		return header;
	}
	protected void hentStasjonerButton_actionPerformed(ActionEvent e) {
//		if (module.getMainMap().getLayerHandler().getLayers(MipssMapLayerType.WEBCAM.getType())==null)
			plugin.createWebcams(); 
		
	}
}
