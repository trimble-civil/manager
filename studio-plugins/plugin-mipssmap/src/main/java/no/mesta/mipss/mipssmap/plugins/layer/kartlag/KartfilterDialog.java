package no.mesta.mipss.mipssmap.plugins.layer.kartlag;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Window;

import java.util.PropertyResourceBundle;

import javax.swing.JDialog;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeListener;


/**
 * Valgdialog for visning av kartlag
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class KartfilterDialog extends JDialog{
    
    private Color valgtFarge;
    private Color forrigeFarge;
    private float mix=1f;
    private float forrigeMix;
    private KartFilterPanel filterPanel;
    
    /**
     * Konstruktør
     * @param owner eieren av dialogen
     * @param forrigeFarge den forrige filterfargen
     * @param forrigeMix den forrige mixverdien
     */
    public KartfilterDialog(Window owner, Color forrigeFarge, float forrigeMix) {
        super(owner);
        
        if (forrigeFarge==null)
            forrigeFarge = Color.white;
            
        this.forrigeFarge = forrigeFarge;
        this.forrigeMix = forrigeMix;
        this.valgtFarge = forrigeFarge;
        this.mix = forrigeMix;
        initGUI();
    }
    
    private void initGUI() {
        filterPanel = new KartFilterPanel(this, forrigeFarge, forrigeMix);
        setLayout(new BorderLayout());
        add(filterPanel);
    }
    
    public void addChangeListener(ChangeListener l) {
        filterPanel.addChangeListener(l);
    }
   
}
