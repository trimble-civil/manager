package no.mesta.mipss.mipssmap.task;

import java.awt.Color;
import java.awt.Point;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.CollectionPainter;
import no.mesta.mipss.mapplugin.layer.FeltLayer;
import no.mesta.mipss.mapplugin.layer.LabelLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayerCollection;
import no.mesta.mipss.mapplugin.layer.PeriodLineListLayer;
import no.mesta.mipss.mapplugin.layer.PinLayer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.textboxdata.InspeksjonTextboxProducer;
import no.mesta.mipss.mipssfelt.InspeksjonQueryParams;
import no.mesta.mipss.mipssfelt.InspeksjonService;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.KonfigparamCache;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.service.produksjon.ProdReflinkStrekning;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.service.produksjon.PunktregVO;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;
import no.mesta.mipss.webservice.ReflinkStrekning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HentInspeksjonTask extends MipssMapTask{
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private InspeksjonSokResult view;
	private final InspeksjonQueryParams params;
	private final MipssMapModule plugin;
	private LabelLayer label;
	private	Layer pointLayer;
	private List<FeltLayer> avvikLayer;
	private List<FeltLayer> hendelseLayer;
	private List<FeltLayer> forsikringsskadeLayer;
	private List<FeltLayer> skredLayer;
	private final Map<MipssGeoPosition, FeltLayer> funnmap;
	private ProduksjonService produksjon;
	private String guid;
	
	public HentInspeksjonTask(MipssMapModule plugin, InspeksjonQueryParams params, InspeksjonSokResult view, Map<MipssGeoPosition, FeltLayer> funnmap){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.params = params;
		this.view = view;
		this.funnmap = funnmap;
	}
	public HentInspeksjonTask(MipssMapModule plugin, InspeksjonQueryParams params, String guid, Map<MipssGeoPosition, FeltLayer> funnmap){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.params = params;
		this.guid = guid;
		this.funnmap = funnmap;
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		getProgressWorkerPanel().setBarIndeterminate(true);
    	getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
    	
    	InspeksjonService inspeksjonService = BeanUtil.lookup(InspeksjonService.BEAN_NAME, InspeksjonService.class);
    	produksjon = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
    	Inspeksjon inspeksjon = inspeksjonService.hentInspeksjon(view.getGuid());
    	
    	Color defaultInspeksjonColor = new Color(100,100,255);
    	LayerBuilder builder = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
    	
    	String guid = view==null?this.guid:view.getGuid();
    	LineListLayerCollection layer =null;
    	if (params.isHentStrekninger()){
	    	List<ProdReflinkStrekning> strekninger = produksjon.hentInspeksjonStrekninger(guid, params);
	    	if (strekninger!=null){
		    	layer = createInspeksjonLayer(strekninger, view, guid, builder, defaultInspeksjonColor);
		    	if (layer!=null&&!params.isHentPunkter()){
		    		List<ReflinkStrekning> strekningList = strekninger.get(strekninger.size()-1).getStrekningList();
			    	Point[] v = strekningList.get(strekningList.size()-1).getVectors();
			    	MipssGeoPosition labelPos = new MipssGeoPosition(v[0].getY(), v[0].getX());
			    	label = createLabel(inspeksjon, labelPos);
		    	}
	    	}
    	}
    	if (params.isHentPunkter()){
	    	List<ProdpunktVO> inspeksjonPunkter = produksjon.hentInspeksjonPunkter(guid, params.isKunSistePunkt());
	    	PointLayerCollection pl = builder.getInspeksjonLayerFromProdpunkVO(inspeksjonPunkter, defaultInspeksjonColor);
	    	if (pl.getLayers().size()>0){
		    	ProdpunktPointLayer point = (ProdpunktPointLayer)pl.getLayers().get(pl.getLayers().size()-1);
		    	label = createLabel(inspeksjon, point.getPoint());
		    	if (pl.getLayers().size()==1){
		    		pointLayer = point; 
		    		((ProdpunktPointLayer)pointLayer).setInitGraphics(true);
		    	}else
		    		pointLayer = pl;
	    	}
    	}
    	if (params.isHentHendelser()){
    		List<PunktregVO> hendelser = produksjon.hentHendelseFraInspeksjon(guid);
    		hendelseLayer = builder.getLayersFromPunktregsFromInspeksjon(hendelser, funnmap);
    	}
    	if (params.isHentAvvik()){
    		List<PunktregVO> funn = produksjon.hentAvvikFraInspeksjon(guid);
    		avvikLayer = builder.getLayersFromPunktregsFromInspeksjon(funn, funnmap);
    	}
    	if (params.isHentForsikringsskade()){
    		List<PunktregVO> funn = produksjon.hentForsikringsskadeFraInspeksjon(guid);
    		forsikringsskadeLayer = builder.getLayersFromPunktregsFromInspeksjon(funn, funnmap);
    	}
    	if (params.isHentSkred()){
    		List<PunktregVO> funn = produksjon.hentSkredFraInspeksjon(guid);
    		skredLayer = builder.getLayersFromPunktregsFromInspeksjon(funn, funnmap);
    	}
    	
		return layer;
	}
	public LabelLayer getLabel(){
		return label;
	}
	public Layer getPointLayer(){
		return pointLayer;
	}
	
	private LineListLayerCollection createInspeksjonLayer(List<ProdReflinkStrekning> strekninger, InspeksjonSokResult insp, String guid, LayerBuilder layerBuilder, Color defaultInspeksjonColor){
		Bounds bounds = new Bounds();
		List<PeriodLineListLayer> layerList = layerBuilder.createPeriodLineListLayerFromProdReflinkStrekning(insp.getGuid(), strekninger, bounds, false, defaultInspeksjonColor);
		LineListLayerCollection layer = new LineListLayerCollection(guid, layerList, bounds, false);
		layer.setTextboxProducer(new InspeksjonTextboxProducer(plugin.getLoader(), insp, guid));
		layer.setReturnThisOnLayerCloseTo(true);
    	if (layer.getBounds().isNull()){
    		log.debug("layerbounds is null..");
    		return null;
    	}
    	layer.setColor(defaultInspeksjonColor);
    	
    	layer.setCollectionPainter(new CollectionPainter(layer.getLayers(), layer));
    	return layer;
	}
	/**
	 * Lager en label til inspeksjonen
	 * @param i
	 * @param pos
	 * @return
	 */
	private LabelLayer createLabel(Inspeksjon i, MipssGeoPosition pos){
		String title = "";
		if (i.getPdaDfuIdent()!=null){
			title ="("+i.getPdaDfuIdent()+")";
		}else{
			title = "(ukjent id)";
		}
		
		String kosenavn = produksjon.getKosenavnForPda(i.getPdaDfuIdent(), i.getKontraktId());
		if (kosenavn!=null){
			title += " "+kosenavn;
		}else{
			title += " ukjent";
		}
			
		String[] text = new String[]{
				title, 
				MipssDateFormatter.formatDate(i.getFraTidspunkt(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT)
		};
		String v1 = KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_INSPEKSJON);
		String v2= KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_TEXT_INSPEKSJON);
		Color fill = ColorUtil.setAlpha(ColorUtil.fromHexString(v1), 140);
		Color border = ColorUtil.fromHexString(v1);
		Color textColor = ColorUtil.fromHexString(v2);
		LabelLayer l =  new LabelLayer("label"+i.getGuid(), pos, text, fill, border, textColor);
		l.setPreferredLayerType(MipssMapLayerType.INSPEKSJONLABEL.getType());
		return l;
	}
	
	public List<FeltLayer> getAvvikLayer() {
		return avvikLayer;
	}
	public List<FeltLayer> getHendelseLayer(){
		return hendelseLayer;
	}
	public List<FeltLayer> getForsiringsskadeLayer(){
		return forsikringsskadeLayer;
	}
	public List<FeltLayer> getSkredLayer(){
		return skredLayer;
	}
	
}
