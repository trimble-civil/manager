package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

public class ProsessTableObject implements CheckBoxTableObject{

	private Prosess prosess;
	private boolean valgt;
	
	public ProsessTableObject(Prosess prosess){
		this.prosess = prosess;
	}
	/**
	 * @return the prosess
	 */
	public Prosess getProsess() {
		return prosess;
	}

	/**
	 * @param prosess the prosess to set
	 */
	public void setProsess(Prosess prosess) {
		this.prosess = prosess;
	}

	/**
	 * @return the valgt
	 */
	public Boolean getValgt() {
		return valgt;
	}
	/**
	 * @param valgt the valgt to set
	 */
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
	}
	public String getProsessKode(){
		return prosess.getProsessKode();
	}
	
	public String getNavn(){
		return  prosess.getProsessKode()+" "+prosess.getNavn();
	}
	

	@Override
	public String getTextForGUI() {
		return "";
	}
	

}
