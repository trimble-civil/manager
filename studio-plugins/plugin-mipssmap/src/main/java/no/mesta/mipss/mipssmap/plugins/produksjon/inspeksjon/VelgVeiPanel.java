package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.StringLongConverter;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonPanelNew;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.ui.DefaultButtonPanel;
import no.mesta.mipss.ui.VeiAttributtPanel;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

public class VelgVeiPanel extends JPanel{
	private VeiAttributtPanel veiAttributtPanel;
	private VeiInfo veiSok = new VeiInfo();
    private StringLongConverter cfb = BindingHelper.createStringLongConverter();
    private JDialog owner;
	public VelgVeiPanel(JDialog owner){
		this.owner = owner;
		setLayout(new BorderLayout());
		veiAttributtPanel = new VeiAttributtPanel(false);
		initGui();
		bind();
	}
	private void initGui(){
		add(veiAttributtPanel);
		add(DefaultButtonPanel.getDefaultButtonPanel(new OkAction("", null), new CancelAction("", null), new ResetAction(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.VelgVeiPanel.nullstill.text"), null)), BorderLayout.SOUTH);
	}
	
	public static void main(String [] args){
		try {UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); } catch (Exception e){}
		JFrame f = new JFrame("Test av inspeksjonpanel");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(new VelgVeiPanel(null));
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}
	
    private void bind() {
        BindingGroup group = new BindingGroup();
        Binding fb = BindingHelper.createbinding(veiAttributtPanel.getFylkeField(), "text", veiSok, "fylkesnummer");
        fb.setConverter(cfb);
        group.addBinding(fb);
        
        Binding kb = BindingHelper.createbinding(veiAttributtPanel.getKommuneField(), "text", veiSok, "kommunenummer");
        kb.setConverter(cfb);
        group.addBinding(kb);

        Binding vkb = BindingHelper.createbinding(veiAttributtPanel.getKategoriField(), "text", veiSok, "veikategori");
        group.addBinding(vkb);
        
        Binding vsb = BindingHelper.createbinding(veiAttributtPanel.getStatusField(), "text", veiSok, "veistatus");
        group.addBinding(vsb);

        Binding vnb = BindingHelper.createbinding(veiAttributtPanel.getNummerField(), "text", veiSok, "veinummer");
        vnb.setConverter(cfb);
        group.addBinding(vnb);

        Binding hb = BindingHelper.createbinding(veiAttributtPanel.getHpField(), "text", veiSok, "hp");
        hb.setConverter(cfb);
        group.addBinding(hb);
        
        group.bind();
    }
    
    public VeiInfo getVeiSok() {
        return veiSok;
    }
    /**
     * Actionklasse for ok knappen
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    private class OkAction extends AbstractAction{
		public OkAction(String text, ImageIcon icon){
			super(text, icon);
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			owner.setVisible(false);
		}
	}
	
    /**
     * Actionklasse for nullstill knappen
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
	private class ResetAction extends AbstractAction{
		public ResetAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			veiAttributtPanel.clear();
		}
	}
	/**
	 * Actionklasse for avbrytknappen
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	private class CancelAction extends AbstractAction{
		public CancelAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Container parent = VelgVeiPanel.this;
	        while (!(parent instanceof JDialog)){
	            parent = parent.getParent();
	        }
			veiAttributtPanel.clear();
			owner.setVisible(false);
		}
	}
}
