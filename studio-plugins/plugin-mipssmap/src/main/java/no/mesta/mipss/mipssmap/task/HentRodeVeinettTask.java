package no.mesta.mipss.mipssmap.task;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

/**
 * Worker som henter 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class HentRodeVeinettTask extends MipssMapTask{

	private MipssMapModule plugin;
	private Rode rode;
	
	public HentRodeVeinettTask(MipssMapModule plugin, Rode rode){
		super(ExecutorTaskType.PARALELL);//QUEUE??
		this.plugin = plugin;
		this.rode = rode;
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
			return null;
		}
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		VeinettService veinettService = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
		Veinett veinettForRode = veinettService.getVeinettForRode(rode);
		return new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).getLayerFromRode(rode, veinettForRode);
	}

}
