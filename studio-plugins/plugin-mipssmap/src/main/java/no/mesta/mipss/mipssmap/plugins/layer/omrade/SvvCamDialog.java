package no.mesta.mipss.mipssmap.plugins.layer.omrade;

import java.awt.BorderLayout;
import java.awt.Window;

import javax.swing.JDialog;

import no.mesta.mipss.mipssmap.MipssMapModule;

public class SvvCamDialog extends JDialog{
	public SvvCamDialog(Window owner, OmradePlugin plugin, MipssMapModule module){
		super(owner);
		initGui(plugin, module);
	}
	
	private void initGui(OmradePlugin plugin, MipssMapModule module){
		setLayout(new BorderLayout());
		SvvCamPanel camPanel = new SvvCamPanel(plugin, module);
        add(camPanel);
	}
}
