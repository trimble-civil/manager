package no.mesta.mipss.mipssmap.swing.treetable;



import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;
/**
 * 
 * @author Harald A. Kulø
 *
 */
public class CheckButtonNode extends DefaultMutableTreeTableNode {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private PropertyChangeSupport props = new PropertyChangeSupport(this);
    
	private List<CheckButtonNode> childs = new ArrayList<CheckButtonNode>();
	private CheckButtonNode parent;
	private String text;
	private Image icon;
	private JButton button;
	private boolean selected;
	private boolean prevSelected;
	
	private String defaultFeilmelding;
	
	private Object instance;
	private String method;
	private Object[] params;
	
	private String restoreMethod;
	

	//	}
	/**
	 * Creates a new instance of PropertyNode
	 * this constructor is used when we need a root node
	 *
	 */
	public CheckButtonNode(String text){
		this.text = text;
	}
	
	public CheckButtonNode(String text, JButton button){
		this.text = text;
		this.button = button;
	}

	/**
	 * Get this nodes children as a list of other PropertyNodes
	 * @return childs
	 */
	public List<CheckButtonNode> getChilds() {
		return childs;
	}
	
	public List<CheckButtonNode> getAllChilds(){
		List<CheckButtonNode> nodes = new ArrayList<CheckButtonNode>();
		for (CheckButtonNode n:childs){
			if (n.getAllChilds().size()>0)
				nodes.addAll(n.getAllChilds());
		}
		return nodes;
	}
	
	/**
	 * Add a child to this PropertyNode
	 * @param child the child to add
	 */
	public void add(CheckButtonNode child){
		childs.add(child);
		child.setParent(this);
	}
	
	/**
	 * Get the child at position index from this PropertyNode 
	 */
	public TreeTableNode getChildAt(int index){
		return childs.get(index);
	}
	/**
	 * Get this PropertyNode's number of children
	 */
	public int getChildCount(){
		return childs.size();
	}
	/**
	 * Set this PropertyNode's parent
	 * @param parent
	 */
	public void setParent(CheckButtonNode parent){
		this.parent = parent;
	}
	/**
	 * Get this PropertyNode's parent
	 */
	public CheckButtonNode getParent(){
		return parent;
	}
	
	/**
	 * Get the index of specified node
	 * @param node the node to find the index on
	 * @return index
	 */
	public int getIndex(CheckButtonNode node){
		return childs.indexOf(node);
	}
	
	/**
	 * Check to see if this PropertyNode is equal to the specified Object
	 * returns true if o.getTooltip equals this.getTooltip
	 */
	public boolean equals(Object o){
		if (o==null||!(o instanceof CheckButtonNode))
			return false;
		CheckButtonNode n = (CheckButtonNode) o;
		
		return this.text==n.getText();
	}
	
	public String getTooltip(){
		return "";
	}
	
	public JButton getButton(){
		return button;
	}
	public String getText(){
		return text;
	}
	
	public Image getIcon(){
		return icon;
	}
	public void setIcon(Image icon){
		this.icon = icon;
	}
	
	/**
	 * @param button the button to set
	 */
	public void setButton(JButton button) {
		this.button = button;
	}
	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		boolean old = this.selected;
		prevSelected = old;
        this.selected = selected;
        props.firePropertyChange("selected", old, selected);
	}
	
	/**
	 * Setter alle child-noder til selected. 
	 * @param selected
	 */
	public void setAllSelected(boolean selected){
		setSelected(selected);
		for (CheckButtonNode n:childs){
			n.setAllSelected(selected);
		}
	}
	/**
	 * Setter alle valg tilbake til det de var forrige gang
	 */
	public void restoreSelected(){
		selected = prevSelected;
	}
	
	public void restoreAllSelected(){
		restoreSelected();
		for (CheckButtonNode n : childs){
			n.restoreAllSelected();
		}
	}
	/**
	 * @return the instance
	 */
	public Object getInstance() {
		return instance;
	}

	/**
	 * @param instance the instance to set
	 */
	public void setInstance(Object instance) {
		this.instance = instance;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @return the params
	 */
	public Object[] getParams() {
		return params;
	}

	/**
	 * @param params the params to set
	 */
	public void setParams(Object[] params) {
		this.params = params;
	}

	/**
	 * @return the defaultFeilmelding
	 */
	public String getDefaultFeilmelding() {
		return defaultFeilmelding;
	}

	/**
	 * @param defaultFeilmelding the defaultFeilmelding to set
	 */
	public void setDefaultFeilmelding(String defaultFeilmelding) {
		this.defaultFeilmelding = defaultFeilmelding;
	}

	/**
	 * @return the restoreMethod
	 */
	public String getRestoreMethod() {
		return restoreMethod;
	}

	/**
	 * @param restoreMethod the restoreMethod to set
	 */
	public void setRestoreMethod(String restoreMethod) {
		this.restoreMethod = restoreMethod;
	}

	/**
	 * The PropertyNode's toString
	 */
	public String toString(){

		return text;
	}
	
	/**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + l + ")");
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + propName + ", " + l + ")");
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + l + ")");
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + propName + ", " + l + ")");
        props.removePropertyChangeListener(propName, l);
    }
	
	
}
