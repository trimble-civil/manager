package no.mesta.mipss.mipssmap.utils;

import java.util.logging.Level;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

public class ErrorHandler {

	public static void showError(Throwable t, String feilmelding){
//		String msg = ResourceUtil.getResource("mipssmap.feilmelding");
        ErrorInfo info = new ErrorInfo("Feil ved henting av data", feilmelding, null, "Advarsel", t, Level.SEVERE, null);
        JXErrorPane.showDialog(null, info);
	}
}
