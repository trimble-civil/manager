package no.mesta.mipss.mipssmap.swing;

import javax.swing.JScrollPane;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;

/**
 * Klasse for å vise en indikator på at data hentes til tabellen
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class TableWaitScroller extends JScrollPane{
	
	private JXBusyLabel busyLabel ;
	private JXTable table;
	
	public TableWaitScroller(JXTable table, String text){
		this.table = table;
		busyLabel = new JXBusyLabel();
		busyLabel.setText(text);
	}
	
	public void setBusy(boolean busy){
		busyLabel.setVisible(busy);
		busyLabel.setBusy(busy);
		table.setVisible(!busy);
		this.setViewportView(busy?busyLabel:table);
		repaint();
	}
}