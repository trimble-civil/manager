package no.mesta.mipss.mipssmap.swing;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LayerHandler;
import no.mesta.mipss.mapplugin.util.LayerEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Harald A. Kulø
 */
public class MipssMapPanel extends MapPanel{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * Opprett ny MipssMapPanel med konfigurasjon
     * @param boundsAndResolutions konfigurasjonen av kartserveren.
     */
    public MipssMapPanel(){
    }
    
    /**
     * Oppdater kartet når et Layer endres. Hvis LayerEventet er triggerRecenterAndZoom
     * skal kartet flyttes og nytt zoomnivå regnes ut. 
     * 
     * @param e eventet 
     */
    public void layerChanged(LayerEvent e) {
        //log.trace("Layer changed "+e);
        if (e.getSource()!=null&&e.getSource() instanceof LayerHandler){
            LayerHandler handler = (LayerHandler)e.getSource();
            Layer l = handler.getLastChangedLayer();
            if (e.isTriggerRecenterAndZoom()){
                Bounds layerBounds = l.getBounds();
                setZoom(calculateZoomForBounds(layerBounds));
                setAddressLocation(calculateCenterForBounds(layerBounds));
                
            }        
        }
        repaint();
    }
    
    
}
