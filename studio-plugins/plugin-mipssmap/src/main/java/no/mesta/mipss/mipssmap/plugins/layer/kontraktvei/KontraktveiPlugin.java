package no.mesta.mipss.mipssmap.plugins.layer.kontraktvei;

import java.awt.Color;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.KontraktveiCollectionLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.layer.LineListLayerCollection;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.task.HentKontraktveinettTask;
import no.mesta.mipss.mipssmap.utils.ErrorHandler;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.VeitypePanel;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;


/**
 * MipssMapLayerPlugin for å vise frem kontraktveinettet
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class KontraktveiPlugin implements MipssMapLayerPlugin{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssMapText");

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private JCheckButton button = new JCheckButton("Kontraktvei");
    
    private MipssMapWindowManager windowManager;
    private MipssMapModule plugin;
    private Binding kontraktveiCheckBinding;
    
    
    public KontraktveiPlugin(MipssMapModule plugin, MipssMapWindowManager windowManager) {
        this.plugin = plugin;
        this.windowManager = windowManager;
        
        button.getCheckBox().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                check_actionPerformed(e);
            }
        });
        button.getButton().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                button_actionPerformed(e);
            }
        });
        button.getButton().setToolTipText(resources.getGuiString("grunndata.kontraktvei.tooltip"));
        button.getCheckBox().setToolTipText(resources.getGuiString("grunndata.vis.kontraktvei.tooltip"));
    }

    public JCheckButton getCheckButton() {
        return button;
    }
    
    
    public void createKontraktveinett(Driftkontrakt kontrakt, boolean veireferanser, final VeitypePanel p){
    	int id = kontrakt.getId().intValue();
        //kontraktveier finnes ikke fra før, hent fra webservice og lag et Layer
        //deretter skal kartet resentreres rundt kontraktens geografiske senter 
        //og riktig zoomnivå skal finnes.
        if (!plugin.getMainMap().getLayerHandler().isLayerPresent(MipssMapLayerType.KONTRAKTVEI.getType(), ""+id)){
        	String navn = kontrakt.getTextForGUI();
        	final HentKontraktveinettTask task = new HentKontraktveinettTask(plugin, kontrakt, veireferanser);
        	ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.layer.kontraktvei.KontraktveiPlugin.HentKontraktveinettTask.title")+navn);
            workerPanel.setWorker(task);
            task.setProgressController(plugin.getProgressController());
            task.setProgressWorkerPanel(workerPanel);
            plugin.getProgressController().addWorkerPanel(workerPanel);
            
            log.debug("Executing task (HentKontraktveinett)");
            Thread wait = new Thread(){ 
                public void run(){
                    Layer layer=null;
                    try {
                        layer = task.get();
                        if (layer!=null){
                        	plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.KONTRAKTVEI.getType(), layer, true);
                        	if (p!=null){
                        		KontraktveiCollectionLayer kl = (KontraktveiCollectionLayer)layer;
                    			kl.showEv(p.isEv());
                    			kl.showRv(p.isRv());
                    			kl.showFv(p.isFv());
                    			kl.showKv(p.isKv());
                    			plugin.getMainMap().getLayerHandler().changeLayer(layer);
                        	}
                        }                     
                    } catch (ExecutionException e) {
                    	e.printStackTrace();
                    	String msg = ResourceUtil.getResource("mipssmap.feilmelding");
                    	ErrorHandler.showError(e, msg);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (CancellationException e){
                    	log.info("Task Cancelled "+e.getMessage());
                    } 
                }
            };
            wait.start();
        }
        //Kontraktveinettlag finnes fra før, gjør det synlig
        if (plugin.getMainMap().getLayerHandler().isLayerPresent(MipssMapLayerType.KONTRAKTVEI.getType(), ""+id)){
        	plugin.getMainMap().getLayerHandler().showLayer(MipssMapLayerType.KONTRAKTVEI.getType(), ""+id);
        }
    }
    
    /**
     * Action-metode for kontraktvei checkbox, ber controlleren om å vise eller skjule lag når den er <code>visible</code>/<code>hidden</code>
     * @param e
     */
    private void check_actionPerformed(ActionEvent e) {
    	plugin.getMainMap().repaint();
    	JCheckBox visVeinett = (JCheckBox)e.getSource();
    	if (visVeinett.isSelected()){
			createKontraktveinett(plugin.getCurrentKontrakt(), false, null);
		}else{
			plugin.getMainMap().getLayerHandler().hideLayer(MipssMapLayerType.KONTRAKTVEI.getType(), plugin.getCurrentKontrakt().getId().intValue()+"");
		}
    }
    
    
    
    /**
     * Action-metode for kontraktvei-knappen, viser gui for å manipulere kontraktveinettets utseende
     * @param e
     */
    private void button_actionPerformed(ActionEvent e){
        //finn en JFrame som kan være owner for den nye dialogen
        Container parent = getCheckButton();
        while (!(parent instanceof Window)){
            parent = parent.getParent();
        }
        
        Layer l=null;
        List<Layer> layers = plugin.getMainMap().getLayerHandler().getLayers(MipssMapLayerType.KONTRAKTVEI.getType());
        if (layers!=null&&layers.size()>0){
        	l = layers.get(0);
        }
        //ingen grunn til å vise frem dialogen hvis vi ikke har et Layer å manipulere
        if (l!=null){
            KontraktveiDialog dialog = (KontraktveiDialog)windowManager.getDialog(KontraktveiDialog.class);
            if (dialog==null){
                dialog = new KontraktveiDialog((Window)parent, l.getColor());
                dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.kontraktvei.KontraktveiDialog.title"));
                //legg til en lytter som oppdaterer kartet når ny farge er valgt
                dialog.addChangeListener(new ChangeListener(){
                    public void stateChanged(ChangeEvent e) {
                    	Layer l=null;
                    	List<Layer> kontraktveiLayer = plugin.getMainMap().getLayerHandler().getLayers(MipssMapLayerType.KONTRAKTVEI.getType());
                        if (kontraktveiLayer!=null&&kontraktveiLayer.size()>0){
                        	l = kontraktveiLayer.get(0);
                        }
                    	if (e.getSource().getClass().equals(KontraktveiPanel.class)){
	                        if (getCheckButton().getCheckBox().isSelected()){
	                            Color farge = ((KontraktveiPanel)e.getSource()).getValgtFarge();
	                            if (l instanceof LineListLayerCollection){
	        						List<? extends CollectionEntity> layers = ((LineListLayerCollection)l).getLayers();
	        						for (CollectionEntity ll:layers){
	        							((Layer)ll).setColor(farge);
	        							ll.getParent().setChanged(true);
	        						}
	        					}else
	        						l.setColor(farge);
	                            plugin.getMainMap().getLayerHandler().changeLayer(l);
	                        }
                        }
                    	if (e.getSource().getClass().equals(VeitypePanel.class)){
                    		VeitypePanel p = (VeitypePanel)e.getSource();
                    		if (l!=null){
	                    		if (l.getClass().equals(KontraktveiCollectionLayer.class)){
	                    			KontraktveiCollectionLayer kl = (KontraktveiCollectionLayer)l;
	                    			kl.showEv(p.isEv());
	                    			kl.showRv(p.isRv());
	                    			kl.showFv(p.isFv());
	                    			kl.showKv(p.isKv());
	                    		}
	                    		else{
	                    			int v = JOptionPane.showConfirmDialog(plugin.getModuleGUI(), ResourceUtil.getResource("henteNyttkontraktveinett"), "Hente kontraktsveinett", JOptionPane.OK_CANCEL_OPTION);
	                    			if (v==JOptionPane.OK_OPTION){
	                					plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.KONTRAKTVEI.getType());
	                					createKontraktveinett(plugin.getCurrentKontrakt(), true, p);
	                    			}
	                    		}
	                    		plugin.getMainMap().getLayerHandler().changeLayer(l);
                    		}
                    	}
                    }
                });
                
                dialog.pack();
                dialog.setLocationRelativeTo(null);
                windowManager.addDialog(dialog);
            }
            
            dialog.setVisible(true);
        }else{
        	showWarningMessage(parent, ResourceUtil.getResource("mipssmap.plugins.layer.kontraktvei.warning.kontraktveinettmissing.message"), ResourceUtil.getResource("mipssmap.plugins.layer.kontraktvei.warning.kontraktveinettmissing.title"));
        }
    }
    private void showWarningMessage(Container parent, String msg, String title){
    	JOptionPane.showMessageDialog(parent, msg, title, JOptionPane.INFORMATION_MESSAGE);
    }
    class ColorChangeListener implements ChangeListener{
        final Driftkontrakt kontrakt = plugin.getCurrentKontrakt();//mipssMapController.getCurrentKontrakt();//(Kundekontrakt)kontraktPanel.getKontraktComboModel().getSelectedItem();
        final Layer layer = plugin.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.KONTRAKTVEI.getType(), ""+kontrakt.getId());
        public void stateChanged(ChangeEvent e) {
            if (getCheckButton().getCheckBox().isSelected()){
                Color farge = ((KontraktveiPanel)e.getSource()).getValgtFarge();
                if (layer instanceof LineListLayer){
                    layer.setColor(farge);
                    plugin.getMainMap().getLayerHandler().changeLayer(layer);
                }
            }
        }
    }
	@Override
	public void reset() {
		button.getCheckBox().setSelected(false);
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		getCheckButton().setEnabled(enabled);
		
	}
}
