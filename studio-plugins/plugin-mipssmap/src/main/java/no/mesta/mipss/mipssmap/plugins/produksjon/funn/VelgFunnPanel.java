package no.mesta.mipss.mipssmap.plugins.produksjon.funn;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssfelt.AvvikQueryParams;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.mipssfield.PunktregQueryFilter;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.task.HentFunnTask;
import no.mesta.mipss.mipssmap.task.MipssMapTask;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JPunktregstatusTypePanel;
import no.mesta.mipss.ui.JTilstandPanel;
import no.mesta.mipss.ui.process.JProcessPanel;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

/**
 * Panel for å gjøre utvalg på Funn i kartet. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class VelgFunnPanel extends ObservasjonPanel {

	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssMapText");
	private JCheckBox visIKartCheck;
	private JLabel visIKartImage;
	
	private JProcessPanel prosessPanel;
	private JTilstandPanel tilstandPanel;
	private JPunktregstatusTypePanel statusPanel;
	private FunnPlugin plugin;
	private MipssMapModule module;
	
	/**
	 * Konstruktør
	 * 
	 * @param module 
	 * @param plugin
	 * @param origIcon
	 * @param tab
	 * @param index
	 */
	public VelgFunnPanel(MipssMapModule module, FunnPlugin plugin, final ImageIcon origIcon, JTabbedPane tab, int index) {
		super(origIcon, tab, index);
		this.module = module;
		this.plugin = plugin;
		initGui();
	}

	private void initGui() {
		setLayout(new BorderLayout());
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.add(getCheckPanel());
		mainPanel.add(getFilterPanel());
		
		add(mainPanel);
	}
	
	/**
	 * Lager panelet som inneholder bilde og checkbox. 
	 * @return
	 */
	private JPanel getCheckPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		visIKartImage = new JLabel(ImageUtil.toGrayScale(getOrigIcon()));
		visIKartCheck = new JCheckBox("Vis i kart");
		visIKartCheck.setSelected(true);
		visIKartCheck.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (visIKartCheck.isSelected()){
					checkSelected();
				}
				else{
					checkNotSelected();
				}
			}
		});
		panel.add(visIKartCheck);
		panel.add(visIKartImage);
		panel.add(Box.createHorizontalGlue());
		
		JPanel vPanel = new JPanel();
		vPanel.setLayout(new BoxLayout(vPanel, BoxLayout.PAGE_AXIS));
		vPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		vPanel.add(panel);
		return vPanel;
	}
	
	public void checkSelected(){
		visIKartImage.setIcon(getOrigIcon());
		getTabPane().setIconAt(getIndex(), getOrigIcon());
		module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.FUNN.getType());
	}
	private void checkNotSelected(){
		visIKartImage.setIcon(ImageUtil.toGrayScale(getOrigIcon()));
		getTabPane().setIconAt(getIndex(), ImageUtil.toGrayScale(getOrigIcon()));
		module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.FUNN.getType());
	}
	/**
	 * Lager panelet som inneholder filter til søket. 
	 * @return
	 */
	private JPanel getFilterPanel(){
		prosessPanel = new JProcessPanel(module, "${currentKontrakt != null ? currentKontrakt.id : null}", true);
		tilstandPanel = new JTilstandPanel();
		statusPanel = new JPunktregstatusTypePanel();
		prosessPanel.setSelected(true);
		tilstandPanel.setSelected(true);
		statusPanel.setSelected(true);
		
		JPanel panel = new JPanel();
		/*
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		panel.add(prosessPanel);
		panel.add(tilstandPanel);
		panel.add(aarsakPanel);
		panel.add(statusPanel);
		*/
		panel.setLayout(new GridBagLayout());
		panel.add(prosessPanel,  new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		panel.add(tilstandPanel, new GridBagConstraints(1,0, 1,1, 0.0,1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0,0,0,0), 0,0));
		panel.add(statusPanel,   new GridBagConstraints(3,0, 1,1, 0.0,1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0,0,0,0), 0,0));
		
		tilstandPanel.setMinimumSize(new Dimension(200,200));
		statusPanel.setMinimumSize(new Dimension(200,200));
		
		tilstandPanel.setPreferredSize(new Dimension(200,200));
		statusPanel.setPreferredSize(new Dimension(200,200));
		/*
		JPanel vPanel = new JPanel();
		vPanel.setLayout(new BoxLayout(vPanel, BoxLayout.PAGE_AXIS));
		vPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		vPanel.add(panel);
		*/
		PropertyChangeListener l = new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("alleValgte")){
					checkSelected();
					visIKartCheck.setSelected(true);
				}
			}
		};
		prosessPanel.addPropertyChangeListener(l);
		tilstandPanel.addPropertyChangeListener(l);
		statusPanel.addPropertyChangeListener(l);
		return panel;
		
	}
	
	@Override
	public MipssMapTask getTask() {
		AvvikQueryParams params = new AvvikQueryParams();
		params.setDateFra(plugin.getProduksjonOptions().getDatoFra().getDate());
		params.setDateTil(plugin.getProduksjonOptions().getDatoTil().getDate());
		params.setValgtKontrakt(module.getCurrentKontrakt());
		params.setAvvikstatusId(Arrays.asList(statusPanel.getAlleValgteId()));
		List<Long> prosesser = Arrays.asList(prosessPanel.getAlleValgteProsesser());
		params.setProsesser(prosesser.toArray(new Long[]{}));
		params.setTilstandId(tilstandPanel.getAlleValgte());
		
		return new HentFunnTask(module, ExecutorTaskType.QUEUE, params);
	}
	
	public boolean isVisIKart(){
		return visIKartCheck.isSelected();
	}

}
