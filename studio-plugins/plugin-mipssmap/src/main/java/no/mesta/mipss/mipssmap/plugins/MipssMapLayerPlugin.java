package no.mesta.mipss.mipssmap.plugins;

import no.mesta.mipss.mipssmap.utils.JCheckButton;

public interface MipssMapLayerPlugin {

    JCheckButton getCheckButton();
    void reset();
    void setEnabled(boolean enabled);
    
}
