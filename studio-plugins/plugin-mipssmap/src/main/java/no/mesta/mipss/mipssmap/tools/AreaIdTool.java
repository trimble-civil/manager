package no.mesta.mipss.mipssmap.tools;

import java.awt.Color;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.event.MouseInputListener;

import org.jdesktop.swingx.mapviewer.GeoPosition;

import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.PolygonLayer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.textboxdata.AreaIdTextBoxProducer;
import no.mesta.mipss.mipssmap.MipssMapModule;

public class AreaIdTool implements MouseInputListener{

	private final MipssMapModule plugin;

	public AreaIdTool(MipssMapModule plugin){
		this.plugin = plugin;
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		final GeoPosition pos = plugin.getMainMap().convertPointToGeoPosition(e.getPoint());
		e.consume();
		int[] ei = getEastAndNorthAreaIndex(pos.getLongitude(), pos.getLatitude());
		int areaId = encodeAreaId(ei[0], ei[1]);
		
		PolygonLayer layer = new PolygonLayer("ID="+areaId, getPolygonLayerFromArray(createMapBoxFromAreaId(areaId)), false);
		layer.setBorder(Color.red);
		layer.setForeground(new Color(1f,0f,0f,0.25f));
		layer.setTextboxProducer(new AreaIdTextBoxProducer(ei, areaId, createMapBoxFromAreaId(areaId)));
		plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PIN.getType(), layer, true);

//		String msg = String.format("East index=%d \nNorth index=%d\nAreaId=%d\nbbox=%s", ei[0], ei[1], areaId, createMapBoxFromAreaId(areaId));
//		JOptionPane.showMessageDialog(plugin.getModuleGUI(), msg);
	}

	private MipssGeoPosition[] getPolygonLayerFromArray(double[] a){
		return new MipssGeoPosition[]{
//			new MipssGeoPosition(a[0], a[1]),
//			new MipssGeoPosition(a[0], a[3]),
//			new MipssGeoPosition(a[2], a[3]),
//			new MipssGeoPosition(a[2], a[1])
				new MipssGeoPosition(a[1], a[0]),
				new MipssGeoPosition(a[3], a[0]),
				new MipssGeoPosition(a[3], a[2]),
				new MipssGeoPosition(a[1], a[2])
		};
	}
	private double[] createMapBoxFromAreaId(int areaId){
        int eastIndex = 0;
        int northIndex = 0;
        int eastnorth[] = decodeAreaId(areaId);
        eastIndex = eastnorth[0];
        northIndex = eastnorth[1];
        double west = eastIndex * 1000;
        double south = northIndex * 1000;
        double east = west + 1000D;
        double north = south + 1000D;
        return new double[]{west,south,east,north};
    }
	private int[] getEastAndNorthAreaIndex(double easting, double northing){
        int eastIndex = (int)Math.floor(easting / 1000D);
        int northIndex = (int)Math.floor(northing / 1000D);
        return (new int[] {
            eastIndex, northIndex
        });
    }
	
	private int encodeAreaId(int eastIndex, int northIndex){
        if(northIndex > 32767 || northIndex < -32767)
            throw new IllegalArgumentException((new StringBuilder()).append("North index out of range. North indes is ").append(northIndex).append("Max/Min is ").append(32767).append("/").append(-32767).toString());
        else
            return (eastIndex << 16) + northIndex;
    }
	public int[] decodeAreaId(int areaId){
        int eastIndex = areaId + 32768 >> 16;
        int northIndex = areaId - (eastIndex << 16);
        return (new int[] {
            eastIndex, northIndex
        });
    }

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
