package no.mesta.mipss.mipssmap.swing.treetable;

import javax.swing.JButton;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.TreeTableModel;

/**
 * This class is a subclass of JXTreeTable and delegates correct editors and
 * renderers to the different cells in the TreeTable, based on what kinds of objects
 * is found in the underlying PropertyNode.getValueObject()
 * @author Harald A. Kulø
 *
 */
public class JCheckTreeTable extends JXTreeTable{
	    
    /**
     * Create a new PropertiesTreeTable
     * 
     * @param model the underlying TreeTableModel
     */
	public JCheckTreeTable(TreeTableModel model){
		super(model);
	}
	
	/**
	 * Get a cell editor for row, column.
	 */
	public TableCellEditor getCellEditor(int row, int column) {
		TableModel model = this.getModel();
	      
	    Object o  = model.getValueAt(row, column);
	    Class clazz = null;
	    if (o!=null){
	    	switch(column){
				case 1: return new JButtonCellEditor(((JButton)o));
	    	}
	    }
	    return super.getCellEditor(row, column);
	}

	/**
	 * Get cellRenderer for row, column
	 */
	public TableCellRenderer getCellRenderer(int row, int column){
		TableModel model = this.getModel();
		Object o  = model.getValueAt(row, column);
	     	
		if (o!=null){
			switch(column){
				case 1: return new JButtonCellRenderer(((JButton)o).getText());
			}
		}
		TableCellRenderer cellRenderer = super.getCellRenderer(row, column);

		return cellRenderer;
	 }
	 
}
