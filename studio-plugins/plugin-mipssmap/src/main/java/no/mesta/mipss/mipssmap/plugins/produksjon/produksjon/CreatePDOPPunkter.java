package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.Color;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.LabelLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.task.HentPDOPTask;
import no.mesta.mipss.mipssmap.task.HentProduksjonTask;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.KonfigparamCache;
import no.mesta.mipss.mipssmap.utils.ProduksjonOptions;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.mipssmapserver.KjoretoyDTO;
import no.mesta.mipss.mipssmapserver.KjoretoyVO;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreatePDOPPunkter {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private List<KjoretoyVO> dfuListe; 
	private List<Prodtype> prodtyper; 
	private List<KjoretoyDTO> failed; 
	private boolean allProduksjon;
	private boolean kunSiste;
	private MipssMapModule plugin;
	private JCheckButton button;
	private ProduksjonOptions options;
	private final boolean labels;
	private final ProduksjonCriteria criteria;
	
	public CreatePDOPPunkter(MipssMapModule plugin, JCheckButton button, ProduksjonOptions options,  List<KjoretoyVO> dfuListe, List<Prodtype> prodtyper, List<KjoretoyDTO> failed, boolean allProduksjon, boolean kunSiste, boolean labels, ProduksjonCriteria criteria){
		this.plugin = plugin;
		this.button = button;
		this.options = options;
		this.dfuListe = dfuListe;
		this.prodtyper = prodtyper;
		this.failed = failed;
		this.allProduksjon = allProduksjon;
		this.kunSiste = kunSiste;
		this.labels = labels;
		this.criteria = criteria;
	}
	
	
	public void doIt(){
		Date datoFra = options.getDatoFra().getDate();
    	Date datoTil = options.getDatoTil().getDate();
    	for (KjoretoyVO kjoretoy:dfuListe){
    		Long kjoretoyId=null;
    		Long pdaId=null;
    		String guiNavn="";
    		boolean mestaBil = false;
    		if (kjoretoy.getKjoretoy()!=null){
    			kjoretoyId = kjoretoy.getKjoretoy().getId();
    			guiNavn = kjoretoy.getKjoretoy().getGuiTekst();
    			if (guiNavn==null)
    				guiNavn = kjoretoy.getKjoretoy().getNavn();
    			if (kjoretoy.getKjoretoy().getLeverandorNavn()!=null)
    				mestaBil = kjoretoy.getKjoretoy().getLeverandorNavn().toLowerCase().indexOf("mesta")!=-1;
    		}
    		if (kjoretoy.getPda()!=null)
    			pdaId = kjoretoy.getPda().getId();
    		
    		ProduksjonQueryParams params = new ProduksjonQueryParams();
    		params.setKjoretoyId(kjoretoyId);
    		params.setDfuId(pdaId);
    		params.setFraDato(datoFra);
    		params.setTilDato(datoTil);
    		params.setProdtyper(prodtyper);
    		params.setAllProduksjon(allProduksjon);
    		params.setKunSiste(kunSiste);
    		log.debug("Henter produksjon for kjoretoy:"+kjoretoyId+" eller pda:"+pdaId);
    		final HentPDOPTask task = new HentPDOPTask(plugin, params, guiNavn, mestaBil, criteria);
	        
	        ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.HentProduksjonTask.title")+((kjoretoyId==null)?pdaId:kjoretoyId));
	        workerPanel.setWorker(task);
	        task.setProgressController(plugin.getProgressController());
	        task.setProgressWorkerPanel(workerPanel);
	        plugin.getProgressController().addWorkerPanel(workerPanel);
	        
	        Thread wait = new Thread("Hent produksjon Thread(kjoretoy="+kjoretoyId+"pda="+pdaId+")"){ 
	            public void run(){
	                Layer layer=null;
	                try {
	                    layer = task.get();
	                    
	                    if (layer!=null){
		                    	plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONPUNKT.getType(), layer, true);
		                        button.setChecked(true);
		                        MipssGeoPosition lastPosition = task.getLastPosition();
		                        if (labels){
		                        	String labelId = "label:"+task.getSistePunkt().getDfuId().toString();
		                        	boolean mestaBil = task.isMestaBil();
		                        	LabelLayer label = null;
		                        	if (mestaBil){
		                        		label = getMestabilLabel(task, lastPosition, labelId);
		                        	}else if (task.isPda()){
		                        		label = getLabelPDA(task, lastPosition, labelId);
		                        	} else{
		                        		label = getLabelUE(task, lastPosition, labelId);
		                        	}
		                        	label.setPreferredLayerType(MipssMapLayerType.PRODUKSJONLABEL.getType());
			                        plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), label, false);
		                        }
	                    }
	                } catch (ExecutionException e) {
	                    e.printStackTrace();
	                } catch (InterruptedException e) {
	                    e.printStackTrace();
	                }
	            }

				private LabelLayer getLabelUE(final HentPDOPTask task, MipssGeoPosition lastPosition, String labelId) {
					String v1 = KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_UE);
					String v2= KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_TEXT_UE);
					Color fill = ColorUtil.setAlpha(ColorUtil.fromHexString(v1), 128);
					Color border = ColorUtil.fromHexString(v1);
					Color textColor = ColorUtil.fromHexString(v2);
					return new LabelLayer(labelId, lastPosition, task.getLabelText(), fill, border, textColor);
				}

				private LabelLayer getLabelPDA(final HentPDOPTask task, MipssGeoPosition lastPosition, String labelId) {
					String v1 = KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_INSPEKSJON);
					String v2= KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_TEXT_INSPEKSJON);
					Color fill = ColorUtil.setAlpha(ColorUtil.fromHexString(v1), 140);
					Color border = ColorUtil.fromHexString(v1);
					Color textColor = ColorUtil.fromHexString(v2);
					return new LabelLayer(labelId, lastPosition, task.getLabelText(),fill, border, textColor);
				}

				private LabelLayer getMestabilLabel(final HentPDOPTask task,  MipssGeoPosition lastPosition, String labelId) {
					String v1 = KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_MESTA);
					String v2= KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_TEXT_MESTA);
					Color c = ColorUtil.fromHexString(v1);
					Color fill = ColorUtil.setAlpha(c, 128);
					Color border = ColorUtil.fromHexString(v1);
					Color textColor = ColorUtil.fromHexString(v2);
					return new LabelLayer(labelId, lastPosition, task.getLabelText(),fill, border, textColor);
				}
	        };
	        wait.start();
    	}
    	KonfigparamCache.clearCache();
	}
	
	
}
