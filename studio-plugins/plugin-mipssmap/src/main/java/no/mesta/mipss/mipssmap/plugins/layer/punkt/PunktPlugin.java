package no.mesta.mipss.mipssmap.plugins.layer.punkt;

import javax.swing.JDialog;

import no.mesta.mipss.mapplugin.layer.LayerHandler;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;

public class PunktPlugin implements MipssMapLayerPlugin{
    public PunktPlugin() {
    }

    public JCheckButton getCheckButton() {
        JCheckButton btn = new JCheckButton(ResourceUtil.getResource("mipssmap.plugins.layer.punkt.PunktPlugin.button.text"));
        btn.setEnabled(false);
        return btn;
    }

	@Override
	public void reset() {
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		
	}
}
