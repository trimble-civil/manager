package no.mesta.mipss.mipssmap.task;

import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.service.produksjon.ProdReflinkStrekning;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Henter produserte strekninger fra databsen og 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class HentStrekningerTask extends MipssMapTask{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MipssMapModule plugin;
    private ProduksjonQueryParams params;
    
    private boolean noDataFound;
    private boolean kjoretoy;
    
	public HentStrekningerTask(MipssMapModule plugin, ProduksjonQueryParams params) {
		super(ExecutorTaskType.QUEUE);
        this.plugin = plugin;
        this.params = params;
        if (params.getKjoretoyId()!=null)
        	kjoretoy = true;
    }

    protected Layer doInBackground() {
    	if (isCancelled()){
    		return null;
    	}
    	getProgressWorkerPanel().setBarIndeterminate(true);
    	getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
        log.debug("doInBackground() henter prodstrekninger:"+params.getKjoretoyId()+" "+params.getFraDato()+" "+params.getTilDato());
        ProduksjonService service = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
        List<ProdReflinkStrekning> vertices  = null;
        if (kjoretoy)
        	vertices = service.getProduksjonStrekninger(params);
        else
        	vertices = service.getProduksjonStrekningerDfu(params);
        log.debug("Strekninger hentet. antall:"+vertices.size());
        Layer l = new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).getPeriodLineListLayerFromProdReflinkStrekning(vertices);
        if (l!=null){
	        String zoom = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "zoomHentingAvProduksjonStrekn").getVerdi();
	        boolean triggerRecenterAndZoom = true;
	        if (zoom!=null)
	        	triggerRecenterAndZoom = Boolean.valueOf(zoom);
	        
	        l.setTriggerRecenterAndZoom(triggerRecenterAndZoom);
	        if (vertices.isEmpty()){
	            noDataFound = true;
	            return null;
	        }
	        return l;
        }
        return null;
    }
    
    public boolean isNoDataFound() {
		return noDataFound;
	}
}
