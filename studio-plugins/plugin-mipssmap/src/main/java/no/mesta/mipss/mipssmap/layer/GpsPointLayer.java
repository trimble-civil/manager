package no.mesta.mipss.mipssmap.layer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.List;

import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.LayerConstants;
import no.mesta.mipss.mapplugin.layer.PointLayer;
import no.mesta.mipss.mapplugin.layer.ProdpunktGeoPosition;
import no.mesta.mipss.mapplugin.layer.SelectableLayer;
import no.mesta.mipss.mapplugin.widgets.TextboxData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
@Deprecated
public class GpsPointLayer extends PointLayer implements SelectableLayer, CollectionEntity{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private ProdpunktGeoPosition position;
    
    private Color pointColorFill;
    private Color pointColorBorder;
    
    private int arrowEndOffsetX;
    private int arrowEndOffsetY;
    private int arrowHead1X;
    private int arrowHead1Y;
    private int arrowHead2X;
    private int arrowHead2Y;
    private Rectangle viewport;
        
    public GpsPointLayer(String id, ProdpunktGeoPosition position, Color color, boolean triggerRecenterAndZoom) {
        super(id, position.getGeoPosition(), color, triggerRecenterAndZoom);
        
        this.position = position;
        this.pointColorFill = color;
        this.pointColorBorder = new Color(pointColorFill.getRed(), pointColorFill.getGreen(), pointColorFill.getBlue()).darker();
        
//        if (position.getProdpunktinfo().getProdpunkt()!=null){
//            double vinkel = (90 - position.getProdpunktinfo().getProdpunkt().getRetning()) * Math.PI / 180.0;
//            double pilVinkel1 = vinkel + Math.PI*0.8;
//            double pilVinkel2 = vinkel - Math.PI*0.8;
//            arrowEndOffsetX = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.cos(vinkel));
//            arrowEndOffsetY = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.sin(vinkel));
//            arrowHead1X = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.cos(pilVinkel1));
//            arrowHead1Y = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.sin(pilVinkel1));
//            arrowHead2X = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.cos(pilVinkel2));
//            arrowHead2Y = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.sin(pilVinkel2));
//        }
    }
    
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        //g = super.initGraphics(g, map);//TODO only if this is a standalone pointlayer...
        g.setColor(pointColorFill);
        
        boolean paint = false;
        viewport = map.getViewportBounds();
        
        g.setColor(pointColorFill);
        GeoPosition gp = new GeoPosition(position.getGeoPosition().getLatitude(), position.getGeoPosition().getLongitude());
        Point2D pt1 = map.getTileFactory().geoToPixel(gp, map.getZoom());
        
        if (!isDrawOutsideViewport()){
            if (viewport.contains(pt1)){
                paint = true;
            }
        }else
            paint = true;
        
        
        if (paint){
            g.setStroke(new BasicStroke(1));
            int x = (int)pt1.getX();
            int y = (int)pt1.getY();
            int dia = LayerConstants.DEFAULT_POINT_DIAMETER;
            g.fillOval(x-(dia/2), y-(dia/2), dia+1, dia+1);
            //pil
            g.drawLine(x+arrowEndOffsetX/2, y-arrowEndOffsetY/2, x+arrowEndOffsetX, y-arrowEndOffsetY);
            g.drawLine(x+arrowEndOffsetX, y-arrowEndOffsetY, x+arrowEndOffsetX+(int)(arrowHead1X/1.5), y-arrowEndOffsetY-(int)(arrowHead1Y/1.5));
            g.drawLine(x+arrowEndOffsetX, y-arrowEndOffsetY, x+arrowEndOffsetX+(int)(arrowHead2X/1.5), y-arrowEndOffsetY-(int)(arrowHead2Y/1.5));
            
            //nese
            /*
            Polygon p = new Polygon();
            p.addPoint(x+arrowEndOffsetX, y-arrowEndOffsetY);
            p.addPoint(x+arrowEndOffsetX+(int)(arrowHead1X/1.5), y-arrowEndOffsetY-(int)(arrowHead1Y/1.5));
            p.addPoint(x+arrowEndOffsetX+(int)(arrowHead2X/1.5), y-arrowEndOffsetY-(int)(arrowHead2Y/1.5));
            g.fillPolygon(p);
            */
             g.setColor(pointColorBorder);
             g.drawOval(x-(dia/2), y-(dia/2), dia, dia);
        }
    }
    /**
     * Returns a meaningfull representation of the data contained in this point
     * 
     * @return
     */
    public List<TextboxData> getInfo() {
        if (getTextboxProducer()!=null){
            return getTextboxProducer().getTextboxData();
        }
        /*
        TextboxData data = new TextboxData();
        data.setTitle("Prodpunktinfo");
        //Map<String, String> d = new HashMap<String, String>();
        
        String speed = ""+position.getGpsPoint().getData().getSpeed();
        String lat = position.getGpsPoint().getData().getLatitude();
        String lon = position.getGpsPoint().getData().getLongitude();
        String status = position.getGpsPoint().getData().getProductionStatus();
        String retning = ""+position.getGpsPoint().getData().getCourse();
        String enhet = position.getGpsPoint().getData().getUnitID();
        String[] tid = position.getGpsPoint().getData().getTime().toString().split("T");
        //String[] tid = "21.08.2008T10:34:53".split("T");
        String td = tid[0]+" "+tid[1];
        
        data.addData("Enhet", enhet);
        data.addData("Hastighet", speed);
        data.addData("Prodstatus", status);
        data.addData("Retning", retning);
        data.addData("Latitude", lat);
        data.addData("Longitude", lon);
        data.addData("Tidspunkt", td);
        //data.setData(d);
        
        return data;*/
        return null;
    }
    
    public ProdpunktGeoPosition getGpsGeoPosition(){
        return position;
    }
    
}
