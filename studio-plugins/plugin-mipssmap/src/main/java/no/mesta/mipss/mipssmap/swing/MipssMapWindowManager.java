package no.mesta.mipss.mipssmap.swing;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JDialog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hånterer dialoger. En dialog som blir lagt til i denne manageren vil kun bli vist som en instans på skjermen.
 * Før man legger til en dialog bør getDialog kalles for å forsikre seg om at den ikke er lagt til fra før. Hvis addDialog
 * bli kalt med en instans av en klasse som finnes fra før vil denne dialogen vises frem på skjermen. Alle dialoger
 * som legges til vil automatisk få denne klassen som WindowListener og ComponentListener samt defaultCloseOperation=HIDE_ON_CLOSE
 * Hvis en dialog lukkes med dispose() vil denne fjernes fra windowmanageren. For at manageren skal klare å håndtere dialogene
 * skal setVisible(false) kalles på dialogene når de ønskes bort fra skjermen. 
 * 
 * @author Harald A. Kulø
 */
public class MipssMapWindowManager implements WindowListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private HashMap<Class, JDialog> dialoger;
    
    public MipssMapWindowManager() {
        dialoger = new HashMap<Class, JDialog>();
    }
    
    public void hideAll(){
    	Set<Class> keySet = dialoger.keySet();
    	for (Class c:keySet){
    		dialoger.get(c).setVisible(false);
    	}
    }
    public JDialog getDialog(Class c){
    	return dialoger.get(c);
    }
    
    public JDialog getVisibleDialog(Class c){
    	return dialoger.get(c);
    }
    public void clear(){
        dialoger.clear();
    }
    
    public void addDialog(JDialog dialog){
    	synchronized(this){
    		if (dialoger.containsKey(dialog.getClass())){
    			JDialog visibleDialog = dialoger.get(dialog.getClass());
    			visibleDialog.requestFocus();
    		}else{
    			dialoger.put(dialog.getClass(), dialog);
    			dialog.addWindowListener(this);
//    			dialog.addComponentListener(this);
    			dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
    		}
    	}
    }
//    public void addDialog(JDialog dialog){
//        synchronized(this){
//            if (dialoger.containsKey(dialog.getClass())){
//                JDialog visibleDialog = dialoger.get(dialog.getClass());
//                
//            }else{
//                if (hidden.containsKey(dialog.getClass())){
//                    dialoger.put(dialog.getClass(), hidden.remove(dialog.getClass()));
//                    dialog.setVisible(true);
//                }else{
//                    dialoger.put(dialog.getClass(), dialog);   
//                    dialog.setVisible(true);
//                    dialog.addWindowListener(this);
//                    dialog.addComponentListener(this);
//                    dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
//                }
//            }
//        }
//    }
    public void addDialog(JDialog dialog, boolean visible){
        synchronized(this){
        	dialoger.put(dialog.getClass(), dialog);
            dialog.addWindowListener(this);
//            dialog.addComponentListener(this);
            dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        }
    }

    public void windowOpened(WindowEvent e) {
        
    }

    public void windowClosing(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
        JDialog dialog = (JDialog)e.getSource();
        dialoger.remove(dialog);
//        hidden.remove(dialog);
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void componentResized(ComponentEvent e) {
    }

    public void componentMoved(ComponentEvent e) {
    }

    public void componentShown(ComponentEvent e) {
    }
    /**
     * Blir kalt når dialog.setVisible(false);
     * @param e
     */
//    public void componentHidden(ComponentEvent e) {
//        synchronized(this){
//            JDialog dialog = (JDialog)e.getSource();
//            JDialog d = dialoger.remove(dialog.getClass());
//            hidden.put(dialog.getClass(), d);
//        }
//    }
}
