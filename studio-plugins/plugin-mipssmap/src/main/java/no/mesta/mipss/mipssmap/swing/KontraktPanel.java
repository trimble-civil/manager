package no.mesta.mipss.mipssmap.swing;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.util.MapUtilities;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.plugins.MipssMapProductionPlugin;
import no.mesta.mipss.mipssmap.plugins.layer.kontraktvei.KontraktveiPlugin;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;


/**
 * Lite panel for å velge kontrakt. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class KontraktPanel extends JPanel {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private JComboBox kontraktCombo;
    private MipssComboBoxModel<Driftkontrakt> model;
    private MipssMapWindowManager windowManager;
    private Driftkontrakt currentKontrakt;
    private KontraktveiPlugin kontraktveiPlugin;
    private MipssMapModule plugin;
    public KontraktPanel(MipssMapModule plugin, KontraktveiPlugin kontraktveiPlugin, MipssMapWindowManager windowManager) {
        super();
        this.plugin = plugin;
        this.kontraktveiPlugin = kontraktveiPlugin;
        this.windowManager = windowManager;
        initGUI();
    }
    
    private void initGUI() {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        final JMipssContractPicker mipssContractPicker = new JMipssContractPicker(plugin, false);
        mipssContractPicker.getDropdown().setMaximumRowCount(30);
        mipssContractPicker.getDropdown().addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e) {
        	  if (mipssContractPicker.getValgtKontrakt()!=null){
        		  velgKontrakt(mipssContractPicker);
        	  }
          }
        });
        mipssContractPicker.getDropdown().addKeyListener(new KeyAdapter(){
        	public void keyReleased(KeyEvent e){
        		if (e.getKeyCode()==KeyEvent.VK_ENTER){
	        		velgKontrakt(mipssContractPicker);
	        		JComboBox combo = (JComboBox)e.getSource();
	        		combo.hidePopup();
        		}
        	}
        });
        
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        final JCheckBox visVeinett = new JCheckBox("Vis kontraktveinett");
        Binding bind = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ_WRITE, visVeinett, BeanProperty.create("selected"), kontraktveiPlugin.getCheckButton().getCheckBox(), BeanProperty.create("selected"));
        bind.bind();
        
        visVeinett.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (visVeinett.isSelected()){
					kontraktveiPlugin.createKontraktveinett(currentKontrakt, false, null);
				}else{
					plugin.getMainMap().getLayerHandler().hideLayer(MipssMapLayerType.KONTRAKTVEI.getType(), currentKontrakt.getId().intValue()+"");
				}
			}
        	
        });
        panel.add(visVeinett);
        panel.add(Box.createHorizontalGlue());
        
        add(mipssContractPicker);
        add(Box.createRigidArea(new Dimension(0,2)));
        if (plugin.getCurrentKontrakt()!=null){
        	int index = mipssContractPicker.getModel().getKontrakter().indexOf(plugin.getCurrentKontrakt());
        	mipssContractPicker.getDropdown().setSelectedIndex(index);
        }
    }
    
    private void velgKontrakt(JMipssContractPicker mipssContractPicker){
    	currentKontrakt = mipssContractPicker.getValgtKontrakt();
		plugin.setCurrentKontrakt(currentKontrakt);
		windowManager.clear();
		
		if (plugin.getLayerPlugins()!=null){
			List<MipssMapLayerPlugin> layerPlugins = plugin.getLayerPlugins();
			for (MipssMapLayerPlugin mp:layerPlugins){
				mp.reset();
			}
		}
		if (plugin.getProduksjonPlugins()!=null){
			List<MipssMapProductionPlugin> produksjonPlugins = plugin.getProduksjonPlugins();
			for (MipssMapProductionPlugin mp:produksjonPlugins){
				mp.reset();
			}
		}
		
		plugin.getMainMap().getLayerHandler().removeAllLayers();
		if (currentKontrakt!=null&&currentKontrakt.getAreal()!=null){
			final Bounds b = MapUtilities.convertArealToBounds(currentKontrakt.getAreal());
        	if (plugin.getStatus().equals(MipssPlugin.Status.RUNNING))
        		plugin.getMainMap().ensureVisibilityForBounds(b);
        	else{
        		new Thread(){
        			public void run(){
		        		while (!plugin.getStatus().equals(MipssPlugin.Status.RUNNING)){
		        			Thread.yield();
		        		}
		        		try {
			        		SwingUtilities.invokeAndWait(new Runnable(){
	    						public void run(){
	    							plugin.getMainMap().ensureVisibilityForBounds(b);
	    						}
	    					});
		        		} catch (InvocationTargetException e){
		        			e.printStackTrace();
		        		} catch (InterruptedException e) {
							e.printStackTrace();
						}
		        	}
        		}.start();
        	}
        }
    }
    public JComboBox getKontraktCombo(){
        return kontraktCombo;
    }

   
}
