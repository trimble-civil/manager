package no.mesta.mipss.mipssmap.utils;

import no.mesta.mipss.mapplugin.util.LayerType;

public enum MipssMapLayerType {
    
    KONTRAKTVEI (new LayerType(1, "kontraktvei", false)),
    RODE        (new LayerType(2, "rode", false)),
    RODESTREKNING (new LayerType(3, "rodestrekning", false)),
    STREKNING       (new LayerType(4, "strekning", false)),
    PRODUKSJONSTREKNING (new LayerType(5, "produksjonstrekning", false)),
    PRODUKSJONPUNKT     (new LayerType(6, "produksjonpunkt", false)), 
    VAERSTASJON			(new LayerType(7, "vaerstasjon", false)),
    SELECTED            (new LayerType(8, "selected", true)),
    LABEL               (new LayerType(9, "label", false)),
    TEXTBOX             (new LayerType(10, "textbox", true));
    
    
    private final LayerType type;
    
    MipssMapLayerType(LayerType type){
        this.type = type;
    }
    
    public LayerType getType(){
        return type;
    }
    
}
