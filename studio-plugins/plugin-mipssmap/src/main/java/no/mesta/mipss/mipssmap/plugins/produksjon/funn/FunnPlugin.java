package no.mesta.mipss.mipssmap.plugins.produksjon.funn;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;

import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.ProductionPlugin;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.task.MipssMapTask;
import no.mesta.mipss.mipssmap.utils.ErrorHandler;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

 /**
  * MipssMapProductionPlugin for å vise frem funn
  * 
  * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
  */
public class FunnPlugin extends ProductionPlugin{
	
    public FunnPlugin(MipssMapWindowManager windowManager, MipssMapModule plugin) {
    	super(windowManager, plugin, ResourceUtil.getResource("mipssmap.plugins.produksjon.funn.FunnPlugin.button.text"),
    			ResourceUtil.getResource("produksjon.observasjon.tooltip"), ResourceUtil.getResource("produksjon.vis.observasjon.tooltip"));
    	getCheckButton().setAction(new ClickAction());
    	
    }
    
    protected void buttonActionPerformed(ActionEvent e) {
    	if (getPlugin().getCurrentKontrakt()!=null){
    		FunnDialog dialog = (FunnDialog)getWindowManager().getDialog(FunnDialog.class);
            if (dialog==null){
                dialog = new FunnDialog(SwingUtilities.windowForComponent(getCheckButton()), this, getPlugin());
                dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.produksjon.funn.FunnPlugin.dialog.title"));
                dialog.setSize(new Dimension(850, 330));
                dialog.setLocationRelativeTo(null);
                getWindowManager().addDialog(dialog);
            }
            dialog.setVisible(true);
            
            
        }
    }
    
    protected void checkActionPerformed(ActionEvent e){
    	if (getCheckButton().getCheckBox().isSelected()&&!isLoaded()){
    		new FunnPanel(getPlugin(), this).hentData();
    		setLoaded(true);
    	}
    	
    	else{
	    	if (getCheckButton().getCheckBox().isSelected()){
	    		getPlugin().getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.HENDELSE.getType());
	    		getPlugin().getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.FUNN.getType());
	    	}else{
	    		getPlugin().getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.HENDELSE.getType());
	    		getPlugin().getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.FUNN.getType());
	    	}    
    	}
    }
    
    
    public void executeTask(final MipssMapTask task, String title, final LayerType type){
        setLoaded(true);
    	inSynch();
        ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(title);
        workerPanel.setWorker(task);
        task.setProgressController(getPlugin().getProgressController());
        task.setProgressWorkerPanel(workerPanel);
        getPlugin().getProgressController().addWorkerPanel(workerPanel);
        Thread wait = new Thread("Hent inspeksjon Thread()"){ 
            public void run(){
            	Layer layer=null;
                try {
                    layer = task.get();
                    if (layer!=null&&!layer.getBounds().isNull()){	
                    	getPlugin().getMainMap().getLayerHandler().addLayer(type, layer, true);
                    	getPlugin().getMainMap().repaint();
                    	getCheckButton().getCheckBox().setSelected(true);
                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    String msg = ResourceUtil.getResource("mipssmap.feilmelding");
                    ErrorHandler.showError(e, msg);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } 
            }
        };
        wait.start();
    }
    
    @SuppressWarnings("serial")
	private class ClickAction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e){
			refresh();
		}
	}
    public void refresh() {
    	if (isEnabled()){
			if (!isTimeSynched()){ 
				if (!isLoaded()){
					getCheckButton().getCheckBox().setSelected(true);
					checkActionPerformed(null);
				}else{
					FunnDialog funnDialog = (FunnDialog)getWindowManager().getDialog(FunnDialog.class);
					if (funnDialog!=null){
						funnDialog.getFunnPanel().hentData();
					}else{
						setLoaded(false);
						getCheckButton().getCheckBox().setSelected(true);
						checkActionPerformed(null);
					}
				}
				setPrevFra(getProduksjonOptions().getDatoFra().getDate());
				setPrevTil(getProduksjonOptions().getDatoTil().getDate());
				inSynch();
			}
    	}
	}
}
