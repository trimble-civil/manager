package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.util.List;

import no.mesta.mipss.mipssmapserver.KjoretoyVO;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;

public class ProduksjonCriteria {

	private List<KjoretoyVO> enhetList; 
	private boolean punkter;
	private boolean kunSiste; 
	private boolean strekninger; 
	private List<Prodtype> prodtyper;
	private boolean allProduksjon;
	private boolean labels;
	private boolean pdop;
	private double rmax;
	private double rmin;
	private double ymax;
	private double ymin;
	private double gmax;
	private double gmin;
	
	private boolean redVisible;
	private boolean yellowVisible;
	private boolean greenVisible;
	
	private boolean grunndata;
	private Long dfuId;
	public double getRmax() {
		return rmax;
	}

	public double getRmin() {
		return rmin;
	}

	public double getYmax() {
		return ymax;
	}

	public double getYmin() {
		return ymin;
	}

	public double getGmax() {
		return gmax;
	}

	public double getGmin() {
		return gmin;
	}

	public void setRmax(double rmax) {
		this.rmax = rmax;
	}

	public void setRmin(double rmin) {
		this.rmin = rmin;
	}

	public void setYmax(double ymax) {
		this.ymax = ymax;
	}

	public void setYmin(double ymin) {
		this.ymin = ymin;
	}

	public void setGmax(double gmax) {
		this.gmax = gmax;
	}

	public void setGmin(double gmin) {
		this.gmin = gmin;
	}

	public ProduksjonCriteria(){
	}

	public List<KjoretoyVO> getEnhetList() {
		return enhetList;
	}

	public void setEnhetList(List<KjoretoyVO> enhetList) {
		this.enhetList = enhetList;
	}

	public boolean isPunkter() {
		return punkter;
	}

	public void setPunkter(boolean punkter) {
		this.punkter = punkter;
	}

	public boolean isKunSiste() {
		return kunSiste;
	}

	public void setKunSiste(boolean kunSiste) {
		this.kunSiste = kunSiste;
	}

	public boolean isStrekninger() {
		return strekninger;
	}

	public void setStrekninger(boolean strekninger) {
		this.strekninger = strekninger;
	}

	public List<Prodtype> getProdtyper() {
		return prodtyper;
	}

	public void setProdtyper(List<Prodtype> prodtyper) {
		this.prodtyper = prodtyper;
	}

	public boolean isAllProduksjon() {
		return allProduksjon;
	}

	public void setAllProduksjon(boolean allProduksjon) {
		this.allProduksjon = allProduksjon;
	}

	public void setLabels(boolean labels) {
		this.labels = labels;
	}

	public boolean isLabels() {
		return labels;
	}

	public boolean isRedVisible() {
		return redVisible;
	}

	public boolean isYellowVisible() {
		return yellowVisible;
	}

	public boolean isGreenVisible() {
		return greenVisible;
	}

	public void setRedVisible(boolean redVisible) {
		this.redVisible = redVisible;
	}

	public void setYellowVisible(boolean yellowVisible) {
		this.yellowVisible = yellowVisible;
	}

	public void setGreenVisible(boolean greenVisible) {
		this.greenVisible = greenVisible;
	}

	public void setPdop(boolean pdop) {
		this.pdop = pdop;
	}

	public boolean isPdop() {
		return pdop;
	}

	public void setGrunndata(boolean grunndata) {
		this.grunndata = grunndata;
	}

	public boolean isGrunndata() {
		return grunndata;
	}

	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}
	public Long getDfuId(){
		return dfuId;
	}
}
