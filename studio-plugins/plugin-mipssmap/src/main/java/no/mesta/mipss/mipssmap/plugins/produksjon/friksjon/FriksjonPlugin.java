package no.mesta.mipss.mipssmap.plugins.produksjon.friksjon;

import java.awt.event.ActionEvent;

import no.mesta.mipss.mipssmap.plugins.ProductionPlugin;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.resources.images.IconResources;

/**
 * MipssMapProductionPlugin for å vise frem friksjon
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class FriksjonPlugin extends ProductionPlugin{
    
    
    public FriksjonPlugin() {
    	super(null, null, ResourceUtil.getResource("mipssmap.plugins.produksjon.friksjon.FriksjonPlugin.button.text"), "", "");
    	getCheckButton().setEnabled(false);
    	getCheckButton().setAction(null);
    	getCheckButton().setIcon(ImageUtil.toGrayScale(IconResources.REFRESH_VIEW_ICON));
    }
    @Override
    public void setEnabled(boolean enabled){
		getCheckButton().setEnabled(false);
	}
    @Override
	protected void buttonActionPerformed(ActionEvent e) {
		
	}

	@Override
	protected void checkActionPerformed(ActionEvent e) {
		
	}

	@Override
	public void refresh() {
		
	}
}
