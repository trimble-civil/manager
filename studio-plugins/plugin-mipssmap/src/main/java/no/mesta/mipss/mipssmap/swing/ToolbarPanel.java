package no.mesta.mipss.mipssmap.swing;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.ImageFileFilter;
import no.mesta.mipss.core.UTMUtils;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.PinLayer;
import no.mesta.mipss.mapplugin.tools.MoveLayerTool;
import no.mesta.mipss.mapplugin.tools.PanMapTool;
import no.mesta.mipss.mapplugin.tools.QuickZoomDialog;
import no.mesta.mipss.mapplugin.tools.QuickZoomTool;
import no.mesta.mipss.mapplugin.tools.SelectTool;
import no.mesta.mipss.mapplugin.tools.SelectionTool;
import no.mesta.mipss.mapplugin.util.MapUtilities;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.MouseListenerManager;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.model.Address;
import no.mesta.mipss.mipssmap.tools.AreaIdTool;
import no.mesta.mipss.mipssmap.tools.RulerTool;
import no.mesta.mipss.mipssmap.tools.VeiTool;
import no.mesta.mipss.mipssmap.tools.ViapunktTool;
import no.mesta.mipss.mipssmap.tools.YrWeatherDataTool;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssSplitButton;
import no.mesta.mipss.ui.roadpicker.JRoadSelectDialog;
import no.mesta.mipss.util.ProxyUtil;
import no.mesta.mipss.xml.XMLHelper;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * @author  harkul
 */
public class ToolbarPanel extends JPanel {
	public enum ToolMode{
		PANORER, 
		ZOOM,
		LINJAL,
		VIAPUNKT,
		VEI,
		YR, 
		PLAYBACK, 
		RESET,
		TRIONA_AREA_TOOL
	}
	private ToolMode toolMode;
	
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssMapText");
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private JToggleButton panButton;
    private MipssSplitButton playbackButton;
    private MipssSplitButton zoomButton;
    private MipssSplitButton meassureButton;
    
    private JToggleButton yrButton;
//    private JToggleButton veiButton;
    private MipssSplitButton veiButton;
    
    private JButton copyToClipboard;
    
    private ButtonGroup buttonGroup;
    private QuickZoomDialog quickZoomDialog;
    private JDialog playBackDialog;
    private JDialog playStrekningerDialog;
    
    private MipssMapModule plugin;
    
    private MipssMapWindowManager windowManager = new MipssMapWindowManager();

	private JButton saveButton;
	private JButton searchButton;
	private JComboBox searchCombo;
	private DefaultComboBoxModel searchModel;
	
	private JToggleButton areaButton;

	private VeiInfo veiInfo = new VeiInfo();
    
    /** Creates new form ToolbarPanel */
    public ToolbarPanel(MipssMapModule plugin) {
    	this.plugin = plugin;
        initComponents();
    }

    private void initComponents() {
    	
    	setBorder(BorderFactory.createTitledBorder(""));    
    	setLayout(new GridBagLayout());
        buttonGroup = new ButtonGroup();
        
        panButton = new JToggleButton();
        zoomButton = new MipssSplitButton();
        
        zoomButton.addMenuItem(ResourceUtil.getResource("mipssmap.swing.ToolbarPanel.zoomButton.synlige.text"), new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	setTool(ToolMode.RESET);
            	zoomAllDataActionPerformed(e);
                panButton.setSelected(true);
                setTool(ToolMode.PANORER);
            }
        }, null);
        
        zoomButton.addMenuItem(ResourceUtil.getResource("mipssmap.swing.ToolbarPanel.zoomButton.kontraktveinett.text"), new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	setTool(ToolMode.RESET);
            	zoomKontraktveinettActionPerformed(e);
                panButton.setSelected(true);
                setTool(ToolMode.PANORER);
            }
        }, null);
        zoomButton.addMenuItem(ResourceUtil.getResource("mipssmap.swing.ToolbarPanel.zoomButton.QuickZoom.text"), new ActionListener(){
            public void actionPerformed(ActionEvent e){
                quickZoomButtonActionPerformed(e);
                zoomButton.setSelected(true);
                setTool(ToolMode.ZOOM);
            }
        }, null);
        
        playbackButton = new MipssSplitButton();
        playbackButton.addMenuItem(ResourceUtil.getResource("mipssmap.swing.ToolbarPanel.playbackButton.valgt"),new ActionListener(){
            public void actionPerformed(ActionEvent e){
                playbackButtonActionPerformed(e);
                playbackButton.setSelected(true);
                setTool(ToolMode.PLAYBACK);
            }                   
        }, null);
        playbackButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                playbackButtonActionPerformed(e);
                setTool(ToolMode.PLAYBACK);
            }
        });
        
        playbackButton.addMenuItem(ResourceUtil.getResource("mipssmap.swing.ToolbarPanel.playbackButton.alle"), new ActionListener(){
            public void actionPerformed(ActionEvent e){
                playbackAlleButtonActionPerformed(e);
                playbackButton.setSelected(true);
                setTool(ToolMode.PLAYBACK);
            }
        }, null);
        
        meassureButton = new MipssSplitButton();
        meassureButton.addMenuItem(ResourceUtil.getResource("ToolbarPanel.meassureButton.linear"), ResourceUtil.getResource("ToolbarPanel.meassureButton.linear.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		meassureButtonActionPerformed(e);
        		meassureButton.setSelected(true);
        	}
        }, IconResources.LINEAR_ICON);
        meassureButton.addMenuItem(ResourceUtil.getResource("ToolbarPanel.meassureButton.viapunkt"), ResourceUtil.getResource("ToolbarPanel.meassureButton.viapunkt.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		viapunktActionPerformed(e);
        		meassureButton.setSelected(true);
        	}
        }, IconResources.VIAPUNKT_ICON);
        
        yrButton = new JToggleButton();
        
        veiButton = new MipssSplitButton();
        veiButton.addMenuItem(ResourceUtil.getResource("ToolbarPanel.veiButton.punktTilVeiref"), ResourceUtil.getResource("ToolbarPanel.veiButton.punktTilVeiref.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		veiButtonActionPerformed(e);
        	}
        }, IconResources.VEI_ICON);
        
        veiButton.addMenuItem(ResourceUtil.getResource("ToolbarPanel.veiButton.veirefTilPunkt"), ResourceUtil.getResource("ToolbarPanel.veiButton.veirefTilPunkt.tooltip"), new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		veirefButtonActionPerformed(e);
        	}
        }, IconResources.MAP_PIN_LEFT_SM);
        
        //veiButton = new JToggleButton();
        copyToClipboard = new JButton(new CopyToClipboardAction(null, IconResources.COPY_ICON_BIG));
        saveButton = new JButton(new SaveAction(null, IconResources.SAVE_ICON_BIG));
        
        areaButton = new JToggleButton(getAreaButtonActionPerformed());
        
        
        searchButton = new JButton(new SearchAction(null, IconResources.SEARCH_ICON));
        searchCombo = new JComboBox();
        searchCombo.setEditable(true);
        searchModel = new DefaultComboBoxModel();
        searchCombo.setModel(searchModel);
        searchCombo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (searchCombo.getSelectedItem() instanceof Address){
					Address a = (Address)searchCombo.getSelectedItem();
					MipssGeoPosition gp = a.getPoint();
					PinLayer l = new PinLayer("adressePin", false, PinLayer.ORIENTATION.LEFT, new GeoPosition(gp.getLatitude(), gp.getLongitude()), true);
					Bounds b = new Bounds();
					b.setLlx(gp.getLongitude());
					b.setLly(gp.getLatitude());
					b.setUrx(gp.getLongitude());
					b.setUry(gp.getLatitude());
					l.setBounds(b);
					l.setPreferredLayerType(MipssMapLayerType.ALLWAYSONTOP.getType());

					plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.ALLWAYSONTOP.getType(), l, true);
					plugin.getMainMap().calculateZoomForLayer(l);
					plugin.getMainMap().setZoom(plugin.getMainMap().getZoom()-3);
				}else{
					searchButton.doClick();
				}
			}
        });
        
        buttonGroup.add(panButton);
        buttonGroup.add(playbackButton);
        buttonGroup.add(meassureButton);
        buttonGroup.add(yrButton);
        buttonGroup.add(veiButton);
        buttonGroup.add(zoomButton);
        buttonGroup.add(areaButton);
        
        panButton.setIcon(IconResources.HAND_OPEN_ICON);
        zoomButton.setIcon(IconResources.TOOL_ZOOM_ICON);
        meassureButton.setIcon(IconResources.RULER_ICON);
        playbackButton.setIcon(IconResources.PLAYER_PLAY_S);
        yrButton.setIcon(IconResources.YR_ICON);
        veiButton.setIcon(IconResources.VEI_BIG_ICON);
        panButton.setSelected(true);
        
        setButtonSize(panButton);
        setButtonSize(zoomButton);
        setButtonSize(meassureButton);
        setButtonSize(playbackButton);
        setButtonSize(yrButton);
        setButtonSize(veiButton);
        setButtonSize(copyToClipboard);
        setButtonSize(saveButton);
        setButtonSize(areaButton);
        
        panButton.setToolTipText(resources.getGuiString("toolbar.pan.tooltip"));
        zoomButton.setToolTipText(resources.getGuiString("toolbar.zoom.tooltip"));
        zoomButton.getArrowButton().setToolTipText(resources.getGuiString("toolbar.zoom.more.tooltip"));
        meassureButton.setToolTipText(resources.getGuiString("toolbar.ruler.tooltip"));
        playbackButton.setToolTipText(resources.getGuiString("toolbar.play.tooltip"));
        playbackButton.getArrowButton().setToolTipText(resources.getGuiString("toolbar.play.more.tooltip"));
        yrButton.setToolTipText(resources.getGuiString("toolbar.yr.tooltip"));
        veiButton.setToolTipText(resources.getGuiString("toolbar.vei.tooltip"));
        copyToClipboard.setToolTipText(resources.getGuiString("toolbar.copyToClipboard.tooltip"));
        saveButton.setToolTipText(resources.getGuiString("toolbar.save.tooltip"));
        
        add(panButton, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0,0));
        
        add(zoomButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        add(zoomButton.getArrowButton(), new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0,0));
        
        add(meassureButton, new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        add(meassureButton.getArrowButton(), new GridBagConstraints(4,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0,0));
        
        add(playbackButton, new GridBagConstraints(5,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        add(playbackButton.getArrowButton(), new GridBagConstraints(6,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,15), 0,0));

        add(yrButton, new GridBagConstraints(7,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0,0));

        add(veiButton, new GridBagConstraints(8,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        add(veiButton.getArrowButton(), new GridBagConstraints(9,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));

        add(saveButton, new GridBagConstraints(10,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
        int x = 11;
        if (UserUtils.isAdmin(plugin.getLoader().getLoggedOnUser(false))){
        	add(areaButton, new GridBagConstraints(x++,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
        }
        add(searchCombo, new GridBagConstraints(x++,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
        add(searchButton, new GridBagConstraints(x++,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));

        panButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panButtonActionPerformed(e);
            }
        });
        zoomButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                zoomButtonActionPerformed(e);
            }                   
        });
        
        meassureButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                meassureButtonActionPerformed(e);
            }
        });
        yrButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				yrButtonActionPerformed(e);
			}
        });
        veiButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				veiButtonActionPerformed(e);
			}
        });
    }
    private void setButtonSize(JComponent b){
        Dimension s = new Dimension(35,35);
        b.setMaximumSize(s);
        b.setPreferredSize(s);
        b.setMinimumSize(s);
    }

	private void panButtonActionPerformed(ActionEvent e) {
		setTool(ToolMode.PANORER);
    }
     
    private void zoomButtonActionPerformed(ActionEvent e) {
    	setTool(ToolMode.ZOOM);
    }
    
    /**
     * Action metode for hva som skal skje når en bruker trykker på quick zoom 
     * menyen.
     * 
     * Viser frem en dialog med et forstørret bilde av kartet der musepekeren befinner seg
     * 
     * @param e
     */
    private void quickZoomButtonActionPerformed(ActionEvent e) {
        if (quickZoomDialog==null){
            Cursor zoomCursor = Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon(getClass().getResource("/images/quickzoomcursor.png")).getImage(), new Point(13,13), "zoom");
            plugin.getMainMap().setCursor(zoomCursor);
            Component owner = this;
            while (!(owner instanceof Window)){
                owner = owner.getParent();
            }
            Point screenLocation = plugin.getMainMap().getLocationOnScreen();
            
            quickZoomDialog= new QuickZoomDialog((Frame)owner);
            quickZoomDialog.setSize(new Dimension(200,220));
            quickZoomDialog.setLocation(screenLocation.x+plugin.getMainMap().getSize().width-quickZoomDialog.getSize().width, screenLocation.y);
            QuickZoomTool zoom = new QuickZoomTool(quickZoomDialog);
            quickZoomDialog.addWindowListener(new WindowAdapter(){
                public void windowClosed(WindowEvent e) {
                	plugin.getMainMap().getMouseListenerManager().removeListener(QuickZoomTool.class);
                    quickZoomDialog = null;
                }
            });
            
            plugin.getMainMap().getMouseListenerManager().addListener(0, zoom);
            quickZoomDialog.setVisible(true);
        }
    }
    
    /**
     * Action metode for playback knappen
     * Viser frem en dialog med muligheter for avspilling av kjøretur.
     * @param e
     */
    private void playbackButtonActionPerformed(ActionEvent e) {
        if (playBackDialog==null){
            Component owner = this;
            while (!(owner instanceof Window)){
                owner = owner.getParent();
            }
            playBackDialog = new JDialog((Window)owner);
            playBackDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            final PlaybackPanel panel = new PlaybackPanel(plugin.getMainMap());
            playBackDialog.add(panel);
            playBackDialog.pack();
            playBackDialog.setLocationRelativeTo(null);
            //stopper avspilling og fjerner referansen til tråder når dialogen lukkes
            playBackDialog.addWindowListener(new WindowAdapter(){
                public void windowClosed(WindowEvent e) {
                    playBackDialog = null;
                    panel.close();
                    panButton.setSelected(true);
                    setTool(ToolMode.PANORER);
                    
                }
            });
            playBackDialog.setVisible(true);
        }
    }
    
    private void playbackAlleButtonActionPerformed(ActionEvent e) {
        if (playStrekningerDialog==null){
            Component owner = this;
            while (!(owner instanceof Window)){
                owner = owner.getParent();
            }
            playStrekningerDialog = new JDialog((Window)owner);
            playStrekningerDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            final PlaybackStrekningerPanel panel = new PlaybackStrekningerPanel(plugin.getMainMap());
            playStrekningerDialog.add(panel);
            playStrekningerDialog.pack();
            playStrekningerDialog.setLocationRelativeTo(null);
            playStrekningerDialog.addWindowListener(new WindowAdapter(){
                public void windowClosed(WindowEvent e) {
                    playStrekningerDialog = null;
                    panel.close();
                    panButton.setSelected(true);
                    setTool(ToolMode.PANORER);
                    
                }
            });
            playStrekningerDialog.setVisible(true);
        }
        
    }
    
    /**
     * Action metode for målings knappen
     * Viser frem en dialog med målene på utvalget som gjøres
     * @param e
     */
    private void meassureButtonActionPerformed(ActionEvent e) {
    	setTool(ToolMode.LINJAL);
    }
    private void viapunktActionPerformed(ActionEvent e) {
    	setTool(ToolMode.VIAPUNKT);
    }
    
    /**
     * Action metode for zoom all data menyen
     * Zoomer kartet slik at all data som befinner seg på kartet vil bli synlig
     * @param e
     */
    private void zoomAllDataActionPerformed(ActionEvent e) {
    	plugin.getMainMap().calculateZoomForAllLayers(true);
    }
    /**
     * Action metode for zoom kontraktveinett
     * Zoomer kartet til å dekke kontraktveinettet som er valgt. 
     * @param e
     */
    private void zoomKontraktveinettActionPerformed(ActionEvent e) {
    	if (plugin.getCurrentKontrakt()!=null&&plugin.getCurrentKontrakt().getAreal()!=null){
    		final Bounds b = MapUtilities.convertArealToBounds(plugin.getCurrentKontrakt().getAreal());
        	if (plugin.getStatus().equals(MipssPlugin.Status.RUNNING))
        		plugin.getMainMap().ensureVisibilityForBounds(b);
    	}
    }
    
    protected void yrButtonActionPerformed(ActionEvent e) {
    	setTool(ToolMode.YR);
	}
    private void veiButtonActionPerformed(ActionEvent e){
    	setTool(ToolMode.VEI);
    }
    
    private Action getAreaButtonActionPerformed(){
    	return new AbstractAction("", IconResources.QMARK_ICON){
    		public void actionPerformed(ActionEvent e){
    			setTool(ToolMode.TRIONA_AREA_TOOL);
    		}
    	};
    	
    }
    private void veirefButtonActionPerformed(ActionEvent e){
    	JRoadSelectDialog dialog = new JRoadSelectDialog(plugin.getLoader().getApplicationFrame(), true,false);
    	dialog.setDriftkontrakt(plugin.getCurrentKontrakt());
    	dialog.setVeiSok(veiInfo);
		dialog.setVisible(true);
		
		if (dialog.isSet()){
			veiInfo = dialog.getVeiSok();
			if (veiInfo!=null){
				VeiTool tool = new VeiTool(plugin);
				Veinettveireferanse vr = new Veinettveireferanse();
				vr.setFylkesnummer(Long.valueOf(veiInfo.getFylkesnummer()));
				vr.setKommunenummer(Long.valueOf(veiInfo.getKommunenummer()));
				vr.setVeikategori(veiInfo.getVeikategori());
				vr.setVeinummer(Long.valueOf(veiInfo.getVeinummer()));
				vr.setVeistatus(veiInfo.getVeistatus());
				vr.setFraKm(Double.valueOf((double)veiInfo.getMeter()/1000d));
				vr.setTilKm(Double.valueOf((double)veiInfo.getMeter()/1000d));
				vr.setHp(Long.valueOf(veiInfo.getHp()));
				tool.setVeireferanse(vr);
			}
		}
    }
    
    public void setTool(ToolMode toolMode){
    	MouseListenerManager mm = plugin.getMainMap().getMouseListenerManager();
    	resetManager(mm);
    	switch(toolMode){
    		case PANORER : 
    			if (!ToolMode.PANORER.equals(this.toolMode)){
	    			mm.addListener(0, new PanMapTool(plugin.getMainMap()));
	    			mm.addListener(0, new SelectTool(plugin.getMainMap()));
	    			mm.addListener(0, new MoveLayerTool(plugin.getMainMap()));
    			}
    			break;
    		case ZOOM:
    			if (!ToolMode.ZOOM.equals(this.toolMode)){
    				mm.addListener(0, new SelectionTool(plugin.getMainMap()));
    			}
    			break;
    		case VEI : 
    			if (!ToolMode.VEI.equals(this.toolMode)){
    				Cursor veiCursor = Toolkit.getDefaultToolkit().createCustomCursor(IconResources.MAP_PIN_CURSOR.getImage(), new Point(6, 31), "PinCursor");
    		    	plugin.getMainMap().setCursor(veiCursor);
    				mm.addListener(0, new VeiTool(plugin));
    			}
    			break;
    		case YR : 
    			if (this.toolMode!=ToolMode.YR){
    				Cursor yrCursor = Toolkit.getDefaultToolkit().createCustomCursor(IconResources.TEMPERATURE_ICON.getImage(), new Point(0,31), "temp");
    				plugin.getMainMap().setCursor(yrCursor);
    				mm.addListener(0, new YrWeatherDataTool(plugin, plugin.getMainMap()));
    			}
    			break;
    		case LINJAL:
    			if (this.toolMode!=ToolMode.LINJAL){
	    			Cursor veiCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
			    	plugin.getMainMap().setCursor(veiCursor);
			    	mm.addListener(0, new RulerTool(plugin));
    			}
    			break;
    		case VIAPUNKT:
    			if (this.toolMode!=ToolMode.VIAPUNKT){
    				ViapunktTool lineTool =  new ViapunktTool(plugin);
    				mm.addListener(0, lineTool);
    			}
    			break;
    		case PLAYBACK:
    			plugin.getMainMap().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    			setTool(ToolMode.PANORER);
    			break;
    		case RESET:
    			plugin.getMainMap().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    			setTool(ToolMode.PANORER);
    			break;
    		case TRIONA_AREA_TOOL:
    			if (this.toolMode!=ToolMode.TRIONA_AREA_TOOL){
	    			plugin.getMainMap().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	    			mm.addListener(0, new AreaIdTool(plugin));
    			}
    		default : break;
    	}
    	this.toolMode = toolMode;
    }
    
    private void resetManager(MouseListenerManager mm){
    	mm.removeListener(PanMapTool.class);
    	mm.removeListener(SelectTool.class);
    	mm.removeListener(MoveLayerTool.class);
    	mm.removeListener(YrWeatherDataTool.class);
    	mm.removeListener(VeiTool.class);
    	mm.removeListener(SelectionTool.class);
    	mm.removeListener(RulerTool.class);
    	mm.removeListener(ViapunktTool.class);
    }
    
    class CopyToClipboardAction extends AbstractAction{
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public CopyToClipboardAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	plugin.getMainMap().copyToClipboard();
        }
    }
    class SaveAction extends AbstractAction{
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public SaveAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	BufferedImage dst = plugin.getMainMap().getMapImage();
			JFileChooser chooser = new JFileChooser();
			chooser.addChoosableFileFilter(new ImageFileFilter());
			String date = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT);
			date = date.replace(":", "");
			date = date.replace(" ", "_");
			date = date.replace(".", "");
			chooser.setSelectedFile(new File("kartutsnitt" + date + ".png"));
			int saved = chooser.showSaveDialog(ToolbarPanel.this);
			if (saved==JFileChooser.APPROVE_OPTION){
				File file = chooser.getSelectedFile();
				try {
					ImageIO.write(dst, "png", file);
				} catch (IOException ex) {
					log.error("actionPerformed", ex);
				}
			}
        }
    }
    
    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    class SearchAction extends AbstractAction{
    	private String gurl= "http://maps.google.com/maps/geo?q=%query&output=xml";
    	/**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public SearchAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent ev) {
        	
        	System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
    		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
    		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "error");
    		if (searchCombo.getSelectedItem()==null)
    			return;
        	String search = searchCombo.getSelectedItem().toString();
        	Object o = searchCombo.getSelectedItem();
        	searchModel.removeAllElements();
        	searchModel.addElement(o);
        	search = search.replaceAll(" ", "%20")+",%20norway";
        	String url = gurl.replaceAll("%query", search);
        	
    		DefaultHttpClient client = new DefaultHttpClient();
			HttpGet method = new HttpGet(url);
			try {
				HttpResponse response = client.execute((HttpUriRequest) method);
				InputStream rstream = response.getEntity().getContent();
				Document document = XMLHelper.createDocument(rstream);
	            NodeList liste = XMLHelper.getElementsByTagName(document, "Placemark");
	            List<Address> addressList = new ArrayList<Address>();
	            UTMUtils u = new UTMUtils();
	            for (int i=0;i<liste.getLength();i++){
	            	Address a = new Address();
	            	Node node = liste.item(i);
	            	a.setAddress(XMLHelper.getTagValue("address", node));
	            	String[] coords = XMLHelper.getTagValue("coordinates", node).split(",");
	            	Point2D p = new Point2D.Double();
        			u.convert2UTM(coords[1], coords[0], p);
        			MipssGeoPosition gp = new MipssGeoPosition(p.getX(), p.getY());
        			a.setPoint(gp);
        			if (a.getAddress().toLowerCase().indexOf("norway")!=-1){
		            	addressList.add(a);
		            	searchModel.addElement(a);
	            	}
	            }
	            
	            searchCombo.showPopup();
			} catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}
        	
        }
        public String convertStreamToString(InputStream is) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
    }
}
