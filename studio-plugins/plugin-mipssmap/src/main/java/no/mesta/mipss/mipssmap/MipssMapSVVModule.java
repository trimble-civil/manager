package no.mesta.mipss.mipssmap;

import javax.swing.JPanel;

/**
 * Modul som arver fra kartmodulen. Benyttes for visning av et tilpasset kart til Statens Vegvesen.
 * Det eneste som er forskjellig fra den vanlige kartmodulen er muligheten til å velge tidsrom, osv.
 * Her vil det kun være mulig å se data/produksjon fra de siste X minuttene (x hentes fra konfigparam (svv_hale)).
 */
public class MipssMapSVVModule extends MipssMapModule {

	@Override
	public JPanel getModuleGUI() {
		return super.getModuleGUI();
		
	}

	@Override
	public void initPluginGUI() {
		super.initPluginGUI();
		setSVVOptionsPanel(true);		
	}

	@Override
	public void shutdown() {
		super.shutdown();		
	}
}
