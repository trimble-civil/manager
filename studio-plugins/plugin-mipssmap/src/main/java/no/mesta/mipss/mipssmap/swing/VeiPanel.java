package no.mesta.mipss.mipssmap.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

import org.jdesktop.swingx.JXBusyLabel;

public class VeiPanel extends JPanel{
	private JTextField fylkeField = new JTextField();
	private JTextField kommuneField = new JTextField();
	private JTextField veiField = new JTextField();
	private JTextField hpField = new JTextField();
	private JTextField kmField = new JTextField();
	private JTextField textField = new JTextField();
	private JLabel infoLabel = new JLabel("Klikk i kartet");
	private JXBusyLabel label = new JXBusyLabel();
	private JScrollPane labelScroll = new JScrollPane();
	private JPanel labelPanel;
	
	public VeiPanel(){
		
		initGui();
		setLabel(false);
	}
	
	public void setLabel(boolean busy){
		label.setVisible(busy);
		labelPanel.setVisible(!busy);
		labelScroll.setViewportView(busy?label:labelPanel);
	}
	
	public static void main(String[] args) {
		JDialog dialog  = new JDialog();
		dialog.setLayout(new BorderLayout());
		dialog.setLocationRelativeTo(null);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.add(new VeiPanel());
		dialog.pack();
		dialog.setVisible(true);
	}
	private void initGui(){
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		label.setText("Søker etter vei");
		label.setBusy(true);
		
		labelScroll.setBorder(null);
		fylkeField.setEditable(false);
		kommuneField.setEditable(false);
		veiField.setEditable(false);
		hpField.setEditable(false);
		kmField.setEditable(false);
		
		JLabel fylkeLabel = new JLabel("Fylke:");
		JLabel kommuneLabel = new JLabel("Kommune:");
		JLabel veiLabel = new JLabel("Vei:");
		JLabel hpLabel = new JLabel("HP:");
		JLabel kmLabel = new JLabel("Km:");
		JLabel textLabel = new JLabel("Kortform:");
		
		Dimension small = new Dimension(0,5);
		Dimension medium = new Dimension(0,10);
		
		labelPanel = getPanel(infoLabel);
		add(labelScroll);
		
		add(Box.createRigidArea(medium));
		add(getHPanel(fylkeLabel, fylkeField));
		add(Box.createRigidArea(small));
		add(getHPanel(kommuneLabel, kommuneField));
		add(Box.createRigidArea(small));
		add(getHPanel(veiLabel, veiField));
		add(Box.createRigidArea(small));
		add(getHPanel(hpLabel, hpField));
		add(Box.createRigidArea(small));
		add(getHPanel(kmLabel, kmField));
		add(Box.createRigidArea(medium));
		add(getHPanel(textLabel, textField));
		add(Box.createRigidArea(medium));
		add(Box.createVerticalGlue());
	}
	
	private JPanel getPanel(JLabel lbl){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.add(Box.createRigidArea(new Dimension(10, 0)));
		panel.add(lbl);
		panel.add(Box.createHorizontalGlue());
		return panel;
	}
	
	private JPanel getHPanel(JLabel lbl, JTextField txt){
		txt.setPreferredSize(new Dimension(120,20));
		txt.setMaximumSize(new Dimension(Short.MAX_VALUE,20));
		txt.setMinimumSize(new Dimension(120,20));
		lbl.setPreferredSize(new Dimension(70, 20));
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.add(Box.createRigidArea(new Dimension(10, 0)));
		panel.add(lbl);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
		panel.add(txt);
		panel.add(Box.createRigidArea(new Dimension(10, 0)));
//		panel.add(Box.createHorizontalGlue());
		return panel;
	}
	
	public void setVeireferanse(Veinettveireferanse veiref){
		if (veiref==null){
			infoLabel.setText("Fant ingen veireferanse i området!");
			fylkeField.setText("");
			kommuneField.setText("");
			veiField.setText("");
			hpField.setText("");
			kmField.setText("");
			textField.setText("");
		}else{
			infoLabel.setText("Fant veireferanse!");
			fylkeField.setText(veiref.getFylkesnummer().toString());
			kommuneField.setText(veiref.getKommunenummer().toString());
			veiField.setText(veiref.getVeikategori()+veiref.getVeistatus()+veiref.getVeinummer());
			hpField.setText(veiref.getHp().toString());
			kmField.setText(veiref.getFraKm()+"-"+veiref.getTilKm());
			textField.setText(getTextFromVeiref(veiref));
		}
	}
	
	private String getTextFromVeiref(Veinettveireferanse v){
		return v.getFylkesnummer()+"/"+v.getKommunenummer()+" "+v.getVeikategori()+""+v.getVeistatus()+v.getVeinummer()+"("+v.getHp()+": "+v.getFraKm()+"-"+v.getTilKm()+")";

	}
	
	
}
