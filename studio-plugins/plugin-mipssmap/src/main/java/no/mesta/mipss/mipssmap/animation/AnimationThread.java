package no.mesta.mipss.mipssmap.animation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.swing.JTable;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.LabelLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.layer.LineListLayerCollection;
import no.mesta.mipss.mapplugin.layer.PeriodLineListLayer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTargetAdapter;
import org.jdesktop.animation.timing.interpolation.PropertySetter;


public class AnimationThread extends Thread{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private boolean pleaseWait;
    private boolean running = true;
    
    private PointLayerCollection punktData;
    private LineListLayerCollection strekningData;
    private MapPanel mapPanel;
    private int speed;
    private int punktIndex;
    private int wait;
    private AnimationDates synch;
	private final JTable punktDataTable;
	private boolean followPoint;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private boolean reversed;
    private boolean killed;
    public AnimationThread(PointLayerCollection punktData, JTable punktDataTable, LineListLayerCollection strekningData, MapPanel mapPanel, int speed, String name) {
        super(name);
    	this.punktData = punktData;
		this.punktDataTable = punktDataTable;
        this.strekningData = strekningData;
        this.mapPanel = mapPanel;
        this.speed = speed;
        log.debug("inited player (start:"+getFirst()+" end:"+getLast());
    }
    public void setWait(int wait){
        this.wait = wait;
    }
    public Date getFirst(){
    	return convertFromGMTtoCET(((ProdpunktPointLayer)punktData.getLayers().get(0)).getProdpunktGeoPosition().getProdpunktVO().getGmtTidspunkt());
    }
    public Date getLast(){
        List<ProdpunktPointLayer> l = (List<ProdpunktPointLayer>)punktData.getLayers();
        return convertFromGMTtoCET((l.get(l.size()-1)).getProdpunktGeoPosition().getProdpunktVO().getGmtTidspunkt());
    }
    /**
     * Konverterer en Date fra CET til GMT tidssone
     * @param date
     * @return
     * TODO denne logikken skal flyttes
     */
    private Date convertFromGMTtoCET(Date gmtDate){
    	
    	Calendar cc = new GregorianCalendar();
    	cc.setTime(gmtDate);
    	Calendar gmtCal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    	gmtCal.set(Calendar.YEAR, cc.get(Calendar.YEAR));
    	gmtCal.set(Calendar.MONTH, cc.get(Calendar.MONTH));
    	gmtCal.set(Calendar.DAY_OF_MONTH, cc.get(Calendar.DAY_OF_MONTH));
    	gmtCal.set(Calendar.HOUR_OF_DAY, cc.get(Calendar.HOUR_OF_DAY));
        gmtCal.set(Calendar.MINUTE, cc.get(Calendar.MINUTE));
        gmtCal.set(Calendar.SECOND, cc.get(Calendar.SECOND));
        gmtCal.set(Calendar.MILLISECOND, cc.get(Calendar.MILLISECOND));
    	
        Calendar c = new GregorianCalendar(TimeZone.getTimeZone("CET"));
        c.setTime(gmtCal.getTime());
    	return c.getTime();
        
    
    }
   
    public void setStrekningSynchronizer(AnimationDates synch){
        this.synch = synch;
    }
    private int strekningIndex = 0;
    public void run(){
        initPunkter();
        initStrekninger();
        PeriodLineListLayer strekning=null;
        
        
        if (strekningData!=null){
        	strekning =(PeriodLineListLayer) strekningData.getLayers().get(strekningIndex);
        }
        ProdpunktPointLayer prevPoint = (ProdpunktPointLayer)punktData.getLayers().get(0);
        Date prevDate = null;
        
        while ((punktIndex<punktData.getLayers().size()||running)&&!killed){
        	if (punktIndex>=punktData.getLayers().size()-1){
        		while (running&&!killed){
        			if (prevDate!=null&&prevDate.after(synch.getCurrentDate())){
        				break;
                	}
        			try {
                    	synchronized(synch){
                    		synch.wait();
                    	}
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
        		}
        	}
        	ProdpunktPointLayer currentPoint = null;
        	if (punktIndex>=punktData.getLayers().size()-1){
        		currentPoint = (ProdpunktPointLayer)punktData.getLayers().get(punktData.getLayers().size()-1);
        	}else
        		currentPoint = (ProdpunktPointLayer)punktData.getLayers().get(punktIndex);
            //hent start og stopptider for animasjonen
        	Date start = convertFromGMTtoCET(prevPoint.getProdpunktGeoPosition().getProdpunktVO().getGmtTidspunkt());
            Date stop = convertFromGMTtoCET(currentPoint.getProdpunktGeoPosition().getProdpunktVO().getGmtTidspunkt());
            
            //strekning..
            //vis strekninger etterhvert som tiden matcher med punktet
            if (strekningData!=null){
            	 PeriodLineListLayer forrige = strekning;
            	strekning = nesteStrekning(strekning,strekningData, prevPoint, currentPoint, start);
            	if (strekning==null)
            		strekning = forrige;
            }else{
            	nestePunkt(prevPoint, currentPoint);
            }
            
            nesteLabel(prevPoint, currentPoint, stop);
            //vent inntil pleaseWait = false
            synchronized(this){
                while (pleaseWait) {
                    try {
                        wait();
                    } catch (Exception e) {
                    }
                }   
            }
            
            //første gang tråden itererer skal den muligens vente, og det gjør den
            //inntil datoen for det første punktet kommer etter synch sin current dato        
            while (stop.after(synch.getCurrentDate())){
            	if (prevDate!=null&&prevDate.after(synch.getCurrentDate())){
            		break;
            	}
                try {
                	synchronized(synch){
                		synch.wait();
                	}
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //ruller bakover.. 
            if (prevDate!=null&&synch.getCurrentDate().before(prevDate)){
            	int index = getIndexAt(synch.getCurrentDate());
            	if (index!=-1){
            		hideAllBefore(synch.getCurrentDate());
            		punktIndex = index;
            		if (strekningData!=null){
            			strekning = (PeriodLineListLayer) strekningData.getLayers().get(strekningIndex>0?strekningIndex-1:0);
            		}
            	}
            }
            prevPoint = currentPoint;
            if (reversed)
            	punktIndex--;
            else{
            	punktIndex++;
            }
            
            prevDate = synch.getCurrentDate();
        }
    }
	private void initPunkter() {
		for (CollectionEntity l:punktData.getLayers()){
            l.setVisible(false);
        }
	}
	private void initStrekninger() {
		if (strekningData!=null){
        	for (CollectionEntity l:strekningData.getLayers()){
        		if (l instanceof LineListLayer){
        			((LineListLayer)l).setStroke(1);
        		}
        		l.setVisible(false);
        	}
        }
	}
	public void restoreStrekninger(){
		if (strekningData!=null){
        	for (CollectionEntity l:strekningData.getLayers()){
        		if (l instanceof LineListLayer){
        			((LineListLayer)l).restoreStroke();
        		}
        		l.setVisible(true);
        	}
        }
	}
    public void setReversed(boolean reversed){
		this.reversed = reversed;
    }
    private int getIndexAt(Date date){
    	int c = 0;
    	for (CollectionEntity l:punktData.getLayers()){
    		ProdpunktPointLayer pl=(ProdpunktPointLayer)l;
    		if (pl.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt().equals(date)||pl.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt().after(date)){
    			return c;
    		}
    		c++;
        }
    	return -1;
    }
    /**
     * Skjuler alle punktene før angitt dato, brukes hvis brukeren flytter tiden bakover i avspilleren
     * @param date
     */
    private void hideAllBefore(Date date){
    	for (CollectionEntity l:punktData.getLayers()){
    		ProdpunktPointLayer pl=(ProdpunktPointLayer)l;
    		if (pl.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt().after(date)){
    			l.setVisible(false);
    		}
        }
        
        if (strekningData!=null){
        	for (CollectionEntity l:strekningData.getLayers()){
        		PeriodLineListLayer pl =(PeriodLineListLayer)l;
        		if (pl.getStopp().after(date)){
        			if (pl.isVisible()){
        				pl.setVisible(false);
        				strekningIndex--;
        			}
        		}
        	}
        }
    }
    
	private void nesteLabel(ProdpunktPointLayer prevPoint, ProdpunktPointLayer currentPoint, Date stop) {
		String guid = prevPoint.getProdpunktGeoPosition().getProdpunktVO().getGuid();
		String labelId = "label:"+prevPoint.getProdpunktGeoPosition().getProdpunktVO().getDfuId();
		if (guid!=null){
			LabelLayer layer = (LabelLayer)mapPanel.getLayerHandler().getLayer(MipssMapLayerType.INSPEKSJONLABEL.getType(), "label"+guid);
			mapPanel.getLayerHandler().removeLayer(MipssMapLayerType.INSPEKSJONLABEL.getType(), "label"+guid);
			String value = layer.getLabelText()[0];
			String[] params = new String[]{value, dateFormat.format(stop)+" "+timeFormat.format(stop)};
			LabelLayer label = layer.clone();//new LabelLayer("label"+guid, currentPoint.getProdpunktGeoPosition().getGeoPosition(), params);
			label.setLabelText(params);
			label.setPosition(currentPoint.getProdpunktGeoPosition().getGeoPosition());
			mapPanel.getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONLABEL.getType(), label, false);
			
		}else{
			LabelLayer layer = (LabelLayer)mapPanel.getLayerHandler().getLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), labelId);
			mapPanel.getLayerHandler().removeLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), labelId);
			String value = layer.getLabelText()[0];
			String[] params = new String[]{value, dateFormat.format(stop)+" "+timeFormat.format(stop)};
			LabelLayer label = layer.clone();
			label.setLabelText(params);
			label.setPosition(currentPoint.getProdpunktGeoPosition().getGeoPosition());
			mapPanel.getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), label, false);
		}
	}
	
	private void nestePunkt(ProdpunktPointLayer prevPoint, ProdpunktPointLayer currentPoint) {
		long c1 = prevPoint.getProdpunktGeoPosition().getProdpunktVO().getGmtTidspunkt().getTime();
		long c2 = currentPoint.getProdpunktGeoPosition().getProdpunktVO().getGmtTidspunkt().getTime();
		int delta = (int)((c2-c1) /getSpeed());
		if (followPoint)
		    mapPanel.panTo(currentPoint.getProdpunktGeoPosition().getGeoPosition(), delta-10);
		else
		    mapPanel.ensureVisibilityForPosition(currentPoint.getProdpunktGeoPosition().getGeoPosition(), 500);
		
		mapPanel.getLayerHandler().unselectAll();
		mapPanel.getLayerHandler().fakeSelect(currentPoint, null);		
		prevPoint.setVisible(true);
		moveTable(0, punktIndex-1, punktIndex);
	}
	private PeriodLineListLayer strekning;
	private PeriodLineListLayer nesteStrekning(PeriodLineListLayer strekning, LineListLayerCollection strekningData,
			ProdpunktPointLayer prevPoint, 
			ProdpunktPointLayer currentPoint,Date start) {
		if (start.after(strekning.getStopp())){
		    strekning.setVisible(true);
		    if (strekningIndex>=strekningData.getLayers().size()-1)
		    	return null;
		    strekning = (PeriodLineListLayer)strekningData.getLayers().get(strekningIndex);
		    strekningIndex++;
//		    if (strekninger.hasNext())
//		    	strekning = (PeriodLineListLayer)strekninger.next();
		}
		Layer selected = prevPoint.getSelectionLayer();
		String id = currentPoint.getProdpunktGeoPosition().getProdpunktVO().getDfuId().toString();
		mapPanel.getLayerHandler().removeLayer(selected.getPreferredLayerType(), id);
		mapPanel.getLayerHandler().fakeSelect(currentPoint, id);
		return strekning;
	}
    
    public void stopRunning(){
        running = false;
        punktIndex=punktData.getLayers().size();
        killed = true;
        
    }
    public void setPunktIndex(int punktIndex){
        this.punktIndex = punktIndex;
    }
    private int getSpeed(){
        return speed;
    }
    /**
     * hvis denne kalles før trådene har startet og står i wait() vil starten 
     * av avspillingen se litt rar ut med et plutselig stort hopp
     * @param speed
     */
    public void setSpeed(int speed){
    	this.speed = speed;
    }
    /**
     * Flytt viewporten til tabellen og gjør et nytt valg.
     * @param time
     * @param fraIndex
     * @param tilIndex
     */
    public void moveTable(int time, int fraIndex, int tilIndex){
        PropertySetter ps = new PropertySetter(punktDataTable.getSelectionModel(), "leadSelectionIndex", new Integer[]{new Integer(fraIndex), new Integer(tilIndex)});
        Animator a = new Animator(time, ps);
        a.addTarget(new TimingTargetAdapter() {
            public void timingEvent(float fraction) {
            	punktDataTable.scrollRectToVisible(punktDataTable.getCellRect(punktDataTable.getSelectionModel().getLeadSelectionIndex()+3,-1, true));
            }});
        a.start();
    }

    public PointLayerCollection getPunktData() {
        return punktData;
    }

    public LineListLayerCollection getStrekningData() {
        return strekningData;
    }
	/**
	 * @return the pleaseWait
	 */
	public boolean isPleaseWait() {
		return pleaseWait;
	}
	/**
	 * @param pleaseWait the pleaseWait to set
	 */
	public void setPleaseWait(boolean pleaseWait) {
		this.pleaseWait = pleaseWait;
	}
    /**
     * True hvis punktet som er aktivt alltid skal være i senter av kartet.
     * @param followPoint
     */
    public void setFollowPoint(boolean followPoint) {
        this.followPoint = followPoint;
    }
}
