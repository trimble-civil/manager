package no.mesta.mipss.mipssmap.plugins.layer.kontraktvei;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import no.mesta.mipss.ui.ColorChooserPanel;

/**
 * Panel for editering av visningen av kontrakveinettet
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
public class KontraktveiPanel extends JPanel{
    
    private ColorChooserPanel colorChooserPanel;
    
    private JPanel buttonPanel;
    
    private JButton brukButton;
    private JButton okButton;
    private JButton avbrytButton;
    
    private Color valgtFarge;
    private Color forrigeFarge;
    private EventListenerList listenerList = new EventListenerList();
    protected transient ChangeEvent changeEvent = null;
    
    /**
     * Kontruktør
     * @param owner eieren av dialogen
     * @param forrigeFarge den fargen som returneres dersom man velger å avbryte
     */
    public KontraktveiPanel(Color forrigeFarge) {
        
        if (forrigeFarge==null) 
            forrigeFarge = Color.yellow;
        this.forrigeFarge = forrigeFarge;
        this.valgtFarge = forrigeFarge;
        initComponents();
    }
    
    private void initComponents() {
        colorChooserPanel = new ColorChooserPanel();
        buttonPanel = new JPanel();
        brukButton = new JButton();
        avbrytButton = new JButton();
        okButton = new JButton();

        GroupLayout colorChooserPanelLayout = new GroupLayout(colorChooserPanel);
        colorChooserPanel.setLayout(colorChooserPanelLayout);
        colorChooserPanelLayout.setHorizontalGroup(
            colorChooserPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGap(0, 238, Short.MAX_VALUE)
        );
        colorChooserPanelLayout.setVerticalGroup(
            colorChooserPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGap(0, 171, Short.MAX_VALUE)
        );

        brukButton.setText("Bruk");
        brukButton.setPreferredSize(new java.awt.Dimension(65, 23));
        brukButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        brukButtonAction(e);
                }
        });
        
        avbrytButton.setText("Avbryt");
        avbrytButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        avbrytButtonAction(e);
                }
        });
        
        okButton.setText("Ok");
        okButton.setPreferredSize(new java.awt.Dimension(65, 23));
        okButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        okButtonAction(e);
                }
        });
        
        JColorChooser chooser = new JColorChooser();
        AbstractColorChooserPanel[] panels = {colorChooserPanel};
        chooser.setChooserPanels(panels);
    
        
        colorChooserPanel.getColorSelectionModel().addChangeListener(new ColorChangeListener());
        colorChooserPanel.setCurrentColor(forrigeFarge);
        
        GroupLayout buttonPanelLayout = new GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(33, Short.MAX_VALUE)
                .addComponent(okButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(avbrytButton)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(brukButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(buttonPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(brukButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(avbrytButton)
                    .addComponent(okButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(colorChooserPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(colorChooserPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>
   
    /**
     * Legg til en ny lytter
     * @param l
     */
    public void addChangeListener(ChangeListener l) {
        listenerList.add(ChangeListener.class, l);
    }
    
    /**
     * Fjern en lytter fra listen
     * @param l
     */
    public void removeChangeListener(ChangeListener l) {
        listenerList.remove(ChangeListener.class, l);
    }
    
    /**
     * Gi beskjed til alle lyttere om at ny valgtFarge er valgt
     */
    protected void fireStateChanged(){
        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -=2 ) {
            if (listeners[i] == ChangeListener.class) {
                if (changeEvent == null) {
                    changeEvent = new ChangeEvent(this);
                }
                ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
            }
        }
    }
    
    /**
     * Action-metode for avbryt-knappen. Setter fargen tilbake til det
     * den var når dialogen ble åpnet, fyrer av et event og disposer dialogen
     * @param e
     */
    private void avbrytButtonAction(ActionEvent e){
        valgtFarge = forrigeFarge;
        fireStateChanged();
        Container parent = this;
        while (!(parent instanceof JDialog)){
            parent = parent.getParent();
        }
        
        ((JDialog)parent).setVisible(false);
        
    }
    /**
     * Action-metode for ok-knappen. Fyrer av et event og disposer dialogen
     * @param e
     */
    private void okButtonAction(ActionEvent e){
        fireStateChanged();
        Container parent = this;
        while (!(parent instanceof JDialog)){
            parent = parent.getParent();
        }
        
        ((JDialog)parent).setVisible(false);
    }
    /**
     * Action-metode for bruk-knappen. Fyrer av et event. 
     * @param e
     */
    private void brukButtonAction(ActionEvent e){
        forrigeFarge = valgtFarge;
        fireStateChanged();
    }
    /**
     * Returnerer valgte valgtFarge
     * @return
     */
    public Color getValgtFarge() {
        return valgtFarge;
    }
    
    /**
     * Set valgt farge
     * @param farge ny verdi
     */
    public void setValgtFarge(Color farge){
        valgtFarge = farge;
    }
    
    /**
     * Lytter som setter verdien til den valgte fargen når
     * brukeren gjør endring i fargevelgeren
     */
    class ColorChangeListener implements ChangeListener{
        /** {@inheritDoc} */
        public void stateChanged(ChangeEvent e) {
            DefaultColorSelectionModel colorModel = (DefaultColorSelectionModel)e.getSource();
            setValgtFarge(colorModel.getSelectedColor());
            fireStateChanged();
        }
    }
}
