package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.FeltLayer;
import no.mesta.mipss.mapplugin.layer.LabelLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.PinLayer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssfelt.InspeksjonQueryParams;
import no.mesta.mipss.mipssfelt.InspeksjonService;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.ProductionPlugin;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.task.HentInspeksjonTask;
import no.mesta.mipss.mipssmap.utils.ErrorHandler;
import no.mesta.mipss.mipssmap.utils.KonfigparamCache;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MipssMapProductionPlugin for å vise frem inspeksjoner
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class InspeksjonPlugin extends ProductionPlugin{
    private Logger log = LoggerFactory.getLogger(this.getClass());
	private InspeksjonQueryParams params;

    public InspeksjonPlugin(MipssMapWindowManager windowManager, MipssMapModule plugin) {
    	super(windowManager, plugin, ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.InspeksjonPlugin.button.text"),
    			ResourceUtil.getResource("produksjon.inspeksjon.tooltip"), ResourceUtil.getResource("produksjon.vis.inspeksjon.tooltip"));
    	getCheckButton().setAction(new ClickAction());
    }
    
    protected void buttonActionPerformed(ActionEvent e) {
    	if (getPlugin().getCurrentKontrakt()!=null){
            InspeksjonDialog dialog = (InspeksjonDialog)getWindowManager().getDialog(InspeksjonDialog.class);
            if (dialog==null){
                Window parent = SwingUtilities.windowForComponent(getCheckButton());
                dialog = new InspeksjonDialog(parent, this, getPlugin());
                dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.InspeksjonPlugin.dialog.title"));
                dialog.setMinimumSize(new Dimension(300, 350));
        		dialog.setSize(600, 400);
                dialog.setLocationRelativeTo(null);
                getWindowManager().addDialog(dialog);
            }
            dialog.setVisible(true);
            
        }
    }

    protected void checkActionPerformed(ActionEvent e){
    	if (!isLoaded()&&getCheckButton().getCheckBox().isSelected()){
    		Window parent = SwingUtilities.windowForComponent(getCheckButton());
    		if (new InspeksjonDialog(parent, this, getPlugin()).getInspeksjonPanel().hentInspeksjoner()){
    			setLoaded(true);
    			
    		}
    	}else{
    		if (getCheckButton().getCheckBox().isSelected()){
    			getPlugin().getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONPUNKT.getType());
    			getPlugin().getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONSTREKNING.getType());
    			getPlugin().getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONFUNN.getType());
    			getPlugin().getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONHENDELSE.getType());
    			getPlugin().getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONLABEL.getType());
    		}else{
    			getPlugin().getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONPUNKT.getType());
    			getPlugin().getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONSTREKNING.getType());
    			getPlugin().getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONFUNN.getType());
    			getPlugin().getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONHENDELSE.getType());
    			getPlugin().getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONLABEL.getType());
    		}
    	}
    }
    
   
	public void hentInspeksjoner(final InspeksjonQueryParams params){

		this.params = params;
    	if (!validateParams(params)){
    		return;
    	}
    	setLoaded(true);
    	setPrevFra(getProduksjonOptions().getDatoFra().getDate());
		setPrevTil(getProduksjonOptions().getDatoTil().getDate());
		inSynch();
		
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.INSPEKSJONPUNKT.getType());
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.INSPEKSJONSTREKNING.getType());
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.SELECTED.getType());
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.TEXTBOX.getType());
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.INSPEKSJONLABEL.getType());
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.INSPEKSJONHENDELSE.getType());
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.INSPEKSJONFUNN.getType());
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.INSPEKSJONFORSIKRINGSSKADE.getType());
    	getPlugin().getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.INSPEKSJONSKRED.getType());
    	
    	Date fraTidspunkt = getProduksjonOptions().getDatoFra().getDate();
    	Date tilTidspunkt = getProduksjonOptions().getDatoTil().getDate();
    	params.setDateFra(fraTidspunkt);
    	params.setDateTil(tilTidspunkt);
    	
    	InspeksjonService produksjon = BeanUtil.lookup(InspeksjonService.BEAN_NAME, InspeksjonService.class);
    	List<InspeksjonSokResult> inspeksjonGuids=null;
		try {
			inspeksjonGuids = produksjon.sokInspeksjoner(params);
		} catch (Exception e1) {
			ErrorHandler.showError(e1, ResourceUtil.getResource("mipssmap.feilmelding"));
			return;
		}
    	log.debug("fant:"+inspeksjonGuids.size()+" inspeksjoner..");
    	int c = 1;
    	Map<MipssGeoPosition, FeltLayer> funnmap = new HashMap<MipssGeoPosition, FeltLayer>();
    	for (InspeksjonSokResult ins :inspeksjonGuids){
    		final HentInspeksjonTask t = new HentInspeksjonTask(getPlugin(), params, ins, funnmap);
    		ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.InspeksjonPlugin.HentInspeksjonTask.title")+"("+c+" av "+inspeksjonGuids.size());
            workerPanel.setWorker(t);
            t.setProgressController(getPlugin().getProgressController());
            t.setProgressWorkerPanel(workerPanel);
            getPlugin().getProgressController().addWorkerPanel(workerPanel);
            
            Thread wait = new Thread("Hent inspeksjon(guid="+ins.getGuid()+")"){
            	public void run(){
            		Layer layer = null;
            		try{
            			layer = t.get();
            			if (layer!=null){
            				getPlugin().getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONSTREKNING.getType(), layer, true);
                    		getCheckButton().setChecked(true);
            			}
            			if (params.isHentPunkter()){
            				getPlugin().getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONPUNKT.getType(), t.getPointLayer(), true);
            				getCheckButton().setChecked(true);
            			}
            			if (params.isHentAvvik()){
            				for (Layer l:t.getAvvikLayer()){
            					getPlugin().getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONFUNN.getType(), l, true);
            				}
            			}
            			if (params.isHentHendelser()){
            				for (Layer l:t.getHendelseLayer()){
            					if (((FeltLayer)l).getRegistreringstype().equals(FeltLayer.REGISTRERINGSTYPE.HENDELSE))
            						getPlugin().getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONHENDELSE.getType(), l, true);
            				}
            			}
            			if (params.isHentForsikringsskade()){
            				for (Layer l:t.getForsiringsskadeLayer()){
            					if (((FeltLayer)l).getRegistreringstype().equals(FeltLayer.REGISTRERINGSTYPE.FORSIKRINGSSKADE))
            						getPlugin().getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONFORSIKRINGSSKADE.getType(), l, true);
            				}
            			}
            			if (params.isHentSkred()){
            				for (Layer l:t.getSkredLayer()){
            					if (((FeltLayer)l).getRegistreringstype().equals(FeltLayer.REGISTRERINGSTYPE.SKRED))
            						getPlugin().getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONSKRED.getType(), l, true);
            				}
            			}
            			LabelLayer labelLayer = t.getLabel();
            			getPlugin().getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONLABEL.getType(), labelLayer, false);
            			if (params.isVisMerkelapper())
            				labelLayer.setVisible(false);
            		} catch (ExecutionException e) {
                        e.printStackTrace();
                        String msg = ResourceUtil.getResource("mipssmap.feilmelding");
                        ErrorHandler.showError(e, msg);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } 
            	}
            };
            wait.start();
            c++;
    	}
    	KonfigparamCache.clearCache();
    }

	boolean validateParams(InspeksjonQueryParams params){
    	String error = "";
    	if (params.getPdaDfuIdentList()==null||params.getPdaDfuIdentList().length==0){
    		error+=ResourceUtil.getResource("InspeksjonPlugin.hentinspeksjon.nopda.description")+"\n";
    	}
    	if ((params.getProsesser()==null||params.getProsesser().length==0)){
    		error +=ResourceUtil.getResource("InspeksjonPlugin.hentinspeksjon.noprosess.description");
    	}
    	if (error.length()!=0){
    		JOptionPane.showMessageDialog(getPlugin().getLoader().getApplicationFrame(),error,
					ResourceUtil.getResource("InspeksjonPlugin.hentinspeksjon.missing.title"),
					JOptionPane.INFORMATION_MESSAGE);
    		return false;
    	}
    	return true;
    }
	
	@SuppressWarnings("serial")
	private class ClickAction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e){
			refresh();
		}
	}

	@Override
	public void refresh() {
		if (isEnabled()){
			if (!isTimeSynched()){
				if (!isLoaded()){
					getCheckButton().getCheckBox().setSelected(true);
					checkActionPerformed(null);
				}else{
					if (params!=null){
						params.setDateFra(getProduksjonOptions().getDatoFra().getDate());
						params.setDateTil(getProduksjonOptions().getDatoTil().getDate());
						hentInspeksjoner(params);
					}else{
						setLoaded(false);
						getCheckButton().getCheckBox().setSelected(true);
						checkActionPerformed(null);
					}
				}
			}
		}
		
	}
}
