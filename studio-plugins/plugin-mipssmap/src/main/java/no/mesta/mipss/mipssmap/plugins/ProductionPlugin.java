package no.mesta.mipss.mipssmap.plugins;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import javax.swing.JDialog;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.ProduksjonOptions;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.resources.images.IconResources;

public abstract class ProductionPlugin implements MipssMapProductionPlugin{
	private JCheckButton button;
	private boolean loaded;
	private ProduksjonOptions options;
	private Date prevFra;
	private Date prevTil;
	private boolean timeSynched;
	private final MipssMapWindowManager windowManager;
	private final MipssMapModule plugin;
	private boolean enabled;
	
	public ProductionPlugin(MipssMapWindowManager windowManager, MipssMapModule plugin, String buttonText, String buttonTooltip, String checkTooltip){
		this.windowManager = windowManager;
		this.plugin = plugin;
		button = new JCheckButton(buttonText);
        button.getButton().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                buttonActionPerformed(e);
            }
        });
        button.getCheckBox().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if (!isLoaded()){
                	button.setIcon(IconResources.REFRESH_VIEW_ICON);
                }
            	checkActionPerformed(e);
            }
        });
        button.getButton().setToolTipText(buttonTooltip);
        button.getCheckBox().setToolTipText(checkTooltip);
        
        button.setIconVisible(true);
        reset();
	}
	
	@Override
	public JCheckButton getCheckButton() {
		return button;
	}

	@Override
	public JDialog getDialog() {
		return null;
	}

	@Override
	public void reset() {
		button.getCheckBox().setSelected(false);
		setLoaded(false);
		timeSynched=false;
		prevFra = null;
		prevTil = null;
		button.setIconTooltip("");
		
	}

	public void setProduksjonOptions(final ProduksjonOptions options) {
		this.options = options;
		options.getDatoFra().addPropertyChangeListener(new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (isLoaded()){
					if (options.getDatoFra().getDate().equals(getPrevFra())&&options.getDatoTil().getDate().equals(getPrevTil())){
						inSynch();
					}else{
						outOfSynch();
					}
				}
			}
		});
		options.getDatoTil().addPropertyChangeListener(new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (isLoaded()){
					if (options.getDatoTil().getDate().equals(getPrevTil())&&options.getDatoFra().getDate().equals(getPrevFra())){
						inSynch();	
					}else{
						outOfSynch();
					}
				}
			}
			
		});
	}
	public ProduksjonOptions getProduksjonOptions(){
		return options;
	}
	
	protected abstract void checkActionPerformed(ActionEvent e);
	protected abstract void buttonActionPerformed(ActionEvent e);

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setPrevFra(Date prevFra) {
		this.prevFra = prevFra;
	}

	public Date getPrevFra() {
		return prevFra;
	}

	public void setPrevTil(Date prevTil) {
		this.prevTil = prevTil;
	}

	public Date getPrevTil() {
		return prevTil;
	}
	
	public MipssMapWindowManager getWindowManager() {
		return windowManager;
	}

	public MipssMapModule getPlugin() {
		return plugin;
	}

	protected void inSynch(){
		button.setIconTooltip("<html>Utvalgskriteriene for dette valget <br>stemmer overens med valgt dato.");
		button.setIcon(IconResources.YES_ICON);
		timeSynched=true;
	}
	
	protected void outOfSynch(){
		String fraDato = MipssDateFormatter.formatDate(prevFra, MipssDateFormatter.LONG_DATE_TIME_FORMAT);
		String tilDato = MipssDateFormatter.formatDate(prevTil, MipssDateFormatter.LONG_DATE_TIME_FORMAT);
		button.setIconTooltip("<html>Dato for utvalg:<br>fra:"+fraDato+"<br>til: "+tilDato+"</html>");
		button.setIcon(IconResources.REFRESH_VIEW_ICON);
		timeSynched=false;
	}

	public boolean isTimeSynched() {
		return timeSynched;
	}
	
	public void setEnabled(boolean enabled){
		this.enabled = enabled;
		getCheckButton().setEnabled(enabled);
		if (enabled){
			getCheckButton().setIcon(IconResources.REFRESH_VIEW_ICON);
		}
		else{
			getCheckButton().setIcon(ImageUtil.toGrayScale(IconResources.REFRESH_VIEW_ICON));
		}
	}
	public boolean isEnabled(){
		return enabled;
	}
}
