package no.mesta.mipss.mipssmap.model;

import java.util.List;

import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

public class RodeTableModel<T> extends MipssRenderableEntityTableModel{
    public RodeTableModel(Class<T> clazz, List<T> entities, String... beanColumns) {
        super(clazz, entities, beanColumns);
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex==1||columnIndex==0;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex==3){
            Object o = super.getValueAt(rowIndex, columnIndex);
            if (o instanceof Long){
                Long value = (Long)o;
                if (value!=0)
                    return "Nei";
                else
                    return "Ja";       
            }
        }
        return super.getValueAt(rowIndex, columnIndex);
    }
    
    public Object getRowObject(int rowIndex){
        return this.getEntities().get(rowIndex);
    }
}
