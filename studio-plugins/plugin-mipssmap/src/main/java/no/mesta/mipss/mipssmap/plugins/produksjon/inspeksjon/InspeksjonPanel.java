package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssfelt.InspeksjonQueryParams;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.PunktStrekningPanel;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.roadpicker.KontraktveinettDialog;

/**
 * Hovedpanelet for å gjøre utvalg på å vise frem inspeksjoner i kart.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class InspeksjonPanel extends JPanel{
	private InspeksjonPlugin plugin;
	private MipssMapModule module;
	private ProsessPDAPanel prosessPdaPanel;

	private no.mesta.mipss.mipssfelt.InspeksjonQueryParams params = new no.mesta.mipss.mipssfelt.InspeksjonQueryParams();
	
	private FunnHendelseUtvalgPanel funnHendelsePanel;
	private PunktStrekningPanel punktStrekningPanel;
	
	public InspeksjonPanel(MipssMapModule module, InspeksjonPlugin plugin){
		setLayout(new BorderLayout());
		this.plugin = plugin;
		this.module = module;
		initGui();
	}
	
	private void initGui(){
		JPanel mainPanel = getVerticalPanel();
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		prosessPdaPanel = new ProsessPDAPanel(module);
		funnHendelsePanel = new FunnHendelseUtvalgPanel();
		punktStrekningPanel = new PunktStrekningPanel(false);
		punktStrekningPanel.getPunktCheck().setSelected(false);
		punktStrekningPanel.getAlleRadio().setEnabled(false);
		punktStrekningPanel.getKunSisteRadio().setEnabled(false);
		punktStrekningPanel.getStrekningCheck().setSelected(true);
		
		punktStrekningPanel.getPunktCheck().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (punktStrekningPanel.getPunktCheck().isSelected()){
					module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONPUNKT.getType());
				}else{
					module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONPUNKT.getType());
				}
			}
		});
		punktStrekningPanel.getStrekningCheck().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (punktStrekningPanel.getStrekningCheck().isSelected()){
					module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONSTREKNING.getType());
				}else{
					module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONSTREKNING.getType());
				}
			}
		});
		funnHendelsePanel.getVisMerkelappCheck().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (funnHendelsePanel.getVisMerkelappCheck().isSelected()){
					module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONLABEL.getType());
				}else{
					module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONLABEL.getType());
				}
					
			}
		});
		funnHendelsePanel.getFunnCheck().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (funnHendelsePanel.getFunnCheck().isSelected()){
					module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONFUNN.getType());
				}else{
					module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONFUNN.getType());
				}
					
			}
		});
		funnHendelsePanel.getRskjemaCheck().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (funnHendelsePanel.getRskjemaCheck().isSelected()){
					module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONHENDELSE.getType());
					module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONFORSIKRINGSSKADE.getType());
					module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.INSPEKSJONSKRED.getType());
				}else{
					module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONHENDELSE.getType());
					module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONFORSIKRINGSSKADE.getType());
					module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.INSPEKSJONSKRED.getType());
				}
					
			}
		});
		mainPanel.add(prosessPdaPanel);
		mainPanel.add(punktStrekningPanel);
		punktStrekningPanel.add(funnHendelsePanel);
		
		add(mainPanel, BorderLayout.CENTER);
		add(getButtonPanel(new OkAction("", null), new CancelAction("", null), new ApplyAction("", null), new GoToMipssFieldAction(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.InspeksjonPanel.gotoButton.title"), null)), BorderLayout.SOUTH);
	}
	
	
	
	
	private JPanel getVerticalPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		return panel;
	}

	
	
	/**
	 * Henter ut standard knapperad
	 * @param okAction action for hva som skjer ved klikk på ok
	 * @param cancelAction action for hva som skjer ved klikk på avbryt
	 * @param applyAction action for hva som skjer ved klikk på bruk
	 * @return
	 */
	private JPanel getButtonPanel(AbstractAction okAction, AbstractAction cancelAction, AbstractAction applyAction, AbstractAction otherAction){
		okAction.putValue(Action.NAME, ResourceUtil.getResource("okButton.text"));
		cancelAction.putValue(Action.NAME, ResourceUtil.getResource("avbrytButton.text"));
		applyAction.putValue(Action.NAME, ResourceUtil.getResource("brukButton.text"));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JButton velgVeiButton = new JButton(new VelgVeiAction(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.OptionsPanel.velgveiButton.text"), IconResources.VEI_ICON));
		JButton gotoButton = new JButton(otherAction);
		gotoButton.setEnabled(false);
		gotoButton.setToolTipText(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.InspeksjonPanel.gotoButton.tooltip"));
		buttonPanel.add(gotoButton);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(velgVeiButton);
		buttonPanel.add(Box.createHorizontalStrut(15));
		buttonPanel.add(new JButton(applyAction));
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(new JButton(cancelAction));
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(new JButton(okAction));
		return buttonPanel;
	}
	
	/**
	 * 
	 */
	public boolean hentInspeksjoner(){
		boolean punkter = punktStrekningPanel.getPunktCheck().isSelected();
	    boolean strekninger = punktStrekningPanel.getStrekningCheck().isSelected();
	    boolean kunSistePunkt = punktStrekningPanel.getKunSisteRadio().isSelected();
	    boolean hentAvvik = funnHendelsePanel.getFunnCheck().isSelected();
	    boolean hentRskjema = funnHendelsePanel.getRskjemaCheck().isSelected();
	    
		params.setProsesser(prosessPdaPanel.getAlleValgteProsesser());
		params.setPdaDfuIdentList(prosessPdaPanel.getAlleValgteEnheter());
		
		params.setHentPunkter(punkter);
		params.setHentStrekninger(strekninger);
		params.setKunSistePunkt(kunSistePunkt);
		params.setValgtKontrakt(module.getCurrentKontrakt());
		params.setIngenProsesser(prosessPdaPanel.isIngenProsesserValgt());
		params.setHentAvvik(hentAvvik);
		
		params.setHentHendelser(hentRskjema);
		params.setHentForsikringsskade(hentRskjema);
		params.setHentSkred(hentRskjema);
		
		params.setVisMerkelapper(funnHendelsePanel.getVisMerkelappCheck().isSelected());
		if (plugin.validateParams(params)){
			plugin.hentInspeksjoner(params);
			return true;
		}
		return false;	
		
	}
	
	private class OkAction extends AbstractAction{
		public OkAction(String text, ImageIcon icon){
			super(text, icon);
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			boolean ok = hentInspeksjoner();
			Container parent = InspeksjonPanel.this;
	        while (!(parent instanceof JDialog)){
	            parent = parent.getParent();
	        }
	        if (ok)
	        	((JDialog)parent).setVisible(false);
		}
	}
	
	private class CancelAction extends AbstractAction{
		public CancelAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Container parent = InspeksjonPanel.this;
	        while (!(parent instanceof JDialog)){
	            parent = parent.getParent();
	        }
	        ((JDialog)parent).setVisible(false);
		}
	}
	private class ApplyAction extends AbstractAction{
		public ApplyAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			hentInspeksjoner();
		}
	}
	
	private class GoToMipssFieldAction extends AbstractAction{
		public GoToMipssFieldAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			//TODO implement
		}
	}
	
	private class VelgVeiAction extends AbstractAction{
		public VelgVeiAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			
			KontraktveinettDialog veiVelger = new KontraktveinettDialog(module.getLoader().getApplicationFrame(), Resources.getResource("label.velgVei"), module.getCurrentKontrakt(), false, false, false, true, false);
			veiVelger.setLocationRelativeTo(module.getLoader().getApplicationFrame());
			veiVelger.setSelectedVeireferanser(params.getVeiListe());
			veiVelger.setVisible(true);
			if(!veiVelger.isCancelled())
				params.setVeiListe(veiVelger.getSelectedVeireferanser());
		}
	}
}
