package no.mesta.mipss.mipssmap.swing.treetable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");} catch (Exception e) {e.printStackTrace();}
		
		CheckButtonNode node = new CheckButtonNode("root");
		
		node.add(new CheckButtonNode("test2"));
		node.add(new CheckButtonNode("test3"));
		
		final CheckButtonNode n = new CheckButtonNode("kikkaboo", new JButton("Valg"));
		
		JButton btn = new JButton("Valg");
		btn.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				n.setSelected(true);
				System.out.println("ACTION");	
			}
		});
		
		n.add(new CheckButtonNode("node", btn));
		node.add(n);
		
		CheckButtonNode n2 = new CheckButtonNode("kikkaboo2", new JButton("Valg"));
		n.add(n2);
		
		CheckTreeTableModel model = new CheckTreeTableModel(node);
		JCheckTreeTable tree = new JCheckTreeTable(model);
		tree.getColumn(1).setMaxWidth(100);//TODO det er ikke sikkert dette vil stemme i fremtiden...
		tree.setSelectionBackground(Color.white);
		tree.setRowHeight(22);
		final CheckTreeManager checkTreeManager = new CheckTreeManager(tree);
		model.setView(tree);
		
		checkTreeManager.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener(){

			@Override
			public void valueChanged(TreeSelectionEvent e) {
				System.out.println(e.getSource()+" changed");
				TreePath checkedPaths[] = checkTreeManager.getSelectionModel().getSelectionPaths();
				if (checkedPaths!=null){
					for (TreePath p:checkedPaths){
						CheckButtonNode node = (CheckButtonNode)p.getLastPathComponent();
						System.out.println(node);
					}
				}
				System.out.println();
			}
			
		});
		
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setSize(300,300);
		frame.setLocationRelativeTo(null);
		frame.add(tree);
		frame.setVisible(true);
		
	}

}
