package no.mesta.mipss.mipssmap.utils;

import java.awt.Dimension;

import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;


/**
 * Et panel som inneholder JCheckButton's. Dette panelet har en BoxLayout som legger ut komponentene
 * fra top til bunn og med en avstand på 4 px.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class JCheckButtonPanel extends JPanel{
    
    private Dimension panelSize;
    /**
     * Konstruktør, legger alle komponentene i buttonList til på panelet
     * @param buttonList liste med JCheckButton komponenter. 
     */
    public JCheckButtonPanel(List<JCheckButton> buttonList) {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));    
        initGui(buttonList);
    }
    
    /**
     * Standard kontstruktør, 
     * dersom denne benyttes må addComponents(List<JCheckButton> list) kalles for
     * å legge til knapper
     */
    public JCheckButtonPanel(){
    }
    
    /**
     * Setter listen med JCheckButton komponenter. Denne metoden
     * fjerner først alt fra JPanel'et og setter det så opp på nytt.
     * 
     * @param buttonList listen med JCheckButton komponentene
     */
    public void setCheckButtonList(List<JCheckButton> buttonList){
        removeAll();
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        initGui(buttonList);
    }
    
    /**
     * Legger ut alle knappene på panelet
     * @param buttonList listen med JCheckButton komponentene
     */
    private void initGui(List<JCheckButton> buttonList){
        panelSize = new Dimension(200, buttonList.size()*33);
        setPreferredSize(panelSize);
        setMinimumSize(panelSize);
        setMaximumSize(panelSize);
        
        Dimension spacing = new Dimension(0,4);
        for (JCheckButton b:buttonList){
            add(b);
            add(Box.createRigidArea(spacing));
        }
//        add(Box.createVerticalGlue());
    }

    public Dimension getPanelSize() {
        return panelSize;
    }
}
