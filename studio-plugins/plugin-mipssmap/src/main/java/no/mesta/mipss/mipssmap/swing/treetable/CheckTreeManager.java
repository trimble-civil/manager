package no.mesta.mipss.mipssmap.swing.treetable;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import org.jdesktop.swingx.JXTreeTable;

public class CheckTreeManager extends MouseAdapter implements TreeSelectionListener{ 
    private CheckTreeSelectionModel selectionModel; 
    private JXTreeTable tree = new JXTreeTable();
    int hotspot = new JCheckBox().getPreferredSize().width; 
 
    public CheckTreeManager(JXTreeTable tree){ 
        this.tree = tree; 
        selectionModel = new CheckTreeSelectionModel(tree.getTreeTableModel()); 
        
        //trenger et tre for å rendre trestrukturen før checkbox, ikon og tekst
        JTree tre = new JTree();
        tree.setTreeCellRenderer(new CheckTreeCellRenderer(tre.getCellRenderer(), selectionModel)); 
        tree.addMouseListener(this); 
        selectionModel.addTreeSelectionListener(this); 
    } 
 
    public void mouseClicked(MouseEvent e){ 
        TreePath path = tree.getPathForLocation(e.getX(), e.getY());
        if(path==null) 
            return;
        int row = tree.getRowForPath(path);
        
        if(e.getX()>tree.getColumn(0).getWidth()) 
            return; 
        
        if (e.getX()<10)
        	return;
        boolean selected = selectionModel.isPathSelected(path, true); 
        selectionModel.removeTreeSelectionListener(this);
        tree.expandPath(path);
        try{ 
            if(selected) 
                selectionModel.removeSelectionPath(path); 
            else 
                selectionModel.addSelectionPath(path); 
        } finally{ 
            selectionModel.addTreeSelectionListener(this); 
            tree.updateUI();
        } 
    } 
 
    public CheckTreeSelectionModel getSelectionModel(){ 
        return selectionModel; 
    } 
 
    public void valueChanged(TreeSelectionEvent e){ 
//        tree.treeDidChange(); 
    } 
}
