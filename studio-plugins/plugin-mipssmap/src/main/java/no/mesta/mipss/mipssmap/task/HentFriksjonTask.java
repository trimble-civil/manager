package no.mesta.mipss.mipssmap.task;

import java.awt.Color;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.CollectionPainter;
import no.mesta.mipss.mapplugin.layer.LabelLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.KonfigparamCache;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

@SuppressWarnings("unchecked")
public class HentFriksjonTask extends MipssMapTask{
	private ProduksjonService service = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);

	private final Prodstrekning ps;
	private LabelLayer label;
	
	private final MipssMapModule plugin;


	private PointLayerCollection pointLayers;
	public HentFriksjonTask(MipssMapModule plugin, Prodstrekning ps){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.ps = ps;
		
	}
	@Override
	protected Layer doInBackground() throws Exception {
		List<FriksjonVO> friksjon = service.hentFriksjonsdataForProdstrekning(ps);
		getProgressWorkerPanel().setBarIndeterminate(true);
    	getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
    	
    	LayerBuilder lb = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
    	Bounds bounds = new Bounds();
    	List<CollectionEntity> region = lb.getPointlayersFromFriksjonVOList(friksjon, bounds);
    	FriksjonVO vo = friksjon.get(0);
    	
    	pointLayers = new PointLayerCollection(vo.getDfuId()+""+vo.getDate()+"", region, bounds, true);
//    	if (region.size()>20){
//        	pointLayers.setCollectionPainter(new CollectionPainter(pointLayers.getLayers(), pointLayers));
//        }else{
        	List<ProdpunktPointLayer> ls = (List<ProdpunktPointLayer>)pointLayers.getLayers();
        	for (ProdpunktPointLayer pl:ls){
        		pl.setInitGraphics(true);
        	}
//        }
    	label = createLabel(ps, ((ProdpunktPointLayer)region.get(0)).getProdpunktGeoPosition().getGeoPosition(), vo);
		return pointLayers;
	}
	/**
	 * Lager en label til inspeksjonen
	 * @param i
	 * @param pos
	 * @return
	 */
	private LabelLayer createLabel(Prodstrekning ps, MipssGeoPosition pos, FriksjonVO vo){
		String title = vo.getEksterntNavn();
		String[] text = new String[]{
				title, 
				MipssDateFormatter.formatDate(ps.getFraTidspunkt(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT)
		};
		String v1 = KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_MESTA);
		String v2= KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_TEXT_MESTA);
		Color fill = ColorUtil.setAlpha(ColorUtil.fromHexString(v1), 140);
		Color border = ColorUtil.fromHexString(v1);
		Color textColor = ColorUtil.fromHexString(v2);
		LabelLayer l =  new LabelLayer("label:"+ps.getDfuId(), pos, text, fill, border, textColor);
		l.setPreferredLayerType(MipssMapLayerType.PRODUKSJONLABEL.getType());
		return l;
	}
	public PointLayerCollection getLayer(){
		return pointLayers;
	}
	public LabelLayer getLabelLayer(){
		return label;
	}

}
