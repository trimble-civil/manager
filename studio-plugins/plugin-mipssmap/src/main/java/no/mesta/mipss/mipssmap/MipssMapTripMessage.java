package no.mesta.mipss.mipssmap;

import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.plugin.PluginMessage;

import java.awt.*;
import java.util.Map;

public class MipssMapTripMessage extends PluginMessage<String,String> {

    private Trip trip;

    public MipssMapTripMessage(Trip trip) {
        this.trip = trip;
    }

    public Trip getTrip() {
        return trip;
    }

    @Override
    public void processMessage() {

    }

    @Override
    public Map<String, String> getResult() {
        return null;
    }

    @Override
    public Component getMessageGUI() {
        return null;
    }

}