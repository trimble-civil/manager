package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.applikasjon.KonfigparamPK;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.table.ColorRenderer;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

import org.jdesktop.swingx.JXTable;

/**
 * 
 * Panel for å gjøre utvalg på produksjonstyper når man skal vise produksjon i kart 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ProduksjonstyperPanel extends JPanel{
	private static final int TOMKJORING_ID=98;
	private JXTable prodtypeTable;
	private JScrollPane prodtypeScroll;
	private MipssRenderableEntityTableModel<ProdtypeTableObject> prodtypeTableModel;
	private JCheckBox alleProdtyperCheck;
	
	private JCheckBox allProdCheck;
	private JCheckBox tomkjoringProdCheck;
	
	private PunktStrekningPanel punktStrekningPanel;
	private ProdtypeTableObject tomkjoring;
	
	private ProduksjonPlugin plugin;
	public ProduksjonstyperPanel(ProduksjonPlugin plugin){
		this.plugin = plugin;
		setLayout(new BorderLayout());
		initGui();
	}
	
	/**
	 * Initialiser gui komponentene.
	 */
	private void initGui(){
		JPanel mainPanel = createMainPanel();
		mainPanel.add(createAlleProdtyperPanel());	
		mainPanel.add(createProdtypeTablePanel());
		mainPanel.add(createAnnenProdtypePanel());
		mainPanel.add(createTomkjoringProdtypePanel());
		allProdCheck.setSelected(true);
		tomkjoringProdCheck.setSelected(true);
		
		punktStrekningPanel = new PunktStrekningPanel(true);
		punktStrekningPanel.getPunktCheck().addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				if (punktStrekningPanel.getPunktCheck().isSelected()){
					plugin.setProduksjonVisible(true, MipssMapLayerType.PRODUKSJONPUNKT);
				}else{
					plugin.setProduksjonVisible(false, MipssMapLayerType.PRODUKSJONPUNKT);
				}
			}
		});
		punktStrekningPanel.getStrekningCheck().addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (punktStrekningPanel.getStrekningCheck().isSelected()){
					plugin.setProduksjonVisible(true, MipssMapLayerType.PRODUKSJONSTREKNING);
				}else{
					plugin.setProduksjonVisible(false, MipssMapLayerType.PRODUKSJONSTREKNING);
				}
				
			}
		});
		punktStrekningPanel.getLabelCheck().addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (punktStrekningPanel.getLabelCheck().isSelected()){
					plugin.setProduksjonVisible(true, MipssMapLayerType.PRODUKSJONLABEL);
				}else{
					plugin.setProduksjonVisible(false, MipssMapLayerType.PRODUKSJONLABEL);
				}
				
			}
		});
		mainPanel.add(punktStrekningPanel);
		add(mainPanel, BorderLayout.CENTER);
		
	}
	/**
	 * Lager et horisontalt panel for annen produksjon valg
	 * @return
	 */
	private JPanel createAnnenProdtypePanel(){
		Color color = ColorUtil.fromHexString(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "fargeProduksjon").getVerdi());
		JPanel panel = createHorizPanel();
		allProdCheck = new JCheckBox();
		JLabel annenProdLabel = new JLabel(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.allProdCheck.text"));
		JLabel colorLabel = new JLabel("          ");
		colorLabel.setOpaque(true);
		colorLabel.setBackground(color);
		colorLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		
		panel.add(allProdCheck);
		panel.add(colorLabel);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
		panel.add(annenProdLabel);
		
		return panel;
	}
	
	/**
	 * Lager et horisontalt panel for annen produksjon valg
	 * @return
	 */
	private JPanel createTomkjoringProdtypePanel(){
		Color color = ColorUtil.fromHexString(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "fargeTomkjoring").getVerdi());
		JPanel panel = createHorizPanel();
		tomkjoringProdCheck = new JCheckBox();
		JLabel tomkjoringLabel = new JLabel(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.tomkjoringProdCheck.text"));
		JLabel colorLabel = new JLabel("          ");
		colorLabel.setOpaque(true);
		colorLabel.setBackground(color);
		colorLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		panel.add(tomkjoringProdCheck);
		panel.add(colorLabel);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
		panel.add(tomkjoringLabel);
		
		return panel;
	}
	/**
	 * Lager et panel for en tabell med produksjonstyper
	 * @return
	 */
	private JPanel createProdtypeTablePanel(){
		JPanel panel = createHorizPanel();
		prodtypeScroll = getProdtypeScroll();
		panel.add(prodtypeScroll);
		return panel;
	}
	
	/**
	 * Lager et panel for valg av alle produksjontyper
	 * 
	 */
	private JPanel createAlleProdtyperPanel(){
		JPanel panel = createHorizPanel();
		alleProdtyperCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.alleProdtyperCheck.text"));
		panel.add(alleProdtyperCheck);
		return panel;
	}
	
	/**
	 * Lager hovedpanelet
	 * @return
	 */
	private JPanel createMainPanel(){
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		return mainPanel;
	}
	
	/**
	 * Lager et horisontalt panel som har satt default verdier.
	 * @return
	 */
	private JPanel createHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
		return panel;
	}
	
	private JScrollPane getProdtypeScroll(){
		prodtypeTable = new JXTable();
		ProduksjonService service = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		List<Prodtype> prodtypeList = service.getProdtypeList();
		List<ProdtypeTableObject> typeList = new ArrayList<ProdtypeTableObject>();
		for (Prodtype p:prodtypeList){
			if (p.getId().intValue()!=TOMKJORING_ID){
				typeList.add(new ProdtypeTableObject(p));
			}else
				tomkjoring = new ProdtypeTableObject(p);
		}
		String[] columns = new String[]{"valgt", "prodtypeNavn", "farge", "sesong"};
		prodtypeTableModel = new MipssRenderableEntityTableModel<ProdtypeTableObject>(ProdtypeTableObject.class, new int[]{0}, columns); 
		prodtypeTableModel.getEntities().clear();
		prodtypeTableModel.getEntities().addAll(typeList);
		prodtypeTable.setModel(prodtypeTableModel);
		prodtypeTable.getColumn(0).setMaxWidth(50);
		prodtypeTable.setDefaultRenderer(Color.class, new ColorRenderer(true));
		
		return new JCheckTablePanel<ProdtypeTableObject>(prodtypeTable, prodtypeTableModel,alleProdtyperCheck);
	}
	
	/**
	 * Returnerer alle valgte produksjonstyper
	 * @return
	 */
	public List<Prodtype> getSelectedProdtyper(){
		List<Prodtype> selected = new ArrayList<Prodtype>();
		ProdtypeTableObject[] prodtyper = prodtypeTableModel.getEntities().toArray(new ProdtypeTableObject[]{});
		for (ProdtypeTableObject p:prodtyper){
			if (p.getValgt().booleanValue()){
				selected.add(p.getProdtype());
			}		
		}
		if (tomkjoringProdCheck.isSelected()){
			selected.add(tomkjoring.getProdtype());
		}
		return selected;
	}
	/**
	 * Returnerer alle produksjonstyper
	 * @return
	 */
	public List<Prodtype> getAllProdtyper(){
		List<Prodtype> alle = new ArrayList<Prodtype>();
		ProdtypeTableObject[] prodtyper = prodtypeTableModel.getEntities().toArray(new ProdtypeTableObject[]{});
		for (ProdtypeTableObject p:prodtyper){
			alle.add(p.getProdtype());		
		}
		if (tomkjoringProdCheck.isSelected()){
			alle.add(tomkjoring.getProdtype());
		}
		return alle;
	}
	
	/**
	 * Returnerer true hvis alleprodtyper er valgt
	 * @return
	 * @deprecated
	 */
	public boolean isAlleValgt(){
		return false;
//		return alleProdtyperCheck.isSelected();
	}
	
	/**
	 * Returnerer true hvis punkter er valgt
	 * @return
	 */
	public boolean isPunktSelected(){
		return punktStrekningPanel.getPunktCheck().isSelected();
	}
	/**
	 * Returnerer true hvis strekninger er valgt
	 * @return
	 */
	public boolean isStrekningSelected(){
		return punktStrekningPanel.getStrekningCheck().isSelected();
	}
	/**
	 * Returnerer true hvis alle punkter er valgt
	 * @return
	 */
	public boolean isAllPunktSelected(){
		return punktStrekningPanel.getAlleRadio().isSelected();
	}
	
	public boolean isLabelsSelected(){
		if (punktStrekningPanel.getLabelCheck()!=null)
			return punktStrekningPanel.getLabelCheck().isSelected();
		return true;
	}
	/**
	 * returnerer true hvis kun siste punkt er valgt
	 * @return
	 */
	public boolean isKunSisteSelected(){
		return punktStrekningPanel.getKunSisteRadio().isSelected();
	}
	public boolean isTidsvinduSelected(){
		return punktStrekningPanel.getTidsvinduRadio().isSelected();
	}
	/**
	 * Returnerer true hvis annen produksjon også skal vises
	 * @return
	 */
	public boolean isAllProdSelected(){
		return allProdCheck.isSelected();
	}
	
	public void setSanntidMode(){
		punktStrekningPanel.getKunSisteRadio().setSelected(Boolean.TRUE);
		punktStrekningPanel.getStrekningCheck().setEnabled(Boolean.FALSE);
		punktStrekningPanel.getAlleRadio().setEnabled(Boolean.FALSE);
		punktStrekningPanel.getTidsvinduRadio().setEnabled(Boolean.TRUE);
		
		tomkjoringProdCheck.setEnabled(Boolean.FALSE);
		allProdCheck.setEnabled(Boolean.FALSE);
		
		tomkjoringProdCheck.setSelected(Boolean.TRUE);
		allProdCheck.setSelected(Boolean.TRUE);
		
	}
	public void setPeriodeMode(){
		punktStrekningPanel.getStrekningCheck().setEnabled(Boolean.TRUE);
		punktStrekningPanel.getAlleRadio().setEnabled(Boolean.TRUE);
		tomkjoringProdCheck.setEnabled(Boolean.TRUE);
		allProdCheck.setEnabled(Boolean.TRUE);
		punktStrekningPanel.getTidsvinduRadio().setEnabled(Boolean.FALSE);
	}
}
