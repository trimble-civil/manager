package no.mesta.mipss.mipssmap.task;

import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mipssfelt.AvvikQueryParams;
import no.mesta.mipss.mipssfelt.AvvikService;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

public class HentFunnTask extends MipssMapTask{
		
	private AvvikQueryParams filter;
	private MipssMapModule plugin;
	
	public HentFunnTask(MipssMapModule plugin, ExecutorTaskType taskType, AvvikQueryParams filter) {
		super(taskType);
		this.plugin = plugin;
		this.filter = filter;
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
		AvvikService bean = BeanUtil.lookup(AvvikService.BEAN_NAME, AvvikService.class);
		List<AvvikSokResult> avvikList = bean.sokAvvikEntity(filter);
		
		LayerBuilder b = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
		return b.getPointLayerCollectionFromAvvik(avvikList);
	}
}
