package no.mesta.mipss.mipssmap;

import java.awt.BorderLayout;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.klimadata.wsklima.Klimadata;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.tools.MoveLayerTool;
import no.mesta.mipss.mapplugin.tools.PanMapTool;
import no.mesta.mipss.mapplugin.tools.PointCollectionKeyScrollTool;
import no.mesta.mipss.mapplugin.tools.SelectTool;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.MouseListenerManager;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.plugins.MipssMapProductionPlugin;
import no.mesta.mipss.mipssmap.swing.JStatusPanel;
import no.mesta.mipss.mipssmap.swing.MipssMapPanel;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.swing.OptionsPanel;
import no.mesta.mipss.mipssmap.swing.ToolbarPanel;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginMessage;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.progressbar.ProgressController;
import no.mesta.mipss.webservice.Produksjon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.border.DropShadowBorder;

public class MipssMapModule extends MipssPlugin {
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private JPanel mapModuleGui;

	private OptionsPanel optionsPanel;
	private MipssMapPanel mainMap;

	private JStatusPanel statusPanel;

	private MaintenanceContract contractBean;
	private Produksjon produksjonBean;
	private MipssMap mipssMapBean;
	private Klimadata klimadataBean;
	private MipssField mipssFieldBean;
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private MipssBeanTableModel<Dfuindivid> dfuTableModel;
	private Driftkontrakt currentKontrakt;
	private boolean running;
	private List<MipssMapLayerPlugin> layerPlugins;

	public List<MipssMapLayerPlugin> getLayerPlugins() {
		return layerPlugins;
	}

	public void setLayerPlugins(List<MipssMapLayerPlugin> layerPlugins) {
		this.layerPlugins = layerPlugins;
	}

	/**
	 * Returnerer en av pluginsene til kartet. 
	 * @param mapPluginClass
	 * @return
	 */
	public MipssMapLayerPlugin getLayerPlugin(Class<?> mapPluginClass) {
		for (MipssMapLayerPlugin p : layerPlugins) {
			if (p.getClass().equals(mapPluginClass)) {
				return p;
			}
		}
		return null;
	}

	public List<MipssMapProductionPlugin> getProduksjonPlugins() {
		return produksjonPlugins;
	}

	public void setProduksjonPlugins(
			List<MipssMapProductionPlugin> produksjonPlugins) {
		this.produksjonPlugins = produksjonPlugins;
	}

	private List<MipssMapProductionPlugin> produksjonPlugins;
	private MipssMapWindowManager windowManager;

	public MipssMapModule() {
		// Calendar c = Calendar.getInstance();
		// c.set(Calendar.YEAR, 2008);
		// c.set(Calendar.MONTH, Calendar.OCTOBER);
		// c.set(Calendar.DAY_OF_MONTH, 11);
		// c.set(Calendar.HOUR_OF_DAY, 11);
		// c.set(Calendar.MINUTE, 9);
		// c.set(Calendar.SECOND, 50);
		//
		// Clock.setClock(c.getTime());
		// Clock.setRunning(true);
	}

	public void shutdown() {
		running = false;
		mainMap.dispose();
		loader = null;
		statusPanel = null;
		windowManager.clear();
	}

	@Override
	public void finalize() throws Throwable {
		super.finalize();
	}

	public void initPluginGUI() {
		windowManager = new MipssMapWindowManager();
		running = true;
		if (loader.getSelectedDriftkontrakt() != null) {
			setCurrentKontrakt(loader.getSelectedDriftkontrakt());
		}
		// hovedpanel
		mapModuleGui = new JPanel();

		mapModuleGui.setLayout(new BorderLayout());

		// statuspanel
		statusPanel = new JStatusPanel(this);
		statusPanel.setBorder(BorderFactory.createEtchedBorder());
		mapModuleGui.add(statusPanel, BorderLayout.SOUTH);

		// høyrepanel
		JPanel mapPanel = new JPanel(new BorderLayout());
		mapPanel.setBorder(new DropShadowBorder(new Color(0,0,153), 5, .5f, 12, false, false, true, true));

		mainMap = new MipssMapPanel();
		mapPanel.add(mainMap, BorderLayout.CENTER);

		mainMap.addCoordinateMessageListener(statusPanel);
		mapPanel.add(new ToolbarPanel(this), BorderLayout.NORTH);

		mapModuleGui.add(mapPanel, BorderLayout.CENTER);
		// venstrepanel
		optionsPanel = new OptionsPanel(this, windowManager, false);
		optionsPanel.setBorder(new DropShadowBorder(new Color(0,0,153), 5, .5f, 12, false, false, true, true));
		mapModuleGui.add(optionsPanel, BorderLayout.WEST);

		// set opp kartet til å brukte gitte kontrollere
		MouseListenerManager mouseListenerManager = new MouseListenerManager();
		SelectTool selectListener = new SelectTool(mainMap);

		MoveLayerTool moveListener = new MoveLayerTool(mainMap);

		mouseListenerManager.addListener(0, new PanMapTool(mainMap));
		mouseListenerManager.addListener(0, selectListener);
		mouseListenerManager.addListener(0, moveListener);
		mainMap.setMouseListenerManager(mouseListenerManager);

		PointCollectionKeyScrollTool scrollTool = new PointCollectionKeyScrollTool(mainMap);
		mainMap.getLayerHandler().addLayerListener(scrollTool);
		mainMap.addKeyListener(scrollTool);
		List<LayerType> layerTypes = new ArrayList<LayerType>();
		for (MipssMapLayerType l : MipssMapLayerType.values()) {
			layerTypes.add(l.getType());
		}
		mainMap.getLayerHandler().setLayerTypes(layerTypes);

		// mainMap.getLayerHandler().addLayer(new LayerType(1, "tomt"), Test.getTom(), true);
		// mainMap.getLayerHandler().addLayer(new LayerType(2, "hus", true), Test.getHus(), true);
		// mainMap.getLayerHandler().addLayer(new LayerType(2, "gar", true), Test.getGar(), true);
	}

	public JPanel getModuleGUI() {
		return mapModuleGui;
	}

	// Sørger for at det blir opprettet et optionspanel som benytter SVV-innsyn
	// view
	public void setSVVOptionsPanel(boolean svvView) {
		mapModuleGui.remove(optionsPanel);
		optionsPanel = null;
		optionsPanel = new OptionsPanel(this, windowManager, svvView);
		mapModuleGui.add(optionsPanel, BorderLayout.WEST);
	}

	/**
	 * Returnerer bønnen som styrer henting av produksjon
	 * 
	 * @return
	 */
	public Produksjon getProduksjonBean() {
		if (produksjonBean == null) {
			produksjonBean = BeanUtil.lookup(Produksjon.BEAN_NAME, Produksjon.class);
		}
		return produksjonBean;
	}

	public MaintenanceContract getContractBean() {
		if (contractBean == null) {
			contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		}
		return contractBean;
	}

	public MipssMap getMipssMapBean() {
		if (mipssMapBean == null) {
			mipssMapBean = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
		}
		return mipssMapBean;
	}

	public Klimadata getKlimadataBean() {
		if (klimadataBean == null) {
			klimadataBean = BeanUtil.lookup(Klimadata.BEAN_NAME, Klimadata.class);
		}
		return klimadataBean;
	}

	public MipssField getMipssFieldBean() {
		if (mipssFieldBean == null) {
			mipssFieldBean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
		}
		return mipssFieldBean;
	}

	public JStatusPanel getStatusPanel() {
		return statusPanel;
	}

	public MipssMapPanel getMainMap() {
		return mainMap;
	}

	/**
	 * @return the progressController
	 */
	public ProgressController getProgressController() {
		return getStatusPanel().getProgressController();
	}

	public void setCurrentKontrakt(Driftkontrakt currentKontrakt) {
		Driftkontrakt old = this.currentKontrakt;
		this.currentKontrakt = currentKontrakt;
		props.firePropertyChange("currentKontrakt", old, currentKontrakt);

	}

	public Driftkontrakt getCurrentKontrakt() {
		return this.currentKontrakt;
	}

	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * Lager et temporært layer som benyttes av tegneverktøyene.
	 * @return
	 */
	public LineListLayer createTempLayer() {
		KonfigParam params = KonfigparamPreferences.getInstance().getKonfigParam();
		String col = params.hentEnForApp("studio.mipss.mipssmap", "lengdeMalFarge").getVerdi();
		Color c = ColorUtil.fromHexString(col);
		int width = 2;
		LineListLayer layer = new LineListLayer("lengdetemp", null, false, null);
		layer.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
		layer.setAntialias(true);
		layer.setColor(c);
		layer.setStroke(width);
		layer.setConnectorMarkers(true);
		return layer;
	}

	@Override
	public void receiveMessage(PluginMessage<?, ?> message) {
		if (getStatus() == MipssPlugin.Status.RUNNING) {
			message.setConsumed(true);
			new MessageHandler(this).handleMessage(message);
			getLoader().activatePlugin(getCacheKey());
		}
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		log.debug("addPropertyChangeListener(" + l + ")");
		props.addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName,
			PropertyChangeListener l) {
		log.debug("addPropertyChangeListener(" + propName + ", " + l + ")");
		props.addPropertyChangeListener(propName, l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		log.debug("removePropertyChangeListener(" + l + ")");
		props.removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName,
			PropertyChangeListener l) {
		log.debug("removePropertyChangeListener(" + propName + ", " + l + ")");
		props.removePropertyChangeListener(propName, l);
	}

	public MipssMapWindowManager getWindowManager() {
		return windowManager;
	}

}
