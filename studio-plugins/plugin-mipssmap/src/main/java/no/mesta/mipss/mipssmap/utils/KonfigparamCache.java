package no.mesta.mipss.mipssmap.utils;

import java.util.HashMap;
import java.util.Map;

import no.mesta.mipss.core.KonfigparamPreferences;

public class KonfigparamCache {

	private static Map<String, String> cachedValues = new HashMap<String, String>();
	
	public static final String FARGE_LABEL_INSPEKSJON = "fargeLabelInspeksjon";
	public static final String FARGE_LABEL_UE = "fargeLabelUE";
	public static final String FARGE_LABEL_MESTA = "fargeLabelMesta";
	public static final String FARGE_LABEL_TEXT_INSPEKSJON = "fargeLabelTextInspeksjon";
	public static final String FARGE_LABEL_TEXT_UE= "fargeLabelTextUE";
	public static final String FARGE_LABEL_TEXT_MESTA = "fargeLabelTextMesta";
	
	public static final String APP_NAVN = "studio.mipss.mipssmap";
	
	private KonfigparamCache(){
		
	}
	
	public static String getKonfigparamValue(String navn){
		if (cachedValues==null)
			cachedValues=new HashMap<String, String>();
		
		String value = cachedValues.get(navn);
		if (value==null){
			value = KonfigparamPreferences.getInstance().hentEnForApp(APP_NAVN, navn).getVerdi();
			cachedValues.put(navn, value);
		}
		return value;		
	}
	
	public static void clearCache(){
		cachedValues.clear();
	}
	
	public static void dispose(){
		cachedValues=null;
	}
}
