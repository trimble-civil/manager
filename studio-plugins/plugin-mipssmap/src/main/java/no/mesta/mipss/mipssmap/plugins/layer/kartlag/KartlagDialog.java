package no.mesta.mipss.mipssmap.plugins.layer.kartlag;

import java.awt.BorderLayout;
import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JPanel;

import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;

public class KartlagDialog extends JDialog{

	private MipssMapModule module;
	private MipssMapWindowManager windowManager;
	private KartlagPlugin plugin;
	
	private KartlagPanel panel;
	
	public KartlagDialog(Window parent, MipssMapModule module, MipssMapWindowManager windowManager, KartlagPlugin plugin){
		super(parent);
		this.module = module;
		this.windowManager = windowManager;
		this.plugin = plugin;
		initGui();
	}
	
	private void initGui(){
		setLayout(new BorderLayout());
		panel = new KartlagPanel(module, windowManager, plugin, this);
		
		add(panel);
		add(new JPanel(), BorderLayout.EAST);
		add(new JPanel(), BorderLayout.WEST);
//		add(new JPanel(), BorderLayout.SOUTH);
	}
	
	public KartlagPanel getPanel(){
		return panel;
	}
}
