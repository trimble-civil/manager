package no.mesta.mipss.mipssmap.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringUtils;

import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.PointLayer;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProdtypeVO;

/**
 * Tabellmodell for avspillingsdata for kjøretur
 * 
 * @author Harald A. Kulø
 */
public class PlayDataTableModel<T extends PointLayer> extends DefaultTableModel{
    private List<CollectionEntity> data;
    private String[] header;
    private SimpleDateFormat format = new SimpleDateFormat("dd.MMM HH:mm:ss");
    
    public static final int dato       = 0;
    public static final int hastighet  = 1;
    public static final int prodstatus = 2;
    
    public PlayDataTableModel(List<CollectionEntity> data){
        this.data = data;
        this.header = new String[]{"Dato", "Hastighet", "Prod.status"};
    }
    
    public Object getValueAt(int row, int column){
        ProdpunktPointLayer layer = (ProdpunktPointLayer)data.get(row);
        switch (column){
            //case dato: return format.format(XMLCalendarConvert.convertCalendar(layer.getGpsGeoPosition().getGpsPoint().getData().getTime()).getTime());
            //case hastighet: return layer.getGpsGeoPosition().getGpsPoint().getData().getSpeed();
            //case prodstatus: return layer.getGpsGeoPosition().getGpsPoint().getData().getProductionStatus();
            
            case dato: return format.format(layer.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt());
            case hastighet: return layer.getProdpunktGeoPosition().getProdpunktVO().getHastighet();
            case prodstatus: return getProdstatus(layer.getProdpunktGeoPosition().getProdpunktVO());//.getDigiAndMask();
        }
        return null;
    }
    private String getProdstatus(ProdpunktVO vo){
    	List<ProdtypeVO> prodtypeList = vo.getProdtypeList();
    	if (prodtypeList!=null){
	    	List<String> prodtyper = new ArrayList<String>();
	    	for (ProdtypeVO v:prodtypeList){
	    		if (v.getProdtype()!=null){
	    			prodtyper.add(v.getProdtype().getNavn());
	    		}
	    	}
	    	return StringUtils.join(prodtyper, ',');
    	}
    	return vo.getDigiAndMask();
    }
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    @Override
    public int getColumnCount() {
        return header.length;
    }
    @Override
    public int getRowCount(){
        if (data==null)
            return 0;
        return data.size();
    }
    @Override
    public String getColumnName(int index){
        return header[index];
    }
    
    
}