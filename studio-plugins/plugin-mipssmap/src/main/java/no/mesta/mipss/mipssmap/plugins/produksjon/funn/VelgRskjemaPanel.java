package no.mesta.mipss.mipssmap.plugins.produksjon.funn;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssfelt.RskjemaQueryParams;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.task.HentForsikringsskadeTask;
import no.mesta.mipss.mipssmap.task.HentHendelserTask;
import no.mesta.mipss.mipssmap.task.HentSkredTask;
import no.mesta.mipss.mipssmap.task.MipssMapTask;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.ui.JAarsakPanel;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;
import no.mesta.mipss.valueobjects.ElrappTypeR.Type;

public class VelgRskjemaPanel extends ObservasjonPanel {

	private JCheckBox visR2IKartCheck;
	private JCheckBox visR5IKartCheck;
	private JCheckBox visR11IKartCheck;
	private JLabel visR2IKartImage;
	private JAarsakPanel aarsakPanel;
	private FunnPlugin plugin;
	private MipssMapModule module;
	private JLabel visR5IKartImage;
	private JLabel visR11IKartImage;
	public VelgRskjemaPanel(MipssMapModule module, FunnPlugin plugin, ImageIcon origIcon, JTabbedPane tab, int index) {
		super(origIcon, tab, index);
		this.module = module;
		this.plugin = plugin;
		initGui();
	}

	private void initGui() {
		setLayout(new BorderLayout());
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.add(getCheckPanel());
		mainPanel.add(getFilterPanel());
		add(mainPanel);
	}

	private JPanel getCheckPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		visR2IKartImage = new JLabel(ImageUtil.toGrayScale(getOrigIcon()));
		visR5IKartImage = new JLabel(ImageUtil.toGrayScale(getOrigIcon()));
		visR11IKartImage = new JLabel(ImageUtil.toGrayScale(getOrigIcon()));
		
		visR2IKartCheck = new JCheckBox("Vis R2 kart");
		visR2IKartCheck.setSelected(true);
		visR2IKartCheck.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (visR2IKartCheck.isSelected()){
					checkR2Selected(true);}
				else{checkR2Selected(false);}
		}});
		
		visR5IKartCheck = new JCheckBox("Vis R5 kart");
		visR5IKartCheck.setSelected(true);
		visR5IKartCheck.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (visR5IKartCheck.isSelected()){
					checkR5Selected(true);}
				else{checkR5Selected(false);}
		}});
		visR11IKartCheck = new JCheckBox("Vis R11 kart");
		visR11IKartCheck.setSelected(true);
		visR11IKartCheck.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (visR11IKartCheck.isSelected()){
					checkR11Selected(true);}
				else{checkR11Selected(false);}
		}});
		
		panel.add(visR2IKartCheck);
		panel.add(visR2IKartImage);
		panel.add(visR5IKartCheck);
		panel.add(visR5IKartImage);
		panel.add(visR11IKartCheck);
		panel.add(visR11IKartImage);
		panel.add(Box.createHorizontalGlue());
		
		JPanel vPanel = new JPanel();
		vPanel.setLayout(new BoxLayout(vPanel, BoxLayout.PAGE_AXIS));
		vPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		vPanel.add(panel);
		
		return vPanel;
	}
	

	public void checkR2Selected(boolean selected) {
		if (selected){
			visR2IKartImage.setIcon(getOrigIcon());
			getTabPane().setIconAt(getIndex(), getOrigIcon());
			module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.HENDELSE.getType());
		}else{
			visR2IKartImage.setIcon(ImageUtil.toGrayScale(getOrigIcon()));
			getTabPane().setIconAt(getIndex(), ImageUtil.toGrayScale(getOrigIcon()));
			module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.HENDELSE.getType());
		}
	}
	public void checkR5Selected(boolean selected) {
		if (selected){
			visR5IKartImage.setIcon(getOrigIcon());
			getTabPane().setIconAt(getIndex(), getOrigIcon());
			module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.FORSIKRINGSSKADE.getType());
		}else{
			visR5IKartImage.setIcon(ImageUtil.toGrayScale(getOrigIcon()));
			getTabPane().setIconAt(getIndex(), ImageUtil.toGrayScale(getOrigIcon()));
			module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.FORSIKRINGSSKADE.getType());
		}
	}
	public void checkR11Selected(boolean selected) {
		if (selected){
			visR11IKartImage.setIcon(getOrigIcon());
			getTabPane().setIconAt(getIndex(), getOrigIcon());
			module.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.SKRED.getType());
		}else{
			visR11IKartImage.setIcon(ImageUtil.toGrayScale(getOrigIcon()));
			getTabPane().setIconAt(getIndex(), ImageUtil.toGrayScale(getOrigIcon()));
			module.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.SKRED.getType());
		}
	}
	private JPanel getFilterPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		aarsakPanel = new JAarsakPanel();
		aarsakPanel.setSelected(true);
		panel.setLayout(new GridBagLayout());
		
		panel.add(aarsakPanel, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
		aarsakPanel.setMinimumSize(new Dimension(200,200));
		aarsakPanel.setPreferredSize(new Dimension(200,200));
		
		PropertyChangeListener l = new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("alleValgte")){
					checkR2Selected(true);
					visR2IKartCheck.setSelected(true);
				}
			}
		};
		aarsakPanel.addPropertyChangeListener(l);
		return panel;
	}

	@Override
	public MipssMapTask getTask() {
//		HendelseQueryFilter filter = new HendelseQueryFilter(null);
//		filter.setFraDato(plugin.getProduksjonOptions().getDatoFra().getDate());
//		filter.setTilDato(plugin.getProduksjonOptions().getDatoTil().getDate());
//		filter.setKontraktId(module.getCurrentKontrakt().getId());
//		
//		filter.setIngenAarsaker(aarsakPanel.isIngenValgt());
//		filter.setAarsaker(aarsakPanel.getAlleValgte());
		
		RskjemaQueryParams params = new RskjemaQueryParams();
		params.setDateFra(plugin.getProduksjonOptions().getDatoFra().getDate());
		params.setDateTil(plugin.getProduksjonOptions().getDatoTil().getDate());
		params.setValgtKontrakt(module.getCurrentKontrakt());
		params.setTypeR(Type.R2);
		HentHendelserTask task = new HentHendelserTask(module, ExecutorTaskType.QUEUE, params, aarsakPanel.getAlleValgte());
		return task;
	}
	
	public MipssMapTask getR5Task(){
		RskjemaQueryParams params = new RskjemaQueryParams();
		params.setDateFra(plugin.getProduksjonOptions().getDatoFra().getDate());
		params.setDateTil(plugin.getProduksjonOptions().getDatoTil().getDate());
		params.setValgtKontrakt(module.getCurrentKontrakt());
		params.setTypeR(Type.R5);
		HentForsikringsskadeTask task = new HentForsikringsskadeTask(module, ExecutorTaskType.QUEUE, params);
		return task;
	}
	public MipssMapTask getR11Task(){
		RskjemaQueryParams params = new RskjemaQueryParams();
		params.setDateFra(plugin.getProduksjonOptions().getDatoFra().getDate());
		params.setDateTil(plugin.getProduksjonOptions().getDatoTil().getDate());
		params.setValgtKontrakt(module.getCurrentKontrakt());
		params.setTypeR(Type.R11);
		//TODO implementer en r11 task
		HentSkredTask task = new HentSkredTask(module, ExecutorTaskType.QUEUE, params);
		return task;
	}
	public boolean isVisIKart(){
		//TABBE..
		return true;
	}
	public boolean isVisR2IKart(){
		return visR2IKartCheck.isSelected();
	}
	public boolean isVisR5IKart(){
		return visR5IKartCheck.isSelected();
	}
	public boolean isVisR11IKart(){
		return visR11IKartCheck.isSelected();
	}
}
