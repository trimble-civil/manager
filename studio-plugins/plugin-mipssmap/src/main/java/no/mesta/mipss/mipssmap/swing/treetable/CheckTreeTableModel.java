package no.mesta.mipss.mipssmap.swing.treetable;


import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;
/**
 * 
 * @author Harald A. Kulø
 *
 */
public class CheckTreeTableModel extends DefaultTreeTableModel {

	
	public static final int NAME = 0;
    public static final int VALUE = 1;
    private JCheckTreeTable view;
    
    
    
    /**
     * Creates a new instance of PropertiesModel 
     * @param n the node to fill the model with
	 *
     */
    public CheckTreeTableModel(TreeTableNode n){
    	super(n);
    }
    public void setView(JCheckTreeTable view){
    	this.view = view;
    }
    public int getColumnCount() {
        return 2;
    }

    public String getColumnName(int columnIndex) {
    	return columnIndex==NAME?"Name":"Value";
    }

    public boolean isCellEditable(Object node, int columnIndex) {
    	
    	CheckButtonNode n = (CheckButtonNode)node;
    	
    	//indicates a subroot node
    	if (n.getChilds().size()>1&&columnIndex==0)
    		return false;
    	return (columnIndex==0?false:true);
    }

    /**
     * If the column index = 0(NAME) the value that returns is the info from propertyNode.
     * 
     * If the column index = 1(VALUE) and prop 
     * 
     */
    public Object getValueAt(Object node, int columnIndex){
    	CheckButtonNode prop = (CheckButtonNode)node;
    	view.setToolTipText("tooltiptext");
    	switch (columnIndex){
    		case 0: return prop.getText();
    		case 1: return prop.getButton();
    	}
    	return null;
    }

    
    public void setValueAt(Object value, Object node, int columnIndex){
    	CheckButtonNode prop = (CheckButtonNode)node;
//    	logger.debug("property : "+prop.getParent().getAttribute().getName()+"."+prop.getAttribute().getName()+" set to "+value);
    	switch(columnIndex){
//    		case NAME: prop.getAttribute().setName((String)value);
//    		case VALUE: prop.setValue(value);
    	}
    	
    }
}
