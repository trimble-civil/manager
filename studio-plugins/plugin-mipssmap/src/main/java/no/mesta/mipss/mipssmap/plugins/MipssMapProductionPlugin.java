package no.mesta.mipss.mipssmap.plugins;

import javax.swing.JDialog;

import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.ProduksjonOptions;

public interface MipssMapProductionPlugin {
    JDialog getDialog();
    /**
     * Utvalgskriterier for å hente produksjon
     * @param options
     */
    void setProduksjonOptions(ProduksjonOptions options);
    /**
     * Returnerer JCheckButton til plugin
     * @return
     */
    JCheckButton getCheckButton();
    /**
     * Tilbakestill plugin til utgangspunktet
     */
    void reset();
    /**
     * Henter alle data til plugin på nytt
     */
    void refresh();
    /**
     * Setter denne plugin til å være enabled eller ikke
     * @param enable
     */
    void setEnabled(boolean enable);
    /**
     * Returnerer true når plugin er lastet
     * @return
     */
    boolean isLoaded();
}
