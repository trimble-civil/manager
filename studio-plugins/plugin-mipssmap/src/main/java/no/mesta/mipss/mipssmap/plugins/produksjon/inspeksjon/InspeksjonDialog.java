package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import java.awt.Window;

import javax.swing.JDialog;

import no.mesta.mipss.mipssmap.MipssMapModule;

public class InspeksjonDialog extends JDialog{
    private InspeksjonPanel inspeksjonPanel;
	public InspeksjonDialog(Window owner, InspeksjonPlugin plugin, MipssMapModule module){
		super(owner);
        initGUI(plugin, module);
	}
	
	private void initGUI(InspeksjonPlugin plugin, MipssMapModule module) {
		inspeksjonPanel = new InspeksjonPanel(module, plugin);
		add(inspeksjonPanel);
	}
	public InspeksjonPanel getInspeksjonPanel(){
		return inspeksjonPanel;
	}
}
