package no.mesta.mipss.mipssmap.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.mipssmapserver.KjoretoyVO;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SanntidTask extends SwingWorker<Void, Void>{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private final JTextField fraText;
	private final JTextField tilText;
	private final int minuteTail;
	private boolean running=true;
	private int sleep = 1000*10; //10 sekunder
	private final MipssMapModule plugin;
	private final ProduksjonPlugin produksjonPlugin;
	private HashMap<Long, PointLayerCollection> layers = new HashMap<Long, PointLayerCollection>();
	
	public SanntidTask(MipssMapModule plugin, JTextField fraText, JTextField tilText, int minuteTail, ProduksjonPlugin produksjonPlugin){
		this.plugin = plugin;
		this.fraText = fraText;
		this.tilText = tilText;
		this.minuteTail = minuteTail;
		this.produksjonPlugin = produksjonPlugin;
		sleep = Integer.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "pollingIntervalSanntid").getVerdi());
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.PRODUKSJONPUNKT.getType());
		plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.PRODUKSJONLABEL.getType());
		plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.PRODUKSJONSTREKNING.getType());
		
		while (running){
			try{
				Date start = getStart(minuteTail);
				Date end = getStop();
				List<KjoretoyVO> selectedKjoretoy = produksjonPlugin.getSelectedKjoretoy();
				for (KjoretoyVO v:selectedKjoretoy){
					Long kjoretoyId=null;
		    		Long pdaId=null;
		    		String guiNavn="";
		    		boolean mestaBil = false;
		    		PointLayerCollection layer = null;
					if (v.getKjoretoy()!=null){
		    			kjoretoyId = v.getKjoretoy().getId();
		    			guiNavn = v.getKjoretoy().getGuiTekst();
		    			if (guiNavn==null)
		    				guiNavn = v.getKjoretoy().getNavn();
		    			if (v.getKjoretoy().getLeverandorNavn()!=null)
		    				mestaBil = v.getKjoretoy().getLeverandorNavn().toLowerCase().indexOf("mesta")!=-1;
		    			layer = getLayerFromCache(kjoretoyId);
		    		}
		    		if (v.getPda()!=null){
		    			pdaId = v.getPda().getId();
		    			layer = getLayerFromCache(pdaId);
		    		}
		    		
					final HentProdpunktDfuinfoTask task = initTask(pdaId, kjoretoyId, start, end, layer, guiNavn, mestaBil, produksjonPlugin.isTidsvinduSelected());
					
					new Thread(){
						public void run(){
							try {
								task.get();
								plugin.getMainMap().repaint();
							} catch (InterruptedException e) {
								e.printStackTrace();
							} catch (ExecutionException e) {
								e.printStackTrace();
							}
						}
					}.start();
				}
				updateTime(start, end);
				Thread.sleep(sleep);
			} catch (InterruptedException e){
				log.debug("Wait was interrupted.."+e.getMessage());
			}
		}
		
		return null;
	}
	private PointLayerCollection getLayerFromCache(Long id){
		if (layers.get(id)==null){
			//new layer
			List<CollectionEntity> region = new ArrayList<CollectionEntity>();
			PointLayerCollection l = new PointLayerCollection(""+id, region, new Bounds(), Boolean.FALSE);
			layers.put(id, l);
			plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONPUNKT.getType(), l, false);
			
		}
		return layers.get(id);
	}
	private Date getStart(int minuteTail){
		return CalendarUtil.getAgo(Clock.now(), minuteTail);
	}
	private Date getStop(){
		return Clock.now();
	}
	
	private HentProdpunktDfuinfoTask initTask(Long dfuId, Long kjoretoyId, Date tidsvinduStart, Date tidsvinduStopp, PointLayerCollection layer, String guiNavn, boolean mestaBil, boolean tidsvindu){
		final HentProdpunktDfuinfoTask task = new HentProdpunktDfuinfoTask(plugin, dfuId, kjoretoyId, tidsvinduStart, tidsvinduStopp, layer, guiNavn, mestaBil, tidsvindu);
		ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.HentProduksjonTask.title")+dfuId+", "+kjoretoyId);
    	workerPanel.setWorker(task);
    	task.setProgressController(plugin.getProgressController());
    	task.setProgressWorkerPanel(workerPanel);
    	plugin.getProgressController().addWorkerPanel(workerPanel);
    	return task;
	}
	
	@Override
	public void done(){
	}
	
	private void updateTime(Date fra, Date til){
		fraText.setText(MipssDateFormatter.formatDate(fra, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
		tilText.setText(MipssDateFormatter.formatDate(til, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
	}
	public void stopRunning(){
		running = false;
		this.cancel(true);
	}
	
}
