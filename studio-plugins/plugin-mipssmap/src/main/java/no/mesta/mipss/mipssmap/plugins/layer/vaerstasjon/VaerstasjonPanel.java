package no.mesta.mipss.mipssmap.plugins.layer.vaerstasjon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXHeader;

import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.resources.images.IconResources;

/**
 * Panel for å vise frem værstasjoner
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class VaerstasjonPanel extends JPanel{
	
	private VaerstasjonPlugin plugin;
	private MipssMapModule module;
	public VaerstasjonPanel(VaerstasjonPlugin plugin, MipssMapModule module){
		this.plugin = plugin;
		this.module = module;
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		JButton hentStasjonerButton = new JButton(ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.VaerstasjonPanel.hentStasjonerButton.text"));
		
		hentStasjonerButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				hentStasjonerButton_actionPerformed(e);
			}
		});
		add(getHeader());
		add(Box.createVerticalStrut(20));
		add(Box.createVerticalGlue());
		add(hentStasjonerButton);
		add(Box.createVerticalStrut(20));
		
	}
	private JXHeader getHeader(){
		String title = ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.VaerstasjonPanel.header.title");
		String description = ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.VaerstasjonPanel.header.description");
		JXHeader header = new JXHeader();
		header.setIcon(IconResources.VAERSTASJON_ICON);
		header.setTitle(title);
		header.setDescription(description);
		return header;
	}
	protected void hentStasjonerButton_actionPerformed(ActionEvent e) {
		if (module.getMainMap().getLayerHandler().getLayers(MipssMapLayerType.VAERSTASJON.getType())==null)
			plugin.createVaerstasjoner(); 
		
	}
}
