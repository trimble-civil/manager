package no.mesta.mipss.mipssmap.layer;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;

import javax.swing.ImageIcon;

import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LayerCollection;
import no.mesta.mipss.mapplugin.layer.SelectableLayer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.resources.images.IconResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class VaerstasjonLayer extends Layer implements SelectableLayer, CollectionEntity{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private ImageIcon image ;
	private GeoPosition position;
	private Point2D point ;
	private ImageIcon defaultIcon = IconResources.FORTRESS_SELECTED_ICON;
	public VaerstasjonLayer(String id, boolean triggerRecenterAndZoom, GeoPosition position, ImageIcon image) {
		super(id, triggerRecenterAndZoom);
		this.position = position;
		this.image = image;
	}

	@Override
	public void paint(Graphics2D g, JXMapViewer map, int width, int height) {
	
		g = super.initGraphics(g, map);
        point = map.getTileFactory().geoToPixel(position, map.getZoom());
        int x = (int)point.getX()- image.getIconWidth()/2;
        int y = (int)point.getY() - image.getIconHeight()/2;

        g.drawImage(image.getImage(), x,y,null);
        
	}
	
	public void setSelectedImage(ImageIcon defaultIcon){
		this.defaultIcon = defaultIcon;
	}
	@Override
	public SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius) {
		if (position.getLatitude()< gp.getLatitude()+searchRadius&&
			position.getLatitude()> gp.getLatitude()-searchRadius&&
			position.getLongitude()<gp.getLongitude()+searchRadius&&
			position.getLongitude()>gp.getLongitude()-searchRadius){
            return this;
        }
        else{
            return null;
        }
	}

	@Override
	public Layer getSelectionLayer() {
		VaerstasjonLayer vaerstasjonLayer = new VaerstasjonLayer("selected"+getId(), false, position, defaultIcon);
		vaerstasjonLayer.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
		return vaerstasjonLayer;
	}

	@Override
	public LayerCollection getParent() {
		return null;
	}

	@Override
	public void setParent(LayerCollection parent) {
		
	}
	

}
