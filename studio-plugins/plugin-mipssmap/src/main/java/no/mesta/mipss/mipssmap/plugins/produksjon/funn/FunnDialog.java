package no.mesta.mipss.mipssmap.plugins.produksjon.funn;

import java.awt.Window;

import javax.swing.JDialog;

import no.mesta.mipss.mipssmap.MipssMapModule;

public class FunnDialog extends JDialog {
	private FunnPanel funnPanel;
	public FunnDialog(Window owner, FunnPlugin plugin, MipssMapModule module){
		super(owner);
		initGUI(plugin, module);
	}
	private void initGUI(FunnPlugin plugin, MipssMapModule module) {
		add(funnPanel = new FunnPanel(module, plugin));
	}
	public FunnPanel getFunnPanel(){
		return funnPanel;
	}
}
