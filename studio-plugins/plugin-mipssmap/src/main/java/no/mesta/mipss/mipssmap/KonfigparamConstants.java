package no.mesta.mipss.mipssmap;

public class KonfigparamConstants {
	public static final String APPLICATION_NAME = "studio.mipss.mipssmap";
	public static final String LINEWIDTH_PRODUKSJON = "antallPixlerProduksjon";
	public static final String LINEWIDTH_SELECTION = "antallPixlerSelect";
	public static final String SELECTION_COLOR = "selectionColor";
	public static final String ZOOM_PRODUCTION = "zoomVedHentingAvProduksjon";
	
}
