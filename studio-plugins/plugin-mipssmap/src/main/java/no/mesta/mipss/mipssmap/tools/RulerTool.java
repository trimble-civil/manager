package no.mesta.mipss.mipssmap.tools;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.MouseInputListener;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.util.MapUtilities;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;
import no.mesta.mipss.mapplugin.widgets.Widget;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;

import org.jdesktop.swingx.mapviewer.GeoPosition;

public class RulerTool implements MouseInputListener{
	private MipssMapModule plugin;
	private TextboxData data;
	private TextboxWidget text;
	private boolean active;
	private LineListLayer layer;
	
	public RulerTool(MipssMapModule plugin){
		this.plugin = plugin;
		initTextbox();
		initLayer();
	}
	
	private void initLayer() {
		plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.SELECTED.getType(), "lengdetemp");
		layer = getTempLayer();
		plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.SELECTED.getType(), layer, true);
	}

	private void initTextbox() {
		plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.TEXTBOX.getType(), "lengdeinfo");
		data = new TextboxData();
		data.setTitle(ResourceUtil.getResource("lengdemal.title"));
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		dataList.add(data);
		text = new TextboxWidget("lengdeinfo", plugin.getMainMap(), 10, Widget.GLUE.TOP_CENTER, dataList);
		text.setPreferredLayerType(MipssMapLayerType.TEXTBOX.getType());
		plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.TEXTBOX.getType(), text, true);
	}
	
	/**
     * Lager et temporært layer som benyttes av tegneverktøyene. 
     * @return
     */
    public LineListLayer getTempLayer(){
    	KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
    	String col = params.hentEnForApp("studio.mipss.mipssmap", "lengdeMalFarge").getVerdi();
    	Color c = ColorUtil.fromHexString(col);
		int width = 2;
    	LineListLayer layer = new LineListLayer("lengdetemp", null, false, null);
		layer.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
        layer.setAntialias(true);
        layer.setColor(c);
        layer.setStroke(width);
        layer.setConnectorMarkers(true);
        return layer;
    }
    private int count = 0;
	@Override
	public void mouseClicked(MouseEvent e) {
		if (plugin.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.SELECTED.getType(), "lengdetemp")==null){
			initLayer();
		}
		if (e.getButton()==MouseEvent.BUTTON3){
			count++;
			active=false;
			if (count>1){
				plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.SELECTED.getType(), "lengdetemp");
				if (layer.getPoints()!=null)
					layer.getPoints().clear();
				count=0;
			}
		}else{
			active = true;
	        List<MipssGeoPosition[]> points = layer.getPoints();
	        
	        MipssGeoPosition[] geoLine = new MipssGeoPosition[2];
	        //GeoPosition pointInGeo = map.getTileFactory().pixelToGeo(e.getPoint(), map.getZoom());
	        GeoPosition tempPoint = plugin.getMainMap().convertPointToGeoPosition(e.getPoint());
	        MipssGeoPosition pointInGeo = new MipssGeoPosition(tempPoint.getLatitude(), tempPoint.getLongitude());
	        Bounds newBounds = new Bounds();
	        newBounds.setLlx(pointInGeo.getLongitude());
	        newBounds.setLly(pointInGeo.getLatitude());
	        newBounds.setUrx(pointInGeo.getLongitude());
	        newBounds.setUry(pointInGeo.getLatitude());
	        //no points present, create a geoLine
	        if (points==null){
	            geoLine[0] = pointInGeo;
	        }else{
	            //points present
	            if (points.size()>0){
	            	MipssGeoPosition[] gp = points.get(points.size()-1);
	                if (gp[1]!=null){
	                    geoLine[0]=gp[1];
	                    geoLine[1]=pointInGeo;
	                }
	                //second point of last line is not present, set second point to the clicked point
	                if (gp[1]==null){
	                    gp[1]=pointInGeo;
	                }else{//add the geoLine with second point as null
	                    points.add(geoLine);
	                    layer.getBounds().addBounds(newBounds);
	                }
	            }
	        }
	        
	        //add the first point to layer
	        if (points == null){
	            points = new ArrayList<MipssGeoPosition[]>();
	            points.add(geoLine);
	            layer.setBounds(newBounds);
	            layer.setPoints(points);
	        }
	        plugin.getMainMap().getLayerHandler().changeLayer(layer);
	        plugin.getMainMap().repaint();
		}
	}
	public void setLengde(double lengde){
		DecimalFormat fo = new DecimalFormat("0.000");
		data.getData().clear();
		data.addData(ResourceUtil.getResource("lengdemal.lengde"), fo.format(lengde)+" m");
		data.addData(ResourceUtil.getResource("lengdemal.km"), fo.format(lengde/1000)+" km");
		data.addData(ResourceUtil.getResource("lengdemal.mil"), fo.format(lengde/10000)+" mi");
		plugin.getMainMap().repaint();
		
	}
	private double calcLengde(){
		double l = 0;
		if (layer.getPoints()!=null){
			for (MipssGeoPosition[] strekn: layer.getPoints()){
				if (strekn.length>1)
					l +=calcLengde(strekn[0], strekn[1]);
			}
		}
		return l;
	}
	
	private double calcLengde(MipssGeoPosition m1, MipssGeoPosition m2){
		return MapUtilities.calculateDistance(new GeoPosition(m1.getLatitude(), m1.getLongitude()), new GeoPosition(m2.getLatitude(), m2.getLongitude()));
	}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	@Override
	public void mousePressed(MouseEvent e) {}
	@Override
	public void mouseReleased(MouseEvent e) {}
	@Override
	public void mouseDragged(MouseEvent e) {}
	
	public void mouseMoved(MouseEvent e) {
        if (active){
            List<MipssGeoPosition[]> points = layer.getPoints();
            GeoPosition tempPoint = plugin.getMainMap().convertPointToGeoPosition(e.getPoint());
            MipssGeoPosition pointInGeo = new MipssGeoPosition(tempPoint.getLatitude(), tempPoint.getLongitude());
            if (points!=null){
                if (points.size()>0){
                    MipssGeoPosition[] lastPoint = points.get(points.size()-1);
                    lastPoint[1] = pointInGeo;
                    plugin.getMainMap().repaint();
                }
            }
            setLengde(calcLengde());
        }
    }
}
