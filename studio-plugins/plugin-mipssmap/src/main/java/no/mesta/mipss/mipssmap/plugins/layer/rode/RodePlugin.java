package no.mesta.mipss.mipssmap.plugins.layer.rode;

import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.PropertyResourceBundle;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JCheckBox;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapController;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.task.HentRodeVeinettTask;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Bindings;


/**
 * MipssMapLayerPlugin for å vise frem roder
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class RodePlugin implements MipssMapLayerPlugin{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssMapText");

	private Logger log = LoggerFactory.getLogger(this.getClass());
    private JCheckButton button = new JCheckButton(ResourceUtil.getResource("mipssmap.plugins.layer.rode.RodePlugin.button.text"));
    private MipssMapWindowManager windowManager;
    private MipssMapModule plugin;

	private boolean loaded;
    
    public RodePlugin(MipssMapModule plugin, MipssMapWindowManager windowManager) {
        this.plugin = plugin;
        this.windowManager = windowManager;
        
        button.getButton().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                button_actionPerformed(e);
            }
        });
        button.getCheckBox().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                check_actionPerformed(e);
            }
        });
        button.getButton().setToolTipText(resources.getGuiString("grunndata.rode.tooltip"));
        button.getCheckBox().setToolTipText(resources.getGuiString("grunndata.vis.rode.tooltip"));

    }

    public JCheckButton getCheckButton() {
        return button;
    }
    
    /**
     * Action-metode for rode-knappen, viser gui for å manipulere rodenes utseende
     * @param e
     */
    private void button_actionPerformed(ActionEvent e){
        final Driftkontrakt kontrakt = plugin.getCurrentKontrakt();
        if (kontrakt!=null){
            Container parent = button;
            while (!(parent instanceof Window)){
                parent = parent.getParent();
            }
            RodeDialog dialog = (RodeDialog)windowManager.getDialog(RodeDialog.class);
            if (dialog==null){
                dialog = new RodeDialog((Window)parent,plugin, this);
                dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.rode.RodeDialog.title"));
                dialog.setSize(400, 358);
                dialog.setLocationRelativeTo(null);
                windowManager.addDialog(dialog);
            }
            dialog.setVisible(true);
        }
    }
    
     /**
      * Action-metode for rode checkbox, ber controlleren om å vise eller skjule lag når den er <code>visible</code>/<code>hidden</code>
      * @param e
      */
    private void check_actionPerformed(ActionEvent e){
    	if ((RodeDialog)windowManager.getDialog(RodeDialog.class)==null&&!loaded){
    		button_actionPerformed(null);
    		loaded = true;
    		((JCheckBox)e.getSource()).setSelected(false);
    	}else{
	        JCheckBox check = (JCheckBox)e.getSource();
	        if (check.isSelected()){
	        	plugin.getMainMap().getLayerHandler().showLayerType(MipssMapLayerType.RODE.getType());
	        }else{
	        	plugin.getMainMap().getLayerHandler().hideLayerType(MipssMapLayerType.RODE.getType());
	        }
    	}
    }
    
    public void createRodeVeinett(Rode rode){
    	final HentRodeVeinettTask task = new HentRodeVeinettTask(plugin, rode);
    	ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.layer.rode.RodePlugin.HentRodeVeinettTask.title")+rode.getNavn()+")");
        workerPanel.setWorker(task);
        task.setProgressController(plugin.getProgressController());
        task.setProgressWorkerPanel(workerPanel);
        plugin.getProgressController().addWorkerPanel(workerPanel);
        
        Thread wait = new Thread(){ 
            public void run(){
                Layer layer=null;
                try {
                    layer = task.get();
                    if (layer!=null){
                    	plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.RODE.getType(), layer, true);
                    }                        
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (CancellationException e){
                	log.info("Task Cancelled "+e.getMessage());
                }
            }
        };
        wait.start();
    }
    
    public void removeRodeVeinett(Rode rode){
    	plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.RODE.getType(), ""+rode.getId());
    }

	@Override
	public void reset() {
		button.getCheckBox().setSelected(false);
		loaded = false;
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		getCheckButton().setEnabled(enabled);
		
	}
}
