package no.mesta.mipss.mipssmap.plugins.layer.kontraktvei;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Window;

import java.util.PropertyResourceBundle;

import javax.swing.Icon;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.ui.VeitypePanel;

import org.jdesktop.swingx.JXHeader;

/**
 * Valgdialog for å vise frem kontraktveinettet
 * 
 * @author 
 */
public class KontraktveiDialog extends javax.swing.JDialog {
    private Color forrigeFarge;
    
    private KontraktveiPanel kontraktveiPanel;
    private VeitypePanel veitypePanel;
    
    private JTabbedPane tabPane ;
    protected transient ChangeEvent changeEvent = null;
    
    /**
     * Kontruktør
     * @param owner eieren av dialogen
     * @param forrigeFarge den fargen som returneres dersom man velger å avbryte
     */
    public KontraktveiDialog(Window owner, Color forrigeFarge) {
            super(owner);
            if (forrigeFarge==null) 
                forrigeFarge = Color.yellow;
            this.forrigeFarge = forrigeFarge;
            initGUI();
    }
    private void initGUI() {
        kontraktveiPanel = new KontraktveiPanel(forrigeFarge);
        veitypePanel = new VeitypePanel();
        setLayout(new BorderLayout());
        add(getHeader(), BorderLayout.NORTH);
        tabPane = new JTabbedPane();
        tabPane.addTab(ResourceUtil.getResource("mipssmap.plugins.layer.kontraktvei.KontraktveiDialog.tab.kontraktveipanel"), kontraktveiPanel);
        tabPane.addTab(ResourceUtil.getResource("tab.veitypePanel"), veitypePanel);
        add(tabPane);
    }   
    private JXHeader getHeader(){
        JXHeader header = new JXHeader();
        header.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.kontraktvei.KontraktveiDialog.header.title"));
        header.setDescription(ResourceUtil.getResource("mipssmap.plugins.layer.kontraktvei.KontraktveiDialog.header.description"));
        Icon icon = new javax.swing.ImageIcon(getClass().getResource("/images/icon_kontraktvei.png"));
        header.setIcon(icon);
        return header;
    }
     public void addChangeListener(ChangeListener l) {
         kontraktveiPanel.addChangeListener(l);
         veitypePanel.addChangeListener(l);
     }
}
