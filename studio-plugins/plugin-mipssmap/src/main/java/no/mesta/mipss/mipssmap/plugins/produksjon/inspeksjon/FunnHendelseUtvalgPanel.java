package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.resources.images.IconResources;

/**
 * Panel som inneholder gui kompoenter for å gjøre utvalg på funn og hendelse
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class FunnHendelseUtvalgPanel extends JPanel{
	private JCheckBox funnCheck;
	private JCheckBox rskjemaCheck;
	private JCheckBox visMerkelappCheck;
	
	private JLabel funnImage;
	private JLabel hendelseImage;
	private JLabel merkelappImage;
	
	public FunnHendelseUtvalgPanel(){
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		initGui();
	}
	
	private void initGui(){
		funnCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.ProsessPDAPanel.funnCheck.text"));
		rskjemaCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.ProsessPDAPanel.hendelseCheck.text"));
		visMerkelappCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.ProsessPDAPanel.merkelapp.text"));
//		funnCheck.setEnabled(false);
		visMerkelappCheck.setSelected(true);
		
		visMerkelappCheck.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (visMerkelappCheck.isSelected()){
					merkelappImage.setIcon(IconResources.BUBBLE_SM);
				}else
					merkelappImage.setIcon(ImageUtil.toGrayScale(IconResources.BUBBLE_SM));
			}
		});
		
		funnCheck.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (funnCheck.isSelected()){
					funnImage.setIcon(IconResources.MAP_PIN_LEFT_SM);
				}else
					funnImage.setIcon(ImageUtil.toGrayScale(IconResources.MAP_PIN_LEFT_SM));
				
			}
		});
		
		rskjemaCheck.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (rskjemaCheck.isSelected()){
					hendelseImage.setIcon(IconResources.MAP_PIN_RIGHTY_SM);
				}else
					hendelseImage.setIcon(ImageUtil.toGrayScale(IconResources.MAP_PIN_RIGHTY_SM));
				
			}
		});
		
		funnImage = new JLabel(IconResources.MAP_PIN_LEFT_SM);
		hendelseImage = new JLabel(IconResources.MAP_PIN_RIGHTY_SM);
		merkelappImage =new JLabel(IconResources.BUBBLE_SM);
		rskjemaCheck.setSelected(true);
		funnCheck.setSelected(true);
		
		JPanel additionsPanel = getHorizPanel();
		additionsPanel.add(funnCheck);
		additionsPanel.add(funnImage);
		additionsPanel.add(rskjemaCheck);
		additionsPanel.add(hendelseImage);
		additionsPanel.add(visMerkelappCheck);
		additionsPanel.add(merkelappImage);
		add(additionsPanel);
	}
		
	private JPanel getHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		return panel;
	}

	/**
	 * @return the funnCheck
	 */
	public JCheckBox getFunnCheck() {
		return funnCheck;
	}

	/**
	 * @return the hendelseCheck
	 */
	public JCheckBox getRskjemaCheck() {
		return rskjemaCheck;
	}

	public JCheckBox getVisMerkelappCheck() {
		return visMerkelappCheck;
	}
	
}
