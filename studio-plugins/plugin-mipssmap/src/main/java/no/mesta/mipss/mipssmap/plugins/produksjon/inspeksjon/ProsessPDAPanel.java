package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.ui.process.JProcessPanel;
import no.mesta.mipss.ui.table.CheckBoxTableObject;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

import org.jdesktop.swingx.JXTable;

/**
 * Panel som inneholder utvalgslister for prosesser og dfu. 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ProsessPDAPanel extends JPanel{

//	private JXTable prosessTable;
	private JXTable pdaTable;
//	private String[] prosessColumns = new String[]{"valgt", "navn"};
	private String[] pdaColumns = new String[]{"valgt", "enhetId", "modell", "navn"};
//	private MipssRenderableEntityTableModel<ProsessTableObject> prosessTableModel;
	private MipssRenderableEntityTableModel<DfuTableObject> pdaTableModel;
	private JProcessPanel prosessPanel;
//	private JCheckBox alleProsesserCheck;
	private JCheckBox alleEnheterCheck;
	private ProsessTableObject ingenProsesser;
	
	private MipssMapModule module;
	public ProsessPDAPanel(MipssMapModule module){
		this.module = module;
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		initGui();
	}
	
	private void initGui(){
//		alleProsesserCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.ProsessPDAPanel.alleProsesserCheck.text"));
		alleEnheterCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.ProsessPDAPanel.alleEnheterCheck.text"));
		
		prosessPanel = new JProcessPanel(module, "${currentKontrakt != null ? currentKontrakt.id : null}", false);
		prosessPanel.setSelected(true);
		JCheckTablePanel<DfuTableObject> pdaScroll = getPdaTableScroll();
		pdaScroll.setSelected(true);
		
		JPanel pdaPanel = new JPanel();
		pdaPanel.setLayout(new GridBagLayout());
		pdaPanel.add(pdaScroll, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		pdaPanel.add(alleEnheterCheck, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
		setSize(prosessPanel, new Dimension(200, 200));
		setSize(pdaPanel, new Dimension(200, 200));
		
		prosessPanel.setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.ProsessPDAPanel.prosessScroll.border")));
		pdaPanel.setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.ProsessPDAPanel.pdaScroll.border")));

		
		JPanel mainPanel = getHorizPanel();
		mainPanel.add(prosessPanel);
		mainPanel.add(Box.createHorizontalStrut(5));
		mainPanel.add(pdaPanel);
		
		add(mainPanel);
		add(Box.createVerticalStrut(5));
	}
	
	private void setSize(JComponent c, Dimension size){
		c.setMinimumSize(size);
		c.setPreferredSize(size);
	}
	
	private JCheckTablePanel<DfuTableObject> getPdaTableScroll(){
		pdaTable = new JXTable();
		List<Dfuindivid> dfuList = module.getMipssMapBean().getPdaForKontrakt(module.getCurrentKontrakt().getId());
		List<DfuTableObject> pdaList = new ArrayList<DfuTableObject>();
		for (Dfuindivid dfu:dfuList){
			pdaList.add(new DfuTableObject(dfu, module.getCurrentKontrakt()));
		}
		pdaTableModel = new MipssRenderableEntityTableModel<DfuTableObject>(DfuTableObject.class, new int[]{0}, pdaColumns);
		pdaTableModel.getEntities().clear();
		pdaTableModel.getEntities().addAll(pdaList);
		pdaTable.setModel(pdaTableModel);
		pdaTable.getColumn(0).setMaxWidth(30);
		pdaTable.getColumn(1).setMaxWidth(50);
		return new JCheckTablePanel<DfuTableObject>(pdaTable, pdaTableModel, alleEnheterCheck);
	}
	
	
	
	private JPanel getHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		return panel;
	}
	private JPanel getVertPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		return panel;
	}
	
	
	public Long[] getAlleValgteEnheter(){
		List<Long> ids = null;
		boolean all = false;
		if (isAlleEnheterValgt()&&pdaTableModel.getEntities().size()>0){
			all = true;
		}
		for (DfuTableObject d:pdaTableModel.getEntities()){
			if (d.getValgt()||all){
				if (ids==null){
					ids = new ArrayList<Long>();
				}
				ids.add(d.getEnhetId());
			}
		}
		if (ids==null)
			return null;
		return ids.toArray(new Long[]{});
	}
	public Long[] getAlleValgteProsesser(){
		/*
		List<Long> pids = null;
		boolean all = false;
		prosessPanel.getAlleValgteProsesser();
		if (isAlleProsesserValgt()&&prosessTableModel.getEntities().size()>0){
			all = true;
		}
		for (ProsessTableObject o:prosessTableModel.getEntities()){
			if ((o.getValgt()||all)&&o.getProsess().getId()!=null){
				if (pids==null)
					pids = new ArrayList<Long>();
				pids.add(o.getProsess().getId());
			}
		}
		if (pids==null)
			return null;
		return pids.toArray(new Long[]{});*/
		return prosessPanel.getAlleValgteProsesser();
	}
	
	public Long[] getAlleValgteUnderprosesser(){
		return prosessPanel.getAlleValgteUnderprosesser();
	}
	public boolean isAlleEnheterValgt(){
		return alleEnheterCheck.isSelected();
	}
	public boolean isAlleProsesserValgt(){
		return prosessPanel.isAlleValgt();
	}
	public boolean isIngenProsesserValgt(){
		return prosessPanel.isIngenValgt();
	}
	
}
