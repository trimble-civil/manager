package no.mesta.mipss.mipssmap.plugins.produksjon.funn;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.mipssmap.task.MipssMapTask;

public abstract class ObservasjonPanel extends JPanel{
	private ImageIcon origIcon;
	private int index;
	private JTabbedPane tab;
	
	public ObservasjonPanel(ImageIcon origIcon, JTabbedPane tab, int index){
		this.origIcon = origIcon;
		this.tab = tab;
		this.index = index;
	}
	
	protected ImageIcon getOrigIcon(){
		return origIcon;
	}
	
	protected JTabbedPane getTabPane(){
		return tab;
	}
	protected int getIndex(){
		return index;
	}
	
	public abstract MipssMapTask getTask();
	public abstract boolean isVisIKart();
	
}
