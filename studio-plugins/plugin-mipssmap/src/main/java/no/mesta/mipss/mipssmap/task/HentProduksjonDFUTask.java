package no.mesta.mipss.mipssmap.task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.CollectionPainter;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mapplugin.util.textboxdata.ProdpunktTextboxProducer;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonCriteria;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unchecked")
public class HentProduksjonDFUTask extends MipssMapTask{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private boolean noDataFound = false;
    private MipssGeoPosition lastPosition;
    private String[] labelText;
    private PointLayerCollection pointLayers;
	private ProduksjonQueryParams params;
	private ProduksjonService produksjon = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
	private final MipssMapModule plugin;
	private final String guiNavn;
	private ProdpunktVO sistePunkt;
	private boolean pda;
	private final ProduksjonCriteria criteria;
	
    public HentProduksjonDFUTask(MipssMapModule plugin, ProduksjonQueryParams params, String guiNavn, ProduksjonCriteria criteria){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.guiNavn = guiNavn;
		this.criteria = criteria;
		this.params = params;
    }
    
    protected PointLayerCollection doInBackground() {
    	if (isCancelled()){
    		return null;
    	}
    	getProgressWorkerPanel().setBarIndeterminate(true);
    	getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
        List<ProdpunktVO> prodpunktList = null;
        	prodpunktList = produksjon.getPdopPunkterDFU(params);
        if (prodpunktList.isEmpty()){
            noDataFound = true;
            return null;
        }
        
        sistePunkt = prodpunktList.get(prodpunktList.size()-1);
        if (sistePunkt!=null){
	        String label = guiNavn.equals("")?getSistePunkt().getDfuId()+"":guiNavn;
	        labelText = new String[]{label, getTimeStringAsCET(getSistePunkt().getCetTidspunkt())};
	        
	        log.debug("doInBackground: fant antall prodpunkter:"+prodpunktList.size());
	        Bounds bounds = new Bounds();
	        List<CollectionEntity> region = new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).convertProdpunkToPointLayers(prodpunktList, bounds);
	        if (!criteria.isLabels())
	        	skjulIdFraTekstboks(region);
	        //lagre det siste punktet
	        ProdpunktPointLayer l =(ProdpunktPointLayer) region.get(region.size()-1);
	        lastPosition = l.getPoint();
	        log.debug("Lastposition:"+lastPosition);
	        
	        log.debug("Bounds til punktlaget:"+bounds);
	        String zoom = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "zoomHentingAvProduksjonPunkt").getVerdi();
	        boolean triggerRecenterAndZoom = true;
	        if (zoom!=null)
	        	triggerRecenterAndZoom = Boolean.valueOf(zoom);
	        pointLayers = new PointLayerCollection(getSistePunkt().getDfuId()+""+getSistePunkt().getGmtTidspunkt()+"", region, bounds, triggerRecenterAndZoom);
	        if (region.size()>20){
	        	pointLayers.setCollectionPainter(new CollectionPainter(pointLayers.getLayers(), pointLayers));
	        }else{
	        	List<ProdpunktPointLayer> ls = (List<ProdpunktPointLayer>)pointLayers.getLayers();
	        	for (ProdpunktPointLayer pl:ls){
	        		pl.setInitGraphics(true);
	        	}
	        }
	        return pointLayers;
        }
        return null;
    }

    private void skjulIdFraTekstboks(List<CollectionEntity> layers){
    	for (CollectionEntity e:layers){
        	ProdpunktPointLayer p = (ProdpunktPointLayer)e;
        	TextboxProducer tp = p.getTextboxProducer();
        	if (tp instanceof ProdpunktTextboxProducer){
        		((ProdpunktTextboxProducer)tp).setSkjulId(true);
        	}
        }
    }

    private String getTimeStringAsCET(Date gmtTime){
    	TimeZone cetTime = TimeZone.getTimeZone("CET");
        DateFormat gmtFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        gmtFormat.setTimeZone(cetTime);
    	return gmtFormat.format(gmtTime);
    }
    
    public MipssGeoPosition getLastPosition() {
		return lastPosition;
	}
    public String[] getLabelText(){
    	return labelText;
    }
    protected void done(){
    	if (!noDataFound){
        }
    	super.done();
    }
    
    public boolean isNoDataFound() {
		return noDataFound;
	}

	public ProdpunktVO getSistePunkt() {
		return sistePunkt;
	}
}
