package no.mesta.mipss.mipssmap.task;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.klimadata.wsklima.StasjonVO;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.textboxdata.VaerstasjonTextboxProducer;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.layer.VaerstasjonLayer;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class HentVaerstasjonerTask extends MipssMapTask{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private MipssMapModule plugin;
	private boolean noDataFound = false;
	
	public HentVaerstasjonerTask(MipssMapModule plugin){
		super(ExecutorTaskType.PARALELL);
		this.plugin = plugin;
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
        List<StasjonVO> stasjoner = plugin.getKlimadataBean().getStations();
        if (stasjoner.isEmpty()){
        	noDataFound = true;
        	return null;
        }
        List<CollectionEntity> layerList = new ArrayList<CollectionEntity>();
        Bounds bounds = new Bounds();
        
        for (final StasjonVO stasjon:stasjoner){
        	GeoPosition gp = new GeoPosition(stasjon.getUtmX(), stasjon.getUtmY());
        	Bounds b = new Bounds();
        	b.setLlx(stasjon.getUtmX());
    		b.setUrx(stasjon.getUtmX());
    		b.setLly(stasjon.getUtmY());
    		b.setUry(stasjon.getUtmY());
    		
        	if (bounds.isNull())
        		bounds.setBounds(b);
        	else
        		bounds.addBounds(b);
        	
        	VaerstasjonLayer layer = new VaerstasjonLayer("Værstasjon", false, gp, IconResources.FORTRESS_ICON);
        	
        	VaerstasjonTextboxProducer tp = new VaerstasjonTextboxProducer(stasjon);
        	layer.setTextboxProducer(tp);
        	layerList.add(layer);
        }
        log.debug("Bounds: {}", bounds);
        PointLayerCollection vaerstasjonLayer = new PointLayerCollection("Vaerstasjoner", layerList, bounds, false);
		return vaerstasjonLayer;
	}
	
	public boolean isNoDataFound() {
		return noDataFound;
	}
}
