package no.mesta.mipss.mipssmap.layer;

import java.util.List;

import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
@Deprecated
public class GpsPointTextboxProducer implements TextboxProducer{

	@Override
	public LayerType getPreferredLayerType() {
		return null;
	}

	@Override
	public List<TextboxData> getTextboxData() {
		return null;
	}
//    
//    private GpsGeoPosition gpsPoint;
//    private Contract contractBean;
//    private WebServiceFacade webservice;
//    
//    public GpsPointTextboxProducer(WebServiceFacade webservice, Contract contractBean, GpsGeoPosition gpsPoint) {
//        this.webservice = webservice;
//        this.gpsPoint = gpsPoint;
//        this.contractBean = contractBean;
//    }
//    
//    public TextboxData getTextboxData() {
//        TextboxData data = new TextboxData();
//        data.setTitle("Prodpunktinfo");
//        //Map<String, String> d = new HashMap<String, String>();
//        
//        String speed = ""+gpsPoint.getGpsPoint().getData().getSpeed();
//        String lat = gpsPoint.getGpsPoint().getData().getLatitude();
//        String lon = gpsPoint.getGpsPoint().getData().getLongitude();
//        String retning = ""+gpsPoint.getGpsPoint().getData().getCourse();
//        String enhet = gpsPoint.getGpsPoint().getData().getUnitID();
//        Calendar c = convertCalendar(gpsPoint.getGpsPoint().getData().getTime());
//        String tid = c.get(Calendar.DAY_OF_MONTH)+"."+(c.get(Calendar.MONTH)+1)+"."+c.get(Calendar.YEAR)+" "+c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);
//        //String[] tid = gpsPoint.getGpsPoint().getData().getTime().toString().split("T");
//        //String[] tid = "21.08.2008T10:34:53".split("T");
//        //String td = tid[0]+" "+tid[1];
//        
//        //String status = gpsPoint.getGpsPoint().getData().getProductionStatus();
//        Produksjonstatus ps = contractBean.getProduksjonStatus(enhet, tid);
//        String status = ps.getProduksjonstatus();
//        String kilometrering = webservice.getRoadInfoInPosition(gpsPoint.getPosition().getLongitude(), gpsPoint.getPosition().getLatitude(), -1);
//        
//        data.addData("Hastighet", speed);
//        data.addData("Kjøretøy", enhet);
//        data.addData("Prodstatus", status);
//        data.addData("Retning", retning);
//        data.addData("Tidspunkt", tid);
//        data.addData("Kilometrering", kilometrering);
//        data.addData("Latitude", lat);
//        data.addData("Longitude", lon);
//        
//        //data.setData(d);
//        
//        return data;
//    }
//    
//    /**
//     * Konverter fra en XMLGregorianCalendar til en Calendar. 
//     * @param c
//     * @return
//     */
//    private Calendar convertCalendar(XMLGregorianCalendar c){
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.YEAR, c.getYear());
//        cal.set(Calendar.MONTH, c.getMonth()-1);
//        cal.set(Calendar.DAY_OF_MONTH, c.getDay());
//        cal.set(Calendar.HOUR_OF_DAY, c.getHour());
//        cal.set(Calendar.MINUTE, c.getMinute());
//        cal.set(Calendar.SECOND, c.getSecond());
//        return cal;
//    }
//
//	@Override
//	public LayerType getPreferredLayerType() {
//		return MipssMapLayerType.TEXTBOX.getType();
//	}

}
