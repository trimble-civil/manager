package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import no.mesta.mipss.mipssmap.utils.ResourceUtil;

/**
 * Panel som inneholder gui komponenter for å gjøre valg på om man ønsker å ta med
 * strekninger eller punkter (alle eller kun siste) 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */

public class PunktStrekningPanel extends JPanel{
	private JCheckBox strekningCheck;
	private JCheckBox punktCheck;
	private JCheckBox labelCheck;
	private JRadioButton kunSisteRadio;
	private JRadioButton alleRadio;
	private JRadioButton tidsvinduRadio;
	private final boolean labels;
	
	public PunktStrekningPanel(boolean labels){
		this.labels = labels;
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.southPanel.border")));
		initGui();
	}
	
	private void initGui(){
		add(createPunktPanel());
		add(createStrekningPanel());
		if (labels){
			add(createLabelPanel());
		}
		add(Box.createHorizontalGlue());
	}
	
	private JPanel createLabelPanel(){
		JPanel panel = createHorizPanel();
		labelCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.labelCheck.text"));
		labelCheck.setSelected(true);
		panel.add(labelCheck);
		return panel;
	}
	private JPanel createStrekningPanel(){
		JPanel panel = createHorizPanel();
		strekningCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.strekningCheck.text"));
		panel.add(strekningCheck);
		return panel;
	}
	
	/**
	 * Lager et horisontalt panel for punkter.
	 * @return
	 */
	private JPanel createPunktPanel(){
		JPanel panel = createHorizPanel();
		punktCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.punktCheck.text"));
		punktCheck.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				if (punktCheck.isSelected()){
					alleRadio.setEnabled(true);
					kunSisteRadio.setEnabled(true);
				}else{
					alleRadio.setEnabled(false);
					kunSisteRadio.setEnabled(false);
				}
			}
		});
		
		ButtonGroup group = new ButtonGroup();
		kunSisteRadio = new JRadioButton(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.kunSisteRadio.text"));
		alleRadio = new JRadioButton(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.alleRadio.text"));
		tidsvinduRadio = new JRadioButton(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonstyperPanel.tidsvinduCheck.text"));
		tidsvinduRadio.setEnabled(Boolean.FALSE);
		group.add(alleRadio);
		group.add(kunSisteRadio);
		group.add(tidsvinduRadio);
		alleRadio.setSelected(true);
		punktCheck.setSelected(true);
		
		panel.add(punktCheck);
		panel.add(alleRadio);
		panel.add(kunSisteRadio);
		panel.add(tidsvinduRadio);
		
		return panel;
	}
	/**
	 * Lager et horisontalt panel som har satt default verdier.
	 * @return
	 */
	private JPanel createHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
		return panel;
	}

	/**
	 * @return the strekningCheck
	 */
	public JCheckBox getStrekningCheck() {
		return strekningCheck;
	}

	/**
	 * @return the punktCheck
	 */
	public JCheckBox getPunktCheck() {
		return punktCheck;
	}
	/**
	 * @return the labelCheck
	 */
	public JCheckBox getLabelCheck() {
		return labelCheck;
	}

	/**
	 * @return the kunSisteRadio
	 */
	public JRadioButton getKunSisteRadio() {
		return kunSisteRadio;
	}

	/**
	 * @return the alleRadio
	 */
	public JRadioButton getAlleRadio() {
		return alleRadio;
	}
	
	public JRadioButton getTidsvinduRadio(){
		return tidsvinduRadio;
	}
}
