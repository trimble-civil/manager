package no.mesta.mipss.mipssmap.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.CollectionPainter;
import no.mesta.mipss.mapplugin.layer.LabelLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.textboxdata.ProdpunktTextboxProducer;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ProdpunktLabelCreator;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

public class HentProdpunktDfuinfoTask extends MipssMapTask{
	
	private final Long dfuId;
	private final Date tidsvinduStart;
	private final Date tidsvinduStopp;
	private final MipssMapModule plugin;
	private ProduksjonService bean = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
	private final PointLayerCollection pointLayers;
	private final Long kjoretoyId;
	private final String guiNavn;
	private final boolean mestaBil;
	private final boolean tidsvindu;
	/**
	 * 
	 * @param plugin mipssmapmodulen
	 * @param dfuId dfu_id til enheten det skal hentes punkter for (kan være null hvis det er et kjøretøy)
	 * @param kjoretoyId kjøretøy_id til kjøretøyet det skal hentes punkter for 
	 * @param tidsvinduStart begynnelsen på tidsvinduet man skal hente data for
	 * @param tidsvinduStopp slutten på tidsvinduet man skal hente data for
	 * @param pointLayers layeret i kartet som punktene skal tilhøre
	 * @param guiNavn guiNavnet til bilen, enten gui_tekst eller regnr
	 * @param mestaBil true hvis bilen tilhører Mesta (brukes da til å lage en gul merkelapp)
	 * @param tidsvindu true hvis punktene skal legges til layeret (layeret vil da inneholde alle punktene innenfor tidsvinduet), false hvis kun siste punkt skal vises.
	 */
	public HentProdpunktDfuinfoTask(MipssMapModule plugin, Long dfuId, Long kjoretoyId, 
			Date tidsvinduStart, Date tidsvinduStopp, PointLayerCollection pointLayers, String guiNavn, 
			boolean mestaBil, boolean tidsvindu){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.dfuId = dfuId;
		this.kjoretoyId = kjoretoyId;
		this.tidsvinduStart = tidsvinduStart;
		this.tidsvinduStopp = tidsvinduStopp;
		this.pointLayers = pointLayers;
		this.guiNavn = guiNavn;
		this.mestaBil = mestaBil;
		this.tidsvindu = tidsvindu;
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
    		return null;
    	}
    	getProgressWorkerPanel().setBarIndeterminate(true);
    	getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
    	
    	Date forrigePunktGmt = null;
    	if (!pointLayers.getLayers().isEmpty()){
	    	CollectionEntity ce = pointLayers.getLayers().get(pointLayers.getLayers().size()-1);
	    	ProdpunktPointLayer p = (ProdpunktPointLayer)ce;
	    	forrigePunktGmt = p.getProdpunktGeoPosition().getProdpunktVO().getGmtTidspunkt();
    	}
    	removeLayersOutOfBounds(tidsvinduStart);
    	List<ProdpunktVO> prodpunktList = bean.getProdpunktFromDfuinfo(dfuId, kjoretoyId, tidsvinduStart, tidsvinduStopp, forrigePunktGmt, tidsvindu);
    	if (prodpunktList!=null&&!prodpunktList.isEmpty()){
    		List<CollectionEntity> layerList = new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).convertProdpunkToPointLayers(prodpunktList, pointLayers.getBounds());
    		if (!tidsvindu){
	    		pointLayers.clearLayers();
	    	}
	    	pointLayers.addPointLayers(layerList);
	    	
	    	if (pointLayers.getLayers().size()>10){
	        	pointLayers.setCollectionPainter(new CollectionPainter(pointLayers.getLayers(), pointLayers));
	        }else{
	        	List<ProdpunktPointLayer> ls = (List<ProdpunktPointLayer>)pointLayers.getLayers();
	        	for (ProdpunktPointLayer pl:ls){
	        		pl.setInitGraphics(true);
	        	}
	        	pointLayers.setCollectionPainter(null);
	        }
	    	
	    	ProdpunktVO prodpunkt = prodpunktList.get(prodpunktList.size()-1);
	    	ProdpunktPointLayer layer = (ProdpunktPointLayer)layerList.get(layerList.size()-1);
	    	
	    	String labelId = "label:"+prodpunkt.getDfuId();
	    	Layer oldLabel = plugin.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), labelId);
	    	Layer label = null;
    		String labelText = guiNavn.equals("")?prodpunkt.getDfuId()+"":guiNavn;
    		String[] text = new String[]{labelText, MipssDateFormatter.formatDate(prodpunkt.getCetTidspunkt(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT)};
    		ProdpunktLabelCreator cr = new ProdpunktLabelCreator();
    		
    		if (mestaBil){
        		label = cr.getMestabilLabel(text, layer.getProdpunktGeoPosition().getGeoPosition(), labelId);
        	}else if (kjoretoyId==null){
        		label = cr.getLabelPDA(text, layer.getProdpunktGeoPosition().getGeoPosition(), labelId);
        	} else{
        		label = cr.getLabelUE(text, layer.getProdpunktGeoPosition().getGeoPosition(), labelId);
        	}
    		if (oldLabel!=null){
    			((LabelLayer)oldLabel).setLabelText(((LabelLayer)label).getLabelText());
    			((LabelLayer)oldLabel).setPosition(((LabelLayer)label).getPosition());
    		}else{
	        	label.setPreferredLayerType(MipssMapLayerType.PRODUKSJONLABEL.getType());
	            plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), label, true);
    		}
	    	
	    	updateProdpunktLayers();
    	}
		return pointLayers;
	}
	
	private void updateProdpunktLayers(){
		int size = pointLayers.getLayers().size();
		CollectionEntity first = pointLayers.getLayers().get(0);
		CollectionEntity last = pointLayers.getLayers().get(size-1);
		Date serieFra = getDateFromProdpunktPointLayer(first);
		Date serieTil = getDateFromProdpunktPointLayer(last);
		
		for (CollectionEntity l:pointLayers.getLayers()){
			if (l instanceof ProdpunktPointLayer){
				ProdpunktPointLayer p = (ProdpunktPointLayer)l;
				TextboxProducer textboxProducer = p.getTextboxProducer();
				if (textboxProducer instanceof ProdpunktTextboxProducer){
					ProdpunktTextboxProducer ptp = (ProdpunktTextboxProducer)textboxProducer;
					ptp.setSerieFra(serieFra);
					ptp.setSerieTil(serieTil);
				}
			}
		}
	}
	private void removeLayersOutOfBounds(Date fra){
		Long dfuId = null;
		List<CollectionEntity> remove = new ArrayList<CollectionEntity>();
		for (CollectionEntity l:pointLayers.getLayers()){
			if (l instanceof ProdpunktPointLayer){
				ProdpunktPointLayer p = (ProdpunktPointLayer)l;
				if (p.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt().before(fra)){
					remove.add(l);
					dfuId = p.getProdpunktGeoPosition().getProdpunktVO().getDfuId();
				}
			}
		}
		
		
		if (!remove.isEmpty()){
			pointLayers.setChanged(true);
			pointLayers.getLayers().removeAll(remove);
		}
		if (pointLayers.getLayers().isEmpty()&&!remove.isEmpty()){
			String labelId = "label:"+dfuId;
			plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), labelId);
		}
	}
	
	private Date getDateFromProdpunktPointLayer(CollectionEntity e){
		Date tid = null;
		if (e instanceof ProdpunktPointLayer){
			ProdpunktPointLayer p = (ProdpunktPointLayer)e;
			tid = p.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt();
		}
		return tid;
	}

}
