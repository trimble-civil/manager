package no.mesta.mipss.mipssmap.plugins.layer.kartlag;

import java.awt.Color;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.mapplugin.utm.UTMProjectionTileFactory;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MipssMapLayerPlugin for å vise frem kartlag
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class KartlagPlugin implements MipssMapLayerPlugin {
    private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssMapText");
    
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MipssMapModule plugin;
    private MipssMapWindowManager windowManager;
    private KartlagDialog dialog;
    
    private JCheckButton button = new JCheckButton("Kartlag");

	private boolean loaded;
    
    public KartlagPlugin(MipssMapModule plugin, MipssMapWindowManager windowManager) {
        this.plugin = plugin;
        this.windowManager = windowManager;
        
        button.getButton().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                kartLagButton_actionPerformed(e);
            }
        });
        button.getCheckBox().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                kartLagCheck_actionPerformed(e);
            }
        });
        
        button.getButton().setToolTipText(resources.getGuiString("grunndata.kartlag.tooltip"));
        button.getCheckBox().setToolTipText(resources.getGuiString("grunndata.vis.kartlag.tooltip"));
    }

    public JCheckButton getCheckButton() {
        return button;
    }
    
    /**
     * Action-metode for kartlag checkbox, viser eller skjuler kartfiltre/lag når den er <code>visible</code>/<code>hidden</code>
     * @param e
     */
    private void kartLagCheck_actionPerformed(ActionEvent e){
    	if (dialog==null&&!loaded){
    		if (button.getCheckBox().isSelected()){
	    		if (plugin.getMainMap().getTileFactory() instanceof UTMProjectionTileFactory){
	            	KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
	            	String blendCol = params.hentEnForApp("studio.mipss.rodegenerator", "mapBlendColor").getVerdi();
	            	String blendVal = params.hentEnForApp("studio.mipss.rodegenerator", "mapBlendValue").getVerdi();
	            	Color color = ColorUtil.fromHexString(blendCol);
	            	Float val = Float.valueOf(blendVal);
	            	((UTMProjectionTileFactory)plugin.getMainMap().getTileFactory()).setColorFilter(color, val);
	            }
    		}else{
    			if (plugin.getMainMap().getTileFactory() instanceof UTMProjectionTileFactory){
	            	((UTMProjectionTileFactory)plugin.getMainMap().getTileFactory()).removeColorFilter();
	            }
    		}
    		plugin.getMainMap().repaint();
    	}
    	
    	if (dialog!=null){
    		if (button.getCheckBox().isSelected()){
    			dialog.getPanel().restoreAllSelected();
    		}else{
    			dialog.getPanel().setAllSelected(false);
    			plugin.getMainMap().repaint();
    		}
    	}
    }
    /**
     * Action-metode for kartlag knappen
     * @param e
     */
    private void kartLagButton_actionPerformed(ActionEvent e){
        
        Container parent = getCheckButton();
        while (!(parent instanceof Window)){
            parent = parent.getParent();
        }
        dialog = (KartlagDialog)windowManager.getDialog(KartlagDialog.class);
        if (dialog==null){
        	dialog = new KartlagDialog((Window)parent, plugin, windowManager, this);
        	dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartlagDialog.title"));
        	dialog.pack();
        	dialog.setLocationRelativeTo(null);
        	windowManager.addDialog(dialog);
        }
        dialog.setVisible(true);
        
    }

	@Override
	public void reset() {
		dialog = null;
		button.getCheckBox().setSelected(false);
		loaded = false;
		if (plugin.getMainMap().getTileFactory() instanceof UTMProjectionTileFactory){
        	((UTMProjectionTileFactory)plugin.getMainMap().getTileFactory()).removeColorFilter();
        }
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		
	}
}
