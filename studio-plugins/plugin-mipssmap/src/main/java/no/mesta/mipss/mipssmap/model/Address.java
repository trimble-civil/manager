package no.mesta.mipss.mipssmap.model;

import java.awt.Point;

import no.mesta.mipss.mapplugin.MipssGeoPosition;

public class Address {

	private String address;
	private MipssGeoPosition point;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public MipssGeoPosition getPoint() {
		return point;
	}
	public void setPoint(MipssGeoPosition point) {
		this.point = point;
	}
	
	public String toString(){
		return address;
	}
	
	
}
