package no.mesta.mipss.mipssmap.swing;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.JCheckButtonPanel;

/**
 * Panelet som viser frem knappene for grunndata
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class GrunndataPanel extends JCheckButtonPanel{
    
    private final List<MipssMapLayerPlugin> plugins;

	public GrunndataPanel(List<MipssMapLayerPlugin> plugins) {
        this.plugins = plugins;
		List<JCheckButton> buttonList = new ArrayList<JCheckButton>();
        
        for (MipssMapLayerPlugin p:plugins){
            buttonList.add(p.getCheckButton());
        }
        super.setCheckButtonList(buttonList);
    }
    
    public void setEnabled(boolean enabled){
    	for (MipssMapLayerPlugin p:plugins){
            p.setEnabled(enabled);
        }
    }
}
