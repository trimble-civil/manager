package no.mesta.mipss.mipssmap.tools;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.util.concurrent.ExecutionException;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import no.mesta.mipss.klimadata.yr.WeatherChart;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.task.HentYrDataTask;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class YrWeatherDataTool implements MouseInputListener{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private MipssMapModule plugin;
	private MapPanel mapPanel;
	private JDialog dialog ;
	private JPanel chartPanel;
	private WeatherChart chart;
	
	public YrWeatherDataTool(MipssMapModule plugin, MapPanel mapPanel){
		this.plugin = plugin;
		this.mapPanel = mapPanel;
		chartPanel = new JPanel(new BorderLayout());
		//this.windowManager = windowManager;
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		e.consume();
		final GeoPosition pos = mapPanel.convertPointToGeoPosition(e.getPoint());
		
		Component owner = plugin.getModuleGUI();
        while (!(owner instanceof Window)){
            owner = owner.getParent();
        }
        
        final String title = ResourceUtil.getResource("mipssmap.tools.YrWeatherDataTool.dialog.title")+pos.getLatitude()+ResourceUtil.getResource("mipssmap.tools.YrWeatherDataTool.dialog.title2")+pos.getLongitude()+")";
        if (dialog==null){
			dialog  = new JDialog((Window)owner, title);
			dialog.setLayout(new BorderLayout());
			dialog.setSize(875, 330);
			dialog.setLocationRelativeTo(null);
			dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
			dialog.add(chartPanel);
		}

		new Thread(){
			public void run(){
				HentYrDataTask task = new HentYrDataTask(plugin, pos, dialog);
				ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.tools.YrWeatherDataTool.HentYrDataTask.title"));
		        workerPanel.setWorker(task);
		        task.setProgressController(plugin.getProgressController());
		        task.setProgressWorkerPanel(workerPanel);
		        plugin.getProgressController().addWorkerPanel(workerPanel);
				task.execute();
				try {
					task.get();
					chart = task.getWeatherChart();
					chartPanel.removeAll();
					chartPanel.add(chart);
					dialog.setTitle(title);
					
					dialog.setVisible(false);
					if (!dialog.isVisible())
						dialog.setVisible(true);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	@Override
	public void mousePressed(MouseEvent e) {}
	@Override
	public void mouseReleased(MouseEvent e) {}
	@Override
	public void mouseDragged(MouseEvent e) {}
	@Override
	public void mouseMoved(MouseEvent e) {}

}
