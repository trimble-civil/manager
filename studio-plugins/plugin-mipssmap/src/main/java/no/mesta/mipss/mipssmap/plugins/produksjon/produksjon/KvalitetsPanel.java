package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.UIUtils;

public class KvalitetsPanel extends JPanel{

	private JTextField gFra;
	private JLabel green = new JLabel(ResourceUtil.getResource("green"));
	private JCheckBox greenVisible;
	private JRadioButton grunndataRadio;
	private JRadioButton dfuRadio;
	
	
	private JTextField gTil;
	private JRadioButton hastighetRadio;
	private JRadioButton pdopRadio;
	
	private final ProduksjonPlugin plugin;
	
	private JCheckBox prodpunktCheck;
	
	private JCheckBox prodpunktinfoCheck;
	private JLabel red = new JLabel(ResourceUtil.getResource("red"));
	private JCheckBox redVisible;
	
	private JTextField rFra;
	private JTextField rTil;
	private JRadioButton vanligRadio;
	
	private JLabel yellow = new JLabel(ResourceUtil.getResource("yellow"));
	private JCheckBox yellowVisible;
	private JTextField yFra;
	
	private JTextField yTil;
	
	private JTextField txtDfuId;
	
	private ActionListener enable;
	public KvalitetsPanel(ProduksjonPlugin plugin){
		this.plugin = plugin;
		setLayout(new BorderLayout());
		initGui();
	}
	private JPanel createCheckButtonPanel(JCheckBox btn){
		JPanel panel = createHorizPanel();
		panel.add(btn);
		panel.add(Box.createHorizontalGlue());
		
		return panel;
	}
	
	private JPanel createColorPanel(JTextField fra, JTextField til, JLabel label, JCheckBox vis, Color bg){
		vis.setOpaque(false);
		JPanel panel = createHorizPanel();
		panel.add(label);
		panel.add(UIUtils.getHStrut(5));
		panel.add(new JLabel("fra"));
		panel.add(UIUtils.getHStrut(5));
		panel.add(fra);
		panel.add(UIUtils.getHStrut(5));
		panel.add(new JLabel("til"));
		panel.add(til);
		panel.add(UIUtils.getHStrut(5));
		panel.add(vis);
		panel.add(Box.createHorizontalGlue());
		panel.setBackground(bg);
		return panel;
	}
	private JPanel createDfuPanel(){
		
		txtDfuId = new JTextField();
		initTF(txtDfuId);
		JLabel lblDfu = new JLabel(ResourceUtil.getResource("label.dfuId"));
		initLabel(lblDfu);
		JPanel panel = UIUtils.getVertPanel();
		panel.setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("label.dfu")));
		
		JPanel hPanel = createHorizPanel();
		hPanel.add(lblDfu);
		hPanel.add(UIUtils.getHStrut(5));
		hPanel.add(txtDfuId);
		hPanel.add(Box.createHorizontalGlue());
		panel.add(hPanel);
		
//		
//		JPanel pnl = new JPanel(new GridBagLayout());
//		pnl.add(lblDfu,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
//		pnl.add(txtDfuId, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		
		return panel;
	}
	private JPanel createGrunndataPanel(){
		JPanel panel = UIUtils.getVertPanel();
		panel.setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("grunndata")));
		panel.add(createCheckButtonPanel(prodpunktCheck));
		panel.add(createCheckButtonPanel(prodpunktinfoCheck));
		return panel;
	}
	/**
	 * Lager et horisontalt panel som har satt default verdier.
	 * @return
	 */
	private JPanel createHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
		return panel;
	}
	private JPanel createMainPanel(){
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		return mainPanel;
	}
	
	private JPanel createPDOPPanel(){
		JPanel panel = UIUtils.getVertPanel();
		rFra = new JTextField();
		gFra = new JTextField();
		yFra = new JTextField();
		rFra.setDocument(new DocumentFilter(5, "0123456789.", false));
		gFra.setDocument(new DocumentFilter(5, "0123456789.", false));
		yFra.setDocument(new DocumentFilter(5, "0123456789.", false));
		rTil = new JTextField();
		gTil = new JTextField();
		yTil = new JTextField();
		rTil.setDocument(new DocumentFilter(5, "0123456789.", false));
		gTil.setDocument(new DocumentFilter(5, "0123456789.", false));
		yTil.setDocument(new DocumentFilter(5, "0123456789.", false));
		
		redVisible = new JCheckBox("Vis");
		yellowVisible = new JCheckBox("Vis");
		greenVisible = new JCheckBox("Vis");
		redVisible.setSelected(true);
		yellowVisible.setSelected(true);
		greenVisible.setSelected(true);
		redVisible.setEnabled(false);
		yellowVisible.setEnabled(false);
		greenVisible.setEnabled(false);
		String r1 = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "pdopRedValue1").getVerdi();
		String r2 = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "pdopRedValue2").getVerdi();
		
		String y1 = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "pdopYellowValue1").getVerdi();
		String y2 = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "pdopYellowValue2").getVerdi();
		
		String g1 = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "pdopGreenValue1").getVerdi();
		String g2 = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "pdopGreenValue2").getVerdi();
		
		initTF(rFra);
		initTF(rTil);
		
		initTF(yFra);
		initTF(yTil);
		
		initTF(gFra);
		initTF(gTil);
		
		rFra.setText(r1);
		rTil.setText(r2);
		
		yFra.setText(y1);
		yTil.setText(y2);
		
		gFra.setText(g1);
		gTil.setText(g2);
		
		initLabel(red);
		initLabel(yellow);
		initLabel(green);
		red.setForeground(new Color(150,0,0));
		yellow.setForeground(new Color(150,150,0));
		green.setForeground(new Color(0,150,0));
		Color light = new Color(230,230,230);
		Color dark = new Color(210,210,210);
		panel.add(createColorPanel(rFra, rTil, red, redVisible, dark));
		panel.add(createColorPanel(yFra, yTil, yellow, yellowVisible, light));
		panel.add(createColorPanel(gFra, gTil, green, greenVisible, dark));
		
		panel.setBorder(BorderFactory.createTitledBorder("Grenseverdier for PDOP"));
		return panel;
	}
	private JPanel createRadioButtonPanel(JRadioButton btn){
		JPanel panel = createHorizPanel();
		panel.add(btn);
		panel.add(Box.createHorizontalGlue());
		
		return panel;
	}
	public Double getGfra(){
		return Double.parseDouble(gFra.getText());
	}
	public Double getGtil(){
		return Double.parseDouble(gTil.getText());
	}
	public Double getRfra(){
		return Double.parseDouble(rFra.getText());
	}
	
	public Double getRtil(){
		return Double.parseDouble(rTil.getText());
	}
	
	public Double getYfra(){
		return Double.parseDouble(yFra.getText());
		
	}
	public Double getYtil(){
		return Double.parseDouble(yTil.getText());
	}
	
	private void initGui(){
		JPanel mainPanel = createMainPanel();	
		
		prodpunktCheck = new JCheckBox("Vis fra prodpunkt (grunndata)");
		prodpunktinfoCheck = new JCheckBox("Vis fra prodpunktinfo (behandlede data)");
		prodpunktCheck.setEnabled(false);
		prodpunktinfoCheck.setEnabled(false);
		
		ButtonGroup group = new ButtonGroup();
		vanligRadio = new JRadioButton(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.KvalitetsPanel.vanligRadio.text"));
		hastighetRadio = new JRadioButton(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.KvalitetsPanel.hastighetRadio.text"));
		hastighetRadio.setEnabled(false);
		grunndataRadio = new JRadioButton("Grunndata");
		pdopRadio = new JRadioButton("PDOP visning");
		dfuRadio = new JRadioButton("DFU (Hent for kun en DFU)");
		
		group.add(vanligRadio);
		group.add(hastighetRadio);
		group.add(grunndataRadio);
		group.add(pdopRadio);
		group.add(dfuRadio);
		

		enable = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (grunndataRadio.isSelected()) {
					prodpunktCheck.setEnabled(true);
					prodpunktinfoCheck.setEnabled(true);
				} else {
					prodpunktCheck.setEnabled(false);
					prodpunktinfoCheck.setEnabled(false);
				}
				if (pdopRadio.isSelected()){
					rFra.setEnabled(true);
					yFra.setEnabled(true);
					gFra.setEnabled(true);
					rTil.setEnabled(true);
					yTil.setEnabled(true);
					gTil.setEnabled(true);
					redVisible.setEnabled(true);
					yellowVisible.setEnabled(true);
					greenVisible.setEnabled(true);
				}else{
					rFra.setEnabled(false);
					yFra.setEnabled(false);
					gFra.setEnabled(false);
					rTil.setEnabled(false);
					yTil.setEnabled(false);
					gTil.setEnabled(false);
					redVisible.setEnabled(false);
					yellowVisible.setEnabled(false);
					greenVisible.setEnabled(false);
				}
				if (dfuRadio.isSelected()){
					txtDfuId.setEnabled(true);
				}else{
					txtDfuId.setEnabled(false);
				}
			}
		};
		
		vanligRadio.addActionListener(enable);
		grunndataRadio.addActionListener(enable);
		pdopRadio.addActionListener(enable);
		dfuRadio.addActionListener(enable);
		
		vanligRadio.setSelected(true);
		JPanel vanligPanel = createRadioButtonPanel(vanligRadio);
//		JPanel hastighetPanel = createRadioButtonPanel(hastighetRadio);
//		JPanel grunndataPanel = createRadioButtonPanel(grunndataRadio);
		JPanel pdopPanel = createRadioButtonPanel(pdopRadio);
		JPanel dfuPanel = createRadioButtonPanel(dfuRadio);
		
		mainPanel.add(vanligPanel);
//		mainPanel.add(hastighetPanel);
//		mainPanel.add(grunndataPanel);
//		mainPanel.add(createGrunndataPanel());
		
		if (UserUtils.isAdmin(plugin.getPlugin().getLoader().getLoggedOnUser(true))){
			mainPanel.add(pdopPanel);
			mainPanel.add(createPDOPPanel());
			mainPanel.add(dfuPanel);
			mainPanel.add(createDfuPanel());
		}else{
			createPDOPPanel();
			createDfuPanel();
		}
		
		add(mainPanel, BorderLayout.CENTER);	
	}
	
	private void initLabel(JLabel label){
		Dimension size = new Dimension(50,20);
		label.setPreferredSize(size);
		label.setMinimumSize(size);
		label.setMaximumSize(size);
	}
	
	private void initTF(JTextField f){
		f.setEnabled(false);
		Dimension size = new Dimension(40, 20);
		f.setPreferredSize(size);
		f.setMinimumSize(size);
		f.setMaximumSize(size);
	}
	
	public boolean isGreenVisible(){
		return greenVisible.isSelected();
	}
	
	/**
	 * Returnerer true hvis produksjon som hastighet er valgt
	 * @return
	 */
	public boolean isHastighet(){
		return hastighetRadio.isSelected();
	}
	public boolean isPDOP(){
		if (pdopRadio.isSelected())
			return true;
		return false;
	}
	
	public boolean isRedVisible(){
		return redVisible.isSelected();
	}
	
	public boolean isGrunndata(){
		return grunndataRadio.isSelected();
	}
	
	public boolean isDfu(){
		return dfuRadio.isSelected();
	}
	/**
	 * Returnerer true hvis vanlig visning er valgt
	 * @return
	 */
	public boolean isVanlig(){
		return vanligRadio.isSelected();
	}
	public void setVanligRadio(Boolean vanligVisning){
		vanligRadio.setSelected(vanligVisning);
		pdopRadio.setEnabled(!vanligVisning);
		enable.actionPerformed(null);
	}
	public boolean isYellowVisible(){
		return yellowVisible.isSelected();
	}
	public Long getDfuId() {
		return Long.valueOf(txtDfuId.getText());
	}
}
