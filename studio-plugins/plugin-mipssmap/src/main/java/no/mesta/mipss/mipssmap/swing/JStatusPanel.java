package no.mesta.mipss.mipssmap.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import no.mesta.mipss.mapplugin.util.CoordinateEvent;
import no.mesta.mipss.mapplugin.util.CoordinateMessageListener;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JXProgressBar;
import no.mesta.mipss.ui.progressbar.ProgressController;
import no.mesta.mipss.ui.progressbar.ProgressPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * StatusPanel for the map
 * 
 * Implements a PaintMessageListener for receiving information on the progress of painting the map
 * and CoordinateMessageListener for receiving information about the actual coordinate under the mouse cursor
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class JStatusPanel extends JPanel implements CoordinateMessageListener{

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private JXProgressBar progressBar = new JXProgressBar();
    private JLabel eastingLabel;
    private JLabel northingLabel;
    private JLabel eastingValue;
    private JLabel northingValue;
    
    private JButton removeAllLayers;
    
    private ProgressPanel progressPanel ;
	private ProgressController progressController;
	
    //private JXBusyLabel mapInfoLabel;
    private JLabel infoLabel;
    
    private MipssMapModule plugin;
    
    private JDialog workerDialog;
    
    public JStatusPanel(final MipssMapModule plugin) {
    	this.plugin = plugin;
        progressBar.setPreferredSize(new Dimension(150, 20));
        progressBar.setSize(150, 20);
//        progressBar.setStringPainted(true);
        progressPanel = new ProgressPanel(progressBar);
        progressController = new ProgressController(progressPanel);
        
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        
        eastingLabel = new JLabel(ResourceUtil.getResource("mipssmap.swing.JStatusPanel.eastingLabel.text"));
        northingLabel = new JLabel(ResourceUtil.getResource("mipssmap.swing.JStatusPanel.northingLabel.text"));
        eastingValue = new JLabel();
        northingValue = new JLabel();
        
        removeAllLayers = new JButton(IconResources.DELETE_LAYERS);
        removeAllLayers.setMargin(new Insets(0,0,0,0));
        removeAllLayers.setToolTipText(ResourceUtil.getResource("toolbar.removeAllLayers.tooltip"));
        removeAllLayers.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		plugin.getMainMap().getLayerHandler().removeAllLayers();
        	}
        });
        
        //mapInfoLabel = new JXBusyLabel(new Dimension(20,15));
        infoLabel = new JLabel();
        
        JPanel coordinatePanel = new JPanel();
        coordinatePanel.setLayout(new BoxLayout(coordinatePanel, BoxLayout.LINE_AXIS));
        
        Dimension valueSize = new Dimension(120,20);
        Dimension sepSize = new Dimension(2, 20);
        
        coordinatePanel.add(northingLabel);
        coordinatePanel.add(Box.createRigidArea(new Dimension(5,0)));
        setComponentSize(valueSize, northingValue);
        coordinatePanel.add(northingValue);
        
        JSeparator sep = new JSeparator(JSeparator.VERTICAL);
        setComponentSize(sepSize, sep);
        coordinatePanel.add(sep);
        
        coordinatePanel.add(eastingLabel);
        coordinatePanel.add(Box.createRigidArea(new Dimension(5,0)));
        setComponentSize(valueSize, eastingValue);
        coordinatePanel.add(eastingValue);
        
        
        add(coordinatePanel);
        
        JSeparator sep1 = new JSeparator(JSeparator.VERTICAL);
        setComponentSize(sepSize, sep1);
        add(sep1);
        
        add(Box.createHorizontalGlue());
        JSeparator sep2 = new JSeparator(JSeparator.VERTICAL);
        setComponentSize(sepSize, sep2);
        add(sep2);
        Dimension infoSize = new Dimension(100, 20);
        setComponentSize(infoSize, infoLabel);
        //add(mapInfoLabel);
        add(infoLabel);
        
        add(Box.createRigidArea(new Dimension(5,0)));
        
        add(removeAllLayers);
        JSeparator sep3 = new JSeparator(JSeparator.VERTICAL);
        setComponentSize(sepSize, sep3);
        add(sep3);
        
        Dimension progressSize = new Dimension(150, 20);
        setComponentSize(progressSize, progressBar);
        add(progressBar);
        
        progressBar.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if (e.getClickCount()==2){
					if (workerDialog == null){
						Component c = getParent();
						while (!(c instanceof Window)){
							c = c.getParent();
						}
						workerDialog = new JDialog((Window)c);
						workerDialog.add(progressPanel);
						workerDialog.setTitle(ResourceUtil.getResource("mipssmap.swing.JStatusPanel.workerDialog.title"));
						workerDialog.pack();
						workerDialog.setLocationRelativeTo(null);
						workerDialog.setVisible(true);
						progressPanel.setDialog(workerDialog);
						
					}
					else{
						workerDialog.setVisible(true);
						workerDialog.requestFocus();
					}
					
				}
			}
		});
        startProgressThread();
    }
    
    private void startProgressThread(){
    	
    	new Thread("JStatusPanel update thread"){
    		
			public void run(){
				while (plugin.isRunning()){
					try{
						if (progressPanel.getUiList().isEmpty()){
							progressBar.setString(ResourceUtil.getResource("mipssmap.swing.JStatusPanel.progressBar.defaultText"));
							if (progressBar.isIndeterminate())
								progressBar.setIndeterminate(false);
						}else{
							progressBar.setString(ResourceUtil.getResource("mipssmap.swing.JStatusPanel.progressBar.workingText1")+progressPanel.getUiList().size()+ResourceUtil.getResource("mipssmap.swing.JStatusPanel.progressBar.workingText2"));
							if (!progressBar.isIndeterminate())
								progressBar.setIndeterminate(true);
						}
						Thread.sleep(500);
					}catch (Exception e){
						
					}
				}
			}
		}.start();
    }
    /**
     * Set the component size
     * @param dim size
     * @param comp JComponent
     */
    private void setComponentSize(Dimension dim, JComponent comp){
        comp.setPreferredSize(dim);
        comp.setMinimumSize(dim);
        comp.setMaximumSize(dim);
    }
    /**
     * Returns the progressbar from the statuspanel
     * @return
     */
    public JXProgressBar getProgressBar(){
        return progressBar;
    }
    
    /**
     * Set the value to be displayed as north coordinate
     * @param value
     */
    public void setNorthingValue(String value){
        northingValue.setText(value);
    }
    /**
     * Set the value to be displayed as east coordinate
     * @param value
     */
    public void setEastingValue(String value){
        eastingValue.setText(value);
    }
    
    /**
     * Set a text on the infolabel and start the JXBusyLabel 
     * @param value
     */
    public void startMapInfo(final String value){
        infoLabel.setText(value);
        //mapInfoLabel.setBusy(true);
    }
    /**
     * Set a text on the infoLabel and stopp the JXBusyLabel from spinning
     * @param value
     */
    public void stopMapInfo(String value){
        infoLabel.setText(value);
        //mapInfoLabel.setBusy(false);
    }
    /**
     * A Message is received
     * @param messageEvent
     */
//    public void message(MessageEvent messageEvent) {
//        paintStart(messageEvent);
//    }
//    
//    
//    public void paintStart(MessageEvent e) {
//        startMapInfo(e.getMessage());
//    }
//
//    public void paintStop(MessageEvent e) {
//        stopMapInfo(e.getMessage());
//    }
    
    /**
     * Event received 
     * @param e
     */
    public void coordinateChanged(CoordinateEvent e) {
        setNorthingValue(e.getNorthing());
        setEastingValue(e.getEasting());
    }

	/**
	 * @return the controller
	 */
	public ProgressController getProgressController() {
		return progressController;
	}
}

