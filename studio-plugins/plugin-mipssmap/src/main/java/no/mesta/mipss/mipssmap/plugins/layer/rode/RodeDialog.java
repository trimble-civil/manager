package no.mesta.mipss.mipssmap.plugins.layer.rode;

import java.awt.BorderLayout;
import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JTabbedPane;

import no.mesta.mipss.mipssmap.MipssMapController;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;


public class RodeDialog extends JDialog{

    private JTabbedPane tabPane;
    private RodePanel rodePanel;
    
    public RodeDialog(Window owner, MipssMapModule module, RodePlugin plugin) {
        super(owner);
        
        setLayout(new BorderLayout());
        tabPane = new JTabbedPane();
        
        rodePanel = new RodePanel(module, plugin, this);
        tabPane.addTab(ResourceUtil.getResource("mipssmap.plugins.layer.rode.RodeDialog.tab.rodepanel"), rodePanel);
        add(tabPane);
        
    }
}
