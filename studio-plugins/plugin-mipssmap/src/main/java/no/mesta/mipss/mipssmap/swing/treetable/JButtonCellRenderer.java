package no.mesta.mipss.mipssmap.swing.treetable;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * CellRenderer that displays a JButton
 * @author Harald
 *
 */
public class JButtonCellRenderer extends JPanel implements TableCellRenderer {
	private JButton button;
	
	
	public JButtonCellRenderer(String btntext){
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setOpaque(false);
		button = new JButton(btntext);
		button.setFont(new Font("ARIAL", Font.PLAIN, 10));
		
		add(Box.createVerticalStrut(1));
		add(getButtonPanel(button));
		add(Box.createVerticalStrut(1));
	}
	
	private JPanel getButtonPanel(JButton button){
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.add(button);
		panel.add(Box.createHorizontalGlue());
		return panel;
		
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		return this;
	}
}
