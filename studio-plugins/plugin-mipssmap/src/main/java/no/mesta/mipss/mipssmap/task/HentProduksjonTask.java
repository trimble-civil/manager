package no.mesta.mipss.mipssmap.task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.CollectionPainter;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mapplugin.util.textboxdata.ProdpunktTextboxProducer;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonCriteria;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 * Henter produksjonspunkter fra databasen og lager et PointLayerCollection layer til kartet
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 * @see PointLayerCollection
 */
@SuppressWarnings("unchecked")
public class HentProduksjonTask extends MipssMapTask{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private boolean noDataFound = false;
    private MipssGeoPosition lastPosition;
    private String[] labelText;
    private Long id;
    private PointLayerCollection pointLayers;
    private boolean kjoretoy;
	private ProduksjonQueryParams params;
	private ProduksjonService produksjon = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
	private final MipssMapModule plugin;
	private final String guiNavn;
	private ProdpunktVO sistePunkt;
	private final boolean mestaBil;
	private boolean pda;
	private final ProduksjonCriteria criteria;
	
    public HentProduksjonTask(MipssMapModule plugin, ProduksjonQueryParams params, String guiNavn, boolean mestaBil, ProduksjonCriteria criteria){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.guiNavn = guiNavn;
		this.mestaBil = mestaBil;
		this.criteria = criteria;
        if (params.getKjoretoyId()!=null){
        	this.id = params.getKjoretoyId();
        	kjoretoy = true;
        }
        if (params.getDfuId()!=null){
        	this.id = params.getDfuId();
        }
		this.params = params;
    }
    
    protected PointLayerCollection doInBackground() {
    	if (isCancelled()){
    		return null;
    	}
    	getProgressWorkerPanel().setBarIndeterminate(true);
    	getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
        List<ProdpunktVO> prodpunktList = null;
        
        if (kjoretoy){
	        if (params.isKunSiste()){
	        	prodpunktList = new ArrayList<ProdpunktVO>();
	        	ProdpunktVO sisteProdpunkt = produksjon.getSisteProduksjonPunktKjoretoy(params);
	        	prodpunktList.add(sisteProdpunkt);
	        }else{
	        	prodpunktList = produksjon.getProduksjonPunktKjoretoy(params);
	        	System.out.println(prodpunktList);
	        	
	        }
        }else{
        	if (params.isKunSiste()){
	        	prodpunktList = new ArrayList<ProdpunktVO>();
	        	ProdpunktVO sisteProdpunkt = produksjon.getSisteProduksjonPunktDfu(params);
	        	prodpunktList.add(sisteProdpunkt);
	        }else{
	        	prodpunktList = produksjon.getProduksjonPunktDfu(params);
	        }
        	pda = true;
        }
        
        if (prodpunktList.isEmpty()){
            noDataFound = true;
            return null;
        }
        
        sistePunkt = prodpunktList.get(prodpunktList.size()-1);
        if (sistePunkt!=null){
	        String label = guiNavn.equals("")?getSistePunkt().getDfuId()+"":guiNavn;
	        labelText = new String[]{label, getTimeStringAsCET(getSistePunkt().getCetTidspunkt())};
	        
	        log.debug("doInBackground: fant antall prodpunkter:"+prodpunktList.size());
	        Bounds bounds = new Bounds();
	        List<CollectionEntity> region = new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).convertProdpunkToPointLayers(prodpunktList, bounds);
	        if (!criteria.isLabels())
	        	skjulIdFraTekstboks(region);
	        //lagre det siste punktet
	        ProdpunktPointLayer l =(ProdpunktPointLayer) region.get(region.size()-1);
	        lastPosition = l.getPoint();
	        log.debug("Lastposition:"+lastPosition);
	        
	        log.debug("Bounds til punktlaget:"+bounds);
	        String zoom = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "zoomHentingAvProduksjonPunkt").getVerdi();
	        boolean triggerRecenterAndZoom = true;
	        if (zoom!=null)
	        	triggerRecenterAndZoom = Boolean.valueOf(zoom);
	        pointLayers = new PointLayerCollection(getSistePunkt().getDfuId()+""+getSistePunkt().getGmtTidspunkt()+"", region, bounds, triggerRecenterAndZoom);
	        if (region.size()>20){
	        	pointLayers.setCollectionPainter(new CollectionPainter(pointLayers.getLayers(), pointLayers));
	        }else{
	        	List<ProdpunktPointLayer> ls = (List<ProdpunktPointLayer>)pointLayers.getLayers();
	        	for (ProdpunktPointLayer pl:ls){
	        		pl.setInitGraphics(true);
	        	}
	        }
	        return pointLayers;
        }
        return null;
    }

    private void skjulIdFraTekstboks(List<CollectionEntity> layers){
    	for (CollectionEntity e:layers){
        	ProdpunktPointLayer p = (ProdpunktPointLayer)e;
        	TextboxProducer tp = p.getTextboxProducer();
        	if (tp instanceof ProdpunktTextboxProducer){
        		((ProdpunktTextboxProducer)tp).setSkjulId(true);
        	}
        }
    }

    private String getTimeStringAsCET(Date gmtTime){
    	TimeZone cetTime = TimeZone.getTimeZone("CET");
        DateFormat gmtFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        gmtFormat.setTimeZone(cetTime);
    	return gmtFormat.format(gmtTime);
    }
    
    public MipssGeoPosition getLastPosition() {
		return lastPosition;
	}
    public String[] getLabelText(){
    	return labelText;
    }
    
    public boolean isMestaBil(){
    	return mestaBil;
    }
    public boolean isPda(){
    	return pda;
    }

    protected void done(){
    	if (!noDataFound){
//        	plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONPUNKT.getType(), pointLayers, true);
//	    	MipssGeoPosition lastPosition = getLastPosition();
//	        LabelLayer label = new LabelLayer(lastPosition.toString(), lastPosition, getLabelText());
//	        plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.LABEL.getType(), label, false);
        }
    	super.done();
    }
    
    public boolean isNoDataFound() {
		return noDataFound;
	}

	public ProdpunktVO getSistePunkt() {
		return sistePunkt;
	}
}
