package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ProduksjonOptions;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXTable;

/**
 * @deprecated
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class ProduksjonPanel extends JPanel{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private JCheckBox alleKjoretoyCheck;
    private JCheckBox alleProsesserCheck;
    private JRadioButton alleRadio;
    private JButton avbrytButton;
    private JButton brukButton;
    private ButtonGroup gpsValgGroup;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JPanel kjoretoyPanel;
    
    private JXTable prosessTable;
    private JXTable kjoretoyTable;
    
    private JPanel buttonPanel;
    private JPanel prosesserPanel;
    private JRadioButton sisteRadio;
    private JCheckBox visGpsCheck;
    private JCheckBox visStrekningerCheck;
    private JButton okButton;
    private MipssBeanTableModel<Prodtype> prosessTableModel;
    private MipssBeanTableModel<Dfuindivid> dfuindividTableModel;
    private MipssMapModule module;
    
    private ProduksjonPlugin plugin;
    public ProduksjonPanel(ProduksjonOptions options, ProduksjonPlugin plugin, MipssMapModule module) {
         //this.controller= controller;
         this.plugin = plugin;
         this.module = module;
         initComponents();
    }
    
    private void initComponents() {
        gpsValgGroup = new ButtonGroup();
        kjoretoyPanel = new JPanel();
        jScrollPane1 = new JScrollPane();
        //enhetList = new org.jdesktop.swingx.JXList();
        visGpsCheck = new JCheckBox();
        alleRadio = new JRadioButton();
        sisteRadio = new JRadioButton();
        buttonPanel = new JPanel();
        visStrekningerCheck = new JCheckBox();
        alleKjoretoyCheck = new JCheckBox();
        prosesserPanel = new JPanel();
        alleProsesserCheck = new JCheckBox();
        jScrollPane2 = new JScrollPane();
        prosessTable = getProsessTable();
        
        brukButton = new JButton();
        avbrytButton = new JButton();
        okButton = new JButton();
        
        
        kjoretoyPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.kjoretoyPanel.border.title")));
        
        kjoretoyTable = getDfuindividTable();
        jScrollPane1.setViewportView(kjoretoyTable);

        visGpsCheck.setText(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.visGpsCheck.text"));
        visGpsCheck.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                visGpsCheckActionPerformed(e);
            }                    
        });
        visGpsCheck.setSelected(true);
        gpsValgGroup.add(alleRadio);
        alleRadio.setText(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.alleRadio.text"));
        alleRadio.setSelected(true);
        gpsValgGroup.add(sisteRadio);
        sisteRadio.setText(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.sisteRadio.text"));
        
        sisteRadio.setEnabled(false);
        
        visStrekningerCheck.setText(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.visStrekningerCheck.text"));
        visStrekningerCheck.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                visStrekningerCheckActionPerformed(e);
            }                    
        });
        
        alleKjoretoyCheck.setText(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.alleKjoretoyCheck.text"));
        alleKjoretoyCheck.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                alleKjoretoyCheckActionPerformed(e);
            }
        });
        javax.swing.GroupLayout kjoretoyPanelLayout = new javax.swing.GroupLayout(kjoretoyPanel);
        kjoretoyPanel.setLayout(kjoretoyPanelLayout);
        kjoretoyPanelLayout.setHorizontalGroup(
            kjoretoyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kjoretoyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(kjoretoyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(alleKjoretoyCheck)
                    .addGroup(kjoretoyPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(kjoretoyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(visGpsCheck)
                            .addGroup(kjoretoyPanelLayout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(kjoretoyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(sisteRadio)
                                    .addComponent(alleRadio)))
                            .addComponent(visStrekningerCheck))))
                .addContainerGap(73, Short.MAX_VALUE))
        );
        kjoretoyPanelLayout.setVerticalGroup(
            kjoretoyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kjoretoyPanelLayout.createSequentialGroup()
                .addGroup(kjoretoyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(kjoretoyPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(alleKjoretoyCheck))
                    .addGroup(kjoretoyPanelLayout.createSequentialGroup()
                        .addComponent(visGpsCheck)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(alleRadio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sisteRadio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(visStrekningerCheck)))
                .addContainerGap())
        );

        prosesserPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Prosesser"));

        alleProsesserCheck.setText(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.alleProsesserCheck.text"));
        alleProsesserCheck.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                alleProsesserCheckActionPerformed(e);
            }            
        });
        alleProsesserCheck.setSelected(true);
        prosessTable.setEnabled(false);
         
        jScrollPane2.setViewportView(prosessTable);
                javax.swing.GroupLayout prosesserPanelLayout = new javax.swing.GroupLayout(prosesserPanel);
                prosesserPanel.setLayout(prosesserPanelLayout);
                prosesserPanelLayout.setHorizontalGroup(
                    prosesserPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(prosesserPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(prosesserPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
                            .addComponent(alleProsesserCheck))
                        .addContainerGap())
                );
                prosesserPanelLayout.setVerticalGroup(
                    prosesserPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(prosesserPanelLayout.createSequentialGroup()
                        .addComponent(alleProsesserCheck)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                        .addContainerGap())
                );

                brukButton.setText(ResourceUtil.getResource("brukButton.text"));
                brukButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        log.trace("bruk button clicked");
                        brukButtonActionPerformed(e);
                    }
                });
                brukButton.setPreferredSize(new java.awt.Dimension(65, 23));

                avbrytButton.setText(ResourceUtil.getResource("avbrytButton.text"));
                avbrytButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        avbrytButtonActionPerformed(e);
                    }
                });
                okButton.setText(ResourceUtil.getResource("okButton.text"));
                okButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        okButtonActionPerformed(e);
                    }
                });
                okButton.setPreferredSize(new java.awt.Dimension(65, 23));

                javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(buttonPanel);
                buttonPanel.setLayout(jPanel1Layout);
                jPanel1Layout.setHorizontalGroup(
                    jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(131, Short.MAX_VALUE)
                        .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(avbrytButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(brukButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                );
                jPanel1Layout.setVerticalGroup(
                    jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(brukButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(avbrytButton)
                            .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(16, Short.MAX_VALUE))
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
                this.setLayout(layout);
                layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(buttonPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(prosesserPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(kjoretoyPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                );
                layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(kjoretoyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(prosesserPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                );
            }// </editor-fold>
    
    
    
    private JXTable getDfuindividTable(){
    	JXTable table = new JXTable();
    	dfuindividTableModel = new MipssBeanTableModel<Dfuindivid>(Dfuindivid.class, "id", "serienummer");
    	dfuindividTableModel.setBeanInstance(module.getMipssMapBean());
    	dfuindividTableModel.setBeanMethod("getDfuindividForKontrakt");
    	dfuindividTableModel.setBeanInterface(MipssMap.class);
    	dfuindividTableModel.setBeanMethodArguments(new Class<?>[]{Long.class});
    	updateDfuindividTableModel();
    	table.setModel(dfuindividTableModel);
    	
    	table.getColumnModel().getColumn(0).setMaxWidth(100);
        return table;
    }
    
    public void updateDfuindividTableModel(){
    	dfuindividTableModel.setBeanMethodArgValues(new Object[]{module.getCurrentKontrakt().getId()});
    	dfuindividTableModel.loadData();
    }
    
    private JXTable getProsessTable(){
    	JXTable table = new JXTable();
    	prosessTableModel = new MipssBeanTableModel<Prodtype>(Prodtype.class, "navn", "fritekst");
    	prosessTableModel.setBeanInstance(module.getMipssMapBean());
    	prosessTableModel.setBeanMethod("getProdtypeList");
    	prosessTableModel.setBeanInterface(MipssMap.class);
    	prosessTableModel.loadData();
    	table.setModel(prosessTableModel);
    	
    	table.getColumnModel().getColumn(0).setMaxWidth(100);
        return table;
    }
    private void visGpsCheckActionPerformed(ActionEvent e) {
        if (visGpsCheck.isSelected()){
            alleRadio.setEnabled(true);
            sisteRadio.setEnabled(true);
            plugin.setProduksjonVisible(true, MipssMapLayerType.PRODUKSJONPUNKT);
        }
        if (!visGpsCheck.isSelected()){
            alleRadio.setEnabled(false);
            sisteRadio.setEnabled(false);
            plugin.setProduksjonVisible(false, MipssMapLayerType.PRODUKSJONPUNKT);
        }
    }
    
    private void visStrekningerCheckActionPerformed(ActionEvent e) {
        if (visStrekningerCheck.isSelected()){
            plugin.setProduksjonVisible(true, MipssMapLayerType.PRODUKSJONSTREKNING);
        }
        if (!visStrekningerCheck.isSelected()){
            plugin.setProduksjonVisible(false, MipssMapLayerType.PRODUKSJONSTREKNING);
        }
    }
    
    private void alleKjoretoyCheckActionPerformed(ActionEvent e) {
        if (!alleKjoretoyCheck.isSelected())
        	kjoretoyTable.setEnabled(true);
        if (alleKjoretoyCheck.isSelected())
        	kjoretoyTable.setEnabled(false);
    }
    
    private void alleProsesserCheckActionPerformed(ActionEvent e) {
        if (alleProsesserCheck.isSelected()){
            prosessTable.setEnabled(false);
        }
        if (!alleProsesserCheck.isSelected()){
            prosessTable.setEnabled(true);
        }
    }
    
    private void brukButtonActionPerformed(ActionEvent e){
        log.trace("actionPerformed bruk button");
        if (kjoretoyTable.getSelectedRows().length>0)
        	hentProduksjon();
        else
        	JOptionPane.showMessageDialog(null, ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.brukButtonAction.noselection.description"), ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.brukButtonAction.noselection.title"), JOptionPane.INFORMATION_MESSAGE);
    }
    
    private void okButtonActionPerformed(ActionEvent e){
    	if (kjoretoyTable.getSelectedRows().length==0){
    		JOptionPane.showMessageDialog(null, ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.brukButtonAction.noselection.description"), ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPanel.brukButtonAction.noselection.title"), JOptionPane.INFORMATION_MESSAGE);
    		return;
    	}
    	log.trace("ok button clicked");
        Container parent = this;
        while (!(parent instanceof Window)){
            parent = parent.getParent();
        }
        log.trace("hiding window :"+parent);
        ((JDialog)parent).setVisible(false);
        hentProduksjon();
        
    }
    
    private void avbrytButtonActionPerformed(ActionEvent e){
        Container parent = this;
        while (!(parent instanceof JDialog)){
            parent = parent.getParent();
        }
        ((JDialog)parent).setVisible(false);
    }
    
    /**
     * Ber kontrolleren om å hente produksjon for valgte biler
     * innen gitt periode.
     */
    private void hentProduksjon(){
        log.trace("henter produksjon");
        
        List<Dfuindivid> bilListe = new ArrayList<Dfuindivid>();
        if (alleKjoretoyCheck.isSelected()){
            bilListe = dfuindividTableModel.getEntities();
        }	
        else{
            int[] indices = kjoretoyTable.getSelectedRows();
            for (int i:indices){
            	Dfuindivid dfuindivid = (dfuindividTableModel.getEntities().get(i));//.getInstallasjonList().get(0).getDfuindivid();
                bilListe.add(dfuindivid);
            }
        }
        List<Prodtype> prodtyper = null;
        if (!alleProsesserCheck.isSelected()){
        	prodtyper = new ArrayList<Prodtype>();
        	int[] selectedRows = prosessTable.getSelectedRows();
        	for (int i:selectedRows){
        		prodtyper.add(prosessTableModel.get(i));
        	}
        }
    }    
}
