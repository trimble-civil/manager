package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssDatePicker;
/**
 * @deprecated
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class OptionsPanel extends JPanel{
	private JMipssDatePicker fraPicker;
    private JMipssDatePicker tilPicker;
	
    public OptionsPanel(){
    	setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    	setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.OptionsPanel.border.title")));
    	initGui();
    }
    
	private void initGui(){
		JLabel fraDatoLabel = new JLabel(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.OptionsPanel.fraDatoLabel.text"));
        JLabel tilDatoLabel = new JLabel(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.OptionsPanel.tilDatoLabel.text"));
        setSize(fraDatoLabel, new Dimension(50, 25));
        setSize(tilDatoLabel, new Dimension(50, 25));
        
		Calendar c = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        tilPicker = new JMipssDatePicker(c.getTime());
        tilPicker.setFormats(format);
        fraPicker = new JMipssDatePicker(c.getTime());
        fraPicker.setFormats(format);
        JButton velgVeiButton = new JButton(new VelgVeiAction(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.OptionsPanel.velgveiButton.text"), IconResources.VEI_ICON));
        setSize(velgVeiButton, new Dimension(140, 25));
        
        JPanel fraPanel = getHorizPanel();
        fraPanel.add(fraDatoLabel);
        fraPanel.add(Box.createHorizontalGlue());
        fraPanel.add(fraPicker);
        setSize(fraPanel, new Dimension(150, 25));
        
        JPanel tilPanel = getHorizPanel();
        tilPanel.add(tilDatoLabel);
        tilPanel.add(Box.createHorizontalGlue());
        tilPanel.add(tilPicker);
        setSize(tilPanel, new Dimension(150, 25));
        
        JPanel buttonPanel =getHorizPanel();
        buttonPanel.add(velgVeiButton);
        buttonPanel.add(Box.createHorizontalGlue());
        
        add(Box.createVerticalStrut(5));
        add(fraPanel);
        add(Box.createHorizontalGlue());
        add(Box.createVerticalStrut(5));
        add(tilPanel);
        add(Box.createVerticalStrut(5));
        add(buttonPanel);
        add(Box.createVerticalStrut(5));
	}
	
	private void setSize(JComponent c, Dimension size){
		c.setPreferredSize(size);
		c.setMaximumSize(size);
		c.setMinimumSize(size);
	}
	
	private JPanel getHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setBorder(BorderFactory.createEmptyBorder(0,10,0,0));
		return panel;
	}
	
	private class VelgVeiAction extends AbstractAction{
		public VelgVeiAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			
		}
	}
}
