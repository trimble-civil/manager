package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.LabelLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.task.HentProduksjonDFUTask;
import no.mesta.mipss.mipssmap.task.HentProduksjonTask;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.KonfigparamCache;
import no.mesta.mipss.mipssmap.utils.ProdpunktLabelCreator;
import no.mesta.mipss.mipssmap.utils.ProduksjonOptions;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.mipssmapserver.KjoretoyVO;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasse som henter produksjonspunkter for en dfu eller et kjøretøy
 * 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class CreateProduksjonPunkter {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private MipssMapModule plugin;
	private JCheckButton button;
	private ProduksjonOptions options;
	private final ProduksjonCriteria criteria;
	public CreateProduksjonPunkter(MipssMapModule plugin, JCheckButton button, ProduksjonOptions options, ProduksjonCriteria criteria){
		this.plugin = plugin;
		this.button = button;
		this.options = options;
		this.criteria = criteria;
	}
	
	
	public void doIt(){
		Date datoFra = options.getDatoFra().getDate();
    	Date datoTil = options.getDatoTil().getDate();
    	for (KjoretoyVO kjoretoy:criteria.getEnhetList()){
    		Long kjoretoyId=null;
    		Long pdaId=null;
    		String guiNavn="";
    		boolean mestaBil = false;
    		if (kjoretoy.getKjoretoy()!=null){
    			kjoretoyId = kjoretoy.getKjoretoy().getId();
    			guiNavn = kjoretoy.getKjoretoy().getGuiTekst();
    			if (guiNavn==null)
    				guiNavn = kjoretoy.getKjoretoy().getNavn();
    			if (kjoretoy.getKjoretoy().getLeverandorNavn()!=null)
    				mestaBil = kjoretoy.getKjoretoy().getLeverandorNavn().toLowerCase().indexOf("mesta")!=-1;
    		}
    		if (kjoretoy.getPda()!=null)
    			pdaId = kjoretoy.getPda().getId();
    		
    		ProduksjonQueryParams params = new ProduksjonQueryParams();
    		params.setKjoretoyId(kjoretoyId);
    		params.setDfuId(pdaId);
    		params.setFraDato(datoFra);
    		params.setTilDato(datoTil);
    		params.setProdtyper(criteria.getProdtyper());
    		params.setAllProduksjon(criteria.isAllProduksjon());
    		params.setKunSiste(criteria.isKunSiste());
    		log.debug("Henter produksjon for kjoretoy:"+kjoretoyId+" eller pda:"+pdaId);
    		final HentProduksjonTask task = new HentProduksjonTask(plugin, params, guiNavn, mestaBil, criteria);
	        
	        ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.HentProduksjonTask.title")+((kjoretoyId==null)?pdaId:kjoretoyId));
	        workerPanel.setWorker(task);
	        task.setProgressController(plugin.getProgressController());
	        task.setProgressWorkerPanel(workerPanel);
	        plugin.getProgressController().addWorkerPanel(workerPanel);
	        final ProdpunktLabelCreator labelCreator = new ProdpunktLabelCreator();
	        Thread wait = new Thread("Hent produksjon Thread(kjoretoy="+kjoretoyId+" pda="+pdaId+")"){ 
	            public void run(){
	                Layer layer=null;
	                try {
	                    layer = task.get();
	                    
	                    if (layer!=null){
		                    	plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONPUNKT.getType(), layer, true);
		                        button.setChecked(true);
		                        MipssGeoPosition lastPosition = task.getLastPosition();
		                        
	                        	String labelId = "label:"+task.getSistePunkt().getDfuId();
	                        	boolean mestaBil = task.isMestaBil();
	                        	LabelLayer label = null;
	                        	if (mestaBil){
	                        		label = labelCreator.getMestabilLabel(task.getLabelText(), lastPosition, labelId);
	                        	}else if (task.isPda()){
	                        		label = labelCreator.getLabelPDA(task.getLabelText(), lastPosition, labelId);
	                        	} else{
	                        		label = labelCreator.getLabelUE(task.getLabelText(), lastPosition, labelId);
	                        	}
	                        	
	                        	label.setPreferredLayerType(MipssMapLayerType.PRODUKSJONLABEL.getType());
		                        plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), label, true);
		                        if (!criteria.isLabels()){
		                        	label.setVisible(false);
		                        }
	                    }
	                } catch (ExecutionException e) {
	                    e.printStackTrace();
	                } catch (InterruptedException e) {
	                    e.printStackTrace();
	                }
	            }

				
	        };
	        wait.start();
    	}
    	KonfigparamCache.clearCache();
	}
	
	public void doItDfu(){
		Date datoFra = options.getDatoFra().getDate();
    	Date datoTil = options.getDatoTil().getDate();
    		
		ProduksjonQueryParams params = new ProduksjonQueryParams();
		params.setDfuId(criteria.getDfuId());
		params.setFraDato(datoFra);
		params.setTilDato(datoTil);
		params.setProdtyper(criteria.getProdtyper());
		params.setAllProduksjon(criteria.isAllProduksjon());
		params.setKunSiste(criteria.isKunSiste());
		log.debug("Henter produksjon for dfu:"+criteria.getDfuId());
		
		final HentProduksjonDFUTask task = new HentProduksjonDFUTask(plugin, params, "DFU:"+criteria.getDfuId(), criteria);
        
        ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.HentProduksjonTask.title")+criteria.getDfuId());
        workerPanel.setWorker(task);
        task.setProgressController(plugin.getProgressController());
        task.setProgressWorkerPanel(workerPanel);
        plugin.getProgressController().addWorkerPanel(workerPanel);
        final ProdpunktLabelCreator labelCreator = new ProdpunktLabelCreator();
        Thread wait = new Thread("Hent produksjon Thread(dfu="+criteria.getDfuId()+")"){ 
            public void run(){
                Layer layer=null;
                try {
                    layer = task.get();
                    
                    if (layer!=null){
	                    	plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONPUNKT.getType(), layer, true);
	                        button.setChecked(true);
	                        MipssGeoPosition lastPosition = task.getLastPosition();
	                        
                        	String labelId = "label:"+task.getSistePunkt().getDfuId();
                        	LabelLayer label = labelCreator.getLabelDFU(task.getLabelText(), lastPosition, labelId);
                        	
                        	label.setPreferredLayerType(MipssMapLayerType.PRODUKSJONLABEL.getType());
	                        plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), label, true);
	                        if (!criteria.isLabels()){
	                        	label.setVisible(false);
	                        }
	                        plugin.getMainMap().ensureVisibilityForBounds(layer.getBounds());
                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        wait.start();
    	KonfigparamCache.clearCache();
	}
	
}
