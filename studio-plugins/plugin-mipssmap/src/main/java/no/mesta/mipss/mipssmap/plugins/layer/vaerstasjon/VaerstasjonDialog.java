package no.mesta.mipss.mipssmap.plugins.layer.vaerstasjon;

import java.awt.BorderLayout;
import java.awt.Window;

import javax.swing.JDialog;

import no.mesta.mipss.mipssmap.MipssMapModule;

/**
 * Dialog for å vise frem værstasjoner
 * 
 * @author @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class VaerstasjonDialog extends JDialog{
//	private JTabbedPane tabPane ;

	
	public VaerstasjonDialog(Window owner, VaerstasjonPlugin plugin, MipssMapModule module){
		super(owner);
		initGui(plugin, module);
	}
	
	private void initGui(VaerstasjonPlugin plugin, MipssMapModule module){
		setLayout(new BorderLayout());
		VaerstasjonPanel vaerstasjonPanel = new VaerstasjonPanel(plugin, module);
//		tabPane = new JTabbedPane();
//        tabPane.addTab(ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.VaerstasjonDialog.tab.vaerstasjon"), vaerstasjonPanel);
        add(vaerstasjonPanel);
	}
}
