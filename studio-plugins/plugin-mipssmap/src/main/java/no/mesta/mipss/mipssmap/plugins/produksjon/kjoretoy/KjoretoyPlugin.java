package no.mesta.mipss.mipssmap.plugins.produksjon.kjoretoy;

import java.awt.event.ActionEvent;

import no.mesta.mipss.mipssmap.plugins.ProductionPlugin;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.resources.images.IconResources;

/**
 * MipssMapProductionPlugin for å vise frem kjøretøy
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class KjoretoyPlugin extends ProductionPlugin{

    public KjoretoyPlugin() {
    	super(null, null, ResourceUtil.getResource("mipssmap.plugins.produksjon.kjoretoy.KjoretoyPlugin.button.text"), "", "");
    	getCheckButton().setEnabled(false);
    	getCheckButton().setAction(null);
    	getCheckButton().setIcon(ImageUtil.toGrayScale(IconResources.REFRESH_VIEW_ICON));
    }
    
    @Override
    public void setEnabled(boolean enabled){
		getCheckButton().setEnabled(false);
	}
	@Override
	protected void buttonActionPerformed(ActionEvent e) {
		
	}

	@Override
	protected void checkActionPerformed(ActionEvent e) {
		
	}

	@Override
	public void refresh() {
		
	}
}
