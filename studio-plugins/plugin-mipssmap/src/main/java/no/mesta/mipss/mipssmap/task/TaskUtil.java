package no.mesta.mipss.mipssmap.task;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.PeriodLineListLayer;
import no.mesta.mipss.mapplugin.layer.ProdpunktGeoPosition;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.util.textboxdata.ProdpunktTextboxProducer;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProdtypeVO;
import no.mesta.mipss.webservice.ReflinkStrekning;
import no.mesta.mipss.webservice.StrekningVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Denne factoryen konverterer fra webservice sine klasser og til Layer klasser, dette er en litt dirty løsning, 
 * det vil komme en bedre når(hvis) jeg har tid
 * TODO fix.
 * 
 * @author Harald A. Kulø
 */
public class TaskUtil {
    private static Logger log = LoggerFactory.getLogger(TaskUtil.class);
    
    /**
     * private constructor. 
     */
    private TaskUtil(){
    }
    
    
//  /**
//  * Konverterer en liste med punkter til en liste med geoPosisjoner.
//  * @param vertices
//  * @param b
//  * @return
//  * TODO denne må oppdateres når webservicen får nytt grensesnitt for geometry
//  */
// public List<MipssGeoPosition[]> convertReflinkStrekningListToGeoPositionList(List<ReflinkStrekning> vertices, Bounds b){
//     List<MipssGeoPosition[]> geoVertices = new ArrayList<MipssGeoPosition[]>();
//     
//     for (ReflinkStrekning point:vertices){
//         //setter boundingboxen til produksjonen.
//     	b.addBounds(point.getVectors());
//         
//     	MipssGeoPosition[] gp = new MipssGeoPosition[point.getVectors().length];
//     	for (int i=0;i<point.getVectors().length;i++){
//     		Point p = point.getVectors()[i];
//     		gp[i] = new MipssGeoPosition(p.getY(), p.getX());
//     	}
//     }
//     return geoVertices;
// }
    
    
    
    
    
    
    
}
