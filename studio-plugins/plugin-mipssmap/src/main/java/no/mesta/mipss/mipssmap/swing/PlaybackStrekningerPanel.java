/*
 * PlaybackPanel.java
 *
 * Created on 16. juni 2008, 10:39
 */

package no.mesta.mipss.mipssmap.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayerCollection;
import no.mesta.mipss.mapplugin.layer.PeriodLineListLayer;
import no.mesta.mipss.mapplugin.layer.PointLayer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.util.LayerEvent;
import no.mesta.mipss.mapplugin.util.LayerListener;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.animation.AnimationDates;
import no.mesta.mipss.mipssmap.animation.AnimationThread;
import no.mesta.mipss.mipssmap.animation.SliderThread;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXPanel;


/**
 * Panel for å spille av alle kjøreturer som er hentet. 
 * 
 * @author  harkul
 */
public class PlaybackStrekningerPanel extends JXPanel implements LayerListener {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private JSlider playSlider;
//    private JButton backButton;
    private JPanel buttonPanel;
//    private JButton fwdButton;
    private JLabel hastighetLabel;
    private JComboBox speedCombo;
    private JButton lukkButton;
    private JButton pauseButton;
    private JButton playButton;
    private JButton stopButton;
    
    private JPanel playControlPanel;
    private JPanel contentPanel;
    
    private DefaultComboBoxModel speedModel;
    
    private MapPanel mapPanel;
    
    private List<AnimationThread> playThreadList;
    
    private SliderThread sliderRunner;
    
    private JXHeader header;
    private int speed = 10;
    
    /** Creates new form PlaybackPanel */
    public PlaybackStrekningerPanel(MipssMapPanel mapPanel) {
        this.mapPanel = mapPanel;
        mapPanel.getLayerHandler().addLayerListener(this);
        setLayout(new BorderLayout());
        playControlPanel = new JPanel();
        header = getHeader();
        add(header, BorderLayout.NORTH);
        initGui();
        initContentComponents();
        initPlayer();
    }
    
    private JXHeader getHeader(){
        JXHeader header = new JXHeader();
        header.setTitle(ResourceUtil.getResource("mipssmap.swing.PlaybackStrekningerPanel.header.title"));
        header.setDescription(ResourceUtil.getResource("mipssmap.swing.PlaybackStrekningerPanel.header.description"));
        
        Icon icon = new javax.swing.ImageIcon(getClass().getResource("/images/icon_kamera.png"));
        header.setIcon(icon);
        return header;
    }

    private void initContentComponents(){
        contentPanel = new JPanel(new BorderLayout());
        contentPanel.add(playControlPanel, BorderLayout.NORTH);
        
        FlowLayout flow = new FlowLayout();
        flow.setAlignment(FlowLayout.RIGHT);
        JPanel buttonPanel = new JPanel(flow );
        
        buttonPanel.add(lukkButton);
        contentPanel.add(buttonPanel, BorderLayout.SOUTH);
        
        add(contentPanel, BorderLayout.CENTER);
    }
    
    private void initGui(){
    	playControlPanel.setLayout(new BoxLayout(playControlPanel, BoxLayout.PAGE_AXIS));
    	
        playButton = new JButton();
//        backButton = new JButton();
        pauseButton = new JButton();
        stopButton = new JButton();
//        fwdButton = new JButton();
        hastighetLabel = new JLabel();
        playSlider = new JSlider();
        lukkButton = new JButton();
        speedCombo = new JComboBox();

        lukkButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                lukkButtonActionPerformed(e);
            }
        });
        playButton.setIcon(new ImageIcon(getClass().getResource("/images/player_play.png")));
        playButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });

//        backButton.setIcon(new ImageIcon(getClass().getResource("/images/player_back.png")));
//        backButton.addActionListener(new ActionListener(){
//            public void actionPerformed(ActionEvent e){
//                backButtonActionPerformed(e);
//            }
//        });
        pauseButton.setIcon(new ImageIcon(getClass().getResource("/images/player_pause.png")));
        pauseButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                pauseButtonActionPerformed(e);
            }
        });
        stopButton.setIcon(new ImageIcon(getClass().getResource("/images/player_stop.png")));
        stopButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                stopButtonActionPerformed(e);
            }
        });
        
//        fwdButton.setIcon(new ImageIcon(getClass().getResource("/images/player_fwd.png")));
//        fwdButton.addActionListener(new ActionListener(){
//            public void actionPerformed(ActionEvent e){
//                fwdButtonActionPerformed(e);
//            }
//        });
        hastighetLabel.setText(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.hastighetLabel.text"));

        playSlider.setMajorTickSpacing(10);
        playSlider.setMinorTickSpacing(5);
        playSlider.setPaintTicks(true);
        playSlider.setValue(0);
        playSlider.addChangeListener(new ChangeListener(){         
            public void stateChanged(ChangeEvent e) {
                playSliderStateChanged(e);
            }
        });
        
        lukkButton.setText(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.lukkButton.text"));
        speedModel = new DefaultComboBoxModel(new String[]{ "1x","5x","10x","20x","30x","40x", "50x", "100x", "200x", "400x" });
        speedCombo.setModel(speedModel);
        speedCombo.setSelectedIndex(2);
        
        Dimension buttonSize = new Dimension(40,40);
        setComponentSize(playButton, buttonSize);
        setComponentSize(pauseButton, buttonSize);
        setComponentSize(stopButton, buttonSize);
        setComponentSize(speedCombo, new Dimension(75, 24));
        Dimension space = new Dimension(5,0);
        Dimension spaceL = new Dimension(20,0);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(Box.createRigidArea(space));
        buttonPanel.add(playButton);
        buttonPanel.add(Box.createRigidArea(space));
        buttonPanel.add(pauseButton);
        buttonPanel.add(Box.createRigidArea(space));
        buttonPanel.add(stopButton);
        buttonPanel.add(Box.createRigidArea(spaceL));
        buttonPanel.add(hastighetLabel);
        buttonPanel.add(Box.createRigidArea(space));
        buttonPanel.add(speedCombo);
        buttonPanel.add(Box.createHorizontalGlue());
        
        
        JPanel sliderPanel = new JPanel();
        sliderPanel.setLayout(new BoxLayout(sliderPanel, BoxLayout.LINE_AXIS));
        sliderPanel.add(Box.createRigidArea(space));
        sliderPanel.add(playSlider);
        sliderPanel.add(Box.createRigidArea(space));
        
        playControlPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        playControlPanel.add(buttonPanel);
        playControlPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        playControlPanel.add(sliderPanel);
        playControlPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        
    }
    private void setComponentSize(JComponent c, Dimension d){
    	c.setPreferredSize(d);
    	c.setMinimumSize(d);
    	c.setMaximumSize(d);
    }
    
    /**
     * Begynner avspilling av kjøretur, hvis pause knappen er trykket vil denne metoden starte
     * avspilling igjen
     * @param evt
     */
    private void playButtonActionPerformed(java.awt.event.ActionEvent evt) {                                           
        if (playThreadList!=null){
            for (AnimationThread t:playThreadList){
                if (!t.isAlive())
                    t.start();
            }
            if (sliderRunner!=null&&sliderRunner.pleaseWait){
                pauseButtonActionPerformed(evt);
            }
        }
        
        if (sliderRunner!=null&&!sliderRunner.isAlive())
            sliderRunner.start();
    }
    /**
     * Setter avspilling på pause, ved 2. klikk startes avspillin igjen.
     * @param e
     */
    private void pauseButtonActionPerformed(ActionEvent e){
        if (playThreadList!=null){            
            for (AnimationThread t:playThreadList){
                if (t.isPleaseWait()){
                    t.setPleaseWait(false);
                    synchronized(t){
                        t.notify();
                    }
                }else{
                    t.setPleaseWait(true);
                }
            }
        }
        if (sliderRunner!=null){
            if (sliderRunner.pleaseWait){
                sliderRunner.pleaseWait = false;
                synchronized(sliderRunner){
                    sliderRunner.notify();
                }
            }else
                sliderRunner.pleaseWait = true;
        }
    }
    /**
     * Stopper avspillingstråden og initialiserer den på nytt
     * @param e
     */
    private void backButtonActionPerformed(ActionEvent e){
        if (playThreadList!=null){
            for (AnimationThread t:playThreadList){
                t.setPunktIndex(0);
            }
        }
    }
    /**
     * Stopper avspilling
     * @param e
     */
    private void stopButtonActionPerformed(ActionEvent e) {
        if (playThreadList!=null){
            for (AnimationThread t:playThreadList){
                t.stopRunning();
            }
        }
        if (sliderRunner!=null)
            sliderRunner.stopRunning();
        sliderRunner = null;
        initPlayer();
        playSlider.setValue(0);
    }
    /**
     * Setter til avspilling forover
     * @param e
     */
    private void fwdButtonActionPerformed(ActionEvent e) {
        int selected =speedCombo.getSelectedIndex()+1;
        if (selected<speedModel.getSize())
            speedModel.setSelectedItem(speedModel.getElementAt(selected));
    }
    /**
     * 
     * @param e
     */
    private void lukkButtonActionPerformed(ActionEvent e) {
        Component parent = this;
        while (!(parent instanceof Window)){
            parent = parent.getParent();
        }
        ((Window)parent).dispose();
    }
    
    private void playSliderStateChanged(ChangeEvent e) {
        int value = playSlider.getValue();
        if (sliderRunner!=null){
            sliderRunner.setCurrentTime(value);
        }
    }
    
    /**
     * LayerEvent 
     * @param e
     */
    public void layerChanged(LayerEvent e) {
    	if(sliderRunner==null||!sliderRunner.isAlive()&&playThreadList==null){
	        if (e.getSource()!=null&&e.getType().equals(LayerEvent.TYPE_SELECTED)){
	            if (mapPanel.getLayerHandler().getCurrentSelected() instanceof PointLayer){
	                initPlayer();
	            }
	        }
    	}
    }
    /**
     * Kalles når man ønsker å lukke panelet, avslutter avspillingstråden og fjerner denne klassen som
     * lytter til layerhandleren
     */
    public void close(){
        if (playThreadList!=null){
            for (AnimationThread t:playThreadList){
                t.stopRunning();
                for (CollectionEntity l:t.getPunktData().getLayers()){
                    l.setVisible(true);
                }
                t.restoreStrekninger();
            }
            playThreadList=null;
        }
        if (sliderRunner!=null){
            sliderRunner.stopRunning();
            sliderRunner = null;
        }
        mapPanel.getLayerHandler().removeLayerListener(this);
        mapPanel.getLayerHandler().unselectAll();
    }
    
    /**
     * Initialiserer avspilleren
     */
    private boolean initPlayer(){
        List<Layer> pointLayers = mapPanel.getLayerHandler().getLayers(MipssMapLayerType.PRODUKSJONPUNKT.getType());
        List<Layer> strekningLayers = mapPanel.getLayerHandler().getLayers(MipssMapLayerType.PRODUKSJONSTREKNING.getType());
        if (pointLayers==null)
        	pointLayers=new ArrayList<Layer>();
        if (strekningLayers==null)
        	strekningLayers=new ArrayList<Layer>();
        List<Layer> inspPoint = mapPanel.getLayerHandler().getLayers(MipssMapLayerType.INSPEKSJONPUNKT.getType());
        List<Layer> inspStr = mapPanel.getLayerHandler().getLayers(MipssMapLayerType.INSPEKSJONSTREKNING.getType());
        
        if ((pointLayers==null || pointLayers.isEmpty() || strekningLayers==null || strekningLayers.isEmpty())&&
        	(inspPoint==null || inspPoint.isEmpty() || inspStr==null || inspStr.isEmpty())){
        	JOptionPane.showMessageDialog(null, ResourceUtil.getResource("mipssmap.swing.PlaybackStrekningerPanel.feilmelding"), "Mangler data", JOptionPane.INFORMATION_MESSAGE);
        	return false;
        }
        
        Date startDate = null;
        Date stopDate = null;
        
        if (inspPoint!=null&&inspStr!=null){
        	for (Layer l:inspPoint){
        		if (l instanceof PointLayerCollection){
        			List<? extends CollectionEntity> pl = ((PointLayerCollection)l).getLayers();
        			if (!pl.isEmpty()){
	        			CollectionEntity first = pl.get(0);
	        			if (first instanceof ProdpunktPointLayer){
	        				ProdpunktPointLayer  point = ((ProdpunktPointLayer)first);
	                    	String guid = point.getProdpunktGeoPosition().getProdpunktVO().getGuid();
	                    	Layer strekningLayer = finnStrekningerForInspeksjon(inspStr, guid);
	                    	log.debug("fant "+strekningLayer);
	                    	//vi har animerbar strekning
	                        if (strekningLayer!=null){
	                            if (playThreadList==null){
	                                playThreadList = new ArrayList<AnimationThread>();
	                            }
	                            AnimationThread player = new AnimationThread((PointLayerCollection)l, null, (LineListLayerCollection)strekningLayer, mapPanel, speed, point.getId());
	                            
	                            if (startDate==null)
	                                startDate = player.getFirst();
	                            if (player.getFirst().before(startDate))
	                                startDate = player.getFirst();
	                            if (stopDate == null)
	                                stopDate = player.getLast();
	                            if (player.getLast().after(stopDate))
	                                stopDate = player.getLast();
	//                            log.debug(player.getFirst().getTime()+" "+player.getFirst());
	                            playThreadList.add(player);
	                        }
	        			}
        			}
        		}
        	}
        }
        
        
        if (pointLayers!=null&&strekningLayers!=null){
            //finn alle layers som skal animeres
            for (Layer l:pointLayers){
                if (l instanceof PointLayerCollection){
                    List<? extends CollectionEntity> pl = ((PointLayerCollection)l).getLayers();
                    CollectionEntity first = pl.get(0);
                    CollectionEntity last = pl.get(pl.size()-1);
                    
                    if (first instanceof ProdpunktPointLayer){
                    	ProdpunktPointLayer  point = ((ProdpunktPointLayer)first);
                    	Long dfuId = point.getProdpunktGeoPosition().getProdpunktVO().getDfuId();
                    	
                        Layer strekningLayer = finnStrekningerForEnhet(strekningLayers, ((ProdpunktPointLayer)first).getProdpunktGeoPosition().getProdpunktVO().getDfuId());

                        //vi har animerbar strekning
                        if (strekningLayer!=null){
                            if (playThreadList==null){
                                playThreadList = new ArrayList<AnimationThread>();
                            }
                            AnimationThread player = new AnimationThread((PointLayerCollection)l, null, (LineListLayerCollection)strekningLayer, mapPanel, speed, point.getId());
                            
                            if (startDate==null)
                                startDate = player.getFirst();
                            if (player.getFirst().before(startDate))
                                startDate = player.getFirst();
                            if (stopDate == null)
                                stopDate = player.getLast();
                            if (player.getLast().after(stopDate))
                                stopDate = player.getLast();
//                            log.debug(player.getFirst().getTime()+" "+player.getFirst());
                            playThreadList.add(player);
                        }
                    }
                }
            }
        }
        
        AnimationDates ss = new AnimationDates(startDate, stopDate);
//        log.debug(ss+" "+ss.getLastDate()+" "+ss.getFirstDate()+" "+playSlider);
        playSlider.setMinimum(0);
        playSlider.setMaximum((int)(ss.getLastDate().getTime()-ss.getFirstDate().getTime())/1000);
        playSlider.setValue(0);
        
        if (playThreadList!=null){
            for (AnimationThread t:playThreadList){
                long wait = t.getFirst().getTime()-startDate.getTime();
                t.setWait((int)wait/speed);
                t.setStrekningSynchronizer(ss);
            }
        }
        sliderRunner = new SliderThread(playSlider, ss, speedModel, mapPanel.getJFreeChartTool());
        return true;
    }
    
    private Layer finnStrekningerForInspeksjon(List<Layer> strekningLayers, String guid){
    	for (Layer strekningLayer : strekningLayers){
    		log.debug("Layer: {}", strekningLayer.getClass());
            if (strekningLayer instanceof LineListLayerCollection){
            	if (!((LineListLayerCollection)strekningLayer).getLayers().isEmpty()){
	                CollectionEntity ll = ((LineListLayerCollection)strekningLayer).getLayers().get(0);
	                if (ll instanceof PeriodLineListLayer){
	                    PeriodLineListLayer pll = (PeriodLineListLayer)ll;
	                    if (pll.getGuid().equals(guid)){
	                    	return strekningLayer;
	                    }
	                }
            	}
            }
        }
    	
    	return null;
    }
    
    private Layer finnStrekningerForEnhet(List<Layer> strekningLayers, Long dfuId){
//    	log.debug("looking for:"+dfuId+"@"+prodpunkt.getCetTidspunkt());
        for (Layer strekningLayer : strekningLayers){
            if (strekningLayer instanceof LineListLayerCollection){
                CollectionEntity ll = ((LineListLayerCollection)strekningLayer).getLayers().get(0);
                if (ll instanceof PeriodLineListLayer){
                    PeriodLineListLayer pll = (PeriodLineListLayer)ll;
//                    log.debug(pll.getDfuId()+" **********  "+dfuId.toString());
                    if (pll.getDfuId().intValue()==dfuId.intValue()){
                    	Date start = pll.getStart();
                    	Date stopp = pll.getStopp();
                    	log.debug("fant:"+dfuId+" "+start+" "+stopp);
//                    	if ( (prodpunkt.getCetTidspunkt().after(start)||prodpunkt.getCetTidspunkt().equals(start))&&prodpunkt.getCetTidspunkt().before(stopp))
                    	return strekningLayer;
                    }
                }
            }
        }
        return null;
    }
}
            
