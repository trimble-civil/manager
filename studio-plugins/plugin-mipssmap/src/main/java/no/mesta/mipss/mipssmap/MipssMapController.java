package no.mesta.mipss.mipssmap;

import java.util.List;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.contract.Contract;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.swing.JStatusPanel;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;


/**
 * @aboutToDie
 */
public class MipssMapController {
    private Contract contractBean;
    private MaintenanceContract maintenanceContractBean;
    
//    private MipssMapLayerController mapLayerController;
    private MapPanel mainMap;
    private JStatusPanel statusPanel;
    
    private Driftkontrakt currentKontrakt;
    private List<Rode> rodeCache;
    
    public MipssMapController(MapPanel mainMap, JStatusPanel statusPanel) {
//        this.mapLayerController = mapLayerController;
        this.mainMap = mainMap;
        this.statusPanel = statusPanel;
    }
    
    public void setContractBean(Contract contractBean){
        this.contractBean = contractBean;
    }
    
    public void setMaintanenceContract(MaintenanceContract maintenanceContractBean){
        this.maintenanceContractBean = maintenanceContractBean;
    }
    
    
    public Contract getContractBean() {
        return contractBean;
    }
    public MaintenanceContract getMainenanceContractBean(){
        return maintenanceContractBean;
    }

//    public MipssMapLayerController getMapLayerController() {
//        return mapLayerController;
//    }


    public MapPanel getMainMap() {
        return mainMap;
    }

    public JStatusPanel getStatusPanel() {
        return statusPanel;
    }

    public Driftkontrakt getCurrentKontrakt() {
        return currentKontrakt;
    }
    
//    public List<Rode> getRoderForKontrakt(long rodetypeid, boolean localCache){
//        if (localCache && rodeCache!=null){
//            return rodeCache;
//        }else{
//            clearRodeCache();
//            List<Rode> rodelist =  getContractBean().getRode(currentKontrakt.getId(), rodetypeid);    
//            rodeCache = rodelist;
//            return rodeCache;
//        }
//    }
    
//    public void clearRodeCache(){
//        rodeCache=null;
//        getMapLayerController().removeLayerType(MipssMapLayerType.RODE.getType());
//    }
//    
//    public void resetMapPanel(){
//        clearRodeCache();
//        getMapLayerController().removeAllLayers();
//    }
    
   
   

}
