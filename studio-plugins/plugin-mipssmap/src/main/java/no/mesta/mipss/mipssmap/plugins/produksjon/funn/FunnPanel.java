package no.mesta.mipss.mipssmap.plugins.produksjon.funn;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.resources.images.IconResources;

public class FunnPanel extends JPanel{
	private FunnPlugin plugin;
	private MipssMapModule module;
	private VelgFunnPanel funnPanel;
	private VelgRskjemaPanel hendelsePanel;
	
	public FunnPanel(MipssMapModule module, FunnPlugin plugin){
		setLayout(new BorderLayout());
		this.plugin = plugin;
		this.module = module;
		JTabbedPane tab = new JTabbedPane();
		funnPanel = new VelgFunnPanel(module, plugin, ImageUtil.padIcon(5, 5, 2, 0, IconResources.MAP_PIN_LEFT_SM), tab, 0);
		hendelsePanel = new VelgRskjemaPanel(module, plugin, ImageUtil.padIcon(5,5,2,0, IconResources.MAP_PIN_RIGHTY_SM), tab, 1);
		tab.addTab("Avvik", ImageUtil.toGrayScale(funnPanel.getOrigIcon()), funnPanel);
		tab.addTab("R-skjema", ImageUtil.toGrayScale(hendelsePanel.getOrigIcon()), hendelsePanel);
		add(tab, BorderLayout.CENTER);
		add(createButtonPanel(), BorderLayout.SOUTH);
		funnPanel.checkSelected();
		hendelsePanel.checkR2Selected(true);
		hendelsePanel.checkR5Selected(true);
		hendelsePanel.checkR11Selected(true);
	}
	
	/**
	 * Henter ut standard knapperad
	 * @param okAction action for hva som skjer ved klikk på ok
	 * @param cancelAction action for hva som skjer ved klikk på avbryt
	 * @param applyAction action for hva som skjer ved klikk på bruk
	 * @return
	 */
	private JPanel createButtonPanel(){
		OkAction okAction = new OkAction("", null);
		CancelAction cancelAction = new CancelAction("", null);
		ApplyAction applyAction = new ApplyAction("", null);
		okAction.putValue(Action.NAME, ResourceUtil.getResource("okButton.text"));
		cancelAction.putValue(Action.NAME, ResourceUtil.getResource("avbrytButton.text"));
		applyAction.putValue(Action.NAME, ResourceUtil.getResource("brukButton.text"));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(new JButton(applyAction));
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(new JButton(cancelAction));
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(new JButton(okAction));
		return buttonPanel;
	}
	
	public void hentData(){
		module.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.FUNN.getType());
		module.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.HENDELSE.getType());
		module.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.TEXTBOX.getType());
		module.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.SELECTED.getType());
		
		if (hendelsePanel.isVisR2IKart()){
			plugin.executeTask(hendelsePanel.getTask(), ResourceUtil.getResource("mipssmap.plugins.produksjon.observasjon.FunnPanel.HentHendelseTask.title"), MipssMapLayerType.HENDELSE.getType());
		}
		if (hendelsePanel.isVisR5IKart()){
			plugin.executeTask(hendelsePanel.getR5Task(), ResourceUtil.getResource("mipssmap.plugins.produksjon.observasjon.FunnPanel.HentForsikringsskadeTask.title"), MipssMapLayerType.FORSIKRINGSSKADE.getType());
		}
		if (hendelsePanel.isVisR11IKart()){
			plugin.executeTask(hendelsePanel.getR11Task(), ResourceUtil.getResource("mipssmap.plugins.produksjon.observasjon.FunnPanel.HentSkredTask.title"), MipssMapLayerType.SKRED.getType());
		}
		if (funnPanel.isVisIKart()){
			plugin.executeTask(funnPanel.getTask(), ResourceUtil.getResource("mipssmap.plugins.produksjon.observasjon.FunnPanel.HentFunnTask.title"), MipssMapLayerType.FUNN.getType());
		}
	}
	
	private class OkAction extends AbstractAction{
		public OkAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			hentData();
			Container parent = FunnPanel.this;
	        while (!(parent instanceof JDialog)){
	            parent = parent.getParent();
	        }
	        ((JDialog)parent).setVisible(false);
			
		}
	}
	
	private class CancelAction extends AbstractAction{
		public CancelAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Container parent = FunnPanel.this;
	        while (!(parent instanceof JDialog)){
	            parent = parent.getParent();
	        }
	        ((JDialog)parent).setVisible(false);
			
		}
	}
	private class ApplyAction extends AbstractAction{
		public ApplyAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			hentData();
		}
	}
}
