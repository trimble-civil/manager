package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.textboxdata.ProdpunktTextboxProducer;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;
import no.mesta.mipss.mapplugin.widgets.WidgetController;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.ProductionPlugin;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.task.HentStrekningerTask;
import no.mesta.mipss.mipssmap.utils.ErrorHandler;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.mipssmapserver.KjoretoyDTO;
import no.mesta.mipss.mipssmapserver.KjoretoyVO;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MipssMapProductionPlugin for å vise frem produksjon
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class ProduksjonPlugin extends ProductionPlugin{
	

	private Logger log = LoggerFactory.getLogger(this.getClass());
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssMapText");
	private ProduksjonCriteria criteria ;
	
	public ProduksjonPlugin(MipssMapWindowManager windowManager, MipssMapModule plugin) {
		super(windowManager, plugin, "Produksjon", ResourceUtil.getResource("produksjon.produksjon.tooltip"), ResourceUtil.getResource("produksjon.vis.produksjon.tooltip"));
    	getCheckButton().setAction(new ClickAction());
	}

	protected void buttonActionPerformed(ActionEvent e) {
		if (getPlugin().getCurrentKontrakt() != null) {
			ProduksjonDialog dialog = (ProduksjonDialog) getWindowManager().getDialog(ProduksjonDialog.class);
			if (dialog == null) {
				dialog = initDialog();
				getWindowManager().addDialog(dialog);
			}
			
			dialog.setVisible(true);
		}
	}
	
	public void initPlugin(){
		if (getPlugin().getCurrentKontrakt() != null) {
			ProduksjonDialog dialog = (ProduksjonDialog) getWindowManager().getDialog(ProduksjonDialog.class);
			if (dialog == null) {
				dialog = initDialog();
				getWindowManager().addDialog(dialog);
			}
		}
	}

	protected void checkActionPerformed(ActionEvent e) {
		if (!isLoaded()&&getCheckButton().getCheckBox().isSelected()){
			ProduksjonDialog dialog = initDialog();
			if (dialog.getProduksjonPanel().hentProduksjon())
				setLoaded(true);
		}else{
			boolean visible = getCheckButton().getCheckBox().isSelected();
			setProduksjonVisible(visible, MipssMapLayerType.PRODUKSJONPUNKT);
			setProduksjonVisible(visible, MipssMapLayerType.PRODUKSJONSTREKNING);
			setProduksjonVisible(visible, MipssMapLayerType.PRODUKSJONLABEL);
		}
	}

	/**
	 * Lager produksjonspunkter for visning i kart.
	 * 
	 * @param bil
	 * @param fraDato
	 * @param tilDato
	 */
	private void createProduksjonPunkt(ProduksjonCriteria criteria, List<KjoretoyDTO> failed){
		new CreateProduksjonPunkter(getPlugin(), getCheckButton(), getProduksjonOptions(), criteria).doIt();
	}
	/**
	 * Lager produksjonspunkter for en gitt DFU fo visning i kart.
	 * 
	 * @param bil
	 * @param fraDato
	 * @param tilDato
	 */
	private void createProduksjonPunktDfu(ProduksjonCriteria criteria){
		new CreateProduksjonPunkter(getPlugin(), getCheckButton(), getProduksjonOptions(), criteria).doItDfu();
	}
	private void createPdopPunkt(ProduksjonCriteria criteria, List<KjoretoyDTO> failed){
		new CreatePDOPPunkter(getPlugin(), getCheckButton(), getProduksjonOptions(), criteria.getEnhetList(), 
				criteria.getProdtyper(), failed, criteria.isAllProduksjon(), criteria.isKunSiste(), criteria.isLabels(), criteria).doIt();
	}
	/**
	 * Lager produksjonsstrekninger for visning i kart
	 * 
	 * @param fraDato
	 * @param tilDato
	 */
	private void createProduksjonStrekning(ProduksjonCriteria criteria, List<KjoretoyDTO> failed){
		Date fraDato = getProduksjonOptions().getDatoFra().getDate();
		Date tilDato = getProduksjonOptions().getDatoTil().getDate();
		log.debug("Lager produksjonstrekninger av" + criteria.getEnhetList());
		for (KjoretoyVO kjoretoy : criteria.getEnhetList()) {
			Long kjoretoyId = null;
			Long pdaId = null;
			if (kjoretoy.getKjoretoy() != null)
				kjoretoyId = kjoretoy.getKjoretoy().getId();
			if (kjoretoy.getPda() != null)
				pdaId = kjoretoy.getPda().getId();

			log.debug("Henter strekninger for kjoretoy:" + kjoretoyId+ " eller pda:" + pdaId);
			ProduksjonQueryParams params = new ProduksjonQueryParams();
			params.setKjoretoyId(kjoretoyId);
			params.setDfuId(pdaId);
			params.setFraDato(fraDato);
			params.setTilDato(tilDato);
			params.setProdtyper(criteria.getProdtyper());
			params.setAllProduksjon(criteria.isAllProduksjon());

			final HentStrekningerTask task = new HentStrekningerTask(getPlugin(),params);
			ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.HentStrekningerTask.title"));
			workerPanel.setWorker(task);
			task.setProgressController(getPlugin().getProgressController());
			task.setProgressWorkerPanel(workerPanel);
			getPlugin().getProgressController().addWorkerPanel(workerPanel);

			Thread wait = new Thread("Hent strekning Thread(kjoretoyId="
					+ kjoretoyId + " pda=" + pdaId + ")") {
				public void run() {
					Layer layer = null;
					try {
						layer = task.get();
						if (task.isNoDataFound()) {
							getPlugin().getStatusPanel().getProgressBar().setString("No data found");
						} else if (layer != null) {
							getPlugin().getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONSTREKNING.getType(), layer, true);
							getCheckButton().setChecked(true);
						}
					} catch (ExecutionException e) {
						String msg = ResourceUtil.getResource("mipssmap.feilmelding.general");
						ErrorHandler.showError(e, msg);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			wait.start();
		}
	}
	
	private void handleMissingDfuError(List<KjoretoyDTO> failed) {
		String failedNavn = "";
		for (KjoretoyDTO k : failed) {
			failedNavn += "\n" + k.getNavn();
		}
		failedNavn += "\n";
		JOptionPane.showMessageDialog(getPlugin().getLoader().getApplicationFrame(),failedNavn+ ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.hentkjoretoy.MissingDfuError.description"),
						ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.hentkjoretoy.MissingDfuError.title"),
						JOptionPane.INFORMATION_MESSAGE);
	}

	public boolean hentProduksjon(ProduksjonCriteria criteria){
		setLoaded(true);
		this.criteria = criteria;
		Date datoFra = getProduksjonOptions().getDatoFra().getDate();
		Date datoTil = getProduksjonOptions().getDatoTil().getDate();
		setPrevFra(datoFra);
		setPrevTil(datoTil);
		inSynch();
		
		log.trace("Hent produksjon: datoFra:" + datoFra + " datoTil:" + datoTil
				+ " punkter:" + criteria.isPunkter() + " strekninger:" + criteria.isStrekninger());
		// TODO dette må revurderes!
		removeLayerType(MipssMapLayerType.PRODUKSJONPUNKT);
		removeLayerType(MipssMapLayerType.PRODUKSJONSTREKNING);
		removeLayerType(MipssMapLayerType.PRODUKSJONLABEL);
		removeLayerType(MipssMapLayerType.SELECTED);
		
		WidgetController wc = getPlugin().getMainMap().getLayerHandler().getWidgetController();
		wc.removeWidgets(wc.getWidgetsOfClass(TextboxWidget.class));
		
		if (criteria.getDfuId()!=null){
			createProduksjonPunktDfu(criteria);
			return true;
		}
		
		if (criteria.getEnhetList().size() == 0) {
			JOptionPane
					.showMessageDialog(
							getPlugin().getLoader().getApplicationFrame(),ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.hentkjoretoy.nokjoretoy.description"),
							ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.hentkjoretoy.nokjoretoy.title"),
							JOptionPane.INFORMATION_MESSAGE);
			return false;
		}

		List<KjoretoyDTO> failed = new ArrayList<KjoretoyDTO>();
		if (criteria.isPdop()){
			createPdopPunkt(criteria, failed);
		}
		else if (criteria.isPunkter()) {
			createProduksjonPunkt(criteria, failed);
		}
		
		if (criteria.isStrekninger()) {
			createProduksjonStrekning(criteria, failed);
		}
		

		if (failed.size() > 0)
			handleMissingDfuError(failed);
		return true;
	}
	public List<KjoretoyVO> getSelectedKjoretoy(){
		List<KjoretoyVO> kjoretoy = null;
		ProduksjonDialog dialog = (ProduksjonDialog)getWindowManager().getDialog(ProduksjonDialog.class);
		if (dialog!=null){
			kjoretoy = dialog.getProduksjonPanel().getSelectedKjoretoy();
		}
		return kjoretoy;
	}
	
	public boolean isTidsvinduSelected(){
		ProduksjonDialog dialog = (ProduksjonDialog)getWindowManager().getDialog(ProduksjonDialog.class);
		if (dialog!=null){
			return dialog.getProduksjonPanel().isTidsvinduSelected();
		}
		return false;
	}
	
	private ProduksjonDialog initDialog(){
		Window parent = SwingUtilities.windowForComponent(getCheckButton());
		ProduksjonDialog dialog = new ProduksjonDialog(parent, this, getPlugin());
		dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin.dialog.title"));
		dialog.setMinimumSize(new Dimension(300, 350));
		dialog.setSize(400, 450);
		dialog.setLocationRelativeTo(null);
		return dialog;
	}

	private void removeLayerType(MipssMapLayerType type) {
		getPlugin().getMainMap().getLayerHandler().removeLayerType(type.getType());
	}

	/**
	 * Skjuler eller setter produksjon synlige i kartet
	 * 
	 * @param visible
	 */
	public void setProduksjonVisible(boolean visible, MipssMapLayerType type) {
		if (type==MipssMapLayerType.PRODUKSJONLABEL){
			System.out.println("bob type");
			List<Layer> layers = getPlugin().getMainMap().getLayerHandler().getLayers(MipssMapLayerType.PRODUKSJONPUNKT.getType());
			for (Layer ll:layers){
				if (ll instanceof PointLayerCollection){
					PointLayerCollection pl = (PointLayerCollection)ll;
					List<? extends CollectionEntity> l = pl.getLayers();
					settIdFraTekstboksSynlig(l, !visible);
				}
			}
		}
		if (visible)
			getPlugin().getMainMap().getLayerHandler().showLayerType(type.getType());
		else
			getPlugin().getMainMap().getLayerHandler().hideLayerType(type.getType());
	}
	
    private void settIdFraTekstboksSynlig(List<? extends CollectionEntity> layers, boolean synlig){
    	for (CollectionEntity e:layers){
        	ProdpunktPointLayer p = (ProdpunktPointLayer)e;
        	TextboxProducer tp = p.getTextboxProducer();
        	if (tp instanceof ProdpunktTextboxProducer){
        		((ProdpunktTextboxProducer)tp).setSkjulId(synlig);
        	}
        	
        }
    }
	
	private class ClickAction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e){
			refresh();
		}
	}
	public ProduksjonCriteria getCriteria(){
		return criteria;
	}
	@Override
	public void refresh() {
		if (isEnabled()){
			if (!isTimeSynched()){
				if (!isLoaded()){
					getCheckButton().getCheckBox().setSelected(true);
					checkActionPerformed(null);
				}else{
					if (criteria!=null){
						hentProduksjon(criteria);
					}else{
						setLoaded(false);
						getCheckButton().getCheckBox().setSelected(true);
						checkActionPerformed(null);
					}
				}
			}
		}
		
	}
}
