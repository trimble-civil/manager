package no.mesta.mipss.mipssmap.task;

import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import no.mesta.mipss.core.UTMUtils;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.util.Webcam;
import no.mesta.mipss.mapplugin.util.textboxdata.WebcamTextboxProducer;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.layer.VaerstasjonLayer;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.picturepanel.JPictureDialogue;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;
import no.mesta.mipss.xml.XMLHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class HentSvvCamTask extends MipssMapTask{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private MipssMapModule plugin;
	private boolean noDataFound = false;
	
	public static final String SVV_WEBCAM_URL = "http://webkamera.vegvesen.no/metadata";
	
	public HentSvvCamTask(MipssMapModule plugin){
		super(ExecutorTaskType.PARALELL);
		this.plugin = plugin;
	}
	
	private Action getWebcamAction(final Webcam webcam){
		Action a = new AbstractAction(){
			public void actionPerformed(ActionEvent e){
				try {
					JPictureDialogue picture = new JPictureDialogue(plugin.getLoader().getApplicationFrame(), new URL(webcam.getUrl()));
					picture.showBilde(0);
					picture.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		};
		
		return a;
	}

	@Override
	protected Layer doInBackground() throws Exception {
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
		List<Webcam> webCams = getWebCams();
		if (webCams.isEmpty()){
        	noDataFound = true;
        	return null;
        }
        List<CollectionEntity> layerList = new ArrayList<CollectionEntity>();
        Bounds bounds = new Bounds();
        
        for (final Webcam webcam:webCams){
        	GeoPosition gp = new GeoPosition(webcam.getPoint().getLatitude(), webcam.getPoint().getLongitude());
        	Bounds b = new Bounds();
        	b.setLlx(gp.getLongitude());
    		b.setUrx(gp.getLongitude());
    		b.setLly(gp.getLatitude());
    		b.setUry(gp.getLatitude());
    		
        	if (bounds.isNull())
        		bounds.setBounds(b);
        	else
        		bounds.addBounds(b);
        	
        	VaerstasjonLayer layer = new VaerstasjonLayer("SVV_CAM", false, gp, IconResources.WEBCAM_ICON_32);
        	
        	WebcamTextboxProducer tp = new WebcamTextboxProducer(webcam, getWebcamAction(webcam));
        	layer.setTextboxProducer(tp);
        	layerList.add(layer);
        }
        log.debug("Bounds: {}", bounds);
        PointLayerCollection webcamLayer = new PointLayerCollection("SVV_CAMS", layerList, bounds, false);
		return webcamLayer;
	}
	
	
	private List<Webcam> getWebCams(){
		DefaultHttpClient client = new DefaultHttpClient();
		HttpGet method = new HttpGet(SVV_WEBCAM_URL);
		try {
			HttpResponse response = client.execute((HttpUriRequest) method);
			InputStream rstream = response.getEntity().getContent();
			String s = convertStreamToString(rstream);
			s = s.replaceAll("<info />", "<info/>");
			Document document = XMLHelper.createDocument(new ByteArrayInputStream(s.getBytes()));
			
//			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder = factory.newDocumentBuilder();
////            Document document = builder.parse(new InputSource(new StringReader(s)));
//            Document document = builder.parse(new ByteArrayInputStream(s.getBytes()));
////            document.normalize();
//            // tiltak node
            Element rootElement = document.getDocumentElement();
            NodeList liste = rootElement.getElementsByTagName("webkamera");
            List<Webcam> webcamList = new ArrayList<Webcam>();
            UTMUtils u = new UTMUtils();
            for (int i=0;i<liste.getLength();i++){
            	Webcam a = new Webcam();
            	Node node = liste.item(i);
        		a.setUrl(XMLHelper.getTagValue("url", node));
        		a.setStedsnavn(XMLHelper.getTagValue("stedsnavn", node));
        		a.setVeg(XMLHelper.getTagValue("veg", node));
        		a.setLandsdel(XMLHelper.getTagValue("landsdel", node));
        		a.setLengdegrad(Double.valueOf(XMLHelper.getTagValue("lengdegrad", node)));
        		a.setBreddegrad(Double.valueOf(XMLHelper.getTagValue("breddegrad", node)));
        		a.setVaervarselUrl(XMLHelper.getTagValue("vaervarsel", node));
        		a.setInfo(XMLHelper.getTagValue("info", node));
            	
//            	NodeList c = place.getChildNodes();
//            	for (int j=0;j<c.getLength();j++){
//            		Node item = c.item(j);
//            		if (item.getNodeName().equals("url")){
//            			a.setUrl(item.getNodeValue());
//            		}
//            		if (item.getNodeName().equals("stedsnavn")){
//            			a.setStedsnavn(item.getNodeValue());
//            		}
//            		if (item.getNodeName().equals("veg")){
//            			a.setVeg(item.getNodeValue());
//            		}
//            		if (item.getNodeName().equals("landsdel")){
//            			a.setLandsdel(item.getNodeValue());
//            		}
//            		if (item.getNodeName().equals("lengdegrad")){
//            			a.setLengdegrad(Double.valueOf(item.getNodeValue()));
//            		}
//            		if (item.getNodeName().equals("breddegrad")){
//            			a.setBreddegrad(Double.valueOf(item.getNodeValue()));
//            		}
//            		if (item.getNodeName().equals("vaervarsel")){
//            			a.setVaervarselUrl(item.getNodeValue());
//            		}
//            		if (item.getNodeName().equals("info")){
//            			a.setInfo(item.getNodeValue());
//            		}
//            	}
            	Point2D p = new Point2D.Double();
    			u.convert2UTM(a.getBreddegrad()+"", a.getLengdegrad()+"", p);
    			MipssGeoPosition gp = new MipssGeoPosition( p.getX(), p.getY());
    			a.setPoint(gp);
    			webcamList.add(a);
            }
            return webcamList;
            
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}    	
	
	private String getTagValue(String tagName, Element e){
		NodeList node = e.getElementsByTagName(tagName).item(0).getChildNodes();
		Node item = node.item(0);
		if (item!=null)
			return item.getNodeValue();
		return null;
	}
    public String convertStreamToString(InputStream is) {
    	StringBuilder sb = new StringBuilder();
    	try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	        String line = null;
	        try {
	            while ((line = reader.readLine()) != null) {
	                sb.append(line + "\n");
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                is.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
        return sb.toString();
    }
	public boolean isNoDataFound() {
		return noDataFound;
	}
}
