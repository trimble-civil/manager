package no.mesta.mipss.mipssmap.plugins.layer.omrade;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

import javax.swing.JDialog;

import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.task.HentSvvCamTask;
import no.mesta.mipss.mipssmap.task.HentVaerstasjonerTask;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

/**
 * MipssMapLayerPlugin for å vise frem områder.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class OmradePlugin implements MipssMapLayerPlugin{
    private final MipssMapWindowManager windowManager;
	private final MipssMapModule plugin;
	private JCheckButton button;


	public OmradePlugin(MipssMapWindowManager windowManager, MipssMapModule plugin) {
		this.windowManager = windowManager;
		this.plugin = plugin;
		button = new JCheckButton(ResourceUtil.getResource("mipssmap.plugins.layer.omrade.OmradePlugin.button.text"));
        button.getButton().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                omradeActionPerformed(e);
            }
        });
        button.getCheckBox().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if (button.getCheckBox().isSelected()){
            		omradeActionPerformed(e);
            	}else{
            		reset();
            	}
            }
        });
    }


    public JCheckButton getCheckButton() {
        return button;
    }

    private void omradeActionPerformed(ActionEvent e) {
    	JDialog dialog = windowManager.getDialog(SvvCamDialog.class);
    	if (dialog==null){
			dialog = new SvvCamDialog(plugin.getLoader().getApplicationFrame(), this, plugin);
			dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.SvvCamDialog.title"));
	        dialog.setSize(300, 200);
	        dialog.setLocationRelativeTo(null);
	        dialog.setResizable(false);
			windowManager.addDialog(dialog);
		}
		dialog.setVisible(true);
	}
    
    public void createWebcams(){
		
		final HentSvvCamTask task = new HentSvvCamTask(plugin);
		
        ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.layer.vaerstasjon.VaerstasjonPlugin.HentVaerstasjonerTask.title"));
        workerPanel.setWorker(task);
        task.setProgressController(plugin.getProgressController());
        task.setProgressWorkerPanel(workerPanel);
        plugin.getProgressController().addWorkerPanel(workerPanel);
		
//		final HentVaerstasjonerTask task = new HentVaerstasjonerTask(plugin);
		task.execute();

		Thread wait = new Thread() {
			public void run() {
				Layer layer = null;
				try {
					layer = task.get();
					if (task.isNoDataFound()) {
						plugin.getStatusPanel().getProgressBar().setString("Ingen data funnet");
					} else if (layer != null) {
						plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.WEBCAM.getType(), layer, true);
						getCheckButton().setChecked(true);
					}
				} catch (ExecutionException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		wait.start();
	}
	@Override
	public void reset() {
		plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.WEBCAM.getType());
	}


	@Override
	public void setEnabled(boolean enabled) {
		
	}
}
