package no.mesta.mipss.mipssmap.animation;

import java.util.Date;

public class AnimationDates {
    private Date firstDate;
    private Date lastDate;
    private Date currentDate;
    
    public AnimationDates(Date firstDate, Date lastDate) {
        this.firstDate = firstDate;
        currentDate = firstDate;
        this.lastDate = lastDate;
    }
    
    
    public void setCurrentDate(Date currentDate){
    	this.currentDate = currentDate;
    }
    
    public Date getCurrentDate(){
        return currentDate;
    }
    
    public Date getFirstDate() {
        return firstDate;
    }

    public Date getLastDate() {
        return lastDate;
    }
}
