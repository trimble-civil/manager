package no.mesta.mipss.mipssmap.utils;

import java.awt.Color;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.LabelLayer;
import no.mesta.mipss.mipssmap.task.HentProduksjonTask;

public class ProdpunktLabelCreator {
	public LabelLayer getLabelUE(String[] labelText, MipssGeoPosition lastPosition, String labelId) {
		String v1 = KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_UE);
		String v2= KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_TEXT_UE);
		Color fill = ColorUtil.setAlpha(ColorUtil.fromHexString(v1), 128);
		Color border = ColorUtil.fromHexString(v1);
		Color textColor = ColorUtil.fromHexString(v2);
		return new LabelLayer(labelId, lastPosition, labelText, fill, border, textColor);
	}

	public LabelLayer getLabelPDA(String[] labelText, MipssGeoPosition lastPosition, String labelId) {
		String v1 = KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_INSPEKSJON);
		String v2= KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_TEXT_INSPEKSJON);
		Color fill = ColorUtil.setAlpha(ColorUtil.fromHexString(v1), 140);
		Color border = ColorUtil.fromHexString(v1);
		Color textColor = ColorUtil.fromHexString(v2);
		return new LabelLayer(labelId, lastPosition, labelText,fill, border, textColor);
	}
	
	public LabelLayer getLabelDFU(String[] labelText, MipssGeoPosition lastPosition, String labelId) {
		Color fill = ColorUtil.setAlpha(ColorUtil.fromHexString("0x006f61"), 140);
		Color border = ColorUtil.fromHexString("0x007c6c");
		Color textColor = ColorUtil.fromHexString("0xffffff");
		return new LabelLayer(labelId, lastPosition, labelText,fill, border, textColor);
	}

	public LabelLayer getMestabilLabel(String[] labelText,  MipssGeoPosition lastPosition, String labelId) {
		String v1 = KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_MESTA);
		String v2= KonfigparamCache.getKonfigparamValue(KonfigparamCache.FARGE_LABEL_TEXT_MESTA);
		Color c = ColorUtil.fromHexString(v1);
		Color fill = ColorUtil.setAlpha(c, 128);
		Color border = ColorUtil.fromHexString(v1);
		Color textColor = ColorUtil.fromHexString(v2);
		return new LabelLayer(labelId, lastPosition, labelText,fill, border, textColor);
	}
}
