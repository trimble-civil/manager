package no.mesta.mipss.mipssmap.animation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JSlider;

import no.mesta.mipss.mapplugin.tools.JFreeChartTool;

public class SliderThread extends Thread{
    private JSlider slider;
    private AnimationDates ss;
    private boolean running=true;
    public boolean pleaseWait = false;
    private int currentTime = 0;
    private JFreeChartTool chartTool;
	private final DefaultComboBoxModel speedModel;
    
    public SliderThread(JSlider slider, AnimationDates ss, DefaultComboBoxModel speedModel, JFreeChartTool chartTool){
    	super("Timeslider thread");
        this.slider = slider;
        this.ss = ss;
		this.speedModel = speedModel;
		this.chartTool = chartTool;
        
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Hashtable table = new Hashtable();
        long time = (ss.getLastDate().getTime()-ss.getFirstDate().getTime())/1000;
        int times = (int)time/5;
        for (int i=0;i<time;i++){
            if (i%times==0||i==0||i==time-1){
                Date date = new Date(ss.getFirstDate().getTime()+(i*1000));
                table.put(new Integer(i), new JLabel(format.format(date)));
            }
        }
        slider.setMinimum(0);
        slider.setMaximum((int)(ss.getLastDate().getTime()-ss.getFirstDate().getTime())/1000);
        slider.setLabelTable(table);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setValue(0);
        slider.setMajorTickSpacing((int)time/10);
        slider.setMinorTickSpacing((int)time/5);
        
    }
    
    public void run(){
    	currentTime = 0;
    	chartTool.setInAnimation(true);
        while(currentTime<slider.getMaximum()||running){
        	if (currentTime>=slider.getMaximum()){
        		Thread.yield();
        		continue;
        	}
        	slider.setValue(currentTime);
            Date d = new Date(ss.getFirstDate().getTime()+ (currentTime*1000));
            ss.setCurrentDate(d);
            synchronized(ss){
                ss.notifyAll();
            }
            chartTool.setTimeMarkerAll(d);
            synchronized(this){
                try {
                    sleep(1000/getSpeed());//sov i 1 sekund / hastigheten
                } catch (InterruptedException e) {
                }
            }
            
            synchronized(this){
                while (pleaseWait) {
                    try {
                        wait();
                    } catch (Exception e) {
                    }
                }   
            }
            currentTime++;
        }
        chartTool.setInAnimation(false);
    }
    
    public void stopRunning(){
        running = false;
        currentTime = slider.getMaximum();
        ss.setCurrentDate(new Date());
        synchronized(ss){
            ss.notifyAll();
        }
    }
    public int getSpeed(){
        String s = (String)speedModel.getSelectedItem();
        return Integer.parseInt(s.substring(0, s.length()-1));
    }
    public void setCurrentTime(int currentTime){
    	if (currentTime<this.currentTime){
        	Date d = new Date(ss.getFirstDate().getTime()+ (currentTime*1000));
            ss.setCurrentDate(d);
    	}
        this.currentTime = currentTime;
    }
}