package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.Color;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

public class ProdtypeTableObject implements IRenderableMipssEntity, CheckBoxTableObject{

	private Prodtype prodtype;
	private String sesong;
	private Color farge;
	private boolean valgt;
	
	public ProdtypeTableObject(Prodtype prodtype){
		this.prodtype = prodtype;
		if (prodtype.getSesong()!=null)
			this.sesong = prodtype.getSesong().getTextForGUI();
		this.farge = ColorUtil.fromHexString(prodtype.getGuiFarge());
		this.valgt = false;
	}
	
	public Prodtype getProdtype(){
		return prodtype;
	}
	public String getProdtypeNavn(){
		return prodtype.getNavn();
	}
	
	public String getSesong(){
		return sesong;
	}
	
	public Color getFarge(){
		return farge;
	}
	
	public Boolean getValgt(){
		return valgt;
	}
	
	public void setValgt(Boolean valgt){
		this.valgt = valgt;
	}

	@Override
	public String getTextForGUI() {
		return "";
	}
}
