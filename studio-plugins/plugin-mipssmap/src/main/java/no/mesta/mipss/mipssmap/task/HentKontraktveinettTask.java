package no.mesta.mipss.mipssmap.task;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.service.veinett.KontraktveinettVO;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;
/**
 * 
 * Worker som henter kontraktveinett og returnerer et layer til kartet
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 * @See LineListLayer
 *
 */
public class HentKontraktveinettTask extends MipssMapTask{//SwingWorker<Layer, Integer>{

	private Driftkontrakt kontrakt;
	private MipssMapModule plugin;
	private final boolean veireferanser;
		
	public HentKontraktveinettTask(MipssMapModule plugin, Driftkontrakt kontrakt, boolean veireferanser){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.kontrakt = kontrakt;
		this.veireferanser = veireferanser;
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
			return null;
		}
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		
		VeinettService mipssMap = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
		
		KontraktveinettVO veinett = mipssMap.getKontraktveinett(kontrakt.getId(), veireferanser);
        //veinett.setVeinettIndices(null);
        //veinett = null;
		if (veireferanser){
			return new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).getKontraktveiLayer(veinett);
		}
		return new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).getLayerFromKontraktveinettVO(veinett);
	}
}
