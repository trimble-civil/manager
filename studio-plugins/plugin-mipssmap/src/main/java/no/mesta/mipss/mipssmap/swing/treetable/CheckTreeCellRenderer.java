package no.mesta.mipss.mipssmap.swing.treetable;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import org.jdesktop.beansbinding.Binding;

public class CheckTreeCellRenderer extends JPanel implements TreeCellRenderer{ 
    private CheckTreeSelectionModel selectionModel; 
    private TreeCellRenderer delegate; 
    private TristateCheckBox checkBox = new TristateCheckBox(); 
    private Binding binding = null;
    
    public CheckTreeCellRenderer(TreeCellRenderer delegate, CheckTreeSelectionModel selectionModel){ 
        this.delegate = delegate; 
        this.selectionModel = selectionModel; 
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS)); 
        setOpaque(false); 
        checkBox.setOpaque(false); 
        

    } 
 
 
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus){ 
        //Component renderer = delegate.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus); 
        
        TreePath path = tree.getPathForRow(row); 
        CheckButtonNode node = (CheckButtonNode)value;

        if(path!=null){
            if(selectionModel.isPathSelected(path, true)){ 
                checkBox.setState(TristateCheckBox.SELECTED);
                node.setSelected(true);
            }
            else{ 
                checkBox.setState(selectionModel.isPartiallySelected(path) ? null : TristateCheckBox.NOT_SELECTED);
                node.setSelected(false);
            }
        } 
        
        removeAll(); 
        add(checkBox); 
        //add(renderer); 
        
        
        if (node.getIcon()!=null){
        	add(new JLabel(new ImageIcon(node.getIcon())));
        }else{
        	add(Box.createHorizontalStrut(20));
        }
        	
		JLabel label = new JLabel(node.getText());
		label.setOpaque(false);
		
		if (node.getText()!=null){
			add(Box.createHorizontalStrut(5));
			add(label);
		}

		tree.repaint();
		
        return this; 
    } 
} 

//if (binding == null){
//    binding =Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ_WRITE, checkBox, BeanProperty.create("selected"), node, BeanProperty.create("selected"));
//    binding.bind();
//    try{
//    	tree.revalidate();
//    }catch (Throwable e){}
//}