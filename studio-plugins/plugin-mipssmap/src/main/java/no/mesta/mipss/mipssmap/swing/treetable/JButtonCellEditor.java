package no.mesta.mipss.mipssmap.swing.treetable;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;

import javax.swing.AbstractCellEditor;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
/**
 * 
 * This is a CellEditor for the properties table, it contains a JButton. The event removes an Attribute or
 * Property from the underlying PropertyNode
 * @author Harald A. Kulø
 */
public class JButtonCellEditor extends AbstractCellEditor implements TableCellEditor {
    
	private JButton button;
	private JPanel panel = new JPanel();
	
	public JButtonCellEditor(JButton button){
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		panel.setOpaque(false);
		this.button = button;
		button.setFont(new Font("ARIAL", Font.PLAIN, 10));
		
		panel.add(Box.createVerticalStrut(1));
		panel.add(getButtonPanel(button));
		panel.add(Box.createVerticalStrut(1));

	}
	
	private JPanel getButtonPanel(JButton button){
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.add(button);
		panel.add(Box.createHorizontalGlue());
		return panel;
		
	}
	
	public Component getTableCellEditorComponent(final JTable table, Object value, boolean isSelected, int row, int column) {		
		return panel;
	}

	public Object getCellEditorValue() {
		return button.getText();
	}
}
