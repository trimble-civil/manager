package no.mesta.mipss.mipssmap.plugins.layer.rode;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.layer.LineListLayerCollection;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.model.RodeTableModel;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.ColorEditor;
import no.mesta.mipss.ui.table.ColorRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RodeTableObject;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;


@SuppressWarnings("serial")
public class RodePanel extends JPanel {
    private JComboBox rodeTypeCombo;
    private JLabel rodeTypeLabel;
    private RodeScroller rodeScroll;
    private MipssMapModule module;
    private RodePlugin plugin;
    private RodeTableModel<RodeTableObject> rodeTableModel;
    
    private JCheckBox checkAllCheck;
    private JCheckBox activeCheck;
    private JLabel rodetykkelseLabel;
    
    
    private JDialog dialog;
	private JComboBox rodetykkelseCombo;
    public RodePanel(MipssMapModule module, RodePlugin plugin, JDialog dialog) {
        this.module = module;
        this.plugin = plugin;
        this.dialog = dialog;
        initGUI();
    }
    
    /**
     * Setter opp layouten for panelet. 
     */
    private void initGUI() {
    	initComponents();
    	setLayout(new GridBagLayout());
    	checkAllCheck = new JCheckBox(new CheckAllAction("Velg alle", IconResources.SELECT_ALL));
    	
    	JButton closeButton = new JButton(new CloseAction("Lukk", IconResources.CANCEL_ICON));
    	
    	add(rodeTypeLabel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
    	add(rodeTypeCombo, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
    	add(activeCheck, new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
    	
    	add(rodeScroll, new GridBagConstraints(0,1, 3,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
    	
    	
    	JPanel buttonPanel = new JPanel(new GridBagLayout());
    	
    	buttonPanel.add(checkAllCheck,     new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
    	buttonPanel.add(rodetykkelseLabel, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
    	buttonPanel.add(rodetykkelseCombo, new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
    	buttonPanel.add(closeButton, new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
    	
    	add(buttonPanel, new GridBagConstraints(0,2, 3,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
    }
    
    /**
     * Initialiserer guikomponentene 
     */
    private void initComponents(){
    	
    	rodetykkelseLabel = new JLabel(ResourceUtil.getResource("mipssmap.plugins.layer.rode.RodePanel.rodetykkelse.text"));
    	createRodeTykkelseKonfig();
    	
        rodeTypeCombo = new JComboBox();
        List<Rodetype> rodetyper = module.getMipssMapBean().getRodetypeForKontrakt(module.getCurrentKontrakt().getId());
        final MipssComboBoxModel<Rodetype> rodetypeModel = new MipssComboBoxModel<Rodetype>(rodetyper);
        
        List<RodeTableObject> rodeTlist = new ArrayList<RodeTableObject>();
        String[] columns = new String[]{"guiFarge", "vises", "navn"};
        rodeTableModel = new RodeTableModel<RodeTableObject>(RodeTableObject.class, rodeTlist, columns);
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(RodeTableObject.class, columns);
        
        final JXTable rodeTable = new JXTable();
        rodeTypeCombo.setModel(rodetypeModel);
        rodeTypeCombo.setRenderer(new MipssListCellRenderer<Rodetype>());
        rodeTypeCombo.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
            	module.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.RODE.getType());
            	checkAllCheck.setSelected(false);
            	getRodeData(rodetypeModel.getSelectedItem().getId(), activeCheck.isSelected());
			}
        });
        
        activeCheck = new JCheckBox(new ActiveCheckAction("Vis kun aktive roder", null));
        activeCheck.setSelected(true);
        activeCheck.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
            	module.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.RODE.getType());
            	getRodeData(rodetypeModel.getSelectedItem().getId(), activeCheck.isSelected());
			}
        });
        
        initRodeTable(rodeTable, rodeTableModel, columnModel);
        
        rodeScroll = new RodeScroller(rodeTable);
        rodeScroll.setPreferredSize(new java.awt.Dimension(3, 3));
        rodeScroll.setViewportView(rodeTable);
        
        createTableModelListener(rodeTable, rodeTableModel);
            
        
        rodeTypeLabel = new JLabel();
        rodeTypeLabel.setText(ResourceUtil.getResource("mipssmap.plugins.layer.rode.RodePanel.rodetypelabel.text"));
        if (rodetypeModel.getSelectedItem()!=null)
        	getRodeData(rodetypeModel.getSelectedItem().getId(), activeCheck.isSelected());
    }
    
    private void getRodeData(final Long rodetypeId, final boolean kunAktive){
    	final SwingWorker<Object, Object> w = new SwingWorker<Object, Object>(){

			@SuppressWarnings("unchecked")
			@Override
			protected Object doInBackground() throws Exception {
				rodeScroll.setBusy(true);
				Long id = module.getCurrentKontrakt().getId();
				List<Rode> rodeList = kunAktive?module.getMipssMapBean().getAktiveRodeForKontrakt(id, rodetypeId):
												module.getMipssMapBean().getRodeForKontrakt(id, rodetypeId);
		        List<RodeTableObject> rodeTlist = new ArrayList<RodeTableObject>();
		        for (Rode r:rodeList){
		        	RodeTableObject o= new RodeTableObject(r);
		        	rodeTlist.add(o);
		        }
		        
		        rodeScroll.setBusy(false);
		        rodeTableModel.getEntities().clear();
		        rodeTableModel.getEntities().addAll(rodeTlist);
		        if (checkAllCheck.isSelected()){
	        		new CheckAllAction(null,null).actionPerformed(null);
	        	}
		        return null;
			}
    	};
    	w.execute();
    	new Thread(){
    		public void run(){
    			try {
					w.get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
    		}
    	}.start();
    }
    
    
    private JComboBox createRodeTykkelseKonfig(){
    	DefaultComboBoxModel model = new DefaultComboBoxModel();
    	String verdierStr = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "rodebreddeVerdier").getVerdi();
    	String[] verdier = verdierStr.split(",");
    	for (String s:verdier){
    		model.addElement(s);
    	}
    	rodetykkelseCombo = new JComboBox(model);
    	rodetykkelseCombo.setSelectedItem(null);
    	rodetykkelseCombo.addActionListener(new ByttRodeTykkelse());
    	
    	return rodetykkelseCombo;
    }
    /**
     * Setter render, editor og modell til tabellen som inneholder roder
     * @param rodeTable
     * @param rodeTableModel
     * @param columnModel
     */
	private void initRodeTable(final JXTable rodeTable, final RodeTableModel<RodeTableObject> rodeTableModel, MipssRenderableEntityTableColumnModel columnModel) {
		rodeTable.setDefaultRenderer(Color.class, new ColorRenderer(true));
        rodeTable.setDefaultEditor(Color.class, new ColorEditor());
        rodeTable.setModel(rodeTableModel);
        rodeTable.setColumnModel(columnModel);
        
        rodeTable.getColumnModel().getColumn(0).setMaxWidth(45);
        rodeTable.getColumnModel().getColumn(1).setMaxWidth(37);
	}
	
	/**
	 * Legger til en modelListener til rode tabel modellen
	 * @param rodeTable
	 * @param rodeTableModel
	 */
	private void createTableModelListener(final JXTable rodeTable, final RodeTableModel<RodeTableObject> rodeTableModel) {
		rodeTableModel.addTableModelListener(new TableModelListener(){
		    @SuppressWarnings("unchecked")
			public void tableChanged(TableModelEvent e) {
		        if (e.getType()==TableModelEvent.UPDATE)
		        synchronized(this){
		            int row = e.getFirstRow();
		            int column = e.getColumn();
		            
		            RodeTableModel<RodeTableObject> model = (RodeTableModel<RodeTableObject>)e.getSource();
		            
		            Object value = model.getValueAt(row, column);
		            
		            //endre farge på rode
		            if (column==0){
		                if (value instanceof Color){
		                    colorChanged(row, model, value);
		                }
		            }
		            //tegn rode
		            if (column==1){
		                visibilityChanged(rodeTable, row, model, value);
		            }
		        }
		    }
		    
		    /**
		     * Fargen til raden har endret seg
		     * @param row
		     * @param model
		     * @param value
		     */
			private void colorChanged(int row, RodeTableModel<RodeTableObject> model, Object value) {
				Color newColor = (Color)value;
				RodeTableObject r = (RodeTableObject)model.getRowObject(row);
				Rode rode = r.getRode();
				Layer l = module.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.RODE.getType(), ""+rode.getId());
				if (l!=null){
					if (l instanceof LineListLayerCollection){
						
						List<? extends CollectionEntity> layers = ((LineListLayerCollection)l).getLayers();
						for (CollectionEntity ll:layers){
							((Layer)ll).setColor(newColor);
							ll.getParent().setChanged(true);
						}
					}else
						l.setColor(newColor);
					module.getMainMap().getLayerHandler().changeLayer(l);
				}
			}
			
			/**
			 * Synligheten til roden har endret seg. 
			 * @param rodeTable
			 * @param row
			 * @param model
			 * @param value
			 */
			@SuppressWarnings("unchecked")
			private void visibilityChanged(final JXTable rodeTable, int row, RodeTableModel<RodeTableObject> model, Object value) {
				if (value instanceof Boolean){
				    Boolean visible = (Boolean) value;
				    RodeTableObject r = (RodeTableObject)model.getRowObject(row);
				    Rode rode = r.getRode();
				    Layer l = module.getMainMap().getLayerHandler().getLayer(MipssMapLayerType.RODE.getType(), ""+rode.getId());
				    
				    if (l==null){
				    	plugin.createRodeVeinett(rode);
				    }else{
				    	if (visible){
				    		module.getMainMap().getLayerHandler().showLayer(MipssMapLayerType.RODE.getType(), ""+rode.getId());
					    }else{
					    	module.getMainMap().getLayerHandler().hideLayer(MipssMapLayerType.RODE.getType(), ""+rode.getId());
					    }
				    }
				}
				
				List<RodeTableObject> list = model.getEntities();
				boolean checked = false;
				for (RodeTableObject o:list){
					if (o.getVises()!=null&&o.getVises().booleanValue()){
						checked = true;
					}
				}
				plugin.getCheckButton().setChecked(checked);
				rodeTable.repaint();
			}
		});
	}
	private class RodeScroller extends JScrollPane{
		private JXBusyLabel busyLabel ;
		private JXTable table;
		
		public RodeScroller(JXTable table){
			this.table = table;
			busyLabel = new JXBusyLabel();
			busyLabel.setText("Vennligst vent, henter roder..");
		}
		
		public void setBusy(boolean busy){
			busyLabel.setVisible(busy);
			busyLabel.setBusy(busy);
			table.setVisible(!busy);
			this.setViewportView(busy?busyLabel:table);
			repaint();
		}
	}
	private class CloseAction extends AbstractAction{
		
		public CloseAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			dialog.setVisible(false);
			
		}	
	}
	
	private class CheckAllAction extends AbstractAction{
		
		public CheckAllAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JCheckBox b = checkAllCheck;
			if (rodeTableModel!=null){
				for (int i=0;i<rodeTableModel.getEntities().size();i++){
					Boolean selected = (Boolean)rodeTableModel.getValueAt(i, 1);
					if (selected==null)
						selected = false;
					if (b.isSelected()){
						if (!selected.booleanValue())
							rodeTableModel.setValueAt(true, i, 1);
					}
					else
						if (selected.booleanValue())
							rodeTableModel.setValueAt(false, i, 1);
					
				}
			}
		}
	}
	
	private class ActiveCheckAction extends AbstractAction{
		
		public ActiveCheckAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			
		}
	}
	
	private class ByttRodeTykkelse extends AbstractAction{
		public ByttRodeTykkelse(){
			super(null, null);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			List<Layer> rodeLayers = module.getMainMap().getLayerHandler().getLayers(MipssMapLayerType.RODE.getType());
			JComboBox b = (JComboBox)e.getSource();
			String rodeTykkelse = (String)b.getSelectedItem();
			if (rodeTykkelse==null){
				return;
			}
			int stroke = Integer.valueOf(rodeTykkelse);
			
			if (rodeLayers!=null){
				for (Layer l:rodeLayers){
					if (l instanceof LineListLayerCollection){
						List<? extends CollectionEntity> layers = ((LineListLayerCollection)l).getLayers();
						for (CollectionEntity ce:layers){
							if (ce instanceof LineListLayer){
								((LineListLayer)ce).setStroke(stroke);
								l.setChanged(true);
							}
						}
					}
					module.getMainMap().getLayerHandler().changeLayer(l);
				}
				
			}
		}
		
	}

}