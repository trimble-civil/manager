package no.mesta.mipss.mipssmap.utils;

import javax.swing.JCheckBox;

import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.persistence.Clock;

/**
 * Klasse som holder på utvalgskriteriene som er tilgjengelig for tidsbasert utplukking av data for visning i kart
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class ProduksjonOptions {
    private MipssDateTimePickerHolder datoFra;
    private MipssDateTimePickerHolder datoTil;
    private JCheckBox natidCheck;
    
    public ProduksjonOptions() {
    }

    public void setDatoFra(MipssDateTimePickerHolder datoFra) {
        this.datoFra = datoFra;
    }

    public MipssDateTimePickerHolder getDatoFra() {
        return datoFra;
    }

    public void setDatoTil(MipssDateTimePickerHolder datoTil) {
        this.datoTil = datoTil;
    }

    public MipssDateTimePickerHolder getDatoTil() {
    	if (natidCheck.isSelected()){
    		datoTil.setDate(Clock.now());
    	}
        return datoTil;
    }

    public void setNatidCheck(JCheckBox natidCheck) {
        this.natidCheck = natidCheck;
    }

    public JCheckBox getNatidCheck() {
        return natidCheck;
    }
}
