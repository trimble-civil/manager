package no.mesta.mipss.mipssmap.utils;

import java.util.List;

import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

public class CheckBoxTableFactory{

	
	public static <T> MipssRenderableEntityTableModel createCheckBoxTable(int[] editableColumnIndices, String[] columns, List<T> entities, Class<T> clazz){
		MipssRenderableEntityTableModel tableModel = new MipssRenderableEntityTableModel(clazz, editableColumnIndices, columns);
		tableModel.setEntities(entities);
		return tableModel;
	}
}
