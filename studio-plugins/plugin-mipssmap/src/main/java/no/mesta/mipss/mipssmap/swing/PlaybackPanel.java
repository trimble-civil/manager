/*
 * PlaybackPanel.java
 *
 * Created on 16. juni 2008, 10:39
 */

package no.mesta.mipss.mipssmap.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.LayerCollection;
import no.mesta.mipss.mapplugin.layer.PointLayer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.util.LayerEvent;
import no.mesta.mipss.mapplugin.util.LayerListener;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.animation.AnimationDates;
import no.mesta.mipss.mipssmap.animation.AnimationThread;
import no.mesta.mipss.mipssmap.animation.SliderThread;
import no.mesta.mipss.mipssmap.model.PlayDataTableModel;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.resources.images.IconResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTargetAdapter;
import org.jdesktop.animation.timing.interpolation.PropertySetter;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXPanel;


/**
 * Panel for å spille av en kjøretur. 
 * 
 * REMINDER : Klassen er ikke helt stueren,  Fiks l8er
 * @author  harkul
 */
public class PlaybackPanel extends JXPanel implements LayerListener {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private JSlider playSlider;
//    private .JButton backButton;
    private JPanel buttonPanel;
//    private .JButton fwdButton;
    private JLabel hastighetLabel;
    private JComboBox speedCombo;
    private JButton lukkButton;
    private JButton pauseButton;
    private JButton playButton;
    private JButton stopButton;
    private JCheckBox followPointCheck;
    
    private JPanel playControlPanel;
    private JPanel contentPanel;
    private JTable dataTable;
    private DefaultComboBoxModel speedModel;
    private JScrollPane dataScroll;
    
    private PlayDataTableModel playModel;
    
    private MapPanel mapPanel;
    
    private ProdpunktPointLayer first;
    private LayerCollection playData;
    private JXHeader header;
	private SliderThread sliderRunner;
	private AnimationDates timesynchronizer;
	
	private AnimationThread playThread;
	
    /** Creates new form PlaybackPanel */
    public PlaybackPanel(MipssMapPanel mapPanel) {
        this.mapPanel = mapPanel;
        mapPanel.getLayerHandler().addLayerListener(this);
        
        setLayout(new BorderLayout());
        playControlPanel = new JPanel();
        header = getHeader();
        add(header, BorderLayout.NORTH);
        initGui();
        initContentComponents();
    }
    
    private JXHeader getHeader(){
        JXHeader header = new JXHeader();
        header.setTitle(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.header.title"));
        header.setDescription(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.header.description"));
        header.setIcon(IconResources.CAMERA_ICON);
        return header;
    }

    private void initContentComponents(){
        contentPanel = new JPanel(new BorderLayout());
        contentPanel.add(playControlPanel, BorderLayout.NORTH);
        
        dataTable = new JTable();
        dataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        playModel = new PlayDataTableModel(null);
        dataTable.setModel(playModel);
        
        dataScroll = new JScrollPane();
        dataScroll.setViewportView(dataTable);
        dataScroll.setPreferredSize(new Dimension(100, 150));          
        
        followPointCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.followPointCheck.text"));
        followPointCheck.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                followCheckBoxActionPerformed(e);
            }
        });
        
        FlowLayout flow = new FlowLayout();
        flow.setAlignment(FlowLayout.RIGHT);
        JPanel buttonPanel = new JPanel(flow );
        buttonPanel.add(followPointCheck);
        buttonPanel.add(lukkButton);
        contentPanel.add(dataScroll, BorderLayout.CENTER);
        contentPanel.add(buttonPanel, BorderLayout.SOUTH);
        
        add(contentPanel, BorderLayout.CENTER);
        if (mapPanel.getLayerHandler().getCurrentSelected() instanceof PointLayer){
            initPlayer((PointLayer)mapPanel.getLayerHandler().getCurrentSelected());
        }
    }
    
    private void initGui(){
    	playControlPanel.setLayout(new BoxLayout(playControlPanel, BoxLayout.PAGE_AXIS));
    	
        playButton = new JButton();
//        backButton = new JButton();
        pauseButton = new JButton();
        stopButton = new JButton();
//        fwdButton = new JButton();
        hastighetLabel = new JLabel();
        playSlider = new JSlider();
        lukkButton = new JButton();
        speedCombo = new JComboBox();

        lukkButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                lukkButtonActionPerformed(e);
            }
        });
        playButton.setIcon(new ImageIcon(getClass().getResource("/images/player_play.png")));
        playButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });

//        backButton.setIcon(new ImageIcon(getClass().getResource("/images/player_back.png")));
//        backButton.addActionListener(new ActionListener(){
//            public void actionPerformed(ActionEvent e){
//                backButtonActionPerformed(e);
//            }
//        });
        pauseButton.setIcon(new ImageIcon(getClass().getResource("/images/player_pause.png")));
        pauseButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                pauseButtonActionPerformed(e);
            }
        });
        stopButton.setIcon(new ImageIcon(getClass().getResource("/images/player_stop.png")));
        stopButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                stopButtonActionPerformed(e);
            }
        });
        
//        fwdButton.setIcon(new ImageIcon(getClass().getResource("/images/player_fwd.png")));
//        fwdButton.addActionListener(new ActionListener(){
//            public void actionPerformed(ActionEvent e){
//                fwdButtonActionPerformed(e);
//            }
//        });
        hastighetLabel.setText(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.hastighetLabel.text"));

        playSlider.setMajorTickSpacing(10);
        playSlider.setMinorTickSpacing(5);
        playSlider.setPaintTicks(true);
        playSlider.setValue(0);
        playSlider.addChangeListener(new ChangeListener(){         
            public void stateChanged(ChangeEvent e) {
                playSliderStateChanged(e);
            }
        });
        
        lukkButton.setText(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.lukkButton.text"));
        speedModel = new DefaultComboBoxModel(new String[]{ "1x","5x","10x","20x","30x","40x", "50x", "100x", "200x" });
        speedCombo.setModel(speedModel);
        speedCombo.setSelectedIndex(2);
        speedCombo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(playThread!=null)
					playThread.setSpeed(getSpeed());
			}
        });
        
        Dimension buttonSize = new Dimension(40,40);
        setComponentSize(playButton, buttonSize);
        setComponentSize(pauseButton, buttonSize);
        setComponentSize(stopButton, buttonSize);
        setComponentSize(speedCombo, new Dimension(75, 24));
        Dimension space = new Dimension(5,0);
        Dimension spaceL = new Dimension(20,0);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(Box.createRigidArea(space));
        buttonPanel.add(playButton);
        buttonPanel.add(Box.createRigidArea(space));
        buttonPanel.add(pauseButton);
        buttonPanel.add(Box.createRigidArea(space));
        buttonPanel.add(stopButton);
        buttonPanel.add(Box.createRigidArea(spaceL));
        buttonPanel.add(hastighetLabel);
        buttonPanel.add(Box.createRigidArea(space));
        buttonPanel.add(speedCombo);
        buttonPanel.add(Box.createHorizontalGlue());
        
        
        JPanel sliderPanel = new JPanel();
        sliderPanel.setLayout(new BoxLayout(sliderPanel, BoxLayout.LINE_AXIS));
        sliderPanel.add(Box.createRigidArea(space));
        sliderPanel.add(playSlider);
        sliderPanel.add(Box.createRigidArea(space));
        
        playControlPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        playControlPanel.add(buttonPanel);
        playControlPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        playControlPanel.add(sliderPanel);
        playControlPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        
    }
    

	private void setComponentSize(JComponent c, Dimension d){
    	c.setPreferredSize(d);
    	c.setMinimumSize(d);
    	c.setMaximumSize(d);
    }
	private int getSpeed(){
        String s = (String)speedModel.getSelectedItem();
        return Integer.parseInt(s.substring(0, s.length()-1));
    }
    
    /**
     * Begynner avspilling av kjøretur, hvis pause knappen er trykket vil denne metoden starte
     * avspilling igjen
     * @param e
     */
    private void playButtonActionPerformed(ActionEvent e) {       
    	if (playThread!=null&&playThread.isAlive()){
    		pauseButtonActionPerformed(e);
    		return;
    	}
    	
        if (mapPanel.getLayerHandler().getCurrentSelected() instanceof PointLayer){
        	String s = (String)speedModel.getSelectedItem();
            int speed = Integer.parseInt(s.substring(0, s.length()-1));
        	playThread = new AnimationThread((PointLayerCollection)playData,dataTable, null, mapPanel, speed, "Playback vehicle thread");
        	playThread.setStrekningSynchronizer(timesynchronizer);
        	playThread.start();
        	sliderRunner = new SliderThread(playSlider, timesynchronizer, speedModel, mapPanel.getJFreeChartTool());
        	sliderRunner.start();
        }
    }
    /**
     * Setter avspilling på pause, ved 2. klikk startes avspillin igjen.
     * @param e
     */
    private void pauseButtonActionPerformed(ActionEvent e){
    	if (playThread!=null){
	    	if (playThread.isPleaseWait()){
	    		playThread.setPleaseWait(false);
	            synchronized(playThread){
	            	playThread.notify();
	            }
	        }else{
	        	playThread.setPleaseWait(true);
	        }
	    	if (sliderRunner!=null){
	    		if (sliderRunner.pleaseWait){
	    			sliderRunner.pleaseWait = false;
	                synchronized(sliderRunner){
	                	sliderRunner.notify();
	                }
	            }else{
	            	sliderRunner.pleaseWait = true;
	            }
	    	}
    	}
    }

    /**
     * Stopper avspilling
     * @param e
     */
    private void stopButtonActionPerformed(ActionEvent e) {
    	if (playThread!=null){
	    	playThread.stopRunning();
	    	playThread=null;
	        dataTable.getSelectionModel().setLeadSelectionIndex(0);
	        dataTable.scrollRectToVisible(new Rectangle(0,0,10,10));
	        playSlider.setValue(0);
	        if (sliderRunner!=null)
	            sliderRunner.stopRunning();
	        sliderRunner = null;
    	}
    }
  
    /**
     * 
     * @param e
     */
    private void lukkButtonActionPerformed(ActionEvent e) {
        close();
        Component parent = this;
        while (!(parent instanceof Window)){
            parent = parent.getParent();
        }
        ((Window)parent).dispose();
    }
    
    private void followCheckBoxActionPerformed(ActionEvent e) {
        if (playThread!=null){
            if (followPointCheck.isSelected()){
            	playThread.setFollowPoint(true);                
            }else
            	playThread.setFollowPoint(false);
        }
    }
    
    private void playSliderStateChanged(ChangeEvent e) {
    	int value = playSlider.getValue();
        if (sliderRunner!=null){
            sliderRunner.setCurrentTime(value);
        }
    }
   
    
    /**
     * LayerEvent 
     * @param e
     */
    public void layerChanged(LayerEvent e) {
    	if (playThread==null||!playThread.isAlive()){
	        if (e.getSource()!=null&&e.getType().equals(LayerEvent.TYPE_SELECTED)){
	            if (mapPanel.getLayerHandler().getCurrentSelected() instanceof PointLayer){
	                initPlayer((PointLayer)mapPanel.getLayerHandler().getCurrentSelected());
	            }
	        }if (e.getSource()!=null&&e.getType().equals(LayerEvent.TYPE_UNSELECTED)){
	            clearPlayer();
	        }
    	}
    }
    /**
     * Kalles når man ønsker å lukke panelet, avslutter avspillingstråden og fjerner denne klassen som
     * lytter til layerhandleren
     */
    public void close(){
        if (playThread!=null){
        	playThread.stopRunning();
        	for (CollectionEntity l:playThread.getPunktData().getLayers())
                l.setVisible(true);
        	playThread=null;
        }
        if (sliderRunner!=null){
            sliderRunner.stopRunning();
            sliderRunner = null;
        }
        mapPanel.getLayerHandler().removeLayerListener(this);
        mapPanel.getLayerHandler().unselectAll();
    }
    
    /**
     * Initialiserer avspilleren
     */
    private void initPlayer(PointLayer currentSelected){
    
        int index = currentSelected.getParent().getIndexOfLayer(currentSelected);
        playData = ((ProdpunktPointLayer)mapPanel.getLayerHandler().getCurrentSelected()).getParent();
        mapPanel.getLayerHandler().unselectAll();
        first = (ProdpunktPointLayer)playData.getLayers().get(0);
        ProdpunktPointLayer last = (ProdpunktPointLayer)playData.getLayers().get(playData.getLayers().size()-1);
        
        SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
        log.debug("First geoposition: {}", first.getProdpunktGeoPosition());
        log.debug("First time: {}", first.getProdpunktGeoPosition().getProdpunktVO().getGmtTidspunkt());
        String start = f.format(first.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt());
        String stop = f.format(last.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt());
        
        header.setTitle(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.header.title")+first.getProdpunktGeoPosition().getProdpunktVO().getDfuId()+" "+start+"-"+stop);
        
        int size = playData.getLayers().size();
        playSlider.setMaximum(size-1);
        playSlider.setValue(index);
        Hashtable table = new Hashtable();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        int times = size/5;
        for (int i=0;i<size;i++){
            if (i%times==0||i==0||i==size-1){
                Calendar c = Calendar.getInstance();
                c.setTime(((ProdpunktPointLayer)playData.getLayers().get(i)).getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt());//  XMLCalendarConvert.convertCalendar(((GpsPointLayer)playData.getLayers().get(i)).getGpsGeoPosition().getGpsPoint().getData().getTime());
                String tid = format.format(c.getTime());
                table.put(new Integer(i+1), new JLabel(tid, JLabel.CENTER));
            }
        }
        playSlider.setLabelTable(table);
        playSlider.setPaintTicks(true);
        playSlider.setPaintLabels(true);
        
        //resentrer kartet rundt det første punktet og marker start.
        //mapPanel.setAddressLocation(first.getGpsGeoPosition().getPosition());
       
        mapPanel.getLayerHandler().addLayer(MipssMapLayerType.SELECTED.getType(), currentSelected.getSelectionLayer(), true);
        
        playModel = new PlayDataTableModel(playData.getLayers());
        dataTable.setModel(playModel);
        dataTable.setRowSelectionInterval(index, index);
        int selected = dataTable.getSelectionModel().getLeadSelectionIndex();
        moveTable(Math.abs(selected-index)*10, selected, index);
        
        timesynchronizer = new AnimationDates(first.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt(), last.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt());
    }
    /**
     * Fjerner innholdet til tabellen og slideren. 
     */
    private void clearPlayer(){
        //only clear if player is null or not running
        if (playThread==null||!playThread.isAlive()){
            playModel = new PlayDataTableModel(new ArrayList<CollectionEntity>());
            dataTable.setModel(playModel);
            playSlider.setLabelTable(null);
            playSlider.setValue(0);
            header.setTitle(ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.header.title")+ResourceUtil.getResource("mipssmap.swing.PlaybackPanel.header.title2"));
        }
    }
    
    public void moveTable(int time, int fraIndex, int tilIndex){
        PropertySetter ps = new PropertySetter(dataTable.getSelectionModel(), "leadSelectionIndex", new Integer[]{new Integer(fraIndex), new Integer(tilIndex)});
        Animator a = new Animator(time, ps);
        a.addTarget(new TimingTargetAdapter() {
                public void timingEvent(float fraction) {
                  dataTable.scrollRectToVisible(
                    dataTable.getCellRect(
                      dataTable.getSelectionModel().getLeadSelectionIndex(),
                      -1, true));
                }});
        a.start();
        dataTable.getVisibleRect();
    }
}
