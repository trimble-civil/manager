package no.mesta.mipss.mipssmap.plugins.produksjon.produksjon;

import java.awt.Window;

import javax.swing.JDialog;

import no.mesta.mipss.mipssmap.MipssMapModule;

/**
 * Dialog som presenterer en gui for å vise frem produksjon
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
 
public class ProduksjonDialog extends JDialog{
    private ProduksjonPanelNew produksjonPanel;
    /**
     * Konstruktør
     * @param owner blir satt som denne dialogens parent
     * @param mipssMapController controller for MipssMap
     * @param fraDato 
     * @param tilDato
     * @param punktCheck
     * @param strekningCheck
     */
    public ProduksjonDialog(Window owner, 
                            ProduksjonPlugin plugin,
                            MipssMapModule module) {
        super(owner);
        initGUI(plugin, module);
    }
    
    private void initGUI(ProduksjonPlugin plugin, MipssMapModule module) {
    	produksjonPanel = new ProduksjonPanelNew(module, plugin);
    	add(produksjonPanel);
    }
    public ProduksjonPanelNew getProduksjonPanel(){
    	return produksjonPanel;
    }
}
