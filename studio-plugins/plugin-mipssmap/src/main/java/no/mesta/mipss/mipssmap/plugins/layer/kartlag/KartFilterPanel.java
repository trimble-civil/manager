package no.mesta.mipss.mipssmap.plugins.layer.kartlag;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.ColorChooserPanel;

import org.jdesktop.swingx.JXHeader;

/**
 * Panel for å vise frem en fargevelger for å endre fargen på kontraktveinettet
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
class KartFilterPanel extends JPanel{
    
    private JLabel blandingsverdiLabel;
    private JSlider mixValue;
    private ColorChooserPanel colorChooserPanel;
    
    private JPanel buttonPanel;
    //private JButton brukButton;
    private JButton okButton;
    private JButton avbrytButton;
    
    
    private Color valgtFarge;
    private Color forrigeFarge;
    private float mix=0.5f;
    private float forrigeMix;
    
    private JPanel c;
    
    private EventListenerList listenerList = new EventListenerList();
    protected transient ChangeEvent changeEvent = null;
    
    public KartFilterPanel(JDialog parent, Color forrigeFarge, float forrigeMix) {        
        if (forrigeFarge==null)
            forrigeFarge = Color.white;
            
        this.forrigeFarge = forrigeFarge;
        this.forrigeMix = forrigeMix;
        this.valgtFarge = forrigeFarge;
        this.mix = forrigeMix;
        
        initComponents();
        setLayout(new BorderLayout());
        add(getHeader(), BorderLayout.NORTH);
        add(new JPanel(), BorderLayout.WEST);
        add(c);
        
    }
    
    private JXHeader getHeader(){
        JXHeader header = new JXHeader();
        header.setPreferredSize(new Dimension(300, 100));
        header.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartFilterPanel.header.title"));
        header.setDescription(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartFilterPanel.header.description"));
        header.setIcon(IconResources.ICON_FARGE);
        return header;
    }
    
    private void initComponents() {
    	c = new JPanel();
    	
        colorChooserPanel = new ColorChooserPanel();
        blandingsverdiLabel = new javax.swing.JLabel();
        mixValue = new javax.swing.JSlider();
        buttonPanel = new javax.swing.JPanel();
        //brukButton = new javax.swing.JButton();
        avbrytButton = new javax.swing.JButton();
        okButton = new javax.swing.JButton();

        javax.swing.GroupLayout colorChooserPanelLayout = new javax.swing.GroupLayout(colorChooserPanel);
        colorChooserPanel.setLayout(colorChooserPanelLayout);
        colorChooserPanelLayout.setHorizontalGroup(
            colorChooserPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 238, Short.MAX_VALUE)
        );
        colorChooserPanelLayout.setVerticalGroup(
            colorChooserPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 171, Short.MAX_VALUE)
        );
        JColorChooser chooser = new JColorChooser();
        AbstractColorChooserPanel[] panels = {colorChooserPanel};
        chooser.setChooserPanels(panels);
        
        colorChooserPanel.getColorSelectionModel().addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e) {
                DefaultColorSelectionModel colorModel = (DefaultColorSelectionModel)e.getSource();
                valgtFarge = colorModel.getSelectedColor();
            }
        });
        colorChooserPanel.setCurrentColor(forrigeFarge);
        
        blandingsverdiLabel.setText(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartFilterPanel.blandingsverdilabel.text"));

        mixValue.setMajorTickSpacing(10);
        mixValue.setMinorTickSpacing(10);
        mixValue.setPaintLabels(true);
        mixValue.setPaintTicks(true);
        mixValue.setToolTipText(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartFilterPanel.blandingsverdislider.tooltip"));
        mixValue.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e){
                JSlider slider = (JSlider)e.getSource();
                mix=((float)slider.getValue()/100);
            }
        });
        mixValue.setValue((int)(forrigeMix*100));
        
        
//        brukButton.setText(properties.getString("brukButton.text"));
//        brukButton.setPreferredSize(new java.awt.Dimension(65, 23));
//        brukButton.addActionListener(new ActionListener(){
//                public void actionPerformed(ActionEvent e){
//                        brukButtonAction(e);
//                }
//        });
        
        avbrytButton.setText(ResourceUtil.getResource("avbryt.text"));
        avbrytButton.setPreferredSize(new java.awt.Dimension(65, 23));
        avbrytButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        avbrytButtonAction(e);
                }
        });
        
        okButton.setText(ResourceUtil.getResource("ok.text"));
        okButton.setPreferredSize(new java.awt.Dimension(65, 23));
        okButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        okButtonAction(e);
                }
        });

        javax.swing.GroupLayout buttonPanelLayout = new javax.swing.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(33, Short.MAX_VALUE)
                .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(avbrytButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
//                .addComponent(brukButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
//                    .addComponent(brukButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(avbrytButton)
                    .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(c);
        c.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(colorChooserPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(blandingsverdiLabel)
                    .addComponent(mixValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(colorChooserPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(blandingsverdiLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mixValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>

    
    
    /**
     * Legg til en ny lytter
     * @param l
     */
    public void addChangeListener(ChangeListener l) {
        listenerList.add(ChangeListener.class, l);
    }
    
    /**
     * Fjern en lytter fra listen
     * @param l
     */
    public void removeChangeListener(ChangeListener l) {
        listenerList.remove(ChangeListener.class, l);
    }
    
    /**
     * Gi beskjed til alle lyttere om at ny farge er valgt
     */
    protected void fireStateChanged(){
        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -=2 ) {
            if (listeners[i] == ChangeListener.class) {
                if (changeEvent == null) {
                    changeEvent = new ChangeEvent(this);
                }
                ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
            }
        }
    }
    
    /**
     * Action-metode for avbryt-knappen
     * @param e
     */
    private void avbrytButtonAction(ActionEvent e){
        valgtFarge = forrigeFarge;
        mix = forrigeMix;
        Container parent = this;
        while (!(parent instanceof JDialog)){
            parent = parent.getParent();
        }
        fireStateChanged();
        ((JDialog)parent).setVisible(false);
    }
    
    /**
     * Action-metode for ok-knappen
     * @param e
     */
    private void okButtonAction(ActionEvent e){
        Container parent = this;
        while (!(parent instanceof JDialog)){
            parent = parent.getParent();
        }
        
        fireStateChanged();
        
        /*
        final Container p = parent;
            new Thread(){
                public void run(){
                    boolean visible = false;
                    while (true){
                        try{
                            Thread.sleep(1000);    
                            ((JDialog)p).setVisible(visible);
                            visible = !visible;
                        } catch (Exception e){}
                    }
                }
            }.start();
        */
        
        ((JDialog)parent).setVisible(false);
    }
    
    /**
     * Action-metode for bruk-knappen
     * @param e
     */
    private void brukButtonAction(ActionEvent e){
        forrigeMix = mix;
        forrigeFarge = valgtFarge;
        fireStateChanged();
    }
    /**
     * Returnerer valgt farge
     * @return
     */
    public Color getValgtFarge() {
        return valgtFarge;
    }
    /**
     * Returnerer blandingsverdien
     * @return
     */
    public float getMix() {
        return mix;
    }
}
