package no.mesta.mipss.mipssmap;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.messages.MipssMapFriksjonMessage;
import no.mesta.mipss.messages.MipssMapFriksjonStrekningMessage;
import no.mesta.mipss.messages.MipssMapOpenMipssFieldItem;
import no.mesta.mipss.messages.MipssMapPosKjoretoyMessage;
import no.mesta.mipss.mipssfelt.InspeksjonQueryParams;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonCriteria;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin;
import no.mesta.mipss.mipssmap.task.HentFriksjonTask;
import no.mesta.mipss.mipssmap.task.HentInspeksjonTask;
import no.mesta.mipss.mipssmap.utils.ErrorHandler;
import no.mesta.mipss.mipssmap.utils.ProdpunktLabelCreator;
import no.mesta.mipss.mipssmap.utils.ProduksjonOptions;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.mipssmapserver.KjoretoyDTO;
import no.mesta.mipss.mipssmapserver.KjoretoyVO;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.plugin.PluginMessage;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;
import no.mesta.mipss.webservice.NvdbWebservice;
import no.mesta.mipss.webservice.ReflinkStrekning;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Håndterer meldinger som kommer inn til modulen
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class MessageHandler {
	
	private final MipssMapModule plugin;
	private ProdpunktPointLayer layer;

	public MessageHandler(MipssMapModule plugin){
		this.plugin = plugin;
		
	}
	public void handleMessage(PluginMessage<?, ?> message){
		if (message instanceof MipssMapFriksjonMessage){
			handleFriksjonsMessage((MipssMapFriksjonMessage)message);
		}else if (message instanceof MipssMapPosKjoretoyMessage){
			handlePosKjoretoyMessage((MipssMapPosKjoretoyMessage) message);
		}else if (message instanceof MipssMapOpenMipssFieldItem){
			handleMipssFieldMessage((MipssMapOpenMipssFieldItem)message);
		}else if (message instanceof MipssMapFriksjonStrekningMessage){
			handleFriksjonStrekningMessage((MipssMapFriksjonStrekningMessage)message);
		} else if (message instanceof MipssMapTripMessage) {
			handleTripMessage((MipssMapTripMessage) message);
		}
		
	}
	/**
	 * Håndter meldinger angående mipss-felt
	 * @param message
	 */
	private void handleMipssFieldMessage(MipssMapOpenMipssFieldItem message) {
		if (!message.getItems().isEmpty()){
			Bounds b = new Bounds();
			for (MipssFieldDetailItem item :message.getItems()){
				Layer l = null;
				LayerType type = null;
				if(item instanceof Avvik){
					Avvik avvik = (Avvik) item;
					Punktstedfest p = avvik.getStedfesting(); 
					if (p==null){
						System.err.println("[ERROR] MipssMap.MessageHandler:Fant avvik uten punktstedfest refnummer:"+avvik.getRefnummer()+" guid:"+avvik.getGuid());
						continue;
					}
					if (p.getSnappetX()==null||p.getSnappetY()==null){
						System.err.println("[WARN] MipssMap.MessageHandler: Fant avvik som ikke er snappet, refnummer:"+avvik.getRefnummer()+" guid:"+avvik.getGuid());
					}
					if (p.getX()==null&&p.getY()==null){
						p.setX(p.getSnappetX());
						p.setY(p.getSnappetY());
					}
						
					
					if (p.getX()!=null&&p.getY()!=null){
						l = openAvvik(avvik);
						b.addBounds(l.getBounds());
						type = MipssMapLayerType.FUNN.getType();
					}
				}
				if(item instanceof Hendelse){
					Hendelse hendelse = (Hendelse) item;
					Punktstedfest p = hendelse.getSak().getStedfesting().get(0); 
					if (p.getX()!=null&&p.getY()!=null){
						l = openHendelse(hendelse);
						b.addBounds(l.getBounds());
						type = MipssMapLayerType.HENDELSE.getType();
					}
				}
				if (item instanceof Skred){
					Skred skred = (Skred) item;
					Punktstedfest p = skred.getSak().getStedfesting().get(0); 
					if (p.getX()!=null&&p.getY()!=null){
						l = openSkred(skred);
						b.addBounds(l.getBounds());
						type = MipssMapLayerType.SKRED.getType();
					}
				}
				if (item instanceof Forsikringsskade){
					Forsikringsskade forsikringsskade = (Forsikringsskade) item;
					Punktstedfest p = forsikringsskade.getSak().getStedfesting().get(0); 
					if (p.getX()!=null&&p.getY()!=null){
						l = openForsikringsskade(forsikringsskade);
						b.addBounds(l.getBounds());
						type = MipssMapLayerType.FORSIKRINGSSKADE.getType();
					}
				}
				if (item instanceof InspeksjonSokResult){
					openInspeksjon((InspeksjonSokResult) item);
				}

				if (l!=null&&type!=null)
						plugin.getMainMap().getLayerHandler().addLayer(type, l, false);
			}
			waitForMapAndZoom(b);
		}
		
	}
	
	/**
	 * Henter et avvik og tegner det i kartet
	 * @param avvik
	 */
	private Layer openAvvik(Avvik avvik){
		LayerBuilder b = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
		Layer l = b.getPinLayerFromAvvik(avvik, new Bounds());
		return l;
	}
	
	/**
	 * Henter en inspeksjon og tegner det i kartet 
	 * @param inspeksjon
	 */
	private void openInspeksjon(InspeksjonSokResult inspeksjon){
		InspeksjonQueryParams params = new InspeksjonQueryParams();
		params.setHentStrekninger(true);
		final HentInspeksjonTask task = new HentInspeksjonTask(plugin, params, inspeksjon, null);
		ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.plugins.produksjon.inspeksjon.InspeksjonPlugin.HentInspeksjonTask.title"));
        workerPanel.setWorker(task);
        task.setProgressController(plugin.getProgressController());
        task.setProgressWorkerPanel(workerPanel);
        plugin.getProgressController().addWorkerPanel(workerPanel);
        
        Thread wait = new Thread("Hent inspeksjon(guid="+inspeksjon.getGuid()+")"){
        	public void run(){
        		Layer layer = null;
        		try{
        			layer =task.get();
        			if (layer!=null){
        				plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONSTREKNING.getType(), layer, true);
            			plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONLABEL.getType(), task.getLabel(), false);
            			plugin.getMainMap().calculateZoomForAllLayers(true);
        			}
        		} catch (ExecutionException e) {
                    e.printStackTrace();
                    String msg = ResourceUtil.getResource("mipssmap.feilmelding");
                    ErrorHandler.showError(e, msg);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } 
        	}
        };
        wait.start();
	}
	/**
	 * Henter en hendelse og tegner det i kartet
	 * @param hendelse
	 */
	private Layer openHendelse(Hendelse hendelse){
		LayerBuilder b = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
		Layer l = b.getHendelseLayer(hendelse, new Bounds());
		return l;
	}
	/**
	 * Henter et skred og tegner det i kartet
	 * @param skred
	 */
	private Layer openSkred(Skred skred){
		LayerBuilder b = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
		Layer l = b.getSkredLayer(skred, new Bounds());
		return l;
	}
	/**
	 * Henter en forsikringsskade og tegner det i kartet
	 * @param fs
	 */
	private Layer openForsikringsskade(Forsikringsskade fs){
		LayerBuilder b = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
		Layer l = b.getForsikringsskadeLayer(fs, new Bounds());
		return l;
	}
	
	private void handleFriksjonsMessage(MipssMapFriksjonMessage message){
		ProduksjonService bean = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		List<Friksjonsdetalj> friksjonList = message.getFriksjonList();

		LayerBuilder b = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
		ProdpunktPointLayer layer=null;
		for (Friksjonsdetalj f:friksjonList){
			Long dfuId = f.getDfuId();
			Date gmtTidspunkt = f.getGmtTidspunkt();
			ProdpunktVO prodpunkt = null;
			String leverandor = "";
			try{
				prodpunkt =  getGeometryForFriksjon(f);//bean.getProdpunkt(dfuId, gmtTidspunkt);
				leverandor = bean.getLeverandorNavnDfu(dfuId, gmtTidspunkt, gmtTidspunkt);
			} catch (Exception e){
				e.printStackTrace();
			}
			if (prodpunkt!=null){
				layer = b.getPointLayerFromFriksjonsdetalj(f, prodpunkt);
				layer.setPreferredLayerType(MipssMapLayerType.FRIKSJONPUNKT.getType());
				
				Layer label = null;
				String labelTitle = "";
				if (f.getRegnr()==null){
					labelTitle = "Feil! mangler";
				}else{
					labelTitle = f.getRegnr();
				}
				ProdpunktLabelCreator pc = new ProdpunktLabelCreator();
				String[] labelText = new String[]{labelTitle, MipssDateFormatter.formatDate(MipssDateFormatter.convertFromGMTtoCET(gmtTidspunkt), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT)};
				String labelId = dfuId+"";
				
				if (leverandor!=null&&leverandor.toLowerCase().indexOf("mesta")!=-1){
					label = pc.getMestabilLabel(labelText, layer.getPoint(), labelId);
				}else if(leverandor!=null){
					label = pc.getLabelUE(labelText, layer.getPoint(), labelId);
				}else if (leverandor==null){
					label = pc.getLabelPDA(labelText, layer.getPoint(), labelId);
				}
				
				plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.FRIKSJONPUNKT.getType(), layer, false);
				plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), label, false);
				waitForMapAndZoom(layer.getBounds());
			}
		}
		plugin.getMainMap().getLayerHandler().changeLayer(layer);
		
	}
	private void handleFriksjonStrekningMessage(MipssMapFriksjonStrekningMessage message){
		List<Prodstrekning> strekninger = message.getProdstrekningList();
		for (Prodstrekning str : strekninger){
			final HentFriksjonTask task = new HentFriksjonTask(plugin, str);
			ProgressWorkerPanel workerPanel = new ProgressWorkerPanel(ResourceUtil.getResource("mipssmap.HentFriksonTask.title"));
	        workerPanel.setWorker(task);
	        task.setProgressController(plugin.getProgressController());
	        task.setProgressWorkerPanel(workerPanel);
	        plugin.getProgressController().addWorkerPanel(workerPanel);
	        
	        Thread wait = new Thread("Hent friksjonsdata()"){
	        	public void run(){
	        		Layer layer = null;
	        		try{
	        			layer =task.get();
	        			if (layer!=null){
	        				plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.FRIKSJONPUNKT.getType(), layer, true);
	        				plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), task.getLabelLayer(), false);
//	        				plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONSTREKNING.getType(), layer, true);
//	            			plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.INSPEKSJONLABEL.getType(), task.getLabel(), false);
//	            			plugin.getMainMap().calculateZoomForAllLayers(true);
	        			}
	        		} catch (ExecutionException e) {
	                    e.printStackTrace();
	                    String msg = ResourceUtil.getResource("mipssmap.feilmelding");
	                    ErrorHandler.showError(e, msg);
	                } catch (InterruptedException e) {
	                    e.printStackTrace();
	                } 
	        	}
	        };
	        wait.start();
			
		}
	}

	private void handleTripMessage(MipssMapTripMessage message) {
        Trip trip = message.getTrip();

		ProduksjonPlugin prodPlugin = new ProduksjonPlugin(plugin.getWindowManager(), plugin);
		prodPlugin.setProduksjonOptions(createProductionOptionsFromTrip(trip));

		ProduksjonCriteria criteria = new ProduksjonCriteria();
        criteria.setAllProduksjon(true);
        criteria.setProdtyper(createProdTypeList());
        criteria.setGreenVisible(true);
        criteria.setRedVisible(true);
        criteria.setYellowVisible(true);
        criteria.setPunkter(true);
        criteria.setKunSiste(false);
        criteria.setStrekninger(false);
        criteria.setLabels(true);
        criteria.setEnhetList(createVehicleListFromTripVehicle(trip.getVehicle()));
		criteria.setPdop(false);
        criteria.setDfuId(null);
		criteria.setGrunndata(false);

		prodPlugin.hentProduksjon(criteria);
	}

	private List<KjoretoyVO> createVehicleListFromTripVehicle(Kjoretoy tripVehicle) {
		KjoretoyDTO vehicle = new KjoretoyDTO();
		vehicle.setId(tripVehicle.getId());
		vehicle.setNavn(tripVehicle.getEksterntNavn());

		List<KjoretoyVO> vehicleList = new ArrayList<>();
		vehicleList.add(new KjoretoyVO(vehicle));

		return vehicleList;
	}

	private List<Prodtype> createProdTypeList() {
        ProduksjonService service = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
        List<Prodtype> prodTypeList = new ArrayList<>();
        prodTypeList.add(service.getSingleProdType(98L));

        return prodTypeList;
    }

	private ProduksjonOptions createProductionOptionsFromTrip(Trip trip) {
		JXDatePicker fromDate = new JXDatePicker(trip.getStartTime());
		JSpinner fromTime = new javax.swing.JSpinner(new SpinnerDateModel(trip.getStartTime(), null, null, Calendar.MINUTE));
		fromTime.setEditor(new JSpinner.DateEditor(fromTime, MipssDateFormatter.TIME_FORMAT));
		MipssDateTimePickerHolder fromDateTimePicker = new MipssDateTimePickerHolder(fromDate, fromTime);

		JXDatePicker toDate = new JXDatePicker(trip.getEndTime());
		JSpinner toTime = new javax.swing.JSpinner(new SpinnerDateModel(trip.getEndTime(), null, null, Calendar.MINUTE));
		toTime.setEditor(new JSpinner.DateEditor(toTime, MipssDateFormatter.TIME_FORMAT));
		MipssDateTimePickerHolder toDateTimePicker = new MipssDateTimePickerHolder(toDate, toTime);

		ProduksjonOptions options = new ProduksjonOptions();
		options.setDatoFra(fromDateTimePicker);
		options.setDatoTil(toDateTimePicker);
		options.setNatidCheck(new JCheckBox("", false));

		return options;
	}
	
	private ProdpunktVO getGeometryForFriksjon(Friksjonsdetalj f){
		NvdbWebservice nvdb = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
		
		ProdpunktVO prod = new ProdpunktVO();
		prod.setGmtTidspunkt(f.getGmtTidspunkt());
		prod.setDfuId(f.getDfuId());
		
		Veinettveireferanse v = new Veinettveireferanse();
		v.setFraKm(f.getKm());
		v.setFylkesnummer(f.getFylkesnummer());
		v.setHp(f.getHp());
		v.setKommunenummer(f.getKommunenummer());
		v.setTilKm(f.getKm());
		v.setVeikategori(f.getVeikategori());
		v.setVeinummer(f.getVeinummer());
		v.setVeistatus(f.getVeistatus());
		List<Veinettreflinkseksjon> reflinkSectionsWithinRoadref = nvdb.getReflinkSectionsWithinRoadref(v);
		List<ReflinkStrekning> geo = nvdb.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(reflinkSectionsWithinRoadref);
		if (geo!=null&&geo.size()>0){
			Point[] p = geo.get(0).getVectors();
			if (p!=null && p.length>0){
				prod.setSnappetX(p[0].getX());
				prod.setSnappetY(p[0].getY());
			}
		}
		return prod;
	}
	
	private void handlePosKjoretoyMessage(MipssMapPosKjoretoyMessage message){
		ProduksjonService bean = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		ProduksjonQueryParams p = new ProduksjonQueryParams();
		p.setKjoretoyId(message.getKjoretoyId());
		p.setFraDato(message.getGmtTidspunkt());
		p.setTilDato(message.getGmtTidspunkt());
		p.setAllProduksjon(true);
		ProdpunktVO prodpunkt = bean.getSisteProduksjonPunktKjoretoy(p);
		System.out.println(message.getKjoretoyId()+" "+message.getGmtTidspunkt());
		if (prodpunkt!=null){
			LayerBuilder b = new LayerBuilder(plugin.getLoader(), plugin.getMainMap());
			layer = b.getPointLayerFromProdpunktVO(prodpunkt, new Bounds(), message.getGmtTidspunkt(), message.getGmtTidspunkt());
			layer.setInitGraphics(true);
			layer.setPreferredLayerType(MipssMapLayerType.PRODUKSJONPUNKT.getType());
			
			
			ProdpunktLabelCreator pc = new ProdpunktLabelCreator();
			String date = MipssDateFormatter.formatDate(message.getGmtTidspunkt(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
			
			
			String[] labelText = new String[]{message.getEksterntNavn(), date};
			String labelId = message.getKjoretoyId()+"";
			Layer label = null;
			if (message.getLeverandor().toLowerCase().indexOf("mesta")!=-1){
				label = pc.getMestabilLabel(labelText, layer.getPoint(), labelId);
			}else{
				label = pc.getLabelUE(labelText, layer.getPoint(), labelId);
			}
			
			plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONLABEL.getType(), label, false);
			plugin.getMainMap().getLayerHandler().addLayer(MipssMapLayerType.PRODUKSJONPUNKT.getType(), layer, true);
			waitForMapAndZoom(layer.getBounds());
			
		}
	}
	private void waitForMapAndZoom(final Bounds bounds){
		new Thread(){
			private int c = 0;
			public void run(){
				while (plugin.getMainMap().getViewportBounds().getWidth()==0){
					Thread.yield();
					c++;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (c>100)
						break;
				}
				plugin.getMainMap().ensureVisibilityForBounds(bounds);
			}
		}.start();
	}
	
}

