package no.mesta.mipss.mipssmap.utils;

import java.util.Calendar;

import javax.xml.datatype.XMLGregorianCalendar;

public class XMLCalendarConvert {
    /**
     * Konverter fra en XMLGregorianCalendar til en Calendar. 
     * @param c
     * @return
     */
    public static Calendar convertCalendar(XMLGregorianCalendar c){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, c.getYear());
        cal.set(Calendar.MONTH, c.getMonth()-1);
        cal.set(Calendar.DAY_OF_MONTH, c.getDay());
        cal.set(Calendar.HOUR_OF_DAY, c.getHour());
        cal.set(Calendar.MINUTE, c.getMinute());
        cal.set(Calendar.SECOND, c.getSecond());
        return cal;
    }
}
