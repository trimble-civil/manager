package no.mesta.mipss.mipssmap.task;

import javax.swing.SwingWorker;

import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;
import no.mesta.mipss.ui.progressbar.MipssProgressBarTask;
import no.mesta.mipss.ui.progressbar.ProgressController;
import no.mesta.mipss.ui.progressbar.ProgressWorkerPanel;

public abstract class MipssMapTask extends SwingWorker<Layer, Integer> implements MipssProgressBarTask<Layer, Integer> {
	private ProgressController progressController;
	private ProgressWorkerPanel progressWorkerPanel;
	
	private ExecutorTaskType taskType;
	private boolean cancelled = false;
	public MipssMapTask(ExecutorTaskType taskType){
		this.taskType = taskType;
	}
	/* (non-Javadoc)
	 * @see no.mesta.mipss.mipssmap.task.MipssProgressBarTask#setProgressController(no.mesta.mipss.mipssmap.swing.progressbar.ProgressController)
	 */
	public void setProgressController(ProgressController progressController){
		this.progressController = progressController;
	}
	/* (non-Javadoc)
	 * @see no.mesta.mipss.mipssmap.task.MipssProgressBarTask#setProgressWorkerPanel(no.mesta.mipss.mipssmap.swing.progressbar.ProgressWorkerPanel)
	 */
	public void setProgressWorkerPanel(ProgressWorkerPanel progressWorkerPanel){
		this.progressWorkerPanel = progressWorkerPanel;
	}
	protected ProgressController getProgressController(){
		return progressController;
	}
	protected ProgressWorkerPanel getProgressWorkerPanel(){
		return progressWorkerPanel;
	}
	
	protected void done(){
		progressWorkerPanel.setBarIndeterminate(false);
		progressController.removeWorkerPanel(progressWorkerPanel);
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.mipssmap.task.MipssProgressBarTask#cancelTask()
	 */
	public void cancelTask(){
		this.cancelled = true;
	}
	/* (non-Javadoc)
	 * @see no.mesta.mipss.mipssmap.task.MipssProgressBarTask#isTaskCancelled()
	 */
	public boolean isTaskCancelled(){
		return cancelled;
	}
	/* (non-Javadoc)
	 * @see no.mesta.mipss.mipssmap.task.MipssProgressBarTask#getTaskType()
	 */
	public ExecutorTaskType getTaskType(){
		return taskType;
	}
	
	@Override
	public SwingWorker getWorker() {
		return this;
	}
}
