package no.mesta.mipss.mipssmap;

import java.awt.Color;

import javax.swing.JCheckBox;
import javax.swing.JProgressBar;

import no.mesta.mipss.contract.Contract;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LayerHandler;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mipssmap.swing.JStatusPanel;
import no.mesta.mipss.mipssmap.swing.MipssMapPanel;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Klassen innkapsler gis-webservicen og LayerHandleren og legger på/sletter/endrer lag i kartet
 * 
 * @author Harald A. Kulø
 */
public class MipssMapLayerController {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    private JStatusPanel statusPanel;
    
    private JCheckBox kontraktveiCheck;
    private JCheckBox rodeveiCheck;
    
    private JCheckBox produksjonCheck;
    
    
    private MipssMapModule plugin;
    
    
    /**
     * Konstruktør
     * @param layerHandler klassen som håndterer lagene i kartet
     * @param webservice facade til webservicen
     * @param statusPanel panelet som viser status til brukeren. 
     */
    public MipssMapLayerController(MipssMapModule plugin, LayerHandler layerHandler, Contract contractBean, 
                                   JStatusPanel statusPanel, 
                                   MipssMapPanel map) {
        this.plugin = plugin;
        this.statusPanel = statusPanel;
    }
//    public void addLayer(MipssMapLayerType type, Layer l){
//        plugin.getMainMap().getLayerHandler().addLayer(type.getType(), l, true);
//    }
//    
//    public void setLayerTypeVisible(boolean visible, MipssMapLayerType type){
//        if (visible){
//        	plugin.getMainMap().getLayerHandler().showLayerType(type.getType());
//        }else{
//        	plugin.getMainMap().getLayerHandler().hideLayerType(type.getType());
//        }
//    }
//    public boolean addVeinett(IRenderableMipssEntity entity, Color layerColor){
//        if (entity instanceof Rode){
//            return addRodeveinett((Rode)entity, layerColor);
//        }   
//        return false;
//    }
//    
//    public void removeVeinett(IRenderableMipssEntity entity){
//        if (entity instanceof Driftkontrakt){
//            removeKontraktveinett((Driftkontrakt)entity);
//        }
//        if (entity instanceof Rode){
//            
//        }
//    }
    
//    /**
//     * Legger til et rodeveinett
//     * @param rode
//     * @return true hvis veinettet lages, false hvis det er hentet fra før
//     */
//    private boolean addRodeveinett(Rode rode, Color layerColor){
//        int id = rode.getId().intValue();
//        //roder finnes ikke fra før, hent fra webservice og lag et nytt layer
//        if (!plugin.getMainMap().getLayerHandler().isLayerPresent(MipssMapLayerType.RODE.getType(), ""+id)){
//            createRodeVeinett(rode, layerColor);
//            return true;
//        }else{
//        	plugin.getMainMap().getLayerHandler().showLayer(MipssMapLayerType.RODE.getType(), ""+id);
//            return false;
//        }
//    }
    
//    /**
//     * Fjerner et kontraktveinett
//     * @param kontrakt
//     */
//    private void removeKontraktveinett(Driftkontrakt kontrakt){
//        int id = kontrakt.getId().intValue();
//        plugin.getMainMap().getLayerHandler().removeLayer(MipssMapLayerType.KONTRAKTVEI.getType(), ""+id);
//    }
//    
//    /**
//     * Lager et rodeveinett
//     * @param rode
//     */
//    private void createRodeVeinett(Rode rode, Color layerColor){
//        final JProgressBar bar = statusPanel.getProgressBar();
//        bar.setValue(0);
//        bar.setStringPainted(true);
//        bar.setMaximum(100);
//        bar.setString("Henter rodeveinett...");
//    }
    
//    /**
//     * Verifiser at laget inneholder data. Dette gjøres ved å sjekke at laget har
//     * en geografisk bredde og høyde som er større enn 0
//     * @param layer
//     * @return
//     */
//    private boolean verifyLayer(Layer layer){
//        Bounds layerBounds = layer.getBounds();
//        double width = layerBounds.getUrx() - layerBounds.getLlx();
//        double height = layerBounds.getUry() - layerBounds.getLly();
//        boolean rvl = width>0&&height>0;
//        return rvl;
//    }
    
//    /**
//     * just delegate
//     * @param id
//     */
//    public void hideLayer(LayerType type, String id){
//    	plugin.getMainMap().getLayerHandler().hideLayer(type, id);
//    }
//    /**
//     * just delegate
//     * @param id
//     */
//    public void hideLayerType(LayerType layerType){
//    	plugin.getMainMap().getLayerHandler().hideLayerType(layerType);
//    }
    /**
//     * Skjul et rodelag. 
//     * @param rode
//     */
//    public void hideRodeLayer(Rode rode){
//    	plugin.getMainMap().getLayerHandler().hideLayer(MipssMapLayerType.RODE.getType(), ""+rode.getId());
//    }
    
//    /**
//     * just delegate
//     * @param id
//     */
//    public void showLayer(LayerType type, String id){
//    	plugin.getMainMap().getLayerHandler().showLayer(type, id);
//    }
//    /**
//     * just delegate
//     * @param id
//     */
//    public void showLayerType(LayerType layerType){
//    	plugin.getMainMap().getLayerHandler().showLayerType(layerType);
//    }
//    /**
//     * just delegate
//     * @param id
//     * @return
//     */
//    public Layer getLayer(LayerType type, String id){
//        return plugin.getMainMap().getLayerHandler().getLayer(type, id);
//    }
    
//    /**
//     * just delegate
//     * @param layer
//     */
//    public void changeLayer(Layer layer){
//    	plugin.getMainMap().getLayerHandler().changeLayer(layer);
//    }
//    /**
//     * just delegate
//     */
//    public void removeAllLayers(){
//        uncheckAll();
//        plugin.getMainMap().getLayerHandler().removeAllLayers();
//    }
//    
//    /**
//     * 
//     * @param type
//     */
//    public void removeLayerType(LayerType type){
//    	plugin.getMainMap().getLayerHandler().removeLayerType(type); 
//    }
    
//    /**
//     * En liten hack for å sette checkboxen til kontrakt til <code>selected</code> når et
//     * kontraktveinett vises
//     * @param kontraktveiCheck
//     */
//    public void setKontraktveiCheck(JCheckBox kontraktveiCheck) {
//        this.kontraktveiCheck = kontraktveiCheck;
//    }
//    
//    public void setRodeveiCheck(JCheckBox rodeveiCheck) {
//        this.rodeveiCheck = rodeveiCheck;
//    }
//    
//    public void setProduksjonCheck(JCheckBox produksjonCheck){
//        this.produksjonCheck = produksjonCheck;
//    }
    
//    private void uncheckAll(){
//        if (kontraktveiCheck!=null)
//            kontraktveiCheck.setSelected(false);
//        if (rodeveiCheck!=null)
//            rodeveiCheck.setSelected(false);
//        if (produksjonCheck!=null)
//            produksjonCheck.setSelected(false);
//    }
//
//    public JStatusPanel getStatusPanel() {
//        return statusPanel;
//    }
}
