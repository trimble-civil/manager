package no.mesta.mipss.mipssmap.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.MipssMapLayerPlugin;
import no.mesta.mipss.mipssmap.plugins.MipssMapProductionPlugin;
import no.mesta.mipss.mipssmap.plugins.layer.kartlag.KartlagPlugin;
import no.mesta.mipss.mipssmap.plugins.layer.kontraktvei.KontraktveiPlugin;
import no.mesta.mipss.mipssmap.plugins.layer.nvdb.NvdbObjektPlugin;
import no.mesta.mipss.mipssmap.plugins.layer.omrade.OmradePlugin;
import no.mesta.mipss.mipssmap.plugins.layer.punkt.PunktPlugin;
import no.mesta.mipss.mipssmap.plugins.layer.rode.RodePlugin;
import no.mesta.mipss.mipssmap.plugins.layer.vaerstasjon.VaerstasjonPlugin;
import no.mesta.mipss.mipssmap.plugins.produksjon.friksjon.FriksjonPlugin;
import no.mesta.mipss.mipssmap.plugins.produksjon.funn.FunnPlugin;
import no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon.InspeksjonPlugin;
import no.mesta.mipss.mipssmap.plugins.produksjon.kjoretoy.KjoretoyPlugin;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;


/**
 * Hovedpanelet for valg av operasjoner mot kartklienten
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
public class OptionsPanel extends javax.swing.JPanel {
    private KontraktPanel velgKontraktPanel1;
    
    private MipssMapModule plugin;

	private final MipssMapWindowManager windowManager;
	
	private boolean brukSVVInnsyn;
    
    /**
     * Konstruktør
     * @param plugin
     * @param windowManager
     * @param brukSVVInnsyn
     */
    public OptionsPanel(MipssMapModule plugin, MipssMapWindowManager windowManager, boolean brukSVVInnsyn){
    	super();
        this.plugin = plugin;
		this.windowManager = windowManager;
    	this.brukSVVInnsyn = brukSVVInnsyn;
    	initGUI();
    }
    
    private void initGUI(){
//        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    	setLayout(new GridBagLayout());
        List<MipssMapLayerPlugin> layerPlugins = new ArrayList<MipssMapLayerPlugin>();
        KartlagPlugin kart = new KartlagPlugin(plugin, windowManager);
        KontraktveiPlugin kontraktveiPlugin = new KontraktveiPlugin(plugin, windowManager);
        RodePlugin rode = new RodePlugin(plugin, windowManager);
        OmradePlugin omrade = new OmradePlugin(windowManager, plugin);
        NvdbObjektPlugin nvdb = new NvdbObjektPlugin();
        VaerstasjonPlugin vaerstasjon = new VaerstasjonPlugin(windowManager, plugin);
        PunktPlugin punkt = new PunktPlugin();
        
        layerPlugins.add(kart);
        layerPlugins.add(kontraktveiPlugin);
        layerPlugins.add(rode);
        layerPlugins.add(omrade);
//        layerPlugins.add(nvdb);
//        layerPlugins.add(vaerstasjon);
//        layerPlugins.add(punkt);
        
        plugin.setLayerPlugins(layerPlugins);
        
        velgKontraktPanel1 = new KontraktPanel(plugin, kontraktveiPlugin, windowManager);
        velgKontraktPanel1.setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("mipssmap.swing.OptionsPanel.velgKontraktPanel1.border")));
        
        GrunndataPanel grunndataPanel = new GrunndataPanel(layerPlugins);
        
        
        grunndataPanel.setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("mipssmap.swing.OptionsPanel.grunndataPanel.border")));
        
        List<MipssMapProductionPlugin> produksjonPlugins = new ArrayList<MipssMapProductionPlugin>();
        ProduksjonPlugin produksjon = new ProduksjonPlugin(windowManager, plugin);
        InspeksjonPlugin inspeksjon = new InspeksjonPlugin(windowManager, plugin);
        
        KjoretoyPlugin kjoretoy = new KjoretoyPlugin();
        FriksjonPlugin friksjon = new FriksjonPlugin();
        
        FunnPlugin funn = new FunnPlugin(windowManager, plugin);
        produksjonPlugins.add(produksjon);
//        produksjonPlugins.add(kjoretoy);
//        produksjonPlugins.add(friksjon);
        produksjonPlugins.add(inspeksjon);
        produksjonPlugins.add(funn);
        plugin.setProduksjonPlugins(produksjonPlugins);
        
        ProduksjonPanel produksjonPanel = new ProduksjonPanel(plugin, produksjonPlugins, windowManager, brukSVVInnsyn);
        produksjon.setProduksjonOptions(produksjonPanel.getOptions());
        inspeksjon.setProduksjonOptions(produksjonPanel.getOptions());
        funn.setProduksjonOptions(produksjonPanel.getOptions());
        
        produksjonPanel.setBorder(BorderFactory.createTitledBorder(ResourceUtil.getResource("mipssmap.swing.OptionsPanel.produksjonPanel.border")));
        
        add(velgKontraktPanel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(grunndataPanel, 	new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(produksjonPanel, 	new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        
//        add(velgKontraktPanel1);
//        add(grunndataPanel);
//        add(produksjonPanel);
//        add(Box.createVerticalGlue());
        if (!brukSVVInnsyn) {
          BindingHelper.createbinding(plugin, "${currentKontrakt!=null}", grunndataPanel, "enabled").bind();
          BindingHelper.createbinding(plugin, "${currentKontrakt!=null}", produksjonPanel, "enabled").bind();
		}
    }
}
