package no.mesta.mipss.mipssmap.utils;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class ResourceUtil {

	private static PropertyResourceBundleUtil properties;
	private static final String BUNDLE_NAME = "mipssMapText";
	
	
	public static String getResource(String key){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key);
	}
	
	public static String getResource(String key, String... values){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key, values);
	}
}
