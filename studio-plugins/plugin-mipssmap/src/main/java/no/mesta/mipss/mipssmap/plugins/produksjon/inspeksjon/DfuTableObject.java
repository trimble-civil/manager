package no.mesta.mipss.mipssmap.plugins.produksjon.inspeksjon;

import java.util.List;

import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

public class DfuTableObject implements CheckBoxTableObject{

	private boolean valgt;
	private Dfuindivid dfuindivid;
	private Driftkontrakt currentkontrakt;
	
	public DfuTableObject(Dfuindivid dfuindivid, Driftkontrakt currentkontrakt){
		this.dfuindivid = dfuindivid;
		this.currentkontrakt = currentkontrakt;
	}
	
	
	public Long getEnhetId(){
		return dfuindivid.getId();
	}
	public String getType(){
		String type = "";
		if (dfuindivid!=null)
			type = dfuindivid.getDfumodell().getDfukategori().getNavn();
		
		if (type==null)
			type = "";
		return type;
	}
	public String getModell(){
		String modell = "";
		if (dfuindivid!=null){
			if (dfuindivid.getDfumodell()!=null)
				modell = dfuindivid.getDfumodell().getNavn();
		}
		if (modell==null)
			modell="";
		return modell;
	}
	
	public String getNavn(){
		String navn = "";
		List<DfuKontrakt> dfuKontraktList = dfuindivid.getDfuKontraktList();
		if (dfuKontraktList!=null){
			for (DfuKontrakt dk:dfuKontraktList){
				if (dk.getKontraktId().longValue()==currentkontrakt.getId().longValue()){
					navn = dk.getNavn();
				}
			}
		}
		return navn;
	}
	@Override
	public Boolean getValgt() {
		return valgt;
	}
	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
	}
	@Override
	public String getTextForGUI() {
		return "";
	}
	
	
}
