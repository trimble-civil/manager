package no.mesta.mipss.mipssmap.plugins.layer.kartlag;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.PropertyResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.mapplugin.utm.UTMProjectionTileFactory;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.swing.MipssMapWindowManager;
import no.mesta.mipss.mipssmap.swing.treetable.CheckButtonNode;
import no.mesta.mipss.mipssmap.swing.treetable.CheckTreeManager;
import no.mesta.mipss.mipssmap.swing.treetable.CheckTreeTableModel;
import no.mesta.mipss.mipssmap.swing.treetable.JCheckTreeTable;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.DefaultButtonPanel;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXHeader;


public class KartlagPanel extends JPanel{
	
	private MipssMapModule module;
	private KartlagPlugin plugin;
	private MipssMapWindowManager windowManager;
	private KartlagDialog dialog;
	private CheckTreeManager treeManager;
	private CheckButtonNode root = new CheckButtonNode("Root");
	private Binding bindCheckToNode;
	public KartlagPanel(MipssMapModule module, MipssMapWindowManager windowManager, KartlagPlugin plugin, KartlagDialog dialog){
		this.module = module;
		this.plugin = plugin;
		this.windowManager = windowManager;
		this.dialog = dialog;
//		bindCheckToNode = Bindings.createAutoBinding(UpdateStrategy.READ, plugin.getCheckButton().getCheckBox(), BeanProperty.create("selected"), root, BeanProperty.create("anyoneSelected"));
//		bindCheckToNode.bind();
		initGui();
	}
	
	private void initGui(){
		setLayout(new BorderLayout());
		
		JPanel center = new JPanel();
		center.setLayout(new BoxLayout(center, BoxLayout.PAGE_AXIS));
		center.add(getHeader());
		center.add(Box.createVerticalStrut(10));
		center.add(getOptionsPanel());
		center.add(Box.createVerticalGlue());
		add(center, BorderLayout.CENTER);
		add(getButtonPanel(), BorderLayout.SOUTH);
		
	}
	
	private JPanel getHeader(){
        JXHeader header = new JXHeader();
        header.setPreferredSize(new Dimension(310, 110));
        header.setMaximumSize(new Dimension(310, 110));
        header.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartlagPanel.header.title"));
        header.setDescription(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartlagPanel.header.description"));
        header.setIcon(IconResources.ICON_NORGE);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        panel.add(header);
        panel.add(Box.createHorizontalGlue());
        return panel;
    }
	
	private JPanel getOptionsPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		JScrollPane scroll = new JScrollPane(createTreeTable());
		scroll.setPreferredSize(new Dimension(310,200));
		panel.add(scroll);
		//Dimension size = new Dimension(200, 300);
		//panel.setPreferredSize(size);
		//panel.setMaximumSize(size);
		//panel.setMinimumSize(size);
		return panel;
	}
	
	private JCheckTreeTable createTreeTable(){
		CheckButtonNode fargeFilter = new CheckButtonNode(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartlagPanel.fargefilter.node.fargefilter.text"));
		JButton fargeButton = new JButton(new FargeFilterAction(ResourceUtil.getResource("valgButton.text"), null, fargeFilter));
		fargeFilter.setButton(fargeButton);
		fargeFilter.setDefaultFeilmelding(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartlagPanel.fargefilter.warning.missing"));
		
		root.add(fargeFilter);
		CheckTreeTableModel model = new CheckTreeTableModel(root);
		JCheckTreeTable tree = new JCheckTreeTable(model);
		model.setView(tree);
		treeManager = new CheckTreeManager(tree);
		tree.getColumn(1).setMaxWidth(100);
		tree.setSelectionBackground(Color.white);
		tree.setRowHeight(22);
		return tree;
	}
	
	private JPanel getButtonPanel(){
		return DefaultButtonPanel.getDefaultButtonPanel(new OkButtonAction("", null), new CancelButtonAction("", null), new ApplyButtonAction("", null));
//		JPanel panel = new JPanel();
//		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
//		JButton okButton = new JButton(new OkButtonAction("Ok", null));
//		JButton cancelButton = new JButton(new CancelButtonAction("Avbryt", null));
//		JButton applyButton = new JButton(new ApplyButtonAction("Bruk", null));
//		panel.add(Box.createHorizontalGlue());
//		panel.add(okButton);
//		panel.add(cancelButton);
//		panel.add(applyButton);
//		//panel.add(Box.createHorizontalStrut(10));
//		
//		return panel;
	}
	
	public void setAllSelected(boolean selected){
		root.setAllSelected(selected);
		if (!selected){
			new ApplyButtonAction(null, null).actionPerformed(null);
		}
	}
	
	public void restoreAllSelected() {
		root.restoreAllSelected();
		new ApplyButtonAction(null, null).actionPerformed(null);
	}
	/**
	 * Actionklasse for å vise frem kartfilter dialogen.  
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	class FargeFilterAction extends AbstractAction{
	    
		private CheckButtonNode node;
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public FargeFilterAction(String text, Icon image, CheckButtonNode node){
            super(text, image);
            this.node = node;
            
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	Container parent = KartlagPanel.this;
            while (!(parent instanceof Window)){
                parent = parent.getParent();
            }
            
            final UTMProjectionTileFactory fact = (UTMProjectionTileFactory)module.getMainMap().getTileFactory();
            Color forrigeFarge = fact.getColorForFilter();
            float forrigeMix = fact.getMixValueForFilter();
            KartfilterDialog dialog = (KartfilterDialog)windowManager.getDialog(KartfilterDialog.class);
            if (dialog==null){
                dialog = new KartfilterDialog((Window)parent, forrigeFarge, forrigeMix);
                dialog.setTitle(ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartfilterDialog.title"));
                dialog.addChangeListener(new ChangeListener(){
                    public void stateChanged(ChangeEvent e){
                        if (e.getSource() instanceof KartFilterPanel){
                            KartFilterPanel panel = (KartFilterPanel)e.getSource();
                            Color farge = panel.getValgtFarge();
                            float mix = panel.getMix();
                            module.getMainMap().repaint();
                            node.setSelected(true);
                            
                            node.setMethod("setColorFilter");
                            node.setRestoreMethod("removeColorFilter");
                            node.setParams(new Object[]{farge, mix});
                            node.setInstance(fact);
                        }
                    }
                });
                dialog.pack();
                dialog.setLocationRelativeTo(null);
            }
            windowManager.addDialog(dialog);
            dialog.setVisible(true);
        }
    
	}
	
	/**
	 * Actionklasse for å vise frem de valgene som er gjort i treet.  
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	class OkButtonAction extends AbstractAction{
	    
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public OkButtonAction(String text, Icon image){
            super(text, image);
            
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent ev) {
        	ApplyButtonAction action = new ApplyButtonAction(null, null);
        	action.actionPerformed(null);
        	if (action.isFailed()){
        		
        	}else{
        		dialog.setVisible(false);
        	}
        }
	}
	
	/**
	 * Actionklasse for å avbryte de valgene som er gjort i treet.  
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	class CancelButtonAction extends AbstractAction{
	    
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public CancelButtonAction(String text, Icon image){
            super(text, image);
            
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	dialog.setVisible(false);
        }
	}
	
	/**
	 * Actionklasse for å avbryte de valgene som er gjort i treet.  
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	class ApplyButtonAction extends AbstractAction{
	    
		private boolean failed = false;
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public ApplyButtonAction(String text, Icon image){
            super(text, image);
            
        }
        
        public boolean isFailed(){
        	return failed;
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	List<CheckButtonNode> allChilds = root.getChilds();
        	boolean allUnselected = true;
        	for (CheckButtonNode n:allChilds){
        		if (n.isSelected()){
        			allUnselected = false;
        			String method =null;
        			Object[] arg = null;
        			Object instance = null;
        			Class[] classes = null;
        			Class instanceClass =null;
        			try{
	        			method = n.getMethod();
	        			arg = n.getParams();
	        			classes = new Class[arg.length];
	        			for (int i=0;i<arg.length;i++){
	        				classes[i]=arg[i].getClass();
	        			}
	        			instance = n.getInstance();
	        			instanceClass = n.getInstance().getClass();
        			} catch (Exception ex){
        				JOptionPane.showMessageDialog(KartlagPanel.this, n.getDefaultFeilmelding(), ResourceUtil.getResource("mipssmap.plugins.layer.kartlag.KartlagPanel.fargefilter.warning.errorSettingLayer")+n.getText(), JOptionPane.WARNING_MESSAGE);
        				failed = true;
        				continue;
        			}
        			
					try {
						Method m = instanceClass.getMethod(method, classes);
						m.invoke(instance, arg);
						module.getMainMap().repaint();
					} catch (SecurityException e1) {
						e1.printStackTrace();
					} catch (NoSuchMethodException e1) {
						e1.printStackTrace();
					} catch (IllegalArgumentException e1) {
						e1.printStackTrace();
					} catch (IllegalAccessException e1) {
						e1.printStackTrace();
					} catch (InvocationTargetException e1) {
						e1.printStackTrace();
					}
        		}else{
        			String method = n.getRestoreMethod();
        			Object instance = n.getInstance();
        			if (instance!=null){
	        			Class instanceClass = instance.getClass();
	        			try {
							Method m = instanceClass.getMethod(method, new Class[]{});
							
							m.invoke(instance, new Object[]{});
							module.getMainMap().repaint();
						} catch (SecurityException e1) {
							e1.printStackTrace();
						} catch (NoSuchMethodException e1) {
							e1.printStackTrace();
						} catch (IllegalArgumentException e1) {
							e1.printStackTrace();
						} catch (IllegalAccessException e1) {
							e1.printStackTrace();
						} catch (InvocationTargetException e1) {
							e1.printStackTrace();
						}
        			}
        		}
        	}
        	if (!failed){
	        	if (allUnselected){
	        		plugin.getCheckButton().getCheckBox().setSelected(false);
	        	} else{
	        		plugin.getCheckButton().getCheckBox().setSelected(true);
	        	}
        	}
        		
        }
	}


}
