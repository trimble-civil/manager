package no.mesta.mipss.mipssmap.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.mipssmap.MipssMapModule;
import no.mesta.mipss.mipssmap.plugins.MipssMapProductionPlugin;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonDialog;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonPanelNew;
import no.mesta.mipss.mipssmap.plugins.produksjon.produksjon.ProduksjonPlugin;
import no.mesta.mipss.mipssmap.task.SanntidTask;
import no.mesta.mipss.mipssmap.utils.JCheckButton;
import no.mesta.mipss.mipssmap.utils.JCheckButtonPanel;
import no.mesta.mipss.mipssmap.utils.ProduksjonOptions;
import no.mesta.mipss.mipssmap.utils.ResourceUtil;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.UIUtils;
import no.mesta.mipss.util.DatePickerConstraintsUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXDatePicker;
import org.joda.time.LocalTime;

@SuppressWarnings("serial")
public class ProduksjonPanel extends JPanel{
    private Logger log =  LoggerFactory.getLogger(this.getClass());
    private JCheckButtonPanel checkButtons;
    
    private final int DEFAULT_SVV_HALE_MINUTES = 15;

    private JLabel tidLabel = new JLabel(ResourceUtil.getResource("mipssmap.swing.ProduksjonPanel.tidLabel.text"));
    private JLabel fraLabel = new JLabel(ResourceUtil.getResource("mipssmap.swing.ProduksjonPanel.fraLabel.text"));
    private JLabel tilLabel = new JLabel(ResourceUtil.getResource("mipssmap.swing.ProduksjonPanel.tilLabel.text"));
    
    private JRadioButton sanntidRadio = new JRadioButton(ResourceUtil.getResource("mipssmap.swing.ProduksjonPanel.sanntidRadio.text"));
    private JRadioButton periodeRadio = new JRadioButton(ResourceUtil.getResource("mipssmap.swing.ProduksjonPanel.periodeRadio.text"));
    private JCheckBox natidCheck = new JCheckBox(ResourceUtil.getResource("mipssmap.swing.ProduksjonPanel.natidLabel.text"));
    
    private JMipssDatePicker fraPicker;
    private JMipssDatePicker tilPicker;
    private JSpinner fraSpinner;
    private JSpinner tilSpinner;
    
    private MipssDateTimePickerHolder fraDato;
    private MipssDateTimePickerHolder tilDato;
    
	private final List<MipssMapProductionPlugin> plugins;

	private JPanel periodePanel;

	private JPanel sanntidPanel;
	private final MipssMapWindowManager windowManager;
	private final MipssMapModule plugin;
	private final boolean brukSVVInnsyn;
	private int svvHaleMinutes;
    
    public ProduksjonPanel(MipssMapModule plugin, List<MipssMapProductionPlugin> plugins, MipssMapWindowManager windowManager, boolean brukSVVInnsyn) {
        this.plugin = plugin;
		this.plugins = plugins;
		this.windowManager = windowManager;
		this.brukSVVInnsyn = brukSVVInnsyn;
		
		if (!brukSVVInnsyn) {
			List<JCheckButton> buttonList = new ArrayList<JCheckButton>();
			if (plugins!=null)
	        for (MipssMapProductionPlugin p:plugins){
	            buttonList.add(p.getCheckButton());
	        }
	        checkButtons = new JCheckButtonPanel(buttonList);
	        initGUI();
		} else{
			initSVVInnsynGUI();
		}
    }
    
    public void setEnabled(boolean enabled){
    	for (MipssMapProductionPlugin p:plugins){
    		p.setEnabled(enabled);
    	}
    	sanntidRadio.setEnabled(enabled);
    	natidCheck.setEnabled(enabled);
    	refresh.setEnabled(enabled);
    	
    }
    private void initGUI(){
        setLayout(new GridBagLayout());
        Dimension size = new Dimension(200, Short.MAX_VALUE);
        setMaximumSize(size);

        Calendar c = Calendar.getInstance();
        c.setTime(Clock.now());
        
        tilPicker = new JMipssDatePicker(c.getTime());
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        tilPicker.setFormats(format);
        fraPicker = new JMipssDatePicker(c.getTime());
        fraPicker.setFormats(format);
        
        DatePickerConstraintsUtils.setNotBeforeDate(plugin.getLoader(), fraPicker, tilPicker);
        
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        
        Dimension spinnerSize = new Dimension(70, 22);
        fraSpinner = new javax.swing.JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        fraSpinner.setPreferredSize(spinnerSize);
        fraSpinner.setMinimumSize(spinnerSize);
        fraSpinner.setMaximumSize(spinnerSize);
        fraSpinner.setEditor(new JSpinner.DateEditor(fraSpinner, "HH:mm:ss"));
        
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        tilSpinner = new javax.swing.JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        tilSpinner.setPreferredSize(spinnerSize);
        tilSpinner.setMinimumSize(spinnerSize);
        tilSpinner.setMaximumSize(spinnerSize);
        tilSpinner.setEditor(new JSpinner.DateEditor(tilSpinner, "HH:mm:ss"));
        
        fraDato = new MipssDateTimePickerHolder(fraPicker, fraSpinner);
        tilDato = new MipssDateTimePickerHolder(tilPicker, tilSpinner);
        
        fraPicker.getMonthView().addFocusListener(new FocusAdapter(){
        	public void focusLost(FocusEvent e) {
        		tilPicker.setDate(fraPicker.getDate());
	            if (tilDato.getDate().before(fraDato.getDate())){
	            	fraPicker.setDate(tilDato.getDate());
	                tilSpinner.getModel().setValue(fraDato.getDate());
	            }
        	}
        });
        
        fraPicker.getEditor().addKeyListener(new KeyAdapter(){
        	@Override
    		public void keyTyped(KeyEvent e) {
    			if (e.getKeyChar()==KeyEvent.VK_ENTER){
    				tilPicker.setDate(fraPicker.getDate());
                    if (tilDato.getDate().before(fraDato.getDate())){
                        fraPicker.setDate(tilDato.getDate());
                        tilSpinner.getModel().setValue(fraDato.getDate());
                    }
    			}
    		}
        });
        
        tilPicker.getEditor().addFocusListener(new FocusAdapter(){
                  public void focusLost(FocusEvent e) {
                      if (tilDato.getDate().before(fraDato.getDate())){
                          fraPicker.setDate(tilDato.getDate());
                          tilSpinner.getModel().setValue(fraDato.getDate());
                      }
                  }
        });
        tilPicker.getEditor().addKeyListener(new KeyAdapter(){
        	public void keyTyped(KeyEvent e) {
    			if (e.getKeyChar()==KeyEvent.VK_ENTER){
    				if (tilDato.getDate().before(fraDato.getDate())){
                        fraPicker.setDate(tilDato.getDate());
                        tilSpinner.getModel().setValue(fraDato.getDate());
                    }
    			}
    		}
        });
           
        tilSpinner.addChangeListener(new ChangeListener(){                
           public void stateChanged(ChangeEvent e) {
               if (tilDato.getDate().before(fraDato.getDate())){
                   tilSpinner.getModel().setValue(fraDato.getDate());
               }
           }
        });
        fraSpinner.addChangeListener(new ChangeListener(){                
           public void stateChanged(ChangeEvent e) {
               if (tilDato.getDate().before(fraDato.getDate())){
                   tilSpinner.getModel().setValue(fraDato.getDate());
               }
           }
        });
        
        JPanel velgPeriodePanel = new JPanel();
        velgPeriodePanel.setLayout(new BoxLayout(velgPeriodePanel, BoxLayout.LINE_AXIS));
        velgPeriodePanel.add(tidLabel);
        velgPeriodePanel.add(sanntidRadio);
        velgPeriodePanel.add(periodeRadio);
        ButtonGroup group = new ButtonGroup();
        group.add(sanntidRadio);
        group.add(periodeRadio);
        
        periodeRadio.setSelected(true);
        sanntidRadio.setEnabled(true);
        periodeRadio.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				periodePanel.setVisible(true);
				sanntidPanel.setVisible(false);
				ProduksjonDialog dialog = (ProduksjonDialog)windowManager.getDialog(ProduksjonDialog.class);
				if (dialog==null){
					dialog = (ProduksjonDialog)windowManager.getDialog(ProduksjonDialog.class);
				}
				if (dialog!=null){
					ProduksjonPanelNew pp = dialog.getProduksjonPanel();
					pp.setPeriodeMode();
				}
				for (MipssMapProductionPlugin p:plugins){
		    		p.setEnabled(true);
		    	}
			}
        });
        sanntidRadio.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				periodePanel.setVisible(false);
				sanntidPanel.setVisible(true);
				ProduksjonDialog dialog = (ProduksjonDialog)windowManager.getDialog(ProduksjonDialog.class);
				if (dialog == null){
					for (MipssMapProductionPlugin p:plugins){
			    		if ((p instanceof ProduksjonPlugin))
			    			((ProduksjonPlugin)p).initPlugin();
			    	}
					dialog = (ProduksjonDialog)windowManager.getDialog(ProduksjonDialog.class);
				}
				
				if (dialog!=null){
					ProduksjonPanelNew pp = dialog.getProduksjonPanel();
					pp.setSanntidMode();
				}
				for (MipssMapProductionPlugin p:plugins){
		    		if (!(p instanceof ProduksjonPlugin))
		    			p.setEnabled(false);
		    	}
			}
        });
        
        BindingHelper.createbinding(natidCheck, "${selected!=true}", tilSpinner, "enabled").bind();
        BindingHelper.createbinding(natidCheck, "${selected!=true}", tilPicker, "enabled").bind();
        
        refresh = new JButton();
        refresh.setIcon(IconResources.REFRESH_VIEW_ICON);
        refresh.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				tilDato.setDate(Clock.now());
				refreshPlugins();
			}
        });
        refresh.setPreferredSize(new Dimension(22,22));
        refresh.setMaximumSize(new Dimension(22,22));
        refresh.setMinimumSize(new Dimension(22,22));
        refresh.setToolTipText(ResourceUtil.getResource("mipssmap.swing.ProduksjonPanel.refreshIcon.tooltip"));
        periodePanel = UIUtils.getVertPanel();
        
        
        Dimension spacing = new Dimension(5, 0);
        JPanel fraPanel = new JPanel();
        fraPanel.setLayout(new BoxLayout(fraPanel, BoxLayout.LINE_AXIS));
        fraPanel.add(fraLabel);
        fraPanel.add(Box.createRigidArea(new Dimension(4, 0)));
        fraPanel.add(fraPicker);
        fraPanel.add(Box.createRigidArea(spacing));
        fraPanel.add(fraSpinner);
        
        JPanel tilPanel = new JPanel();
        tilPanel.setLayout(new BoxLayout(tilPanel, BoxLayout.LINE_AXIS));
        tilPanel.add(tilLabel);
        tilPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        tilPanel.add(tilPicker);
        tilPanel.add(Box.createRigidArea(spacing));
        tilPanel.add(tilSpinner);
        
        JPanel natidCheckPanel = new JPanel();
        natidCheckPanel.setLayout(new BoxLayout(natidCheckPanel, BoxLayout.LINE_AXIS));
        natidCheckPanel.add(Box.createHorizontalGlue());
        natidCheckPanel.add(natidCheck);
        natidCheckPanel.add(Box.createHorizontalGlue());
        natidCheckPanel.add(refresh);
        
        periodePanel.add(fraPanel);
        periodePanel.add(tilPanel);
        periodePanel.add(Box.createRigidArea(new Dimension(0, 5)));
//        periodePanel.add(natidCheckPanel);
        
        sanntidPanel = createSanntidPanel();
        sanntidPanel.setVisible(false);
        
        add(velgPeriodePanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(periodePanel, 	  new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 12), 0, 0));
        add(sanntidPanel, 	  new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 15, 0), 0, 0));
        add(checkButtons, 	  new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        plugin.addPropertyChangeListener(new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("currentKontrakt".equals(evt.getPropertyName())){
					if (sanntidRunner!=null)
						sanntidRunner.stopRunning();
					setSanntidRunning(false);
					goButton.setIcon(IconResources.START_SANNTID);
					periodeRadio.doClick();
				}
			}
        });
    }
    
    public JPanel createSanntidPanel(){  	
        JPanel sanntidPanel = new JPanel(new GridBagLayout());
        JLabel tidLabel = new JLabel("Tidsvindu");
        goButton = new JButton("Start");
        final JTextField fraText  = new JTextField();
        final JTextField tilText  = new JTextField();
        
        tidCombo = new JComboBox();
        tidCombo.addItem("1 min");
        tidCombo.addItem("2 min");
        tidCombo.addItem("3 min");
        tidCombo.addItem("4 min");
        tidCombo.addItem("5 min");
        tidCombo.addItem("10 min");
        tidCombo.addItem("15 min");
        tidCombo.addItem("20 min");
        tidCombo.addItem("30 min");
        tidCombo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int t = getMinuteTail(tidCombo);
				fraText.setText(MipssDateFormatter.formatDate(CalendarUtil.getAgo(Clock.now(), t), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
				tilText.setText(MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
			}
        });
        
        fraText.setEditable(false);
        tilText.setEditable(false);
        
        goButton.setAction(getStartAction(goButton, fraText, tilText, tidCombo));
        goButton.setIcon(IconResources.START_SANNTID);
        goButton.setHorizontalAlignment(SwingConstants.LEFT);
        goButton.setIconTextGap(2);
        goButton.setMargin(new Insets(0,2,0,0));
        Dimension btnSize = new Dimension(60, 22);
        UIUtils.setComponentSize(btnSize, goButton);
        
        fraText.setText(MipssDateFormatter.formatDate(CalendarUtil.getAgo(Clock.now(), 1), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
        tilText.setText(MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
        
        JLabel fraLabel = new JLabel("Fra:");
        JLabel tilLabel = new JLabel("Til:");
        
        sanntidPanel.add(tidLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        sanntidPanel.add(tidCombo, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        sanntidPanel.add(goButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 5), 0, 0));
        
        sanntidPanel.add(fraLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        sanntidPanel.add(fraText,  new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
        sanntidPanel.add(tilLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        sanntidPanel.add(tilText,  new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
        return sanntidPanel;
    }

    private int getMinuteTail(JComboBox c){
    	String s = (String) c.getSelectedItem();
    	return Integer.valueOf(s.substring(0, s.indexOf(' ')));
    }
    private SanntidTask sanntidRunner;
	private JButton refresh;
	private JComboBox tidCombo;
	private JButton goButton;
    
    private Action getStartAction(final JButton goButton, final JTextField fra, final JTextField til, final JComboBox cbox){
    	Action start = new AbstractAction("Start"){
			@Override
			public void actionPerformed(ActionEvent e) {
				int minuteTail = getMinuteTail(cbox);
				goButton.setAction(getStoppAction(goButton, fra, til, cbox));
				goButton.setIcon(IconResources.STOPP_SANNTID);
				setSanntidRunning(true);
				sanntidRunner = new SanntidTask(plugin, fra, til, minuteTail, getProduksjonPlugin());
				sanntidRunner.execute();
			}
        };
        return start;
    }
    
    private Action getStoppAction(final JButton goButton, final JTextField fra, final JTextField til, final JComboBox cbox){
    	AbstractAction stopp = new AbstractAction("Stopp"){
			@Override
			public void actionPerformed(ActionEvent e) {
				goButton.setAction(getStartAction(goButton, fra, til, cbox));
				goButton.setIcon(IconResources.START_SANNTID);
				sanntidRunner.stopRunning();
				setSanntidRunning(false);
				sanntidRunner = null;
			}
        };
        return stopp;
    }
    private void setSanntidRunning(boolean running){
    	periodeRadio.setEnabled(!running);
		sanntidRadio.setEnabled(!running);
		tidCombo.setEnabled(!running);
		ProduksjonPlugin pl = getProduksjonPlugin();
		if (pl!=null){
			pl.getCheckButton().setEnabled(!running);
		}
    }
    private ProduksjonPlugin getProduksjonPlugin(){
    	for (MipssMapProductionPlugin m:plugins){
    		if (m instanceof ProduksjonPlugin){
    			return (ProduksjonPlugin)m;
    		}
    	}
    	return null;
    }
    
    
    private void refreshPlugins(){
    	for (MipssMapProductionPlugin mp:plugins){
    		if (mp.getCheckButton().getCheckBox().isSelected()){
    			mp.refresh();
    		}
    	}
    }
    public ProduksjonOptions getOptions(){
        ProduksjonOptions options = new ProduksjonOptions();
        if (brukSVVInnsyn) {
        	
        	Calendar calNow = Calendar.getInstance();
//        	calNow.set(2014, Calendar.MARCH, 15, 12, 0); // TEST TEST TEST TEST TEST
        	
        	Date now = calNow.getTime();       	
        	        	
        	Date minutesBefore = LocalTime.fromCalendarFields(calNow).minusMinutes(svvHaleMinutes).toDateTimeToday().toDate();  	
        	
        	JXDatePicker datePickerFra = new JXDatePicker();
        	datePickerFra.setDate(now);
        	
        	
        	JSpinner timeNow = new javax.swing.JSpinner(new SpinnerDateModel(now, null, null, Calendar.MINUTE));
        	timeNow.setEditor(new JSpinner.DateEditor(timeNow, "HH:mm:ss"));       
        	
        	JSpinner time15 = new javax.swing.JSpinner(new SpinnerDateModel(minutesBefore, null, null, Calendar.MINUTE));
        	time15.setEditor(new JSpinner.DateEditor(time15, "HH:mm:ss"));
        	
        	MipssDateTimePickerHolder newFra = new MipssDateTimePickerHolder(datePickerFra, time15);
        	MipssDateTimePickerHolder newTil = new MipssDateTimePickerHolder(datePickerFra, timeNow);
        	options.setDatoFra(newFra);
        	options.setDatoTil(newTil);
        	JCheckBox box = new JCheckBox();
        	box.setSelected(false);
        	options.setNatidCheck(box);
		} else{
			options.setDatoFra(fraDato);
	        options.setDatoTil(tilDato);
	        options.setNatidCheck(natidCheck);
		}      
        return options;
    }
    
    private void initSVVInnsynGUI() {
    	
    	String svvHaleMinutesString = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "svv_hale").getVerdi();    	
    	svvHaleMinutes = svvHaleMinutesString == null ? DEFAULT_SVV_HALE_MINUTES : Integer.parseInt(svvHaleMinutesString);
    	
    	JPanel svvPanel = new JPanel(new BorderLayout());
		JButton btnHentData = new JButton(ResourceUtil.getResource("svv.hentDataButton"));
		JLabel txtInfoTekst = new JLabel(ResourceUtil.getResource("svv.infotekst", svvHaleMinutes + ""));
		
		svvPanel.add(btnHentData, BorderLayout.NORTH);
		svvPanel.add(txtInfoTekst, BorderLayout.CENTER);
		add(svvPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		
		btnHentData.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ProduksjonPlugin pPlugin = new ProduksjonPlugin(windowManager, plugin);
				ProduksjonPanelNew pPanelNew = new ProduksjonPanelNew(plugin, pPlugin);
				
				pPlugin.setProduksjonOptions(getOptions());
				
				pPanelNew.hentSVVProduksjon();
				
			}
		});	
	}
}
