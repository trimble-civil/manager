import java.awt.Point;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.service.produksjon.ProdReflinkStrekning;
import no.mesta.mipss.service.produksjon.ProdtypeVO;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.webservice.ReflinkStrekning;


public class TestAvShape {
	public static void main(String[] args) {
		ProduksjonService service = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		ProduksjonQueryParams params = new ProduksjonQueryParams();
//		params.setDfuId(Long.valueOf(2084));
		params.setKjoretoyId(Long.valueOf(1532));
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2010);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DAY_OF_MONTH, 11);
		c.set(Calendar.HOUR_OF_DAY, 12);
		
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		params.setFraDato(c.getTime());
		
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		params.setTilDato(c.getTime());
		List<ReflinkStrekning> lrs = new ArrayList<ReflinkStrekning>();
		List<ProdReflinkStrekning> ps = service.getProduksjonStrekninger(params);
		for (ProdReflinkStrekning p:ps){
			List<ProdtypeVO> prodtypeList = p.getProdtypeList();
			printProdtype(prodtypeList);
			List<ReflinkStrekning> str = p.getStrekningList();
			for (ReflinkStrekning s:str){
				lrs.add(s);
				Point[] vectors = s.getVectors();
				printVec(vectors);
				printRef(s);
			}
			System.out.println("\n\n");
		}
		LagShape s = new LagShape();
		try {
			s.lagShape(lrs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void printRef(ReflinkStrekning rs){
		System.out.println(rs.getReflinkId()+":"+rs.getFra()+"-"+rs.getTil());
	}
	public static void printVec(Point[] vec){
		for (Point p:vec){
			System.out.println(p.getX()+" "+p.getY());
		}
	}
	public static void printProdtype(List<ProdtypeVO> p){
		for (ProdtypeVO po:p){
			System.out.print(po.getProdtype().getNavn()+",");
		}
		System.out.println();
	}
}
