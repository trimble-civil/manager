package test.no.mesta.mipss.mapplugin;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import junit.framework.TestCase;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.Resolutions;

public class MapReadConfigTest extends TestCase {
    public MapReadConfigTest(String sTestName) {
        super(sTestName);
    }

    protected void setUp() throws Exception {
        
        readConfig(getClass().getClassLoader().getResourceAsStream("kartKonfig.xml"));
    }
    private Resolutions resolutions;
    private Bounds bounds;
    private String baseURL;
    /**
     * Read the config file stored as XML. This file contains information
     * about the tilecache server such as:
     *          bounds
     *          resolutions
     *          baseURL
     * 
     * @param boundsAndResolutions
     */
    private void readConfig(InputStream boundsAndResolutions) {
        
        bounds = new Bounds();
        try {
            //create properties from xml file
            Properties p = new Properties();
            p.loadFromXML(boundsAndResolutions);

            //read the bounds from properties
            bounds.setLlx(Double.parseDouble("" + p.get("llx")));
            bounds.setLly(Double.parseDouble("" + p.get("lly")));
            bounds.setUrx(Double.parseDouble("" + p.get("urx")));
            bounds.setUry(Double.parseDouble("" + p.get("ury")));

            //read the resolutions from properties
            String[] resolutionsString = 
                p.getProperty("resolutions").split(",");
            double[] res = new double[resolutionsString.length];
            for (int i = 0; i < resolutionsString.length; i++) {
                res[i] = Double.parseDouble(resolutionsString[i]);
            }
            resolutions = new Resolutions(res);
            //resolutions.setResolutions(res);

            //read baseURL
            baseURL = "" + p.get("baseURL");

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (InvalidPropertiesFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    protected void tearDown() throws Exception {
        super.tearDown();
        resolutions = null;
        bounds = null;
        baseURL = null;
    }
    /**
     * @see Bounds#getLlx()
     */
    public void testGetLlx() {
        assertFalse(Double.isNaN(bounds.getLlx()));
    }

    /**
     * @see Bounds#getLly()
     */
    public void testGetLly() {
        assertFalse(Double.isNaN(bounds.getLly()));
    }

    /**
     * @see Bounds#getUrx()
     */
    public void testGetUrx() {
        assertFalse(Double.isNaN(bounds.getUrx()));
    }

    /**
     * @see Bounds#getUry()
     */
    public void testGetUry() {
        assertFalse(Double.isNaN(bounds.getUry()));
    }
    
   
    
    /**
     * Test that base url is present
     */
    public void testGetBaseURL(){
        assertNotNull(baseURL);
        assertTrue(baseURL.length()>0);
    }
}
