package no.mesta.mipss.epost.swing;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class GuiTexts {
	private static PropertyResourceBundleUtil props;
	
	public static final String RESEND = "resend";
	public static final String VELG_DATO = "velgdato";
	public static final String SAVE = "save";
	public static final String CHANGE_WARNING = "change.warning";
	public static final String CHANGE_TITLE = "change.title";
	public static final String VENNLIGST_VENT = "vent";

	public static final String NO_RECEIVERS_TITLE = "noreceivers.title";
	public static final String NO_RECEIVERS = "noreceivers";
	public static final String ALLE = "alle";
	public static final String INGEN = "ingen";
	public static final String LEVERANDORER = "leverandorer";
	public static final String EIERE = "eiere";
	public static final String KOPIER = "kopier";

	public static final String MAIL_SENT_TITLE = "mail.sent.title";

	public static final String MAIL_SENT = "mail.sent";

	public static final String MAIL_MISSING_TITLE = "mail.missing.title";

	public static final String MAIL_MISSING = "mail.missing";
	
	/**
     * Fyller ut poperties
     * 
     * @return
     */
    private static PropertyResourceBundleUtil getProps() {
        if(props == null) {
            props = PropertyResourceBundleUtil.getPropertyBundle("epostText");
        }
        
        return props;
    }
    
    /**
     * Henter ut GUI teksten
     * 
     * @param key
     * @return
     */
    public static String getText(String key) {
        return getProps().getGuiString(key);
    }
    
    /**
     * Henter GUI teksten, og fletter inn objekter
     * 
     * @param key
     * @param values
     * @return
     */
    public static String getText(String key, Object... values) {
        return getProps().getGuiString(key, values);
    }
}
