package no.mesta.mipss.epost.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.beansbinding.Binding;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.epost.EpostModule;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssDatePicker;
/**
 * Panel for knappene til epostmodulen
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class EpostButtonPanel extends JPanel{

	private final EpostModule plugin;
	private final EpostTablePanel tablePanel;
	private JMipssDatePicker datePicker;

	public EpostButtonPanel(EpostModule plugin, EpostTablePanel tablePanel){
		this.plugin = plugin;
		this.tablePanel = tablePanel;
		initGui();
	}

	@SuppressWarnings("unchecked")
	private void initGui() {
		setLayout(new GridBagLayout());
		JButton resendButton = new JButton(new ResendAction(GuiTexts.getText(GuiTexts.RESEND), IconResources.MAIL_SEND));
		datePicker = new JMipssDatePicker(CalendarUtil.getDaysAgo(Clock.now(), 1));
		JLabel velgLabel = new JLabel(GuiTexts.getText(GuiTexts.VELG_DATO));
		JButton saveButton = new JButton(new SaveAction(GuiTexts.getText(GuiTexts.SAVE), IconResources.SAVE_ICON));
		
		Binding lagreActivate = BindingHelper.createbinding(tablePanel, "${readyToSave==true}", saveButton, "enabled");
		Binding resendActivate = BindingHelper.createbinding(tablePanel.getTableModel(), "${size>0}", resendButton, "enabled");
		lagreActivate.bind();
		resendActivate.bind();
		
		add(resendButton, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(velgLabel, 	  new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(datePicker,   new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(saveButton,   new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
	}
	
	
	class SaveAction extends AbstractAction{
		public SaveAction(String text, Icon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			plugin.getController().saveMipssEpostEntities();
		}
	}
	class ResendAction extends AbstractAction{
		public ResendAction(String text, Icon icon){
			super(text, icon);
		}
		
		@Override
		public void actionPerformed(ActionEvent e){
			plugin.getController().sendEkstraDagsrapport(datePicker.getDate(), tablePanel.getSelectedEkstraEpostEntities());
		}
	}
}
