package no.mesta.mipss.epost.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.jdesktop.beansbinding.Binding;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.MipssEpostEntity;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.resources.images.IconResources;


/**
 * Knappepanel for å velge mottakere av dagsrapport
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */

public class EpostTableButtonPanel extends JPanel {
	
	private final EpostTablePanel tablePanel;
	public EpostTableButtonPanel(EpostTablePanel tablePanel){
		this.tablePanel = tablePanel;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		JButton alleButton = new JButton(new AlleAction(GuiTexts.getText(GuiTexts.ALLE), IconResources.SELECT_ALL));
		JButton ingenButton = new JButton(new IngenAction(GuiTexts.getText(GuiTexts.INGEN), IconResources.REMOVE_ITEM_ICON));
		JButton levButton = new JButton(new LevAction(GuiTexts.getText(GuiTexts.LEVERANDORER), IconResources.SELECT_LEVS));
		JButton eierButton = new JButton(new EierAction(GuiTexts.getText(GuiTexts.EIERE), IconResources.SELECT_OWNERS));
		JButton kopierButton = new JButton(new KopierAction(GuiTexts.getText(GuiTexts.KOPIER), IconResources.COPY_ICON));
		
		alleButton.setHorizontalAlignment(JButton.LEFT);
		ingenButton.setHorizontalAlignment(JButton.LEFT);
		levButton.setHorizontalAlignment(JButton.LEFT);
		eierButton.setHorizontalAlignment(JButton.LEFT);
		kopierButton.setHorizontalAlignment(JButton.LEFT);
		
		BindingHelper.createbinding(tablePanel.getTableModel(), "${size>0}", alleButton, "enabled").bind();
		BindingHelper.createbinding(tablePanel.getTableModel(), "${size>0}", ingenButton, "enabled").bind();
		BindingHelper.createbinding(tablePanel.getTableModel(), "${size>0}", levButton, "enabled").bind();
		BindingHelper.createbinding(tablePanel.getTableModel(), "${size>0}", eierButton, "enabled").bind();
		BindingHelper.createbinding(tablePanel.getTableModel(), "${size>0}", kopierButton, "enabled").bind();
		
		add(kopierButton,   new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,0), 0,0));
		add(alleButton,  	new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,0), 0,0));
		add(levButton,		new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,0), 0,0));
		add(eierButton,		new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,0), 0,0));
		add(ingenButton,	new GridBagConstraints(0,4, 1,1, 0.0,1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,0), 0,0));
	}
	
	private class AlleAction extends AbstractAction{
		public AlleAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent ev) {
			for (MipssEpostEntity e:tablePanel.getTableModel().getEntities()){
				e.setEkstraDagsrapport(Boolean.TRUE);
			}
		}
	}
	private class IngenAction extends AbstractAction{
		public IngenAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent ev) {
			for (MipssEpostEntity e:tablePanel.getTableModel().getEntities()){
				e.setEkstraDagsrapport(Boolean.FALSE);
			}
		}
	}
	private class LevAction extends AbstractAction{
		public LevAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent ev) {
			for (MipssEpostEntity e:tablePanel.getTableModel().getEntities()){
				if (e instanceof KontraktLeverandor)
					e.setEkstraDagsrapport(Boolean.TRUE);
			}
		}
	}
	private class EierAction extends AbstractAction{
		public EierAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent ev) {
			for (MipssEpostEntity e:tablePanel.getTableModel().getEntities()){
				if (e instanceof Kjoretoyeier)
					e.setEkstraDagsrapport(Boolean.TRUE);
				
			}
		}
	}
	private class KopierAction extends AbstractAction{
		public KopierAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent ev) {
			for (MipssEpostEntity e:tablePanel.getTableModel().getEntities()){
				e.setEkstraDagsrapport(e.getSendeEpost());
			}		
		}
	}
	
}
