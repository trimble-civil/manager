package no.mesta.mipss.epost;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.epost.swing.EpostTablePanel;
import no.mesta.mipss.epost.swing.GuiTexts;
import no.mesta.mipss.persistence.MipssEpostEntity;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.epost.EpostService;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.apache.commons.lang.StringUtils;

public class EpostController implements VetoableChangeListener {

	private EpostService bean = BeanUtil.lookup(EpostService.BEAN_NAME, EpostService.class);
	private EpostTablePanel epostTablePanel;
	private JMipssContractPicker contractPicker;
	private final EpostModule plugin;
	
	public EpostController(EpostModule plugin){
		this.plugin = plugin;	
	}
	
	public EpostService getBean(){
		return bean;
	}
	
	public void setContractPicker(JMipssContractPicker contractPicker){
		this.contractPicker = contractPicker;
		this.contractPicker.addVetoableChangeListener(this);
		this.contractPicker.addPropertyChangeListener("valgtKontrakt", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				plugin.setValgtKontrakt((Driftkontrakt)evt.getNewValue());
			}
		});
	}
	
	public void setEpostTablePanel(EpostTablePanel epostTablePanel){
		this.epostTablePanel = epostTablePanel;
		plugin.addPropertyChangeListener("valgtKontrakt", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				EpostController.this.epostTablePanel.loadData(); 
			}
		});
	}
	
	public void saveMipssEpostEntities(){
		bean.saveEpostList(epostTablePanel.getChangedList());
		epostTablePanel.clearChangeList();
	}
	
	public void sendEkstraDagsrapport(Date dato, List<MipssEpostEntity> ekstra){
		Long id = plugin.getValgtKontrakt().getId();
		if (ekstra==null){
			MipssDialogue dialog = new MipssDialogue(
					plugin.getLoader().getApplicationFrame(), IconResources.INFO_ICON, GuiTexts.getText(GuiTexts.NO_RECEIVERS_TITLE),
					GuiTexts.getText(GuiTexts.NO_RECEIVERS),
		            MipssDialogue.Type.QUESTION,
		            new MipssDialogue.Button[] {MipssDialogue.Button.OK});
			dialog.askUser();
		}else{
			bean.sendEkstraDagsrapport(dato, id, ekstra);
			MipssDialogue dialog = new MipssDialogue(
					plugin.getLoader().getApplicationFrame(), IconResources.INFO_ICON, GuiTexts.getText(GuiTexts.MAIL_SENT_TITLE),
					GuiTexts.getText(GuiTexts.MAIL_SENT),
		            MipssDialogue.Type.INFO,
		            new MipssDialogue.Button[] {MipssDialogue.Button.OK});
			dialog.askUser();
		}
	}
	
	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		if(StringUtils.equals("valgtKontrakt", evt.getPropertyName())) {
			Driftkontrakt valgtKontrakt = (Driftkontrakt)evt.getNewValue(); //contractPicker.getValgtKontrakt();
			
			if(valgtKontrakt != null && valgtKontrakt.equals(plugin.getValgtKontrakt())) {
				return;
			}
			if (!epostTablePanel.getChangedList().isEmpty()){
				MipssDialogue dialogue = new MipssDialogue(
						plugin.getLoader().getApplicationFrame(), IconResources.INFO_ICON, GuiTexts.getText(GuiTexts.CHANGE_TITLE),
						GuiTexts.getText(GuiTexts.CHANGE_WARNING, epostTablePanel.getChangedList().size()),
			            MipssDialogue.Type.QUESTION,
			            new MipssDialogue.Button[] {MipssDialogue.Button.YES, MipssDialogue.Button.NO});
				dialogue.askUser();
				if(dialogue.getPressedButton() == MipssDialogue.Button.YES) {
					epostTablePanel.loadData();
				} else if(dialogue.getPressedButton() == MipssDialogue.Button.NO) {
					throw new PropertyVetoException("Ulagrede endringer", evt);
				}
			}
		}
	}
}
