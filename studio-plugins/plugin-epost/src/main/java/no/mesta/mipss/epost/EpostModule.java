package no.mesta.mipss.epost;

import javax.swing.JComponent;

import no.mesta.mipss.epost.swing.EpostMainPanel;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.plugin.MipssPlugin;

public class EpostModule extends MipssPlugin{

	private EpostMainPanel guiPanel;
	private Driftkontrakt valgtKontrakt;
	private EpostController controller;
	
	@Override
	public JComponent getModuleGUI() {
		return guiPanel;
	}

	@Override
	protected void initPluginGUI() {
		guiPanel = new EpostMainPanel(this);
		setValgtKontrakt(getLoader().getSelectedDriftkontrakt());
	}

	@Override
	public void shutdown() {
		
	}
	public void setValgtKontrakt(Driftkontrakt valgtKontrakt) {
		Driftkontrakt old = this.valgtKontrakt;
		this.valgtKontrakt = valgtKontrakt;
		props.firePropertyChange("valgtKontrakt", old, valgtKontrakt);
	}

	public Driftkontrakt getValgtKontrakt() {
		return valgtKontrakt;
	}
	
	public EpostController getController(){
		if (controller==null)
			controller = new EpostController(this);
		return controller;
	}
}
