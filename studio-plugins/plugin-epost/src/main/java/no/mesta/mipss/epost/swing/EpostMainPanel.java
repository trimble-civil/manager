package no.mesta.mipss.epost.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;

import no.mesta.mipss.epost.EpostModule;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.JMipssContractPicker;

public class EpostMainPanel extends JPanel{
	
	private final EpostModule plugin;

	public EpostMainPanel(EpostModule plugin){
		this.plugin = plugin;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		JMipssContractPicker cp = new JMipssContractPicker(plugin, true);
		
		EpostTablePanel tp = new EpostTablePanel(plugin);
		EpostButtonPanel bp = new EpostButtonPanel(plugin, tp);
		plugin.getController().setContractPicker(cp);
		plugin.getController().setEpostTablePanel(tp);
		
		add(cp, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15, 10, 5, 5), 0, 0));
		add(tp, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 10, 5, 5), 0, 0));
		add(bp, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 5, 5), 0, 0));
		
	}

}
