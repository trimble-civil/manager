package no.mesta.mipss.epost;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import no.mesta.mipss.persistence.MipssEpostEntity;

public class BeanChangeRegistry {
	private MipssEpostEntity entity;
	private Map<String, Object> values = new HashMap<String, Object>();
	
	public BeanChangeRegistry(MipssEpostEntity entity){
		this.entity = entity;
	}
	
	public void registerField(String field, Object initVale){
		if (!values.containsKey(field)){
			values.put(field, initVale);
		}
	}
	
	public boolean isPropertiesChanged(MipssEpostEntity entity){
		for (String key:values.keySet()){
			Object o = values.get(key);
			try {
				Method m = this.entity.getClass().getMethod(getFieldGetter(key), null);
				Object value = m.invoke(entity, null);
				if (value==null&&o!=null){
					return true;
				}
				if (value!=null){
					if (!value.equals(o))
						return true;
				}
				if (o!=null){
					if (!o.equals(value))
						return true;
				}

			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		
		return false;
	}
	
	private String getFieldGetter(String field){
		return "get"+Character.toUpperCase(field.charAt(0))+field.substring(1);
	}
	@Override
	public boolean equals(Object o){
		if (!(o instanceof BeanChangeRegistry))
			return false;
		BeanChangeRegistry other = (BeanChangeRegistry)o;
		return entity.equals(other.getEntity());
	}

	private MipssEpostEntity getEntity() {
		return entity;
	}
}
