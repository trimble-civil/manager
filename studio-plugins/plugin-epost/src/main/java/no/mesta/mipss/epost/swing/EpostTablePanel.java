package no.mesta.mipss.epost.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;

import no.mesta.mipss.common.MipssObservableListListener;
import no.mesta.mipss.epost.BeanChangeRegistry;
import no.mesta.mipss.epost.EpostModule;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.MipssEpostEntity;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.epost.EpostService;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.table.EmailEditor;
import no.mesta.mipss.ui.table.MipssBeanTableHelper;

import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;

public class EpostTablePanel extends JPanel{

	private final EpostModule plugin;
	private MipssBeanTableModel<MipssEpostEntity> model;
	private JScrollPane scroll = new JScrollPane();
	private JMipssBeanTable<MipssEpostEntity> table;
	
	private List<MipssEpostEntity> changedList = new ArrayList<MipssEpostEntity>();
	private List<BeanChangeRegistry> touchedValues = new ArrayList<BeanChangeRegistry>();
	
	public EpostTablePanel(EpostModule plugin){
		this.plugin = plugin;
		initTable();
		initGui();
	}
	
	public boolean isReadyToSave(){
		return (!changedList.isEmpty());
	}
	
	private void initTable() {
		int[] editableColumns = new int[]{3, 4, 5, 6};
		Class[] argumentsClass = {Long.class};
		String [] columns = {"number", "type", "navn", "epostAdresse", "kopiEpostAdresse", "sendeEpost", "ekstraDagsrapport"};
		MipssBeanTableHelper<MipssEpostEntity> th = new MipssBeanTableHelper<MipssEpostEntity>(
				editableColumns, MipssEpostEntity.class, plugin.getController().getBean(), EpostService.class, "getEpostList", argumentsClass, columns);
		model = th.getModel();
		model.addTableLoaderListener(new EpostLoaderListener(scroll));
		table = th.getTable();		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setDefaultEditor(String.class, new EmailEditor());
		table.setHighlighters(new Highlighter[] { new ColorHighlighter(new EpostHighlightPredicate(),Color.WHITE, null,null, null) });
		
		table.getColumn(0).setCellRenderer(new RightRenderer());
		table.getColumn(1).setCellRenderer(new RightRenderer());
		
		table.getColumn(0).setMaxWidth(30);
		table.getColumn(1).setMaxWidth(70);
		table.getColumn(0).setMinWidth(30);
		table.getColumn(1).setMinWidth(70);
		table.getColumn(2).setPreferredWidth(160);
		table.getColumn(3).setPreferredWidth(160);
		table.getColumn(4).setPreferredWidth(160);
		table.getColumn(5).setMaxWidth(70);
		table.getColumn(6).setMaxWidth(65);
		table.getColumn(5).setMinWidth(70);
		table.getColumn(6).setMinWidth(65);
		
	}
	/**
	 * Returnerer alle mipssEpostEntities som har avhuket 'sende ekstra' kolonnen
	 * @return
	 */
	public List<MipssEpostEntity> getSelectedEkstraEpostEntities(){
		List<MipssEpostEntity> selectedList = null;
		List<MipssEpostEntity> entities = model.getEntities();
		for (MipssEpostEntity e:entities){
			if (e.getEkstraDagsrapport()!=null&&e.getEkstraDagsrapport()){
				if (selectedList==null){
					selectedList = new ArrayList<MipssEpostEntity>();
				}
				selectedList.add(e);
			}
		}
		return selectedList;
	}
	private class RightRenderer extends DefaultTableCellRenderer{
		
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			Component r = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			JLabel sdf = new JLabel();
			sdf.setOpaque(true);
			if (value!=null)
				sdf.setText(value.toString());
			sdf.setBackground(r.getBackground());
			sdf.setForeground(r.getForeground());
			
			int rowModel = table.convertRowIndexToView(row);
			MipssEpostEntity entity = model.get(rowModel);
			
			int col0 = table.convertColumnIndexToView(0);
			int col1 = table.convertColumnIndexToView(1);
			
			if (column==col0){
				sdf.setHorizontalAlignment(JLabel.CENTER);
			}
			if (column==col1 && (entity instanceof Kjoretoyeier) ){
				sdf.setHorizontalAlignment(JLabel.RIGHT);
			}
			
			return sdf;
		}

	}
	private void initGui(){
		setLayout(new GridBagLayout());
		table.setPreferredScrollableViewportSize(new Dimension(0, 0));
		table.setFillsViewportWidth(true);
		table.setDoWidthHacks(false);
		table.setRowHeightEnabled(true);
		scroll.setViewportView(table);
		this.add(scroll, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(new EpostTableButtonPanel(this), new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	public MipssBeanTableModel<MipssEpostEntity> getTableModel(){
		return model;
	}
	public void loadData(){
		
		clearChangeList();
		
		if (plugin.getValgtKontrakt()!=null){
			model.setBeanMethodArgValues(new Object[]{plugin.getValgtKontrakt().getId()});
			model.loadData();
			new Thread(){
				public void run(){
					while (model.isLoadingData()){
						Thread.yield();
					}
					int i=1;
					for (MipssEpostEntity ep:model.getEntities()){
						if (ep instanceof KontraktLeverandor){
							((KontraktLeverandor)ep).setNumber(i);
							i++;
						}
					}
					
					MipssObservableList<MipssEpostEntity> observableList = new MipssObservableList<MipssEpostEntity>(model.getEntities(), true);
					EpostTableChangeListener changeListener = new EpostTableChangeListener();
					observableList.addObservableListListener(changeListener);
					observableList.addVetoableListListener(changeListener);
					model.setEntities(observableList);
					
				}
			}.start();
		}
		else
			model.setEntities(new MipssObservableList<MipssEpostEntity>());
	}
	
	public void clearChangeList(){
		boolean old = isReadyToSave();
		changedList.clear();
		touchedValues.clear();
		firePropertyChange("readyToSave", old, isReadyToSave());
		
	}
	public List<MipssEpostEntity> getChangedList(){
		return changedList;
	}
	private class EpostHighlightPredicate implements HighlightPredicate {

		/** {@inheritDoc} */
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			int row = adapter.row;
			int rowModel = table.convertRowIndexToView(row);
			MipssEpostEntity e = model.get(rowModel);
			if (table.getSelectedRow()!=row){
				if (e instanceof KontraktLeverandor){
					renderer.setBackground(new Color(220,225,255));
				}
			}
			return false;
		}
	}
	
	
	class EpostTableChangeListener implements MipssObservableListListener{
		@Override
		public void listElementPropertyChanged(ObservableList list, int index, PropertyChangeEvent evt) {
			boolean old = isReadyToSave();
			MipssEpostEntity changed = (MipssEpostEntity)list.get(index);
			changed.getOwnedMipssEntity().setEndretAv(plugin.getLoader().getLoggedOnUserSign());
			changed.getOwnedMipssEntity().setEndretDato(Clock.now());
			if (!evt.getPropertyName().equals("ekstraDagsrapport")){				
				boolean add = true;
				BeanChangeRegistry r = new BeanChangeRegistry(changed);
				if (touchedValues.contains(r)){ //hvis properties ikke har endret seg (dvs tilbake til orginaltilstanden) skal de fjernes fra changelisten
					r = touchedValues.get(touchedValues.indexOf(r));
					r.registerField(evt.getPropertyName(), evt.getOldValue());
					if (!r.isPropertiesChanged(changed)){
						add=false;
						changedList.remove(changed);
					}
				}else{
					r.registerField(evt.getPropertyName(), evt.getOldValue());
					touchedValues.add(r);
				}
				if (!changedList.contains(changed)&&add){
					changedList.add(changed);
				}
				firePropertyChange("readyToSave", old, isReadyToSave());
				if (evt.getPropertyName().equals("epostAdresse")){
					if (evt.getNewValue()==null||evt.getNewValue().equals("")){
						changed.setEkstraDagsrapport(Boolean.FALSE);
						changed.setSendeEpost(Boolean.FALSE);
					}
					
				}
			}
		}
		@Override
		public void listElementPropertyChanged(ObservableList arg0, int arg1) {}
		@Override
		public void listElementReplaced(ObservableList arg0, int arg1, Object arg2) {}
		@Override
		public void listElementsAdded(ObservableList arg0, int arg1, int arg2) {}
		@Override
		public void listElementsRemoved(ObservableList arg0, int arg1, List arg2) {}
		@Override
		public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
			MipssEpostEntity changed = (MipssEpostEntity)evt.getSource();
			String p = evt.getPropertyName();
			if (p.equals("ekstraDagsrapport")){
				if (evt.getNewValue()==(Boolean)true){
					if (changed.getEpostAdresse()==null||changed.getEpostAdresse().equals("")){
						showEpostMissingWarning();
						throw new PropertyVetoException("flarp!!", evt);
					}
				}
			}
			if (p.equals("sendeEpost")){
				if (evt.getNewValue()==(Boolean)true){
					if (changed.getEpostAdresse()==null||changed.getEpostAdresse().equals("")){
						showEpostMissingWarning();
						throw new PropertyVetoException("flarp!!", evt);
					}
				}
			}
		}
	}
	
	private void showEpostMissingWarning(){
		MipssDialogue dialog = new MipssDialogue(
				plugin.getLoader().getApplicationFrame(), IconResources.INFO_ICON, GuiTexts.getText(GuiTexts.MAIL_MISSING_TITLE),
				GuiTexts.getText(GuiTexts.MAIL_MISSING),
	            MipssDialogue.Type.WARNING,
	            new MipssDialogue.Button[] {MipssDialogue.Button.OK});
		dialog.askUser();
	}
	
	class EpostLoaderListener implements MipssBeanLoaderListener{
		private JXBusyLabel busyLabel;
		private final JScrollPane scroll;
		private Component view;
		
		public EpostLoaderListener(JScrollPane scroll){
			this.scroll = scroll;
			busyLabel = new JXBusyLabel();
			busyLabel.setText(GuiTexts.getText(GuiTexts.VENNLIGST_VENT));
			view = scroll.getViewport().getView();
		}
		
		@Override
		public void loadingFinished(MipssBeanLoaderEvent event) {
			busyLabel.setBusy(false);
			scroll.setViewportView(view);
		}

		@Override
		public void loadingStarted(MipssBeanLoaderEvent event) {
			view = scroll.getViewport().getView();
			busyLabel.setBusy(true);
			scroll.setViewportView(busyLabel);
			
		}
	}
}
