package no.mesta.mipss.excelrapport.swing;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Action;
import javax.swing.JPanel;

import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ReportModelObject;
import no.mesta.mipss.persistence.kontrakt.Rodetype;

/**
 * Hovedpanel for excelrapporter
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class ExcelreportPanel extends JPanel{

	private ExcelreportModule plugin;
	private JPanel optionsPanel;
	
	public ExcelreportPanel(ExcelreportModule plugin){
		this.plugin = plugin;
		initGui();
	}
	
	private void initGui(){
		setLayout(new BorderLayout());
		
		JPanel content = new JPanel(new GridBagLayout());
		CommonCriteriaPanel cpanel = new CommonCriteriaPanel(plugin, this);
				
		optionsPanel = new JPanel(new GridBagLayout());
		optionsPanel.add(new ProduksjonPanel(plugin), new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		
		content.add(cpanel, 		new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,0), 0,0));
		content.add(optionsPanel, 	new GridBagConstraints(0,1, 2,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,0), 0,0));
		add(content, BorderLayout.CENTER);
	}
	
	public void setReport(ReportModelObject.GROUP group, Action action){
		plugin.getRodetypeCombo().setEnabled(true);
		optionsPanel.removeAll();
		plugin.getReportController().setCurrentAction(action);
		
		switch (group){
			case PRODUKSJONSRAPPORTER:
				optionsPanel.add(new ProduksjonPanel(plugin), new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				break;
			case VINTERDRIFT:
				optionsPanel.add(new VinterdriftPanel(plugin), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				disableRodetypeCombo("Strø- og brøyterode");
				break;
			case VEIRAPPORT:
				optionsPanel.add(new ProduksjonPanel(plugin, false, true), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				break;
			case PRODUKSJON_UTEN_TILLEG_KOLONNER:
				optionsPanel.add(new ProduksjonPanel(plugin, true), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				disableRodetypeCombo("R12 rode");
				break;
			case DAURAPPORT:
				optionsPanel.add(new DAUPanel(plugin), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				plugin.getRodetypeCombo().setEnabled(false);
				break;
			case SAMPRODUKSJON:
				optionsPanel.add(new SamproduksjonPanel(plugin), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				break;
			case SAMMENDRAG:
				optionsPanel.add(new SammendragPanel(plugin), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				break;
			case PASSERING:
				optionsPanel.add(new PasseringPanel(plugin), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				break;
			case UKJENT_STROPRODUKT:
				optionsPanel.add(new UkjentStroproduktPanel(plugin), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
				break;
			default:
				break;
		}
		
		optionsPanel.revalidate();
		repaint();
		updateUI();
	}

	private void disableRodetypeCombo(String rodetype) {
		int count = plugin.getRodetypeCombo().getItemCount();
		for(int i = 0; i < count; i++) {
			Rodetype rodeTyper = (Rodetype) plugin.getRodetypeCombo().getModel().getElementAt(i);
			if (rodeTyper.getNavn().equals(rodetype)){
				plugin.getRodetypeCombo().setSelectedIndex(i);
				break;
			}
		}
		plugin.getRodetypeCombo().setEnabled(false);
	}

	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose() {
		plugin = null;
	}

}