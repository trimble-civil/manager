package no.mesta.mipss.excelrapport.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ResourceUtils;

@SuppressWarnings("serial")
public class SammendragPanel extends JPanel{

	private final ExcelreportModule plugin;

	private JCheckBox chkMnd;
	private JButton reportButton;
	
	public SammendragPanel(ExcelreportModule plugin){
		this.plugin = plugin;
		initComponents();
		initGui();
		bind();
	}
	
	private void initComponents(){
		chkMnd = new JCheckBox(ResourceUtils.getResource("label.sumMnd"));
		reportButton = new JButton();
		reportButton.setAction(new AbstractAction("Hent sammendragsrapport"){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.getReportController().getCurrentAction().actionPerformed(null);
			}
		});

	}
	private void initGui(){
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("label.sammendrag.title")));
		
		add(chkMnd, 		new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,30,0), 0,0));
		add(reportButton, 	new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));

	}
	private void bind() {
		BindingHelper.createbinding(chkMnd, "selected", plugin.getProdrapportParams(), "sumMnd").bind();
	}
}
