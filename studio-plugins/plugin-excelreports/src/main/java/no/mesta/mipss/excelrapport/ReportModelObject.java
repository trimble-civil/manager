package no.mesta.mipss.excelrapport;

import javax.swing.Action;

public class ReportModelObject {
	public enum REPORT_TYPE{
		KJORETOY,
		RODE,
		PERIODE,
		LEVERANDOR,
		VEI,
		VEI_UTVIDET,
		SAMPRODUKSJON,
		SAMMENDRAG,
		PASSERING,
		UKJENT_STROPRODUKT
	}
	
	public enum GROUP{
		PRODUKSJONSRAPPORTER,
		PRODUKSJON_UTEN_TILLEG_KOLONNER,
		VEIRAPPORT,
		VINTERDRIFT,
		DAURAPPORT,
		SAMPRODUKSJON,
		SAMMENDRAG,
		PASSERING,
		UKJENT_STROPRODUKT
	}
	private REPORT_TYPE reportType;
	private GROUP group;
	private final String text;
	private Action action;
	
	public ReportModelObject(REPORT_TYPE reportType, GROUP group, String text, Action action){
		this.reportType = reportType;
		this.group = group;
		this.text = text;
		this.action = action;
	}
	
	public REPORT_TYPE getReportType(){
		return reportType;
	}
	public GROUP getGroup(){
		return group;
	}
	public String getText(){
		return text;
	}
	public Action getAction(){
		return action;
	}
	@Override
	public String toString(){
		return text;
	}
}
