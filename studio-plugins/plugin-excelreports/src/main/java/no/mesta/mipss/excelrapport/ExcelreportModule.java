package no.mesta.mipss.excelrapport;

import java.beans.PropertyChangeListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.excelrapport.swing.ExcelreportActions;
import no.mesta.mipss.excelrapport.swing.ExcelreportPanel;
import no.mesta.mipss.messages.OpenDetaljertProdrapportMessage;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginMessage;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams.RapportType;

import org.jdesktop.beansbinding.Binding;
/**
 * Modul for å hente ut Excelrapporter fra systemet
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class ExcelreportModule extends MipssPlugin{
	
	private ProdrapportParams prodrapportParams;
	
	private JComboBox rodetypeCombo;
	private JComboBox rodeCombo;

	private ExcelreportPanel mainPanel;
	private FieldToolkit fieldToolkit;
	
	private ExcelreportActions actions;
	
	private ExcelreportController reportController;

	private JComboBox rodetypeUtenOverlappCombo;
	
	@Override
	public JPanel getModuleGUI() {
		return mainPanel;
	}

	@Override
	protected void initPluginGUI() {
		actions = new ExcelreportActions(this);
		prodrapportParams = new ProdrapportParams();
		prodrapportParams.setBrukerSign(getLoader().getLoggedOnUserSign());
		
		rodetypeCombo = new JComboBox();
		rodetypeCombo.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
		
		rodetypeUtenOverlappCombo = new JComboBox();
		rodetypeUtenOverlappCombo.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
		
		rodeCombo = new JComboBox();
		rodeCombo.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
		rodeCombo.setMaximumRowCount(15);
		
		reportController = new ExcelreportController(this);
		mainPanel = new ExcelreportPanel(this);
		
		Binding<JComboBox, Rodetype, ProdrapportParams, Long> rodetypeIdBind = BindingHelper.createbinding(getRodetypeCombo(), "${selectedItem.id}", getProdrapportParams(), "rodetypeId");
		Binding<JComboBox, Rodetype, ProdrapportParams, String> rodetypeNavnBind = BindingHelper.createbinding(getRodetypeCombo(), "${selectedItem.navn}", getProdrapportParams(), "rodetypeNavn");
		
		Binding<JComboBox, Rodetype, ProdrapportParams, Long> rodetypeUtenOverlappIdBind = BindingHelper.createbinding(getRodetypeUtenOverlappCombo(), "${selectedItem.id}", getProdrapportParams(), "rodetypeId");
		Binding<JComboBox, Rodetype, ProdrapportParams, String> rodetypeUtenOverlappNavnBind = BindingHelper.createbinding(getRodetypeUtenOverlappCombo(), "${selectedItem.navn}", getProdrapportParams(), "rodetypeNavn");

		
		Binding<JComboBox, Rode, ProdrapportParams, Long> rodeIdBind = BindingHelper.createbinding(getRodeCombo(), "${selectedItem.id}", getProdrapportParams(), "rodeId");
		Binding<JComboBox, Rode, ProdrapportParams, String> rodeNavnBind = BindingHelper.createbinding(getRodeCombo(), "${selectedItem.navn}", getProdrapportParams(), "rodeNavn");
		getFieldToolkit().addBinding(rodetypeIdBind);
		getFieldToolkit().addBinding(rodeIdBind);
		getFieldToolkit().addBinding(rodetypeNavnBind);
		getFieldToolkit().addBinding(rodeNavnBind);
		getFieldToolkit().addBinding(rodetypeUtenOverlappIdBind);
		getFieldToolkit().addBinding(rodetypeUtenOverlappNavnBind);
		
		fieldToolkit.bind();
		getReportController().setDriftkontrakt(getLoader().getSelectedDriftkontrakt());
	}
	
	public ExcelreportActions getExcelreportActions(){
		return actions;
	}

    public FieldToolkit getFieldToolkit() {
        if (fieldToolkit==null){
            fieldToolkit = FieldToolkit.createToolkit();
        }
        return fieldToolkit;
    }
	@Override
	public void shutdown() {
		mainPanel.dispose();
		reportController.dispose();
		fieldToolkit.unbindAndRemove();
		for (PropertyChangeListener l:props.getPropertyChangeListeners()){
			props.removePropertyChangeListener(l);
		}
		
	}
	
	public JComboBox getRodetypeCombo() {
		return rodetypeCombo;
	}

	public JComboBox getRodetypeUtenOverlappCombo() {
		return rodetypeUtenOverlappCombo;
	}
	
	public JComboBox getRodeCombo() {
		return rodeCombo;
	}

	public ExcelreportController getReportController() {
		return reportController;
	}

	public ProdrapportParams getProdrapportParams() {
		return prodrapportParams;
	}
	
	/** {@inheritDoc} */
	@Override
	public void receiveMessage(PluginMessage<?, ?> message){
		if ((message instanceof OpenDetaljertProdrapportMessage)){
			OpenDetaljertProdrapportMessage m = (OpenDetaljertProdrapportMessage)message;
			ProdrapportParams params = new ProdrapportParams();
			params.setType(RapportType.SAMPRODUKSJON);
			Long rodetypeId = Long.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("FELLES", "stro_broyte_rodetype_id").getVerdi());
			params.setRodetypeId(rodetypeId);
			params.setDriftkontraktId(m.getDriftkontraktId());
			params.setKjoretoyId(m.getKjoretoyId());
			params.setFraDato(m.getFraDato());
			params.setTilDato(m.getTilDato());
			getExcelreportActions().getRapport(params);
			
			message.setConsumed(true);
		}else{
			throw new IllegalArgumentException("Unsupported message type");
		}
		
	}
    
}
