package no.mesta.mipss.excelrapport.swing;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Box.Filler;
/**
 * Utilklasse for vanlige UI operasjoner i modulen
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class UIUtils {
	
	public static JPanel getHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
		return panel;
	}
	
	public static JPanel getVertPanel(){
		JPanel panel =new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		return panel;
	}
	
	public static Component getHStrut(int width){
		return new Filler(new Dimension(width,0), new Dimension(width,0), 
				  new Dimension(width, 0));
	}
	
	public static Component getVStrut(int height){
		return new Filler(new Dimension(0,height), new Dimension(0,height), 
				  new Dimension(0, height));
	}
	
	public static void setComponentSize(Dimension s, JComponent c){
		c.setPreferredSize(s);
		c.setMaximumSize(s);
		c.setMinimumSize(s);
	}
	
	
}
