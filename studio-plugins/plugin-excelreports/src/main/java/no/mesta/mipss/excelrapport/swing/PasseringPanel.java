package no.mesta.mipss.excelrapport.swing;

import java.awt.Dialog;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ResourceUtils;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.ui.roadpicker.KontraktveinettDialog;

@SuppressWarnings("serial")
public class PasseringPanel extends JPanel{

	private JPanel thisPanel;
	private final ExcelreportModule plugin;
	private Veinettveireferanse valgtVeiRef;

	private JButton velgveiButton;
	private JLabel lblMeter;
	private JTextField txtKilometer;
	private JButton reportButton;
	private JLabel valgtPasseringspunkt;
	
	public PasseringPanel(ExcelreportModule plugin){
		this.plugin = plugin;
		thisPanel = this;
		initComponents();
		initGui();
	}
	
	private void initComponents(){
		velgveiButton = new JButton(ResourceUtils.getResource("button.passering.velgVei"));
		lblMeter = new JLabel(ResourceUtils.getResource("label.passering.km"));
		txtKilometer = new JTextField();
		reportButton = new JButton();
		valgtPasseringspunkt = new JLabel(ResourceUtils.getResource("label.passering.valgtPasseringspunkt"));
		
		txtKilometer.setVisible(false);	
		valgtPasseringspunkt.setVisible(false);
		lblMeter.setVisible(false);
		
		velgveiButton.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				KontraktveinettDialog dialog = new KontraktveinettDialog((Dialog)SwingUtilities.windowForComponent(new PasseringPanel(plugin)), 
						ResourceUtils.getResource("button.passering.velgVei"), plugin.getReportController().getDriftkontrakt(), true, false, false, false, false);
	    		dialog.setLocationRelativeTo(plugin.getModuleGUI());
	        	dialog.setVisible(true);
	        	List<Veinettveireferanse> valgte = dialog.getSelectedVeireferanser();
	        	if (valgte!=null){
	        		if (valgte.size()>1) {
	        			JOptionPane.showMessageDialog(thisPanel, ResourceUtils.getResource("message.passering.forMangeVeierMessage"), ResourceUtils.getResource("message.passering.forMangeVeierTitle"), JOptionPane.ERROR_MESSAGE);
	        		} else if (valgte.size() == 1) {
	        			valgtVeiRef = valgte.get(0);	
	        			int kmFra = new Double(valgtVeiRef.getFraKm() * 1000).intValue();
	        			int kmTil = new Double(valgtVeiRef.getTilKm() * 1000).intValue();
	        			String valgtVeirefString = valgtVeiRef.getFylkesnummer() + "/" + valgtVeiRef.getKommunenummer() + " " + valgtVeiRef.getVeikategori() + valgtVeiRef.getVeistatus() + valgtVeiRef.getVeinummer() + " HP" + valgtVeiRef.getHp() + " (" + kmFra + " - " + kmTil + ")   m:";	        			
	        			lblMeter.setText(valgtVeirefString);
	        			lblMeter.setVisible(true);
	        			velgveiButton.setText("Velg annen vei");
	        			txtKilometer.setVisible(true);
	        			reportButton.setEnabled(true);
	        			valgtPasseringspunkt.setVisible(true);
	        		}     		
	        	}				
			}
		});	
		
		reportButton.setAction(new AbstractAction(ResourceUtils.getResource("button.passering.hentRapport")){
			@Override
			public void actionPerformed(ActionEvent e) {
				Double valgtKm;
				
				try {
					valgtKm = Double.parseDouble(txtKilometer.getText()) / 1000;
				} catch (NumberFormatException nfe) {
					valgtKm = -1.0;
				}
				
				
				if (txtKilometer.getText().equals("")) {
        			JOptionPane.showMessageDialog(thisPanel, ResourceUtils.getResource("message.passering.meterMangler"), ResourceUtils.getResource("message.passering.meterManglerTitle"), JOptionPane.ERROR_MESSAGE);
				} else if (valgtKm < valgtVeiRef.getFraKm() || valgtKm > valgtVeiRef.getTilKm()) {
        			JOptionPane.showMessageDialog(thisPanel, ResourceUtils.getResource("message.passering.utenforValgtVeiref"), ResourceUtils.getResource("message.passering.utenforValgtVeirefTitle"), JOptionPane.ERROR_MESSAGE);

				}else{
					ProdrapportParams params = plugin.getProdrapportParams();
					params.setFylkesnummer(valgtVeiRef.getFylkesnummer());
					params.setKommunenummer(valgtVeiRef.getKommunenummer());
					params.setVeikategori(valgtVeiRef.getVeikategori());
					params.setVeistatus(valgtVeiRef.getVeistatus());
					params.setVeinummer(valgtVeiRef.getVeinummer());
					params.setHp(valgtVeiRef.getHp());
					params.setKm(Double.parseDouble(txtKilometer.getText()) / 1000);
					
					plugin.getReportController().getCurrentAction().actionPerformed(null);
				}							
			}
		});
		
		reportButton.setEnabled(false);

	}
	
	private void initGui(){
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("label.passering.title")));
		
		JPanel kmPanel = new JPanel(new GridLayout(1, 2));
		kmPanel.setSize(200, 20);
		kmPanel.add(lblMeter);
		kmPanel.add(txtKilometer);
		
		add(velgveiButton, 			new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(20,5,10,0), 0,0));
		add(valgtPasseringspunkt,	new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(20,5,10,0), 0,0));		
		add(kmPanel, 				new GridBagConstraints(0,3, 1,1, 3.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,0), 0,0));		
		add(reportButton, 			new GridBagConstraints(0,4, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));

	}
}
