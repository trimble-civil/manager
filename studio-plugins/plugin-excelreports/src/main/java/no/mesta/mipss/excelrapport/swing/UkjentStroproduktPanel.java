package no.mesta.mipss.excelrapport.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ResourceUtils;

@SuppressWarnings("serial")
public class UkjentStroproduktPanel extends JPanel {

	private final ExcelreportModule plugin;
	private JLabel infoTekstLabel;
	private JButton hentRapportButton;

	public UkjentStroproduktPanel(ExcelreportModule plugin) {
		this.plugin = plugin;
		initComponents();
		initGUI();
	}

	private void initComponents() {

		infoTekstLabel = new JLabel(ResourceUtils.getResource("label.ukjentStrometode.info"));
		hentRapportButton = new JButton();

		hentRapportButton.setAction(new AbstractAction(ResourceUtils.getResource("button.ukjentStrometode.hentRapport")) {

			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.getReportController().getCurrentAction().actionPerformed(null);

			}
		});
	}

	private void initGUI() {

		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("panel.ukjentStrometode.title")));

		add(infoTekstLabel, 		new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(20,5,10,0), 0,0));
		add(hentRapportButton,		new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(20,5,10,0), 0,0));				
	}
}
