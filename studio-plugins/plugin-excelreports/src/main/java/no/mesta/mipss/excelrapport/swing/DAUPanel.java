package no.mesta.mipss.excelrapport.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;

import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ResourceUtils;
import no.mesta.mipss.excelrapport.swing.ExcelreportActions.ReportActionType;
import no.mesta.mipss.persistence.kontrakt.Rodetype;

@SuppressWarnings("serial")
public class DAUPanel extends JPanel{

	private final ExcelreportActions actions;
	private final ExcelreportModule plugin;
//	private JCheckBox averageCheck;
//	private JCheckBox frequencyCheck;
//	private JCheckBox percentCheck;
	private JButton sendDauButton;

	public DAUPanel(ExcelreportModule plugin){
		this.plugin = plugin;
		this.actions = plugin.getExcelreportActions();
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		JLabel rodetypeLabel = new JLabel();
		rodetypeLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgrodetype"));
		JComboBox rodetypeCombo = plugin.getRodetypeCombo();
		rodetypeCombo.setPreferredSize(new Dimension(300, rodetypeCombo.getPreferredSize().height));

		JLabel rodeLabel = new JLabel();
		rodeLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgrode"));
		JComboBox rodeCombo = plugin.getRodeCombo();
		rodeCombo.setPreferredSize(new Dimension(300, rodeCombo.getPreferredSize().height));

//		averageCheck = new JCheckBox(ResourceUtils.getResource("excelreports.AdditionalColumnsPanel.average"));
//		frequencyCheck = new JCheckBox(ResourceUtils.getResource("excelreports.AdditionalColumnsPanel.frequency"));
//		percentCheck = new JCheckBox(ResourceUtils.getResource("excelreports.AdditionalColumnsPanel.percent"));
		
		sendDauButton = new JButton(getSendDauAction());
		
		JLabel previewLabel = new JLabel();
		previewLabel.setText(ResourceUtils.getResource("excelreports.ActionButtonPanel.dau.label"));
		JLabel sendDauLabel = new JLabel();
		sendDauLabel.setText(ResourceUtils.getResource("excelreports.ActionButtonPanel.dau.send.label"));
		
		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("panel.DAU.title")));
		
//		add(rodetypeLabel, 	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,19), 0,0));
//		add(rodetypeCombo, 	new GridBagConstraints(1,0, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
//		add(rodeLabel, 		new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,19), 0,0));
//		add(rodeCombo, 		new GridBagConstraints(1,1, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
//		add(averageCheck, 	new GridBagConstraints(0,2, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));
//		add(frequencyCheck, new GridBagConstraints(0,3, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));
//		add(percentCheck, 	new GridBagConstraints(0,4, 3,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));
		add(sendDauButton, 	new GridBagConstraints(0,6, 2,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,10,5), 0,0));
		add(sendDauLabel, 	new GridBagConstraints(2,6, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		
		int count = plugin.getRodetypeCombo().getItemCount();
		for (int i=0;i<count;i++){
			Rodetype rt = (Rodetype)plugin.getRodetypeCombo().getModel().getElementAt(i);
			if (rt.getNavn().equals("DAU-rode")){
				plugin.getRodetypeCombo().setSelectedIndex(i);
				break;
			}
		}
		bind();
	}
	private void bind() {
		Binding enableComponents = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getReportController(), BeanProperty.create("rodetypeNotNull"), this, BeanProperty.create("componentsEnabled"));
		enableComponents.bind();
		
		
		Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getReportController(), BeanProperty.create("dauRodetypeValgt"), this, BeanProperty.create("dauRodetypeValgt")).bind();
	}
	
	public void setComponentsEnabled(boolean enabled){
		//previewButton.setEnabled(enabled);
		//sendDauButton.setEnabled(enabled);
//		averageCheck.setEnabled(enabled);
//		frequencyCheck.setEnabled(enabled);
//		percentCheck.setEnabled(enabled);
	}
	
	public void setDauRodetypeValgt(boolean dauRodetype){
		sendDauButton.setEnabled(dauRodetype);
//		previewButton.setEnabled(dauRodetype);	
	}
	
	private AbstractAction getSendDauAction(){
		return actions.getAction(ReportActionType.DAU_SEND, ResourceUtils.getResource("button.DAU.send"), null);
	}
}
