package no.mesta.mipss.excelrapport.swing;

import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.ejb.EJBTransactionRolledbackException;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.driftslogg.DriftsloggBean;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.driftslogg.NoArtikkelException;
import no.mesta.mipss.driftslogg.rapport.*;
import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ResourceUtils;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.Daubestilling;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Object;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.rapportfunksjoner.UkjentStroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Vinterdrift;
import no.mesta.mipss.produksjonsrapport.Prodrapport;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams.RapportType;
import no.mesta.mipss.produksjonsrapport.query.Rapp1Query;
import no.mesta.mipss.ui.WaitPanel;
import no.mesta.mipss.util.ukjentproduksjon.UkjentStroproduktRapport;

import no.mesta.mipss.util.vinterdriftsklasser.VinterdriftsklasserRapport;
import org.apache.commons.io.IOUtils;

/**
 * Klasse som inneholder actionmetodene for rapportgenerering. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class ExcelreportActions {
	
	public enum ReportActionType{
		KJORETOY,
		RODE,
		PERIODE,
		LEVERANDOR,
		VEI,
		VINTERDRIFT,
		VEI_UTVIDET,
		DAU_SEND,
		DAU,
		SAMPRODUKSJON, 
		SAMMENDRAG, 
		PASSERING,
		UKJENT_STROPRODUKT
	}
	private ExcelreportModule plugin;

	public ExcelreportActions(ExcelreportModule plugin){
		this.plugin= plugin;
		
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		plugin = null;
	}
	public AbstractAction getAction(ReportActionType type, String text, Icon icon){
		AbstractAction action = null;
		switch (type) {
			case KJORETOY:
				action = new KjoretoyAction(text, icon);
				action.putValue("name", ResourceUtils.getResource("kjoretoy"));
				break;
			case RODE:
				action = new RodeAction(text, icon);
				action.putValue("name", "Rode");
				break;
			case PERIODE:
				action = new PeriodeAction(text, icon);
				action.putValue("name", "Periode");
				break;
			case LEVERANDOR:
				action = new LeverandorAction(text, icon);
				action.putValue("name", ResourceUtils.getResource("leverandor"));
				break;
			case DAU_SEND:
				action = new DauSendAction(text, icon);
				break;
			case VEI:
				action = new VeiAction(text, icon);
				action.putValue("name", "Vei");
				break;
			case VINTERDRIFT:
				action = new VinterdriftAction(text, icon);
				action.putValue("name", "Vinterdrift");
				break;
			case VEI_UTVIDET:
				action = new VeiUtvidetAction(text, icon);
				action.putValue("name", "R12-");
				break;
			case SAMPRODUKSJON:
				action = new SamproduksjonAction(text, icon);
				break;
			case SAMMENDRAG:
				action = new SammendragAction(text, icon);
				action.putValue("name", "Sammendrags");
				break;
			case PASSERING:
				action = new PasseringAction(text, icon);
				action.putValue("name", "Passerings");
				break;
			case UKJENT_STROPRODUKT:
				action = new UkjentStroproduktAction(text, icon);
				action.putValue("name", "Ukjent strøprodukt");
			default:
				break;
		}
		return action;
	}
	
	private static String getTempDir() {
		String tempdir = System.getProperty("java.io.tmpdir");
    	return tempdir;
    }

	private JDialog displayWait(){
		JDialog dialog = new JDialog(SwingUtilities.getWindowAncestor(plugin.getModuleGUI()));
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		WaitPanel panel = new WaitPanel("Vennligst vent mens rapporten genereres");
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		return dialog;
	}

	public void getRapport(final ProdrapportParams params) {
		plugin.getReportController().setRapportFailed(false);
		plugin.getReportController().setRapportSendt(false);
		plugin.getReportController().setGeneratingReport(true);
		final JDialog dialog = displayWait();
		dialog.setVisible(true);
		SwingWorker<?, ?> w = new SwingWorker<Object, Object>(){
			
			public Object doInBackground() {
				Prodrapport rapp = BeanUtil.lookup(Prodrapport.BEAN_NAME, Prodrapport.class);
				if (params.getType() == RapportType.UKJENT_STROPRODUKT) {
					try {
						List<UkjentStroprodukt> ukjenteStroprodukter = rapp.genererUkjentStroproduktRapport(params.getDriftkontraktId(), params.getFraDato(), params.getTilDato(), null);
						if (ukjenteStroprodukter == null || ukjenteStroprodukter.isEmpty()) {
							JOptionPane.showMessageDialog(SwingUtilities.windowForComponent(plugin.getModuleGUI()), "Fant ingen data for utvalget", "Ingen data funnet", JOptionPane.INFORMATION_MESSAGE);
						} else {
							new UkjentStroproduktRapport(ukjenteStroprodukter, params.getFraDato(), params.getTilDato(), null).finalizeAndOpen();
						}
						dialog.dispose();
						return null;
					} catch (Exception e) {
                        rapportFeil(dialog, e);
						return null;
					}
				} else if (params.getType() == RapportType.VINTERDRIFT) {
                    Driftkontrakt kontrakt = plugin.getReportController().getDriftkontrakt();

                    try {
                        List<Vinterdrift> vinterdrift = rapp.genererVinterdriftklasserRapport(kontrakt, params.getRodetypeId(), params.getRodeId(), params.getFraDato(), params.getTilDato());

                        if (vinterdrift == null || vinterdrift.isEmpty()) {
                            JOptionPane.showMessageDialog(SwingUtilities.windowForComponent(plugin.getModuleGUI()), "Fant ingen data for utvalget", "Ingen data funnet", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            new VinterdriftsklasserRapport(kontrakt, vinterdrift, params.getFraDato(), params.getTilDato()).finalizeAndOpen();
                        }

                        dialog.dispose();
                        return null;
                    } catch (Exception e) {
                        rapportFeil(dialog, e);
                        return null;
                    }
				} else {
					File f = new File(getTempDir()+getRapportFileName(params, 0));
					int c = 0;
					while (f.exists()){
						f = new File(getTempDir()+getRapportFileName(params, c++));
					}
					byte[] rapport=null;
					try {
						rapport = rapp.getRapport(params);
					}catch (EJBTransactionRolledbackException e){
						dialog.dispose();
						ErrorHandler.showError(e, ResourceUtils.getResource("error.timeout"));
						e.printStackTrace();
						plugin.getReportController().setGeneratingReport(false);
						return null;
					}
					catch (Exception e) {
                        rapportFeil(dialog, e);
						return null;
					}
					
					if (rapport!=null){
						try {
							FileOutputStream fo = new FileOutputStream(f);
							System.out.println(f);
							ByteArrayInputStream st = new ByteArrayInputStream(rapport);
							IOUtils.copy(st,fo);
							IOUtils.closeQuietly(fo);
							IOUtils.closeQuietly(st);
							DesktopHelper.openFile(f);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						dialog.dispose();
					}else{
						dialog.dispose();
						JOptionPane.showMessageDialog(SwingUtilities.windowForComponent(plugin.getModuleGUI()), "Fant ingen data for utvalget", "Ingen data funnet", JOptionPane.INFORMATION_MESSAGE);
						
					}
					plugin.getReportController().setGeneratingReport(false);
					return null;
				}	
			}
		};
		w.execute();
	}

    private void rapportFeil(JDialog dialog, Exception e) {
        dialog.dispose();
        plugin.getLoader().handleException(e, plugin, "Feil under henting av Excelrapport");
        e.printStackTrace();
        plugin.getReportController().setGeneratingReport(false);
    }
	
	private String getRapportFileName(ProdrapportParams p, int c){
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		String fileName = f.format(Clock.now());
		String knr = plugin.getReportController().getDriftkontrakt().getKontraktnummer();
		String ext = p.getType().equals(RapportType.PERIODE)?".xls":".xlsx";
		if (c==0){
			fileName+="-"+p.getType()+"-"+knr+ext;
		}else{
			fileName+="-"+p.getType()+"-"+knr+"("+c+")"+ext;
		}
		return fileName;
	}
	
	
	private class KjoretoyAction extends AbstractAction{
		
		public KjoretoyAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.KJORETOY);
			getRapport(params);
		}
	}
	
	private class RodeAction extends AbstractAction{
		
		public RodeAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.RODE);
			getRapport(params);
		}
	}
	
	private class PeriodeAction extends AbstractAction{
		
		public PeriodeAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.PERIODE);
			getRapport(params);
		}
	}
	
	private class LeverandorAction extends AbstractAction{
		
		public LeverandorAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.LEVERANDOR);
			getRapport(params);
		}
	}
	
	private class DauSendAction extends AbstractAction{
		public DauSendAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			int rvl = JOptionPane.showConfirmDialog(plugin.getLoader().getApplicationFrame(), "Advarsel! Dette sender data direkte til statens vegvesen. Ok=send data.", "Sende data til SVV", JOptionPane.OK_CANCEL_OPTION);
			if (rvl==JOptionPane.OK_OPTION){
				ProdrapportParams params = plugin.getProdrapportParams();
				Daubestilling bestilling = new Daubestilling();
				bestilling.setKontraktId(params.getDriftkontraktId());
				bestilling.setRodeId(params.getRodeId());
				bestilling.setPeriodeFra(params.getFraDato());
				bestilling.setPeriodeTil(params.getTilDato());
				String sign = plugin.getLoader().getLoggedOnUserSign();
				bestilling.setOpprettetAv(sign);
				bestilling.setOpprettetDato(Clock.now());
				try{
					Prodrapport rapp = BeanUtil.lookup(Prodrapport.BEAN_NAME, Prodrapport.class);
					rapp.sendDauBestilling(bestilling);
					plugin.getReportController().setRapportSendt(true);
					JOptionPane.showMessageDialog(plugin.getLoader().getApplicationFrame(), "Data sendt til SVV", "Data sendt ok", JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception ex){
					plugin.getReportController().setRapportFailed(true);
				}
			}
		}
	}
	
	private class VeiAction extends AbstractAction{
		public VeiAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.VEI);
			getRapport(params);
			
		}
	}

	private class VinterdriftAction extends AbstractAction {

		public VinterdriftAction(String text, Icon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.VINTERDRIFT);
			getRapport(params);
		}

	}
	
	private class VeiUtvidetAction extends AbstractAction{
		public VeiUtvidetAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.R12);
			getRapport(params);
		}
	}
	
	
	private class SamproduksjonAction extends AbstractAction{
		public SamproduksjonAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.SAMPRODUKSJON);
			getRapport(params);
			
		}
	}
	private class SammendragAction extends AbstractAction{
		public SammendragAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.SAMMENDRAG);
			getRapport(params);
		}
	}
	
	private class PasseringAction extends AbstractAction{
		public PasseringAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.PASSERING);
			getRapport(params);
		}
	}
	
	private class UkjentStroproduktAction extends AbstractAction{
		public UkjentStroproduktAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			ProdrapportParams params = plugin.getProdrapportParams();
			params.setType(ProdrapportParams.RapportType.UKJENT_STROPRODUKT);
			getRapport(params);
		}
	}
}
