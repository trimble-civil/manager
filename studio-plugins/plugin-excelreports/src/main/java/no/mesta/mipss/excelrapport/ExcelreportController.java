package no.mesta.mipss.excelrapport;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.Action;
import javax.swing.JComboBox;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.excelrapport.swing.ExcelreportActions.ReportActionType;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.rodeservice.Rodegenerator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;

/**
 * Kontrollerklasse for excelrapportmodulen. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ExcelreportController implements PropertyChangeListener{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("excelreportsText");

	private Logger log = LoggerFactory.getLogger(this.getClass());
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	
	private Driftkontrakt driftkontrakt;
	private Rodetype rodetype;
	private Rode rode;
	private Date fraDato;
	private Date tilDato;
	
    private boolean driftkontraktNotNull;
    private boolean rodetypeNotNull;
    private boolean dauRodetypeValgt;
    private boolean generatingReport;
    
	private ExcelreportModule plugin;
	private MipssMap mipssMap;
	private Rodegenerator rodeBean;
	
	private JComboBoxBinding<Rodetype, List<Rodetype>, JComboBox> rodetypeBinding;
	private JComboBoxBinding<Rode, List<Rode>, JComboBox> rodeBinding;

	private boolean rapportSendt;

	private boolean rapportFailed;

	private boolean kunAktiveRoder;

	private JComboBoxBinding<Rodetype, List<Rodetype>, JComboBox> rodetypeUtenOverlappBinding;
	
	private Action currentAction;
	
	public ExcelreportController(ExcelreportModule plugin){
		this.plugin = plugin;
		mipssMap = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
		rodeBean = BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class);
		plugin.getFieldToolkit().bindDropDown(plugin.getRodetypeCombo(), 			 ExcelreportController.class, this, "rodetype", 			null, Rodetype.class, getRodetyper(null), 			  resources.getGuiString("excelreports.ExcelreportController.kontraktForst"));
		plugin.getFieldToolkit().bindDropDown(plugin.getRodetypeUtenOverlappCombo(), ExcelreportController.class, this, "rodetypeUtenOverlapp", null, Rodetype.class, getRodetyperUtenOverlapp(null), resources.getGuiString("excelreports.ExcelreportController.kontraktForst"));
		plugin.getFieldToolkit().bindDropDown(plugin.getRodeCombo(), ExcelreportController.class, this, "rode", null, Rode.class, getRoder(null), resources.getGuiString("excelreports.ExcelreportController.rodetypeForst"));
		
		Binding driftkontraktNotNull = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, this, BeanProperty.create("driftkontraktNotNull"), plugin.getRodetypeCombo(), BeanProperty.create("enabled"));
		Binding rodetypeNotNull = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, this, BeanProperty.create("rodetypeNotNull"), plugin.getRodeCombo(), BeanProperty.create("enabled"));
		
		plugin.getFieldToolkit().addBinding(driftkontraktNotNull);
		plugin.getFieldToolkit().addBinding(rodetypeNotNull);
		setCurrentAction(plugin.getExcelreportActions().getAction(ReportActionType.KJORETOY, null, null));
	}
	public void setCurrentAction(Action currentAction){
		this.currentAction = currentAction;
	}
	public Action getCurrentAction(){
		return currentAction;
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		plugin = null;
		rodeBinding=null;
		for (PropertyChangeListener l:props.getPropertyChangeListeners()){
			props.removePropertyChangeListener(l);
		}
		props = null;
	}
    /**
     * Setter den valgte driftkontrakten
     * @param driftkontrakt
     */
    public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
        Driftkontrakt old = this.driftkontrakt;
        this.driftkontrakt = driftkontrakt;
        props.firePropertyChange("driftkontrakt", old, driftkontrakt);
        setDriftkontraktNotNull(driftkontrakt!=null);
        
        try{
//	        plugin.getReportTypesCombo().setSelectedIndex(0);
	        plugin.getRodetypeCombo().setSelectedIndex(0);
	        plugin.getRodetypeCombo().setEnabled(true);
        } catch (Throwable t){
        }
        if (rodetypeBinding!=null)
        	rodetypeBinding.unbind();
        rodetypeBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ_WRITE, getRodetyper(driftkontrakt==null?null:driftkontrakt.getId()), plugin.getRodetypeCombo() );
        rodetypeBinding.bind();
        
        if (rodetypeUtenOverlappBinding!=null){
        	rodetypeUtenOverlappBinding.unbind();
        }
        rodetypeUtenOverlappBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ_WRITE, getRodetyperUtenOverlapp(driftkontrakt==null?null:driftkontrakt.getId()), plugin.getRodetypeUtenOverlappCombo() );
        rodetypeUtenOverlappBinding.bind();
    }
    
    /**
     * Setter den valgte rodetypen
     * @param rodetype
     */
    public void setRodetype(Rodetype rodetype){
    	Rodetype old = this.rodetype;
    	this.rodetype = rodetype;
    	props.firePropertyChange("rodetype", old, rodetype);
    	props.firePropertyChange("rodetypeUtenOverlapp", old, rodetype);
    	setRodetypeNotNull(rodetype!=null);
    	if (rodetype!=null){
    		setRodetypeNotNull(rodetype.getId()!=null);
    	}
    	
    	if (rodeBinding!=null)
    		rodeBinding.unbind();
    	rodeBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ, getRoder(rodetype==null?null:rodetype.getId()), plugin.getRodeCombo());
    	rodeBinding.bind();
    	
    	log.trace("Setter ny rodetype");
    }
    
    public void setRodetypeUtenOverlapp(Rodetype rodetype){
    	setRodetype(rodetype);
    }
    
    /**
     * Setter den valgte roden
     * @param rode
     */
    public void setRode(Rode rode){
    	Rode old = this.rode;
    	this.rode = rode;
    	props.firePropertyChange("rode", old, rode);
    	
    	if (rode!=null&&rode.getRodetype()!=null&&rode.getRodetype().getGenererDauFlagg()!=null)
			setDauRodetypeValgt(Boolean.valueOf(rode.getRodetype().getGenererDauFlagg()));
		else
			setDauRodetypeValgt(false);
    	log.trace("Setter ny rode");
    }
    
    /**
     * Setter fradatoen til rapportutvalget
     * @param fraDato
     */
    public void setFraDato(Date fraDato){
    	Date old = this.fraDato;
    	this.fraDato = fraDato;
    	props.firePropertyChange("fraDato", old, fraDato);
    }
    
    /**
     * Setter tildatoen til rapportutvalget
     * @param tilDato
     */
    public void setTilDato(Date tilDato){
    	Date old = this.tilDato;
    	this.tilDato = tilDato;
    	props.firePropertyChange("tilDato", old, tilDato);
    }
    
    
    /**
     * Returnerer den valgte driftkontrakten
     * @return
     */
    public Driftkontrakt getDriftkontrakt(){
    	return driftkontrakt;
    }
    
    /**
     * Returnerer den valgte rodetypen
     * @return
     */
    public Rodetype getRodetype(){
    	return rodetype;
    }
    public Rodetype getRodetypeUtenOverlapp(){
    	return rodetype;
    }
    
    /**
     * Returnerer den valgte roden (kan være null)
     * @return
     */
    public Rode getRode(){
    	return rode;
    }
    
    /**
     * Returnerer fradatoen til rapportutvalget
     * @return
     */
    public Date getFraDato(){
    	return fraDato;
    }
    
    /**
     * Returnerer tildatoen til rapportutvalget
     * @return
     */
    public Date getTilDato(){
    	return tilDato;
    }
    
    /**
     * Setter driftkontraktNotNull, true hvis driftkontrakt er vaglt
     * 
     * @param driftkontraktNotNull
     */
    public void setDriftkontraktNotNull(boolean driftkontraktNotNull) {
        boolean old = this.driftkontraktNotNull;
        this.driftkontraktNotNull = driftkontraktNotNull;
        props.firePropertyChange("driftkontraktNotNull", old, driftkontraktNotNull);
        log.debug("setting driftkontraktnotnull:"+driftkontraktNotNull);
    }
    
    /**
     * True hvis driftkontrakt er valgt
     * @return
     */
    public boolean isDriftkontraktNotNull(){
    	return driftkontraktNotNull;
    }
    
    /**
     * Setter rodetypeNotNull, true hvis rodetype er valgt
     * @param rodetypeNotNull
     */
    public void setRodetypeNotNull(boolean rodetypeNotNull){
    	boolean old = this.rodetypeNotNull;
    	this.rodetypeNotNull = rodetypeNotNull;
    	props.firePropertyChange("rodetypeNotNull", old, rodetypeNotNull);
    }
    
    /**
     * True hvis rodetype er valgt
     * @return
     */
    public boolean isRodetypeNotNull(){
    	return rodetypeNotNull;
    }
    
    /**
     * Setter dauRodetypeValgt, true hvis valgte rodetype er DAU
     * @param dauRodetypeValgt
     */
    public void setDauRodetypeValgt(boolean dauRodetypeValgt){
    	boolean old = this.dauRodetypeValgt;
    	this.dauRodetypeValgt = dauRodetypeValgt;
    	props.firePropertyChange("dauRodetypeValgt", old, dauRodetypeValgt);
    }
    
    public void setKunAktiveRoder(boolean kunAktiveRoder){
		this.kunAktiveRoder = kunAktiveRoder;
		setRodetype(getRodetype());
    	
    }
    /**
     * True hvis valgte rodetype er DAU
     * @return
     */
    public boolean isDauRodetypeValgt(){
    	return dauRodetypeValgt;
    }
    
    public void setGeneratingReport(boolean generatingReport){
    	boolean old = this.generatingReport;
    	this.generatingReport = generatingReport;
    	props.firePropertyChange("generatingReport", old, generatingReport);
    }
    
    
    public boolean isGeneratingReport(){
    	return generatingReport;
    }
    /**
     * Henter rodetyper fra sessionbean
     * @return
     */
    public List<Rodetype> getRodetyper(Long driftkontraktId){
        log.trace("Henter rodetyper");
        List<Rodetype> rodetypeForKontrakt =null;
        if (driftkontraktId!=null){
        	rodetypeForKontrakt = mipssMap.getRodetypeForKontrakt(driftkontraktId);
        	if (rodetypeForKontrakt.isEmpty()){
        		Rodetype empty = new Rodetype();
        		empty.setNavn("Kontrakten har ingen rodetyper...");
        		rodetypeForKontrakt.add(empty);
        	}
        }
        if (rodetypeForKontrakt!=null){
        	Collections.sort(rodetypeForKontrakt, new Comparator<Rodetype>() {
				@Override
				public int compare(Rodetype r1, Rodetype r2) {
					return r1.getId().compareTo(r2.getId());
				}
			});
        }
        return rodetypeForKontrakt;
    }
    
    public List<Rodetype> getRodetyperUtenOverlapp(Long driftkontraktId){
    	List<Rodetype> rodetypeForKontrakt=null;
    	List<Rodetype> utenOverlapp = new ArrayList<Rodetype>();
    	if (driftkontraktId!=null){
        	rodetypeForKontrakt = mipssMap.getRodetypeForKontrakt(driftkontraktId);
        	if (rodetypeForKontrakt.isEmpty()){
        		Rodetype empty = new Rodetype();
        		empty.setNavn("Kontrakten har ingen rodetyper...");
        		rodetypeForKontrakt.add(empty);
        	}else{
        		for (Rodetype r:rodetypeForKontrakt){
        			if ((r.getKanHaOverlappFlagg()==null)||!r.getKanHaOverlappFlagg()){
        				utenOverlapp.add(r);
        			}
        		}
        	}
        }
    	
    	return utenOverlapp;
    }
    /**
     * Henter roder fra sessionbean
     * @param rodetypeId
     * @return
     */
    public List<Rode> getRoder(Long rodetypeId){
    	List<Rode> rodeList=null;
    	if (driftkontrakt!=null&&rodetypeId!=null){
    		
	    	rodeList = rodeBean.getRodeForKontrakt(driftkontrakt.getId(), rodetypeId, Boolean.FALSE, Boolean.valueOf(kunAktiveRoder));
	    	
//    		rodeList = mipssMap.getRodeForKontrakt(driftkontrakt.getId(), rodetypeId);
    		if (rodeList.isEmpty()){
    			Rode empty = new Rode();
        		empty.setNavn(resources.getGuiString("excelreports.ExcelreportController.manglerRoder"));
        		rodeList.add(empty);
    		}else{
    			if (rodeList.size()>1){
	    			Rode empty = new Rode();
		    		empty.setNavn(resources.getGuiString("excelreports.ExcelreportController.alleValgt"));
		    		rodeList.add(0, empty);
    			}
	    		
    		}
    	}
    	return rodeList;
    }
	
    
    @Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();
		if (StringUtils.equals("valgtKontrakt", property)) {
			Driftkontrakt now = (Driftkontrakt) evt.getNewValue();
			setDriftkontrakt(now);
		}
	}
    
	/**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + l + ")");
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + propName + ", " + l + ")");
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + l + ")");
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + propName + ", " + l + ")");
        props.removePropertyChangeListener(propName, l);
    }

	public void setRapportSendt(boolean rapportSendt) {
		boolean old = this.rapportSendt;
		this.rapportSendt = rapportSendt;
		props.firePropertyChange("rapportSendt", old, rapportSendt);
	}
	public boolean isRapportSendt(){
		return rapportSendt;
	}
	public void setRapportFailed(boolean rapportFailed){
		boolean old = this.rapportFailed;
		this.rapportFailed = rapportFailed;
		props.firePropertyChange("rapportFailed", old, rapportFailed);
	}
	public boolean isRapportFailed(){
		return rapportFailed;
	}
	
}
