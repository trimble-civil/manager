package no.mesta.mipss.excelrapport.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.excelrapport.ExcelreportController;
import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ResourceUtils;
import no.mesta.mipss.excelrapport.swing.ExcelreportActions.ReportActionType;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.KjoretoySokPanel;
import no.mesta.mipss.ui.table.KjoretoyTableObject;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.decorator.HighlighterFactory;

public class SamproduksjonPanel extends JPanel{

	private ExcelreportModule plugin;
	private KjoretoySokPanel kjoretoyPanel;
	private JLabel rodetypeLabel;
	private JComboBox rodetypeCombo;
	private JLabel rodeLabel;
	private JComboBox rodeCombo;
	private JCheckBox kunAktiveCheck;

	public SamproduksjonPanel(ExcelreportModule plugin){
		this.plugin = plugin;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		kjoretoyPanel = new KjoretoySokPanel(plugin.getReportController().getDriftkontrakt(), false, true, false, new String[] {"enhetId", "beskrivelse", "type", "modell", "leverandor", "regnr" });
		kjoretoyPanel.getKjoretoyTable().setGridColor(new Color(0,0,0,0));
		kjoretoyPanel.setSingleSelection(true);

		kjoretoyPanel.addPropertyChangeListener("kjoretoy", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				KjoretoyTableObject obj = (KjoretoyTableObject)evt.getNewValue();
				if (obj.getKjoretoy()!=null){
					plugin.getProdrapportParams().setKjoretoyId(obj.getKjoretoy().getId());
				}else{
					plugin.getProdrapportParams().setKjoretoyId(null);
				}
			}
		});
		plugin.getReportController().addPropertyChangeListener("driftkontrakt", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getNewValue()!=null)
					kjoretoyPanel.getKjoretoy();			
			}
		});
		kjoretoyPanel.getKjoretoyTable().setHighlighters(HighlighterFactory.createAlternateStriping());
		Dimension size = new Dimension(500, 150);
		kjoretoyPanel.setMinimumSize(size);
		kjoretoyPanel.setPreferredSize(size);
		
		rodetypeLabel = new JLabel();
		rodetypeLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgrodetype"));
		rodetypeCombo = plugin.getRodetypeUtenOverlappCombo();
		rodetypeCombo.setPreferredSize(new Dimension(300, rodetypeCombo.getPreferredSize().height));

		rodeLabel = new JLabel();
		rodeLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgrode"));
		rodeCombo = plugin.getRodeCombo();
		rodeCombo.setPreferredSize(new Dimension(300, rodeCombo.getPreferredSize().height));
		kunAktiveCheck = new JCheckBox(ResourceUtils.getResource("excelreports.MainCriteriaPanel.kunAktive"));
		BindingHelper.createbinding(kunAktiveCheck, "selected", plugin.getReportController(), "kunAktiveRoder").bind();
		kunAktiveCheck.setSelected(true);

		
		JPanel rodePanel = new JPanel(new GridBagLayout());
		rodePanel.add(rodetypeLabel, 		new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,19), 0,0));
		rodePanel.add(rodetypeCombo, 		new GridBagConstraints(2,0, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		rodePanel.add(rodeLabel, 			new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,19), 0,0));
		rodePanel.add(rodeCombo, 			new GridBagConstraints(2,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		rodePanel.add(kunAktiveCheck, 		new GridBagConstraints(3,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));

		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("panel.samproduksjon.title")));
		
		JButton hentRapportButton = new JButton(plugin.getExcelreportActions().getAction(ReportActionType.SAMPRODUKSJON, ResourceUtils.getResource("button.samproduksjon"), null));
		
		add(rodePanel, 			new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,0), 0,0));
		add(kjoretoyPanel, 		new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,5,10,0), 0,0));
		add(hentRapportButton, 	new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,0), 0,0));
		
		
		bind();
	}

	private void bind() {
		Binding<ExcelreportController, Driftkontrakt, KjoretoySokPanel, Driftkontrakt> driftkontraktBind = BindingHelper.createbinding(plugin.getReportController(), "driftkontrakt", kjoretoyPanel, "valgtKontrakt");
		driftkontraktBind.bind();
		
	}
}
