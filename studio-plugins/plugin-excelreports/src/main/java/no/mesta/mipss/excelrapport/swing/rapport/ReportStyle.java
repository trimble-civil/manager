package no.mesta.mipss.excelrapport.swing.rapport;

import jxl.format.Alignment;
import jxl.format.BoldStyle;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.DateFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;

public class ReportStyle {

	private Colour tableHeaderBackground = Colour.DARK_BLUE;
	private Colour tableHeaderForeground = Colour.WHITE;
	private WritableFont tableHeaderFont = new WritableFont(WritableFont.ARIAL, 12, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, tableHeaderForeground);
	
	
	private WritableFont dataFont = new WritableFont(WritableFont.ARIAL, 10);
	private WritableFont sumFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

	private DateFormat timeFormat = new DateFormat("[hh]:mm:ss");
	
	private WritableCellFormat tableHeaderFormat = new WritableCellFormat(tableHeaderFont);
	private WritableCellFormat dataTimeFormat = new WritableCellFormat(dataFont, timeFormat);
	private WritableCellFormat dataFormat = new WritableCellFormat(dataFont);
	
	private WritableCellFormat sumTimeFormat = new WritableCellFormat(sumFont, timeFormat);
	private WritableCellFormat sumFormat = new WritableCellFormat(sumFont);
	private WritableCellFormat veinummerFormat = new WritableCellFormat(sumFont);
	
	
	public ReportStyle(){
		try {
			tableHeaderFormat.setBackground(tableHeaderBackground);
			tableHeaderFormat.setAlignment(Alignment.CENTRE);
			tableHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
			tableHeaderFormat.setBorder(Border.LEFT, BorderLineStyle.HAIR, Colour.WHITE);
			tableHeaderFormat.setBorder(Border.RIGHT, BorderLineStyle.HAIR, Colour.WHITE);
			
			sumFormat.setBorder(Border.TOP, BorderLineStyle.MEDIUM, Colour.BLACK);
			sumFormat.setBorder(Border.BOTTOM, BorderLineStyle.DOUBLE, Colour.BLACK);
			sumTimeFormat.setBorder(Border.TOP, BorderLineStyle.MEDIUM, Colour.BLACK);
			sumTimeFormat.setBorder(Border.BOTTOM, BorderLineStyle.DOUBLE, Colour.BLACK);
		} catch (WriteException e) {
			e.printStackTrace();
		}
	}
	
	public WritableCellFormat getTableHeaderFormat(){
		return tableHeaderFormat;
	}
	public WritableCellFormat getDataTimeFormat(){
		return dataTimeFormat;
	}
	public WritableCellFormat getDataFormat(){
		return dataFormat;
	}
	public WritableCellFormat getSumFormat(){
		return sumFormat;
	}
	public WritableCellFormat getSumTimeFormat(){
		return sumTimeFormat;
	}
	public WritableCellFormat getVeinummerFormat(){
		return veinummerFormat;
	}
	public static String convertFromSekundTilTimestring(Long sekunder) {       
	    String format = String.format("%%0%dd", 2);  
	    String seconds = String.format(format, sekunder % 60);  
	    String minutes = String.format(format, (sekunder % 3600) / 60);  
	    String hours = String.format(format, sekunder / 3600);  
	    String time =  hours + ";" + minutes + ";" + seconds;  
	    return time;  
	}  
}

