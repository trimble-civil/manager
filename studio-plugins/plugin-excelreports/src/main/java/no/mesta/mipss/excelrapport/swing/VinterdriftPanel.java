package no.mesta.mipss.excelrapport.swing;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ResourceUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class VinterdriftPanel extends JPanel {

	private final ExcelreportModule plugin;

	private JLabel rodetypeLabel;
	private JLabel rodeLabel;
	private JComboBox rodeCombo;
	private JComboBox rodetypeCombo;
	private JCheckBox kunAktiveCheck;
	private JButton hentRapportButton;

	public VinterdriftPanel(ExcelreportModule plugin) {
		this.plugin = plugin;
		initComponents();
		initGUI();
	}

	private void initComponents() {
		rodetypeLabel = new JLabel();
		rodetypeLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgrodetype"));
		rodetypeCombo = plugin.getRodetypeCombo();
		rodetypeCombo.setPreferredSize(new Dimension(300, rodetypeCombo.getPreferredSize().height));

		rodeLabel = new JLabel();
		rodeLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgrode"));
		rodeCombo = plugin.getRodeCombo();
		rodeCombo.setPreferredSize(new Dimension(300, rodeCombo.getPreferredSize().height));

		kunAktiveCheck = new JCheckBox(ResourceUtils.getResource("excelreports.MainCriteriaPanel.kunAktive"));
		BindingHelper.createbinding(kunAktiveCheck, "selected", plugin.getReportController(), "kunAktiveRoder").bind();
		kunAktiveCheck.setSelected(true);

		hentRapportButton = new JButton();
		hentRapportButton.setAction(new AbstractAction(ResourceUtils.getResource("button.vinterdrift.hentRapport")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.getReportController().getCurrentAction().actionPerformed(null);
			}
		});
	}

	private void initGUI() {
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("panel.vinterdrift.title")));

		final JPanel rodePanel = new JPanel(new GridBagLayout());
		settRodePanel(rodePanel);

		add(rodePanel,				new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,5,0), 0,0));
		add(hentRapportButton,		new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
	}

	private void settRodePanel(final JPanel rodePanel) {
		rodePanel.removeAll();
		rodePanel.add(rodetypeLabel, 		new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,19), 0,0));
		rodePanel.add(rodetypeCombo, 		new GridBagConstraints(2,0, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		rodePanel.add(rodeLabel, 			new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,19), 0,0));
		rodePanel.add(rodeCombo, 			new GridBagConstraints(2,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		rodePanel.add(kunAktiveCheck, 		new GridBagConstraints(3,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		rodePanel.revalidate();
		plugin.getProdrapportParams().setVeiliste(null);
	}

}