package no.mesta.mipss.excelrapport;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class ResourceUtils {
	private static PropertyResourceBundleUtil properties;
	private static final String BUNDLE_NAME = "excelreportsText";
	
	
	public static String getResource(String key){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key);
	}
}