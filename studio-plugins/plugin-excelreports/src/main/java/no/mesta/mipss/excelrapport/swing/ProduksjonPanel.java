package no.mesta.mipss.excelrapport.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ResourceUtils;
import no.mesta.mipss.ui.roadpicker.KontraktVeinettPanel;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;

/**
 * TODO trenger en liten refactoring..
 * @author harkul
 *
 */
@SuppressWarnings("serial")
public class ProduksjonPanel extends JPanel{
	
	private final ExcelreportModule plugin;
	private JButton reportButton;
	private JCheckBox kunKontraktCheck;
	private JCheckBox averageCheck;
	private JCheckBox frequencyCheck;
	private JCheckBox percentCheck;
	private boolean disableTilleggsKolonner;
	private final boolean veivelger;
	private JLabel rodetypeLabel;
	private JLabel rodeLabel;
	private JComboBox rodeCombo;
	private JComboBox rodetypeCombo;
	private JCheckBox kunAktiveCheck;
	private KontraktVeinettPanel kontraktveinettPanel;
	private JCheckBox checkKm;
	
	private JLabel lblRodetypeStro;
	
	public ProduksjonPanel(ExcelreportModule plugin, boolean disableTilleggsKolonner, boolean veivelger){
		this.plugin = plugin;
		this.disableTilleggsKolonner = disableTilleggsKolonner;
		this.veivelger = veivelger;
		initComponents();
		initGui();
	}
	
	public ProduksjonPanel(ExcelreportModule plugin, boolean disableTilleggsKolonner){
		this(plugin, disableTilleggsKolonner, false);
	}
	
	public ProduksjonPanel(ExcelreportModule plugin){
		this(plugin, false);
	}

	private void initComponents(){
		rodetypeLabel = new JLabel();
		rodetypeLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgrodetype"));
		rodetypeCombo = plugin.getRodetypeCombo();
		rodetypeCombo.setPreferredSize(new Dimension(300, rodetypeCombo.getPreferredSize().height));

		rodeLabel = new JLabel();
		rodeLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgrode"));
		rodeCombo = plugin.getRodeCombo();
		rodeCombo.setPreferredSize(new Dimension(300, rodeCombo.getPreferredSize().height));
		
		kunAktiveCheck = new JCheckBox(ResourceUtils.getResource("excelreports.MainCriteriaPanel.kunAktive"));
		BindingHelper.createbinding(kunAktiveCheck, "selected", plugin.getReportController(), "kunAktiveRoder").bind();
		kunAktiveCheck.setSelected(true);
		
		kunKontraktCheck = new JCheckBox(ResourceUtils.getResource("excelreports.AdditionalColumnsPanel.kunKontraktKjoretoy"));
		averageCheck = new JCheckBox(ResourceUtils.getResource("excelreports.AdditionalColumnsPanel.average"));
		frequencyCheck = new JCheckBox(ResourceUtils.getResource("excelreports.AdditionalColumnsPanel.frequency"));
		percentCheck = new JCheckBox(ResourceUtils.getResource("excelreports.AdditionalColumnsPanel.percent"));
		
		if (disableTilleggsKolonner){
			averageCheck.setEnabled(false);
			frequencyCheck.setEnabled(false);
			percentCheck.setEnabled(false);
		}
		reportButton = new JButton("Hent rapport");

		reportButton.setAction(new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (kontraktveinettPanel!=null)
					plugin.getProdrapportParams().setVeiliste(kontraktveinettPanel.getValgte());
				plugin.getReportController().getCurrentAction().actionPerformed(null);
			}
		});
		reportButton.setText("Hent "+plugin.getReportController().getCurrentAction().getValue("name")+"rapport");
		
		lblRodetypeStro = new JLabel(ResourceUtils.getResource("label.rodetypeStro"));
		lblRodetypeStro.setVisible(false);
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("panel.produksjon.title")));
		
		ButtonGroup radioGroup = new ButtonGroup();
		final JRadioButton radioRode = new JRadioButton("Pr rode");
		final JRadioButton radioVei = new JRadioButton("Pr vei");
		checkKm = new JCheckBox("Vis km");
		final JPanel rodePanel = new JPanel(new GridBagLayout());
		
		checkKm.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.getProdrapportParams().setInkluderKmForVeiliste(checkKm.isSelected());
				settVeivelgerPanel(rodePanel);
			}
		});
		BindingHelper.createbinding(radioVei, "selected", checkKm, "enabled").bind();
		radioGroup.add(radioRode);
		radioGroup.add(radioVei);
		
		
		radioRode.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				settRodePanel(rodePanel);
			}
		});
		radioVei.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				settVeivelgerPanel(rodePanel);
			}
		});
		
		radioRode.setSelected(true);
		
		if (veivelger){
			add(radioRode, 		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,5,5), 0,0));
			add(radioVei, 		new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
			add(checkKm, 		new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
			add(lblRodetypeStro,new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,10,5,5), 0,0));
		}
		add(rodePanel, 		new GridBagConstraints(0,2, 4,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,5,0,19), 0,0));
		add(averageCheck, 	new GridBagConstraints(0,4, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,5,5), 0,0));
		add(frequencyCheck, new GridBagConstraints(0,5, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,5,5), 0,0));
		add(percentCheck, 	new GridBagConstraints(0,6, 4,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,5,5), 0,0));
		add(reportButton, 	new GridBagConstraints(0,7, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));
		bind();
	}
	private void settRodePanel(final JPanel rodePanel) {
		
		lblRodetypeStro.setVisible(false);
		kontraktveinettPanel = null;
		rodePanel.removeAll();
		rodePanel.add(rodetypeLabel, 		new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,19), 0,0));
		rodePanel.add(rodetypeCombo, 		new GridBagConstraints(2,0, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		rodePanel.add(rodeLabel, 			new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,19), 0,0));
		rodePanel.add(rodeCombo, 			new GridBagConstraints(2,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		rodePanel.add(kunAktiveCheck, 		new GridBagConstraints(3,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		rodePanel.add(kunKontraktCheck, 	new GridBagConstraints(1,2, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,5,5), 0,0));
		rodePanel.revalidate();
		plugin.getProdrapportParams().setVeiliste(null);
	}
	private void settVeivelgerPanel(final JPanel rodePanel) {
		rodePanel.removeAll();
		
		lblRodetypeStro.setVisible(true);
		kontraktveinettPanel = new KontraktVeinettPanel(plugin.getReportController().getDriftkontrakt(), plugin.getProdrapportParams().getInkluderKmForVeiliste(), false, true, true, true);
		kontraktveinettPanel.setPreferredSize(new Dimension(400, 400));
		kontraktveinettPanel.setMinimumSize(new Dimension(400,190));
		rodePanel.add(kontraktveinettPanel, 		new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,5,5,5), 0,0));
		rodePanel.revalidate();
	}
	private void bind() {
		BindingHelper.createbinding(kunKontraktCheck, "selected", plugin.getProdrapportParams(), "kunKontraktKjoretoy").bind();
		BindingHelper.createbinding(averageCheck, "selected", plugin.getProdrapportParams(), "gjennomsnitt").bind();
		BindingHelper.createbinding(frequencyCheck, "selected", plugin.getProdrapportParams(), "frekvens").bind();
		BindingHelper.createbinding(percentCheck, "selected", plugin.getProdrapportParams(), "prosent").bind();
		Binding enableComponents = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getReportController(), BeanProperty.create("rodetypeNotNull"), this, BeanProperty.create("componentsEnabled"));
		enableComponents.bind();
	}
	public void setComponentsEnabled(boolean enabled){
		reportButton.setEnabled(enabled);
		kunKontraktCheck.setEnabled(enabled);
		if (!disableTilleggsKolonner){
			averageCheck.setEnabled(enabled);
			frequencyCheck.setEnabled(enabled);
			percentCheck.setEnabled(enabled);
			
		}
	}

}
