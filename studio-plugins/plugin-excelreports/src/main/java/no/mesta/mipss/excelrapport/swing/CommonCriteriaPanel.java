package no.mesta.mipss.excelrapport.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.excelrapport.ReportModelObject;
import no.mesta.mipss.excelrapport.ResourceUtils;
import no.mesta.mipss.excelrapport.swing.ExcelreportActions.ReportActionType;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.util.DatePickerConstraintsUtils;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;

@SuppressWarnings("serial")
public class CommonCriteriaPanel extends JPanel{
	
	private final ExcelreportModule plugin;
	private JMipssContractPicker contractPicker;
	private MipssDateTimePickerHolder fraDato;
	private MipssDateTimePickerHolder tilDato;
	private JToggleButton btnKjoretoy;
	private JToggleButton btnPeriode;
	private JToggleButton btnRode;
	private JToggleButton btnLeverandor;
	private JToggleButton btnVei;
	private JToggleButton btnVinterdrift;
	private JToggleButton btnR12;
	private JToggleButton btnAnalyse;
	private JToggleButton btnSammendrag;
	private JToggleButton btnPassering;
	private JToggleButton btnUkjentStroprodukt;
	private final ExcelreportPanel reportPanel;
	
	public CommonCriteriaPanel(ExcelreportModule plugin, ExcelreportPanel reportPanel){
		this.plugin = plugin;
		this.reportPanel = reportPanel;
		initButtons();
		initGui();
	}

	private void initButtons(){
		btnKjoretoy = new JToggleButton(ResourceUtils.getResource("excelreports.ActionButtonPanel.kjoretoy.button"), IconResources.BIL);
		btnKjoretoy.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.PRODUKSJONSRAPPORTER, plugin.getExcelreportActions().getAction(ReportActionType.KJORETOY, null, null));
			}
		});

		btnPeriode = new JToggleButton(ResourceUtils.getResource("excelreports.ActionButtonPanel.periode.button"), IconResources.ROTATECCW_ICON);
		btnPeriode.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.PRODUKSJONSRAPPORTER, plugin.getExcelreportActions().getAction(ReportActionType.PERIODE, null, null));
			}
		});

		btnRode = new JToggleButton(ResourceUtils.getResource("excelreports.ActionButtonPanel.rode.button"), IconResources.VIAPUNKT_ICON);
		btnRode.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.PRODUKSJONSRAPPORTER, plugin.getExcelreportActions().getAction(ReportActionType.RODE, null, null));
			}
		});

		btnLeverandor = new JToggleButton(ResourceUtils.getResource("excelreports.ActionButtonPanel.leverandor.button"), IconResources.SAK_ICON_16);
		btnLeverandor.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.PRODUKSJONSRAPPORTER, plugin.getExcelreportActions().getAction(ReportActionType.LEVERANDOR, null, null));
			}
		});

		btnVei = new JToggleButton("Veirapport", IconResources.VEI_ICON);
		btnVei.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.VEIRAPPORT, plugin.getExcelreportActions().getAction(ReportActionType.VEI, null, null));
			}
		});

		btnVinterdrift = new JToggleButton("<html><center>R12 vinterdriftsklasser</center></html>", IconResources.TABLE3_ICON);
		btnVinterdrift.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.VINTERDRIFT, plugin.getExcelreportActions().getAction(ReportActionType.VINTERDRIFT, null, null));
			}
		});

		btnR12 = new JToggleButton("R12 - Grunnlag", IconResources.SPREDER_ICON);
		btnR12.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.PRODUKSJON_UTEN_TILLEG_KOLONNER, plugin.getExcelreportActions().getAction(ReportActionType.VEI_UTVIDET, null, null));
			}
		});

		btnAnalyse = new JToggleButton("<html><center>Analyse / Detaljert<wbr> produksjonsrapport</center></html>", IconResources.GRAF_ICON_SMALL);
		btnAnalyse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.SAMPRODUKSJON, plugin.getExcelreportActions().getAction(ReportActionType.SAMPRODUKSJON, null, null));
			}
		});

		btnSammendrag = new JToggleButton("Sammendrag", IconResources.DETALJER_ICON_16);
		btnSammendrag.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.SAMMENDRAG, plugin.getExcelreportActions().getAction(ReportActionType.SAMMENDRAG, null, null));				
			}
		});

		btnSammendrag.setToolTipText(ResourceUtils.getResource("button.sammendrag.tooltip"));
		
		//Passeringsrapport
		btnPassering = new JToggleButton(ResourceUtils.getResource("button.passeringer"), IconResources.POINT_SMALL);
		btnPassering.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.PASSERING, plugin.getExcelreportActions().getAction(ReportActionType.PASSERING, null, null));							
			}
		});
		btnPassering.setToolTipText(ResourceUtils.getResource("button.passering.tooltip"));
		
		//Ukjent Strøprodukt
		btnUkjentStroprodukt = new JToggleButton(ResourceUtils.getResource("button.ukjentStrometode"), IconResources.SPREDER_ICON_32);
		btnUkjentStroprodukt.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				reportPanel.setReport(ReportModelObject.GROUP.UKJENT_STROPRODUKT, plugin.getExcelreportActions().getAction(ReportActionType.UKJENT_STROPRODUKT, null, null));
			}
		});
		btnUkjentStroprodukt.setToolTipText(ResourceUtils.getResource("button.ukjentStrometode.tooltip"));
		
		ButtonGroup group = new ButtonGroup();
		group.add(btnKjoretoy);
		group.add(btnPeriode);
		group.add(btnRode);
		group.add(btnLeverandor);
		group.add(btnVei);
		group.add(btnVinterdrift);
		group.add(btnR12);
		group.add(btnAnalyse);
		group.add(btnSammendrag);
		group.add(btnPassering);
		group.add(btnUkjentStroprodukt);
		setButtonSize(btnKjoretoy);
		setButtonSize(btnPeriode);
		setButtonSize(btnRode);
		setButtonSize(btnLeverandor);
		setButtonSize(btnVei);
		setButtonSize(btnVinterdrift);
		setButtonSize(btnR12);
		setButtonSize(btnAnalyse);
		setButtonSize(btnSammendrag);
		setButtonSize(btnPassering);
		setButtonSize(btnUkjentStroprodukt);
	}
	
	private void setButtonSize(JToggleButton btn){
		Dimension size = new Dimension(100,70);
		btn.setPreferredSize(size);
		btn.setMaximumSize(size);
		btn.setMinimumSize(size);
		btn.setSize(size);
		btn.setMargin(new Insets(0,0,0,0));
		btn.setVerticalTextPosition(SwingConstants.BOTTOM);
		btn.setHorizontalTextPosition(SwingConstants.CENTER);
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder("Utvalg"));
		contractPicker = new JMipssContractPicker(plugin, false);
		contractPicker.getDropdown().setMaximumRowCount(30);
		contractPicker.addPropertyChangeListener("valgtKontrakt", plugin.getReportController());
		contractPicker.setPreferredSize(new Dimension(300, contractPicker.getPreferredSize().height));
		contractPicker.setMinimumSize(new Dimension(300, 23));
		
		JPanel pnlRapporttype = new JPanel(new GridLayout(2, 5));
		pnlRapporttype.add(btnKjoretoy);
		pnlRapporttype.add(btnPeriode);
		pnlRapporttype.add(btnRode);
		pnlRapporttype.add(btnLeverandor);
		pnlRapporttype.add(btnVei);
		pnlRapporttype.add(btnVinterdrift);
		pnlRapporttype.add(btnR12);
		pnlRapporttype.add(btnAnalyse);
		pnlRapporttype.add(btnSammendrag);
		pnlRapporttype.add(btnPassering);
		pnlRapporttype.add(btnUkjentStroprodukt);
		
		
		
		JMipssDatePicker fraPicker = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		JMipssDatePicker tilPicker = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		fraPicker.pairWith(tilPicker);
        DatePickerConstraintsUtils.setNotBeforeDate(plugin.getLoader(), fraPicker, tilPicker);

		
		Dimension spinnerSize = new Dimension(70, 22);
		JSpinner fraSpinner = new JSpinner(new SpinnerDateModel(fraPicker.getDate(), null, null, Calendar.MINUTE));
        fraSpinner.setPreferredSize(spinnerSize);
        fraSpinner.setMinimumSize(spinnerSize);
        fraSpinner.setMaximumSize(spinnerSize);
        fraSpinner.setEditor(new JSpinner.DateEditor(fraSpinner, "HH:mm:ss"));
        
        Date midnight = tilPicker.getDate();
		Calendar c = Calendar.getInstance();
		c.setTime(midnight);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		
		JSpinner tilSpinner = new JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        tilSpinner.setPreferredSize(spinnerSize);
        tilSpinner.setMinimumSize(spinnerSize);
        tilSpinner.setMaximumSize(spinnerSize);
        tilSpinner.setEditor(new JSpinner.DateEditor(tilSpinner, "HH:mm:ss"));
        
        fraDato = new MipssDateTimePickerHolder(fraPicker, fraSpinner);
        tilDato = new MipssDateTimePickerHolder(tilPicker, tilSpinner);
        
        
        JLabel kontraktLabel = new JLabel();
        kontraktLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.velgkontrakt"));
        JLabel rapportLabel = new JLabel();
        rapportLabel.setText(ResourceUtils.getResource("label.velgrapport"));
        JLabel fraDatoLabel = new JLabel();
        fraDatoLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.fraDato"));
        JLabel tilDatoLabel = new JLabel();
        tilDatoLabel.setText(ResourceUtils.getResource("excelreports.MainCriteriaPanel.tilDato"));
        
        JPanel fraDatoPanel = new JPanel(new GridBagLayout());
        JPanel tilDatoPanel = new JPanel(new GridBagLayout());
        
        fraDatoPanel.add(fraPicker, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        fraDatoPanel.add(fraSpinner, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        
        tilDatoPanel.add(tilPicker, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        tilDatoPanel.add(tilSpinner, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        
		add(kontraktLabel,    new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));
		add(contractPicker,   new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,1,10,5), 0,0));
		add(rapportLabel,  	  new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));
		add(pnlRapporttype, new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		add(fraDatoLabel,  	  new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));
		add(fraDatoPanel, 	  new GridBagConstraints(1,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		add(tilDatoLabel,  	  new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,10,5), 0,0));
		add(tilDatoPanel, 	  new GridBagConstraints(1,3, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		bind();
	}
	
	private void bind(){
		Binding enableComponents = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, plugin.getReportController(), BeanProperty.create("rodetypeNotNull"), this, BeanProperty.create("componentsEnabled"));
		enableComponents.bind();
		
		Binding<JMipssContractPicker, Long, ProdrapportParams, Long> driftkontraktIdBind = BindingHelper.createbinding(contractPicker, "${valgtKontrakt.id}", plugin.getProdrapportParams(), "driftkontraktId");
		Binding<JMipssContractPicker, String, ProdrapportParams, String> driftkontraktNavnBind = BindingHelper.createbinding(contractPicker, "${valgtKontrakt.textForGUI}", plugin.getProdrapportParams(), "kontraktNavn");
		plugin.getFieldToolkit().addBinding(driftkontraktIdBind);
		plugin.getFieldToolkit().addBinding(driftkontraktNavnBind);
		
		
		Binding<MipssDateTimePickerHolder, Date, ProdrapportParams, Date> fraDatoBind = BindingHelper.createbinding(fraDato, "date", plugin.getProdrapportParams(), "fraDato");
		Binding<MipssDateTimePickerHolder, Date, ProdrapportParams, Date> tilDatoBind = BindingHelper.createbinding(tilDato, "date", plugin.getProdrapportParams(), "tilDato");
		plugin.getFieldToolkit().addBinding(fraDatoBind);
		plugin.getFieldToolkit().addBinding(tilDatoBind);
	}
	public void setComponentsEnabled(boolean enabled){
		btnKjoretoy.setEnabled(enabled);
		btnPeriode.setEnabled(enabled);
		btnRode.setEnabled(enabled);
		btnLeverandor.setEnabled(enabled);
		btnVei.setEnabled(enabled);
		btnVinterdrift.setEnabled(enabled);
		btnR12.setEnabled(enabled);
		btnAnalyse.setEnabled(enabled);
		btnSammendrag.setEnabled(enabled);
		btnPassering.setEnabled(enabled);
		btnUkjentStroprodukt.setEnabled(enabled);
	}
}
