package no.mesta.mipss.brukerstotte;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JList;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.konfigparam.DokumentService;
import no.mesta.mipss.logg.MipssLogger;
import no.mesta.mipss.persistence.BrukerLoggType.LoggType;
import no.mesta.mipss.persistence.dokarkiv.Dokformat;
import no.mesta.mipss.persistence.dokarkiv.Dokument;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.list.MipssRenderableEntityModel;

public class BrukerstotteController {
	private DokumentService dokument = BeanUtil.lookup(DokumentService.BEAN_NAME, DokumentService.class);
	private final BrukerstotteModule plugin;
	
	public BrukerstotteController(BrukerstotteModule plugin){
		this.plugin = plugin;
		
	}
	
	public List<Dokument> getAlleBrukerstotteDokumenter(){
		return dokument.getBrukerstotteDokumenter();
	}
	
	@SuppressWarnings("serial")
	public Action getOpenBrukerstotteAction(final MipssRenderableEntityModel<Dokument> mdlDokumenter, final JList lstDokumenter){
		return new AbstractAction(Resources.getResource("open"), IconResources.SHORTCUT_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				int index = lstDokumenter.getSelectedIndex();
				if (index==-1)
					return;
				Dokument item = mdlDokumenter.getElementAt(index);;
				openBrukerstotte(item);
			}
		};
	}
	
	public void openBrukerstotte(Dokument dokument){
		Dokformat dokformat = dokument.getDokformat();
		String filsti = dokument.getFilsti();

		// Henter hele pathen fra databasen.
		String[] cmd = new String[2];
		cmd[0] = dokformat.getStandardProgram();
		cmd[1] = filsti;

		ProcessBuilder pb = new ProcessBuilder(cmd);
		try {
			pb.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		MipssLogger log = new MipssLogger(plugin.getLoader().getLoggedOnUserSign());
		log.logg(LoggType.BRUKERSTOTTE, dokument.getNavn());
	}
}
