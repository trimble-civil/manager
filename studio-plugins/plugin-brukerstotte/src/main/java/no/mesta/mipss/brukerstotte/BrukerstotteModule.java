package no.mesta.mipss.brukerstotte;

import javax.swing.JComponent;

import no.mesta.mipss.plugin.MipssPlugin;

@SuppressWarnings("serial")
public class BrukerstotteModule extends MipssPlugin {
	private BrukerstottePanel modulGui;
	
	@Override
	public JComponent getModuleGUI() {
		return modulGui;
	}

	@Override
	protected void initPluginGUI() {
		BrukerstotteController controller = new BrukerstotteController(this);
		modulGui = new BrukerstottePanel(controller);
	}

	@Override
	public void shutdown() {
		
	}

}
