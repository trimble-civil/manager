package no.mesta.mipss.brukerstotte;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics2D;

import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import no.mesta.mipss.persistence.dokarkiv.Dokformat;
import no.mesta.mipss.persistence.dokarkiv.Dokument;
import no.mesta.mipss.persistence.dokarkiv.Ikon;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.painter.Painter;

public class DokumentListAlternatingCellRenderer implements ListCellRenderer{

	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		Dokument doc = null;
		if(value != null && value instanceof Dokument) { //.class.equals(value.getClass())) {
			doc = (Dokument) value;
		}
		
		String text = null;
		if(doc != null) {
			text = doc.getTextForGUI();
		} else {
			text = Resources.getResource("message.ingendokumenter");
		}
		JXHeader header = new JXHeader(text, doc.getFritekst(), new IconMapper(doc.getDokformat()).getIcon());
		if (index%2!=0){
			header.setBackgroundPainter(getAlternatingPainter());
		}
			
		if (isSelected){
				header.setBackgroundPainter(getSelectedPainter());
		}
		
		return header;
	}
	
	private Painter<JXHeader> getAlternatingPainter(){
		return new Painter<JXHeader>(){
			@Override
			public void paint(Graphics2D g, JXHeader object, int width,int height) {
				g.setPaint(new Color(239,239,239));
				g.fillRect(0, 0, width, height);
			}
		};
	}
	
	private Painter<JXHeader> getSelectedPainter(){
		return new Painter<JXHeader>(){
			@Override
			public void paint(Graphics2D g, JXHeader object, int width,int height) {
				GradientPaint gp = new GradientPaint(0, 0, new Color(208, 223, 244), 0, height, new Color(240,244,249));
				g.setPaint(gp);
				g.fillRect(0, 0, width, height);
				GradientPaint gp2 = new GradientPaint(width, height, new Color(208, 223, 244), 0, 0, new Color(240,244,249));
				g.setPaint(gp2);
				g.drawRect(0, 0, width, height-1);
			}
			
		};
	}
	
	class IconMapper{
		private final Dokformat doc;
		
		public IconMapper(Dokformat doc){
			this.doc = doc;
			
		}
		public ImageIcon getIcon(){
			Ikon ikon = doc.getIkon();
			if (ikon==null){
				if (doc.getExt().equals("xls"))
					return IconResources.EXCEL_ICON16;
				if (doc.getExt().equals("xlsx"))
					return IconResources.EXCEL_ICON16;
				if (doc.getExt().equals("ppt"))
					return IconResources.POWERPOINT_ICON16;
				if (doc.getExt().equals("pptx"))
					return IconResources.POWERPOINT_ICON16;
				if (doc.getExt().equals("doc"))
					return IconResources.WORD_ICON16;
				if (doc.getExt().equals("docx"))
					return IconResources.WORD_ICON16;
				return IconResources.ADOBE_16;
			}
			return new ImageIcon(ikon.getIkonLob());
		}
	}

}
