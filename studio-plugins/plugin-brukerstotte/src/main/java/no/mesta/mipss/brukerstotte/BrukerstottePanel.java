package no.mesta.mipss.brukerstotte;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import no.mesta.mipss.persistence.dokarkiv.Dokument;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.list.MipssRenderableEntityModel;

import org.jdesktop.swingx.JXHeader;

@SuppressWarnings("serial")
public class BrukerstottePanel extends JPanel{

	private final BrukerstotteController controller;
	
	private JList lstDokumenter;
	private JScrollPane scrDokumenter;
	private MipssRenderableEntityModel<Dokument> mdlDokumenter;
	private JButton btnOpen;

	public BrukerstottePanel(BrukerstotteController controller){
		this.controller = controller;
		initComponents();
		initGui();
	}
	
	private void initComponents() {
		mdlDokumenter = new MipssRenderableEntityModel<Dokument>(controller.getAlleBrukerstotteDokumenter());
		lstDokumenter = new JList(mdlDokumenter);
		lstDokumenter.addMouseListener(new MouseAdapter(){
			  public void mouseClicked(MouseEvent e){
				  if(e.getClickCount() == 2){
					  int index = lstDokumenter.locationToIndex(e.getPoint());
					  Dokument item = mdlDokumenter.getElementAt(index);;
					  lstDokumenter.ensureIndexIsVisible(index);
					  controller.openBrukerstotte(item);
				  }
			  }
		});
		lstDokumenter.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstDokumenter.setCellRenderer(new DokumentListAlternatingCellRenderer());
		btnOpen = new JButton(controller.getOpenBrukerstotteAction(mdlDokumenter, lstDokumenter));
		scrDokumenter = new JScrollPane(lstDokumenter);
//		scrDokumenter.setPreferredSize(new Dimension(300, 500));
		scrDokumenter.setMinimumSize(new Dimension(300, 500));
		lstDokumenter.getSelectionModel();
	}
	
	private JXHeader getHeader(){
		JXHeader head = new JXHeader(Resources.getResource("head.title"), Resources.getResource("head.description"), IconResources.QUESTION_ICON);
		return head;
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		JPanel panel = new JPanel(new GridBagLayout());
		panel.add(scrDokumenter, new GridBagConstraints(0, 0, 1,1, 0.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(getHeader(),new GridBagConstraints(0, 0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		add(panel, 		new GridBagConstraints(0, 1, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0,0));
		add(btnOpen,	new GridBagConstraints(0, 2, 1,1, 1.0,0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(15,0,20,0), 0,0));
	}
}
