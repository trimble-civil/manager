package no.mesta.mipss.tilstand.rapporter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.rapportfunksjoner.RegionTall;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;
import no.mesta.mipss.tilstand.gui.LedelsesrapportPanel;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFConditionalFormatting;
import org.apache.poi.xssf.usermodel.XSSFConditionalFormattingRule;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheetConditionalFormatting;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Ledelsesrapport {
	private static final String TILSTANDSRAPPORT_TEMPLATE = "tilstandsrapport_template";
	/** Antall kolonner med data som skal være en del av summering for betinget formattering */
	private static final int DATA_COLUMN_COUNT = 16;
	private static final int[] DATA_COLUMNS_CONDITIONAL_FORMATTING = new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12, 15};
	/** Index til første kolonne som skal være med i formattering og summeringer */
	private static final int DATA_COLUMN_START = 2;
	
	/** Index til betinget formattering i template for gradient fra rød = 0% til grønn = 100%*/
	private static final int FORMATTING_GRADIENT_RED_TO_GREEN_INDEX = 0;
	/** Index til betinget formattering i template for gradient fra grønn = 0% til rød = 100%*/
	private static final int FORMATTING_GRADIENT_GREEN_TO_RED_INDEX = 1;
	
	private final List<RegionTall> tilstandsrapport;
	private File file;
	private XSSFWorkbook workbook;
	
	private int startRow =8;
	private int rowsBetween = 3;
	private int endDataRow=0;
	
	public Ledelsesrapport(List<RegionTall> tilstandsrapport, int aar, int mnd){
		this.tilstandsrapport = tilstandsrapport;
		sort();
	
		Map<String, Object> regiontall = new HashMap<String, Object>();
		regiontall.put("regioner", tilstandsrapport);
		regiontall.put("aar", Integer.valueOf(aar));
		regiontall.put("maned", LedelsesrapportPanel.months[mnd]);
		regiontall.put("rapportdato", Clock.now());
		file = RapportHelper.createTempFile(TILSTANDSRAPPORT_TEMPLATE);
		workbook = RapportHelper.readTemplate(TILSTANDSRAPPORT_TEMPLATE, regiontall);
		
		List<Span> spans = getSpans();
		addConditionalFormatting(spans);
		addAverage(spans);
	}
	
	/**
	 * Legger på betinget formattering for alle dataradene. 
	 * 
	 * Ettersom POI ikke støtter denne typen betinget formattering ligger
	 * formatteringene i excel-templaten på rad-index 1, kolonne-index 0 og 1
	 * @param spans
	 */
	private void addConditionalFormatting(List<Span> spans){
		XSSFSheet sheet = workbook.getSheetAt(0);
		XSSFSheetConditionalFormatting sheetCF = sheet.getSheetConditionalFormatting();
		XSSFConditionalFormatting gradientInc = sheetCF.getConditionalFormattingAt(FORMATTING_GRADIENT_RED_TO_GREEN_INDEX);
		XSSFConditionalFormatting gradientDec = sheetCF.getConditionalFormattingAt(FORMATTING_GRADIENT_GREEN_TO_RED_INDEX);
		XSSFConditionalFormattingRule rule = gradientInc.getRule(0);
		XSSFConditionalFormattingRule rule2 = gradientDec.getRule(0);
		
		for (int i:DATA_COLUMNS_CONDITIONAL_FORMATTING){
			List<CellRangeAddress> kontraktRanges = new ArrayList<CellRangeAddress>();
			for (Span s:spans){
				CellRangeAddress kontraktRange= new CellRangeAddress(s.getStartRow(), s.getEndRow(), DATA_COLUMN_START+i, DATA_COLUMN_START+i);
				kontraktRanges.add(kontraktRange);
			}
			CellRangeAddress[] rangeArray = kontraktRanges.toArray(new CellRangeAddress[kontraktRanges.size()]);
			if (i==2||i==15){
				sheetCF.addConditionalFormatting(rangeArray, rule2);
			}else{
				sheetCF.addConditionalFormatting(rangeArray, rule);
			}
		}
	}
	
	/**
	 * Legger på celler for beregning av gjennomsnitt i Mesta Drift
	 * @param spans
	 */
	private void addAverage(List<Span> spans){
		for (int i=0;i<DATA_COLUMN_COUNT;i++){ 
			List<String> cellRefInAverage = new ArrayList<String>();
			for (Span s:spans){
				int averageDataRow = s.getEndRow()+1;
				CellReference ref = new CellReference(averageDataRow, DATA_COLUMN_START+i);
				cellRefInAverage.add(ref.formatAsString());
			}
			
			String avgCells = StringUtils.join(cellRefInAverage, ",");
			StringBuilder formula = new StringBuilder();
			formula.append("IF(ISERROR(AVERAGE(");
			formula.append(avgCells);
			formula.append(")),\"\", ");
			formula.append("AVERAGE(");
			formula.append(avgCells);
			formula.append("))");
			XSSFSheet sheet = workbook.getSheetAt(0);
			XSSFRow row = sheet.getRow(endDataRow-1);
			XSSFCell cell = row.getCell(DATA_COLUMN_START+i);
			cell.setCellFormula(formula.toString());
		}
	}
	
	/**
	 * Beregner start- og sluttrad i excel for hver region. 
	 * @return
	 */
	private List<Span> getSpans(){
		List<Span> spans = new ArrayList<Span>();
		int curRow = startRow;
		for (RegionTall rt:tilstandsrapport){
			int st = curRow;
			int en = st+rt.getKontrakter().size()-1;
			spans.add(new Span(st, en, rt));
			curRow = en+rowsBetween;
			endDataRow = curRow;
		}
		return spans;
	}
	
	/**
	 * Lukker arbeidsboken og åpner den i Excel
	 */
	public void finalizeAndOpen(){
		RapportHelper.finalizeAndOpen(workbook, file);
	}
	
	/**
	 * Sorterer tallene på region-id
	 */
	private void sort(){
		Collections.sort(tilstandsrapport, new Comparator<RegionTall>(){
			@Override
			public int compare(RegionTall o1, RegionTall o2) {
				return o1.getRegionId().compareTo(o2.getRegionId());
			}
		});
	}
	
	class Span{
		int startRow;
		int endRow;
		RegionTall regiontall;
		public Span(int startRow, int endRow, RegionTall regiontall){
			this.startRow = startRow;
			this.endRow = endRow;
			this.regiontall = regiontall;
		}
		public int getStartRow(){
			return startRow;
		}
		public int getEndRow(){
			return endRow;
		}
		public RegionTall getRegiontall(){
			return regiontall;
		}
	}
}
