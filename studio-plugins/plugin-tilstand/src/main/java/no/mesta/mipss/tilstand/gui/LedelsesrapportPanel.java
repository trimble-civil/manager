package no.mesta.mipss.tilstand.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.tilstand.TilstandsrapportController;
import no.mesta.mipss.tilstand.util.Resources;

public class LedelsesrapportPanel extends JPanel {

	private JComboBox cmbYear;
	private JComboBox cmbMonth;
	private JLabel lblYear;
	private JLabel lblMonth;
	
	private JButton btnHentRapport;
	private final TilstandsrapportController controller;
	public static final String[] months = new String[]{"Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"};
	public LedelsesrapportPanel(TilstandsrapportController controller) {
		this.controller = controller;
		initComponents();
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(lblYear, 		new GridBagConstraints(0,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0),0,0));
		add(cmbYear, 		new GridBagConstraints(1,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0),0,0));
		add(lblMonth, 		new GridBagConstraints(2,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 10, 0),0,0));
		add(cmbMonth, 		new GridBagConstraints(3,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0),0,0));
		add(btnHentRapport, new GridBagConstraints(4,0,1,1,1.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 10, 0),0,0));
	}

	private void initComponents() {
		cmbYear = new JComboBox();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		for (int i=year-10;i<=year;i++){
			cmbYear.addItem(i+"");
		}
		cmbYear.setSelectedIndex(cmbYear.getModel().getSize()-1);
		cmbMonth = new JComboBox();
		for (String m:months){
			cmbMonth.addItem(m);
		}
		int monthIndex = Calendar.getInstance().get(Calendar.MONTH);
		cmbMonth.setSelectedIndex(monthIndex-1);
		lblYear = new JLabel(Resources.getResource("label.year"));
		lblMonth = new JLabel(Resources.getResource("label.month"));
		btnHentRapport = new JButton(controller.getLedelsesrapportAction(cmbYear, cmbMonth));
	}
}
