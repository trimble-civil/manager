package no.mesta.mipss.tilstand;

import javax.swing.JComponent;

import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.tilstand.gui.TilstandPanel;

@SuppressWarnings("serial")
public class TilstandModule extends MipssPlugin{
	
	private TilstandPanel moduleGui;
	private TilstandsrapportController controller;
	
	@Override
	public JComponent getModuleGUI() {
		return moduleGui;
	}

	@Override
	protected void initPluginGUI() {
		controller = new TilstandsrapportController(this);
		moduleGui = new TilstandPanel(controller);
		
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
	}
}
