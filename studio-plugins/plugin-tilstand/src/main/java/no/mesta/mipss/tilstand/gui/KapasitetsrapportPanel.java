package no.mesta.mipss.tilstand.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.tilstand.TilstandsrapportController;
import no.mesta.mipss.tilstand.util.Resources;
import no.mesta.mipss.ui.JMipssDatePicker;

public class KapasitetsrapportPanel extends JPanel {

	private JLabel lblFra;
	private JLabel lblTil;
	
	private JMipssDatePicker fraPicker;
    private JMipssDatePicker tilPicker;
		
	private JButton btnHentRapport;
	private JButton btnHentDetaljertRapport;
	private final TilstandsrapportController controller;
		
	public KapasitetsrapportPanel(TilstandsrapportController controller) {
		this.controller = controller;
		initComponents();
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(lblFra, 		new GridBagConstraints(1,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0),0,0));
		add(fraPicker, 		new GridBagConstraints(2,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0),0,0));
		add(lblTil, 		new GridBagConstraints(3,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 10, 0),0,0));
		add(tilPicker, 		new GridBagConstraints(4,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0),0,0));
		add(btnHentRapport, 			new GridBagConstraints(5,0,1,1,1.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 5, 0),0,0));
		add(btnHentDetaljertRapport, 	new GridBagConstraints(5,1,1,1,1.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 10, 0),0,0));
	}

	private void initComponents() {
		
		Calendar c = Calendar.getInstance();
        c.setTime(Clock.now());
		
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        fraPicker = new JMipssDatePicker(c.getTime());
        fraPicker.setFormats(format);

        tilPicker = new JMipssDatePicker(c.getTime());
        tilPicker.setFormats(format);
        
        fraPicker.pairWith(tilPicker);
                
        tilPicker.setDate(controller.getAnbefaltSluttdato());
                
		lblFra = new JLabel(Resources.getResource("label.fra"));
		lblTil = new JLabel(Resources.getResource("label.til"));
		
		btnHentRapport = new JButton(controller.getKapasitetsrapportAction(fraPicker, tilPicker, false, Resources.getResource("button.kapasitetsrapport")));
		btnHentDetaljertRapport = new JButton(controller.getKapasitetsrapportAction(fraPicker, tilPicker, true, Resources.getResource("button.kapasitetsrapport_detaljert")));		
	}
}
