package no.mesta.mipss.tilstand.rapporter;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallDatakvalitet;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TilstandsrapportKontrakt {
	private static final String RAPPORT_TEMPLATE = "tilstandsrapportKontrakt_template";
	private static final int FORMATTING_GOOD_BAD = 0;
	
	private static final int DATA_COLUMN_END = 11;
	private static final int DATA_ROW_START = 7;
	
	private File file;
	private XSSFWorkbook workbook;
	private final List<NokkeltallFelt> kontraktTall;
	private final List<NokkeltallDatakvalitet> dataKvalitet;
	
	public TilstandsrapportKontrakt(List<NokkeltallFelt> kontraktTall, List<NokkeltallDatakvalitet> dataKvalitet, Date fraDato, Date tilDato){
		this.kontraktTall = kontraktTall;
		this.dataKvalitet = dataKvalitet;
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("datakvalitetTall", dataKvalitet);
		data.put("nokkeltall", kontraktTall);
		
		String fd = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.DATE_FORMAT);
		String td = MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.DATE_FORMAT);
		
		String periode = fd+" - "+td;
		data.put("periode", periode);
		data.put("rapportdato", Clock.now());
		
		file = RapportHelper.createTempFile(RAPPORT_TEMPLATE);
		workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, data);
	}
	
	/**
	 * Lukker arbeidsboken og åpner den i Excel
	 */
	public void finalizeAndOpen(){
		RapportHelper.finalizeAndOpen(workbook, file);
	}
}
