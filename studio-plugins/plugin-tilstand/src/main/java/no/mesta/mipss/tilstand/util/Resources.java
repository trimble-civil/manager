package no.mesta.mipss.tilstand.util;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class Resources {
	private static PropertyResourceBundleUtil properties;
	private static final String BUNDLE_NAME = "tilstandText";
	
	
	public static String getResource(String key){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key);
	}
	public static String getResource(String key, String... values){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key, values);
	}
}

