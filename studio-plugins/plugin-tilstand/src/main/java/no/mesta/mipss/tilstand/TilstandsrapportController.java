package no.mesta.mipss.tilstand;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.rapportfunksjoner.KapasitetRapportObject;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallDatakvalitet;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.persistence.rapportfunksjoner.RegionTall;
import no.mesta.mipss.produksjonsrapport.AdminrapportService;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.tilstand.rapporter.Kapasitetsrapport;
import no.mesta.mipss.tilstand.rapporter.Ledelsesrapport;
import no.mesta.mipss.tilstand.rapporter.TilstandsrapportKontrakt;
import no.mesta.mipss.tilstand.util.Resources;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.WaitPanel;
import no.mesta.mipss.util.EpostUtils;

import org.joda.time.DateTime;

public class TilstandsrapportController {

	private AdminrapportService bean;
	private final TilstandModule plugin;
	
	public TilstandsrapportController(TilstandModule plugin){
		this.plugin = plugin;
		bean = BeanUtil.lookup(AdminrapportService.BEAN_NAME, AdminrapportService.class);
	}
	
	
	@SuppressWarnings("serial")
	public Action getLedelsesrapportAction(final JComboBox cmbYear, final JComboBox cmbMonth) {
		return new AbstractAction(Resources.getResource("button.ledelsesrapport"), IconResources.EXCEL_ICON16){
			@Override
			public void actionPerformed(ActionEvent e) {
				
				final JDialog displayWait = WaitPanel.displayWait(SwingUtilities.windowForComponent(cmbYear), Resources.getResource("wait.title"));
				displayWait.setVisible(true);
				new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						try{
							int year = Integer.valueOf(cmbYear.getSelectedItem().toString());
							int month = Integer.valueOf(cmbMonth.getSelectedIndex()); //Januar er 0
							List<RegionTall> tilstandsrapport = bean.hentTilstandsrapport(Long.valueOf(year), Long.valueOf(month));
							if (tilstandsrapport.isEmpty()){
								JOptionPane.showMessageDialog(cmbMonth, Resources.getResource("ingendata.message"),Resources.getResource("ingendata.title"), JOptionPane.INFORMATION_MESSAGE);
								return null;
							}
							new Ledelsesrapport(tilstandsrapport, year, month).finalizeAndOpen();
						} catch (Exception e){
							e.printStackTrace();
							ErrorHandler.showError(e, Resources.getResource("error.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
						}
						return null;
					}
					@Override
					protected void done(){
						displayWait.dispose();
					}
				}.execute();
			}
		};
	}
	
	@SuppressWarnings("serial")
	public Action getTilstandsrapportAction(final JMipssDatePicker fraPicker, final JMipssDatePicker tilPicker, final JMipssContractPicker cmbKontrakt) {
		return new AbstractAction(Resources.getResource("button.tilstandsrapport"), IconResources.EXCEL_ICON16){
			@Override
			public void actionPerformed(ActionEvent e) {
				final JDialog displayWait = WaitPanel.displayWait(SwingUtilities.windowForComponent(fraPicker), Resources.getResource("wait.title"));
				displayWait.setVisible(true);
				new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						try{
							Driftkontrakt kontrakt = cmbKontrakt.getValgtKontrakt();
							if (kontrakt==null)
								return null;
							
//							List<NokkeltallDatakvalitet> dk = bean.hentDatakvalitetstall(plugin.getLoader().getLoggedOnUserSign());
//							List<NokkeltallFelt> nf = bean.hentTilstandsrapportKontrakt(plugin.getLoader().getLoggedOnUserSign(), fraPicker.getDate(), tilPicker.getDate());
//							if (dk.isEmpty()&&nf.isEmpty()){
//								JOptionPane.showMessageDialog(fraPicker, Resources.getResource("ingendata.message"),Resources.getResource("ingendata.title"), JOptionPane.INFORMATION_MESSAGE);
//								return null;
//							}
							NokkeltallDatakvalitet datakvalitet = bean.hentDatakvalitetstall(kontrakt.getId());
							NokkeltallFelt tilstand = bean.hentTilstandsrapportKontrakt(kontrakt.getId(), fraPicker.getDate(), tilPicker.getDate());
							new TilstandsrapportKontrakt(Collections.singletonList(tilstand), Collections.singletonList(datakvalitet), fraPicker.getDate(), tilPicker.getDate()).finalizeAndOpen();
						} catch (Exception e){
							e.printStackTrace();
							ErrorHandler.showError(e, Resources.getResource("error.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
						}
						return null;
					}
					@Override
					protected void done(){
						displayWait.dispose();
					}
				}.execute();
			}
		};
	}
	
	@SuppressWarnings("serial")
	public Action getKapasitetsrapportAction(final JMipssDatePicker fraPicker, final JMipssDatePicker tilPicker, final boolean detaljertRapport, String buttonTekst){
		return new AbstractAction(buttonTekst, IconResources.EXCEL_ICON16) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				boolean fortsett = validerDatogrunnlag(tilPicker.getDate());
				
				if (!fortsett) {
					return;
				}
				
				final JDialog displayWait = WaitPanel.displayWait(SwingUtilities.windowForComponent(fraPicker), Resources.getResource("wait.title"));
				displayWait.setVisible(true);
				new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						try{
							List <KapasitetRapportObject> data = bean.getKapasitetsRapportData(fraPicker.getDate(), tilPicker.getDate(), detaljertRapport);
							if (data.size() > 0) {
								new Kapasitetsrapport(data, fraPicker.getDate(), tilPicker.getDate(), detaljertRapport).finalizeAndOpen();
							} else{
								JOptionPane.showMessageDialog(null, Resources.getResource("ingendata.message"),Resources.getResource("ingendata.title"), JOptionPane.INFORMATION_MESSAGE);							}
						} catch (Exception e){
							e.printStackTrace();
							ErrorHandler.showError(e, Resources.getResource("error.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
						}
						return null;
					}
					@Override
					protected void done(){
						displayWait.dispose();
					}
				}.execute();
				
			}
		};
	}
	
	private boolean validerDatogrunnlag(Date tilDato){
		
		Date tilDatoMidnatt = new DateTime(tilDato).toDateMidnight().toDateTime().toDate();
		Date nowMidnatt = new DateTime().toDateMidnight().toDateTime().toDate();
		
		if (tilDato.after(nowMidnatt) || tilDatoMidnatt.getTime() == nowMidnatt.getTime()) {
			JOptionPane.showMessageDialog(null, Resources.getResource("message.kapasitetsrapport.feilDato", MipssDateFormatter.formatDate(getAnbefaltSluttdato(), MipssDateFormatter.SHORT_DATE_FORMAT)),
											Resources.getResource("message.kapasitetsrapport.feilDato.title"),
											JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if (tilDato.after(getAnbefaltSluttdato())) {
			int valg = JOptionPane.showConfirmDialog(null, 
						Resources.getResource("message.kapasitetsrapport", MipssDateFormatter.formatDate(getAnbefaltSluttdato(), MipssDateFormatter.SHORT_DATE_FORMAT)), 
						Resources.getResource("message.kapasitetsrapport.title"), 
						JOptionPane.YES_NO_OPTION);
			
			if (valg == JOptionPane.NO_OPTION) {
				return false;
			}
		}
		
		return true;
	}
	
	public TilstandModule getPlugin(){
		return plugin;
	}
	
	public Date getAnbefaltSluttdato(){
		return new DateTime().minusDays(3).toDate();
	}
}
