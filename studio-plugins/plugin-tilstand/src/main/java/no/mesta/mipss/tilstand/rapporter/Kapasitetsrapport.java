package no.mesta.mipss.tilstand.rapporter;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.rapportfunksjoner.KapasitetRapportObject;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Kapasitetsrapport {
	private static final String RAPPORT_TEMPLATE = "kapasitetsrapport_template";
	private static final String DETALJERT_OPPFOLGINGSRAPPORT_TEMPLATE = "detaljert_oppfolgingsrapport_template";
	
	private File file;
	private XSSFWorkbook workbook;
	private final List<KapasitetRapportObject> kapasiteter;
	
	public Kapasitetsrapport(List<KapasitetRapportObject> kapasiteter, Date fraDato, Date tilDato, Boolean detaljertRapport){
		this.kapasiteter = kapasiteter;
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("kapasiteter", kapasiteter);
		
		String fd = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.DATE_FORMAT);
		String td = MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.DATE_FORMAT);
		
		String periode = fd+" - "+td;
		data.put("periode", periode);
		data.put("rapportdato", Clock.now());
		
		if (detaljertRapport) {
			file = RapportHelper.createTempFile(DETALJERT_OPPFOLGINGSRAPPORT_TEMPLATE);
			workbook = RapportHelper.readTemplate(DETALJERT_OPPFOLGINGSRAPPORT_TEMPLATE, data);
		} else{
			file = RapportHelper.createTempFile(RAPPORT_TEMPLATE);
			workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, data);
		}		
	}
	
	/**
	 * Lukker arbeidsboken og åpner den i Excel
	 */
	public void finalizeAndOpen(){
		RapportHelper.finalizeAndOpen(workbook, file);
	}
}
