package no.mesta.mipss.tilstand.gui;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import no.mesta.mipss.tilstand.TilstandsrapportController;
import no.mesta.mipss.tilstand.util.Resources;

import org.jdesktop.swingx.VerticalLayout;

@SuppressWarnings("serial")
public class TilstandPanel extends JPanel{
	private LedelsesrapportPanel pnlLedelsesrapport;
	private TilstandsrapportPanel pnlTilstandsrapport;
	private KapasitetsrapportPanel pnlKapasitetsrapport;
	private final TilstandsrapportController controller;
	
	public TilstandPanel(TilstandsrapportController controller){
		this.controller = controller;
		initComponents();
		initGui();
		
	}
	private void initGui() {
		add(pnlLedelsesrapport);
		add(pnlTilstandsrapport);
		add(pnlKapasitetsrapport);
		
	}
	private void initComponents() {
		setLayout(new VerticalLayout(15));
		pnlLedelsesrapport = new LedelsesrapportPanel(controller);
		pnlLedelsesrapport.setBorder(BorderFactory.createTitledBorder(Resources.getResource("title.ledelsesrapport")));
		
		pnlTilstandsrapport= new TilstandsrapportPanel(controller);
		pnlTilstandsrapport.setBorder(BorderFactory.createTitledBorder(Resources.getResource("title.tilstandsrapport")));
		
		pnlKapasitetsrapport = new KapasitetsrapportPanel(controller);
		pnlKapasitetsrapport.setBorder(BorderFactory.createTitledBorder(Resources.getResource("title.kapasitetsrapport")));
	}
}
