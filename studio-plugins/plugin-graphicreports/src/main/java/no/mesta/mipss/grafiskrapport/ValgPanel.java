package no.mesta.mipss.grafiskrapport;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.Insets;

import java.awt.Rectangle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.border.EtchedBorder;

import oracle.jdeveloper.layout.VerticalFlowLayout;

public class ValgPanel extends JPanel {
    private JLabel jLabel1 = new JLabel();
    private JPanel valgPanel = new JPanel();
    private JCheckBox jCheckBox1 = new JCheckBox();
    private JCheckBox jCheckBox2 = new JCheckBox();
    private JCheckBox jCheckBox3 = new JCheckBox();
    private JCheckBox jCheckBox4 = new JCheckBox();
    private JPanel jPanel1 = new JPanel();
    private JLabel jLabel2 = new JLabel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JCheckBox jCheckBox5 = new JCheckBox();
    private JButton jButton1 = new JButton();
    private JButton jButton2 = new JButton();
    private BorderLayout borderLayout1 = new BorderLayout();
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel jPanel2 = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JPanel jPanel3 = new JPanel();
    private VerticalFlowLayout verticalFlowLayout1 = new VerticalFlowLayout();

    public ValgPanel() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setLayout(borderLayout1);
        this.setSize(new Dimension(457, 328));
        this.setBounds(new Rectangle(10, 10, 456, 326));
        jLabel1.setText("Vis");
        valgPanel.setLayout(gridBagLayout1);
        jCheckBox1.setText("Hastighet");
        jCheckBox2.setText("Rode");
        jCheckBox3.setText("Vei");
        jCheckBox4.setText("Produksjon");
        jPanel1.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        jPanel1.setLayout(gridBagLayout2);
        jLabel2.setText("Alle kj�ret�y er valgt!");
        jCheckBox5.setText("Vis kun kj�ret�y med produksjon");
        jCheckBox5.setSelected(true);
        jButton1.setText("Generer graf");
        jButton1.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        jButton1_actionPerformed(e);
                    }
                });
        jButton2.setText("Tilbake");
        jPanel2.setLayout(gridBagLayout3);
        jPanel3.setLayout(verticalFlowLayout1);
        this.add(valgPanel, BorderLayout.CENTER);
        valgPanel.add(jPanel1, 
                      new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                             new Insets(10, 0, 0, 0), 153, 0));
        jPanel3.add(jCheckBox2, null);
        jPanel3.add(jCheckBox3, null);
        jPanel3.add(jCheckBox1, null);
        jPanel3.add(jCheckBox4, null);
        valgPanel.add(jPanel3, 
                      new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                             new Insets(0, 0, 0, 0), 0, 0));
        valgPanel.add(jLabel1, 
                      new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                             new Insets(0, 0, 0, 0), 0, 0));
        jPanel1.add(jLabel2, 
                    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, 
                                           GridBagConstraints.NONE, 
                                           new Insets(5, 2, 12, 59), 53, 0));
        jPanel1.add(jCheckBox5, 
                    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, 
                                           GridBagConstraints.NONE, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        jPanel2.add(jButton1, 
                    new GridBagConstraints(1, 0, 1, 1, 0.5, 0.0, GridBagConstraints.EAST, 
                                           GridBagConstraints.NONE, 
                                           new Insets(0, 0, 10, 10), 0, 0));
        jPanel2.add(jButton2, 
                    new GridBagConstraints(0, 0, 1, 1, 0.5, 0.0, GridBagConstraints.WEST, 
                                           GridBagConstraints.NONE, 
                                           new Insets(0, 10, 10, 0), 0, 0));
        this.add(jPanel2, BorderLayout.SOUTH);
    }

    private void jButton1_actionPerformed(ActionEvent e) {
        JDialog ventDialog = new JDialog();
        ventDialog.add(new VentPanel(ventDialog));
        ventDialog.setLocationRelativeTo(null);
        ventDialog.setSize(300,200);
        ventDialog.setVisible(true);
    }
}
