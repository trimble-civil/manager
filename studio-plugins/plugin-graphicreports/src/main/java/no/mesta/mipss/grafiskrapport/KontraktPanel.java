package no.mesta.mipss.grafiskrapport;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EtchedBorder;

import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.decorator.Filter;
import org.jdesktop.swingx.decorator.FilterPipeline;
import org.jdesktop.swingx.decorator.PatternFilter;

public class KontraktPanel extends JPanel{
    private BorderLayout borderLayout1 = new BorderLayout();
    private JPanel sokTekstPanel = new JPanel();
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JTextField sokField = new JTextField();
    private JPanel kontraktUtvalgPanel = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel alleKontrakterPanel = new JPanel();
    private BorderLayout borderLayout3 = new BorderLayout();
    private JPanel valgteKontrakterPanel = new JPanel();
    private JPanel senterPanel = new JPanel();
    private BorderLayout borderLayout4 = new BorderLayout();
    private JLabel alleKontrakterLabel = new JLabel();
    private JLabel valgteKontrakterLabel = new JLabel();
    private JScrollPane alleKontrakterScroll = new JScrollPane();
    private JScrollPane valgteKontrakterScroll = new JScrollPane();
    private JList alleKontrakterList = new JList();
    private JList valgteKontrakterList = new JList();
    private JButton flyttAlleButton = new JButton();
    private JButton flyttValgteButton = new JButton();
    private JButton tilbakeValgteButton = new JButton();
    private JButton tilbakeAlleButton = new JButton();
    private JPanel nestePanel = new JPanel();
    private JButton nesteButton = new JButton();
    private JPanel knappePanel = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();

    private GrafiskRapportUI tabControl;
    private GridBagLayout gridBagLayout5 = new GridBagLayout();
    private GridBagLayout gridBagLayout6 = new GridBagLayout();

    public KontraktPanel() {
    
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void setTabControl(GrafiskRapportUI tabControl){
        this.tabControl = tabControl;
    }
    
    private void createKontrakerModel() {
        DefaultListModel model = new DefaultListModel();
        model.add(0, "0301 - Oslo ytre by");
        model.add(1, "0603 - Kongsberg");
        model.add(2, "2007 - Hammerfest");
        alleKontrakterList.setModel(model);
    }

    private void createValgteKontrakerModel() {
        DefaultListModel model = new DefaultListModel();
        model.add(0, "");
        valgteKontrakterList.setModel(model);
    }

    private void jbInit() throws Exception {
       // alleKontrakterList.setFilterEnabled(true);
        this.setLayout(borderLayout1);
        this.setSize(new Dimension(454, 325));
        createKontrakerModel();
        createValgteKontrakerModel();
        knappePanel.setLayout(gridBagLayout3);
        this.setLayout(gridBagLayout6);
        this.setBounds(new Rectangle(10, 10, 613, 338));
        sokTekstPanel.setLayout(gridBagLayout1);
        sokTekstPanel.setBorder(BorderFactory.createTitledBorder("S�k etter kontrakt"));
        sokField.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        sokField_actionPerformed(e);
                    }
                });
                sokField.addKeyListener(new KeyAdapter(){
                    public void keyPressed(KeyEvent e) {
                        sokField_keyPressed(e);
                    }
                });
        kontraktUtvalgPanel.setLayout(gridBagLayout2);
        kontraktUtvalgPanel.setBorder(BorderFactory.createTitledBorder("Velg kontrakt"));
        alleKontrakterPanel.setLayout(borderLayout3);
        valgteKontrakterPanel.setLayout(borderLayout4);
        senterPanel.setLayout(gridBagLayout4);
        alleKontrakterLabel.setText("Alle kontrakter");
        alleKontrakterLabel.setToolTipText("null");
        valgteKontrakterLabel.setText("Valgte kontrakter");

        ButtonGroup rapportTypeGroup = new ButtonGroup();

        sokTekstPanel.add(sokField, 
                          new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                                 new Insets(0, 10, 10, 10), 0, 
                                                 0));
        this.add(sokTekstPanel, 
                 new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                        new Insets(0, 0, 0, 0), 428, 0));
        alleKontrakterPanel.add(alleKontrakterLabel, BorderLayout.NORTH);
        alleKontrakterScroll.getViewport().add(alleKontrakterList, null);
        alleKontrakterPanel.add(alleKontrakterScroll, BorderLayout.CENTER);
        kontraktUtvalgPanel.add(alleKontrakterPanel, 
                                new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                    new Insets(10, 10, 10, 0), 0, 0));
        valgteKontrakterPanel.add(valgteKontrakterLabel, BorderLayout.NORTH);
        valgteKontrakterScroll.getViewport().add(valgteKontrakterList, null);
        valgteKontrakterPanel.add(valgteKontrakterScroll, BorderLayout.CENTER);
        kontraktUtvalgPanel.add(valgteKontrakterPanel, 
                                new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                    new Insets(10, 0, 10, 10), 0, 0));
        knappePanel.add(flyttAlleButton, 
                        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                    new Insets(5, 5, 0, 5), 9, 0));
        knappePanel.add(flyttValgteButton, 
                        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                    new Insets(0, 5, 0, 5), 15, 0));
        knappePanel.add(tilbakeValgteButton, 
                        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                    new Insets(0, 5, 0, 5), 15, 0));
        knappePanel.add(tilbakeAlleButton, 
                        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                    new Insets(0, 5, 0, 5), 9, 0));
        senterPanel.add(knappePanel, new GridBagConstraints(0, 0, 1, GridBagConstraints.REMAINDER, 
                    1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                    new Insets(0, 0, 0, 0), 0, 0));
        kontraktUtvalgPanel.add(senterPanel, 
                                new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                    new Insets(0, 0, 0, 0), 0, 0));
        this.add(kontraktUtvalgPanel, 
                 new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                        new Insets(0, 0, 0, 0), -146, 67));
        nestePanel.setLayout(gridBagLayout5);
        nestePanel.add(nesteButton, 
                       new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,GridBagConstraints.EAST, GridBagConstraints.NONE, 
                                              new Insets(0, 0, 10, 11), 0, 0));
        this.add(nestePanel, 
                 new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                        new Insets(0, 0, 0, 0), 385, 0));

        flyttAlleButton.setText(">>");
        flyttAlleButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        flyttAlleButton_actionPerformed(e);
                    }
                });
        flyttValgteButton.setText("> ");
        flyttValgteButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        flyttValgteButton_actionPerformed(e);
                    }
                });
        tilbakeValgteButton.setText("< ");
        tilbakeValgteButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        tilbakeValgteButton_actionPerformed(e);
                    }
                });
        tilbakeAlleButton.setText("<<");
        tilbakeAlleButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        tilbakeAlleButton_actionPerformed(e);
                    }
                });
        nesteButton.setText("Neste");
        nesteButton.setToolTipText("null");
        nesteButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        nesteButton_actionPerformed(e);
                    }
                });
    }
    
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException e) {
            // TODO
        } catch (IllegalAccessException e) {
            // TODO
        } catch (UnsupportedLookAndFeelException e) {
            // TODO
        } catch (InstantiationException e) {
            // TODO
        }
        JFrame frame = new JFrame();
        frame.setSize(500, 500);
        frame.add(new GrafiskRapportUI());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void nesteButton_actionPerformed(ActionEvent e) {
        tabControl.velgKjoretoy();
    }

    private void flyttAlleButton_actionPerformed(ActionEvent e) {
        DefaultListModel srcModel = (DefaultListModel)alleKontrakterList.getModel();
        DefaultListModel destModel = (DefaultListModel)valgteKontrakterList.getModel();
        int size = srcModel.getSize();
        for (int i=0;i<size;i++){
            String value = (String)srcModel.get(i);
            if (!destModel.contains(value)){
                destModel.add(0,value);
            }
        }
        
    }

    private void flyttValgteButton_actionPerformed(ActionEvent e) {
        DefaultListModel srcModel = (DefaultListModel)alleKontrakterList.getModel();
        DefaultListModel destModel = (DefaultListModel)valgteKontrakterList.getModel();
        int selected = alleKontrakterList.getSelectedIndex();
        if (selected>-1){
            String value = (String)srcModel.get(selected);
            if (!destModel.contains(value))
                destModel.add(0, value);
        }
    }

    private void tilbakeValgteButton_actionPerformed(ActionEvent e) {
        DefaultListModel destModel = (DefaultListModel)valgteKontrakterList.getModel();
        int selected = valgteKontrakterList.getSelectedIndex();
        if (selected>-1){
            destModel.remove(selected);
        }
    }

    private void tilbakeAlleButton_actionPerformed(ActionEvent e) {
        DefaultListModel destModel = (DefaultListModel)valgteKontrakterList.getModel();
        destModel.removeAllElements();
    }

    private void sokField_actionPerformed(ActionEvent e) {
    
    }
    private void sokField_keyPressed(KeyEvent e){
        DefaultListModel model = (DefaultListModel)alleKontrakterList.getModel();
        int size = model.getSize();
        for (int i=0;i<size;i++){
            String value = (String)model.get(i);
            if (value.contains(sokField.getText())){
                alleKontrakterList.setSelectedIndex(i);
                break;
            }
        }
        /*Filter[] filter = new Filter[]{
          new PatternFilter(sokField.getText(), 0, 0)
        };
        FilterPipeline pipeline = new FilterPipeline(filter);
        alleKontrakterList.setFilters(pipeline);
        */
    }
}
