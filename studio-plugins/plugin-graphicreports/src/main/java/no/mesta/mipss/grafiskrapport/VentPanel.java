package no.mesta.mipss.grafiskrapport;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXBusyLabel;

public class VentPanel extends JPanel {
    private JXBusyLabel jXBusyLabel1 = new JXBusyLabel();
    private BorderLayout borderLayout1 = new BorderLayout();
    
    private JPanel knappePanel = new JPanel();
    private FlowLayout flowLayout = new FlowLayout();
    private JButton jButton1 = new JButton();
    private JButton jButton2 = new JButton();
    private JPanel jPanel1 = new JPanel();
    private JLabel infoLabel = new JLabel();
    private JDialog parent;
    private BorderLayout borderLayout2 = new BorderLayout();
    private JPanel jPanel2 = new JPanel();
    private GridBagLayout gridBagLayout1 = new GridBagLayout();

    public VentPanel(JDialog parent) {
        this.parent = parent;
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setLayout(borderLayout1);
        this.setSize(new Dimension(400, 232));
        jXBusyLabel1.setText("Genererer graf..");
        jXBusyLabel1.setLayout(borderLayout2);
        jButton1.setText("Lagre som...");
        jButton1.setEnabled(false);
        jButton2.setText("Avbryt");
        jButton2.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        jButton2_actionPerformed(e);
                    }
                });
        infoLabel.setText("Vennligst vent mens grafen blir generert...");
        infoLabel.setToolTipText("null");
        jPanel2.setLayout(gridBagLayout1);
        knappePanel.add(jButton1, null);
        knappePanel.add(jButton2, null);
        flowLayout.setAlignment(SwingConstants.RIGHT);
        knappePanel.setLayout(flowLayout);
        this.add(knappePanel, BorderLayout.SOUTH);
        jPanel1.add(infoLabel, null);
        this.add(jPanel1, BorderLayout.NORTH);
        jPanel2.add(jXBusyLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                    new Insets(0, 0, 0, 0), 0, 0));
        this.add(jPanel2, BorderLayout.CENTER);
        jXBusyLabel1.setBusy(true);
    }

    private void jButton2_actionPerformed(ActionEvent e) {
        parent.dispose();
    }
}
