package no.mesta.mipss.grafiskrapport;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.Insets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import oracle.jdeveloper.layout.VerticalFlowLayout;

public class KjoretoyPanel extends JPanel {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JTextField sokField = new JTextField();
    private JLabel jLabel1 = new JLabel();
    private JScrollPane alleKjoretoyScroll = new JScrollPane();
    private JList alleKjoretoyList = new JList();
    private JPanel jPanel1 = new JPanel();
    private JScrollPane jScrollPane1 = new JScrollPane();
    private JList jList1 = new JList();
    private JLabel jLabel2 = new JLabel();
    private JButton jButton1 = new JButton();
    private JButton jButton2 = new JButton();
    private JButton jButton3 = new JButton();
    private JButton jButton4 = new JButton();
    private JButton jButton5 = new JButton();
    private VerticalFlowLayout verticalFlowLayout1 = new VerticalFlowLayout();
    private JButton jButton6 = new JButton();
    private GrafiskRapportUI tabControl;
    private JPanel jPanel2 = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel jPanel3 = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JPanel jPanel4 = new JPanel();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();

    public KjoretoyPanel() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setTabControl(GrafiskRapportUI tabControl){
        this.tabControl = tabControl;
    }
    private void jbInit() throws Exception {
        this.setLayout(gridBagLayout1);
        this.setSize(new Dimension(534, 520));
        jLabel1.setText("Alle kj�ret�y");
        jPanel1.setLayout(verticalFlowLayout1);
        jLabel2.setText("Valgte kj�ret�y");
        jButton1.setText("Neste");
        jButton1.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        jButton1_actionPerformed(e);
                    }
                });
        jButton2.setText(">>");
        jButton3.setText(">");
        jButton4.setText("<");
        jButton5.setText("<<");
        jButton6.setText("Tilbake");
        jPanel2.setBorder(BorderFactory.createTitledBorder("S�k etter kj�ret�y"));
        jPanel2.setLayout(gridBagLayout2);
        jPanel3.setBorder(BorderFactory.createTitledBorder("Velg kj�ret�y"));
        jPanel3.setLayout(gridBagLayout3);
        jPanel4.setLayout(gridBagLayout4);
        jPanel1.add(jButton2, null);
        jPanel1.add(jButton3, null);
        jPanel1.add(jButton4, null);
        jPanel1.add(jButton5, null);
        jPanel2.add(sokField, 
                    new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(5, 10, 5, 10), 0, 0));
        this.add(jPanel2, 
                 new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                        new Insets(0, 0, 0, 0), 0, 0));
        this.add(jPanel3, 
                 new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                        new Insets(0, 0, 0, 0), 29, -167));
        jPanel4.add(jButton6, 
                    new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(5, 0, 5, 0), 0, 0));
        jPanel4.add(jButton1, 
                    new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, 
                                           new Insets(5, 0, 5, 0), 0, 0));
        this.add(jPanel4, 
                 new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                        new Insets(0, 0, 0, 0), 0, 0));
        jPanel3.add(jLabel1, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, 
                                           GridBagConstraints.NONE, 
                                           new Insets(5, 79, 5, 79), 0, 0));
        alleKjoretoyScroll.getViewport().add(alleKjoretoyList, null);
        jPanel3.add(alleKjoretoyScroll, 
                    new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                           new Insets(0, 10, 10, 0), -401, 
                                           -87));
        jPanel3.add(jPanel1, 
                    new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                           new Insets(0, 0, 0, 1), 2, 202));
        jPanel3.add(jLabel2, 
                    new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, 
                                           GridBagConstraints.NONE, 
                                           new Insets(0, 0, 10, 0), 0, 0));
        jScrollPane1.getViewport().add(jList1, null);
        jPanel3.add(jScrollPane1, 
                    new GridBagConstraints(2, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                           new Insets(0, 0, 10, 10), 125, 
                                           -377));
    }

    private void jButton1_actionPerformed(ActionEvent e) {
        tabControl.velgUtvalg();;
    }
}
