package no.mesta.mipss.grafiskrapport;

import java.awt.BorderLayout;

import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class GrafiskRapportUI extends JPanel {
    private BorderLayout borderLayout1 = new BorderLayout();
    private JTabbedPane tabPane = new JTabbedPane();
    private JPanel jPanel1 = new JPanel();
    private KontraktPanel kontraktPanel = new KontraktPanel();
    private KjoretoyPanel kjoretoyPanel = new KjoretoyPanel();
    private ValgPanel valgPanel = new ValgPanel();
    public GrafiskRapportUI() {
        kontraktPanel.setTabControl(this);
        kjoretoyPanel.setTabControl(this);
        
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setLayout(borderLayout1);
        this.setBounds(new Rectangle(10, 10, 456, 326));
        tabPane.addTab("1. Velg kontrakt", kontraktPanel);
        tabPane.addTab("2. Velg kj�ret�y", kjoretoyPanel);
        tabPane.addTab("3. Utvalg", valgPanel);
        
        tabPane.setEnabledAt(1, false);
        tabPane.setEnabledAt(2, false);
        this.add(tabPane, BorderLayout.CENTER);
    }
    
    public void velgKjoretoy(){
        tabPane.setEnabledAt(1, true);
        tabPane.setSelectedIndex(1);
    }
    public void velgUtvalg(){
        tabPane.setEnabledAt(2, true);
        tabPane.setSelectedIndex(2);
    }
    
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException e) {
            // TODO
        } catch (IllegalAccessException e) {
            // TODO
        } catch (UnsupportedLookAndFeelException e) {
            // TODO
        } catch (InstantiationException e) {
            // TODO
        }
        JFrame frame = new JFrame();
        frame.setSize(456, 326);
        frame.add(new GrafiskRapportUI());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
