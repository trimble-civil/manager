package no.mesta.mipss.convertclient.elrappimport;

import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.convertclient.ConverterClient;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.veinett.KontraktveiImport;
import no.mesta.mipss.persistence.veinett.KontraktveinettImport;
import no.mesta.mipss.service.veinett.Convert;

public class ElrappImportController {

	private Convert convert;
	private final ConverterClient plugin;
	
	public ElrappImportController(ConverterClient plugin){
		this.plugin = plugin;
		convert = BeanUtil.lookup(Convert.BEAN_NAME, Convert.class);
	}
	
	
	public List<Driftkontrakt> getGyldigeKontrakter(){
		return convert.hentGyldigeKontrakter();
	}
	public KontraktveinettImport getSisteKontraktveinettImport(Long driftkontraktId){
		return convert.getKontraktveinettImportForDriftkontrakt(driftkontraktId);
	}
	public List<KontraktveiImport> hentImportForKontrakt(String kontraktNummer, String oppstartAar){
		return convert.importFromElrapp(kontraktNummer, oppstartAar);
	}
	
	public Date getSistOppdatertVegnett(String kontraktNummer, String oppstartAar){
		return convert.getSistOppdatertVegnett(kontraktNummer, oppstartAar);
	}
	
	public void persistImport(Driftkontrakt kontrakt, List<KontraktveiImport> imports, Date sistOppdatert){
		convert.persistImport(kontrakt, imports, sistOppdatert, plugin.getLoader().getLoggedOnUserSign());
	}
}
