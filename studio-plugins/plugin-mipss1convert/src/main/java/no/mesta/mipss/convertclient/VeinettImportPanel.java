package no.mesta.mipss.convertclient;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.veinett.Convert;
import no.mesta.mipss.service.veinett.VeinettImportService;
import no.mesta.mipss.ui.textpane.TextPaneHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VeinettImportPanel extends JPanel{
	private Logger log = LoggerFactory.getLogger(ConverterClient.class);
	
	private JProgressBar bar;
	private JTextPane textArea;
	private int totalTasks;
	private boolean pause;
	private boolean kill;
	private boolean running;
	private JButton startButton;
	private JButton stopButton;
	private JButton pauseButton;

	private final MipssPlugin plugin;

    private VeinettImportService service;
	
	public VeinettImportPanel(MipssPlugin plugin){
		this.plugin = plugin;
		initGui();
	}
	class ImportWorker extends SwingWorker<Integer, Integer>{
		private int counter = 0;
		private JProgressBar bar;
		private JTextPane pane;
		private TextPaneHelper phelper;
		
		public ImportWorker(JProgressBar bar, JTextPane pane){
			this.bar = bar;
			this.pane = pane;
			phelper = new TextPaneHelper(pane);
		}
		protected void done(){
			System.out.println("DONE");
		}
		@Override
		protected Integer doInBackground() throws Exception {
			
			try{
				
			running = true;
			
			VeinettImportService c = getVeinettImportBean();
			
			phelper.appendInfo("Henter alle veinett fra veinett_import...\n");
			List<Long> veinett = c.getAllVeinettimport();
			totalTasks = veinett.size();
			if (totalTasks==0){
				phelper.appendInfo("Ingen veinett å konverterer...\n");
				return null;
			}else{
				phelper.appendInfo("Hentet "+totalTasks+" veinett, begynner importering..\n");
				bar.setMaximum(totalTasks);
			}
			
			while (counter<totalTasks){
				Long v = veinett.get(counter);
				bar.setValue(counter);
				bar.setString("konverterer veinett "+(counter+1)+" av "+totalTasks+ "(veinettId="+v+")");
				phelper.appendInfo("konverterer veinett med id="+v+"\n");
				c.importFromImportTable(v, plugin.getLoader().getLoggedOnUserSign());
				phelper.appendInfo("Veinett med id="+v+" er konvertert!!!\n");
				
				if (pause){
					phelper.appendInfo("Veinettimport er satt på pause... klikk play-knappen for å gjenoppta\n");
					while(pause){
						try{
							Thread.sleep(500);
						}catch (InterruptedException e){}
					}
				}
				if (kill){
					phelper.appendWarning("Avbrutt av bruker.. "+(counter+1)+"av "+totalTasks+" er importert\n");
					return null;
				}
				counter ++;
			}
			System.out.println("done...");
			phelper.appendInfo("Importeringen av veinett er ferdig!!..\n");
			running = false;
			} catch (Throwable e){
				phelper.appendError(e.getStackTrace(), e.getMessage());
			}
			return null;
		}
		
		
	}
	public VeinettImportService getVeinettImportBean() {
        if(service==null)
            service =  BeanUtil.lookup(VeinettImportService.BEAN_NAME, VeinettImportService.class);
        return service;
    }
	
	protected SwingWorker<Integer, Integer> convertAllVeinett(final JProgressBar bar, final JTextPane textArea){
		return new ImportWorker(bar, textArea);
	}

    /**
     * For use in testing
     */
    public void setVeinettImportBean(VeinettImportService service) {
        this.service = service;
    }
	
	private void initGui(){
		setLayout(new GridBagLayout());
		bar = new JProgressBar();
		bar.setStringPainted(true);
		bar.setString("Venter...");
		
		textArea = new JTextPane();
		textArea.setContentType("text/html");
		
		JScrollPane scroll = new JScrollPane(textArea);
		
		String wtext = PropertyResourceBundleUtil.getPropertyBundle("mipss1convertText").getString("veinettmodul.warning");
		JLabel warning = new JLabel(wtext);
		warning.setForeground(Color.red);
		Font f = new Font("Arial", Font.BOLD, 18);
		warning.setFont(f);
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		
		startButton = new JButton(new StartAction("Start", IconResources.PLAYER_PLAY));
		pauseButton = new JButton(new PauseAction("Pause", IconResources.PLAYER_PAUSE));
		stopButton =  new JButton(new StopAction("Stopp", IconResources.PLAYER_STOP));

		buttonPanel.add(startButton, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(15,0,0,5), 0,0));
		buttonPanel.add(pauseButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15,0,0,5), 0,0));
		buttonPanel.add(stopButton,  new GridBagConstraints(2,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15,0,0,5), 0,0));
		
		JPanel progressPanel = new JPanel(new GridBagLayout());
		progressPanel.setBorder(BorderFactory.createTitledBorder("Fremgang"));
		progressPanel.add(bar, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		
		add(warning, 		new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,15), 0,0));
		add(buttonPanel, 	new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		add(progressPanel, 	new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		add(scroll, 		new GridBagConstraints(0,3, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,5), 0,0));
	}
	

	private void setComponentSize(Component c, Dimension size){
		c.setPreferredSize(size);
		c.setMinimumSize(size);
		c.setMaximumSize(size);
	}
	
	class StartAction extends AbstractAction{

		public StartAction(String text, ImageIcon image){
			super(text, image);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			log.debug("Start klikked");
			if (running){
				pause = false;
			}else{
				new Thread(){
					public void run(){
						SwingWorker<Integer,Integer> convertAllVeinett = convertAllVeinett(bar, textArea);
						convertAllVeinett.execute();
						try {
							convertAllVeinett.get();
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						} catch (ExecutionException e1) {
							e1.printStackTrace();
						}
					}
				}.start();
			}
		}
	}
	
	class StopAction extends AbstractAction{
		
		public StopAction(String text, ImageIcon image){
			super(text, image);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			log.debug("Stop klikked");
			kill = true;
		}
	}
	
	class PauseAction extends AbstractAction{
		
		public PauseAction(String text, ImageIcon image){
			super(text, image);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			log.debug("Pause klikked");
			pause = true;
			
		}
	}
}
