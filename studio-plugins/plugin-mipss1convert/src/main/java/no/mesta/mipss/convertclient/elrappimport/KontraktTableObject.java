package no.mesta.mipss.convertclient.elrappimport;

import java.util.Date;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

@SuppressWarnings("serial")
public class KontraktTableObject implements CheckBoxTableObject{
	private Driftkontrakt kontrakt;
	private boolean valgt;
	private Date sistOppdatert;
	private Date opprettetDato;
	private Date gyldigTil;
	public KontraktTableObject(Driftkontrakt kontrakt){
		this.kontrakt = kontrakt;
		
	}
	
	public String getKontraktnummer(){
		return kontrakt.getKontraktnummer();
	}
	public String getKontraktnavn(){
		return kontrakt.getKontraktnavn();
	}
	
	public Date getSistOppdatert(){
		return sistOppdatert;
	}
	public void setSistOppdatert(Date sistOppdatert){
		this.sistOppdatert = sistOppdatert;
	}
	public Driftkontrakt getKontrakt(){
		return kontrakt;
	}
	@Override
	public String getTextForGUI() {
		return null;
	}

	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;	
	}

	@Override
	public Boolean getValgt() {
		return valgt;
	}

	public Date getOpprettetDato(){
		return opprettetDato;
	}
	
	public void setOpprettetDato(Date opprettetDato){
		this.opprettetDato = opprettetDato;
	}

	public Date getGyldigTil() {
		return gyldigTil;
	}

	public void setGyldigTil(Date gyldigTil) {
		this.gyldigTil = gyldigTil;
	}	
}
