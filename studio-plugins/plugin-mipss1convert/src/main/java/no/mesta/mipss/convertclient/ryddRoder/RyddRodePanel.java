package no.mesta.mipss.convertclient.ryddRoder;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.jdesktop.swingx.JXHeader;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.convertclient.ConverterClient;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.veinett.Convert;

/**
 * UFERDIG!!!.. Ikke sikkert vi skal lage dette. 
 * Tanken er å ha en funksjon som lagrer alle aktive roder på nytt for å rydde opp i evt overlapp og gjenhengende reflinkseksjoner
 * @author harkul
 *
 */
public class RyddRodePanel extends JPanel {
	private JButton startButton;
	private JButton stopButton;
	private JButton pauseButton;
	private JProgressBar bar;
	private JTextPane textArea;
	private boolean pause;
	private boolean running;
	private boolean cancelled;
	
	private final ConverterClient plugin;
	private Convert bean;
	
	
	public RyddRodePanel(ConverterClient plugin){
		this.plugin = plugin;
		
//		bean = BeanUtil.lookup(Convert.BEAN_NAME, Convert.class);
		initGui();
	}

	public static void main(String[] args) {
		JFrame f= new JFrame();
		f.add(new RyddRodePanel(null));
		f.setSize(600, 700);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		
		startButton = new JButton(getStartAction());
		pauseButton = new JButton(getPauseAction());
		stopButton =  new JButton(getStopAction());
		
		bar = new JProgressBar();
		bar.setStringPainted(true);
		bar.setString("Venter...");
		
		textArea = new JTextPane();
		textArea.setContentType("text/html");
		
		JScrollPane scroll = new JScrollPane(textArea);
		
		JXHeader head = new JXHeader("Opprydding i veinett til alle roder", "Denne funksjonen lager nye veireferanser og reflinkseksjoner for alle aktive roder. Kan benyttes til å rydde opp i overlappende veireferanser eller reflinkseksjoner.\n1. Alle veireferanser slettes,\n2. Alle reflinkseksjoner hentes ut og det fjernes overlapp,\n3. Nye veireferanser lages,\n4. Reflinkseksjoner lagres");
		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(startButton, new GridBagConstraints(0,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		buttonPanel.add(pauseButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		buttonPanel.add(stopButton,  new GridBagConstraints(2,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		
		
		add(head, new GridBagConstraints(0,0, 1,1, 1.0,0.0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,10,5), 0,0));
		add(buttonPanel, 	new GridBagConstraints(0,1, 1,1, 0.0,0.0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		add(bar, 			new GridBagConstraints(0,2, 1,1, 1.0,0.0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,10,5), 0,0));
		add(scroll, 		new GridBagConstraints(0,3, 1,1, 1.0,1.0,GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,5), 0,0));
	}
	
	
	public Action getStartAction(){
		return new AbstractAction("Start", IconResources.PLAYER_PLAY){

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!running){
					cancelled=false;
					appendInfo("Starter oppretting av reflinkseksjoner og veireferanser for alle veinett i tabellen temp_nytt_veinett...\n");
					UpdateWorker w = new UpdateWorker();
					w.addPropertyChangeListener(new PropertyChangeListener(){
						@Override
						public void propertyChange(PropertyChangeEvent evt) {
							if (evt.getPropertyName().equals("progress")){
								int value = Integer.valueOf(evt.getNewValue().toString());
								bar.setValue(value);
								bar.setString(value+"/"+bar.getMaximum());
							}
						}
					});
					w.execute();
				}
				pause=false;
			}
		};
	}
	public Action getStopAction(){
		return new AbstractAction("Stopp", IconResources.PLAYER_STOP){

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=true;
				pause=false;
				running=false;
			}
		};
	}
	public Action getPauseAction(){
		return new AbstractAction("Pause", IconResources.PLAYER_PAUSE){

			@Override
			public void actionPerformed(ActionEvent e) {
				pause=!pause;
			}
		};
	}
	
	private class UpdateWorker extends SwingWorker<Void, Void>{

		@Override
		protected Void doInBackground(){
			try{
//				running = true;
//				List<VeinettCopyVO> list = bean.getAlleVeinettSomSkalTransformeres();
//				bar.setMaximum(list.size());
//				int i=1;
//				for (VeinettCopyVO vo:list){
//					bar.setValue(i);
//					bar.setString(i+"/"+bar.getMaximum());
//
//					//setProgress(i);
//					Veinett v = createVeireferanser(vo);
//					appendInfo("Veinett ferdig behandlet:\n");
//					appendInfo("\tId\t:"+v.getId());
//					appendInfo("\n\tNavn\t:"+v.getNavn());
//					appendInfo("\n\tFritekst\t:"+v.getFritekst()+"\n");
//					
//					if (pause){
//						appendInfo("Veinettimport er satt på pause... klikk play-knappen for å gjenoppta\n");
//						while(pause){
//							try{ Thread.sleep(500); }catch (InterruptedException e){}
//						}
//					}
//					if (cancelled){
//						appendWarning("Avbrutt av bruker.. "+(i)+" av "+bar.getMaximum()+" er opprettet\n");
//						break;
//					}
//					i++;
//				}
//				appendInfo("Opprettingen av reflinkseksjoner og veireferanser er ferdig!!..\n");
//				bar.setString("Ferdig..");
			} catch (Throwable t){
				appendError(t.getStackTrace(), t.getMessage());
				bar.setString("Feilet!!...");
			} finally{
				running = false;
				
			}
			return null;
		}
	}
	
//	private Veinett createVeireferanser(VeinettCopyVO vo){
//		return bean.copyAndTransform(vo, plugin.getLoader().getLoggedOnUserSign());
//	}
	
	private void appendInfo(String msg){
		StyledDocument doc = (StyledDocument)textArea.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, Color.blue);
        try {
			doc.insertString(doc.getLength(), msg, style);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		textArea.setCaretPosition(doc.getLength());
	}
	private void appendWarning(String msg){
		StyledDocument doc = (StyledDocument)textArea.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, new Color(200, 100, 0));
        try {
			doc.insertString(doc.getLength(), msg, style);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		textArea.setCaretPosition(doc.getLength());
	}
	private void appendError(StackTraceElement[] msg, String message){
		StyledDocument doc = (StyledDocument)textArea.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, new Color(230, 0, 0));
        Style style2 = doc.addStyle("StyleName2", null);
        StyleConstants.setForeground(style2, new Color(0, 0, 220));
        try {
        	doc.insertString(doc.getLength(), message, style);
        	for (StackTraceElement e:msg){
        		
        		String m = "at "+e.getClassName()+"(";
        		String m2 = e.getFileName()+":"+e.getLineNumber();
        		String m3 = ")\n";
        		
        		doc.insertString(doc.getLength(), m, style);
        		doc.insertString(doc.getLength(), m2, style2);
        		doc.insertString(doc.getLength(), m3, style);
        	}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		textArea.setCaretPosition(doc.getLength());
	}
}
