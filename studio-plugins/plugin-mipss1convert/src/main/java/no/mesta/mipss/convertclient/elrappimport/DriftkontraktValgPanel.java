package no.mesta.mipss.convertclient.elrappimport;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.veinett.KontraktveinettImport;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class DriftkontraktValgPanel extends JPanel{

	private final ElrappImportController controller;
	private MipssBeanTableModel<KontraktTableObject> mdlKontrakter;
	private String[] columns = new String[] { "valgt", "kontraktNr", "kontraktnavn"};
	private JXTable tblKontrakter;
	private JCheckTablePanel<KontraktTableObject> scrKontrakter;
	private JCheckBox chkAlle;
	
	public DriftkontraktValgPanel(ElrappImportController controller){
		this.controller = controller;
		
		initComponents();
		initGui();
		getKontrakter();
	}

	private void initComponents() {
		tblKontrakter = createKontrakterTable();
		chkAlle = new JCheckBox("Velg alle");
		scrKontrakter = new JCheckTablePanel<KontraktTableObject>(
				tblKontrakter,
				mdlKontrakter,
				chkAlle,
				"Vennligst vent, henter kontrakter", false);
	}

	private JXTable createKontrakterTable() {
		mdlKontrakter = new MipssBeanTableModel<KontraktTableObject>(KontraktTableObject.class, new int[] { 0 }, "valgt", "kontraktnummer", "kontraktnavn", "gyldigTil", "sistOppdatert", "opprettetDato");
		MipssRenderableEntityTableColumnModel kontraktColumnModel = new MipssRenderableEntityTableColumnModel(KontraktTableObject.class, mdlKontrakter.getColumns());
		final JXTable table = new JXTable(mdlKontrakter,kontraktColumnModel) {
			@Override
			public boolean getScrollableTracksViewportWidth() {
				return true && getParent() instanceof JViewport && (((JViewport) getParent()).getWidth() > getPreferredSize().width);
			}
			protected boolean shouldSortOnChange(TableModelEvent e) {
				return false;
			}
		};
		table.setDefaultRenderer(Date.class, new DateTimeRenderer(MipssDateFormatter.DATE_FORMAT));
		
		table.setFillsViewportHeight(true);
		table.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
		table.getColumn(0).setPreferredWidth(40);
		table.getColumn(1).setPreferredWidth(65);
		table.getColumn(2).setPreferredWidth(160);
		table.getColumn(3).setPreferredWidth(90);
		table.getColumn(4).setPreferredWidth(90);
		table.getColumn(5).setPreferredWidth(90);
		return table;
	}
	
	public void oppdaterKontrakter(){
		getKontrakter();
	}
	private void getKontrakter() {
		scrKontrakter.setBusy(true);
		final SwingWorker<List<KontraktTableObject>, Void> w = new SwingWorker<List<KontraktTableObject>, Void>() {
			@Override
			protected List<KontraktTableObject> doInBackground() throws Exception {
				try{
					List<Driftkontrakt> gyldigeKontrakter = controller.getGyldigeKontrakter();
					List<KontraktTableObject> list = new ArrayList<KontraktTableObject>();
					for (Driftkontrakt dk:gyldigeKontrakter){
						KontraktveinettImport sisteKontraktveinettImport = controller.getSisteKontraktveinettImport(dk.getId());
						KontraktTableObject kto = new KontraktTableObject(dk);
						if (sisteKontraktveinettImport!=null){
							kto.setSistOppdatert(sisteKontraktveinettImport.getSistOppdatertDato());
							kto.setOpprettetDato(sisteKontraktveinettImport.getOpprettetDato());
						}
						if (dk.getGyldigTilDato() != null) {
							kto.setGyldigTil(dk.getGyldigTilDato());
						}
						list.add(kto);
					}
					return list;
				} catch (Throwable t){
					t.printStackTrace();
				}
				return null;
			}
			@Override
			protected void done(){
				scrKontrakter.setBusy(false);
				mdlKontrakter.getEntities().clear();
				try{
					mdlKontrakter.getEntities().addAll(get());
				}catch(Exception e){
					e.printStackTrace();//ignore...
				}
			}
		};
		w.execute();
		new Thread(){
			public void run(){
				try {
					w.get();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	public List<KontraktTableObject> getAlleValgte(){
		return scrKontrakter.getAlleValgte();
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		add(scrKontrakter, 	new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,0,5), 0,0));
		add(chkAlle, 		new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,0), 0,0));
	}
}
