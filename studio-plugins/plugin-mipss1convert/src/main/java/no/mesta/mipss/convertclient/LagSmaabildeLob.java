package no.mesta.mipss.convertclient;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Iterator;

public class LagSmaabildeLob {
	private String oracleUrlProd = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=mes-bg-37-102-vip.vegprod.no)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=mes-bg-37-103-vip.vegprod.no)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=PSTYR.vegprod.no)))";
	private JTextArea area;
	private JProgressBar bar;
	private JButton smaaButton;
	public LagSmaabildeLob(){
		initGui();
	}

	private void initGui() {
		JFrame f = new JFrame("Lager småbildelob av ALLE!!");
		f.setSize(680, 400);
		f.setLayout(new BorderLayout());
		
		area = new JTextArea();
		JScrollPane scroll = new JScrollPane(area);
		bar = new JProgressBar();
		bar.setStringPainted(true);
		bar.setString(" ");
		
		smaaButton = new JButton(new LagSmaabildeAction()); 
		
		f.add(smaaButton, BorderLayout.NORTH);
		f.add(scroll, BorderLayout.CENTER);
		f.add(bar, BorderLayout.SOUTH);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
		
	}
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(new WindowsLookAndFeel());
		} catch (Exception e) {
			return;
		}
		new LagSmaabildeLob();
	}
	private void appendString(String string){
		area.append(string);
		area.setCaretPosition(area.getText().length());
	}
	/**
	 * Action klasse for å starte overføringen fra funnbilde og fullfort_bilde fra vmp
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	private class LagSmaabildeAction extends AbstractAction{
		public LagSmaabildeAction(){
			super("Lag smålobber!");
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			appendString("Henter alle bilde_guid");
			
			appendString("Starter produksjon av småbilder...");
			try{
				new Thread(){
					public void run(){
						LagLobWorker w = new LagLobWorker();
						w.execute();
					}
				}.start();
				
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	
	private void enableComponents(boolean enabled){
		smaaButton.setEnabled(enabled);
	}
	private void logToFile(String guid){
		System.out.println(guid);
	}
	private Connection getOracleConnection(){
		Connection c = null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			c = DriverManager.getConnection(oracleUrlProd, "mipss", "mipss");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Klarte ikke å koble til oracle\n"+e.getMessage(), "OracleServerConnection", JOptionPane.ERROR_MESSAGE);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}
	
	private int getCount(){
		int count = 0;
		String select = "select count(*) from bilde where bilde_lob is not null and guid <> 'UHtuTeeR8ZTgQIAKWBA59w=='";

		try (Connection oracle = getOracleConnection();
			PreparedStatement ps = oracle.prepareStatement(select);
			ResultSet rset = ps.executeQuery()) {
			if (rset.next()){
				count = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	private class LagLobWorker extends SwingWorker{
		private int c;
		public LagLobWorker(){
			
		}
		
		public Void doInBackground() throws Exception{
			enableComponents(false);
			appendString("Henter bilde_guid som skal skaleres... \n");
			c = getCount();
			if (c==0){
				appendString("Fant ingen bilder");
				return null;
			}
			appendString("Fant:"+c+" bilder, begynner konvertering...");
			
			try(Connection oracle = getOracleConnection()) {
				String select = "select guid, bilde_lob from bilde where bilde_lob is not null and guid <> 'UHtuTeeR8ZTgQIAKWBA59w=='";
			
				int i = 0;
				bar.setValue(0);
				bar.setMaximum(c);
				try (PreparedStatement guidBilde = oracle.prepareStatement(select);
					ResultSet rset = guidBilde.executeQuery()) {
					while (rset.next()) {
						long timeStart = System.currentTimeMillis();
						i++;
						String bildeGuid = rset.getString(1);
						InputStream bildeStream = rset.getBinaryStream(2);
						if (bildeStream != null) {
							byte[] bilde = getScaledInstance(bildeStream, 0.5f);
							bildeStream.close();
							if (bilde == null) {
								System.err.println("Skalert bilde er NULL:" + bildeGuid);
							}
							try(Connection update = getOracleConnection()) {
								update.setAutoCommit(false);
								try(PreparedStatement insert = update.prepareStatement("update bilde set smaabilde_lob = ? where guid=?")) {
									insert.setBytes(1, bilde);
									insert.setString(2, bildeGuid);
									appendString("Lagrer smaa_bilde med guid:" + bildeGuid + " : ");
									int rowCount = insert.executeUpdate();
									appendString(rowCount + " rad" + (rowCount > 1 ? "er\n" : "\n"));
									update.commit();
								}
							}
						} else
							appendString("bilde med guid=" + bildeGuid + " er null\n");
						bar.setString("Skalerer bilde " + i + " av " + c);
						bar.setValue(i);
						logToFile(bildeGuid);
						appendString("runtime:" + (System.currentTimeMillis() - timeStart) + "ms\n");
					}
					appendString("ferdig...\n");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally{
				enableComponents(true);
			}
			return null;
		}
		public void done(){
			bar.setString("Ferdig med skalering ("+c+")" );
		}
	}
	
	private byte[] getScaledInstance(InputStream imageStr, float compression){
		try {
			BufferedImage read = ImageIO.read(imageStr);
			if (read!=null){
				int w = read.getHeight()>read.getWidth()?240:320;
				int h = read.getHeight()>read.getWidth()?320:240;
				
				Image scaled = read.getScaledInstance(w, h, Image.SCALE_FAST);
				ByteArrayOutputStream bas = new ByteArrayOutputStream();
				BufferedImage scaledBuff = new BufferedImage(w, h, read.getType());
				Graphics2D bufImageGraphics = scaledBuff.createGraphics();
				bufImageGraphics.drawImage(scaled, 0, 0, null);
				
				Iterator writers = ImageIO.getImageWritersBySuffix("jpeg");
				if (!writers.hasNext())
					throw new IllegalStateException("No writers found");
				//compress image
				ImageWriter writer = (ImageWriter) writers.next();
				ImageOutputStream ios = ImageIO.createImageOutputStream(bas);
				writer.setOutput(ios);
				ImageWriteParam param = writer.getDefaultWriteParam();
				if (compression >= 0) {
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					param.setCompressionQuality(compression);
				}
				
				writer.write(null, new IIOImage(scaledBuff, null, null), param);
				ios.close();
				writer.dispose();
				
				byte[] data = bas.toByteArray();
				return data;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
