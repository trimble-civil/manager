package no.mesta.mipss.convertclient.nyereflink;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.apache.commons.lang.StringUtils;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.veinett.Convert;

public class OpprettNyeProdReflinkseksjoner extends JPanel{
	private Convert bean;
	private JTextField txtFraStrekning;
	private JTextField txtTilStrekning;
	
	private JButton startButton;
	private JButton stopButton;
	private JButton pauseButton;
	private JProgressBar bar;
	private JTextPane textArea;
	private boolean pause;
	private boolean running;
	private boolean cancelled;
	
	public OpprettNyeProdReflinkseksjoner(){
		bean = BeanUtil.lookup(Convert.BEAN_NAME, Convert.class);
		initGui();
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		
		txtFraStrekning = new JTextField(10);
		txtTilStrekning = new JTextField(10);
		
		startButton = new JButton(getStartAction());
		pauseButton = new JButton(getPauseAction());
		stopButton =  new JButton(getStopAction());
		
		bar = new JProgressBar();
		bar.setStringPainted(true);
		bar.setString("Venter...");
		
		textArea = new JTextPane();
		textArea.setContentType("text/html");
		
		JScrollPane scroll = new JScrollPane(textArea);
		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(startButton, new GridBagConstraints(0,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		buttonPanel.add(pauseButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		buttonPanel.add(stopButton,  new GridBagConstraints(2,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		
		JPanel pnlTxt = new JPanel(new GridBagLayout());
		JLabel lblFra = new JLabel("Strekningid fra:");
		JLabel lblTil = new JLabel("Strekningid til:");
		pnlTxt.add(lblFra, 			new GridBagConstraints(0,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		pnlTxt.add(txtFraStrekning, new GridBagConstraints(1,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		pnlTxt.add(lblTil, 			new GridBagConstraints(2,0, 1,1, 0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		pnlTxt.add(txtTilStrekning, new GridBagConstraints(3,0, 1,1, 1.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		
		add(pnlTxt, 		new GridBagConstraints(0,0, 1,1, 0.0,0.0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		add(buttonPanel, 	new GridBagConstraints(0,1, 1,1, 0.0,0.0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,10,5), 0,0));
		add(bar, 			new GridBagConstraints(0,2, 1,1, 1.0,0.0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,10,5), 0,0));
		add(scroll, 		new GridBagConstraints(0,3, 1,1, 1.0,1.0,GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,5), 0,0));
	}
	
	public Action getStartAction(){
		return new AbstractAction("Start", IconResources.PLAYER_PLAY){

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!running){
					cancelled=false;
					appendInfo("Starter oppretting av prodreflinkseksjoner...\n");
					UpdateWorker w = new UpdateWorker();
					w.addPropertyChangeListener(new PropertyChangeListener(){
						@Override
						public void propertyChange(PropertyChangeEvent evt) {
							if (evt.getPropertyName().equals("progress")){
								int value = Integer.valueOf(evt.getNewValue().toString());
								bar.setValue(value);
								bar.setString(value+"/"+bar.getMaximum());
							}
						}
					});
					w.execute();
				}
				pause=false;
			}
		};
	}
	
	private class UpdateWorker extends SwingWorker<Void, Void>{

		@Override
		protected Void doInBackground(){
			try{
				running = true;
				String fraStr = txtFraStrekning.getText();
				String tilStr = txtTilStrekning.getText();
				long fra = Long.valueOf(fraStr);
				long til = Long.valueOf(tilStr);
				
				Long antallStrekninger = bean.getAntallStrekninger(fra, til);
				bar.setMaximum(antallStrekninger.intValue());
				int totalCount = 0;
				int strekningerPrTransaksjon = 10000;
				while (fra<til&&running){
					long tilv = 0;
					if ((fra+strekningerPrTransaksjon)<til){
						tilv = fra+strekningerPrTransaksjon;
					}else{
						tilv = til;
					}
					appendInfo("Henter strekninger fra:"+fra+" til "+tilv+"\n");
					List<BigDecimal> list = bean.hentAlleProdstrekningerSomSkalGjenopprettes(fra, tilv);
					totalCount = oppdateReflinks(list, totalCount);
					
					fra = tilv;
				}
				appendInfo("Opprettingen av prodreflinkseksjoner er ferdig!!..\n");
			} catch (Throwable t){
				appendError(t.getStackTrace(), t.getMessage());
				bar.setString("Feilet!!...");
				t.printStackTrace();
			} finally{
				running = false;
				
			}
			return null;
		}

		private int oppdateReflinks(List<BigDecimal> list, int totalCount) {
			double totRt = 0;
			int counter = 0;
			long totalSecondsRemaining=-1;
			double secPrTransaction=-1;
			int transformArrayLength= 100;
			Long[] toTransform = new Long[transformArrayLength];
			for (BigDecimal vo:list){
				toTransform[counter]=vo.longValue();
				totalCount++;
				if (totalCount<bar.getMaximum()){
					if (counter<toTransform.length-1){
						counter++;
						continue;
					}
				}
				counter=0;
				long start  = System.currentTimeMillis();
				bar.setValue(totalCount);
				bar.setString(totalCount+"/"+bar.getMaximum()+ " ("+totalSecondsRemaining+"s)");
				
				bean.gjenopprettProdreflinkForStrekning(toTransform);
				
				String join = StringUtils.join(toTransform, ", ");
				appendInfo(join+" ferdig.. på ");
				if (pause){
					appendInfo("Prodreflinkgjenoppretting er satt på pause... klikk play-knappen for å gjenoppta\n");
					while(pause){
						try{ Thread.sleep(500); }catch (InterruptedException e){}
					}
				}
				if (cancelled){
					appendWarning("Avbrutt av bruker.. "+(totalCount)+" av "+bar.getMaximum()+" er opprettet\n");
					break;
				}
				double rt = System.currentTimeMillis()-start;
				totRt +=rt;
				appendInfo((rt/1000)+" sekunderish\n");
					
				secPrTransaction =((totRt/(totalCount/toTransform.length))/1000);
				
				int remaining = (bar.getMaximum()/toTransform.length)-(totalCount/toTransform.length);
				totalSecondsRemaining =(long)( secPrTransaction*remaining);
				
				toTransform = new Long[transformArrayLength];
				if (totalCount%1000==0)
					textArea.setText("");
			}
			return totalCount;
		}
	}
	public Action getStopAction(){
		return new AbstractAction("Stopp", IconResources.PLAYER_STOP){

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=true;
				pause=false;
				running=false;
			}
		};
	}
	public Action getPauseAction(){
		return new AbstractAction("Pause", IconResources.PLAYER_PAUSE){

			@Override
			public void actionPerformed(ActionEvent e) {
				pause=!pause;
			}
		};
	}
	
	private void appendInfo(String msg){
		StyledDocument doc = (StyledDocument)textArea.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, Color.blue);
        try {
			doc.insertString(doc.getLength(), msg, style);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		textArea.setCaretPosition(doc.getLength());
	}
	private void appendWarning(String msg){
		StyledDocument doc = (StyledDocument)textArea.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, new Color(200, 100, 0));
        try {
			doc.insertString(doc.getLength(), msg, style);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		textArea.setCaretPosition(doc.getLength());
	}
	private void appendError(StackTraceElement[] msg, String message){
		StyledDocument doc = (StyledDocument)textArea.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, new Color(230, 0, 0));
        Style style2 = doc.addStyle("StyleName2", null);
        StyleConstants.setForeground(style2, new Color(0, 0, 220));
        try {
        	doc.insertString(doc.getLength(), message, style);
        	for (StackTraceElement e:msg){
        		
        		String m = "at "+e.getClassName()+"(";
        		String m2 = e.getFileName()+":"+e.getLineNumber();
        		String m3 = ")\n";
        		
        		doc.insertString(doc.getLength(), m, style);
        		doc.insertString(doc.getLength(), m2, style2);
        		doc.insertString(doc.getLength(), m3, style);
        	}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		textArea.setCaretPosition(doc.getLength());
	}
}

