package no.mesta.mipss.convertclient;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import no.mesta.mipss.ui.DocumentFilter;

/**
 * Importerer bilder fra SQLServer VMP til Mipss2  VMP
 * 
 * Konfigurer datakilde først:
 * 1. Ctrl-panel - admin - Datakilder
 * 2. Legg til 
 * 3. SQLServer
 * 4. navn = sqlserver
 * 	  beskrivelse = et eller annet
 * 	  server = mes-bg-25-046\VMPAKKE
 * 5. u/p = mestaro/sommer2008
 * 6. done..
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ImportVMPBilder {
	private String sqlServerUrl = "jdbc:odbc:sqlserver";
	private String oracleUrlTest = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=mes-bg-37t-002-vip.vegprod.no)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=mes-bg-37t-003-vip.vegprod.no)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=mes-bg-37t-004-vip.vegprod.no)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=PSTYRU.vegprod.no)))";
	private String oracleUrlProd = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=mes-bg-37-102-vip.vegprod.no)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=mes-bg-37-103-vip.vegprod.no)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=PSTYR.vegprod.no)))";
	private String oracleUrl;
	//	private Connection sqlServerConnection;
	private JProgressBar bar;
	private JTextArea area;
	private JTextField kontraktId;
	private JButton importButton;
	private JButton smaaButton;
	private JCheckBox overskrivCheck;
	private JRadioButton prodRadio;
	private JRadioButton testRadio;
	public ImportVMPBilder(){
		initGui();
	}
	/**
	 * Sett opp gui
	 */
	private void initGui(){
		JFrame f = new JFrame("Importerer og skalerer bilder");
		f.setSize(680, 400);
		f.setLayout(new BorderLayout());
		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		JPanel dbPanel = getHPanel();
		
		prodRadio = new JRadioButton("PROD");
		prodRadio.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (prodRadio.isSelected()){
					oracleUrl = oracleUrlProd;
					appendString("Prod-database valgt:"+oracleUrl+"\n");
				}
			}
		});
		testRadio = new JRadioButton("TEST");
		testRadio.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (testRadio.isSelected()){
					oracleUrl = oracleUrlTest;
					appendString("Test-database valgt:"+oracleUrl+"\n");
				}
			}
		});
		dbPanel.add(new JLabel("Velg database:"));
		dbPanel.add(prodRadio);
		dbPanel.add(testRadio);
		dbPanel.add(Box.createHorizontalGlue());
		ButtonGroup group = new ButtonGroup();
		group.add(prodRadio);
		group.add(testRadio);
		
		
		
		JPanel start = getHPanel();
		ImportAction action = new ImportAction();
		importButton = new JButton(action);
		smaaButton = new JButton(new ScaleAction());
		kontraktId = new JTextField(10);
		kontraktId.setDocument(new DocumentFilter());
		kontraktId.setMaximumSize(new Dimension(100,25));
		overskrivCheck = new JCheckBox("Overskriv eksisterende smaabilde_lob");
		JLabel kLabel = new JLabel("Kontrakt_Id:");
		start.add(kLabel);
		start.add(Box.createRigidArea(new Dimension(5,0)));
		start.add(kontraktId);
		start.add(Box.createRigidArea(new Dimension(15,0)));
		start.add(importButton);
		start.add(Box.createRigidArea(new Dimension(15,0)));
		start.add(smaaButton);
		start.add(Box.createRigidArea(new Dimension(5,0)));
		start.add(overskrivCheck);
		JPanel feedback = getHPanel();
		area = new JTextArea();
		JScrollPane scroll = new JScrollPane(area);
		feedback.add(scroll);
		
		panel.add(dbPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 15)));
		panel.add(start);
		panel.add(Box.createRigidArea(new Dimension(0, 5)));
		panel.add(feedback);
		panel.add(Box.createRigidArea(new Dimension(0, 5)));
		
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.PAGE_AXIS));
		southPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
		JPanel barPanel = getHPanel();
		bar = new JProgressBar();
		bar.setStringPainted(true);
		bar.setString(" ");
		barPanel.add(bar);
		southPanel.add(barPanel);
		
		f.add(panel);
		f.add(southPanel, BorderLayout.SOUTH);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}
	
	private void enableComponents(boolean enabled){
		importButton.setEnabled(enabled);
		smaaButton.setEnabled(enabled);
		kontraktId.setEnabled(enabled);
		overskrivCheck.setEnabled(enabled);
		prodRadio.setEnabled(enabled);
		testRadio.setEnabled(enabled);
	}
	/**
	 * Returnerer et panel med horizontal alignment
	 * @return
	 */
	private JPanel getHPanel(){
		JPanel pnl = new JPanel();
		pnl.setLayout(new BoxLayout(pnl, BoxLayout.LINE_AXIS));
		return pnl;
	}
	
	/**
	 * Action klasse for å starte overføringen fra funnbilde og fullfort_bilde fra vmp
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	private class ImportAction extends AbstractAction{
		public ImportAction(){
			super("Importer fra VMP");
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			if (kontraktId.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Vennligst fyll inn kontrakt id", "Kontrakt_id mangler", JOptionPane.ERROR_MESSAGE);
				return;
			}
			int kid = Integer.parseInt(kontraktId.getText());
			
			appendString("Starter konvertering for kontraktId "+kid+"\n");
			bar.setMaximum(getImportRows(kid));
			ExecutorService executorService =  Executors.newFixedThreadPool(1);
			try{
				executorService.submit(new ImportBilderWorker(kid, bar.getMaximum(),  "Ny"));
				int mx = getFullfortRows(kid);
				executorService.submit(new ImportFullfortWorker(kid, mx, "Lukket"));
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	private class ScaleAction extends AbstractAction{
		
		public ScaleAction(){
			super("Lag smaabilde_lob");
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			if (kontraktId.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Vennligst fyll inn kontrakt id", "Kontrakt_id mangler", JOptionPane.ERROR_MESSAGE);
				return;
			}
			int kid = Integer.parseInt(kontraktId.getText());
			appendString("Starter skalering av bilder for kontraktId "+kid+"\n");
			try{
			new ScaleWorker(kid, getScaleRows(kid)).execute();
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
		
	}
	private void appendString(String string){
		area.append(string);
		area.setCaretPosition(area.getText().length());
	}
	
	private byte[] getScaledInstance(InputStream imageStr, float compression){
		try {
			BufferedImage read = ImageIO.read(imageStr);
			if (read!=null){
				int w = read.getHeight()>read.getWidth()?240:320;
				int h = read.getHeight()>read.getWidth()?320:240;
				
				Image scaled = read.getScaledInstance(w, h, Image.SCALE_SMOOTH);
				ByteArrayOutputStream bas = new ByteArrayOutputStream();
				BufferedImage scaledBuff = new BufferedImage(w, h, read.getType());
				Graphics2D bufImageGraphics = scaledBuff.createGraphics();
				bufImageGraphics.drawImage(scaled, 0, 0, null);
				
				Iterator writers = ImageIO.getImageWritersBySuffix("jpeg");
				if (!writers.hasNext())
					throw new IllegalStateException("No writers found");
				//compress image
				ImageWriter writer = (ImageWriter) writers.next();
				ImageOutputStream ios = ImageIO.createImageOutputStream(bas);
				writer.setOutput(ios);
				ImageWriteParam param = writer.getDefaultWriteParam();
				if (compression >= 0) {
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					param.setCompressionQuality(compression);
				}
				
				writer.write(null, new IIOImage(scaledBuff, null, null), param);
				ios.close();
				writer.dispose();
				
//				ImageIO.write(scaledBuff, "png", bas);
				byte[] data = bas.toByteArray();
				return data;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private byte[] streamToBytes(InputStream image){
		try{
			ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
			byte[] buffer = new byte[1024];
			int len;
			while((len = image.read(buffer)) >= 0){
				out.write(buffer, 0, len);
			}
			image.close();
			out.close();
			return out.toByteArray();
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private int getImportRows(int kid){
		String select = "select count(*) " +
				"from bilde b " +
				"join punktreg_bilde pb " +
				"on pb.bilde_guid=b.guid " +
				"join punktreg p " +
				"on p.guid= pb.punktreg_guid " +
				"where b.bilde_lob is null " +
				"and pb.status='Ny' " +
				"and p.kontrakt_id="+kid;
		try (Connection c = getOracleConnection();
			PreparedStatement rows = c.prepareStatement(select);
			ResultSet rowRset = rows.executeQuery()) {
			rowRset.next();
			int max = rowRset.getInt(1);
			return max;
		} catch (SQLException e){
			return -1;
		}
	}
	
	private int getFullfortRows(int kid){
		String select = "select count(*) " +
				"from bilde b " +
				"join punktreg_bilde pb " +
				"on pb.bilde_guid=b.guid " +
				"join punktreg p " +
				"on p.guid= pb.punktreg_guid " +
				"where b.bilde_lob is null " +
				"and pb.status='Lukket' " +
				"and p.kontrakt_id="+kid;
		try(Connection c = getOracleConnection();
			PreparedStatement rows = c.prepareStatement(select);
			ResultSet rowRset = rows.executeQuery()) {
			rowRset.next();
			int max = rowRset.getInt(1);
			return max;
		} catch (SQLException e){
			return -1;
		}
	}
	
	private int getScaleRows(int kid){
		String select = "select count(*) " +
				"from bilde b " +
				"join punktreg_bilde pb on pb.bilde_guid=b.guid " +
				"join punktreg p on p.guid = pb.punktreg_guid ";
		try(Connection c = getOracleConnection()) {
			if (overskrivCheck.isSelected()){
				select += "where p.kontrakt_id="+kid;
			}else{
				select +="where b.smaabilde_lob is null "+ 
				"and p.kontrakt_id="+kid;
			}
			
			try(PreparedStatement rows = c.prepareStatement(select);
				ResultSet rowRset = rows.executeQuery()) {
				rowRset.next();
				int max = rowRset.getInt(1);
				return max;
			}
		} catch (SQLException e){
			return -1;
		}
	}
	private Connection getSQLServerConnection(){
		Connection c=null;
		try {
			c = DriverManager.getConnection(sqlServerUrl, "mestaro", "sommer2008");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Klarte ikke å koble til sqlserver\n"+e.getMessage(), "SqlServerConnection", JOptionPane.ERROR_MESSAGE);
		}
		return c;
	}
	
	private Connection getOracleConnection(){
		Connection c = null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			c = DriverManager.getConnection(oracleUrl, "mipss", "mipss");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Klarte ikke å koble til oracle\n"+e.getMessage(), "OracleServerConnection", JOptionPane.ERROR_MESSAGE);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}
	
	
	private class ImportBilderWorker extends SwingWorker{
		private int kid;
		private int count;
		private String status;
		
		public ImportBilderWorker(int kid, int count, String status){
			this.kid = kid;
			this.count = count;
			this.status = status;
			System.out.println("create importworker");
		}
		
		public Void doInBackground() throws Exception{
			enableComponents(false);
			bar.setValue(0);
			bar.setMaximum(count);
			System.out.println("doing.. importworker");
			try(Connection oracle = getOracleConnection()) {
				System.out.println("got oracle connection");
				String select = "select pb.punktreg_guid, " +
						"pb.bilde_guid " +
						"from bilde b " +
						"join punktreg_bilde pb on pb.bilde_guid=b.guid " +
						"join punktreg p on p.guid= pb.punktreg_guid " +
						"where b.bilde_lob is null " +
						"and pb.status= '" + status + "' " +
						"and p.kontrakt_id=" + kid;
				int i = 0;
				System.out.println("create statement:" + status + " " + kid);
				try (
					PreparedStatement getGuidFunnBilde = oracle.prepareStatement(select);
					ResultSet rset = getGuidFunnBilde.executeQuery()) {
					while (rset.next()) {
						System.out.println("henter bilde:" + i);
						i++;
						String punktregGuid = rset.getString(1);
						String bildeGuid = rset.getString(2);

						try(Connection sql = getSQLServerConnection();
						PreparedStatement getBilde = sql.prepareStatement(
								"select " +
										"cast(funnguid as nvarchar(100)), " +
										"cast(guid as nvarchar(100)), " +
										"bilde " +
										"from dbtFunnBilde " +
										"where cast(guid as nvarchar(100))='" + bildeGuid + "'")) {

							ResultSet bildeRS = getBilde.executeQuery();
							boolean next = bildeRS.next();
							if (!next) {
								appendString("Fant ikke bilde i SQLserver for punktreg:" + punktregGuid + " bilde:" + bildeGuid + "\n");
							}
							while (next) {
								String funnguid = bildeRS.getString(1);
								String ggid = bildeRS.getString(2);
								try (Connection update = getOracleConnection()){
									InputStream bildeStream = bildeRS.getBinaryStream(3);
									byte[] bilde = streamToBytes(bildeStream);
									update.setAutoCommit(false);
									try(PreparedStatement insert = update.prepareStatement("update bilde set bilde_lob = ? where guid=?")) {
										insert.setBytes(1, bilde);
										insert.setString(2, ggid);
										appendString("Legger inn bilde med guid:" + ggid + " : ");
										int rowCount = insert.executeUpdate();
										appendString(rowCount + " rad" + (rowCount > 1 ? "er\n" : "\n"));
										update.commit();
									}

								} catch (Exception e) {
									e.printStackTrace();
								}
								next = bildeRS.next();
							}
						}
						bar.setString("Henter bilde " + i + " av " + count);
						bar.setValue(i);
					}
					appendString("ferdig...\n");
				} catch (SQLException e1) {
					importButton.setEnabled(true);
					e1.printStackTrace();
					try {
						oracle.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return null;
		}
		public void done(){
			bar.setString("Ferdig med import ("+bar.getMaximum()+")" );
		}
	}
	
	private class ImportFullfortWorker extends SwingWorker{
		private int kid;
		private int count;
		private String status;
		
		public ImportFullfortWorker(int kid, int count, String status){
			this.kid = kid;
			this.count = count;
			this.status = status;//Lukket
		}
		
		public Void doInBackground() throws Exception {
			bar.setValue(0);
			bar.setMaximum(count);
			appendString("Sjekker etter fullforte bilder... starter.\n");
			try (Connection oracle = getOracleConnection()) {
				String select = "select pb.punktreg_guid, " +
						"pb.bilde_guid " +
						"from bilde b " +
						"join punktreg_bilde pb on pb.bilde_guid=b.guid " +
						"join punktreg p on p.guid= pb.punktreg_guid " +
						"where b.bilde_lob is null " +
						"and pb.status= 'Lukket' " +
						"and p.kontrakt_id=" + kid;
				int i = 0;
				try (PreparedStatement getGuidFunnBilde = oracle.prepareStatement(select);
					ResultSet rset = getGuidFunnBilde.executeQuery()) {
					while (rset.next()) {
						i++;
						String punktregGuid = rset.getString(1);
						String bildeGuid = rset.getString(2);
						try(Connection sql = getSQLServerConnection();
						PreparedStatement getBilde = sql.prepareStatement(
								"select " +
										"cast(guid as nvarchar(100)), " +
										"fullfortbilde " +
										"from dbtFunn " +
										"where cast(guid as nvarchar(100))='" + punktregGuid + "'");

						ResultSet bildeRS = getBilde.executeQuery()) {
							boolean next = bildeRS.next();
							if (!next) {
								appendString("Fant ikke bilde i SQLserver for punktreg:" + punktregGuid + " bilde:" + bildeGuid + "\n");
							}
							while (next) {
								String ggid = bildeRS.getString(1);

								try (Connection update = getOracleConnection()){
									InputStream bildeStream = bildeRS.getBinaryStream(2);
									byte[] bilde = streamToBytes(bildeStream);
									update.setAutoCommit(false);
									try(PreparedStatement insert = update.prepareStatement("update bilde set bilde_lob = ? where guid=?")) {
										insert.setBytes(1, bilde);
										insert.setString(2, bildeGuid);
										appendString("Legger inn bilde med guid:" + ggid + " : ");
										int rowCount = insert.executeUpdate();
										appendString(rowCount + " rad" + (rowCount > 1 ? "er\n" : "\n"));
										update.commit();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								next = bildeRS.next();
							}
						}
						bar.setString("Henter fullført bilde " + i + " av " + count);
						bar.setValue(i);
					}
					appendString("ferdig...\n");

					oracle.close();
				} catch (SQLException e1) {
					importButton.setEnabled(true);
					e1.printStackTrace();
					try {
						oracle.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				enableComponents(true);
				return null;
			}
		}
		public void done(){
			bar.setString("Ferdig med import ("+bar.getMaximum()+")" );
		}
	}
	
	private class ScaleWorker extends SwingWorker{
		private int kid;
		private int count;
		
		public ScaleWorker(int kid, int count){
			this.kid = kid;
			this.count = count;
		}
		
		public Void doInBackground() throws Exception{
			enableComponents(false);
			appendString("Sjekker etter bilder som skal skaleres... starter.\n");
			try(Connection oracle = getOracleConnection()) {
				String select = "select b.guid, " +
						"bilde_lob " +
						"from bilde b " +
						"join punktreg_bilde pb on pb.bilde_guid=b.guid " +
						"join punktreg p on p.guid = pb.punktreg_guid ";
				if (overskrivCheck.isSelected()) {
					select += "where p.kontrakt_id=" + kid;
				} else {
					select += "where b.smaabilde_lob is null " +
							"and p.kontrakt_id=" + kid;
				}

				int i = 0;
				try {
					bar.setValue(0);
					bar.setMaximum(count);
					try(PreparedStatement getGuidFunnBilde = oracle.prepareStatement(select);
						ResultSet rset = getGuidFunnBilde.executeQuery()) {
						while (rset.next()) {
							i++;
							String bildeGuid = rset.getString(1);
							InputStream bildeStream = rset.getBinaryStream(2);
							if (bildeStream != null) {
								byte[] bilde = getScaledInstance(bildeStream, 0.5f);

								Connection update = getOracleConnection();
								update.setAutoCommit(false);
								PreparedStatement insert = update.prepareStatement("update bilde set smaabilde_lob = ? where guid=?");
								insert.setBytes(1, bilde);
								insert.setString(2, bildeGuid);
								appendString("Skalerer bilde med guid:" + bildeGuid + " : ");
								int rowCount = insert.executeUpdate();
								appendString(rowCount + " rad" + (rowCount > 1 ? "er\n" : "\n"));
								update.commit();
								update.close();
							} else
								appendString("bilde med guid=" + bildeGuid + " er null\n");
							bar.setString("Skalerer bilde " + i + " av " + count);
							bar.setValue(i);
						}
						appendString("ferdig...\n");
					}
				} catch (SQLException e1) {
					importButton.setEnabled(true);
					e1.printStackTrace();
					try {
						oracle.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				enableComponents(true);
				return null;
			}
		}
		public void done(){
			bar.setString("Ferdig med skalering ("+bar.getMaximum()+")" );
		}
	}
	/**
	 * 
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		new ImportVMPBilder();

	}
}
