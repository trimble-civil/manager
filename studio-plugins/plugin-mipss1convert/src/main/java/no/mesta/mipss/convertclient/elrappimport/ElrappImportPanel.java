package no.mesta.mipss.convertclient.elrappimport;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.veinett.KontraktveiImport;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.textpane.TextPaneHelper;

@SuppressWarnings("serial")
public class ElrappImportPanel extends JPanel{

	private final ElrappImportController controller;
	private JTextPane txtInfo;
	private JProgressBar bar;
	private JButton btnImporter;
	private JButton btnSjekk;
	private JButton btnStop;
	
	private boolean pause;
	private boolean kill;
	private boolean running;

	
	private DriftkontraktValgPanel pnlDriftkontrakter;
	private JScrollPane scroll;
	
	public ElrappImportPanel(ElrappImportController controller) {
		this.controller = controller;
		initComponents();
		initGui();
	}


	private void initComponents() {
		pnlDriftkontrakter = new DriftkontraktValgPanel(controller);
		txtInfo = new JTextPane();
		btnImporter = new JButton(getImportAction());
		btnStop = new JButton(getStopAction());
		btnSjekk = new JButton(getSjekkAction());
		bar = new JProgressBar();
		bar.setStringPainted(true);
		bar.setString("Venter...");
		txtInfo.setContentType("text/html");
		
		scroll = new JScrollPane(txtInfo);
	}

	private Action getSjekkAction(){
		return new AbstractAction("Sjekk", IconResources.PLAYER_PLAY){
			@Override
			public void actionPerformed(ActionEvent e) {
				btnImporter.setEnabled(false);
				btnSjekk.setEnabled(false);
				if (running){
					pause = false;
				}else{
					new Thread(){
						public void run(){
							SwingWorker<Integer,Integer> sjekkVeinett = new SjekkWorker(bar, txtInfo, pnlDriftkontrakter.getAlleValgte());
							sjekkVeinett.execute();
							try {
								sjekkVeinett.get();
							} catch (InterruptedException e1) {
								e1.printStackTrace();
							} catch (ExecutionException e1) {
								e1.printStackTrace();
							}
						}
					}.start();
				}
			}
		};
	}
	private Action getImportAction(){
		return new AbstractAction("Start import", IconResources.PLAYER_PLAY){
			@Override
			public void actionPerformed(ActionEvent e) {
				
				btnImporter.setEnabled(false);
				btnSjekk.setEnabled(false);
				if (running){
					pause = false;
				}else{
					new Thread(){
						public void run(){
							SwingWorker<Integer,Integer> importVeinett = new ImportWorker(bar, txtInfo, pnlDriftkontrakter.getAlleValgte());
							importVeinett.execute();
							try {
								importVeinett.get();
							} catch (InterruptedException e1) {
								e1.printStackTrace();
							} catch (ExecutionException e1) {
								e1.printStackTrace();
							}
						}
					}.start();
				}
			}
		};
	}
	private Action getStopAction(){
		return new AbstractAction("Stopp", IconResources.PLAYER_STOP){
			@Override
			public void actionPerformed(ActionEvent e) {
				kill = true;
			}
		};
	}
	
	private void oppdaterKontraktListe(){
		pnlDriftkontrakter.oppdaterKontrakter();
	}
	
	private class SjekkWorker extends SwingWorker<Integer, Integer>{
		private TextPaneHelper phelper;
		private JProgressBar bar;
		private final List<KontraktTableObject> selected;
		
		public SjekkWorker(JProgressBar bar, JTextPane pane, List<KontraktTableObject> selected){
			this.selected = selected;
			phelper = new TextPaneHelper(pane);
			this.bar = bar;
		}
		protected void done(){
			running=false;
			kill=false;
			btnSjekk.setEnabled(true);
			btnImporter.setEnabled(true);
		}
		@Override
		protected Integer doInBackground() throws Exception {
			try{
				running = true;
				
				bar.setMaximum(selected.size());
				phelper.appendInfo("Starter sjekk av kontraktveinett..\n");
				int counter = 0;
				for (KontraktTableObject kontrakt:selected){
					
					String kontraktNummer = "D"+kontrakt.getKontraktnummer().substring(0, 4);
					bar.setString("Sjekker kontrakt:"+kontraktNummer+" ("+(counter+1)+"/"+selected.size()+")");
					Date elrappStartDato = kontrakt.getKontrakt().getElrappStartDato();
					String oppstartAar = MipssDateFormatter.formatDate(elrappStartDato, "yyyy");
					Date sistOppdatertVegnett = controller.getSistOppdatertVegnett(kontraktNummer, oppstartAar);
					String kontraktString = make(kontrakt.getKontraktnummer()+"/"+kontraktNummer, 20);
					if (kontrakt.getSistOppdatert()==null){
						phelper.appendWarning("Kontrakt:"+kontraktString+" \thar nytt veinett fra:"+MipssDateFormatter.formatDate(sistOppdatertVegnett, MipssDateFormatter.DATE_FORMAT)+" forrige dato=NULL\n");
					}
					else if (sistOppdatertVegnett.after(kontrakt.getSistOppdatert())){
						phelper.appendWarning("Kontrakt:"+kontraktString+" \thar nytt veinett fra:"+MipssDateFormatter.formatDate(sistOppdatertVegnett, MipssDateFormatter.DATE_FORMAT)+"\n");
					}else{
						phelper.appendInfo("Kontrakt:"+kontraktString+" \tOK\n");
					}
					
					if (pause){
						phelper.appendInfo("Kontraktsjekken er satt på pause... klikk play-knappen for å gjenoppta\n");
						btnSjekk.setEnabled(true);
						while(pause){
							try{
								Thread.sleep(500);
							}catch (InterruptedException e){}
						}
					}
					if (kill){
						phelper.appendWarning("Avbrutt av bruker.. ");
						return null;
					}
					counter++;
					bar.setValue(counter);
				}
				
				phelper.appendInfo("Sjekk av import er ferdig!!..\n");
			} catch (Throwable t){
				phelper.appendError(t.getStackTrace(), t.getMessage());
				t.printStackTrace();
			}
			return null;
		}
	}
	
	private class ImportWorker extends SwingWorker<Integer, Integer>{
		private TextPaneHelper phelper;
		private JProgressBar bar;
		private final List<KontraktTableObject> selected;
		
		public ImportWorker(JProgressBar bar, JTextPane pane, List<KontraktTableObject> selected){
			this.selected = selected;
			phelper = new TextPaneHelper(pane);
			this.bar = bar;
		}
		protected void done(){
			running=false;
			kill=false;
			btnSjekk.setEnabled(true);
			btnImporter.setEnabled(true);
			oppdaterKontraktListe();
		}
		@Override
		protected Integer doInBackground() throws Exception {
			try{
				running = true;
				
				bar.setMaximum(selected.size()-1);
				phelper.appendInfo("Starter import av kontraktveinett..\n");
				int counter = 0;
				for (KontraktTableObject kontrakt:selected){
					bar.setValue(counter);
					String kontraktNummer = "D"+kontrakt.getKontraktnummer().substring(0, 4);
					bar.setString("Importerer kontrakt:"+kontraktNummer+" ("+(counter+1)+"/"+selected.size()+")");
					Date elrappStartDato = kontrakt.getKontrakt().getElrappStartDato();
					String oppstartAar = MipssDateFormatter.formatDate(elrappStartDato, "yyyy");
					Date sistOppdatertVegnett = controller.getSistOppdatertVegnett(kontraktNummer, oppstartAar);
					String kontraktString = make(kontrakt.getKontraktnummer()+"/"+kontraktNummer, 20);
					if (kontrakt.getSistOppdatert()==null){
						//TODO hent nytt veinett
						phelper.appendInfo("Henter nytt veinett for:"+kontraktString+" forrige = NULL \n");
						try{
							List<KontraktveiImport> nyttVeinett = controller.hentImportForKontrakt(kontraktNummer, oppstartAar);
							controller.persistImport(kontrakt.getKontrakt(), nyttVeinett, sistOppdatertVegnett);
							phelper.appendInfo(kontraktString+" OK\n");
						} catch (ArithmeticException e) {
							phelper.appendInfo("Kontrakt:" + kontraktString + " : \tFEILET (ArithmeticException)\n");
						}				
					}
					else if (sistOppdatertVegnett == null) {
						phelper.appendInfo("Kontrakt:" + kontraktString + " : \tFEILET (Fant ikke veinett i ELRAPP (SistOppdatertVegnett er null).)\n");
					}
					
					else if (sistOppdatertVegnett.after(kontrakt.getSistOppdatert())){
						phelper.appendInfo("Henter nytt veinett for:"+kontraktString+"\n");
						List<KontraktveiImport> nyttVeinett = controller.hentImportForKontrakt(kontraktNummer, oppstartAar);
						controller.persistImport(kontrakt.getKontrakt(), nyttVeinett, sistOppdatertVegnett);
						phelper.appendInfo(kontraktString+" OK\n");
					}else{
						//NO NEED
						phelper.appendInfo("Kontrakt:"+kontraktString+" : \tEr allerede oppdatert\n");
					}
					
					if (pause){
						phelper.appendInfo("Kontraktveinettimport er satt på pause... klikk play-knappen for å gjenoppta\n");
						btnImporter.setEnabled(true);
						while(pause){
							try{
								Thread.sleep(500);
							}catch (InterruptedException e){}
						}
					}
					if (kill){
						phelper.appendWarning("Avbrutt av bruker.. ");
						return null;
					}
					counter++;
				}
				
				phelper.appendInfo("Import av kontraktveinett er ferdig!!..\n");
			} catch (Throwable t){
				phelper.appendError(t.getStackTrace(), t.getMessage());
				t.printStackTrace();
			}
			return null;
		}
	}
	private String make(String string, int len){
		int length = string.length();
		StringBuilder sb = new StringBuilder();
		sb.append(string);
		int count = length;
		while (count<len){
			sb.append(" ");
			count++;
		}
		return sb.toString();
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		
		JPanel pnlRight = new JPanel(new GridBagLayout());
		pnlRight.add(btnImporter, 	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 	  new Insets(0,5,5,0), 0,0));
		pnlRight.add(btnSjekk, 		new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 	  new Insets(0,5,5,0), 0,0));
		pnlRight.add(btnStop, 		new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 	  new Insets(0,5,5,0), 0,0));
		pnlRight.add(bar, 			new GridBagConstraints(0,1, 3,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,5), 0,0));
		pnlRight.add(scroll, 		new GridBagConstraints(0,2, 3,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, 	  new Insets(0,5,5,5), 0,0));
		
		add(pnlDriftkontrakter, new GridBagConstraints(0,0, 1,1, 0.0,1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(5,5,5,5), 0,0));
		add(pnlRight, 			new GridBagConstraints(1,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, 	new Insets(5,0,5,5), 0,0));
	}

	
}
