package no.mesta.mipss.convertclient;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.convertclient.elrappimport.ElrappImportController;
import no.mesta.mipss.convertclient.elrappimport.ElrappImportPanel;
import no.mesta.mipss.convertclient.nyereflink.OpprettNyeProdReflinkseksjoner;
import no.mesta.mipss.convertclient.nyereflink.OpprettNyeReflinkPanel;
import no.mesta.mipss.convertclient.rapporter.RapportController;
import no.mesta.mipss.convertclient.rapporter.RapportPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.service.veinett.Convert;
import no.mesta.mipss.webservice.NvdbWebservice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* @author Harald A Kulø
*/
public class ConverterClient extends MipssPlugin{

	private static final Logger log = LoggerFactory.getLogger(ConverterClient.class);
	
	private JPanel mainPanel;
	private VeinettImportPanel modulePanel;
	private OpprettNyeReflinkPanel reflinkPanel;
	private OpprettNyeProdReflinkseksjoner prodreflinkPanel;
	private ElrappImportPanel pnlElrappImport;
	private RapportPanel pnlRapporter;
	private NvdbWebservice nvdb;
	private PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("mipss1convertText");
	
	@Override
	public JComponent getModuleGUI() {
		return mainPanel;
	}

	@Override
	protected void initPluginGUI() {
        log.info("Initiating GUI");
        log.debug("Creating panels");
		modulePanel = new VeinettImportPanel(this);
		reflinkPanel = new OpprettNyeReflinkPanel(this);
		prodreflinkPanel = new OpprettNyeProdReflinkseksjoner();
		pnlElrappImport = new ElrappImportPanel(new ElrappImportController(this));
		pnlRapporter = new RapportPanel(new RapportController(this));

        log.debug("Creating tab pane");
		JTabbedPane tab = new JTabbedPane();
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(tab);
		tab.addTab("Veinettimport", modulePanel);
		tab.addTab("Opprette veireferanser", reflinkPanel);
		tab.addTab("Opprette prodreflinkseksjoner", prodreflinkPanel);
		tab.addTab("Importer fra Elrapp", pnlElrappImport);
		tab.addTab("Rapporter", pnlRapporter);

        log.debug("Looking up Webservice bean");
		nvdb = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
		if (!nvdb.isNvdbLoaded()){
			JOptionPane.showMessageDialog(getLoader().getApplicationFrame(), props.getGuiString("nvdb.notloaded"), props.getGuiString("nvdb.notloaded.title"), JOptionPane.WARNING_MESSAGE);
		}
		
	}
	@Override
	public void shutdown() {
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	private void p(Veinett veinett){
//		log.debug(veinett.getGuiFarge());
//		log.debug(veinett.getGuiTykkelse());
//		List<Veinettreflinkseksjon> v = veinett.getVeinettreflinkseksjonList();
//		for (Veinettreflinkseksjon r:v){
//			log.debug("\t"+r.getFra()+"  "+r.getTil()+"  "+r.getReflinkIdent());
//			for (Veinettveireferanse ref : r.getVeinettveireferanseList()){
//				log.debug("\t\t"+ref.getVeikategori()+" "+ref.getVeinummer()+" "+ref.getVeistatus());
//			}
//		}
//	}
//	
//	public void testRoadrefsWithinReflinkSections(Long reflinkId, double from, double to){
//		log.debug("testRoadrefsWithinReflinkSections("+reflinkId+", "+from+", "+to+")");
//		
//		ArrayOfReflinkSeksjon reflinkArray = new ArrayOfReflinkSeksjon();
//		ReflinkSeksjon ref = new ReflinkSeksjon();
//		ref.setReflinkId(reflinkId.intValue());
//		ref.setFra(from);
//		ref.setTil(to);
//		reflinkArray.getReflinkSeksjon().add(ref);
//		Holder<ArrayOfVeireferanse> results = new Holder<ArrayOfVeireferanse>();
//		Holder<Boolean> historicFlag = new Holder<Boolean>();
//		getGisService().getRoadrefsWithinReflinkSections(reflinkArray, results, historicFlag);
//		
//		if (results.value==null){
//			log.debug("NO RESULTS\n");
//			return;
//		}
//		
//		List<Veireferanse> veiref = results.value.getVeireferanse();
//		for (Veireferanse v: veiref){
//			log.debug(v.getFrahp()+" "+v.getTilhp()+" "+v.getFraMeter()+" "+v.getTilMeter());
//		}
//	}
//	
//	public void testGetReflinkSectionsWithinHP(Long hp, Double km){
//		log.debug("testGetReflinkSectionsWithinHP("+hp+", "+km+", ALL_HP)");
//		List<ReflinkSeksjon> rs = getGisService().getReflinkSectionsWithinHp(hp.intValue(), km.doubleValue(), ReflinkSectionDirection.ALL_HP).getReflinkSeksjon();
//		log.debug("Antall reflikseksjoner:"+rs.size());
//		for (ReflinkSeksjon s : rs){
//			log.debug(s.getFra()+ " "+s.getTil()+" "+s.getReflinkId());
//		}
//		
//	}
//	
//	private GisServicesSoap getGisService(){
//    	if (gisService==null){
//    		gisService = new GisServices().getGisServicesSoap();
//    	}
//    	
//    	return gisService;
//    }
	/*private static void p(Veinett veinett){
	log.debug(veinett.getGuiFarge());
	log.debug(veinett.getGuiTykkelse());
	List<Veinettreflinkseksjon> v = veinett.getVeinettreflinkseksjonList();
	for (Veinettreflinkseksjon r:v){
		log.debug("\t"+r.getFra()+"  "+r.getTil()+"  "+r.getReflinkIdent());
	}
}*/
}
