package no.mesta.mipss.convertclient.rapporter;

import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;

import no.mesta.mipss.admin.rapporter.gui.SqlRapportPanel;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.convertclient.ConverterClient;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.produksjonsrapport.AdminrapportService;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.util.EpostUtils;

public class RapportController {
	
	private ConverterClient plugin;
	private AdminrapportService rapportService;
	private PropertyResourceBundleUtil properties = PropertyResourceBundleUtil.getPropertyBundle("mipss1convertText");
	
	public RapportController(ConverterClient plugin) {
		this.plugin = plugin;
		rapportService = BeanUtil.lookup(AdminrapportService.BEAN_NAME, AdminrapportService.class);
	}
	
	public Action getVeinettManglerAction() {
		return createSqlRapportAction(properties.getGuiString("label.rapporter.veinettMangler"), 
				new SqlRapportPanel(properties.getGuiString("title.rapporter.manglerRapport")){
			@Override
			protected Map<String, Vector<?>> getData(){
				try {
					return getVeinettManglerRapport();
				} catch (Exception e) {
					e.printStackTrace();
					ErrorHandler.showError(e, properties.getGuiString("error.rapporter.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		});
	}
	
	public Action getVeinettTilleggAction() {
		return createSqlRapportAction(properties.getGuiString("label.rapporter.veinettTillegg"), 
				new SqlRapportPanel(properties.getGuiString("title.rapporter.tilleggsRapport")){
			@Override
			protected Map<String, Vector<?>> getData(){
				try {
					return getVeinettTilleggRapport();
				} catch (Exception e) {
					e.printStackTrace();
					ErrorHandler.showError(e, properties.getGuiString("error.rapporter.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		});
	}
	
	public Map<String, Vector<?>> getVeinettManglerRapport() {
		return rapportService.hentVeinettManglerRapport();
	}
	public Map<String, Vector<?>> getVeinettTilleggRapport() {
		return rapportService.hentVeinettTilleggRapport();
	}
	
	private Action createSqlRapportAction(final String title, final SqlRapportPanel panel){
		return new AbstractAction(title, IconResources.SPREADSHEET_SMALL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog d = new JDialog(plugin.getLoader().getApplicationFrame(), title);
				panel.loadTable();
				d.add(panel);
				d.setSize(500, 500);
				d.setLocationRelativeTo(d.getParent());
				d.setVisible(true);
			}
		};
	}
}
