package no.mesta.mipss.convertclient.rapporter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class RapportPanel extends JPanel{

	private JButton btnMangler;
	private JButton btnTillegg;
	private JLabel lblBeskrivelse;
	private RapportController controller;
	private PropertyResourceBundleUtil properties = PropertyResourceBundleUtil.getPropertyBundle("mipss1convertText");

	public RapportPanel(RapportController controller) {
		this.controller = controller;
		initComponents();
		initGui();
	}

	private void initComponents() {
		lblBeskrivelse = new JLabel(properties.getGuiString("label.rapporter.rapportbeskrivelse"));
		btnMangler = new JButton(controller.getVeinettManglerAction());
		btnTillegg = new JButton(controller.getVeinettTilleggAction());		
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(lblBeskrivelse, new GridBagConstraints(0,0,1,1,1.0,0.0,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 0),0,0));
		add(btnMangler, 	new GridBagConstraints(0,1,1,1,1.0,0.0,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 0),0,0));
		add(btnTillegg, 	new GridBagConstraints(0,2,1,1,1.0,1.0,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0),0,0));		
	}
}
