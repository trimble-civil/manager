package no.mesta.mipss;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.accesscontrol.PluginAccessException;

@SuppressWarnings("serial")
public class StudioOvervaaking extends JFrame {
	private String brukerSignatur = "lareid";
	private String smtpServer = "mes-bg-2e-005";
	private long modulId = Long.valueOf(7);
	private long pingWaitSeconds = 10;
	private long mailWaitMillis = 100;
	private long maxResponseMillis = 5000;	
	private String mailAddress = "mahild@mesta.no";
	
	private JLabel lblOppdateringsintervall = new JLabel();
	private JLabel lblMaxResponse = new JLabel();
	private JLabel lblLastResponse = new JLabel();
	private JLabel lblLastUpdate = new JLabel();
	private JLabel lblConfMail = new JLabel();
	private JLabel lblConfJNDI = new JLabel();
	private JLabel lblBrukerSignatur = new JLabel();
	private JLabel lblModulId = new JLabel();
	private JLabel lblSmtpServer = new JLabel();
	private JButton btnStartOvervaakning = new JButton();
	private JButton btnLukk = new JButton();
	private JButton btnSendMail = new JButton();
	private JButton btnEndreMail = new JButton();
	private JButton btnEndreMaxRespons = new JButton();
	private JButton btnEndreBrukerSignatur = new JButton();
	private JButton btnEndreModulId = new JButton();
	private JButton btnEndreSmtpServer = new JButton();
	private JButton btnEndrePingWait = new JButton();
	private JPanel pnlButtons = new JPanel();
	
	private AccessControl bean;
	
	private boolean shutDown = false;
	private boolean waitingForResponse;
	private long requestStartMillis;
	private Thread mailThread;
	private Thread pingThread;
	
	public StudioOvervaaking() {
		init();
		initGui();
		showGui();
	}
	
	public void showGui() {
		this.setTitle("Mipss studio overvåking");
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	private void init() {
		try {
        	InitialContext context = new InitialContext();
            bean = (AccessControl)PortableRemoteObject.narrow(context.lookup(AccessControl.BEAN_NAME), AccessControl.class);
        } catch (NamingException e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        pingThread = new Thread(new Runnable() {
        	@Override
			public void run() {
				try {
					while(!shutDown) {
						waitingForResponse = true;
						requestStartMillis = System.currentTimeMillis();
						try {
							bean.hasAccess(brukerSignatur, modulId);
						} catch (PluginAccessException e) {
							e.printStackTrace();
							//Ikke farlig
						}
						waitingForResponse = false;
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								updateGui(System.currentTimeMillis() - requestStartMillis);
							}
						});
						Thread.sleep(pingWaitSeconds * 1000);
					}
				} catch (Exception e) {
					e.printStackTrace();
					sendMail("Tråden som overvåker mipss gikk ned med feilen:\n" + e.getMessage());
					shutDown = true;
					System.exit(1);
				}
				System.out.println("pingThread avslutter");
			}
		});
        
        mailThread = new Thread(new Runnable() {
        	@Override
			public void run() {
				try {
					while(!shutDown) {
						if(waitingForResponse) {
							long currentResponseMillis = System.currentTimeMillis() - requestStartMillis; 
							if(currentResponseMillis > maxResponseMillis) {
								sendMail("For lang responstid: " + currentResponseMillis + " ms eller mer");
								shutDown = true;
							}
						}
						
						Thread.sleep(mailWaitMillis);
					}
				} catch (Exception e) {
					e.printStackTrace();
					sendMail("Tråden som sender mail gikk ned med feilen:\n" + e.getMessage());
					shutDown = true;
					System.exit(1);
				}
				System.out.println("mailThread avslutter");
			}
		});
	}
	
	private void startOvervaakning() {
        pingThread.start();
        try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        mailThread.start();
	}
	
	private synchronized void sendMail(final String message) {
		System.out.println("---> SENDER MAIL: " + message);

		try {
			Properties props = new Properties(); props.put("mail.smtp.host", smtpServer); 
			Session session = Session.getDefaultInstance(props,null);
			
			Message mailMessage = new MimeMessage(session);

			mailMessage.setFrom(new InternetAddress("no-reply@mesta.no", "Mipss overvåkning"));
			mailMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(mailAddress));
			
			mailMessage.setSubject("MIPPS HENGER!!!");
			mailMessage.setText(message);
			mailMessage.setSentDate(new Date());

			Transport.send(mailMessage); 
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					JOptionPane.showMessageDialog(StudioOvervaaking.this, "Mail sendt pga: \n" + message);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private synchronized void updateGui(final long responseTime) {
		lblLastResponse.setText("Responstid: " + responseTime + " ms");
		lblLastUpdate.setText("Siste oppdatering: " + Calendar.getInstance().getTime());
	}
	
	private synchronized void updateStaticGui() {
		lblMaxResponse.setText("Responsterskel: " + maxResponseMillis + " ms");
		lblConfMail.setText("Mail: " + mailAddress);
		lblOppdateringsintervall.setText("Venter " + pingWaitSeconds + " sekunder mellom hvert serverkall");
		lblConfJNDI.setText("JNDI host: " + getServer());
		lblBrukerSignatur.setText("Brukersignatur for overvåkning: " + brukerSignatur);
		lblModulId.setText("ModulId for overvåkning: " + modulId);
		lblSmtpServer.setText("SMTP-server: " + smtpServer);
	}
	
    private String getServer() {
    	try {
    		URI uri = new URI(new InitialContext().getEnvironment().get(InitialContext.PROVIDER_URL).toString()); 
			return uri.getHost() + ":" + uri.getPort();
		} catch (Throwable t) {
			return "Feil ved henting";
		}
    }
	
	private void initGui() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				shutDown = true;
				dispose();
			}
		});
		btnStartOvervaakning.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnStartOvervaakning.setEnabled(false);
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						startOvervaakning();
					}
				});
			}
		});
		btnSendMail.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sendMail("Manuell test av mail!");
			}
		});
		
		btnLukk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				shutDown = true;
				dispose();		
			}
		});
		btnEndreMail.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String nyEpost = JOptionPane.showInputDialog(StudioOvervaaking.this, "Skriv inn ny epostadresse:");
				if(nyEpost != null && nyEpost.length() > 0) {
					mailAddress = nyEpost;
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							updateStaticGui();
						}
					});
				}
			}
		});
		btnEndreMaxRespons.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String nyTerskel = JOptionPane.showInputDialog(StudioOvervaaking.this, "Skriv inn ny maks responstid:");
				if(nyTerskel != null && nyTerskel.length() > 0) {
					try{
						maxResponseMillis = Long.parseLong(nyTerskel);
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								updateStaticGui();
							}
						});
					} catch(NumberFormatException nfe) {}
				}
			}
		});
		btnEndreBrukerSignatur.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String nySignatur = JOptionPane.showInputDialog(StudioOvervaaking.this, "Skriv inn ny brukersignatur (6 tegn) for kall mot server:");
				if(nySignatur != null && nySignatur.length() == 6) {
					brukerSignatur = nySignatur;
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							updateStaticGui();
						}
					});
				}
			}
		});
		btnEndreModulId.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String nyModulId = JOptionPane.showInputDialog(StudioOvervaaking.this, "Skriv inn ny modulId for kall mot server:");
				if(nyModulId != null && nyModulId.length() > 0) {
					try{
						modulId = Long.parseLong(nyModulId);
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								updateStaticGui();
							}
						});
					} catch(NumberFormatException nfe) {}
				}
			}
		});
		btnEndreSmtpServer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String nySmtpServer = JOptionPane.showInputDialog(StudioOvervaaking.this, "Skriv inn ny SMTP-server:");
				if(nySmtpServer != null && nySmtpServer.length() > 0) {
					smtpServer = nySmtpServer;
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							updateStaticGui();
						}
					});
				}
			}
		});
		btnEndrePingWait.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String nyPingWait = JOptionPane.showInputDialog(StudioOvervaaking.this, "Skriv inn ny hviletid mellom kall til server:");
				if(nyPingWait != null && nyPingWait.length() > 0) {
					try{
						pingWaitSeconds = Long.parseLong(nyPingWait);
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								updateStaticGui();
							}
						});
					} catch(NumberFormatException nfe) {}
				}
			}
		});
		
		btnStartOvervaakning.setText("Start");
		btnLukk.setText("Lukk");
		btnSendMail.setText("Send testmail");
		btnEndreMail.setText("...");
		btnEndreMaxRespons.setText("...");
		btnEndreBrukerSignatur.setText("...");
		btnEndreModulId.setText("...");
		btnEndreSmtpServer.setText("...");
		btnEndrePingWait.setText("...");
		
		updateStaticGui();
		updateGui(0);
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnStartOvervaakning, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnSendMail, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnLukk, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        this.setLayout(new GridBagLayout());
        this.add(lblConfJNDI, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(lblBrukerSignatur, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(btnEndreBrukerSignatur, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(lblModulId, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(btnEndreModulId, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(lblSmtpServer, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(btnEndreSmtpServer, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(lblConfMail, new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(btnEndreMail, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(lblMaxResponse, new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(btnEndreMaxRespons, new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(lblOppdateringsintervall, new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(btnEndrePingWait, new GridBagConstraints(1, 6, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(lblLastResponse, new GridBagConstraints(0, 7, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(lblLastUpdate, new GridBagConstraints(0, 8, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 11), 0, 0));
        this.add(pnlButtons, new GridBagConstraints(0, 9, 2, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(19, 11, 11, 11), 0, 0));
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(new net.java.plaf.windows.WindowsLookAndFeel());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new StudioOvervaaking();
	}
}
