@echo off
echo Installerer f�lgende avhengigheter (dependecies) for maven 2
echo i det lokale bibliotekslageret (.m2/repository).
echo.
echo Avhengigheter:
dir non_mavenized

echo ====== start! ======

echo ====== jms ======
call mvn install:install-file -DgroupId=javax.jms -DartifactId=jms -Dversion=1.1 -Dpackaging=jar -Dfile=non_mavenized\jms-1.1.jar

echo ====== jmxri ======
call mvn install:install-file -DgroupId=com.sun.jmx -DartifactId=jmxri -Dversion=1.2.1 -Dpackaging=jar -Dfile=non_mavenized\jmxri-1.2.1.jar

echo ====== jmxtools ======
call mvn install:install-file -DgroupId=com.sun.jdmk -DartifactId=jmxtools -Dversion=1.2.1 -Dpackaging=jar -Dfile=non_mavenized\jmxtools-1.2.1.jar

echo ====== winlaf ======
call mvn install:install-file -DgroupId=net.java -DartifactId=winlaf -Dversion=0.5.1 -Dpackaging=jar -Dfile=non_mavenized\winlaf-0.5.1.jar

echo ====== swingx-ws ======
call mvn install:install-file -DgroupId=org.swinglabs -DartifactId=swingx-ws -Dversion=2008_05_18 -Dpackaging=jar -Dfile=non_mavenized\swingx-ws-2008_05_18.jar

echo ====== ojdbc14 ======
call mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.2.0 -Dpackaging=jar -Dfile=non_mavenized\ojdbc14-10.2.0.2.0.jar

echo ====== swingx ======
call mvn install:install-file -DgroupId=org.swinglabs -DartifactId=swingx -Dversion=2008_06_08 -Dpackaging=jar -Dfile=non_mavenized\swingx-2008_06_08.jar

echo ====== jdicplus ======
call mvn install:install-file -DgroupId=org.jdesktop -DartifactId=jdicplus -Dversion=0.2.2 -Dpackaging=jar -Dfile=non_mavenized\JDICplus.jar

echo ====== jdicplus  native ======
call mvn install:install-file -DgroupId=org.jdesktop -DartifactId=jdicpluss-native -Dversion=0.2.2 -Dpackaging=jar -Dfile=non_mavenized\JDICplus_native_applet.jar

echo ====== beansbinding ======
call mvn install:install-file -DgroupId=org.jdesktop -DartifactId=beansbinding -Dversion=1.2.1 -Dpackaging=jar -Dfile=non_mavenized\beansbinding-1.2.1.jar

echo ====== Oracle SOAP ======
call mvn install:install-file -DgroupId=com.oracle -DartifactId=soap -Dversion=10.1.3.3.0 -Dpackaging=jar -Dfile=non_mavenized\soap-10.1.3.3.0.jar

echo ====== Oracle http_client ======
call mvn install:install-file -DgroupId=com.oracle -DartifactId=http_client -Dversion=10.1.3.3.0 -Dpackaging=jar -Dfile=non_mavenized\http_client-10.1.3.3.0.jar

echo ====== JAXB XJC Maven 2 Plugin ======
call mvn install:install-file -DgroupId=com.sun.tools.xjc.maven2 -DartifactId=maven-jaxb-plugin -Dversion=1.1 -Dpackaging=jar -Dfile=non_mavenized\maven-jaxb-plugin-1.1.jar

echo ====== Optic OPMN lookup ======
call mvn install:install-file -DgroupId=com.oracle -DartifactId=optic -Dversion=10.1.3.4 -Dpackaging=jar -Dfile=non_mavenized\optic-10.1.3.4.jar

echo ====== Swinglabs Wizard ======
call mvn install:install-file -DgroupId=org.jdesktop -DartifactId=wizard -Dversion=pre.1.0 -Dpackaging=jar -Dfile=non_mavenized\wizard.jar

echo ====== robotframework swinglibrary ======
call mvn install:install-file -DgroupId=robotframework -DartifactId=swinglibrary -Dversion=0.6 -Dpackaging=jar -Dfile=non_mavenized\swinglibrary-0.6.jar

echo ====== java excel api (jexcelapi) ======
call mvn install:install-file -DgroupId=jexcelapi -DartifactId=jexcelapi -Dversion=2.6.9 -Dpackaging=jar -Dfile=non_mavenized\jexcelapi-2.6.9.jar

echo ====== jasperreports-maven-plugin ======
cd ..\jasperreports-maven-plugin
call mvn clean install

echo.
echo ====== ferdig! ======

