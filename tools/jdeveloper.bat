@echo off
echo Dette skriptet lager deg en .jws fil for ditt prosjekt.
echo.
echo Legg til tools i din path, og kall dette skriptet fra
echo mappa til maven prosjektet du vil legge til jdeveloper,
echo eller kall skriptet med full path.
echo.
echo Du vil f� en .jws fil for jdeveloper for en versjon som er
echo noe eldre enn den Mesta bruker. Dette skal ikke v�re noe
echo problem.
echo.
echo ====== start! ======
call mvn org.apache.myfaces.trinidadbuild:maven-jdev-plugin:jdev

echo ====== ferdig! ======
